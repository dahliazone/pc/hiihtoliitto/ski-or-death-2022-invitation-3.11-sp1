#!/bin/bash

inputDir="data-to-scale"
outputDir="data/textures/SkiOrDeath/gen"
mkdir -p "${outputDir}"
tempImg="TEMP.png"
format="-define png:format=png32 -define png:exclude-chunks=date +set date:create +set date:modify -strip"

scale320x2000Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 1280x8000\! "$tempImg"
  # w = 1440*(320/320), h=1080*(2000/200)
  convert "$tempImg" $format -resize 1440x10800\! "$outputDir/$1"
  rm "$tempImg"
}

scale320x400Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 1280x4200\! "$tempImg"
  # w = 1440*(320/320), h=1080*(400/200)
  convert "$tempImg" $format -resize 1440x2160\! "$outputDir/$1"
  rm "$tempImg"
}

scale320x200Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 1280x2400\! "$tempImg"
  # w = 1440*(320/320), h=1080*(200/200)
  convert "$tempImg" $format -resize 1440x1080\! "$outputDir/$1"
  rm "$tempImg"
}

scale500x200Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 2000x2400\! "$tempImg"
  # w = 1440*(500/320), h=1080*(200/200)
  convert "$tempImg" $format -resize 2250x1080\! "$outputDir/$1"
  rm "$tempImg"
}

scale320x100Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 1280x1200\! "$tempImg"
  # w = 1440*(320/320), h=1080*(100/200)
  convert "$tempImg" $format -resize 1440x540\! "$outputDir/$1"
  rm "$tempImg"
}

scale320x50Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 1280x600\! "$tempImg"
  # w = 1440*(320/320), h=1080*(50/200)
  convert "$tempImg" $format -resize 1440x270\! "$outputDir/$1"
  rm "$tempImg"
}

scale342x50Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 1368x600\! "$tempImg"
  # w = 1440*(342/320), h=1080*(50/200)
  convert "$tempImg" $format -resize 1539x270\! "$outputDir/$1"
  rm "$tempImg"
}

scale140x50Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 560x600\! "$tempImg"
  # w = 1440*(140/320), h=1080*(50/200)
  convert "$tempImg" $format -resize 630x270\! "$outputDir/$1"
  rm "$tempImg"
}

scale160x240Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 640x2880\! "$tempImg"
  # w = 1440*(160/320), h=1080*(240/200)
  convert "$tempImg" $format -resize 720x1296\! "$outputDir/$1"
  rm "$tempImg"
}

scale256x200Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 1024x2400\! "$tempImg"
  # w = 1440*(256/320), h=1080*(200/200)
  convert "$tempImg" $format -resize 1152x1080\! "$outputDir/$1"
  rm "$tempImg"
}

scale100x100Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 400x1200\! "$tempImg"
  # w = 1440*(100/320), h=1080*(100/200)
  convert "$tempImg" $format -resize 450x540\! "$outputDir/$1"
  rm "$tempImg"
}

scale1325x40Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 5300x480\! "$tempImg"
  # w = 1440*(1325/320), h=1080*(40/200)
  convert "$tempImg" $format -resize 5962x216\! "$outputDir/$1"
  rm "$tempImg"
}

scale2650x80Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 10600x960\! "$tempImg"
  # w = 1440*(2650/320), h=1080*(80/200)
  convert "$tempImg" $format -resize 11925x432\! "$outputDir/$1"
  rm "$tempImg"
}

scale150x50Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 600x600\! "$tempImg"
  # w = 1440*(150/320), h=1080*(50/200)
  convert "$tempImg" $format -resize 675x270\! "$outputDir/$1"
  rm "$tempImg"
}

scale150x650Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 600x7800\! "$tempImg"
  # w = 1440*(150/320), h=1080*(650/200)
  convert "$tempImg" $format -resize 675x3510\! "$outputDir/$1"
  rm "$tempImg"
}

scale100x50Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 400x600\! "$tempImg"
  # w = 1440*(100/320), h=1080*(50/200)
  convert "$tempImg" $format -resize 450x270\! "$outputDir/$1"
  rm "$tempImg"
}

scale200x50Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 800x600\! "$tempImg"
  # w = 1440*(200/320), h=1080*(50/200)
  convert "$tempImg" $format -resize 900x270\! "$outputDir/$1"
  rm "$tempImg"
}

scale50x50Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 200x600\! "$tempImg"
  # w = 1440*(50/320), h=1080*(50/200)
  convert "$tempImg" $format -resize 225x270\! "$outputDir/$1"
  rm "$tempImg"
}

scale400x50Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 1600x600\! "$tempImg"
  # w = 1440*(400/320), h=1080*(50/200)
  convert "$tempImg" $format -resize 1800x270\! "$outputDir/$1"
  rm "$tempImg"
}

scale128x160Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 512x1920\! "$tempImg"
  # w = 1440*(128/320), h=1080*(160/200)
  convert "$tempImg" $format -resize 576x864\! "$outputDir/$1"
  rm "$tempImg"
}

scale144x160Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 576x1920\! "$tempImg"
  # w = 1440*(144/320), h=1080*(160/200)
  convert "$tempImg" $format -resize 648x864\! "$outputDir/$1"
  rm "$tempImg"
}

scale480x80Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 1920x960\! "$tempImg"
  # w = 1440*(480/320), h=1080*(80/200)
  convert "$tempImg" $format -resize 2160x432\! "$outputDir/$1"
  rm "$tempImg"
}

scale150x150Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 600x1800\! "$tempImg"
  # w = 1440*(150/320), h=1080*(150/200)
  convert "$tempImg" $format -resize 675x810\! "$outputDir/$1"
  rm "$tempImg"
}

scale448x25Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 1792x300\! "$tempImg"
  # w = 1440*(448/320), h=1080*(25/200)
  convert "$tempImg" $format -resize 2016x135\! "$outputDir/$1"
  rm "$tempImg"
}

scale450x125Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 1800x1500\! "$tempImg"
  # w = 1440*(450/320), h=1080*(125/200)
  convert "$tempImg" $format -resize 2025x675\! "$outputDir/$1"
  rm "$tempImg"
}

scale450x150Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 1800x1800\! "$tempImg"
  # w = 1440*(450/320), h=1080*(150/200)
  convert "$tempImg" $format -resize 2025x810\! "$outputDir/$1"
  rm "$tempImg"
}

scale28x400Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 112x4800\! "$tempImg"
  # w = 1440*(28/320), h=1080*(400/200)
  convert "$tempImg" $format -resize 126x2160\! "$outputDir/$1"
  rm "$tempImg"
}

scale60x20Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 240x240\! "$tempImg"
  # w = 1440*(60/320), h=1080*(20/200)
  convert "$tempImg" $format -resize 270x108\! "$outputDir/$1"
  rm "$tempImg"
}

scale120x20Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 480x240\! "$tempImg"
  # w = 1440*(120/320), h=1080*(20/200)
  convert "$tempImg" $format -resize 540x108\! "$outputDir/$1"
  rm "$tempImg"
}

scale240x20Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 960x240\! "$tempImg"
  # w = 1440*(240/320), h=1080*(20/200)
  convert "$tempImg" $format -resize 1080x108\! "$outputDir/$1"
  rm "$tempImg"
}

scale840x20Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 3360x240\! "$tempImg"
  # w = 1440*(840/320), h=1080*(20/200)
  convert "$tempImg" $format -resize 3780x108\! "$outputDir/$1"
  rm "$tempImg"
}

scale196x40Img () {
  echo "Scaling '$inputDir/$1' -> '$outputDir/$1'..."
  # scale w*4, h*12
  convert "$inputDir/$1" $format -scale 784x480\! "$tempImg"
  # w = 1440*(196/320), h=1080*(40/200)
  convert "$tempImg" $format -resize 882x216\! "$outputDir/$1"
  rm "$tempImg"
}

scale320x200Img  "pipe.png"
scale320x50Img   "doomstatus.png"
scale320x2000Img "downhill-blitz.png"
scale342x50Img   "downhill-skier.png"
scale100x50Img   "go-die.png"
scale100x50Img   "go-ski.png"
scale100x50Img   "grue.png"
scale320x200Img  "highscores3.png"
scale320x200Img  "highscores3-layer.png"
scale448x25Img   "hiihtoheikki1-anim-h.png"
scale28x400Img   "hiihtoheikki1-anim-v.png"
scale320x200Img  "hiihtoliitto.png"
scale320x200Img  "hiihtoliitto-black.png"
scale320x200Img  "hiihtoliitto-white.png"
scale140x50Img   "himoslogo-50px.png"
scale500x200Img  "himos-rinteet.png"
scale450x125Img  "juominen.png"
scale450x150Img  "lappu.png"
scale320x200Img  "map.png"
scale196x40Img   "map-hissi-anim.png"
scale60x20Img    "map-sign.png"
scale120x20Img   "map-sign-anim.png"
scale240x20Img   "map-sign-anim2.png"
scale840x20Img   "map-sign-anim3.png"
scale150x50Img   "msgbox.png"
scale100x100Img  "p3.png"
scale2650x80Img  "p3-anim2.png"
scale50x50Img    "p3-mini.png"
scale1325x40Img  "p3-anim2-mini.png"
scale50x50Img    "raato.png"
scale50x50Img    "rengas.png"
scale50x50Img    "rengas2.png"
scale256x200Img  "sauva-l-2.png"
scale256x200Img  "sauva-r-2.png"
scale320x200Img  "screen-black.png"
scale320x200Img  "screen-white.png"
scale50x50Img    "seisoja-l.png"
scale50x50Img    "seisoja-r.png"
scale320x200Img  "seppo.png"
scale320x200Img  "seppo-bg.png"
scale320x200Img  "seppo-mask-black.png"
scale320x200Img  "seppo-mask-white.png"
scale480x80Img   "seppoface.png"
scale320x200Img  "signin2.png"
scale320x200Img  "signin2-layer.png"
scale144x160Img  "skifont2.png"
scale144x160Img  "skifont2-pink.png"
scale128x160Img  "skifont3.png"
scale160x240Img  "skifont-big-black.png"
scale160x240Img  "skifont-big-black-smooth.png"
scale160x240Img  "skifont-big-blue.png"
scale160x240Img  "skifont-big-blue-smooth.png"
scale160x240Img  "skifont-big-cyan.png"
scale160x240Img  "skifont-big-cyan-smooth.png"
scale160x240Img  "skifont-big-green.png"
scale160x240Img  "skifont-big-green-smooth.png"
scale160x240Img  "skifont-big-magenta.png"
scale160x240Img  "skifont-big-magenta-smooth.png"
scale160x240Img  "skifont-big-pink.png"
scale160x240Img  "skifont-big-pink-smooth.png"
scale160x240Img  "skifont-big-red.png"
scale160x240Img  "skifont-big-red-smooth.png"
scale160x240Img  "skifont-big-white.png"
scale160x240Img  "skifont-big-white-smooth.png"
scale160x240Img  "skifont-big-yellow.png"
scale160x240Img  "skifont-big-yellow-smooth.png"
scale400x50Img   "snoukkaaja-anim1.png"
scale150x650Img  "snoukkaaja-anim2.png"
scale50x50Img    "snoukkari.png"
scale320x200Img  "snowballdoom.png"
scale320x200Img  "snowballdoom-comp.png"
scale320x200Img  "sojashi.png"
scale320x100Img  "speech-bubble-empty.png"
scale320x50Img   "title-subtitle.png"
scale320x200Img  "title1.png"
scale320x400Img  "title2.png"
scale320x200Img  "zombseppo.png"

