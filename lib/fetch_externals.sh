#!/bin/bash


echo "Partakone: Fetching externals from repositories..."

if [ ! -d ../data/gui/iconsttf ]; then
	git clone https://gitlab.com/dahliazone/pc/extlib/IconFontCppHeaders-ttf.git ../data/gui/iconsttf
else
	pushd ../data/gui/iconsttf
	git pull --rebase
	popd
fi

if [ ! -d ../data/gui/icons ]; then
	git clone https://gitlab.com/dahliazone/pc/extlib/material-design-icons-json.git ../data/gui/icons
else
	pushd ../data/gui/icons
	git pull --rebase
	popd
fi

if [ ! -d IconFontCppHeaders ]; then
	git clone https://gitlab.com/dahliazone/pc/extlib/IconFontCppHeaders.git IconFontCppHeaders
else
	pushd IconFontCppHeaders
	git pull --rebase
	popd
fi

if [ ! -d verso-depthio ]; then
	git clone https://gitlab.com/dahliazone/pc/lib/verso-depthio.git verso-depthio
else
	pushd verso-depthio
	git pull --rebase
	popd
fi

echo "verso-depthio: Resursing into..."
pushd verso-depthio
./fetch_parent_externals.sh
popd

echo "Partakone: All done"

