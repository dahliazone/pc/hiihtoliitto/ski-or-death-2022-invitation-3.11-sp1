#!/bin/bash

wc -l ../include/Verso/*.hpp \
../include/Verso/Display/*.hpp \
../include/Verso/GrinderKit/*.hpp \
../include/Verso/GrinderKit/DemoParts/*.hpp \
../include/Verso/GrinderKit/Params/*.hpp \
../include/Verso/Gui/*.hpp \
../include/Verso/Gui/imgui/*.hpp \
../include/Verso/Render/*.hpp \
../include/Verso/Render/Camera/*.hpp \
../include/Verso/Render/Light/*.hpp \
../include/Verso/Render/Material/*.hpp \
../include/Verso/Render/Opengl/*.hpp \
../include/Verso/Render/Particle/*.hpp \
../include/Verso/Render/Particle/Behaviours/*.hpp \
../include/Verso/Render/Particle/Effects/*.hpp \
../include/Verso/Render/Particle/Emitters/*.hpp \
../include/Verso/Render/Particle/Sequencers/*.hpp \
../include/Verso/Render/Scenegraph/*.hpp \
../include/Verso/Render/Shader/*.hpp \
../include/Verso/Render/Texture/*.hpp \
../include/Verso/Render/TextureAtlas/*.hpp \
../include/Verso/Render/Vao/*.hpp \
../include/Verso/Render/VaoGenerator/*.hpp \
../include/Verso/System/*.hpp \
../src/Verso/Display/*.cpp \
../src/Verso/GrinderKit/*.cpp \
../src/Verso/GrinderKit/DemoParts/*.cpp \
../src/Verso/GrinderKit/Params/*.cpp \
../src/Verso/Gui/*.cpp \
../src/Verso/Gui/imgui/*.cpp \
../src/Verso/Render/*.cpp \
../src/Verso/Render/Camera/*.cpp \
../src/Verso/Render/Material/*.cpp \
../src/Verso/Render/Particle/*.cpp \
../src/Verso/Render/Particle/Effects/*.cpp \
../src/Verso/Render/Particle/Emitters/*.cpp \
../src/Verso/Render/Particle/Sequencers/*.cpp \
../src/Verso/Render/Shader/*.cpp \
../src/Verso/Render/Texture/*.cpp \
../src/Verso/Render/TextureAtlas/*.cpp \
../src/Verso/Render/Vao/*.cpp \
../src/Verso/Render/VaoGenerator/*.cpp \
../src/Verso/System/*.cpp

