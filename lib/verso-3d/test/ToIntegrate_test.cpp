
/*
#include <Verso/Audio.hpp>
#include <Verso/Display.hpp>
#include <Verso/Input.hpp>
#include <Verso/Math.hpp>
#include <Verso/Render.hpp>
#include <Verso/System.hpp>
#include <vector>
#include <iostream>

using namespace std;
using namespace Verso;


inline void testRect()
{
	Recti rect(1, 2, 3, 4);
	cout << "Test Recti instance:" << endl;
	cout << "    toStringDebug() = "<<rect.toStringDebug() << endl;
	cout << "    toString() = "<<rect.toString() << endl;
	cout << "    << = "<<rect << endl;
	cout << "    calculateArea() = "<<rect.calculateArea() << endl << endl;
}


inline void testSize()
{
	Sizei size(2, 3);
	cout << "Test Size<size_t> instance:" << endl;
	cout << "    toStringDebug() = "<<size.toStringDebug() << endl;
	cout << "    toString() = "<<size.toString() << endl;
	cout << "    << = "<<size << endl;
	cout << "    calculateArea() = "<<size.calculateArea() << endl << endl;
}


inline void testBlendMode()
{
	BlendMode blend1 = BlendMode::None;
	cout << "    blend1(BlendMode::None) = "<<blendModeToString(blend1) << endl;
	BlendMode blend2 = BlendMode::Transcluent;
	cout << "    blend2(BlendMode::Transcluent) = "<<blendModeToString(blend2) << endl;
	BlendMode blend3 = BlendMode::Additive;
	cout << "    blend3(BlendMode::Additive) = "<<blendModeToString(blend3) << endl;
	BlendMode blend4 = BlendMode::Subtractive;
	cout << "    blend4(BlendMode::Subtractive) = "<<blendModeToString(blend4) << endl;
	BlendMode blend5 = BlendMode::ReverseSubtractive;
	cout << "    blend5(BlendMode::ReverseSubtractive) = "<<blendModeToString(blend5) << endl;
}


inline void testSwapInterval()
{
	cout << "Test SwapInterval" << endl;
	cout << "    swapIntervalToString(SwapInterval::Immediate) => "<<swapIntervalToString(SwapInterval::Immediate) << endl;
	cout << "    swapIntervalToString(SwapInterval::Vsync) => "<<swapIntervalToString(SwapInterval::Vsync) << endl;
	cout << "    swapIntervalToString(SwapInterval::VsyncLateSwapTearing) => "<<swapIntervalToString(SwapInterval::VsyncLateSwapTearing) << endl;
	cout << "    swapIntervalToString(SwapInterval::Unset) => "<<swapIntervalToString(SwapInterval::Unset) << endl << endl;
}


inline void testWindowStyle()
{
	cout << "Test WindowStyle" << endl;
	cout << "    windowStyleToString(WindowStyle::None) => "<<windowStyleToString(WindowStyle::None) << endl;
	cout << "    windowStyleToString(WindowStyle::Fullscreen) => "<<windowStyleToString(WindowStyle::Fullscreen) << endl;
	cout << "    windowStyleToString(WindowStyle::Borderless) => "<<windowStyleToString(WindowStyle::Borderless) << endl;
	cout << "    windowStyleToString(WindowStyle::Resizable) => "<<windowStyleToString(WindowStyle::Resizable) << endl;
	cout << "    windowStyleToString(WindowStyle::Hidden) => "<<windowStyleToString(WindowStyle::Hidden) << endl;
	cout << "    windowStyleToString(WindowStyle::Default) => "<<windowStyleToString(WindowStyle::Default) << endl;
	cout << "    windowStyleToString(Fullscreen | Resizable)  =>  "<<windowStylesToString(WindowStyle::Fullscreen | WindowStyle::Resizable) << endl;
	cout << "    windowStyleToString(Borderless | Resizable)  =>  "<<windowStylesToString(WindowStyle::Borderless | WindowStyle::Resizable) << endl;
	cout << "    windowStyleToString(Fullscreen | Borderless | Resizable)  =>  "<<windowStylesToString(WindowStyle::Fullscreen | WindowStyle::Borderless | WindowStyle::Resizable) << endl;
	cout << "    windowStyleToString(Fullscreen | Borderless | Resizable | Hidden)  =>  "<<windowStylesToString(WindowStyle::Fullscreen | WindowStyle::Borderless | WindowStyle::Resizable | WindowStyle::Hidden) << endl << endl;
}

inline void testDisplayMode()
{
	DisplayMode dm(1280, 720, 32);
	cout << "Test DisplayMode instance:" << endl;
	cout << "    toStringDebug() = "<<dm.toStringDebug() << endl;
	cout << "    toString() = "<<dm.toString() << endl;
	cout << "    isFullscreenMode() = "<<dm.isFullscreenMode() << endl;
	cout << "    << = "<<dm << endl << endl;
	cout << "    size.toStringDebug() = "<<dm.size.toStringDebug() << endl;
}


inline void testDisplay()
{
	Display display(0, "Test Display", Recti(7, 8, 123, 321));
	cout << "Test Display instance:" << endl;
	cout << "    toStringDebug() = "<<display.toStringDebug() << endl;
	cout << "    toString() = "<<display.toString() << endl;
	cout << "    << = "<<display << endl << endl;
}


inline void testAllDisplays()
{
	vector<Display> displays = Display::getDisplays();
	//for (size_t i=0; i<displays.size(); ++i) {
	for (auto& display : displays) {
		cout << "Test "<<display.toStringDebug()<<":" << endl;
		cout << "    "<<"getDesktopMode() = "<<display.getDesktopMode().toStringDebug() << endl;
		cout << "    "<<"getCurrentMode() = "<<display.getCurrentMode().toStringDebug() << endl;
		cout << "    "<<"getFullscreenModes() = " << endl;
		for (const auto& dm : display.getFullscreenModes())
			cout << "        "<<dm.toStringDebug() << endl;
		cout << endl << endl;
	}
}


inline void testMultisampleAntialiasLevel()
{
	cout << "    MultisampleAntialiasLevel::MsaaOff = "<<multisampleAntialiasLevelToString(MultisampleAntialiasLevel::MsaaOff)<<" = "<<multisampleAntialiasLevelToString(intToMultisampleAntialiasLevel(0)) << endl;
	cout << "    MultisampleAntialiasLevel::Msaa2x = "<<multisampleAntialiasLevelToString(MultisampleAntialiasLevel::Msaa2x)<<" = "<<multisampleAntialiasLevelToString(intToMultisampleAntialiasLevel(2)) << endl;
	cout << "    MultisampleAntialiasLevel::Msaa4x = "<<multisampleAntialiasLevelToString(MultisampleAntialiasLevel::Msaa4x)<<" = "<<multisampleAntialiasLevelToString(intToMultisampleAntialiasLevel(4)) << endl;
	cout << "    MultisampleAntialiasLevel::Msaa8x = "<<multisampleAntialiasLevelToString(MultisampleAntialiasLevel::Msaa8x)<<" = "<<multisampleAntialiasLevelToString(intToMultisampleAntialiasLevel(8)) << endl;
	cout << "    MultisampleAntialiasLevel::Msaa16x = "<<multisampleAntialiasLevelToString(MultisampleAntialiasLevel::Msaa16x)<<" = "<<multisampleAntialiasLevelToString(intToMultisampleAntialiasLevel(16)) << endl;
}


inline void testContexts()
{
	ContextSettings settings;
	Context context1(false, settings);
	Context context2(true, settings);

	context1.makeActive();
	context2.makeActive();

	cout << "    context1.toString() = "<<context1.toString() << endl;
	cout << "    context1.toStringDebug() = "<<context1.toStringDebug() << endl;
	cout << "    context2.toString() = "<<context2.toString() << endl;
	cout << "    context2.toStringDebug() = "<<context2.toStringDebug() << endl;
}


inline void testDegreeRadian()
{
	Degree degrees1 = 180.0f+45.0f;
	Radian radians1 = static_cast<float>(M_PI) / 4.0f;
	cout << "    degrees1 = "<<degrees1<<" => "<<degreesToRadians(degrees1)<<" radians" << endl;
	cout << "    radians1 = "<<radians1<<" => "<<radiansToDegrees(radians1)<<" degrees" << endl;
	cout << "    degreesToString("<<degrees1<<") = "<<degreesToString(degrees1) << endl;
	cout << "    radiansToString("<<radians1<<") = "<<radiansToString(radians1) << endl;
}


inline void testAlign()
{
	Sizei objSizei(10, 10);
	Sizef objSizef(3, 3);
	Align align1;
	cout << "    align1().toString() = "<<align1.toString() << endl;
	cout << "    align1().toStringDebug() = "<<align1.toStringDebug() << endl;
	cout << "    align1.pointOffseti(Sizei(10,10)) = "<<align1.pointOffseti(objSizei) << endl;
	cout << "    align1.pointOffsetf(Sizef(3,3)) = "<<align1.pointOffsetf(objSizef) << endl;

	Align align2(HAlign::Left, VAlign::Top);
	cout << "    align2(HAlign::Left, VAlign::Top).toString() = "<<align2.toString() << endl;
	cout << "    align2(HAlign::Left, VAlign::Top).toStringDebug() = "<<align2.toStringDebug() << endl;
	cout << "    align2.pointOffseti(Sizei(10,10)) = "<<align2.pointOffseti(objSizei) << endl;
	cout << "    align2.pointOffsetf(Sizef(3,3)) = "<<align2.pointOffsetf(objSizef) << endl;

	Align align3(HAlign::Center, VAlign::Center);
	cout << "    align3(HAlign::Center, VAlign::Center).toString() = "<<align3.toString() << endl;
	cout << "    align3(HAlign::Center, VAlign::Center).toStringDebug() = "<<align3.toStringDebug() << endl;
	cout << "    align3.pointOffseti(Sizei(10,10)) = "<<align3.pointOffseti(objSizei) << endl;
	cout << "    align3.pointOffsetf(Sizef(3,3)) = "<<align3.pointOffsetf(objSizef) << endl;

	Align align4(HAlign::Right, VAlign::Bottom);
	cout << "    align4(HAlign::Right, VAlign::Bottom).toString() = "<<align4.toString() << endl;
	cout << "    align4(HAlign::Right, VAlign::Bottom).toStringDebug() = "<<align4.toStringDebug() << endl;
	cout << "    align4.pointOffseti(Sizei(10,10)) = "<<align4.pointOffseti(objSizei) << endl;
	cout << "    align4.pointOffsetf(Sizef(3,3)) = "<<align4.pointOffsetf(objSizef) << endl;
	cout << "    align4.pointOffsetCenteredQuad() = "<<align4.pointOffsetCenteredQuad() << endl;
	cout << "    align4.objectInContainerOffseti(Sizei(10,10), Sizei(1000,1000)) = "<<align4.objectInContainerOffseti(objSizei, Sizei(1000, 1000)) << endl;
	cout << "    align4.objectInContainerOffsetf(Sizef(0.5,0.5), Sizef(1,1)) = "<<align4.objectInContainerOffsetf(Sizef(0.5f, 0.5f), Sizef(1, 1)) << endl;
	cout << "    align4.objectInContainerOffseti(Sizei(10,10), Recti(500,500, 1000,1000)) = "<<align4.objectInContainerOffseti(objSizei, Recti(500, 500, 1000, 1000)) << endl;
	cout << "    align4.objectInContainerOffsetf(Sizef(0.5,0.5), Rectf(0.5,0.5, 1,1)) = "<<align4.objectInContainerOffsetf(Sizef(0.25f, 0.25f), Rectf(0.5f, 0.5f, 1, 1)) << endl;

	Align align5(HAlign::Undefined, VAlign::Undefined);
	cout << "    align5(HAlign::Undefined, VAlign::Undefined).toString() = "<<align5.toString() << endl;
	cout << "    align5(HAlign::Undefined, VAlign::Undefined).toStringDebug() = "<<align5.toStringDebug() << endl;
	cout << "    align5.pointOffseti(Sizei(10,10)) = "<<align5.pointOffseti(objSizei) << endl;
	cout << "    align5.pointOffsetf(Sizef(3,3)) = "<<align5.pointOffsetf(objSizef) << endl;

	Align align6;
	cout << "    Align align6().toString() = "<<align6.toString() << endl;
	align6.set(HAlign::Left, VAlign::Top);
	cout << "    align6.set(HAlign::Left, VAlign::Top).toString() = "<<align6.toString() << endl;
	cout << "    align6.set(HAlign::Left, VAlign::Top).toStringDebug() = "<<align6.toStringDebug() << endl;
}


inline void testVector2()
{
	Vector2f zero;
	cout << "    zero << "<<zero << endl;
	cout << "    zero.toString() = "<<zero.toString() << endl;
	cout << "    zero.toStringDebug() = "<<zero.toStringDebug() << endl << endl;

	Vector2f vec1(4.5f, 2.7f);
	cout << "    vec1(4.5f, 2.7f) << "<<vec1 << endl;;
	cout << "    vec1(4.5f, 2.7f).toString() = "<<vec1.toString() << endl;
	cout << "    vec1(4.5f, 2.7f).toStringDebug() = "<<vec1.toStringDebug() << endl << endl;

	Vector2f vec1Copy(vec1);
	cout << "    vec1Copy << "<<vec1Copy << endl;
	cout << "    vec1Copy.toString() = "<<vec1Copy.toString() << endl;
	cout << "    vec1Copy.toStringDebug() = "<<vec1Copy.toStringDebug() << endl << endl;

	Vector2f vec2;
	vec2.set(2, 3);
	cout << "    vec2.set(2,3) << "<<vec2 << endl;;
	cout << "    vec2(2,3).toString() = "<<vec2.toString() << endl;
	cout << "    vec2(2,3).toStringDebug() = "<<vec2.toStringDebug() << endl << endl;

	Vector2f vec2Copy;
	vec2Copy.set(vec2);
	cout << "    vec2Copy.set(vec2) << "<<vec2Copy << endl;;
	cout << "    vec2Copy.toString() = "<<vec2Copy.toString() << endl;
	cout << "    vec2Copy.toStringDebug() = "<<vec2Copy.toStringDebug() << endl << endl;

	Vector2f vec3(2, 1);
	cout << "    vec3(2,1) << "<<vec3 << endl;
	cout << "    vec3(2,1).getLength() << "<<vec3.getLength() << endl;
	vec3.normalize();
	cout << "    vec3.normalize();" << endl;
	cout << "    vec3("<<vec3.x<<","<<vec3.y<<").getLength() << "<<vec3.getLength() << endl << endl;

	Vector2f vec4(-1, -2);
	cout << "    vec4(-1,-2) << "<<vec4 << endl;
	cout << "    vec4(-1,-2).getLength() << "<<vec4.getLength() << endl;
	vec4.normalize();
	cout << "    vec4.normalize();" << endl;
	cout << "    vec4("<<vec4.x<<","<<vec4.y<<").getLength() << "<<vec4.getLength() << endl << endl;

	Vector2f vec5(1, 1);
	cout << "    vec5(1,1) << "<<vec5 << endl;
	cout << "    vec5(1,1).getLength() << "<<vec5.getLength() << endl;
	vec5.normalize();
	cout << "    vec5.normalize();" << endl;
	cout << "    vec5("<<vec5.x<<","<<vec5.y<<").getLength() << "<<vec5.getLength() << endl << endl;

	Vector2f vec6(0, -1);
	cout << "    vec6(1,1) << "<<vec6 << endl;
	cout << "    vec6(1,1).getLength() << "<<vec6.getLength() << endl;
	vec6.normalize();
	cout << "    vec6.normalize();" << endl;
	cout << "    vec6("<<vec6.x<<","<<vec6.y<<").getLength() << "<<vec6.getLength() << endl << endl;

	Vector2f vec7(2, 3);
	cout << "    vec7(2,3) << "<<vec7 << endl;
	vec7.scalarMultiplication(1.5f);
	cout << "    vec7.scalarMultiplication(1.5f);" << endl;
	cout << "    vec7 << "<<vec7 << endl << endl;

	Vector2f vec8(3, 2);
	cout << "    vec8(3,2) << "<<vec8 << endl;
	vec8.scalarMultiplication(-1.5f);
	cout << "    vec8.scalarMultiplication(-1.5f);" << endl;
	cout << "    vec8 << "<<vec8 << endl << endl;

	Vector2f vec9(3, 0);
	cout << "    vec9(3,0) << "<<vec9 << endl;
	vec9.rotateAroundZ(90);
	cout << "    vec9.rotateAroundZ(90);" << endl;
	cout << "    vec9 << "<<vec9 << endl;
	vec9.rotateAroundZ(90);
	cout << "    vec9.rotateAroundZ(90);" << endl;
	cout << "    vec9 << "<<vec9 << endl;
	vec9.rotateAroundZ(90);
	cout << "    vec9.rotateAroundZ(90);" << endl;
	cout << "    vec9 << "<<vec9 << endl;
	vec9.rotateAroundZ(90);
	cout << "    vec9.rotateAroundZ(90);" << endl;
	cout << "    vec9 << "<<vec9 << endl << endl;

	Vector2f vec10(1, 2);
	Vector2f vec11(0.5f, 1);
	cout << "    vec10(1, 2) + vec11(0.5f, 1) = "<<(vec10+vec11) << endl;
	cout << "    vec10(1, 2) - vec11(0.5f, 1) = "<<(vec10-vec11) << endl;
	cout << "    vec10(1, 2) * 1.5f = "<<(vec10*1.5f) << endl;
	cout << "    vec10(1, 2) / 1.5f = "<<(vec10/1.5f) << endl;
	cout << "    -vec10(1, 2) = "<<(-vec10) << endl;
	cout << "    -vec11(0.5f, 1) = "<<(-vec11) << endl;

	vec10 += Vector2f(0.5, -0.5);
	cout << "    vec10(1, 2) += Vector2f(0.5, -0.5) => "<<vec10 << endl;
	vec11 -= Vector2f(-0.5, 0.5);
	cout << "    vec11(0.5, 1) -= Vector2f(-0.5, 0.5) => "<<vec11 << endl;

	vec10.set(1, 2);
	vec10 *= -0.5f;
	cout << "    vec10(1, 2) *= -0.5 => "<<vec10 << endl;
	vec11.set(0.5, 1);
	vec11 /= -0.5f;
	cout << "    vec11(0.5, 1) /= -0.5 => "<<vec11 << endl;

	Vector2f vec12(-4, -9);
	cout << "    vec12(-4, -9).dotProduct(Vector2f(-1, 2)) => "<<vec12.dotProduct(Vector2f(-1, 2))<<" (should be -14)" << endl;

	cout << "    Vector2f::up() << "<<Vector2f::up() << endl;
	cout << "    Vector2f::down() << "<<Vector2f::down() << endl;
	cout << "    Vector2f::left() << "<<Vector2f::left() << endl;
	cout << "    Vector2f::right() << "<<Vector2f::right() << endl;
}


inline void testRgbaColorf()
{
	RgbaColorf color1;
	cout << "    color1 << "<<color1 << endl;
	cout << "    color1.toString() = "<<color1.toString() << endl;
	cout << "    color1.toStringDebug() = "<<color1.toStringDebug() << endl << endl;

	RgbaColorf color2(0.1f, 0.2f, 0.3f, 0.4f);
	cout << "    color2 = "<<color2 << endl;
	cout << "    color2.toString() = "<<color2.toString() << endl;
	cout << "    color2.toStringDebug() = "<<color2.toStringDebug() << endl;
	cout << "    color2.getScaledHue(0.5f) << "<<color2.getScaledHue(0.5f) << endl;
	color2.set(0.9f, 0.8f, 0.7f);
	cout << "    color2.set(0.9f, 0.8f, 0.7f) => "<<color2 << endl;
	color2.set(0.1f, 0.2f, 0.3f, 0.4f);
	cout << "    color2.set(0.1f, 0.2f, 0.3f, 0.4f) => "<<color2 << endl << endl;

	GLfloat color2Arr[4];
	GLfloat* color2ArrPtr = nullptr;
	color2ArrPtr = color2.getArrayRgba(color2Arr);
	cout << "    color2Arr[] = [ "<<color2Arr[0]<<", "<<color2Arr[1]<<", "<<color2Arr[2]<<", "<<color2Arr[3]<<" ]" << endl;
	cout << "    color2ArrPtr[] = [ "<<color2ArrPtr[0]<<", "<<color2ArrPtr[1]<<", "<<color2ArrPtr[2]<<", "<<color2ArrPtr[3]<<" ]" << endl;
	RgbaColorf color2Copy(color2);
	cout << "    color2Copy.toStringDebug() = "<<color2Copy.toStringDebug() << endl << endl;

	RgbaColorf color2Copy2;
	color2Copy2.set(color2);
	cout << "    color2Copy2.set(color2Copy2) => "<<color2Copy2.toStringDebug() << endl << endl;

	RgbaColorf underColor1(-1.0f, -0.2f, -1.4f, -0.000001f);
	cout << "    underColor1 = "<<underColor1 << endl;
	cout << "    underColor1.clamped() = "<<underColor1.clamped() << endl;
	cout << "    underColor1.normalized() = "<<underColor1.normalized() << endl << endl;

	RgbaColorf underColor2(-0.000001f, 0.0f, 1.0f, 0.5f);
	cout << "    underColor2 = "<<underColor2 << endl;
	cout << "    underColor2.clamped() = "<<underColor2.clamped() << endl;
	cout << "    underColor2.normalized() = "<<underColor2.normalized() << endl << endl;

	RgbaColorf overColor1(10, 1, 5, 1.5f);
	cout << "    overColor1 = "<<overColor1 << endl;
	cout << "    overColor1.clamped(0.1f, 0.6f) = "<<overColor1.clamped(0.1f, 0.6f) << endl;
	cout << "    overColor1.normalized(0.6f, 0.9f) = "<<overColor1.normalized(0.6f, 0.9f) << endl << endl;

	cout << "    RgbaColorf::red() << "<<RgbaColorf::red() << endl;
	cout << "    RgbaColorf::red(0.5f) << "<<RgbaColorf::red(0.5f) << endl;
	cout << "    RgbaColorf::green() << "<<RgbaColorf::green() << endl;
	cout << "    RgbaColorf::blue() << "<<RgbaColorf::blue() << endl;
	cout << "    RgbaColorf::niceBackgroundGray() << "<<RgbaColorf::niceBackgroundGray() << endl;
	cout << "    RgbaColorf::niceBackgroundGray2() << "<<RgbaColorf::niceBackgroundGray2() << endl << endl;

	RgbaColorf color3(0.1f, 0.2f, 0.3f, 0.4f);
	cout << "    color1 = "<<color1 << endl;
	cout << "    color2 = "<<color2 << endl;
	cout << "    color3 = "<<color3 << endl;
	cout << "    color1 == color1 => "<<(color1 == color1) << "    color1 != color1 => "<<(color1 != color1) << endl;
	cout << "    color2 == color2 => "<<(color2 == color2) << "    color2 != color2 => "<<(color2 != color2) << endl;
	cout << "    color3 == color3 => "<<(color3 == color3) << "    color3 != color3 => "<<(color3 != color3) << endl;
	cout << "    color1 == color2 => "<<(color1 == color2) << "    color1 != color2 => "<<(color1 != color2) << endl;
	cout << "    color2 == color1 => "<<(color2 == color1) << "    color2 != color1 => "<<(color2 != color1) << endl;
	cout << "    color2 == color3 => "<<(color2 == color3) << "    color2 != color3 => "<<(color2 != color3) << endl;
	cout << "    color3 == color2 => "<<(color3 == color2) << "    color3 != color2 => "<<(color3 != color2) << endl;
	cout << "    color1 == color3 => "<<(color1 == color3) << "    color1 != color3 => "<<(color1 != color3) << endl;
	cout << "    color3 == color1 => "<<(color3 == color1) << "    color3 1= color1 => "<<(color3 != color1) << endl << endl;

	cout << "    color1 = "<<color1 << endl;
	cout << "    color2 = "<<color2 << endl;
	cout << "    color3 = "<<color3 << endl;
	cout << "    color1+color2 = "<<(color1+color2) << "    color2+color1 = "<<(color2+color1) << endl;
	cout << "    color1+color3 = "<<(color1+color3) << "    color3+color1 = "<<(color3+color1) << endl;
	cout << "    color2+color3 = "<<(color2+color3) << "    color3+color2 = "<<(color3+color2) << endl << endl;

	cout << "    color1 = "<<color1 << endl;
	cout << "    color2 = "<<color2 << endl;
	cout << "    color3 = "<<color3 << endl;
	cout << "    color1-color2 = "<<(color1-color2) << "    color2-color1 = "<<(color2-color1) << endl;
	cout << "    color1-color3 = "<<(color1-color3) << "    color3-color1 = "<<(color3-color1) << endl;
	cout << "    color2-color3 = "<<(color2-color3) << "    color3-color2 = "<<(color3-color2) << endl << endl;

	cout << "    color1 = "<<color1 << endl;
	cout << "    color2 = "<<color2 << endl;
	cout << "    color3 = "<<color3 << endl;
	cout << "    color1*color2 = "<<(color1*color2) << "    color2*color1 = "<<(color2*color1) << endl;
	cout << "    color1*color3 = "<<(color1*color3) << "    color3*color1 = "<<(color3*color1) << endl;
	cout << "    color2*color3 = "<<(color2*color3) << "    color3*color2 = "<<(color3*color2) << endl << endl;

	float ratio = 1.5f;
	cout << "    color1*"<<ratio<<" = "<<(color1*ratio) << endl;
	cout << "    color2*"<<ratio<<" = "<<(color2*ratio) << endl;
	cout << "    color3*"<<ratio<<" = "<<(color3*ratio) << endl << endl;

	cout << "    color1 = "<<color1 << endl;
	cout << "    color2 = "<<color2 << endl;
	cout << "    color3 = "<<color3 << endl;
	cout << "    color1/color2 = "<<(color1/color2) << "    color2/color1 = "<<(color2/color1) << endl;
	cout << "    color1/color3 = "<<(color1/color3) << "    color3/color1 = "<<(color3/color1) << endl;
	cout << "    color2/color3 = "<<(color2/color3) << "    color3/color2 = "<<(color3/color2) << endl << endl;

	float divider = 2.0f;
	cout << "    color1/"<<divider<<" = "<<(color1/divider) << endl;
	cout << "    color2/"<<divider<<" = "<<(color2/divider) << endl;
	cout << "    color3/"<<divider<<" = "<<(color3/divider) << endl << endl;

	RgbaColorf color4(0.2f, 0.3f, 0.4f, 1.0f);
	cout << "    color4 = "<<color4 << endl;
	cout << "    color4 *= "<<ratio<<" => "<<(color4 *= ratio) << endl;
	color4.set(0.2f, 0.3f, 0.4f, 1.0f);
	cout << "    color4 /= "<<divider<<" => "<<(color4 /= ratio) << endl << endl;
	color4.set(0.2f, 0.3f, 0.4f, 1.0f);

	RgbaColorf color5(0.1f, 0.2f, 0.3f, 0.4f);
	cout << "    color4 += "<<color5<<" => "<<(color4 += color5) << endl;
	color4.set(0.2f, 0.3f, 0.4f, 1.0f);
	cout << "    color4 -= "<<color5<<" => "<<(color4 -= color5) << endl;
	color4.set(0.2f, 0.3f, 0.4f, 1.0f);
	cout << "    color4 *= "<<color5<<" => "<<(color4 *= color5) << endl;
	color4.set(0.2f, 0.3f, 0.4f, 1.0f);
	cout << "    color4 /= "<<color5<<" => "<<(color4 /= color5) << endl;
	color4.set(0.2f, 0.3f, 0.4f, 1.0f);
}


inline void testMath()
{
	// TODO: test Math

	cout << "    Math::toNearestBiggerPowerOfTwo<int>(-5) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<int>(-5)) << endl;
	cout << "    Math::toNearestBiggerPowerOfTwo<int>(-4) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<int>(-4)) << endl;
	cout << "    Math::toNearestBiggerPowerOfTwo<int>(-3) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<int>(-3)) << endl;
	cout << "    Math::toNearestBiggerPowerOfTwo<int>(-2) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<int>(-2)) << endl;
	cout << "    Math::toNearestBiggerPowerOfTwo<int>(-1) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<int>(-1)) << endl;
	cout << "    Math::toNearestBiggerPowerOfTwo<int>(0) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<int>(0)) << endl;
	cout << "    Math::toNearestBiggerPowerOfTwo<int>(1) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<int>(1)) << endl;
	cout << "    Math::toNearestBiggerPowerOfTwo<int>(2) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<int>(2)) << endl;
	cout << "    Math::toNearestBiggerPowerOfTwo<int>(3) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<int>(3)) << endl;
	cout << "    Math::toNearestBiggerPowerOfTwo<int>(4) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<int>(4)) << endl;
	cout << "    Math::toNearestBiggerPowerOfTwo<int>(5) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<int>(5)) << endl;
	cout << "    Math::toNearestBiggerPowerOfTwo<int>(6) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<int>(6)) << endl;
	cout << "    Math::toNearestBiggerPowerOfTwo<int>(7) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<int>(7)) << endl;
	cout << "    Math::toNearestBiggerPowerOfTwo<int>(8) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<int>(8)) << endl;
	cout << "    Math::toNearestBiggerPowerOfTwo<int>(63) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<int>(63)) << endl;
	cout << "    Math::toNearestBiggerPowerOfTwo<int>(64) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<int>(64)) << endl;
	cout << "    Math::toNearestBiggerPowerOfTwo<int>(65) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<int>(65)) << endl << endl;

	cout << "    Math::toNearestBiggerPowerOfTwo<size_t>(0) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<size_t>(0)) << endl;
	cout << "    Math::toNearestBiggerPowerOfTwo<size_t>(1) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<size_t>(1)) << endl;
	cout << "    Math::toNearestBiggerPowerOfTwo<size_t>(2) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<size_t>(2)) << endl;
	cout << "    Math::toNearestBiggerPowerOfTwo<size_t>(3) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<size_t>(3)) << endl;
	cout << "    Math::toNearestBiggerPowerOfTwo<size_t>(4) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<size_t>(4)) << endl;
	cout << "    Math::toNearestBiggerPowerOfTwo<size_t>(5) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<size_t>(5)) << endl;
	cout << "    Math::toNearestBiggerPowerOfTwo<size_t>(6) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<size_t>(6)) << endl;
	cout << "    Math::toNearestBiggerPowerOfTwo<size_t>(7) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<size_t>(7)) << endl;
	cout << "    Math::toNearestBiggerPowerOfTwo<size_t>(8) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<size_t>(8)) << endl;
	cout << "    Math::toNearestBiggerPowerOfTwo<size_t>(63) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<size_t>(63)) << endl;
	cout << "    Math::toNearestBiggerPowerOfTwo<size_t>(64) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<size_t>(64)) << endl;
	cout << "    Math::toNearestBiggerPowerOfTwo<size_t>(65) => "<<Math::toNearestBiggerPowerOfTwo(static_cast<size_t>(65)) << endl << endl;

	cout << "    Math::cotangent(angleRadians=degreesToRadians(-90)) => "<<Math::cotangent(degreesToRadians(-90)) << endl;
	cout << "    Math::cotangent(angleRadians=degreesToRadians(-65)) => "<<Math::cotangent(degreesToRadians(-65)) << endl;
	cout << "    Math::cotangent(angleRadians=degreesToRadians(-45)) => "<<Math::cotangent(degreesToRadians(-45)) << endl;
	cout << "    Math::cotangent(angleRadians=degreesToRadians(-20)) => "<<Math::cotangent(degreesToRadians(-20)) << endl;
	cout << "    Math::cotangent(angleRadians=degreesToRadians(0)) => "<<Math::cotangent(degreesToRadians(0)) << endl;
	cout << "    Math::cotangent(angleRadians=degreesToRadians(5)) => "<<Math::cotangent(degreesToRadians(5)) << endl;
	cout << "    Math::cotangent(angleRadians=degreesToRadians(15)) => "<<Math::cotangent(degreesToRadians(15)) << endl;
	cout << "    Math::cotangent(angleRadians=degreesToRadians(30)) => "<<Math::cotangent(degreesToRadians(30)) << endl;
	cout << "    Math::cotangent(angleRadians=degreesToRadians(45)) => "<<Math::cotangent(degreesToRadians(45)) << endl;
	cout << "    Math::cotangent(angleRadians=degreesToRadians(75)) => "<<Math::cotangent(degreesToRadians(75)) << endl;
	cout << "    Math::cotangent(angleRadians=degreesToRadians(90)) => "<<Math::cotangent(degreesToRadians(90)) << endl;
	cout << "    Math::cotangent(angleRadians=degreesToRadians(110)) => "<<Math::cotangent(degreesToRadians(110)) << endl;
	cout << "    Math::cotangent(angleRadians=degreesToRadians(180)) => "<<Math::cotangent(degreesToRadians(180)) << endl;
	cout << "    Math::cotangent(angleRadians=degreesToRadians(195)) => "<<Math::cotangent(degreesToRadians(195)) << endl << endl;

	float max = 10;
	cout << "    Math::normalizedValue(-5.5f, "<<max<<") => "<<Math::normalizedValue(-5.0f, max) << endl;
	cout << "    Math::normalizedValue(-1.0f, "<<max<<") => "<<Math::normalizedValue(-1.0f, max) << endl;
	cout << "    Math::normalizedValue(0.0f, "<<max<<") => "<<Math::normalizedValue(0.0f, max) << endl;
	cout << "    Math::normalizedValue(1.0f, "<<max<<") => "<<Math::normalizedValue(1.0f, max) << endl;
	cout << "    Math::normalizedValue(5.0f, "<<max<<") => "<<Math::normalizedValue(5.0f, max) << endl;
	cout << "    Math::normalizedValue(9.5f, "<<max<<") => "<<Math::normalizedValue(9.0f, max) << endl;
	cout << "    Math::normalizedValue(10.0f, "<<max<<") => "<<Math::normalizedValue(10.0f, max) << endl;
	cout << "    Math::normalizedValue(11.5f, "<<max<<") => "<<Math::normalizedValue(11.0f, max) << endl;
	cout << "    Math::normalizedValue(15.0f, "<<max<<") => "<<Math::normalizedValue(15.0f, max) << endl << endl;

	float min = -10;
	cout << "    Math::normalizedValue(-15.0f, "<<min<<", "<<max<<") => "<<Math::normalizedValue(-15.0f, min, max) << endl;
	cout << "    Math::normalizedValue(-11.0f, "<<min<<", "<<max<<") => "<<Math::normalizedValue(-11.0f, min, max) << endl;
	cout << "    Math::normalizedValue(-10.0f, "<<min<<", "<<max<<") => "<<Math::normalizedValue(-10.0f, min, max) << endl;
	cout << "    Math::normalizedValue(-9.5f, "<<min<<", "<<max<<") => "<<Math::normalizedValue(-9.5f, min, max) << endl;
	cout << "    Math::normalizedValue(-5.0f, "<<min<<", "<<max<<") => "<<Math::normalizedValue(-5.0f, min, max) << endl;
	cout << "    Math::normalizedValue(-1.0f, "<<min<<", "<<max<<") => "<<Math::normalizedValue(-1.0f, min, max) << endl;
	cout << "    Math::normalizedValue(0.0f, "<<min<<", "<<max<<") => "<<Math::normalizedValue(0.0f, min, max) << endl;
	cout << "    Math::normalizedValue(1.0f, "<<min<<", "<<max<<") => "<<Math::normalizedValue(1.0f, min, max) << endl;
	cout << "    Math::normalizedValue(5.0f, "<<min<<", "<<max<<") => "<<Math::normalizedValue(5.0f, min, max) << endl;
	cout << "    Math::normalizedValue(9.5f, "<<min<<", "<<max<<") => "<<Math::normalizedValue(9.5f, min, max) << endl;
	cout << "    Math::normalizedValue(10.0f, "<<min<<", "<<max<<") => "<<Math::normalizedValue(10.0f, min, max) << endl;
	cout << "    Math::normalizedValue(11.0f, "<<min<<", "<<max<<") => "<<Math::normalizedValue(11.0f, min, max) << endl;
	cout << "    Math::normalizedValue(15.0f, "<<min<<", "<<max<<") => "<<Math::normalizedValue(15.0f, min, max) << endl << endl;

	cout << "    Math::minValue(static_cast<int>(-5), static_cast<int>(-6)) => "<<Math::minValue(static_cast<int>(-5), static_cast<int>(-6)) << endl;
	cout << "    Math::minValue(static_cast<int>(-6), static_cast<int>(-5)) => "<<Math::minValue(static_cast<int>(-6), static_cast<int>(-5)) << endl;
	cout << "    Math::minValue(static_cast<int>(-1), static_cast<int>(0)) => "<<Math::minValue(static_cast<int>(-1), static_cast<int>(0)) << endl;
	cout << "    Math::minValue(static_cast<int>(0), static_cast<int>(-1)) => "<<Math::minValue(static_cast<int>(0), static_cast<int>(-1)) << endl;
	cout << "    Math::minValue(static_cast<int>(-1), static_cast<int>(1)) => "<<Math::minValue(static_cast<int>(-1), static_cast<int>(1)) << endl;
	cout << "    Math::minValue(static_cast<int>(1), static_cast<int>(-1)) => "<<Math::minValue(static_cast<int>(1), static_cast<int>(-1)) << endl;
	cout << "    Math::minValue(static_cast<int>(999), static_cast<int>(1000)) => "<<Math::minValue(static_cast<int>(999), static_cast<int>(1000)) << endl;
	cout << "    Math::minValue(static_cast<int>(1000), static_cast<int>(999)) => "<<Math::minValue(static_cast<int>(1000), static_cast<int>(999)) << endl << endl;

	cout << "    Math::maxValue(static_cast<int>(-5), static_cast<int>(-6)) => "<<Math::maxValue(static_cast<int>(-5), static_cast<int>(-6)) << endl;
	cout << "    Math::maxValue(static_cast<int>(-6), static_cast<int>(-5)) => "<<Math::maxValue(static_cast<int>(-6), static_cast<int>(-5)) << endl;
	cout << "    Math::maxValue(static_cast<int>(-1), static_cast<int>(0)) => "<<Math::maxValue(static_cast<int>(-1), static_cast<int>(0)) << endl;
	cout << "    Math::maxValue(static_cast<int>(0), static_cast<int>(-1)) => "<<Math::maxValue(static_cast<int>(0), static_cast<int>(-1)) << endl;
	cout << "    Math::maxValue(static_cast<int>(-1), static_cast<int>(1)) => "<<Math::maxValue(static_cast<int>(-1), static_cast<int>(1)) << endl;
	cout << "    Math::maxValue(static_cast<int>(1), static_cast<int>(-1)) => "<<Math::maxValue(static_cast<int>(1), static_cast<int>(-1)) << endl;
	cout << "    Math::maxValue(static_cast<int>(999), static_cast<int>(1000)) => "<<Math::maxValue(static_cast<int>(999), static_cast<int>(1000)) << endl;
	cout << "    Math::maxValue(static_cast<int>(1000), static_cast<int>(999)) => "<<Math::maxValue(static_cast<int>(1000), static_cast<int>(999)) << endl << endl;

	min = 0.0f, max = 1.0f;
	cout << "    Math::clamp(-500.0f, "<<min<<", "<<max<<") => "<<Math::clamp(-500.0f, min, max) << endl;
	cout << "    Math::clamp(-0.1f, "<<min<<", "<<max<<") => "<<Math::clamp(-0.1f, min, max) << endl;
	cout << "    Math::clamp(-0.0001f, "<<min<<", "<<max<<") => "<<Math::clamp(-0.0001f, min, max) << endl;
	cout << "    Math::clamp(0.0f, "<<min<<", "<<max<<") => "<<Math::clamp(0.0f, min, max) << endl;
	cout << "    Math::clamp(0.5f, "<<min<<", "<<max<<") => "<<Math::clamp(0.5f, min, max) << endl;
	cout << "    Math::clamp(1.0f, "<<min<<", "<<max<<") => "<<Math::clamp(1.0f, min, max) << endl;
	cout << "    Math::clamp(1.00001f, "<<min<<", "<<max<<") => "<<Math::clamp(1.00001f, min, max) << endl;
	cout << "    Math::clamp(1.1f, "<<min<<", "<<max<<") => "<<Math::clamp(1.1f, min, max) << endl;
	cout << "    Math::clamp(500.0f, "<<min<<", "<<max<<") => "<<Math::clamp(500.0f, min, max) << endl << endl;

	min = -0.2f; max = -0.1f;
	cout << "    Math::clamp(-500.0f, "<<min<<", "<<max<<") => "<<Math::clamp(-500.0f, min, max) << endl;
	cout << "    Math::clamp(-0.2f, "<<min<<", "<<max<<") => "<<Math::clamp(-0.2f, min, max) << endl;
	cout << "    Math::clamp(-0.15f, "<<min<<", "<<max<<") => "<<Math::clamp(-0.15f, min, max) << endl;
	cout << "    Math::clamp(-0.1f, "<<min<<", "<<max<<") => "<<Math::clamp(-0.1f, min, max) << endl;
	cout << "    Math::clamp(-0.0001f, "<<min<<", "<<max<<") => "<<Math::clamp(-0.0001f, min, max) << endl;
	cout << "    Math::clamp(0.0f, "<<min<<", "<<max<<") => "<<Math::clamp(0.0f, min, max) << endl;
	cout << "    Math::clamp(1.0f, "<<min<<", "<<max<<") => "<<Math::clamp(1.0f, min, max) << endl;
}


inline void testWindow()
{
	Window window(DisplayMode(800, 600), "OpenGL");
	Opengl::printInfo(false);

	Render::clearScreen(1, 0, 0, 1);
	window.swapScreenBuffer();
	SDL_Delay(500);

	window.hide();
	SDL_Delay(250);

	// After showing window contents must be rendered again
	window.show();
	Render::clearScreen(0, 1, 0, 1);
	window.swapScreenBuffer();
	SDL_Delay(1000);

	UString windowTitle = window.getTitle();
	window.setTitle(windowTitle + " testing");
	SDL_Delay(500);
	window.setTitle(windowTitle + " testing 1");
	SDL_Delay(500);
	window.setTitle(windowTitle + " testing 1 2");
	SDL_Delay(500);
	window.setTitle(windowTitle + " testing 1 2 3");
	SDL_Delay(500);

	SwapInterval swapInterval = window.getSwapInterval();
	window.setTitle("Current SwapInterval = "+swapIntervalToString(swapInterval));
	window.setTitle("Set SwapInterval to SwapInterval::Immediate");
	window.setSwapInterval(SwapInterval::Immediate);
	window.setTitle("Set SwapInterval to SwapInterval::Unset");
	window.setSwapInterval(SwapInterval::Unset);
	window.setTitle("Set SwapInterval to SwapInterval::Vsync");
	window.setSwapInterval(SwapInterval::Vsync);
	window.setTitle("Set SwapInterval to SwapInterval::VsyncLateSwapTearing");
	window.setSwapInterval(SwapInterval::VsyncLateSwapTearing);

	Vector2i windowPos = window.getPosition();
	UString newWindowTitle("Current window position = ");
	newWindowTitle += windowPos;
	window.setTitle(newWindowTitle);
	Render::clearScreen(0, 0, 1, 1);
	window.swapScreenBuffer();
	SDL_Delay(2000);

	newWindowTitle = "Moving window to (0, 0)";
	window.setTitle(newWindowTitle);
	window.setPosition(Vector2i(0, 0));
	Render::clearScreen(1, 0, 0, 1);
	window.swapScreenBuffer();
	SDL_Delay(2000);

	window.setTitle("Moving window to centered");
	Render::clearScreen(0, 1, 0, 1);
	window.swapScreenBuffer();
	window.setPositionCentered();
	SDL_Delay(2000);

	vector<Display> displays = Display::getDisplays();
	for (size_t i=0; i<displays.size(); ++i) {
		Align align;

		align.set(HAlign::Left, VAlign::Top);
		newWindowTitle = "Moving window to aligned on display ";
		newWindowTitle += displays[i].toStringDebug()+" ";
		newWindowTitle += align.toStringDebug();
		window.setTitle(newWindowTitle);
		window.setPositionAlignedOnDisplay(displays[i], align);
		Render::clearScreen(0, 0, 1, 1);
		window.swapScreenBuffer();
		SDL_Delay(2000);

		align.set(HAlign::Center, VAlign::Top);
		newWindowTitle = "Moving window to aligned on display ";
		newWindowTitle += displays[i].toStringDebug()+" ";
		newWindowTitle += align.toStringDebug();
		window.setTitle(newWindowTitle);
		window.setPositionAlignedOnDisplay(displays[i], align);
		Render::clearScreen(1, 0, 0, 1);
		window.swapScreenBuffer();
		SDL_Delay(2000);

		align.set(HAlign::Right, VAlign::Top);
		newWindowTitle = "Moving window to aligned on display ";
		newWindowTitle += displays[i].toStringDebug()+" ";
		newWindowTitle += align.toStringDebug();
		window.setTitle(newWindowTitle);
		window.setPositionAlignedOnDisplay(displays[i], align);
		Render::clearScreen(0, 1, 0, 1);
		window.swapScreenBuffer();
		SDL_Delay(2000);

		align.set(HAlign::Left, VAlign::Center);
		newWindowTitle = "Moving window to aligned on display ";
		newWindowTitle += displays[i].toStringDebug()+" ";
		newWindowTitle += align.toStringDebug();
		window.setTitle(newWindowTitle);
		window.setPositionAlignedOnDisplay(displays[i], align);
		Render::clearScreen(0, 0, 1, 1);
		window.swapScreenBuffer();
		SDL_Delay(2000);

		align.set(HAlign::Center, VAlign::Center);
		newWindowTitle = "Moving window to aligned on display ";
		newWindowTitle += displays[i].toStringDebug()+" ";
		newWindowTitle += align.toStringDebug();
		window.setTitle(newWindowTitle);
		window.setPositionAlignedOnDisplay(displays[i], align);
		Render::clearScreen(1, 0, 0, 1);
		window.swapScreenBuffer();
		SDL_Delay(2000);

		align.set(HAlign::Right, VAlign::Center);
		newWindowTitle = "Moving window to aligned on display ";
		newWindowTitle += displays[i].toStringDebug()+" ";
		newWindowTitle += align.toStringDebug();
		window.setTitle(newWindowTitle);
		window.setPositionAlignedOnDisplay(displays[i], align);
		Render::clearScreen(0, 1, 0, 1);
		window.swapScreenBuffer();
		SDL_Delay(2000);

		align.set(HAlign::Left, VAlign::Bottom);
		newWindowTitle = "Moving window to aligned on display ";
		newWindowTitle += displays[i].toStringDebug()+" ";
		newWindowTitle += align.toStringDebug();
		window.setTitle(newWindowTitle);
		window.setPositionAlignedOnDisplay(displays[i], align);
		Render::clearScreen(0, 0, 1, 1);
		window.swapScreenBuffer();
		SDL_Delay(2000);

		align.set(HAlign::Center, VAlign::Bottom);
		newWindowTitle = "Moving window to aligned on display ";
		newWindowTitle += displays[i].toStringDebug()+" ";
		newWindowTitle += align.toStringDebug();
		window.setTitle(newWindowTitle);
		window.setPositionAlignedOnDisplay(displays[i], align);
		Render::clearScreen(1, 0, 0, 1);
		window.swapScreenBuffer();
		SDL_Delay(2000);

		align.set(HAlign::Right, VAlign::Bottom);
		newWindowTitle = "Moving window to aligned on display ";
		newWindowTitle += displays[i].toStringDebug()+" ";
		newWindowTitle += align.toStringDebug();
		window.setTitle(newWindowTitle);
		window.setPositionAlignedOnDisplay(displays[i], align);
		Render::clearScreen(0, 1, 0, 1);
		window.swapScreenBuffer();
		SDL_Delay(2000);
	}

	//window.loadIcon(const UString& iconFileName);
}


inline void testWindow_2()
{
	ContextSettings settings = ContextSettings();
	Window window1(DisplayMode(800, 600), "OpenGL", WindowStyle::Resizable | WindowStyle::Hidden, settings);

	vector<Display> displays = Display::getDisplays();
	cout << "Found "<<displays.size()<<" displays!" << endl;
	Opengl::printInfo(false);

	window1.setPositionAlignedOnDisplay(displays[0], Align(HAlign::Left, VAlign::Top));
	Window window2(DisplayMode(320, 200), "OpenGL", WindowStyle::Default | WindowStyle::Hidden, settings);
	window2.setPositionAlignedOnDisplay(displays[0], Align(HAlign::Right, VAlign::Top));
	window1.show();
	window2.show();

	// Render
	window1.makeActive();
	Render::clearScreen(0, 1, 0, 1);
	window1.swapScreenBuffer();

	window2.makeActive();
	Render::clearScreen(0, 0, 1, 1);
	window2.swapScreenBuffer();
	SDL_Delay(1000);

	window1.makeActive();
	Render::clearScreen(0, 0, 1, 1);
	window1.swapScreenBuffer();

	window2.makeActive();
	Render::clearScreen(0, 1, 0, 1);
	window2.swapScreenBuffer();
	SDL_Delay(1500);
}


inline void testShaderAttribute()
{
	ShaderAttribute attr0(0);
	cout << "    ShaderAttribute attr0(0) << "<<attr0 << endl;
	cout << "    ShaderAttribute attr0(0).toString() = "<<attr0.toString() << endl;
	cout << "    ShaderAttribute attr0(0).toStringDebug() = << "<<attr0.toStringDebug() << endl;
	cout << "    ShaderAttribute attr0(0).getIndex() = "<<attr0.getIndex() << endl << endl;

	ShaderAttribute attr0_2(ShaderAttribute::PositionIndex);
	cout << "    ShaderAttribute attr0_2(ShaderAttribute::PositionIndex) << "<<attr0_2 << endl;
	cout << "    ShaderAttribute attr0_2(ShaderAttribute::PositionIndex).toString() = "<<attr0_2.toString() << endl;
	cout << "    ShaderAttribute attr0_2(ShaderAttribute::PositionIndex).toStringDebug() = << "<<attr0_2.toStringDebug() << endl;
	cout << "    ShaderAttribute attr0_2(ShaderAttribute::PositionIndex).getIndex() = "<<attr0_2.getIndex() << endl << endl;

	ShaderAttribute attr1(ShaderAttribute::UvIndex);
	cout << "    ShaderAttribute attr1(ShaderAttribute::UvIndex) << "<<attr1 << endl;
	cout << "    ShaderAttribute attr1(ShaderAttribute::UvIndex).toString() = "<<attr1.toString() << endl;
	cout << "    ShaderAttribute attr1(ShaderAttribute::UvIndex).toStringDebug() = << "<<attr1.toStringDebug() << endl;
	cout << "    ShaderAttribute attr1(ShaderAttribute::UvIndex).getIndex() = "<<attr1.getIndex() << endl << endl;

	ShaderAttribute attr2(ShaderAttribute::NormalIndex);
	cout << "    ShaderAttribute attr2(ShaderAttribute::NormalIndex) << "<<attr2 << endl;
	cout << "    ShaderAttribute attr2(ShaderAttribute::NormalIndex).toString() = "<<attr2.toString() << endl;
	cout << "    ShaderAttribute attr2(ShaderAttribute::NormalIndex).toStringDebug() = << "<<attr2.toStringDebug() << endl;
	cout << "    ShaderAttribute attr2(ShaderAttribute::NormalIndex).getIndex() = "<<attr2.getIndex() << endl << endl;

	ShaderAttribute attr3(ShaderAttribute::ColorIndex);
	cout << "    ShaderAttribute attr3(ShaderAttribute::ColorIndex) << "<<attr3 << endl;
	cout << "    ShaderAttribute attr3(ShaderAttribute::ColorIndex).toString() = "<<attr3.toString() << endl;
	cout << "    ShaderAttribute attr3(ShaderAttribute::ColorIndex).toStringDebug() = << "<<attr3.toStringDebug() << endl;
	cout << "    ShaderAttribute attr3(ShaderAttribute::ColorIndex).getIndex() = "<<attr3.getIndex() << endl << endl;

	try {
		ShaderAttribute attr4(ShaderAttribute::LastDefaultIndex+1);
		cout << "    ShaderAttribute attr4(ShaderAttribute::LastDefaultIndex+1): ERROR DID NOT THROW RUNTIME_ERROR EXCEPTION!" << endl;
		cout << "    ShaderAttribute attr4(ShaderAttribute::LastDefaultIndex+1).toStringDebug() = << "<<attr4.toStringDebug() << endl << endl;
	} catch (runtime_error& e) {
		(void)e;
		cout << "    ShaderAttribute attr4(ShaderAttribute::LastDefaultIndex+1): Threw runtime_error() as it should." << endl << endl;
	}

	try {
		ShaderAttribute attr5(ShaderAttribute::LastDefaultIndex+5);
		cout << "    ShaderAttribute attr5(ShaderAttribute::LastDefaultIndex+5): ERROR DID NOT THROW RUNTIME_ERROR EXCEPTION!" << endl;
		cout << "    ShaderAttribute attr5(ShaderAttribute::LastDefaultIndex+5).toStringDebug() = << "<<attr5.toStringDebug() << endl << endl;
	} catch (runtime_error& e) {
		(void)e;
		cout << "    ShaderAttribute attr5(ShaderAttribute::LastDefaultIndex+5): Threw runtime_error() as it should." << endl << endl;
	}

	cout << "    ShaderAttribute::Position().toStringDebug() = << "<<ShaderAttribute::Position.toStringDebug() << endl;
	cout << "    ShaderAttribute::Uv().toStringDebug() = << "<<ShaderAttribute::Uv.toStringDebug() << endl;
	cout << "    ShaderAttribute::Normal().toStringDebug() = << "<<ShaderAttribute::Normal.toStringDebug() << endl;
	cout << "    ShaderAttribute::Color().toStringDebug() = << "<<ShaderAttribute::Color.toStringDebug() << endl << endl;

	ShaderAttribute attr6(4, "foobar");
	cout << "    ShaderAttribute attr6(4, \"foobar\") << "<<attr6 << endl;
	cout << "    ShaderAttribute attr6(4, \"foobar\").toString() = "<<attr6.toString() << endl;
	cout << "    ShaderAttribute attr6(4, \"foobar\").toStringDebug() = << "<<attr6.toStringDebug() << endl;
	cout << "    ShaderAttribute attr6(4, \"foobar\").getIndex() = "<<attr6.getIndex() << endl << endl;

	vector<ShaderAttribute> shaderAttrs = ShaderAttribute::getDefaultAttributes();
	cout << "    ShaderAttribute::getDefaultAttributes() = [ ";
	for (auto& attr : shaderAttrs) {
		cout << attr.toStringDebug()<<", ";
	}
	cout << " ]" << endl << endl;
}


inline void testTimestamp()
{
	cout << "Timestamp" << endl;
	Timestamp t1 = Timestamp::microseconds(30000);
	cout << "  t1 = "<<t1.toStringDebug() << endl;
	Timestamp t2 = Timestamp::milliseconds(20);
	cout << "  t2 = "<<t2.toStringDebug() << endl;
	Timestamp t3 = Timestamp::seconds(0.01f);
	cout << "  t3 = "<<t3.toStringDebug() << endl << endl;

	Timestamp now = Timestamp::getCurrentTime();
	cout << "  now = "<<now.toStringDebug() << endl;
	int64_t microSec = now.asMicroseconds();
	cout << "    now.asMicroseconds() = "<<microSec << endl;
	int32_t milliSec = now.asMilliseconds();
	cout << "    now.asMilliseconds() = "<<milliSec << endl;
	float sec  = now.asSeconds();
	cout << "    now.asSeconds() = "<<sec << endl << endl;

	Timestamp t4 = t1 * static_cast<int64_t>(2);
	cout << "    t4 = t1 * static_cast<int64_t>(2) = "<<t4 << endl;
	Timestamp t5 = t1 + t4;
	cout << "    t5 = t1 + t4 = "<<t5 << endl;
	Timestamp t6 = -t5;
	cout << "    t6 = -t5 = "<<t6 << endl;
	bool b1 = (t1 == t4);
	cout << "    b1 = (t1 == t4) = "<<b1 << endl;
	bool b2 = (t5 > t6);
	cout << "    b2 = (t5 > t6) = "<<b2 << endl << endl;
}


inline void testTimer()
{
	cout << "Timer" << endl;
	Timer timer1;
	Time elapsed1 = timer1.getElapsedTime();
	cout << "  elapsed1 << "<<elapsed1 << endl;
	cout << "    toString()="<<elapsed1.toString() << endl;
	cout << "    toStringDebug()="<<elapsed1.toStringDebug() << endl;
	cout << "    asMicroseconds()="<<elapsed1.asMicroseconds() << endl;
	cout << "    asMilliseconds()="<<elapsed1.asMilliseconds() << endl;
	cout << "    asSeconds()="<<elapsed1.asSeconds() << endl << endl;
	timer1.restart();
}


inline void testAll()
{
	testRect();
	testSize();
	testBlendMode();
	testSwapInterval();
	testWindowStyle();
	testDisplayMode();
	testDisplay();
	testAllDisplays();
	testMultisampleAntialiasLevel();
	testContexts();
	testDegreeRadian();
	testAlign();
	testVector2();
	testRgbaColorf();
	testMath();
	testWindow();
	testWindow_2();
	testShaderAttribute();
	testTimestamp();
	testTimer();
}
*/


// TODO: a lot of stuff
/*
//GrinderKit app(argc, argv, "GrinderKit", "0.1");

// TODO: inline void testFile() // TODO: check from depthIo that's if not already tested!
//cout << "    foo("<<param<<") => "<<foo(param) << endl;

//ContextSettings settings = ContextSettings(3,0,MultisampleAntialiasLevel::Msaa2x); // TODO: why does not work?
//ContextSettings settings = ContextSettings(3,0,MultisampleAntialiasLevel::MsaaOff, 32, 8); // TODO: why actual depth & stencil bits wont show?
//ContextSettings settings = ContextSettings(2,1); // TODO: incompatible probably with context flags

//testAll();

// Defaults for OpenGL rendering?
defaultTopLeftSquareVao(false, "AppWindowOpengl::defaultTopLeftSquareVao"),
defaultCenteredSquareVao(false, "AppWindowOpengl::defaultCenteredSquareVao"),
defaultShaderProgram()
//cameras()

glClearColor(1, 0, 0, 0);
GL_CHECK_FOR_ERRORS("", "");

// Create default square VAOs
Shapes::generateRectangle(defaultTopLeftSquareVao, Rect<float>(0.0f, 0.0f, 1.0f, 1.0f));
Shapes::generateRectangle(defaultCenteredSquareVao, Rect<float>(-0.5f, -0.5f, 1.0f, 1.0f));

// Create default shader program
if (defaultShaderProgram.isCreated() == false)
	defaultShaderProgram.create();
defaultShaderProgram.loadAndLinkDefaultShader();
*/
