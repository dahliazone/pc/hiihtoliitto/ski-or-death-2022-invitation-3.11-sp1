
//#include <Verso/Base.hpp>
//#include <Verso/Gfx.hpp>
#include <Verso/Display/Display.hpp>
#include <Verso/Display/DisplayMode.hpp>
#include <Verso/System/UString.hpp>

#include <Verso/Math/Quaternion.hpp>
#include <Verso/System/Logger.hpp>
//#include <Verso/System/main.hpp>
//#include <iostream>
//#include <cstdlib>


using namespace std;
using namespace Verso;

int main(int argc, char* argv[])
{
	(void)argc; (void)argv;
	VERSO_LOG_INFO("verso-3d/examples", "Hello verso-3d example!");
	VERSO_LOG_INFO_VARIABLE("verso-3d/examples", "Quaternion::identity", Quaternion::Identity);

	Display display;
	VERSO_LOG_INFO_VARIABLE("verso-3d/examples", "display.toString()", display.toString());

	DisplayMode displayMode;
	VERSO_LOG_INFO_VARIABLE("verso-3d/examples", "displayMode", displayMode);

	return EXIT_SUCCESS;
}

