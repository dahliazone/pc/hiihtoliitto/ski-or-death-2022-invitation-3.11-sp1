#!/bin/bash

echo "verso-3d: Fetching externals from repositories..."

#if [ ! -d ../glslang ]; then
#        git clone https://gitlab.com/dahliazone/pc/extlib/glslang.git ../glslang
#else
#        pushd ../glslang
#        git pull --rebase
#        popd
#fi

if [ ! -d ../glad ]; then
        git clone https://gitlab.com/dahliazone/pc/extlib/glad.git ../glad
else
        pushd ../glad
        git pull --rebase
        popd
fi

if [ ! -d ../zlib ]; then
        git clone https://gitlab.com/dahliazone/pc/extlib/zlib.git ../zlib
else
        pushd ../zlib
        git pull --rebase
        popd
fi

if [ ! -d ../ocornut-imgui ]; then
        git clone https://gitlab.com/dahliazone/pc/extlib/ocornut-imgui.git ../ocornut-imgui
else
        pushd ../ocornut-imgui
        git pull --rebase
        popd
fi

#if [ ! -d ../bullet3 ]; then
#        git clone https://gitlab.com/dahliazone/pc/extlib/bullet3.git ../bullet3
#else
#        pushd ../bullet3
#        git pull --rebase
#        popd
#fi

#if [ ! -d ../assimp ]; then
#        git clone https://gitlab.com/dahliazone/pc/extlib/assimp.git ../assimp
#else
#        pushd ../assimp
#        git pull --rebase
#        popd
#fi

if [ ! -d ../tinygltf ]; then
        git clone https://gitlab.com/dahliazone/pc/extlib/tinygltf.git ../tinygltf
else
        pushd ../tinygltf
        git pull --rebase
        popd
fi

if [ ! -d ../cgltf ]; then
        git clone https://gitlab.com/dahliazone/pc/extlib/cgltf.git ../cgltf
else
        pushd ../cgltf
        git pull --rebase
        popd
fi

if [ ! -d ../tinyobjloader ]; then
	git clone https://gitlab.com/dahliazone/pc/extlib/tinyobjloader.git ../tinyobjloader
else
	pushd ../tinyobjloader
	git pull --rebase
	popd
fi

if [ ! -d ../duktape ]; then
	git clone https://gitlab.com/dahliazone/pc/extlib/duktape.git ../duktape
else
	pushd ../duktape
	git pull --rebase
	popd
fi

if [ ! -d ../verso-gfx ]; then
        git clone https://gitlab.com/dahliazone/pc/lib/verso-gfx.git ../verso-gfx
else
        pushd ../verso-gfx
        git pull --rebase
        popd
fi

echo "verso-gfx: Resursing into..."
pushd ../verso-gfx
./fetch_parent_externals.sh
popd

echo "verso-3d: All done"

