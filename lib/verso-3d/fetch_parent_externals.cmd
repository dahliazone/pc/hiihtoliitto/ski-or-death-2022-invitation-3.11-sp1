PATH=%PATH%;"C:\Program Files\SlikSvn\bin";"C:\Program Files (x86)\SlikSvn\bin";

echo "verso-3d: Fetching externals from repositories..."

REM IF NOT EXIST ..\glslang (
REM	git clone https://gitlab.com/dahliazone/pc/extlib/glslang.git ..\glslang
REM ) else (
REM	pushd ..\glslang
REM	git pull --rebase
REM	popd
REM )

IF NOT EXIST ..\glad (
	git clone https://gitlab.com/dahliazone/pc/extlib/glad.git ..\glad
) else (
	pushd ..\glad
	git pull --rebase
	popd
)

IF NOT EXIST ..\zlib (
	git clone https://gitlab.com/dahliazone/pc/extlib/zlib.git ..\zlib
) else (
	pushd ..\zlib
	git pull --rebase
	popd
)

IF NOT EXIST ..\ocornut-imgui (
	git clone https://gitlab.com/dahliazone/pc/extlib/ocornut-imgui.git ..\ocornut-imgui
) else (
	pushd ..\ocornut-imgui
	git pull --rebase
	popd
)

REM IF NOT EXIST ..\bullet3 (
REM	git clone https://gitlab.com/dahliazone/pc/extlib/bullet3.git ..\bullet3
REM ) else (
REM	pushd ..\bullet3
REM	git pull --rebase
REM	popd
REM )

REM IF NOT EXIST ..\assimp (
REM	git clone https://gitlab.com/dahliazone/pc/extlib/assimp.git ..\assimp
REM ) else (
REM	pushd ..\assimp
REM	git pull --rebase
REM	popd
REM )

IF NOT EXIST ..\tinygltf (
	git clone https://gitlab.com/dahliazone/pc/extlib/tinygltf.git ..\tinygltf
) else (
	pushd ..\tinygltf
	git pull --rebase
	popd
)

IF NOT EXIST ..\cgltf (
	git clone https://gitlab.com/dahliazone/pc/extlib/cgltf.git ..\cgltf
) else (
	pushd ..\cgltf
	git pull --rebase
	popd
)

IF NOT EXIST ..\tinyobjloader (
	git clone https://gitlab.com/dahliazone/pc/extlib/tinyobjloader.git ..\tinyobjloader
) else (
	pushd ..\tinyobjloader
	git pull --rebase
	popd
)

IF NOT EXIST ..\duktape (
	git clone https://gitlab.com/dahliazone/pc/extlib/duktape.git ..\duktape
) else (
	pushd ..\duktape
	git pull --rebase
	popd
)

IF NOT EXIST ..\verso-gfx (
	git clone https://gitlab.com/dahliazone/pc/lib/verso-gfx.git ..\verso-gfx
) else (
	pushd ..\verso-gfx
	git pull --rebase
	popd
)

echo "verso-gfx: Resursing into..."
pushd ..\verso-gfx
call fetch_parent_externals.cmd
popd

echo "verso-3d: All done"
