#ifndef VERSO_3D_SYSTEM_ERRORHANDLERWINDOWOPENGL_HPP
#define VERSO_3D_SYSTEM_ERRORHANDLERWINDOWOPENGL_HPP

#include <Verso/verso-3d-common.hpp>
#include <Verso/System/UString.hpp>

namespace Verso {


extern VERSO_3D_API void setTerminateHandlerWindowOpengl(const UString& title);
[[ noreturn ]] extern VERSO_3D_API void runTerminateHandlerWindowOpengl(const UString& title);


} // End namespace Verso

#endif // End header guard

