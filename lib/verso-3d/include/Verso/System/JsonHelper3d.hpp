#ifndef VERSO_3D_SYSTEM_JSONHELPER3D_HPP
#define VERSO_3D_SYSTEM_JSONHELPER3D_HPP

#include <Verso/System/JsonHelper.hpp>
#include <Verso/Render/Opengl/BlendMode.hpp>
#include <Verso/Render/ClearFlag.hpp>
#include <Verso/Render/Camera/CameraType.hpp>
#include <Verso/Render/Texture/MinFilter.hpp>
#include <Verso/Render/Texture/MagFilter.hpp>
#include <Verso/GrinderKit/PlayMode.hpp>
#include <Verso/GrinderKit/DebugMode.hpp>
#include <Verso/Render/Particle/Sequencers/BurstInterval.hpp>
#include <Verso/Render/Sprite/AnimationType.hpp>

#define IMGUI_DISABLE_OBSOLETE_FUNCTIONS
#include <imgui.h>

namespace Verso {


namespace JsonHelper {


VERSO_3D_API BlendMode readBlendMode(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const BlendMode& defaultValue = BlendMode::None);
VERSO_3D_API ClearFlag readClearFlag(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const ClearFlag& defaultValue = ClearFlag::SolidColor);
VERSO_3D_API TexturePixelFormat readTexturePixelFormat(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const TexturePixelFormat& defaultValue = TexturePixelFormat::Unset);
VERSO_3D_API MinFilter readMinFilter(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const MinFilter& defaultValue = MinFilter::Linear);
VERSO_3D_API MagFilter readMagFilter(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const MagFilter& defaultValue = MagFilter::Nearest);
VERSO_3D_API WrapStyle readWrapStyle(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const WrapStyle& defaultValue = WrapStyle::Default);
VERSO_3D_API PlayMode readPlayMode(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const PlayMode& defaultValue = PlayMode::Production);
VERSO_3D_API DebugMode readDebugMode(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const DebugMode& defaultValue = DebugMode::Editor);
VERSO_3D_API SequenceType readSequenceType(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const SequenceType& defaultValue = SequenceType::None);
VERSO_3D_API AnimationType readAnimationType(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const AnimationType& defaultValue = AnimationType::Loop);
VERSO_3D_API CameraType readCameraType(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const CameraType& defaultValue = CameraType::Unset);
VERSO_3D_API ProjectionType readProjectionType(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const ProjectionType& defaultValue = ProjectionType::Unset);
VERSO_3D_API ImVec4 readImVec4(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const ImVec4& defaultValue = ImVec4(0, 0, 0, 0));


} // End namespace JsonHelper


} // End namespace Verso

#endif // End header guard

