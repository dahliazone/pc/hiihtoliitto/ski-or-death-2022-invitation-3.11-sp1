#ifndef VERSO_GFX_SYSTEM_IPROGRAMOPENGL_HPP
#define VERSO_GFX_SYSTEM_IPROGRAMOPENGL_HPP

#include <Verso/Time/FrameTimestamp.hpp>
#include <Verso/Display/IWindowOpengl.hpp>
#include <Verso/Input/Event.hpp>
#include <Verso/Audio/Audio2d.hpp>

namespace Verso {


class VERSO_3D_API IProgramOpengl
{
public:
	virtual ~IProgramOpengl() = 0;

//protected:
//	IProgramOpengl(const IProgramOpengl&) = default;

//	IProgramOpengl& operator=(const IProgramOpengl&) = default;

public: // interface IProgramOpengl
	virtual void create(IWindowOpengl& window, Audio2d& audio2d) = 0;

	virtual void reset(IWindowOpengl& window) = 0;

	virtual void destroy() VERSO_NOEXCEPT = 0;

	virtual bool isCreated() const = 0;

	virtual void handleInput(IWindowOpengl& window, const FrameTimestamp& time) = 0;

	virtual void handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event) = 0;

	virtual void render(IWindowOpengl& window, const FrameTimestamp& time) = 0;

	virtual bool isQuitting() const = 0;

	virtual void quit() = 0;
};


} // End namespace Verso

#endif // End header guard

