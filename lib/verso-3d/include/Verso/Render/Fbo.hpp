#ifndef VERSO_3D_RENDER_FBO_HPP
#define VERSO_3D_RENDER_FBO_HPP

#include <Verso/Render/Opengl.hpp>
#include <Verso/Render/Texture.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/Math/Rect.hpp>

namespace Verso {


class IWindowOpengl;


// FBO = Framebuffer Object
class Fbo
{
private: // static
	static GLuint lastBoundReadHandle;
	static GLuint lastBoundDrawHandle;
	static const int MaxColorAttachments;

private:
	bool created;
	UString uniqueId;
	IWindowOpengl* window;
	GLuint handle;
	Rectf relativeViewport;
	std::vector<Texture*> colorsRead;
	std::vector<Texture*> colorsDraw;
	Texture* depthRead;
	Texture* depthDraw;
	Texture* stencilRead;
	Texture* stencilDraw;

public:
	VERSO_3D_API explicit Fbo(const UString& uniqueId);

	Fbo(const Fbo& original) = delete;

	VERSO_3D_API Fbo(Fbo&& original);

	Fbo& operator =(const Fbo& original) = delete;

	VERSO_3D_API Fbo& operator =(Fbo&& original);

	VERSO_3D_API ~Fbo();

public:
	VERSO_3D_API void create(IWindowOpengl* window);

	VERSO_3D_API void destroy() VERSO_NOEXCEPT;

	VERSO_3D_API bool isCreated() const;

	VERSO_3D_API void createColorTexture(const TextureParameters& textureParameters, size_t colorTextureIndex = 0, int mipmapLevel = 0);

	VERSO_3D_API void createColorReadTexture(const TextureParameters& textureParameters, size_t colorTextureIndex = 0, int mipmapLevel = 0);

	VERSO_3D_API void createColorDrawTexture(const TextureParameters& textureParameters, size_t colorTextureIndex = 0, int mipmapLevel = 0);

	VERSO_3D_API void createDepthTexture(const TextureParameters& textureParameters, int mipmapLevel = 0);

	VERSO_3D_API void createDepthReadTexture(const TextureParameters& textureParameters, int mipmapLevel = 0);

	VERSO_3D_API void createDepthDrawTexture(const TextureParameters& textureParameters, int mipmapLevel = 0);

	VERSO_3D_API void createStencilTexture(const TextureParameters& textureParameters, int mipmapLevel = 0);

	VERSO_3D_API void createStencilReadTexture(const TextureParameters& textureParameters, int mipmapLevel = 0);

	VERSO_3D_API void createStencilDrawTexture(const TextureParameters& textureParameters, int mipmapLevel = 0);

	VERSO_3D_API void destroyColorTexture(size_t colorTextureIndex = 0, int mipmapLevel = 0);

	VERSO_3D_API void destroyColorReadTexture(size_t colorTextureIndex = 0, int mipmapLevel = 0);

	VERSO_3D_API void destroyColorDrawTexture(size_t colorTextureIndex = 0, int mipmapLevel = 0);

	VERSO_3D_API void destroyDepthTexture(int mipmapLevel = 0);

	VERSO_3D_API void destroyDepthReadTexture(int mipmapLevel = 0);

	VERSO_3D_API void destroyDepthDrawTexture(int mipmapLevel = 0);

	VERSO_3D_API void destroyStencilTexture(int mipmapLevel = 0);

	VERSO_3D_API void destroyStencilReadTexture(int mipmapLevel = 0);

	VERSO_3D_API void destroyStencilDrawTexture(int mipmapLevel = 0);

	VERSO_3D_API bool isComplete();

	VERSO_3D_API Texture* getColorReadTexture(size_t colorTextureIndex = 0, int mipmapLevel = 0);

	VERSO_3D_API Texture* getColorDrawTexture(size_t colorTextureIndex = 0, int mipmapLevel = 0);

	VERSO_3D_API Texture* getDepthReadTexture(int mipmapLevel = 0);

	VERSO_3D_API Texture* getDepthDrawTexture(int mipmapLevel = 0);

	VERSO_3D_API Texture* getStencilReadTexture(int mipmapLevel = 0);

	VERSO_3D_API Texture* getStencilDrawTexture(int mipmapLevel = 0);

	VERSO_3D_API void bind();

	VERSO_3D_API void bindRead();

	VERSO_3D_API void bindDraw();

	VERSO_3D_API void unbind();

	VERSO_3D_API void unbindRead();

	VERSO_3D_API void unbindDraw();

	VERSO_3D_API Rectf getRelativeViewport() const;

	VERSO_3D_API void setRelativeViewport(const Rectf& relativeViewport);

	VERSO_3D_API void resetRelativeViewport();

	VERSO_3D_API void applyViewport() const;

	VERSO_3D_API UString getUniqueId() const;

	VERSO_3D_API void onResize();

public: // toString
	VERSO_3D_API UString toString(const UString& newLinePadding) const;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const Fbo& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

