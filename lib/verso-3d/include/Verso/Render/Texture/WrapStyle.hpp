#ifndef VERSO_3D_RENDER_TEXTURE_WRAPSTYLE_HPP
#define VERSO_3D_RENDER_TEXTURE_WRAPSTYLE_HPP

#include <Verso/Render/Opengl.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>

namespace Verso {


enum class WrapStyle : GLenum
{
	Unset = 0,
	Repeat = GL_REPEAT, // Texture coordinate is repeated.
	MirroredRepeat = GL_MIRRORED_REPEAT, // Texture coordinate is mirrored over 0 and 1.
	ClampToEdge = GL_CLAMP_TO_EDGE, // Texture coordinate is clamped to range [0,1].
	ClampToBorder = GL_CLAMP_TO_BORDER, // Same as ClampToEdge but at 0 and 1 fixed border color is used.
	//MirroredClampToEdge = GL_MIRROR_CLAMP_TO_EDGE, // MirroredRepeat for one repetition and then ClampToEdge. OpenGL 4.4+ only
	Default = GL_REPEAT,
};


inline UString wrapStyleToString(const WrapStyle& wrapStyle)
{
	switch (wrapStyle)
	{
	case WrapStyle::Unset:
		return "Unset";
	case WrapStyle::Repeat:
		return "Repeat";
	case WrapStyle::MirroredRepeat:
		return "MirroredRepeat";
	case WrapStyle::ClampToEdge:
		return "ClampToEdge";
	case WrapStyle::ClampToBorder:
		return "ClampToBorder";
	//case WrapStyle::MirroredClampToEdge: // OpenGL 4.4+ only
	//	return "MirroredClampToEdge";
	default:
		return "Unknown value";
	}
}


// returns WrapStyle::Unset for any erroneuos string
inline WrapStyle stringToWrapStyle(const UString& str)
{
	if (str.equals("Repeat")) {
		return WrapStyle::Repeat;
	}
	else if (str.equals("MirroredRepeat")) {
		return WrapStyle::MirroredRepeat;
	}
	else if (str.equals("ClampToEdge")) {
		return WrapStyle::ClampToEdge;
	}
	else if (str.equals("ClampToBorder")) {
		return WrapStyle::ClampToBorder;
	}
	//else if (str.equals("MirroredClampToEdge")) {
	//	return WrapStyle::MirroredClampToEdge;
	//}
	else {
		return WrapStyle::Unset;
	}
}


inline GLenum wrapStyleToGlenum(const WrapStyle& wrapStyle)
{
	if (wrapStyle != WrapStyle::Unset) {
		return static_cast<GLenum>(wrapStyle);
	}
	else {
		UString error("Cannot convert WrapStyle::Unset(");
		error.append2(static_cast<GLenum>(wrapStyle));
		error += ") to GLenum";
		VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), "");
	}
}


} // End namespace Verso

#endif // End header guard

