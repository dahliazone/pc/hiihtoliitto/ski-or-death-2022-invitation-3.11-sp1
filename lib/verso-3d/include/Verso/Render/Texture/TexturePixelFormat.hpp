#ifndef VERSO_3D_RENDER_TEXTURE_TEXTUREPIXELFORMAT_HPP
#define VERSO_3D_RENDER_TEXTURE_TEXTUREPIXELFORMAT_HPP

#include <Verso/Render/Opengl.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>

namespace Verso {


//TexturePixelFormat format
// The default is THREE.RGBAFormat for the texture. Other formats are: THREE.AlphaFormat, THREE.RGBFormat,
// THREE.LuminanceFormat, and THREE.LuminanceAlphaFormat. There are also compressed texture formats, if the
// S3TC extension is supported: THREE.RGB_S3TC_DXT1_Format, THREE.RGBA_S3TC_DXT1_Format, THREE.RGBA_S3TC_DXT3_Format,
// and THREE.RGBA_S3TC_DXT5_Format.


enum class TexturePixelFormat : GLenum
{
	Unset = 0,
	Red = GL_RED, // Each element is a single red component. The GL converts it to floating point and assembles it into an RGBA element by attaching 0 for green and blue, and 1 for alpha. Each component is clamped to the range [0,1].
	Rg = GL_RG, // Each element is a red/green double. The GL converts it to floating point and assembles it into an RGBA element by attaching 0 for blue, and 1 for alpha. Each component is clamped to the range [0,1].
	Rgb = GL_RGB, // Each element is an RGB triple. The GL converts it to floating point and assembles it into an RGBA element by attaching 1 for alpha. Each component is clamped to the range [0,1].
	Bgr = GL_BGR, // Each element is an BGR triple. The GL converts it to floating point and assembles it into an RGBA element by attaching 1 for alpha. Each component is clamped to the range [0,1].
	Rgba = GL_RGBA, // Each element contains all four components. Each component is clamped to the range [0,1].
	Bgra = GL_BGRA, // Each element contains all four components. Each component is clamped to the range [0,1].
	RedInteger = GL_RED_INTEGER, //
	RgInteger = GL_RG_INTEGER, //
	RgbInteger = GL_RGB_INTEGER, //
	BgrInteger = GL_BGR_INTEGER, //
	RgbaInteger = GL_RGBA_INTEGER, //
	BgraInteger = GL_BGRA_INTEGER, //
	//StencilIndex = GL_STENCIL_INDEX, // OpenGL 4.4+ only
	DepthComponent = GL_DEPTH_COMPONENT, // Each element is a single depth value. The GL converts it to floating point and clamps to the range [0,1].
	DepthStencil = GL_DEPTH_STENCIL, // Each element is a pair of depth and stencil values. The depth component of the pair is interpreted as in DepthComponent. The stencil component is interpreted based on specified the depth + stencil internal format.
	Default = GL_RGBA,
};


inline UString texturePixelFormatToString(const TexturePixelFormat& texturePixelFormat)
{
	switch (texturePixelFormat)
	{
	case TexturePixelFormat::Unset:
		return "Unset";
	case TexturePixelFormat::Red:
		return "Red";
	case TexturePixelFormat::Rg:
		return "Rg";
	case TexturePixelFormat::Rgb:
		return "Rgb";
	case TexturePixelFormat::Bgr:
		return "Bgr";
	case TexturePixelFormat::Rgba:
		return "Rgba";
	case TexturePixelFormat::Bgra:
		return "Bgra";
	case TexturePixelFormat::RedInteger:
		return "RedInteger";
	case TexturePixelFormat::RgInteger:
		return "RgInteger";
	case TexturePixelFormat::RgbInteger:
		return "RgbInteger";
	case TexturePixelFormat::BgrInteger:
		return "BgrInteger";
	case TexturePixelFormat::RgbaInteger:
		return "RgbaInteger";
	case TexturePixelFormat::BgraInteger:
		return "BgraInteger";
	//case TexturePixelFormat::StencilIndex:
	//	return "StencilIndex";
	case TexturePixelFormat::DepthComponent:
		return "DepthComponent";
	case TexturePixelFormat::DepthStencil:
		return "DepthStencil";
	default:
		return "Unknown value";
	}
}


// returns TexturePixelFormat::Unset for any erroneuos string
inline TexturePixelFormat stringToTexturePixelFormat(const UString& str)
{
	if (str.equals("Red")) {
		return TexturePixelFormat::Red;
	}
	else if (str.equals("Rg")) {
		return TexturePixelFormat::Rg;
	}
	else if (str.equals("Rgb")) {
		return TexturePixelFormat::Rgb;
	}
	else if (str.equals("Bgr")) {
		return TexturePixelFormat::Bgr;
	}
	else if (str.equals("Rgba")) {
		return TexturePixelFormat::Rgba;
	}
	else if (str.equals("Bgra")) {
		return TexturePixelFormat::Bgra;
	}
	else if (str.equals("RedInteger")) {
		return TexturePixelFormat::RedInteger;
	}
	else if (str.equals("RgInteger")) {
		return TexturePixelFormat::RgInteger;
	}
	else if (str.equals("RgbInteger")) {
		return TexturePixelFormat::RgbInteger;
	}
	else if (str.equals("BgrInteger")) {
		return TexturePixelFormat::BgrInteger;
	}
	else if (str.equals("RgbaInteger")) {
		return TexturePixelFormat::RgbaInteger;
	}
	else if (str.equals("BgraInteger")) {
		return TexturePixelFormat::BgraInteger;
	}
	//else if (str.equals("StencilIndex")) {
	//	return TexturePixelFormat::StencilIndex;
	//}
	else if (str.equals("DepthComponent")) {
		return TexturePixelFormat::DepthComponent;
	}
	else if (str.equals("DepthStencil")) {
		return TexturePixelFormat::DepthStencil;
	}
	else {
		return TexturePixelFormat::Unset;
	}
}


inline GLenum texturePixelFormatToGlenum(const TexturePixelFormat& texturePixelFormat)
{
	if (texturePixelFormat != TexturePixelFormat::Unset) {
		return static_cast<GLenum>(texturePixelFormat);
	}
	else {
		UString error("Cannot convert given TexturePixelFormat::Unset(");
		error.append2(static_cast<GLenum>(texturePixelFormat));
		error += ") to GLenum";
		VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), "");
	}
}


inline bool texturePixelHasAlpha(const TexturePixelFormat& texturePixelFormat)
{
	switch (texturePixelFormat)
	{
	case TexturePixelFormat::Rgba:
		return true;
	case TexturePixelFormat::Bgra:
		return true;
	case TexturePixelFormat::RgbaInteger:
		return true;
	case TexturePixelFormat::BgraInteger:
		return true;
	default:
		return false;
	}
}


inline size_t texturePixelFormatToChannelsCount(const TexturePixelFormat& texturePixelFormat)
{
	switch (texturePixelFormat)
	{
	case TexturePixelFormat::Unset:
		return 0;
	case TexturePixelFormat::Red:
		return 1;
	case TexturePixelFormat::Rg:
		return 2;
	case TexturePixelFormat::Rgb:
		return 3;
	case TexturePixelFormat::Bgr:
		return 3;
	case TexturePixelFormat::Rgba:
		return 4;
	case TexturePixelFormat::Bgra:
		return 4;
	case TexturePixelFormat::RedInteger:
		return 0;
	case TexturePixelFormat::RgInteger:
		return 2;
	case TexturePixelFormat::RgbInteger:
		return 3;
	case TexturePixelFormat::BgrInteger:
		return 3;
	case TexturePixelFormat::RgbaInteger:
		return 4;
	case TexturePixelFormat::BgraInteger:
		return 4;
	//case TexturePixelFormat::StencilIndex:
	//	return 1;
	case TexturePixelFormat::DepthComponent:
		return 4;
	case TexturePixelFormat::DepthStencil:
		return 4;
	default:
		return 0;
	}
}


} // End namespace Verso

#endif // End header guard

