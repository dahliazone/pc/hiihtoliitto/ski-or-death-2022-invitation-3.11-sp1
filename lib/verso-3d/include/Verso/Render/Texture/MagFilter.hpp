#ifndef VERSO_3D_RENDER_TEXTURE_MAGFILTER_HPP
#define VERSO_3D_RENDER_TEXTURE_MAGFILTER_HPP

#include <Verso/Render/Opengl.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>

namespace Verso {


enum class MagFilter : GLenum
{
	Unset = 0,
	Nearest = GL_NEAREST, // Returns the value of the texture element that is nearest (in Manhattan distance) to the specified texture coordinates.
	Linear = GL_LINEAR, // Returns the weighted average of the texture elements that are closest to the specified texture coordinates. These can include items wrapped or repeated from other parts of a texture, depending on the values of GL_TEXTURE_WRAP_S and GL_TEXTURE_WRAP_T, and on the exact mapping.
	Default = GL_LINEAR,
};


inline UString magFilterToString(const MagFilter& magFilter)
{
	switch (magFilter)
	{
	case MagFilter::Unset:
		return "Unset";
	case MagFilter::Nearest:
		return "Nearest";
	case MagFilter::Linear:
		return "Linear";
	default:
		return "Unknown value";
	}
}


// returns MagFilter::Unset for any erroneuos string
inline MagFilter stringToMagFilter(const UString& str)
{
	if (str.equals("Nearest")) {
		return MagFilter::Nearest;
	}
	else if (str.equals("Linear")) {
		return MagFilter::Linear;
	}
	else if (str.equals("Default")) {
		return MagFilter::Default;
	}
	else {
		return MagFilter::Unset;
	}
}


inline GLenum magFilterToGlenum(const MagFilter& magFilter)
{
	if (magFilter != MagFilter::Unset) {
		return static_cast<GLenum>(magFilter);
	}
	else {
		UString error("Cannot convert MagFilter::Unset(");
		error.append2(static_cast<GLenum>(magFilter));
		error += ") to GLenum";
		VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), "");
	}
}


} // End namespace Verso

#endif // End header guard

