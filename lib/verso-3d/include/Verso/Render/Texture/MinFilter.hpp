#ifndef VERSO_3D_RENDER_TEXTURE_MINFILTER_HPP
#define VERSO_3D_RENDER_TEXTURE_MINFILTER_HPP

#include <Verso/Render/Opengl.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>

namespace Verso {


enum class MinFilter : GLenum
{
	Unset = 0,
	Nearest = GL_NEAREST, // Returns the value of the texture element that is nearest (in Manhattan distance) to the specified texture coordinates.
	Linear = GL_LINEAR, // Returns the weighted average of the four texture elements that are closest to the specified texture coordinates. These can include items wrapped or repeated from other parts of a texture, depending on the values of GL_TEXTURE_WRAP_S and GL_TEXTURE_WRAP_T, and on the exact mapping.
	NearestMipmapNearest = GL_NEAREST_MIPMAP_NEAREST, // Chooses the mipmap that most closely matches the size of the pixel being textured and uses the GL_NEAREST criterion (the texture element closest to the specified texture coordinates) to produce a texture value.
	LinearMipmapNearest = GL_LINEAR_MIPMAP_NEAREST, // Chooses the mipmap that most closely matches the size of the pixel being textured and uses the GL_LINEAR criterion (a weighted average of the four texture elements that are closest to the specified texture coordinates) to produce a texture value.
	NearestMipmapLinear = GL_NEAREST_MIPMAP_LINEAR, // Chooses the two mipmaps that most closely match the size of the pixel being textured and uses the GL_NEAREST criterion (the texture element closest to the specified texture coordinates ) to produce a texture value from each mipmap. The final texture value is a weighted average of those two values.
	LinearMipmapLinear = GL_LINEAR_MIPMAP_LINEAR, // Chooses the two mipmaps that most closely match the size of the pixel being textured and uses the GL_LINEAR criterion (a weighted average of the texture elements that are closest to the specified texture coordinates) to produce a texture value from each mipmap. The final texture value is a weighted average of those two values.
	Default = GL_NEAREST_MIPMAP_LINEAR,
};


inline bool minFilterNeedMipmaps(const MinFilter& minFilter)
{
	if (minFilter == MinFilter::NearestMipmapNearest) {
		return true;
	}
	else if (minFilter == MinFilter::LinearMipmapNearest) {
		return true;
	}
	else if (minFilter == MinFilter::NearestMipmapLinear) {
		return true;
	}
	else if (minFilter == MinFilter::LinearMipmapLinear) {
		return true;
	}

	return false;
}


inline UString minFilterToString(const MinFilter& minFilter)
{
	switch (minFilter)
	{
	case MinFilter::Unset:
		return "Unset";
	case MinFilter::Nearest:
		return "Nearest";
	case MinFilter::Linear:
		return "Linear";
	case MinFilter::NearestMipmapNearest:
		return "NearestMipmapNearest";
	case MinFilter::LinearMipmapNearest:
		return "LinearMipmapNearest";
	case MinFilter::NearestMipmapLinear:
		return "NearestMipmapLinear";
	case MinFilter::LinearMipmapLinear:
		return "LinearMipmapLinear";
	default:
		return "Unknown value";
	}
}


// returns MinFilter::Unset for any erroneuos string
inline MinFilter stringToMinFilter(const UString& str)
{
	if (str.equals("Nearest")) {
		return MinFilter::Nearest;
	}
	else if (str.equals("Linear")) {
		return MinFilter::Linear;
	}
	else if (str.equals("NearestMipmapNearest")) {
		return MinFilter::NearestMipmapNearest;
	}
	else if (str.equals("LinearMipmapNearest")) {
		return MinFilter::LinearMipmapNearest;
	}
	else if (str.equals("NearestMipmapLinear")) {
		return MinFilter::NearestMipmapLinear;
	}
	else if (str.equals("LinearMipmapLinear")) {
		return MinFilter::LinearMipmapLinear;
	}
	else if (str.equals("Default")) {
		return MinFilter::Default;
	}
	else {
		return MinFilter::Unset;
	}
}


inline GLenum minFilterToGlenum(const MinFilter& minFilter)
{
	if (minFilter != MinFilter::Unset) {
		return static_cast<GLenum>(minFilter);
	}
	else {
		UString error("Cannot convert MinFilter::Unset(");
		error.append2(static_cast<GLenum>(minFilter));
		error += ") to GLenum";
		VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), "");
	}
}


} // End namespace Verso

#endif // End header guard

