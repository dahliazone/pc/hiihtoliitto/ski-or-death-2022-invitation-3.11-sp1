#ifndef VERSO_3D_RENDER_TEXTURE_TEXTURETARGET_HPP
#define VERSO_3D_RENDER_TEXTURE_TEXTURETARGET_HPP

#include <Verso/Render/Opengl.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>

namespace Verso {


enum class TextureTarget : GLenum
{
	Unset = 0,
	Texture2d = GL_TEXTURE_2D, //
	ProxyTexture2d = GL_PROXY_TEXTURE_2D, //
	Texture1dArray = GL_TEXTURE_1D_ARRAY, //
	ProxyTexture1dArray = GL_PROXY_TEXTURE_1D_ARRAY, //
	TextureRectangle = GL_TEXTURE_RECTANGLE, //
	ProxyTextureRectangle = GL_PROXY_TEXTURE_RECTANGLE, //
	TextureCubeMapPositiveX = GL_TEXTURE_CUBE_MAP_POSITIVE_X, //
	TextureCubeMapNegativeX = GL_TEXTURE_CUBE_MAP_NEGATIVE_X, //
	TextureCubeMapPositiveY = GL_TEXTURE_CUBE_MAP_POSITIVE_Y, //
	TextureCubeMapNegativeY = GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, //
	TextureCubeMapPositiveZ = GL_TEXTURE_CUBE_MAP_POSITIVE_Z, //
	TextureCubeMapNegativeZ = GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, //
	ProxyTextureCubeMap = GL_PROXY_TEXTURE_CUBE_MAP, //
	Default = GL_TEXTURE_2D,
};


inline UString textureTargetToString(const TextureTarget& textureTarget)
{
	switch (textureTarget)
	{
	case TextureTarget::Unset:
		return "Unset";
	case TextureTarget::Texture2d:
		return "Texture2d";
	case TextureTarget::ProxyTexture2d:
		return "ProxyTexture2d";
	case TextureTarget::Texture1dArray:
		return "Texture1dArray";
	case TextureTarget::ProxyTexture1dArray:
		return "ProxyTexture1dArray";
	case TextureTarget::TextureRectangle:
		return "TextureRectangle";
	case TextureTarget::ProxyTextureRectangle:
		return "ProxyTextureRectangle";
	case TextureTarget::TextureCubeMapPositiveX:
		return "TextureCubeMapPositiveX";
	case TextureTarget::TextureCubeMapNegativeX:
		return "TextureCubeMapNegativeX";
	case TextureTarget::TextureCubeMapPositiveY:
		return "TextureCubeMapPositiveY";
	case TextureTarget::TextureCubeMapNegativeY:
		return "TextureCubeMapNegativeY";
	case TextureTarget::TextureCubeMapPositiveZ:
		return "TextureCubeMapPositiveZ";
	case TextureTarget::TextureCubeMapNegativeZ:
		return "TextureCubeMapNegativeZ";
	case TextureTarget::ProxyTextureCubeMap:
		return "ProxyTextureCubeMap";
	default:
		return "Unknown value";
	}
}


inline GLenum textureTargetToGlenum(const TextureTarget& textureTarget)
{
	if (textureTarget != TextureTarget::Unset) {
		return static_cast<GLenum>(textureTarget);
	}
	else {
		UString error("Cannot convert TextureTarget::Unset(");
		error.append2(static_cast<GLenum>(textureTarget));
		error += ") to GLenum";
		VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), "");
	}
}


} // End namespace Verso

#endif // End header guard

