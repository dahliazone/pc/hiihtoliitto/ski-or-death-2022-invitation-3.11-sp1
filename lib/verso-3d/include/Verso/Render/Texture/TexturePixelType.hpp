#ifndef VERSO_3D_RENDER_TEXTURE_TEXTUREPIXELTYPE_HPP
#define VERSO_3D_RENDER_TEXTURE_TEXTUREPIXELTYPE_HPP

#include <Verso/Render/Opengl.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>

namespace Verso {


enum class TexturePixelType : GLenum
{
	Unset = 0,
	UnsignedByte = GL_UNSIGNED_BYTE, //
	Byte = GL_BYTE, //
	UnsignedShort = GL_UNSIGNED_SHORT, //
	Short = GL_SHORT, //
	UnsignedInt = GL_UNSIGNED_INT, //
	Int = GL_INT, //
	Float = GL_FLOAT, //
	UnsignedByte_3_3_2 = GL_UNSIGNED_BYTE_3_3_2, //
	UnsignedByte_2_3_3_Rev = GL_UNSIGNED_BYTE_2_3_3_REV, //
	UnsignedShort_5_6_5 = GL_UNSIGNED_SHORT_5_6_5, //
	UnsignedShort_5_6_5_Rev = GL_UNSIGNED_SHORT_5_6_5_REV, //
	UnsignedShort_4_4_4_4 = GL_UNSIGNED_SHORT_4_4_4_4, //
	UnsignedShort_4_4_4_4_Rev = GL_UNSIGNED_SHORT_4_4_4_4_REV, //
	UnsignedShort_5_5_5_1 = GL_UNSIGNED_SHORT_5_5_5_1, //
	UnsignedShort_1_5_5_5_Rev = GL_UNSIGNED_SHORT_1_5_5_5_REV, //
	UnsignedInt_8_8_8_8 = GL_UNSIGNED_INT_8_8_8_8, //
	UnsignedInt_8_8_8_8_Rev = GL_UNSIGNED_INT_8_8_8_8_REV, //
	UnsignedInt_10_10_10_2 = GL_UNSIGNED_INT_10_10_10_2, //
	UnsignedInt_2_10_10_10_Rev = GL_UNSIGNED_INT_2_10_10_10_REV, //
};


inline UString texturePixelTypeToString(const TexturePixelType& texturePixelType)
{
	switch (texturePixelType)
	{
	case TexturePixelType::Unset:
		return "Unset";
	case TexturePixelType::UnsignedByte:
		return "UnsignedByte";
	case TexturePixelType::Byte:
		return "Byte";
	case TexturePixelType::UnsignedShort:
		return "UnsignedShort";
	case TexturePixelType::Short:
		return "Short";
	case TexturePixelType::UnsignedInt:
		return "UnsignedInt";
	case TexturePixelType::Int:
		return "Int";
	case TexturePixelType::Float:
		return "Float";
	case TexturePixelType::UnsignedByte_3_3_2:
		return "UnsignedByte_3_3_2";
	case TexturePixelType::UnsignedByte_2_3_3_Rev:
		return "UnsignedByte_2_3_3_Rev";
	case TexturePixelType::UnsignedShort_5_6_5:
		return "UnsignedShort_5_6_5";
	case TexturePixelType::UnsignedShort_5_6_5_Rev:
		return "UnsignedShort_5_6_5_Rev";
	case TexturePixelType::UnsignedShort_4_4_4_4:
		return "UnsignedShort_4_4_4_4";
	case TexturePixelType::UnsignedShort_4_4_4_4_Rev:
		return "UnsignedShort_4_4_4_4_Rev";
	case TexturePixelType::UnsignedShort_5_5_5_1:
		return "UnsignedShort_5_5_5_1";
	case TexturePixelType::UnsignedShort_1_5_5_5_Rev:
		return "UnsignedShort_1_5_5_5_Rev";
	case TexturePixelType::UnsignedInt_8_8_8_8:
		return "UnsignedInt_8_8_8_8";
	case TexturePixelType::UnsignedInt_8_8_8_8_Rev:
		return "UnsignedInt_8_8_8_8_Rev";
	case TexturePixelType::UnsignedInt_10_10_10_2:
		return "UnsignedInt_10_10_10_2";
	case TexturePixelType::UnsignedInt_2_10_10_10_Rev:
		return "UnsignedInt_2_10_10_10_Rev";
	default:
		return "Unknown value";
	}
}


// returns TexturePixelType::Unset for any erroneuos string
inline TexturePixelType stringToTexturePixelType(const UString& str)
{
	if (str.equals("UnsignedByte")) {
		return TexturePixelType::UnsignedByte;
	}
	else if (str.equals("Byte")) {
		return TexturePixelType::Byte;
	}
	else if (str.equals("UnsignedShort")) {
		return TexturePixelType::UnsignedShort;
	}
	else if (str.equals("Short")) {
		return TexturePixelType::Short;
	}
	else if (str.equals("UnsignedInt")) {
		return TexturePixelType::UnsignedInt;
	}
	else if (str.equals("Int")) {
		return TexturePixelType::Int;
	}
	else if (str.equals("Float")) {
		return TexturePixelType::Float;
	}
	else if (str.equals("UnsignedByte_3_3_2")) {
		return TexturePixelType::UnsignedByte_3_3_2;
	}
	else if (str.equals("UnsignedByte_2_3_3_Rev")) {
		return TexturePixelType::UnsignedByte_2_3_3_Rev;
	}
	else if (str.equals("UnsignedShort_5_6_5")) {
		return TexturePixelType::UnsignedShort_5_6_5;
	}
	else if (str.equals("UnsignedShort_5_6_5_Rev")) {
		return TexturePixelType::UnsignedShort_5_6_5_Rev;
	}
	else if (str.equals("UnsignedShort_4_4_4_4")) {
		return TexturePixelType::UnsignedShort_4_4_4_4;
	}
	else if (str.equals("UnsignedShort_4_4_4_4_Rev")) {
		return TexturePixelType::UnsignedShort_4_4_4_4_Rev;
	}
	else if (str.equals("UnsignedShort_5_5_5_1")) {
		return TexturePixelType::UnsignedShort_5_5_5_1;
	}
	else if (str.equals("UnsignedShort_1_5_5_5_Rev")) {
		return TexturePixelType::UnsignedShort_1_5_5_5_Rev;
	}
	else if (str.equals("UnsignedInt_8_8_8_8")) {
		return TexturePixelType::UnsignedInt_8_8_8_8;
	}
	else if (str.equals("UnsignedInt_8_8_8_8_Rev")) {
		return TexturePixelType::UnsignedInt_8_8_8_8_Rev;
	}
	else if (str.equals("UnsignedInt_10_10_10_2")) {
		return TexturePixelType::UnsignedInt_10_10_10_2;
	}
	else if (str.equals("UnsignedInt_2_10_10_10_Rev")) {
		return TexturePixelType::UnsignedInt_2_10_10_10_Rev;
	}
	else {
		return TexturePixelType::Unset;
	}
}


inline GLenum texturePixelTypeToGlenum(const TexturePixelType& texturePixelType)
{
	if (texturePixelType != TexturePixelType::Unset) {
		return static_cast<GLenum>(texturePixelType);
	}
	else {
		UString error("Cannot convert TexturePixelType::Unset(");
		error.append2(static_cast<GLenum>(texturePixelType));
		error += ") to GLenum";
		VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), "");
	}
}


} // End namespace Verso

#endif // End header guard

