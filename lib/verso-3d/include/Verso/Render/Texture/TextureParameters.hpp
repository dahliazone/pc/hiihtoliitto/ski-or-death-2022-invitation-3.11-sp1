#ifndef VERSO_3D_RENDER_TEXTURE_TEXTUREPARAMETERS_HPP
#define VERSO_3D_RENDER_TEXTURE_TEXTUREPARAMETERS_HPP

#include <Verso/verso-3d-common.hpp>
#include <Verso/Math/AspectRatio.hpp>
#include <Verso/Render/Texture/MinFilter.hpp>
#include <Verso/Render/Texture/MagFilter.hpp>
#include <Verso/Render/Texture/WrapStyle.hpp>
#include <Verso/Render/Texture/TexturePixelFormat.hpp>
#include <Verso/Render/Texture/TexturePixelType.hpp>
#include <Verso/Render/Texture/TextureTarget.hpp>

namespace Verso {

class TextureParam;

//type
//The default is THREE.UnsignedByteType. Other valid types (as WebGL allows) are THREE.ByteType, THREE.ShortType, THREE.UnsignedShortType, THREE.IntType, THREE.UnsignedIntType, THREE.HalfFloatType, THREE.FloatType, THREE.UnsignedShort4444Type, THREE.UnsignedShort5551Type, and THREE.UnsignedShort565Type.
//anisotropy? 1-2^n, get max from gpu


class TextureParameters
{
public: // member variables
	UString typeForShader;
	TexturePixelFormat pixelFormat;
	MinFilter minFilter;
	MagFilter magFilter;
	WrapStyle wrapStyleS;
	WrapStyle wrapStyleT;

public:
	VERSO_3D_API TextureParameters();

	VERSO_3D_API explicit TextureParameters(
			const UString& typeForShader,
			const TexturePixelFormat& pixelFormat = TexturePixelFormat::Unset,
			const MinFilter& minFilter = MinFilter::Default, const MagFilter& magFilter = MagFilter::Default,
			const WrapStyle& wrapStyleS = WrapStyle::Default, const WrapStyle& wrapStyleT = WrapStyle::Default);

	VERSO_3D_API explicit TextureParameters(const TextureParam& textureParam);

	VERSO_3D_API TextureParameters(const TextureParameters& original);

	VERSO_3D_API TextureParameters(TextureParameters&& original);

	VERSO_3D_API TextureParameters& operator =(const TextureParameters& original);

	VERSO_3D_API TextureParameters& operator =(TextureParameters&& original);

	VERSO_3D_API ~TextureParameters();

public: // toString
	VERSO_3D_API UString toString() const;

	VERSO_3D_API UString toStringDebug() const;


	friend std::ostream& operator <<(std::ostream& ost, const TextureParameters& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso

#endif // End header guard

