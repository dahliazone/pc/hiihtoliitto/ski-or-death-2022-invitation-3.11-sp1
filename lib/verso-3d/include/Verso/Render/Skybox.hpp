#ifndef VERSO_3D_RENDER_SKYBOX_HPP
#define VERSO_3D_RENDER_SKYBOX_HPP

#include <Verso/Render/Vao.hpp>
#include <Verso/Render/Material/MaterialTexture3d.hpp>

namespace Verso {

class IWindowOpengl;


class Skybox
{
private:
	Vao vaoPosX;
	Vao vaoNegX;
	Vao vaoPosY;
	Vao vaoNegY;
	Vao vaoPosZ;
	Vao vaoNegZ;
	MaterialTexture3d matPosX;
	MaterialTexture3d matNegX;
	MaterialTexture3d matPosY;
	MaterialTexture3d matNegY;
	MaterialTexture3d matPosZ;
	MaterialTexture3d matNegZ;
	bool created;

public:
	VERSO_3D_API Skybox();

	Skybox(const Skybox& original) = delete;

	VERSO_3D_API Skybox(Skybox&& original);

	Skybox& operator =(const Skybox& original) = delete;

	VERSO_3D_API Skybox& operator =(Skybox&& original);

	VERSO_3D_API virtual ~Skybox();

public:
	VERSO_3D_API virtual void createFromPath(IWindowOpengl& window,
											 const UString& skyboxPath, const ICamera& camera,
											 bool swapX = false, bool swapY = false, bool swapZ = true);

	VERSO_3D_API virtual void createFromFiles(IWindowOpengl& window,
											  const UString& posXFileName, const UString& negXFileName,
											  const UString& posYFileName, const UString& negYFileName,
											  const UString& posZFileName, const UString& negZFileName,
											  const ICamera& camera);

	VERSO_3D_API virtual void destroy() VERSO_NOEXCEPT;

	VERSO_3D_API virtual bool isCreated() const;

	VERSO_3D_API virtual void changeShaders(const UString& vertexSourceFileName, const UString& fragmentSourceFileName);

	VERSO_3D_API virtual void changeShaders(const UString& vertexSourceFileName, const UString& fragmentSourceFileName, const UString& geometrySourceFileName);

	VERSO_3D_API virtual void render(IWindowOpengl& window, const FrameTimestamp& time, const ICamera& camera);

public: // toString
	VERSO_3D_API virtual UString toString(const UString& newLinePadding) const;

	VERSO_3D_API virtual UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const Skybox& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso


#endif // End header guard

