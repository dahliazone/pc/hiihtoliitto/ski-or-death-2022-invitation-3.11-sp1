#ifndef VERSO_3D_RENDER_RESOURCEMANAGER_HPP
#define VERSO_3D_RENDER_RESOURCEMANAGER_HPP

#include <Verso/Render/Fbo.hpp>
#include <Verso/Render/Texture.hpp>
#include <Verso/Render/Camera/ICamera.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/Math/Hash32.hpp>
#include <map>

namespace Verso {


class IWindowOpengl;


// \TODO: make OpenGL context
class ResourceManager
{
private: // member variables
	bool created;
	IWindowOpengl* window;

	Texture* defaultTexture;
	std::map<Hash32, Texture*> textures;
	std::map<Hash32, UString> texturesHashNames;
	std::map<Hash32, size_t> texturesRefCount;

	std::map<UString, ICamera*> cameras;
	std::map<UString, size_t> camerasRefCount;

	std::map<UString, Fbo*> fbos;
	std::map<UString, size_t> fbosRefCount;

public:
	VERSO_3D_API ResourceManager();

	ResourceManager(const ResourceManager& original) = delete;

	VERSO_3D_API ResourceManager(ResourceManager&& original);

	ResourceManager& operator =(const ResourceManager& original) = delete;

	VERSO_3D_API ResourceManager& operator =(ResourceManager&& original);

	VERSO_3D_API ~ResourceManager();

public:
	VERSO_3D_API void create(IWindowOpengl* window);

	VERSO_3D_API void setupDefaultTexture(const UString& defaultTextureFileName);

	VERSO_3D_API void destroy();

	VERSO_3D_API bool isCreated() const;

	VERSO_3D_API void onResize();

public:
	VERSO_3D_API Texture* getTexture(const UString& hashName);
	VERSO_3D_API Texture* getTexture(const Hash32& hash);

	// Resource manager will handle freeing memory!
	VERSO_3D_API Texture* loadTexture(const UString& fileName, const TextureParameters& parameters);
	VERSO_3D_API void releaseTexture(Texture* texture);

public:
	VERSO_3D_API ICamera* getCamera(const UString& uniqueId);

	// Resource manager will NOT handle freeing memory!
	VERSO_3D_API bool registerCamera(ICamera* camera);
	VERSO_3D_API void unregisterCamera(const UString& uniqueId);

	VERSO_3D_API Fbo* getFbo(const UString& uniqueId);

	// Resource manager will handle freeing memory!
	VERSO_3D_API Fbo* createFbo(IWindowOpengl* window, const UString& uniqueId);

	VERSO_3D_API void destroyFbo(Fbo* fbo);
	VERSO_3D_API void destroyFbo(const UString& uniqueId);

public: // hash name generators
	VERSO_3D_API UString generateTextureHashName(const UString& fileName, const TextureParameters& parameters);

public: // toString
	VERSO_3D_API UString toString() const;

	VERSO_3D_API UString toStringDebug() const;


	friend std::ostream& operator <<(std::ostream& ost, const ResourceManager& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso


#endif // End header guard

