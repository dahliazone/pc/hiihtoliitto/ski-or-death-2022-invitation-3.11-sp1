#ifndef VERSO_3D_RENDER_PARTICLE_BEHAVIOURS_PARTICLEBEHAVIOUR_HPP
#define VERSO_3D_RENDER_PARTICLE_BEHAVIOURS_PARTICLEBEHAVIOUR_HPP

#include <Verso/Render/Particle/Particle.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/Time/FrameTimestamp.hpp>

namespace Verso {


class VERSO_3D_API ParticleBehaviour
{
public:
	ParticleBehaviour() {}
	virtual ~ParticleBehaviour() {}

	virtual void reset() = 0;

	virtual void behave(const FrameTimestamp& time, Particle* particle,
						std::vector<Particle>& particles,
						size_t index, size_t startIndex, size_t endIndex) = 0;

	virtual UString getType() const = 0;
	virtual UString toString() const = 0;
	virtual UString toStringDebug() const = 0;
};


} // End namespace Verso

#endif // End header guard

