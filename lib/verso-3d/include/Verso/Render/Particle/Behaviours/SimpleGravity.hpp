#ifndef VERSO_3D_RENDER_PARTICLE_BEHAVIOURS_SIMPLEGRAVITY_HPP
#define VERSO_3D_RENDER_PARTICLE_BEHAVIOURS_SIMPLEGRAVITY_HPP

#include <Verso/Render/Particle/Behaviours/ParticleBehaviour.hpp>
#include <Verso/Math/Vector3.hpp>

namespace Verso {


class SimpleGravity : public ParticleBehaviour
{
private:
	Vector3f gravity;

public:
	SimpleGravity();
	virtual ~SimpleGravity() override;

	virtual void reset() override;

	virtual void behave(const FrameTimestamp& time, Particle* particle,
						std::vector<Particle>& particles,
						size_t index, size_t startIndex, size_t endIndex) override;


	void setGravity(const Vector3f& gravity);

	virtual UString toString() const override;
	virtual UString toStringDebug() const override;

	virtual UString getType() const override
	{
		return getTypeStatic();
	}

	static UString getTypeStatic()
	{
		return "SimpleGravity";
	}
};



inline SimpleGravity::SimpleGravity() :
	gravity(0.0f, 0.0f, 0.0f)
{
}


inline SimpleGravity::~SimpleGravity()
{
}


inline void SimpleGravity::reset()
{
}


inline void SimpleGravity::behave(const FrameTimestamp& time, Particle* particle,
								  std::vector<Particle>& particles,
								  size_t index, size_t startIndex, size_t endIndex)
{
	(void)particles; (void)index; (void)startIndex; (void)endIndex;

	particle->velocity += gravity * static_cast<float>(time.getDt().asSeconds());
}


inline void SimpleGravity::setGravity(const Vector3f& gravity)
{
	this->gravity = gravity;
}


inline UString SimpleGravity::toString() const
{
	return UString("gravity="+gravity.toString());
}


inline UString SimpleGravity::toStringDebug() const
{
	return UString(getType()+"("+toString()+")");
}


} // End namespace Verso

#endif // End header guard

