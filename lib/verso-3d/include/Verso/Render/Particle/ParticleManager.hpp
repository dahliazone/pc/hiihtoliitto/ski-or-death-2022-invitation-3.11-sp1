#ifndef VERSO_3D_RENDER_PARTICLE_PARTICLEMANAGER_HPP
#define VERSO_3D_RENDER_PARTICLE_PARTICLEMANAGER_HPP

#include <Verso/Render/Particle/ParticleSystem.hpp>
#include <Verso/Render/Particle/Effects/ParticleEffect.hpp>
#include <Verso/Render/Particle/Sequencers/ParticleSequencer.hpp>
#include <Verso/System/assert.hpp>

namespace Verso {


class ParticleManager
{
private:
	UString id;
	std::vector<ParticleEffect*> particleEffects;
	std::vector<ParticleSystem*> particleSystems;

	double timer;
	double interval;

public:
	VERSO_3D_API explicit ParticleManager(const UString& id);
	ParticleManager(const ParticleManager& original) = delete;
	VERSO_3D_API ParticleManager(ParticleManager&& original);
	ParticleManager& operator =(const ParticleManager& original) = delete;
	VERSO_3D_API ParticleManager& operator =(ParticleManager&& original);
	VERSO_3D_API ~ParticleManager();

public:
	VERSO_3D_API void update(IWindowOpengl& window, const FrameTimestamp& time, ICamera& camera);

	/**
	 * @return index of added particle effect.
	 */
	VERSO_3D_API size_t addParticleEffect(ParticleEffect* particleEffect);
	VERSO_3D_API ParticleEffect* getParticleEffect(size_t index);
	VERSO_3D_API void removeParticleEffect(size_t particleEffectId);

	/**
	 * note! If you already added a ParticleEffect then
	 * don't add it's ParticleSystem separately.
	 *
	 * @return index of added particle system.
	 */
	VERSO_3D_API size_t addParticleSystem(ParticleSystem* particleSystem);
	VERSO_3D_API ParticleSystem* getParticleSystem(size_t index);
	VERSO_3D_API void removeParticleSystem(size_t particleSystemId);

	VERSO_3D_API UString getId() const;
	VERSO_3D_API void setId(const UString& id);

	VERSO_3D_API double getInterval() const;
	VERSO_3D_API void setInterval(double interval);

private:
	void calculate(const FrameTimestamp& time);
	void render(IWindowOpengl& window, const FrameTimestamp& time, ICamera& camera);
	void calculateAndRender(IWindowOpengl& window, const FrameTimestamp& time, ICamera& camera);
	void removeOld(const FrameTimestamp& time);
};


inline UString ParticleManager::getId() const
{
	return id;
}


inline void ParticleManager::setId(const UString& id)
{
	this->id = id;
}


inline double ParticleManager::getInterval() const
{
	return interval;
}


inline void ParticleManager::setInterval(double interval)
{
	VERSO_ASSERT("verso-3d", interval <= 0.0f);
	this->interval = interval;
}


} // End namespace Verso

#endif // End header guard

