#ifndef VERSO_3D_RENDER_PARTICLE_SEQUENCERS_PARTICLESEQUENCER_HPP
#define VERSO_3D_RENDER_PARTICLE_SEQUENCERS_PARTICLESEQUENCER_HPP

#include <Verso/Render/Particle/Emitters/ParticleEmitter.hpp>
#include <Verso/Time/FrameTimestamp.hpp>

namespace Verso {


class VERSO_3D_API ParticleSequencer
{
private:
	ParticleEmitter* particleEmitter;

public:
	ParticleSequencer();
	virtual ~ParticleSequencer();

	ParticleEmitter* getParticleEmitter() const;
	void setParticleEmitter(ParticleEmitter* particleEmitter);

	virtual void reset() = 0;
	virtual void sequence(const FrameTimestamp& time) = 0;
	virtual bool isDone() const = 0;

	virtual UString getType() const = 0;
	virtual UString toString() const = 0;
	virtual UString toStringDebug() const = 0;
};


inline ParticleSequencer::ParticleSequencer() :
	particleEmitter()
{
}


inline ParticleSequencer::~ParticleSequencer()
{
}


inline ParticleEmitter* ParticleSequencer::getParticleEmitter() const
{
	return particleEmitter;
}


inline void ParticleSequencer::setParticleEmitter(ParticleEmitter* particleEmitter)
{
	this->particleEmitter = particleEmitter;
}


} // End namespace Verso

#endif // End header guard

