#ifndef VERSO_3D_RENDER_PARTICLE_SEQUENCERS_BURSTINTERVAL_HPP
#define VERSO_3D_RENDER_PARTICLE_SEQUENCERS_BURSTINTERVAL_HPP

#include <Verso/Render/Particle/Sequencers/ParticleSequencer.hpp>

namespace Verso {


enum class SequenceType : std::int32_t
{
	Unset,
	None,
	Single,
	Continuous
};


// returns SequenceType::Unset for any erroneuos string
inline SequenceType stringToSequenceType(const UString& str)
{
	if (str.equals("None")) {
		return SequenceType::None;
	}
	else if (str.equals("Single")) {
		return SequenceType::Single;
	}
	else if (str.equals("Continuous")) {
		return SequenceType::Continuous;
	}
	else {
		return SequenceType::Unset;
	}
}


inline UString sequenceTypeToString(SequenceType sequenceType)
{
	switch (sequenceType) {
	case SequenceType::Unset:
		return "Unset";
	case SequenceType::None:
		return "None";
	case SequenceType::Single:
		return "Single";
	case SequenceType::Continuous:
		return "Continuous";
	default:
		return "unknown value";
	}
}


/**
 * .
 *
 * <pre>
 * Example of sequencing emitting timeline:
 *
 * ......>*--....*--....*--....*--..<........
 *
 * ... = nothing happens
 *  >  = startTime
 *  <   = endtime
 *  *  = interval point (burst emitting happens here if defined)
 *  -- = duration of continous particle emitting (at rate of particlesPerSecond)
 *
 * Usage examples:
 *
 *	// Single burst-test (no duration)
 *	// => only single burst
 *	burstInterval->setSequenceType(ParticleSequencers::ParticleSequencer::SINGLE);
 *	burstInterval->setStartTime(2.0f);
 *	burstInterval->setBurstSize(100);
 *
 *	// Single burst-test (with duration of continous emit)
 *	burstInterval->setSequenceType(ParticleSequencers::ParticleSequencer::SINGLE);
 *	burstInterval->setStartTime(2.0f);
 *	burstInterval->setBurstSize(100);
 *	burstInterval->setDuration(5.0f);
 *	burstInterval->setParticlesPerSecond(10);
 *
 *	// Continous emit-test (no burst, no interval, no duration)
 *	// => continous emit
 *	burstInterval->setSequenceType(ParticleSequencers::ParticleSequencer::CONTINOUS);
 *	burstInterval->setStartTime(1.0f);
 *	burstInterval->setEndTime(4.0f);
 *	burstInterval->setParticlesPerSecond(10);
 *
 *	// Continous emit-test (no burst, interval, no duration)
 *	// => nothing on screen(!)
 *	burstInterval->setSequenceType(ParticleSequencers::ParticleSequencer::CONTINOUS);
 *	burstInterval->setStartTime(1.0f);
 *	burstInterval->setEndTime(10.0f);
 *	burstInterval->setInterval(1.0f);
 *	burstInterval->setParticlesPerSecond(10);
 *
 *	// Continous emit-test (burst, interval, no duration)
 *	// => bursts in intervals
 *	burstInterval->setSequenceType(ParticleSequencers::ParticleSequencer::CONTINOUS);
 *	burstInterval->setStartTime(1.0f);
 *	burstInterval->setEndTime(10.0f);
 *	burstInterval->setBurstSize(5);
 *	burstInterval->setInterval(1.0f);
 *
 *	// Continous emit-test (burst, interval, duration)
 *	// => bursts in intervals with duration of continous emit after the burst
 *	burstInterval->setSequenceType(ParticleSequencers::ParticleSequencer::CONTINOUS);
 *	burstInterval->setStartTime(1.0f);
 *	burstInterval->setEndTime(20.0f);
 *	burstInterval->setBurstSize(20);
 *	burstInterval->setInterval(4.0f);
 *	burstInterval->setDuration(2.0f);
 *	burstInterval->setParticlesPerSecond(5);
 * </pre>
 */
class VERSO_3D_API BurstInterval : public ParticleSequencer
{
private:
	SequenceType sequenceType;
	double startTime;
	double endTime;
	bool neverEnding;
	double currentTime;
	double interval;
	double intervalPosition;
	double duration;
	double durationPosition;
	double durationEmitted;
	size_t burstSize;
	size_t particlesPerSecond;

public:
	BurstInterval();
	virtual ~BurstInterval() override;

public: // ParticleSequencer-implemented methods
	virtual void reset() override;
	virtual void sequence(const FrameTimestamp& time) override;
	virtual bool isDone() const override;

	virtual UString toString() const override;
	virtual UString toStringDebug() const override;

	virtual UString getType() const override
	{
		return getTypeStatic();
	}

	static UString getTypeStatic()
	{
		return "BurstInterval";
	}

public: // BurstInterval-specific
	SequenceType getSequenceType() const;
	void setSequenceType(SequenceType sequenceType);

	double getStartTime() const;
	void setStartTime(double startTime);

	double getEndTime() const;
	void setEndTime(double endTime);

	bool isNeverEnding() const;
	void setNeverEnding(bool neverEnding);

	double getInterval() const;
	void setInterval(double interval);

	double getDuration() const;
	void setDuration(double duration);

	size_t getBurstSize() const;
	void setBurstSize(size_t burstSize);

	size_t getParticlesPerSecond() const;
	void setParticlesPerSecond(size_t particlesPerSecond);
};


inline SequenceType BurstInterval::getSequenceType() const
{
	return sequenceType;
}


inline void BurstInterval::setSequenceType(SequenceType sequenceType)
{
	this->sequenceType = sequenceType;
}

/*
^[a-z_][a-zA-Z_0-9]* =

(\w)(\s*)=(\s*)

\b(\w+) = \1\b

\w\+\)s*\<\1\>
*/

inline double BurstInterval::getStartTime() const
{
	return startTime;
}


inline void BurstInterval::setStartTime(double startTime)
{
	VERSO_ASSERT("verso-3d", startTime >= 0.0f);
	this->startTime = startTime;
	this->intervalPosition = startTime;
}


inline double BurstInterval::getEndTime() const
{
	return endTime;
}


inline void BurstInterval::setEndTime(double endTime)
{
	VERSO_ASSERT("verso-3d", endTime >= 0.0f);
	this->endTime = endTime;
}


inline bool BurstInterval::isNeverEnding() const
{
	return neverEnding;
}


inline void BurstInterval::setNeverEnding(bool neverEnding)
{
	this->neverEnding = neverEnding;
}


inline double BurstInterval::getInterval() const
{
	return interval;
}


inline void BurstInterval::setInterval(double interval)
{
	VERSO_ASSERT("verso-3d", interval >= 0.0f);
	this->interval = interval;
}


inline double BurstInterval::getDuration() const
{
	return duration;
}


inline void BurstInterval::setDuration(double duration)
{
	VERSO_ASSERT("verso-3d", duration >= 0.0f);
	this->duration = duration;
}


inline size_t BurstInterval::getBurstSize() const
{
	return burstSize;
}


inline void BurstInterval::setBurstSize(size_t burstSize)
{
	this->burstSize = burstSize;
}


inline size_t BurstInterval::getParticlesPerSecond() const
{
	return particlesPerSecond;
}


inline void BurstInterval::setParticlesPerSecond(size_t particlesPerSecond)
{
	VERSO_ASSERT("verso-3d", particlesPerSecond >= 0.0f);
	this->particlesPerSecond = particlesPerSecond;
}


inline bool BurstInterval::isDone() const
{
	if (currentTime >= endTime) {
		return true;
	}
	else {
		return false;
	}
}


inline UString BurstInterval::toString() const
{
	UString str;
	str += "sequenceType=";
	str += sequenceTypeToString(sequenceType);
	str += ", startTime=";
	str.append2(startTime);
	str += ", endTime=";
	str.append2(endTime);
	str += ", neverEnding=";
	str.append2(neverEnding);
	str += ", currentTime=";
	str.append2(currentTime);
	str += ", interval=";
	str.append2(interval);
	str += ", intervalPosition=";
	str.append2(intervalPosition);
	str += ", duration=";
	str.append2(duration);
	str += ", durationPosition=";
	str.append2(durationPosition);
	str += ", durationEmitted=";
	str.append2(durationEmitted);
	str += ", burstSize=";
	str.append2(burstSize);
	str += ", particlesPerSecond=";
	str.append2(particlesPerSecond);
	return str;
}


inline UString BurstInterval::toStringDebug() const
{
	return UString(getType()+"("+toString()+")");
}


} // End namespace Verso

#endif // End header guard

