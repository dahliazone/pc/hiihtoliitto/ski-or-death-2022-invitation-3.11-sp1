#ifndef VERSO_3D_RENDER_PARTICLE_PARTICLE_HPP
#define VERSO_3D_RENDER_PARTICLE_PARTICLE_HPP

#include <Verso/verso-3d-common.hpp>
#include <Verso/Math/Vector3.hpp>
#include <Verso/Math/RgbaColorf.hpp>
#include <cstdint>
#include <vector>

namespace Verso {


/**
 * Struct representing a particle.
 */
struct Particle
{
public:
	Vector3f position;
	float angle;

	Vector3f velocity;
	float angleVelocity;

	float size;
	float startSize;
	float deltaSize;

	float totalLifetime;
	float totalLifetimeInverse;
	float lifetimePassed;

	RgbaColorf color;

	float startAlpha;
	float deltaAlpha;

	uint32_t animationFrame;

	std::vector<Vector3f> oldPosition;
	std::vector<float> oldAngle;
	std::vector<float> oldSize;
	std::vector<uint32_t> oldAnimationFrame;

	uint32_t trailIndex;
	uint32_t trailSize;

public:
	Particle() :
	    position(),
	    angle(0.0f),
	    velocity(),
	    angleVelocity(0.0f),
	    size(0.0f),
	    startSize(0.0f),
	    deltaSize(0.0f),
	    totalLifetime(0.0f),
	    totalLifetimeInverse(0.0f),
	    lifetimePassed(0.0f),
	    color(),
	    startAlpha(0.0f),
	    deltaAlpha(0.0f),
	    animationFrame(0),
	    oldPosition(),
	    oldAngle(),
	    oldSize(),
	    oldAnimationFrame(),
	    trailIndex(0),
	    trailSize(0)
	{
	}
};


} // End namespace Verso

#endif // End header guard

