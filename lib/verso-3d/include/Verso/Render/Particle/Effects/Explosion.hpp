#ifndef VERSO_3D_RENDER_PARTICLE_EFFECTS_EXPLOSION_HPP
#define VERSO_3D_RENDER_PARTICLE_EFFECTS_EXPLOSION_HPP

#include <Verso/Render/Particle/Effects/ParticleEffect.hpp>
#include <Verso/Render/Particle/Behaviours/SimpleGravity.hpp>
#include <Verso/Render/Particle/Emitters/SimpleRandom.hpp>
#include <Verso/Render/Particle/Sequencers/BurstInterval.hpp>

namespace Verso {


class Explosion : public ParticleEffect
{
private:
	bool created;
	UString id;
	ParticleBehaviour* particleBehaviour;
	ParticleSystem* particleSystem;
	ParticleEmitter* particleEmitter;
	ParticleSequencer* particleSequencer;

	ParticleBehaviour* particleBehaviour2;
	ParticleSystem* particleSystem2;
	ParticleEmitter* particleEmitter2;
	ParticleSequencer* particleSequencer2;

public:
	VERSO_3D_API explicit Explosion(const UString& id);

	// \TODO: move operators!

	VERSO_3D_API virtual ~Explosion() override;

public:
	VERSO_3D_API void create(IWindowOpengl& window,
							 const UString& textureFileName1,
							 const UString& textureFileName2,
							 bool animated=false, bool animated2=false);

	VERSO_3D_API void destroy() override;

	VERSO_3D_API bool isCreated() const override;

	VERSO_3D_API void reset() override;

	VERSO_3D_API void calculate(const FrameTimestamp& time) override;

	VERSO_3D_API void render(IWindowOpengl& window, const FrameTimestamp& time, ICamera& camera) override;

	VERSO_3D_API void calculateAndRender(IWindowOpengl& window, const FrameTimestamp& time, ICamera& camera) override;

	VERSO_3D_API bool isDone() const override;

public:
	VERSO_3D_API UString getId() const override;

	VERSO_3D_API void setId(const UString& id) override;

	VERSO_3D_API UString getType() const override;

	VERSO_3D_API static UString getTypeStatic();

	VERSO_3D_API const Vector3f& getPosition() const override;

	VERSO_3D_API void setPosition(const Vector3f& position) override;

	VERSO_3D_API UString toString() const override;

	VERSO_3D_API UString toStringDebug() const override;

public:
	VERSO_3D_API ParticleBehaviour* getParticleBehaviour() const;

	VERSO_3D_API void setParticleBehaviour(ParticleBehaviour* particleBehaviour);

	VERSO_3D_API ParticleSystem* getParticleSystem() const;

	VERSO_3D_API void setParticleSystem(ParticleSystem* particleSystem);

	VERSO_3D_API ParticleEmitter* getParticleEmitter() const;

	VERSO_3D_API void setParticleEmitter(ParticleEmitter* particleEmitter);

	VERSO_3D_API ParticleSequencer* getParticleSequencer() const;

	VERSO_3D_API void setParticleSequencer(ParticleSequencer* particleSequencer);

	VERSO_3D_API ParticleBehaviour* getParticleBehaviour2() const;

	VERSO_3D_API void setParticleBehaviour2(ParticleBehaviour* particleBehaviour);

	VERSO_3D_API ParticleSystem* getParticleSystem2() const;

	VERSO_3D_API void setParticleSystem2(ParticleSystem* particleSystem);

	VERSO_3D_API ParticleEmitter* getParticleEmitter2() const;

	VERSO_3D_API void setParticleEmitter2(ParticleEmitter* particleEmitter);

	VERSO_3D_API ParticleSequencer* getParticleSequencer2() const;

	VERSO_3D_API void setParticleSequencer2(ParticleSequencer* particleSequencer);
};


inline UString Explosion::getId() const
{
	return id;
}


inline void Explosion::setId(const UString& id)
{
	this->id = id;
}


inline UString Explosion::getType() const
{
	return getTypeStatic();
}


inline UString Explosion::getTypeStatic()
{
	return "Explosion";
}


inline const Vector3f& Explosion::getPosition() const
{
	return particleSystem->getPosition();
}


inline void Explosion::setPosition(const Vector3f& position)
{
	particleSystem->setPosition(position);
}


inline ParticleBehaviour* Explosion::getParticleBehaviour() const
{
	return particleBehaviour;
}


inline void Explosion::setParticleBehaviour(ParticleBehaviour* particleBehaviour)
{
	this->particleBehaviour = particleBehaviour;
}


inline ParticleSystem* Explosion::getParticleSystem() const
{
	return particleSystem;
}


inline void Explosion::setParticleSystem(ParticleSystem* particleSystem)
{
	this->particleSystem = particleSystem;
}


inline ParticleEmitter* Explosion::getParticleEmitter() const
{
	return particleEmitter;
}


inline void Explosion::setParticleEmitter(ParticleEmitter* particleEmitter)
{
	this->particleEmitter = particleEmitter;
}


inline ParticleSequencer* Explosion::getParticleSequencer() const
{
	return particleSequencer;
}


inline void Explosion::setParticleSequencer(ParticleSequencer* particleSequencer)
{
	this->particleSequencer = particleSequencer;
}


} // End namespace Verso

#endif // End header guard

