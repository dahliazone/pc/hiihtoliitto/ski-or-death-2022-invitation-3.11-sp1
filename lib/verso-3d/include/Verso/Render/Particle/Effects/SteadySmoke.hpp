#ifndef VERSO_3D_RENDER_PARTICLE_EFFECTS_STEADYSMOKE_HPP
#define VERSO_3D_RENDER_PARTICLE_EFFECTS_STEADYSMOKE_HPP

#include <Verso/Render/Particle/Effects/ParticleEffect.hpp>
#include <Verso/Render/Particle/Behaviours/SimpleGravity.hpp>
#include <Verso/Render/Particle/Emitters/SimpleRandom.hpp>
#include <Verso/Render/Particle/Sequencers/BurstInterval.hpp>

namespace Verso {


class SteadySmoke : public ParticleEffect
{
private:
	bool created;
	UString id;
	ParticleBehaviour* particleBehaviour;
	ParticleSystem* particleSystem;
	ParticleEmitter* particleEmitter;
	ParticleSequencer* particleSequencer;

	size_t amount;

public:
	VERSO_3D_API explicit SteadySmoke(const UString& id);

	// \TODO: move operators!

	VERSO_3D_API virtual ~SteadySmoke() override;

public:
	VERSO_3D_API void create(IWindowOpengl& window, const UString& textureFileName, bool animated = false);

	VERSO_3D_API void destroy() override;

	VERSO_3D_API bool isCreated() const override;

	VERSO_3D_API void reset() override;

	VERSO_3D_API void calculate(const FrameTimestamp& time) override;

	VERSO_3D_API void render(IWindowOpengl& window, const FrameTimestamp& time, ICamera& camera) override;

	VERSO_3D_API void calculateAndRender(IWindowOpengl& window, const FrameTimestamp& time, ICamera& camera) override;

	VERSO_3D_API bool isDone() const override;

public:
	VERSO_3D_API UString getId() const override;

	VERSO_3D_API void setId(const UString& id) override;

	VERSO_3D_API UString getType() const override;

	VERSO_3D_API static UString getTypeStatic();

	VERSO_3D_API const Vector3f& getPosition() const override;

	VERSO_3D_API void setPosition(const Vector3f& position) override;

	VERSO_3D_API UString toString() const override;

	VERSO_3D_API UString toStringDebug() const override;

public:
	VERSO_3D_API ParticleBehaviour* getParticleBehaviour() const;

	VERSO_3D_API void setParticleBehaviour(ParticleBehaviour* particleBehaviour);

	VERSO_3D_API ParticleSystem* getParticleSystem() const;

	VERSO_3D_API void setParticleSystem(ParticleSystem* particleSystem);

	VERSO_3D_API ParticleEmitter* getParticleEmitter() const;

	VERSO_3D_API void setParticleEmitter(ParticleEmitter* particleEmitter);

	VERSO_3D_API ParticleSequencer* getParticleSequencer() const;

	VERSO_3D_API void setParticleSequencer(ParticleSequencer* particleSequencer);

public:
	VERSO_3D_API size_t getAmount() const;

	VERSO_3D_API void setAmount(size_t amount);
};


inline UString SteadySmoke::getId() const
{
	return id;
}


inline void SteadySmoke::setId(const UString& id)
{
	this->id = id;
}


inline UString SteadySmoke::getType() const
{
	return getTypeStatic();
}

inline UString SteadySmoke::getTypeStatic()
{
	return "SteadySmoke";
}


inline const Vector3f& SteadySmoke::getPosition() const
{
	return particleSystem->getPosition();
}


inline void SteadySmoke::setPosition(const Vector3f& position)
{
	particleSystem->setPosition(position);
}


inline ParticleBehaviour* SteadySmoke::getParticleBehaviour() const
{
	return particleBehaviour;
}


inline void SteadySmoke::setParticleBehaviour(ParticleBehaviour* particleBehaviour)
{
	this->particleBehaviour = particleBehaviour;
}


inline ParticleSystem* SteadySmoke::getParticleSystem() const
{
	return particleSystem;
}


inline void SteadySmoke::setParticleSystem(ParticleSystem* particleSystem)
{
	this->particleSystem = particleSystem;
}


inline ParticleEmitter* SteadySmoke::getParticleEmitter() const
{
	return particleEmitter;
}


inline void SteadySmoke::setParticleEmitter(ParticleEmitter* particleEmitter)
{
	this->particleEmitter = particleEmitter;
}


inline ParticleSequencer* SteadySmoke::getParticleSequencer() const
{
	return particleSequencer;
}


inline void SteadySmoke::setParticleSequencer(ParticleSequencer* particleSequencer)
{
	this->particleSequencer = particleSequencer;
}


inline size_t SteadySmoke::getAmount() const
{
	return amount;
}


inline void SteadySmoke::setAmount(size_t amount)
{
	this->amount = amount;
}


} // End namespace Verso

#endif // End header guard

