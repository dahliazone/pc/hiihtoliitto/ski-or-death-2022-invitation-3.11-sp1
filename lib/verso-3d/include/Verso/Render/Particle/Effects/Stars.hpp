#ifndef VERSO_3D_RENDER_PARTICLE_EFFECTS_STARS_HPP
#define VERSO_3D_RENDER_PARTICLE_EFFECTS_STARS_HPP

#include <Verso/Render/Particle/Effects/ParticleEffect.hpp>
#include <Verso/Render/Particle/Behaviours/SimpleGravity.hpp>
#include <Verso/Render/Particle/Emitters/SimpleRandom.hpp>
#include <Verso/Render/Particle/Emitters/SphericalRandom.hpp>
#include <Verso/Render/Particle/Sequencers/BurstInterval.hpp>

namespace Verso {


class Stars : public ParticleEffect
{
private:
	bool created;
	UString id;
	ParticleBehaviour* particleBehaviour;
	ParticleSystem* particleSystem;
	ParticleEmitter* particleEmitter;
	ParticleSequencer* particleSequencer;

	size_t particleLimit;
	Rangef radiusRange;
	Rangef angleRange;
	Rangef angleVelocityRange;
	float endSize;
	Rangef totalLifeTimeRange;
	float startAlpha;
	size_t burstSize;
	size_t particlesPerSecond;
	bool whiteParticles;

public:
	VERSO_3D_API explicit Stars(const UString& id);

	// \TODO: move operators!

	VERSO_3D_API virtual ~Stars() override;

public:
	VERSO_3D_API void create(IWindowOpengl& window, const UString& textureFileName, bool animated = false);

	VERSO_3D_API void destroy() override;

	VERSO_3D_API bool isCreated() const override;

	VERSO_3D_API void reset() override;

	VERSO_3D_API void calculate(const FrameTimestamp& time) override;

	VERSO_3D_API void render(IWindowOpengl& window, const FrameTimestamp& time, ICamera& camera) override;

	VERSO_3D_API void calculateAndRender(IWindowOpengl& window, const FrameTimestamp& time, ICamera& camera) override;

	VERSO_3D_API bool isDone() const override;

public:
	VERSO_3D_API UString getId() const override;

	VERSO_3D_API void setId(const UString& id) override;

	VERSO_3D_API UString getType() const override;

	VERSO_3D_API static UString getTypeStatic();

	VERSO_3D_API const Vector3f& getPosition() const override;

	VERSO_3D_API void setPosition(const Vector3f& position) override;

	VERSO_3D_API UString toString() const override;

	VERSO_3D_API UString toStringDebug() const override;

public:
	VERSO_3D_API ParticleBehaviour* getParticleBehaviour() const;

	VERSO_3D_API void setParticleBehaviour(ParticleBehaviour* particleBehaviour);

	VERSO_3D_API ParticleSystem* getParticleSystem() const;

	VERSO_3D_API void setParticleSystem(ParticleSystem* particleSystem);

	VERSO_3D_API ParticleEmitter* getParticleEmitter() const;

	VERSO_3D_API void setParticleEmitter(ParticleEmitter* particleEmitter);

	VERSO_3D_API ParticleSequencer* getParticleSequencer() const;

	VERSO_3D_API void setParticleSequencer(ParticleSequencer* particleSequencer);

public:
	VERSO_3D_API size_t getParticleLimit() const;

	VERSO_3D_API void setParticleLimit(size_t particleLimit);

	VERSO_3D_API const Rangef& getRadiusRange() const;

	VERSO_3D_API void setRadiusRange(const Rangef& radiusRange);

	VERSO_3D_API const Rangef& getAngleRange() const;

	VERSO_3D_API void setAngleRange(const Rangef& angleRange);

	VERSO_3D_API const Rangef& getAngleVelocityRange() const;

	VERSO_3D_API void setAngleVelocityRange(const Rangef& angleVelocityRange);

	VERSO_3D_API float getEndSize() const;

	VERSO_3D_API void setEndSize(float endSize);

	VERSO_3D_API const Rangef& getTotalLifeTimeRange() const;

	VERSO_3D_API void setTotalLifeTimeRange(const Rangef& totalLifeTimeRange);

	VERSO_3D_API float getStartAlpha() const;

	VERSO_3D_API void setStartAlpha(float startAlpha);

	VERSO_3D_API size_t getBurstSize() const;

	VERSO_3D_API void setBurstSize(size_t burstSize);

	VERSO_3D_API size_t getParticlesPerSecond() const;

	VERSO_3D_API void setParticlesPerSecond(size_t particlesPerSecond);

	VERSO_3D_API bool isWhiteParticles() const;

	VERSO_3D_API void setWhiteParticles(bool whiteParticles);
};


inline UString Stars::getId() const
{
	return id;
}


inline void Stars::setId(const UString& id)
{
	this->id = id;
}


inline UString Stars::getType() const
{
	return getTypeStatic();
}


inline UString Stars::getTypeStatic()
{
	return "Stars";
}


inline const Vector3f& Stars::getPosition() const
{
	return particleSystem->getPosition();
}


inline void Stars::setPosition(const Vector3f& position)
{
	particleSystem->setPosition(position);
}


inline ParticleBehaviour* Stars::getParticleBehaviour() const
{
	return particleBehaviour;
}


inline void Stars::setParticleBehaviour(ParticleBehaviour* particleBehaviour)
{
	this->particleBehaviour = particleBehaviour;
}


inline ParticleSystem* Stars::getParticleSystem() const
{
	return particleSystem;
}


inline void Stars::setParticleSystem(ParticleSystem* particleSystem)
{
	this->particleSystem = particleSystem;
}


inline ParticleEmitter* Stars::getParticleEmitter() const
{
	return particleEmitter;
}


inline void Stars::setParticleEmitter(ParticleEmitter* particleEmitter)
{
	this->particleEmitter = particleEmitter;
}


inline ParticleSequencer* Stars::getParticleSequencer() const
{
	return particleSequencer;
}


inline void Stars::setParticleSequencer(ParticleSequencer* particleSequencer)
{
	this->particleSequencer = particleSequencer;
}


inline size_t Stars::getParticleLimit() const
{
	return particleLimit;
}


inline void Stars::setParticleLimit(size_t particleLimit)
{
	this->particleLimit = particleLimit;
}


inline const Rangef& Stars::getRadiusRange() const
{
	return radiusRange;
}


inline void Stars::setRadiusRange(const Rangef& radiusRange)
{
	this->radiusRange = radiusRange;
}


inline const Rangef& Stars::getAngleRange() const
{
	return angleRange;
}


inline void Stars::setAngleRange(const Rangef& angleRange)
{
	this->angleRange = angleRange;
}


inline const Rangef& Stars::getAngleVelocityRange() const
{
	return angleVelocityRange;
}


inline void Stars::setAngleVelocityRange(const Rangef& angleVelocityRange)
{
	this->angleVelocityRange = angleVelocityRange;
}


inline float Stars::getEndSize() const
{
	return endSize;
}


inline void Stars::setEndSize(float endSize)
{
	this->endSize = endSize;
}


inline const Rangef& Stars::getTotalLifeTimeRange() const
{
	return totalLifeTimeRange;
}


inline void Stars::setTotalLifeTimeRange(const Rangef& totalLifeTimeRange)
{
	this->totalLifeTimeRange = totalLifeTimeRange;
}


inline float Stars::getStartAlpha() const
{
	return startAlpha;
}


inline void Stars::setStartAlpha(float startAlpha)
{
	this->startAlpha = startAlpha;
}


inline size_t Stars::getBurstSize() const
{
	return burstSize;
}


inline void Stars::setBurstSize(size_t burstSize)
{
	this->burstSize = burstSize;
}


inline size_t Stars::getParticlesPerSecond() const
{
	return particlesPerSecond;
}


inline void Stars::setParticlesPerSecond(size_t particlesPerSecond)
{
	this->particlesPerSecond = particlesPerSecond;
}


inline bool Stars::isWhiteParticles() const
{
	return whiteParticles;
}


inline void Stars::setWhiteParticles(bool whiteParticles)
{
	this->whiteParticles = whiteParticles;
}


} // End namespace Verso

#endif // End header guard

