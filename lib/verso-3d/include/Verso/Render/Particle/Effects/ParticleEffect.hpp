#ifndef VERSO_3D_RENDER_PARTICLE_EFFECTS_PARTICLEEFFECT_HPP
#define VERSO_3D_RENDER_PARTICLE_EFFECTS_PARTICLEEFFECT_HPP

#include <Verso/Display/IWindowOpengl.hpp>
#include <Verso/Render/Camera.hpp>
#include <Verso/Time/FrameTimestamp.hpp>

namespace Verso {


class VERSO_3D_API ParticleEffect
{
public:
	ParticleEffect();
	// \TODO: move operators!
	virtual ~ParticleEffect();

public:
	virtual void destroy() = 0;
	virtual void reset() = 0;

	virtual void calculate(const FrameTimestamp& time) = 0;
	virtual void render(IWindowOpengl& window, const FrameTimestamp& time, ICamera& camera) = 0;
	virtual void calculateAndRender(IWindowOpengl& window, const FrameTimestamp& time, ICamera& camera) = 0;

	virtual bool isCreated() const = 0;
	virtual bool isDone() const = 0;

	virtual UString getId() const = 0;
	virtual void setId(const UString& id) = 0;
	virtual UString getType() const = 0;
	virtual const Vector3f& getPosition() const = 0;
	virtual void setPosition(const Vector3f& position) = 0;
	virtual UString toString() const = 0;
	virtual UString toStringDebug() const = 0;
};


inline ParticleEffect::ParticleEffect()
{
}


inline ParticleEffect::~ParticleEffect()
{
}


} // End namespace Verso

#endif // End header guard

