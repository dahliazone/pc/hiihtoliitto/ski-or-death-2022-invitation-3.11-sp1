#ifndef VERSO_3D_RENDER_PARTICLE_PARTICLESYSTEM_HPP
#define VERSO_3D_RENDER_PARTICLE_PARTICLESYSTEM_HPP

#include <Verso/Render/Particle/Behaviours/ParticleBehaviour.hpp>
#include <Verso/Render/Material/MaterialTextureRgba3d.hpp>
#include <Verso/Display/WindowOpengl.hpp>
#include <Verso/Render/Opengl/BlendMode.hpp>
#include <Verso/Render/Camera/ICamera.hpp>
#include <Verso/Render/Vao/Vao.hpp>

namespace Verso {


/**
 * .
 * \todo support for sorting of particles (maybe in another class?).
 */
class ParticleSystem
{
public:
	VERSO_3D_API static const size_t DEFAULT_PARTICLE_LIMIT;

private:
	bool created;
	UString id;
	Vector3f position;
	ParticleBehaviour* particleBehaviour;
	std::vector<Particle> particles;
	MaterialTextureRgba3d material;
	bool animatedTexture;
	size_t trailSize;
	double trailSaveTime;
	double trailSaveInterval;
	size_t nextParticleIndex;
	size_t firstActiveParticleIndex;
	size_t lastActiveParticleIndex;
	BlendMode blendMode;
	double passedTime;
	Vao vao;

public:
	VERSO_3D_API explicit ParticleSystem(const UString& id);

	ParticleSystem(const ParticleSystem& original) = delete;

	VERSO_3D_API ParticleSystem(ParticleSystem&& original) noexcept;

	ParticleSystem& operator =(const ParticleSystem& original) = delete;

	VERSO_3D_API ParticleSystem& operator =(ParticleSystem&& original) noexcept;

	VERSO_3D_API ~ParticleSystem();

public:
	VERSO_3D_API bool create(size_t particleLimit = DEFAULT_PARTICLE_LIMIT);

	VERSO_3D_API void destroy() VERSO_NOEXCEPT;

	VERSO_3D_API bool isCreated() const;

	VERSO_3D_API void calculate(const FrameTimestamp& time);

	VERSO_3D_API void render(IWindowOpengl& window, const FrameTimestamp& time, ICamera& camera);

	VERSO_3D_API void calculateAndRender(IWindowOpengl& window, const FrameTimestamp& time, ICamera& camera);

public:
	VERSO_3D_API bool isEmpty() const;

	VERSO_3D_API void reset();

	VERSO_3D_API size_t addParticle(
			const Vector3f& position,
			float angle,
			const Vector3f& velocity, float angleVelocity,
			float startSize, float endSize, float totalLifetime,
			const RgbaColorf& color,
			float startAlpha, float endAlpha);

	VERSO_3D_API void removeParticle(size_t index);

public:
	VERSO_3D_API void setTexture(IWindowOpengl& window, const UString& fileName, bool animatedTexture = false);

	VERSO_3D_API void setBlendMode(BlendMode blendMode);

	VERSO_3D_API const Vector3f& getPosition() const;

	VERSO_3D_API void setPosition(const Vector3f& position);

	VERSO_3D_API ParticleBehaviour* getParticleBehaviour() const;

	VERSO_3D_API void setParticleBehaviour(ParticleBehaviour* particleBehaviour);

	VERSO_3D_API size_t getParticleLimit() const;

	VERSO_3D_API void raiseParticleLimit(size_t particleLimit);

	VERSO_3D_API size_t getTrailSize() const;

	// Note: you cannot lower the trail size
	VERSO_3D_API void raiseTrailSize(size_t trailSize);

	VERSO_3D_API double getTrailSaveInterval() const;

	VERSO_3D_API void setTrailSaveInterval(double trailSaveInterval);

private:
	void bindRenderingFlags(IWindowOpengl& window, const FrameTimestamp& time, const ICamera& camera);

	void unbindRenderingFlags(IWindowOpengl& window, const FrameTimestamp& time, const ICamera& camera);

	bool isTimeToSaveTrailLocation(const FrameTimestamp& time);

	void calculateParticle(const FrameTimestamp& time,
						   Particle* p,
						   bool behaviourSet, size_t index, size_t startIndex,
						   size_t endIndex);

	void saveTrailLocations(Particle* p);

	void renderParticleWithTrail(IWindowOpengl& window, const FrameTimestamp& time, const ICamera& camera, Particle* p);

	void renderSingleParticle(
			IWindowOpengl& window, const FrameTimestamp& time, const ICamera& camera, const Vector3f& position,
			float size, float angleZ,
			const RgbaColorf& color);

	void renderSingleParticleAnimated(
			IWindowOpengl& window, const FrameTimestamp& time, const ICamera& camera, const Vector3f& position,
			float size, float angleZ,
			const RgbaColorf& color,
			size_t animationFrame);
};


} // End namespace Verso

#endif // End header guard

