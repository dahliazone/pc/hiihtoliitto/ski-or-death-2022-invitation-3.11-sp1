#ifndef VERSO_3D_RENDER_PARTICLE_EMITTERS_SIMPLERANDOM_HPP
#define VERSO_3D_RENDER_PARTICLE_EMITTERS_SIMPLERANDOM_HPP

#include <Verso/Render/Particle/Emitters/ParticleEmitter.hpp>
#include <Verso/Math/Random.hpp>
#include <Verso/Math/Range.hpp>

namespace Verso {


class SimpleRandom : public ParticleEmitter
{
private:
	Rangef xVelocityRange;
	Rangef yVelocityRange;
	Rangef zVelocityRange;

public:
	SimpleRandom();
	virtual ~SimpleRandom() override;

public:
	virtual void reset() override;
	virtual void emitter(size_t count) override;

	virtual UString toString() const override;
	virtual UString toStringDebug() const override;

	virtual UString getType() const override
	{
		return getTypeStatic();
	}

	static UString getTypeStatic()
	{
		return "SimpleRandom";
	}

public:
	const Rangef& getXVelocityRange() const;
	void setXVelocityRange(const Rangef& xVelocityRange);

	const Rangef& getYVelocityRange() const;
	void setYVelocityRange(const Rangef& yVelocityRange);

	const Rangef& getZVelocityRange() const;
	void setZVelocityRange(const Rangef& zVelocityRange);
};



inline SimpleRandom::SimpleRandom() :
    xVelocityRange(-10.0f, 10.0f),
    yVelocityRange(-10.0f, 10.0f),
    zVelocityRange(-10.0f, 10.0f)
{
}


inline SimpleRandom::~SimpleRandom()
{
}


inline void SimpleRandom::reset()
{
}


inline void SimpleRandom::emitter(size_t count)
{
	ParticleSystem* particleSystem = getParticleSystem();

	for (size_t i=0; i<count; ++i) {
		particleSystem->addParticle(
		    Vector3f(Random::floatRange(xRange),
		             Random::floatRange(yRange),
		             Random::floatRange(zRange)),
		    Random::floatRange(angleRange),
		    Vector3f(Random::floatRange(xVelocityRange),
		             Random::floatRange(yVelocityRange),
		             Random::floatRange(zVelocityRange)),
		    Random::floatRange(angleVelocityRange),
		    Random::floatRange(startSizeRange),
		    Random::floatRange(endSizeRange),
		    Random::floatRange(totalLifeTimeRange),
		    RgbaColorf(Random::floatRange(redRange),
		               Random::floatRange(greenRange),
		               Random::floatRange(blueRange)),
		    Random::floatRange(startAlphaRange),
		    Random::floatRange(endAlphaRange));
	}
}


inline UString SimpleRandom::toString() const
{
	UString str;
	str += "xVelocityRange="+xVelocityRange.toString();
	str += "yVelocityRange="+yVelocityRange.toString();
	str += "zVelocityRange="+zVelocityRange.toString();
	return str;
}


inline UString SimpleRandom::toStringDebug() const
{
	return UString(getType()+"("+toString()+")");
}


inline const Rangef& SimpleRandom::getXVelocityRange() const
{
	return xVelocityRange;
}


inline void SimpleRandom::setXVelocityRange(const Rangef& xVelocityRange)
{
	this->xVelocityRange = xVelocityRange;
}


inline const Rangef& SimpleRandom::getYVelocityRange() const
{
	return yVelocityRange;
}


inline void SimpleRandom::setYVelocityRange(const Rangef& yVelocityRange)
{
	this->yVelocityRange = yVelocityRange;
}


inline const Rangef& SimpleRandom::getZVelocityRange() const
{
	return zVelocityRange;
}


inline void SimpleRandom::setZVelocityRange(const Rangef& zVelocityRange)
{
	this->zVelocityRange = zVelocityRange;
}


} // End namespace Verso

#endif // End header guard

