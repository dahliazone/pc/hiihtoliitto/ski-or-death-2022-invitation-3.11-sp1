#ifndef VERSO_3D_RENDER_PARTICLE_EMITTERS_SPHERICALRANDOM_HPP
#define VERSO_3D_RENDER_PARTICLE_EMITTERS_SPHERICALRANDOM_HPP

#include <Verso/Render/Particle/Emitters/ParticleEmitter.hpp>
#include <Verso/Math/Random.hpp>

namespace Verso {


class SphericalRandom : public ParticleEmitter
{
private:
	Rangef radiusRange;

public:
	SphericalRandom();
	virtual ~SphericalRandom() override;

public:
	virtual void reset() override;
	virtual void emitter(size_t count) override;

	virtual UString toString() const override;
	virtual UString toStringDebug() const override;

	virtual UString getType() const override
	{
		return getTypeStatic();
	}

	static UString getTypeStatic()
	{
		return "SphericalRandom";
	}

public:
	const Rangef& getRadiusRange() const;
	void setRadiusRange(const Rangef& radiusRange);
};


} // End namespace Verso

#endif // End header guard

