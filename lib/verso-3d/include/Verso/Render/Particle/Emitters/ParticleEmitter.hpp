#ifndef VERSO_3D_RENDER_PARTICLE_EMITTERS_PARTICLEEMITTER_HPP
#define VERSO_3D_RENDER_PARTICLE_EMITTERS_PARTICLEEMITTER_HPP

#include <Verso/Render/Particle/ParticleSystem.hpp>
#include <Verso/Math/Range.hpp>

namespace Verso {


class ParticleEmitter
{
protected:
	Rangef xRange;
	Rangef yRange;
	Rangef zRange;
	Rangef angleRange;
	Rangef angleVelocityRange;
	Rangef startSizeRange;
	Rangef endSizeRange;
	Rangef totalLifeTimeRange;
	Rangef redRange;
	Rangef greenRange;
	Rangef blueRange;
	Rangef startAlphaRange;
	Rangef endAlphaRange;

private:
	ParticleSystem* particleSystem;

public:
	ParticleEmitter();
	virtual ~ParticleEmitter();

	virtual void reset() = 0;
	virtual void emitter(size_t count) = 0;

	virtual UString getType() const = 0;
	virtual UString toString() const = 0;
	virtual UString toStringDebug() const = 0;

public:
	ParticleSystem* getParticleSystem() const;
	void setParticleSystem(ParticleSystem* particleSystem);

	const Rangef& getXRange() const;
	void setXRange(const Rangef& xRange);

	const Rangef& getYRange() const;
	void setYRange(const Rangef& yRange);

	const Rangef& getZRange() const;
	void setZRange(const Rangef& zRange);

	const Rangef& getAngleRange() const;
	void setAngleRange(const Rangef& angleRange);

	const Rangef& getAngleVelocityRange() const;
	void setAngleVelocityRange(const Rangef& angleVelocityRange);

	const Rangef& getStartSizeRange() const;
	void setStartSizeRange(const Rangef& startSizeRange);

	const Rangef& getEndSizeRange() const;
	void setEndSizeRange(const Rangef& endSizeRange);

	const Rangef& getTotalLifeTimeRange() const;
	void setTotalLifeTimeRange(const Rangef& totalLifeTimeRange);

	const Rangef& getRedRange() const;
	void setRedRange(const Rangef& redRange);

	const Rangef& getGreenRange() const;
	void setGreenRange(const Rangef& greenRange);

	const Rangef& getBlueRange() const;
	void setBlueRange(const Rangef& blueRange);

	const Rangef& getStartAlphaRange() const;
	void setStartAlphaRange(const Rangef& startAlphaRange);

	const Rangef& getEndAlphaRange() const;
	void setEndAlphaRange(const Rangef& endAlphaRange);
};



inline ParticleEmitter::ParticleEmitter() :
    xRange(0.0f),
    yRange(0.0f),
    zRange(0.0f),
    angleRange(0.0f),
    angleVelocityRange(0.0f),
    startSizeRange(1.0f),
    endSizeRange(1.0f),
    totalLifeTimeRange(1.0f, 3.0f),
    redRange(1.0f),
    greenRange(1.0f),
    blueRange(1.0f),
    startAlphaRange(1.0f),
    endAlphaRange(1.0f),
    particleSystem()
{
}


inline ParticleEmitter::~ParticleEmitter()
{
}


inline ParticleSystem* ParticleEmitter::getParticleSystem() const
{
	return particleSystem;
}


inline void ParticleEmitter::setParticleSystem(ParticleSystem* particleSystem)
{
	this->particleSystem = particleSystem;
}



inline const Rangef& ParticleEmitter::getXRange() const
{
	return xRange;
}


inline void ParticleEmitter::setXRange(const Rangef& xRange)
{
	this->xRange = xRange;
}


inline const Rangef& ParticleEmitter::getYRange() const
{
	return yRange;
}


inline void ParticleEmitter::setYRange(const Rangef& yRange)
{
	this->yRange = yRange;
}


inline const Rangef& ParticleEmitter::getZRange() const
{
	return zRange;
}


inline void ParticleEmitter::setZRange(const Rangef& zRange)
{
	this->zRange = zRange;
}


inline const Rangef& ParticleEmitter::getAngleRange() const
{
	return angleRange;
}


inline void ParticleEmitter::setAngleRange(const Rangef& angleRange)
{
	this->angleRange = angleRange;
}


inline const Rangef& ParticleEmitter::getAngleVelocityRange() const
{
	return angleVelocityRange;
}


inline void ParticleEmitter::setAngleVelocityRange(const Rangef& angleVelocityRange)
{
	this->angleVelocityRange = angleVelocityRange;
}


inline const Rangef& ParticleEmitter::getStartSizeRange() const
{
	return startSizeRange;
}


inline void ParticleEmitter::setStartSizeRange(const Rangef& startSizeRange)
{
	this->startSizeRange = startSizeRange;
}


inline const Rangef& ParticleEmitter::getEndSizeRange() const
{
	return endSizeRange;
}


inline void ParticleEmitter::setEndSizeRange(const Rangef& endSizeRange)
{
	this->endSizeRange = endSizeRange;
}


inline const Rangef& ParticleEmitter::getTotalLifeTimeRange() const
{
	return totalLifeTimeRange;
}


inline void ParticleEmitter::setTotalLifeTimeRange(const Rangef& totalLifeTimeRange)
{
	this->totalLifeTimeRange = totalLifeTimeRange;
}


inline const Rangef& ParticleEmitter::getRedRange() const
{
	return redRange;
}


inline void ParticleEmitter::setRedRange(const Rangef& redRange)
{
	this->redRange = redRange;
}


inline const Rangef& ParticleEmitter::getGreenRange() const
{
	return greenRange;
}


inline void ParticleEmitter::setGreenRange(const Rangef& greenRange)
{
	this->greenRange = greenRange;
}


inline const Rangef& ParticleEmitter::getBlueRange() const
{
	return blueRange;
}


inline void ParticleEmitter::setBlueRange(const Rangef& blueRange)
{
	this->blueRange = blueRange;
}


inline const Rangef& ParticleEmitter::getStartAlphaRange() const
{
	return startAlphaRange;
}


inline void ParticleEmitter::setStartAlphaRange(const Rangef& startAlphaRange)
{
	this->startAlphaRange = startAlphaRange;
}


inline const Rangef& ParticleEmitter::getEndAlphaRange() const
{
	return endAlphaRange;
}


inline void ParticleEmitter::setEndAlphaRange(const Rangef& endAlphaRange)
{
	this->endAlphaRange = endAlphaRange;
}


} // End namespace Verso

#endif // End header guard

