#ifndef VERSO_3D_RENDER_RENDERSTATS_HPP
#define VERSO_3D_RENDER_RENDERSTATS_HPP

#include <Verso/Render/Vao/PrimitiveType.hpp>
#include <cstddef>

namespace Verso {


class VERSO_3D_API RenderStats
{
private: // static
	static int points;
	static int lines;
	static int triangles;
	static int meshes;

public: // static
	static void addPoints(int amount);
	static void addLines(int amount);
	static void addTriangles(int amount);
	static void addPrimitives(const PrimitiveType& primitive, int vertices);
	static void addMeshes(int amount);
	static int getPointsCount();
	static int getLinesCount();
	static int getTrianglesCount();
	static int getMeshesCount();
	static void reset();
	static UString toString();
	static UString toStringDebug();
};


} // End namespace Verso

#endif // End header guard

