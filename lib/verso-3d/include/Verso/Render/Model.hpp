#ifndef VERSO_3D_RENDER_MODEL_HPP
#define VERSO_3D_RENDER_MODEL_HPP

#include <Verso/Render/Mesh.hpp>
#include <Verso/Render/Texture.hpp>
#include <Verso/Render/ShaderProgram.hpp>
#include <Verso/Math/Vertex.hpp>

namespace Verso {

//class aiNode;
//class aiMesh;
//class aiScene;
//class aiMaterial;
//class aiTextureType;


class Model
{
public:
	bool created;
	UString id;
	std::vector<Mesh> meshes;
	std::vector<IMaterial*> materials;
	UString sourceFileName;
	UString directory;

public:
	VERSO_3D_API explicit Model(const UString& id);

	Model(const Model& original) = delete;

	VERSO_3D_API Model(Model&& original) noexcept;

	VERSO_3D_API ~Model();

	Model& operator =(const Model& original) = delete;

	VERSO_3D_API Model& operator =(Model&& original) noexcept;

public:
	VERSO_3D_API bool createFromFileObj(IWindowOpengl& window, const UString& fileName, const UString& materialPath);

	VERSO_3D_API bool createFromFileGltf(IWindowOpengl& window, const UString& fileName);

	VERSO_3D_API bool createFromFileAssimp(IWindowOpengl& window, const UString& fileName);

	VERSO_3D_API void destroy() VERSO_NOEXCEPT;

	VERSO_3D_API bool isCreated() const;

	VERSO_3D_API void render(IWindowOpengl& window);

	VERSO_3D_API void render(IWindowOpengl& window, ShaderProgram& shaderProgram);

	VERSO_3D_API void renderDebugNormals(IWindowOpengl& window, const FrameTimestamp& time, const ICamera& camera);

public: // toString
	VERSO_3D_API UString toString() const;

	VERSO_3D_API UString toStringDebug() const;


	friend std::ostream& operator <<(std::ostream& ost, const Model& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso


#endif // End header guard

