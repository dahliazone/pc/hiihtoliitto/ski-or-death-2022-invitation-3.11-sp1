#ifndef VERSO_3D_RENDER_TEXTUREATLAS_TEXTUREATLAS_HPP
#define VERSO_3D_RENDER_TEXTUREATLAS_TEXTUREATLAS_HPP

#include <Verso/System/UString.hpp>
#include <Verso/Render/TextureAtlas/TextureArea.hpp>
#include <vector>
#include <map>

namespace Verso {


typedef std::uint32_t SubAtlasHandle;
typedef std::uint32_t TextureAreaHandle;


class TextureAtlas
{
private:
	bool created;
	SubAtlasHandle nextSubAtlasHandle;
	std::map<UString, SubAtlasHandle> subAtlasNameToSubAtlasHandle;
	std::map<SubAtlasHandle, UString> subAtlasSourceJsonFileName;
	std::map<SubAtlasHandle, UString> subAtlasSourceTextureFileName;
	std::map<SubAtlasHandle, Texture*> subAtlas;

	TextureAreaHandle nextTextureAreaHandle;
	std::map<SubAtlasHandle, std::map<UString, TextureAreaHandle> > subAtlasAndTextureAreaNameToTextureAreaHandle;
	std::map<TextureAreaHandle, TextureArea> textureAreas;

public:
	VERSO_3D_API TextureAtlas();

	TextureAtlas(const TextureAtlas& original) = delete;

	VERSO_3D_API TextureAtlas(TextureAtlas&& original);

	TextureAtlas& operator =(const TextureAtlas& original) = delete;

	VERSO_3D_API TextureAtlas& operator =(TextureAtlas&& original);

	VERSO_3D_API ~TextureAtlas();

public:
	// Returns the handle of the sub-atlas
	VERSO_3D_API SubAtlasHandle loadTextureCssJson(IWindowOpengl& window, const UString& fileName);

	VERSO_3D_API void destroy();

	VERSO_3D_API bool isCreated() const;

	VERSO_3D_API std::vector<UString> getSubAtlasNames() const;

	VERSO_3D_API std::vector<SubAtlasHandle> getSubAtlasHandles() const;

	VERSO_3D_API SubAtlasHandle getSubAtlasHandle(const UString& subAtlasName) const;

	VERSO_3D_API UString getSubAtlasName(SubAtlasHandle subAtlasHandle) const;

	VERSO_3D_API std::vector<TextureAreaHandle> getTextureAreaHandles(const UString& subAtlasName) const;

	VERSO_3D_API std::vector<UString> getTextureAreaNames(const UString& subAtlasName) const;

	VERSO_3D_API TextureAreaHandle getTextureAreaHandle(const UString& subAtlasName, const UString& textureAreaName) const;

	VERSO_3D_API UString getTextureAreaName(TextureAreaHandle textureAreaHandle) const;

	VERSO_3D_API TextureArea getTextureArea(TextureAreaHandle textureAreaHandle) const;

	VERSO_3D_API TextureArea getTextureArea(const UString& subAtlasName, const UString& textureAreaName) const;

public:
	VERSO_3D_API UString toString() const;

	VERSO_3D_API UString toStringDebug() const;


	friend std::ostream& operator <<(std::ostream& ost, const TextureAtlas& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso

#endif // End header guard

