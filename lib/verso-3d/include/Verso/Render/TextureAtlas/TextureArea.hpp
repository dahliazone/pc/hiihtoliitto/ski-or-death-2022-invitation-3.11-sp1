#ifndef VERSO_3D_RENDER_TEXTUREATLAS_TEXTUREAREA_HPP
#define VERSO_3D_RENDER_TEXTUREATLAS_TEXTUREAREA_HPP

#include <Verso/System/UString.hpp>
#include <Verso/Math/Rect.hpp>
#include <Verso/Render/Texture.hpp>

namespace Verso {


class TextureArea
{
private:
	Texture* texture;
	Rect<float> area;

public:
	VERSO_3D_API TextureArea() :
		texture(nullptr),
		area()
	{
	}

	VERSO_3D_API explicit TextureArea(Texture* texture) :
		texture(texture),
		area()
	{
	}

	VERSO_3D_API TextureArea(Texture* texture, const Rect<float>& area) :
		texture(texture),
		area(area)
	{
	}

	VERSO_3D_API TextureArea(const TextureArea& original) :
		texture(original.texture),
		area(original.area)
	{
	}

	VERSO_3D_API TextureArea(TextureArea&& original) noexcept :
		texture(std::move(original.texture)),
		area(std::move(original.area))
	{
		original.texture = nullptr;
	}


	VERSO_3D_API TextureArea& operator =(const TextureArea& original)
	{
		if (this != &original) {
			texture = original.texture;
			area = original.area;
		}
		return *this;
	}

	VERSO_3D_API TextureArea& operator =(TextureArea&& original) noexcept
	{
		if (this != &original) {
			texture = std::move(original.texture);
			area = std::move(original.area);

			original.texture = nullptr;
		}
		return *this;
	}

	VERSO_3D_API ~TextureArea()
	{
	}

public:
	VERSO_3D_API Texture* getTexture() const
	{
		return texture;
	}

	VERSO_3D_API const Rect<float>& getArea() const
	{
		return area;
	}
};


} // End namespace Verso

#endif // End header guard

