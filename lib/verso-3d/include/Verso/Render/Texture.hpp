#ifndef VERSO_3D_RENDER_TEXTURE_HPP
#define VERSO_3D_RENDER_TEXTURE_HPP

#include <Verso/Render/Texture/TextureParameters.hpp>
#include <Verso/Image/ImageSaveFormat.hpp>
//#include <Verso/Render/Opengl.hpp>
//#include <Verso/System/InitWrapperSdl2.hpp>
//#include <Verso/Math/Vector2.hpp>
//#include <Verso/Math/AspectRatio.hpp>
//#include <Verso/System/UString.hpp>
//#include <cstdint>

// \TODO: support for GL_TEXTURE_BORDER_COLOR?
// \TODO: support for GL_TEXTURE_PRIORITY?
// \TODO: support for float textures (and depth textures)
// \TODO: maybe support for generic types of texture data

namespace Verso {


class IWindowOpengl;


class Texture
{
private: // member variables
	IWindowOpengl* window;
	bool created;
	GLuint handle;
	Vector2i resolution;
	TextureParameters parameters;
	std::uint8_t* pixelData;
	TexturePixelFormat pixelDataFormat;
	UString sourceFileName;
	AspectRatio aspectRatioTarget;

public:
	VERSO_3D_API Texture();

	Texture(const Texture& original) = delete;

	VERSO_3D_API Texture(Texture&& original) noexcept;

	Texture& operator =(const Texture& original) = delete;

	VERSO_3D_API Texture& operator =(Texture&& original) noexcept;

	VERSO_3D_API ~Texture();

public:
	VERSO_3D_API bool createEmpty(IWindowOpengl& window,
								  const Vector2i& resolution,
								  const TextureParameters& parameters,
								  const TexturePixelFormat& cpuRamPixelFormat = TexturePixelFormat::Unset,
								  bool keepCpuRamCopy = false,
								  const AspectRatio& aspectRatioTarget = AspectRatio());

	VERSO_3D_API bool createCopyBuffer(IWindowOpengl& window,
									   const Vector2i& resolution,
									   const TextureParameters& parameters,
									   std::uint8_t* buffer,
									   const TexturePixelFormat& cpuRamPixelFormat,
									   bool keepCpuRamCopy = false,
									   const AspectRatio& aspectRatioTarget = AspectRatio());

	VERSO_3D_API bool createFromFile(IWindowOpengl& window,
									 const UString& fileName,
									 const TextureParameters& parameters,
									 const TexturePixelFormat& cpuRamPixelFormat = TexturePixelFormat::Unset,
									 bool keepCpuRamCopy = false,
									 const AspectRatio& aspectRatioTarget = AspectRatio());

private:
	void reserveGpuHandleAndSetup(IWindowOpengl& window, const Vector2i& resolution,
								  const TextureParameters& parameters,
								  const AspectRatio& aspectRatioTarget);

	size_t allocatePixelData(const TexturePixelFormat& cpuRamPixelFormat);

	void updateGpuTexture(const std::uint8_t* bufferPixelData,
						  const TexturePixelFormat& bufferTexturePixelFormat,
						  const TexturePixelFormat& internalTexturePixelFormat) const;

public:
	VERSO_3D_API void destroy() VERSO_NOEXCEPT;

	// Note: if you pass this method when binding OpenGL textures then call Texture::unbind() before using this.
	VERSO_3D_API void bind();

	VERSO_3D_API void unbind();

	VERSO_3D_API void resize(const Vector2i& newResolution);

	VERSO_3D_API void updateFromInternal();

	VERSO_3D_API void update(std::uint8_t* pixelData, int x, int y, int width, int height,
							 const TexturePixelFormat& pixelFormat = TexturePixelFormat::Unset);

	VERSO_3D_API bool saveToFile(const UString& fileName, const ImageSaveFormat& imageSaveFormat, int mipmapLevel = 0) const;

public: // getters & setters
	VERSO_3D_API bool isCreated() const;

	VERSO_3D_API GLuint getHandle() const;

	VERSO_3D_API Vector2i getResolutioni() const;

	VERSO_3D_API Vector2f getResolutionf() const;

	VERSO_3D_API const TextureParameters& getParameters() const;

	VERSO_3D_API std::uint8_t* getPixelData();

	VERSO_3D_API TexturePixelFormat getPixelDataFormat() const;

	VERSO_3D_API const UString& getSourceFileName() const;

	VERSO_3D_API void setMinFilter(const MinFilter& minFilter);

	VERSO_3D_API void setMagFilter(const MagFilter& magFilter);

	VERSO_3D_API void setWrapStyle(const WrapStyle& wrapStyleS, const WrapStyle& wrapStyleT);

	VERSO_3D_API AspectRatio getAspectRatioTarget() const;

	VERSO_3D_API void setAspectRatioTarget(const AspectRatio& aspectRatio);

	VERSO_3D_API AspectRatio getAspectRatioActual() const;

public: // static
	VERSO_3D_API bool isSupportedByFileExtension(const UString& fileName);

public: // toString
	VERSO_3D_API UString toString(const UString& newLinePadding) const;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const Texture& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso


#endif // End header guard
