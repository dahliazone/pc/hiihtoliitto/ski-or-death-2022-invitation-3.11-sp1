#ifndef VERSO_3D_RENDER_SPRITE3DANIMATION_HPP
#define VERSO_3D_RENDER_SPRITE3DANIMATION_HPP

#include <Verso/System/UString.hpp>
#include <Verso/Math/Rect.hpp>
#include <Verso/Render/Sprite/AnimationType.hpp>
#include <Verso/verso-3d-common.hpp>

namespace Verso {


class Sprite3dAnimation
{
private:
	AnimationType animationType;
	std::vector<float> frameTimes;
	std::vector<Rectf> frameUvs;
	float animationLengthSeconds;

public:
	VERSO_3D_API Sprite3dAnimation();

	VERSO_3D_API Sprite3dAnimation(
			const AnimationType& animationType,
			const std::vector<float>& frameTimes = std::vector<float>(),
			const std::vector<Rectf>& frameUvs = std::vector<Rectf>());

	VERSO_3D_API Sprite3dAnimation(const Sprite3dAnimation& original);

	VERSO_3D_API Sprite3dAnimation(Sprite3dAnimation&& original);

	VERSO_3D_API Sprite3dAnimation& operator =(const Sprite3dAnimation& original);

	VERSO_3D_API Sprite3dAnimation& operator =(Sprite3dAnimation&& original);

	VERSO_3D_API virtual ~Sprite3dAnimation();

public:
	VERSO_3D_API AnimationType getAnimationType() const;

	VERSO_3D_API void setAnimationType(const AnimationType& animationType);

	VERSO_3D_API Rectf getFrameUvBySeconds(float seconds) const;

	VERSO_3D_API float getFrameTime(int frameIndex) const;

	VERSO_3D_API Rectf getFrameUv(int frameIndex) const;

	VERSO_3D_API void setFrames(
			const std::vector<float>& frameTimes,
			const std::vector<Rectf>& frameUvs);

	VERSO_3D_API void clearFrames();

	VERSO_3D_API void addFrame(float frameTime, const Rectf& frameUv);

private:
	VERSO_3D_API void updateAnimationLengthSeconds();

	VERSO_3D_API Rectf calculateFrameUvBySeconds(float seconds) const;

public: // toString
	VERSO_3D_API virtual UString toString() const;

	VERSO_3D_API virtual UString toStringDebug() const;


	friend std::ostream& operator <<(std::ostream& ost, const Sprite3dAnimation& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso


#endif // End header guard

