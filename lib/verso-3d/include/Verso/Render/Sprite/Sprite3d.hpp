#ifndef VERSO_3D_RENDER_SPRITE3D_HPP
#define VERSO_3D_RENDER_SPRITE3D_HPP

#include <Verso/Render/Texture.hpp>
#include <Verso/Display/IWindowOpengl.hpp>
#include <Verso/Time/FrameTimestamp.hpp>
#include <Verso/Render/Camera/ICamera.hpp>
#include <Verso/Render/ShaderProgram.hpp>
#include <Verso/Render/Sprite/Sprite3dAnimation.hpp>
#include <Verso/GrinderKit/Params/Sprite3dAnimationParam.hpp>

namespace Verso {


class Sprite3d
{
private:
	bool created;
	RefScale refScale;
	Vector3f position;
	Vector2f relSize;
	// Eular Angles
	Degree yaw;
	Degree pitch;
	Degree roll;
	Vector2f relOffset;
	Texture* texture;
	float alpha;
	Vector2f spriteUvSize;
	int currentAnimationIndex;
	std::vector<Sprite3dAnimation> animations;
	std::vector<Sprite3d*> children;

public:
	VERSO_3D_API Sprite3d();

	VERSO_3D_API Sprite3d(const Sprite3d& original);

	VERSO_3D_API Sprite3d(Sprite3d&& original) noexcept;

	VERSO_3D_API Sprite3d& operator =(const Sprite3d& original);

	VERSO_3D_API Sprite3d& operator =(Sprite3d&& original) noexcept;

	VERSO_3D_API virtual ~Sprite3d();

public:
	VERSO_3D_API void createFromTexture(Texture* texture);

	VERSO_3D_API void destroy() VERSO_NOEXCEPT;

	VERSO_3D_API bool isCreated() const;

	VERSO_3D_API void setAnimationsFromTexture(
			const std::vector<Sprite3dAnimationParam>& animationParams);

	VERSO_3D_API void setAnimationsFromTexture(
			int spritesPerColumn, int animationsPerRow,
			const std::vector<AnimationType>& animationTypes,
			const std::vector<int>& animationFrameCounts,
			const std::vector<float>& staticAnimationTimesPerAnimation);

	VERSO_3D_API void setAnimationsFromTexture(
			int spritesPerColumn, int animationsPerRow,
			const std::vector<AnimationType>& animationTypes,
			const std::vector<int>& animationFrameCounts,
			const std::vector< std::vector<float> >& animationTimes);

	VERSO_3D_API int getAnimationsCount() const;

	VERSO_3D_API void addAnimation(const Sprite3dAnimation& animation);

	VERSO_3D_API void clearAnimations();

	VERSO_3D_API int getCurrentAnimationIndex() const;

	VERSO_3D_API void disableAnimationIndex();

	VERSO_3D_API void setCurrentAnimationIndex(int index);

	VERSO_3D_API void render(IWindowOpengl& window, const FrameTimestamp& time, const ICamera& camera);

	VERSO_3D_API void render(IWindowOpengl& window, const FrameTimestamp& time, const ICamera& camera, const Matrix4x4f& matrix, const Vector2f& scale);


public:
	virtual RefScale getRefScale() const {
		return refScale;
	}


	virtual void setRefScale(RefScale refScale) {
		this->refScale = refScale;
	}


	virtual Vector3f getPosition() {
		return position;
	}


	virtual void setPosition(const Vector3f& position) {
		this->position = position;
	}


	virtual Vector2f getRelSize() {
		return relSize;
	}


	virtual void setRelSize(const Vector2f& relSize) {
		this->relSize = relSize;
	}


	virtual Degree getYaw() {
		return yaw;
	}


	virtual void setYaw(Degree yaw) {
		this->yaw = yaw;
	}


	virtual Degree getPitch() {
		return pitch;
	}


	virtual void setPitch(Degree pitch) {
		this->pitch = pitch;
	}


	virtual Degree getRoll() {
		return roll;
	}


	virtual void setRoll(Degree roll) {
		this->roll = roll;
	}


	virtual Vector2f getRelOffset() {
		return relOffset;
	}


	virtual void setRelOffset(const Vector2f& relOffset) {
		this->relOffset = relOffset;
	}


	virtual float getAlpha() {
		return alpha;
	}


	virtual void setAlpha(float alpha) {
		this->alpha = alpha;
	}


	virtual void addChild(Sprite3d* child)
	{
		children.push_back(child);
	}

public: // toString
	VERSO_3D_API virtual UString toString(const UString& newLinePadding) const;

	VERSO_3D_API virtual UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const Sprite3d& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso


#endif // End header guard

