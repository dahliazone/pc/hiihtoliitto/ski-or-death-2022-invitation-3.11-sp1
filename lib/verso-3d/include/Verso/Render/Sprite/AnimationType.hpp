#ifndef VERSO_3D_RENDER_SPRITE_ANIMATIONTYPE_HPP
#define VERSO_3D_RENDER_SPRITE_ANIMATIONTYPE_HPP

#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>

namespace Verso {


enum class AnimationType : uint32_t
{
	Unset = 0,
	Once = 2,    // Play animation once
	Loop = 3,    // Loop animation
	Default = 3
};


inline UString animationTypeToString(const AnimationType& animationType)
{
	switch (animationType)
	{
	case AnimationType::Unset:
		return "Unset";
	case AnimationType::Once:
		return "Once";
	case AnimationType::Loop:
		return "Loop";
	default:
		return "Unknown value";
	}
}


// returns AnimationType::Unset for any erroneuos string
inline AnimationType stringToAnimationType(const UString& str)
{
	if (str.equals("Once")) {
		return AnimationType::Once;
	}
	else if (str.equals("Loop")) {
		return AnimationType::Loop;
	}
	else {
		return AnimationType::Unset;
	}
}


} // End namespace Verso

#endif // End header guard
