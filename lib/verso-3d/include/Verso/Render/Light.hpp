﻿#ifndef VERSO_3D_RENDER_LIGHT_HPP
#define VERSO_3D_RENDER_LIGHT_HPP


#include <Verso/Render/Light/Attenuation.hpp>
#include <Verso/Render/Light/DirectionalLight.hpp>
#include <Verso/Render/Light/PointLight.hpp>
#include <Verso/Render/Light/SpotLight.hpp>


#endif // End header guard

