#ifndef VERSO_3D_RENDER_MESH_HPP
#define VERSO_3D_RENDER_MESH_HPP

#include <Verso/Render/Vao.hpp>
#include <Verso/Render/Material/IMaterial.hpp>

namespace Verso {


class IWindowOpengl;
class Texture;
class ShaderProgram;


class Mesh
{
public:
	Vao vao;
	IMaterial* material;
	std::vector<Texture*> textures;

public:
	VERSO_3D_API explicit Mesh(const UString& id);

	Mesh(const Mesh& original) = delete;

	VERSO_3D_API Mesh(Mesh&& original) noexcept;

	Mesh& operator =(const Mesh& original) = delete;

	VERSO_3D_API Mesh& operator =(Mesh&& original) noexcept;

	VERSO_3D_API ~Mesh();

public:
	VERSO_3D_API void create(PrimitiveType primitiveType);

	VERSO_3D_API void destroy() VERSO_NOEXCEPT;

	VERSO_3D_API void render(IWindowOpengl& window);

	VERSO_3D_API void render(IWindowOpengl& window, ShaderProgram& shaderProgram);

public: // toString
	VERSO_3D_API UString toString(const UString& newLinePadding) const;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const Mesh& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso


#endif // End header guard

