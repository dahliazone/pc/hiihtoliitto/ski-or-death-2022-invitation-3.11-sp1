#ifndef VERSO_3D_RENDER_CAMERA_ICAMERA_HPP
#define VERSO_3D_RENDER_CAMERA_ICAMERA_HPP

#include <Verso/verso-3d-common.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/Math/Matrix4x4f.hpp>
#include <Verso/Render/Camera/ProjectionType.hpp>
#include <Verso/Math/Range.hpp>
#include <Verso/GrinderKit/Params/CameraParam.hpp>

namespace Verso {


class IWindowOpengl;


class ICamera
{
public:
	VERSO_3D_API virtual ~ICamera();

public: // interface ICamera
	virtual bool create(IWindowOpengl* window, const UString& uniqueId, bool useDrawableResolution) = 0;

	virtual void destroy() = 0;

	virtual void set(const CameraParam& cameraParam, float seconds = 0.0f) = 0;

	virtual UString getUniqueId() const = 0;

	virtual Vector3f getPosition() const = 0;

	virtual void setPosition(const Vector3f& position) = 0;

	virtual void movePosition(const Vector3f& delta, float deltaTime) = 0;

	virtual Matrix4x4f getViewMatrix() const = 0;

	virtual Matrix4x4f getProjectionMatrix() const = 0;

	virtual ProjectionType getProjectionType() const = 0;

	virtual void setProjectionOrthographicRelative(
			float left = -1.0f, float right = 1.0f, float top = 1.0f, float bottom = 1.0f,
			const Rangef& nearFarPlane = Rangef(0.1f, 100.0f)) = 0;

	virtual void setProjectionOrthographic(
			const Rangef& nearFarPlane = Rangef(0.1f, 100.0f)) = 0;

	virtual void setProjectionPerspective(
			Degree fovY, const Rangef& nearFarPlane = Rangef(0.1f, 100.0f)) = 0;

	virtual float getFovY() const = 0;

	virtual void setFovY(float fovY) = 0;

	virtual Rangef getNearFarPlane() const = 0;

	virtual void setNearFarPlane(const Rangef& nearFarPlane) = 0;

	virtual float getOrthographicZoomLevel() const = 0;

	virtual void setOrthographicZoomLevel(float zoomLevel) = 0;

	virtual void moveOrthographicZoomLevel(float delta, float deltaTime) = 0;

	virtual Vector2f screenToViewportPoint(
			const Vector2f& screenPointPixels, const Vector2i& viewportResolution) const = 0;

	virtual Vector3f screenToViewportPoint(
			const Vector3f& screenPointPixels, const Vector2i& viewportResolution) const = 0;

	virtual Vector2f viewportToScreenPoint(
			const Vector2f& viewportPointRelative, const Vector2i& viewportResolution) const = 0;

	virtual Vector3f viewportToScreenPoint(
			const Vector3f& viewportPointRelative, const Vector2i& viewportResolution) const = 0;

	virtual void onResize() = 0;

public: // toString (interface ICamera)
	virtual UString toString(const UString& newLinePadding) const = 0;

	virtual UString toStringDebug(const UString& newLinePadding) const = 0;

public: // toString
	friend std::ostream& operator <<(std::ostream& ost, const ICamera& right)
	{
		return ost << right.toString("");
	}
};



} // End namespace Verso

#endif // End header guard

