#ifndef VERSO_3D_RENDER_CAMERA_CAMERATARGET_HPP
#define VERSO_3D_RENDER_CAMERA_CAMERATARGET_HPP

#include <Verso/Render/Camera/ICamera.hpp>
#include <Verso/Math/Degree.hpp>

namespace Verso {


class CameraTarget : public ICamera
{
private:
	IWindowOpengl* window;
	UString uniqueId;
	bool useDrawableResolution;

	Vector3f position;
	Vector3f target;
	Vector3f desiredUp;
	Matrix4x4f projection;
	ProjectionType projectionType;
	Degree fovY;
	Rangef nearFarPlane;
	bool orthographicIsRelative;
	float orthographicZoomLevel;
	float orthographicLeft;
	float orthographicRight;
	float orthographicTop;
	float orthographicBottom;

	Degree orthographicRotation;

public:
	VERSO_3D_API CameraTarget();

	CameraTarget(const CameraTarget& original) = delete;

	VERSO_3D_API CameraTarget(CameraTarget&& original) noexcept;

	CameraTarget& operator =(const CameraTarget& original) = delete;

	VERSO_3D_API virtual CameraTarget& operator =(CameraTarget&& original) noexcept;

	VERSO_3D_API virtual ~CameraTarget() override;

	VERSO_3D_API virtual void reset(const Vector3f& position = Vector3f(0.0f, 0.0f, 0.0f),
									const Vector3f& target = Vector3f(0.0f, 0.0f, 0.0f),
									const Vector3f& desiredUp = Vector3f::up());

public: // interface ICamera
	VERSO_3D_API virtual bool create(IWindowOpengl* window, const UString& uniqueId, bool useDrawableResolution) override;

	VERSO_3D_API virtual void destroy() override;

	VERSO_3D_API virtual void set(const CameraParam& cameraParam, float seconds = 0.0f) override;

	VERSO_3D_API virtual UString getUniqueId() const override;

	VERSO_3D_API virtual Vector3f getPosition() const override;

	VERSO_3D_API virtual void setPosition(const Vector3f& position) override;

	VERSO_3D_API virtual void movePosition(const Vector3f& delta, float deltaTime) override;

	VERSO_3D_API virtual Matrix4x4f getViewMatrix() const override;

	VERSO_3D_API virtual Matrix4x4f getProjectionMatrix() const override;

	VERSO_3D_API virtual ProjectionType getProjectionType() const override;

	VERSO_3D_API virtual void setProjectionOrthographicRelative(
			float left = -1.0f, float right = 1.0f, float top = -1.0f, float bottom = 1.0f,
			const Rangef& nearFarPlane = Rangef(0.1f, 100.0f)) override;

	VERSO_3D_API virtual void setProjectionOrthographic(const Rangef& nearFarPlane = Rangef(0.1f, 100.0f)) override;

	VERSO_3D_API virtual void setProjectionPerspective(Degree fovY, const Rangef& nearFarPlane = Rangef(0.1f, 100.0f)) override;

	VERSO_3D_API virtual float getFovY() const override;

	VERSO_3D_API virtual void setFovY(float fovY) override;

	VERSO_3D_API virtual Rangef getNearFarPlane() const override;

	VERSO_3D_API virtual void setNearFarPlane(const Rangef& nearFarPlane) override;

	VERSO_3D_API virtual float getOrthographicZoomLevel() const override;

	VERSO_3D_API virtual void setOrthographicZoomLevel(float zoomLevel) override;

	VERSO_3D_API virtual void moveOrthographicZoomLevel(float delta, float deltaTime) override;

	VERSO_3D_API virtual Vector2f screenToViewportPoint(
			const Vector2f& screenPointPixels, const Vector2i& viewportResolution) const override;

	VERSO_3D_API virtual Vector3f screenToViewportPoint(
			const Vector3f& screenPointPixels, const Vector2i& viewportResolution) const override;

	VERSO_3D_API virtual Vector2f viewportToScreenPoint(
			const Vector2f& viewportPointRelative, const Vector2i& viewportResolution) const override;

	VERSO_3D_API virtual Vector3f viewportToScreenPoint(
			const Vector3f& viewportPointRelative, const Vector2i& viewportResolution) const override;

	VERSO_3D_API virtual void onResize() override;

public:
	VERSO_3D_API virtual const Vector3f& getTarget() const;

	VERSO_3D_API virtual void setTarget(const Vector3f& target);

	VERSO_3D_API virtual void moveTarget(const Vector3f& delta);

	VERSO_3D_API virtual void movePositionAndTarget(const Vector3f& delta, float deltaTime);

	VERSO_3D_API virtual const Vector3f& getDesiredUp() const;

	VERSO_3D_API virtual void setDesiredUp(const Vector3f& desiredUp);

	VERSO_3D_API virtual Degree getOrthographicRotation() const;

	VERSO_3D_API virtual void setOrthographicRotation(Degree orthographicRotation);

	VERSO_3D_API virtual void changeOrthographicRotation(Degree delta, float deltaTime);

public: // toString (interface ICamera)
	VERSO_3D_API UString toString(const UString& newLinePadding) const override;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const override;
};


} // End namespace Verso

#endif // End header guard

