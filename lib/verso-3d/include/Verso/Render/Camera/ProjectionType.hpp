#ifndef VERSO_3D_RENDER_CAMERA_PROJECTIONTYPE_HPP
#define VERSO_3D_RENDER_CAMERA_PROJECTIONTYPE_HPP

#include <Verso/System/UString.hpp>

namespace Verso {


enum class ProjectionType : std::uint8_t
{
	Unset = 0,
	Orthographic,
	Perspective,
};


inline UString projectionTypeToString(const ProjectionType& projectionType)
{
	switch (projectionType) {
	case ProjectionType::Unset:
		return "Unset";
	case ProjectionType::Orthographic:
		return "Orthographic";
	case ProjectionType::Perspective:
		return "Perspective";
	default:
		return "Unknown value";
	}
}


inline ProjectionType stringToProjectionType(const UString& str)
{
	if (str.equals("Orthographic")) {
		return ProjectionType::Orthographic;
	}
	else if (str.equals("Perspective")) {
		return ProjectionType::Perspective;
	}
	else {
		return ProjectionType::Unset;
	}
}


} // End namespace Verso

#endif // End header guard

