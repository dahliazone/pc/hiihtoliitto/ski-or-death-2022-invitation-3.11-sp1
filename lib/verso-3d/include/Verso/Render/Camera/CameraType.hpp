#ifndef VERSO_3D_CAMERA_CAMERATYPE_HPP
#define VERSO_3D_CAMERA_CAMERATYPE_HPP

#include <Verso/System/UString.hpp>
#include <cstdint>

namespace Verso {


enum class CameraType : std::int8_t {
	Unset = 0,
	Arcball,
	Chase,
	Fps,
	Target
};


inline UString cameraTypeToString(const CameraType& cameraType)
{
	if (cameraType ==  CameraType::Arcball)
		return "Arcball";
	else if (cameraType == CameraType::Chase)
		return "Chase";
	else if (cameraType == CameraType::Fps)
		return "Fps";
	else if (cameraType == CameraType::Target)
		return "Target";
	else if (cameraType == CameraType::Unset)
		return "Unset";
	else
		return "Unknown value";
}


inline CameraType stringToCameraType(const UString& str)
{
	if (str.equals("Arcball"))
		return CameraType::Arcball;
	else if (str.equals("Chase"))
		return CameraType::Chase;
	else if (str.equals("Fps"))
		return CameraType::Fps;
	else if (str.equals("Target"))
		return CameraType::Target;
	else
		return CameraType::Unset;
}


} // End namespace Verso

#endif // End header guard

