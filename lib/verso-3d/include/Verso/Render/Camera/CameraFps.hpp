#ifndef VERSO_3D_RENDER_CAMERA_CAMERAFPS_HPP
#define VERSO_3D_RENDER_CAMERA_CAMERAFPS_HPP

#include <Verso/Render/Camera/ICamera.hpp>
#include <Verso/Math/Degree.hpp>
#include <Verso/Math/Vector3.hpp>
#include <Verso/System/Logger.hpp>

namespace Verso {


class CameraFps : public ICamera
{
private:
	IWindowOpengl* window;
	UString uniqueId;
	bool useDrawableResolution;

	Vector3f position;
	Vector3f front;
	Vector3f up;
	Vector3f right;
	Vector3f worldUp;

	// Eular Angles
	Degree yaw;
	Degree pitch;
	//Degree roll;

	// Camera options
	float mouseSensitivity;
	Degree mouseFov;

	Matrix4x4f projection;
	ProjectionType projectionType;
	Degree fovY;

	Rangef nearFarPlane;
	bool orthographicIsRelative;
	float orthographicZoomLevel;
	float orthographicLeft;
	float orthographicRight;
	float orthographicTop;
	float orthographicBottom;

public:
	VERSO_3D_API CameraFps();

	CameraFps(const CameraFps& original) = delete;

	VERSO_3D_API CameraFps(CameraFps&& original) noexcept;

	CameraFps& operator =(const CameraFps& original) = delete;

	VERSO_3D_API virtual CameraFps& operator =(CameraFps&& original) noexcept;

	VERSO_3D_API virtual ~CameraFps() override;

	VERSO_3D_API virtual void reset(const Vector3f& position = Vector3f(0.0f, 0.0f, 0.0f),
									const Vector3f& up = Vector3f::up(), Degree yaw = -90.0f, Degree pitch = 0.0f);

public: // interface ICamera
	VERSO_3D_API virtual bool create(IWindowOpengl* window, const UString& uniqueId, bool useDrawableResolution = false) override;

	VERSO_3D_API virtual void destroy() override;

	VERSO_3D_API virtual void set(const CameraParam& cameraParam, float seconds = 0.0f) override;

	VERSO_3D_API virtual UString getUniqueId() const override;

	VERSO_3D_API virtual Vector3f getPosition() const override;

	VERSO_3D_API virtual void setPosition(const Vector3f& position) override;

	// x=left/right velocity, y=up/down, z=forward/backward
	VERSO_3D_API virtual void movePosition(const Vector3f& delta, float deltaTime) override;

	VERSO_3D_API virtual Matrix4x4f getViewMatrix() const override;

	VERSO_3D_API virtual Matrix4x4f getProjectionMatrix() const override;

	VERSO_3D_API virtual ProjectionType getProjectionType() const override;

	VERSO_3D_API virtual void setProjectionOrthographicRelative(
			float left = -1.0f, float right = 1.0f, float top = 1.0f, float bottom = 1.0f,
			const Rangef& nearFarPlane = Rangef(0.1f, 100.0f)) override;

	VERSO_3D_API virtual void setProjectionOrthographic(const Rangef& nearFarPlane = Rangef(0.1f, 100.0f)) override;

	VERSO_3D_API virtual void setProjectionPerspective(Degree fovY, const Rangef& nearFarPlane = Rangef(0.1f, 100.0f)) override;

	VERSO_3D_API virtual float getFovY() const override;

	VERSO_3D_API virtual void setFovY(float fovY) override;

	VERSO_3D_API virtual Rangef getNearFarPlane() const override;

	VERSO_3D_API virtual void setNearFarPlane(const Rangef& nearFarPlane) override;

	VERSO_3D_API virtual float getOrthographicZoomLevel() const override;

	VERSO_3D_API virtual void setOrthographicZoomLevel(float zoomLevel) override;

	VERSO_3D_API virtual void moveOrthographicZoomLevel(float delta, float deltaTime) override;

	VERSO_3D_API virtual Vector2f screenToViewportPoint(
			const Vector2f& screenPointPixels, const Vector2i& viewportResolution) const override;

	VERSO_3D_API virtual Vector3f screenToViewportPoint(
			const Vector3f& screenPointPixels, const Vector2i& viewportResolution) const override;

	VERSO_3D_API virtual Vector2f viewportToScreenPoint(
			const Vector2f& viewportPointRelative, const Vector2i& viewportResolution) const override;

	VERSO_3D_API virtual Vector3f viewportToScreenPoint(
			const Vector3f& viewportPointRelative, const Vector2i& viewportResolution) const override;

	VERSO_3D_API virtual void onResize() override;

public:
	// @return x=yaw in degrees, y=pitch in degrees, z=roll in degrees (always zero)
	VERSO_3D_API Vector3f getOrientation() const;

	VERSO_3D_API void setOrientation(Degree yaw, Degree pitch, bool constrainPitch = true);

	// Processes input received from a mouse input system. Expects the offset value in both the x and y direction.
	VERSO_3D_API void processMouseMovement(float xoffset, float yoffset, bool constrainPitch = true);

	// Processes input received from a mouse scroll-wheel event. Only requires input on the vertical wheel-axis
	VERSO_3D_API void processMouseScroll(float yoffset);

private:
	VERSO_3D_API void updateCameraVectors();

public: // toString (interface ICamera)
	VERSO_3D_API UString toString(const UString& newLinePadding) const override;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const override;
};


} // End namespace Verso

#endif // End header guard

