#ifndef VERSO_3D_RENDER_CAMERA_CAMERACHASE_HPP
#define VERSO_3D_RENDER_CAMERA_CAMERACHASE_HPP

#include <Verso/Render/Camera/CameraTarget.hpp>

namespace Verso {


class CameraChase : public CameraTarget
{
public:
	VERSO_3D_API CameraChase();

	CameraChase(const CameraChase& original) = delete;

	VERSO_3D_API CameraChase(CameraChase&& original) noexcept;

	CameraChase& operator =(const CameraChase& original) = delete;

	VERSO_3D_API virtual CameraChase& operator =(CameraChase&& original) noexcept;

	VERSO_3D_API virtual ~CameraChase() override;

	// \TODO: implement reset(*)
	//VERSO_3D_API virtual void reset(const Vector3f& position = Vector3f(0.0f, 0.0f, 0.0f),
	//								const Vector3f& target = Vector3f(0.0f, 0.0f, 0.0f),
	//								const Vector3f& desiredUp = Vector3f::up());

public: // interface ICamera
	//VERSO_3D_API virtual bool create(IWindowOpengl* window, const UString& uniqueId) override;

	//VERSO_3D_API virtual void destroy() override;

	//VERSO_3D_API virtual UString getUniqueId() const override; // already implemented by CameraTarget

	VERSO_3D_API virtual void set(const CameraParam& cameraParam, float seconds = 0.0f) override;

	VERSO_3D_API virtual Vector3f getPosition() const override;

	VERSO_3D_API virtual void setPosition(const Vector3f& position) override;

	VERSO_3D_API virtual void movePosition(const Vector3f& delta, float deltaTime) override;

	VERSO_3D_API virtual Matrix4x4f getViewMatrix() const override;

	VERSO_3D_API virtual Matrix4x4f getProjectionMatrix() const override;

	VERSO_3D_API virtual ProjectionType getProjectionType() const override;

	VERSO_3D_API virtual void setProjectionOrthographicRelative(
			float left = -1.0f, float right = 1.0f, float top = 1.0f, float bottom = 1.0f,
			const Rangef& nearFarPlane = Rangef(0.1f, 100.0f)) override;

	VERSO_3D_API virtual void setProjectionOrthographic(const Rangef& nearFarPlane = Rangef(0.1f, 100.0f)) override;

	VERSO_3D_API virtual void setProjectionPerspective(Degree fovY, const Rangef& nearFarPlane = Rangef(0.1f, 100.0f)) override;

	VERSO_3D_API virtual float getFovY() const override;

	VERSO_3D_API virtual void setFovY(float fovY) override;

	VERSO_3D_API virtual Rangef getNearFarPlane() const override;

	VERSO_3D_API virtual void setNearFarPlane(const Rangef& nearFarPlane) override;

	VERSO_3D_API virtual float getOrthographicZoomLevel() const override;

	VERSO_3D_API virtual void setOrthographicZoomLevel(float zoomLevel) override;

	VERSO_3D_API virtual void moveOrthographicZoomLevel(float delta, float deltaTime) override;

	VERSO_3D_API virtual Vector2f screenToViewportPoint(
			const Vector2f& screenPointPixels, const Vector2i& viewportResolution) const override;

	VERSO_3D_API virtual Vector3f screenToViewportPoint(
			const Vector3f& screenPointPixels, const Vector2i& viewportResolution) const override;

	VERSO_3D_API virtual Vector2f viewportToScreenPoint(
			const Vector2f& viewportPointRelative, const Vector2i& viewportResolution) const override;

	VERSO_3D_API virtual Vector3f viewportToScreenPoint(
			const Vector3f& viewportPointRelative, const Vector2i& viewportResolution) const override;

	VERSO_3D_API virtual void onResize() override;

public: // interface CameraTarget
	VERSO_3D_API virtual Degree getOrthographicRotation() const override;

	VERSO_3D_API virtual void setOrthographicRotation(Degree orthographicRotation) override;

	VERSO_3D_API virtual void changeOrthographicRotation(Degree delta, float deltaTime) override;

public: // toString (interface ICamera)
	VERSO_3D_API UString toString(const UString& newLinePadding) const override;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const override;
};


} // End namespace Verso

#endif // End header guard

