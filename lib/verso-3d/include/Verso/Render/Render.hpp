#ifndef VERSO_3D_RENDER_RENDER_HPP
#define VERSO_3D_RENDER_RENDER_HPP

#include <Verso/Display/IWindowOpengl.hpp>
#include <Verso/Render/ShaderProgram.hpp>
#include <Verso/Render/Vao.hpp>
#include <Verso/Render/Texture.hpp>
#include <Verso/Math/RefScale.hpp>
#include <Verso/Math/ColorGenerator.hpp>
#include <Verso/Render/Camera/ICamera.hpp>
#include <Verso/Render/ClearFlag.hpp>
#include <Verso/Render/VaoGenerator.hpp>
#include <Verso/Render/Material/MaterialColor3d.hpp>
#include <Verso/Render/Material/MaterialNormalVisualizer3d.hpp>
#include <Verso/Math/RgbaColorf.hpp>
#include <Verso/Math/Matrix4x4f.hpp>
#include <Verso/Math/Align.hpp>
#include <iostream>

namespace Verso {


class Render
{
public: // static variables
	VERSO_3D_API static bool created;
	VERSO_3D_API static UString shadersPath;
	VERSO_3D_API static UString shadersVersoPath;
	VERSO_3D_API static Vao quad2dVao;
	VERSO_3D_API static Vao quad2dTextureInvYVao;
	VERSO_3D_API static Vao quad3dVao;
	VERSO_3D_API static Vao quad3dTextureInvYVao;
	VERSO_3D_API static ShaderProgram defaultShaderGui2d;
	VERSO_3D_API static ShaderProgram defaultShaderGui3d;
	VERSO_3D_API static ShaderProgram defaultShaderSprite3d;
	VERSO_3D_API static Vao vaoDebugGridPlane1;
	VERSO_3D_API static Vao vaoDebugGridPlane5;
	VERSO_3D_API static Vao vaoDebugGridPlane10;
	VERSO_3D_API static MaterialColor3d matDebugGridPlane1;
	VERSO_3D_API static MaterialColor3d matDebugGridPlane5;
	VERSO_3D_API static MaterialColor3d matDebugGridPlane10;
	VERSO_3D_API static MaterialNormalVisualizer3d matNormalVisualizer;
	VERSO_3D_API static Vao vaoCamera;
	VERSO_3D_API static MaterialColor3d matCamera;
	VERSO_3D_API static Vao vaoLight;
	VERSO_3D_API static MaterialColor3d matLight;

public: // static (create/destroy)
	VERSO_3D_API static void create(IWindowOpengl& window, const UString& shadersPath, const UString& shadersVersoPath);

	VERSO_3D_API static void destroy() VERSO_NOEXCEPT;

	VERSO_3D_API static bool isCreated();

public: // static
	VERSO_3D_API static void clearScreen(ClearFlag clearFlag, RgbaColorf clearColor = ColorGenerator::getColor(ColorRgb::BlenderBackground));

	VERSO_3D_API static void clearScreen(const RgbaColorf& color, bool clearColor = true, bool clearDepth = true);

	VERSO_3D_API static void clearScreen(GLfloat red, GLfloat green, GLfloat blue, bool clearColor = true, bool clearDepth = true);

	VERSO_3D_API static void debugGrid(
			IWindowOpengl& window, const FrameTimestamp& time, const ICamera& camera,
			bool grid1x1 = true, bool grid5x5 = false, bool grid10x10 = false);

	VERSO_3D_API static void debugNormals(
			IWindowOpengl& window, const FrameTimestamp& time, const ICamera& camera,
			Vao& vao, const Matrix4x4f& modelMatrix);

	VERSO_3D_API static void debugCamerasAndLights(
			IWindowOpengl& window, const FrameTimestamp& time, const ICamera& camera,
			const std::vector<ICamera*>& cameras,
			const std::vector<DirectionalLight>& directionalLights,
			const std::vector<PointLight>& pointLights,
			const std::vector<SpotLight>& spotLights);

	VERSO_3D_API static void draw2d(
			IWindowOpengl& window, ICamera& camera, Texture& texture, const Vector3f& point,
			const Align& align = Align(HAlign::Left, VAlign::Top),
			const Vector2f& relativeDestSize = Vector2f(1.0f, 1.0f),
			RefScale refScale = RefScale::ImageSize_KeepAspectRatio_FromX,
			float alpha = 1.0f,
			Degree angleZ = 0.0f,
			bool textureInvertY = false);

	VERSO_3D_API static void draw2d(
			IWindowOpengl& window, ICamera& camera, Texture& texture, const Vector3f& point, const Vector2f& relOffset,
			const Vector2f& relativeDestSize = Vector2f(1.0f, 1.0f),
			RefScale refScale = RefScale::ImageSize_KeepAspectRatio_FromX,
			float alpha = 1.0f,
			Degree angleZ = 0.0f,
			bool textureInvertY = false);

	VERSO_3D_API static void draw2dDrawableResolution(
			IWindowOpengl& window, ICamera& camera, Texture& texture, const Vector3f& point,
			const Align& align = Align(HAlign::Left, VAlign::Top),
			const Vector2f& relativeDestSize = Vector2f(1.0f, 1.0f),
			RefScale refScale = RefScale::ImageSize_KeepAspectRatio_FromX,
			float alpha = 1.0f,
			Degree angleZ = 0.0f,
			bool textureInvertY = false,
			bool applyPixelAspectRatio = false);

	VERSO_3D_API static void draw2dDrawableResolution(
			IWindowOpengl& window, ICamera& camera, Texture& texture, const Vector3f& point, const Vector2f& relOffset,
			const Vector2f& relativeDestSize = Vector2f(1.0f, 1.0f),
			RefScale refScale = RefScale::ImageSize_KeepAspectRatio_FromX,
			float alpha = 1.0f,
			Degree angleZ = 0.0f,
			bool textureInvertY = false,
			bool applyPixelAspectRatio = false);

	//VERSO_3D_API static void draw(const Vector3i& loc, const Vector2i& destSize, const Vector2f& scale, ScaleType scaleType, Radian angleZ, const Align& align)

	////VERSO_3D_API static void drawRel(const Vector3f& loc, const Vector2f& destSize, const Vector2f& scale, ScaleType scaleType, Radian angleZ, const Align& align) = 0;

	//VERSO_3D_API static void drawSubpart(const Vector3i& loc, const Recti& srcRect, const Vector2i& destSize, const Vector2f& scale, ScaleType scaleType, Radian angleZ, const Align& align) = 0;

	////VERSO_3D_API static void drawSubpartRel(const Vector3f& loc, const Rectf& srcRect, const Vector2f& destSize, const Vector2f& scale, ScaleType scaleType, Radian angleZ, const Align& align) = 0;

	//VERSO_3D_API static void drawTile(const Vector3i& loc, size_t index, const Vector2i& tileSize, const Vector2i& destSize, const Vector2f& scale, ScaleType scaleType, Radian angleZ, const Align& align)

	////VERSO_3D_API static void drawTileRel(const Vector3f& loc, size_t index, const Vector2f& tileSize, const Vector2f& destSize, const Vector2f& scale, ScaleType scaleType, Radian angleZ, const Align& align) = 0;
};


} // End namespace Verso

#endif // End header guard

