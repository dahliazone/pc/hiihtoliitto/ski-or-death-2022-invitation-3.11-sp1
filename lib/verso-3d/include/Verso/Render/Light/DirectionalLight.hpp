#ifndef VERSO_3D_RENDER_LIGHT_DIRECTIONALLIGHT_HPP
#define VERSO_3D_RENDER_LIGHT_DIRECTIONALLIGHT_HPP

#include <Verso/verso-3d-common.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/Math/Vector3.hpp>
#include <Verso/Math/RgbaColorf.hpp>

namespace Verso {


class DirectionalLight
{
public:
	Vector3f direction;
	RgbaColorf ambient;
	RgbaColorf diffuse;
	RgbaColorf specular;

public:
	DirectionalLight() :
		direction(),

		ambient(),
		diffuse(),
		specular()
	{
	}


	DirectionalLight(const Vector3f& direction, const RgbaColorf& ambient,
			   const RgbaColorf& diffuse, const RgbaColorf& specular) :
		direction(direction),

		ambient(ambient),
		diffuse(diffuse),
		specular(specular)
	{
	}


	DirectionalLight(const DirectionalLight& original) :
		direction(original.direction),

		ambient(original.ambient),
		diffuse(original.diffuse),
		specular(original.specular)
	{
	}


	DirectionalLight(DirectionalLight&& original) noexcept :
		direction(std::move(original.direction)),

		ambient(std::move(original.ambient)),
		diffuse(std::move(original.diffuse)),
		specular(std::move(original.specular))
	{
	}


	DirectionalLight& operator =(const DirectionalLight& original)
	{
		if (this != &original) {
			direction = original.direction;

			ambient = original.ambient;
			diffuse = original.diffuse;
			specular = original.specular;
		}
		return *this;
	}


	DirectionalLight& operator =(DirectionalLight&& original) noexcept
	{
		if (this != &original) {
			direction = std::move(original.direction);

			ambient = std::move(original.ambient);
			diffuse = std::move(original.diffuse);
			specular = std::move(original.specular);
		}
		return *this;
	}


	~DirectionalLight()
	{
	}


public:
	void set(const Vector3f& direction, const RgbaColorf& ambient,
			 const RgbaColorf& diffuse, const RgbaColorf& specular)
	{
		this->direction = direction;

		this->ambient = ambient;
		this->diffuse = diffuse;
		this->specular = specular;
	}


public: // toString
	UString toString() const
	{
		UString str;
		str += "direction=";
		str += direction.toString();

		str += ", ambient=";
		str += ambient.toString();
		str += ", diffuse=";
		str += diffuse.toString();
		str += ", specular=";
		str += specular.toString();
		return str;
	}


	UString toStringDebug() const
	{
		UString str("DirectionalLight(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const DirectionalLight& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso


#endif // End header guard

