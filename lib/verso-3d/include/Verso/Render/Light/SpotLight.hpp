#ifndef VERSO_3D_RENDER_LIGHT_SPOTLIGHT_HPP
#define VERSO_3D_RENDER_LIGHT_SPOTLIGHT_HPP

#include <Verso/System/UString.hpp>
#include <Verso/Math/Vector3.hpp>
#include <Verso/Math/RgbaColorf.hpp>
#include <Verso/Render/Light/Attenuation.hpp>

namespace Verso {


class SpotLight
{
public:
	Vector3f position;
	Vector3f direction;

	float cutOff;
	float outerCutOff;
	Attenuation attenuation;

	RgbaColorf ambient;
	RgbaColorf diffuse;
	RgbaColorf specular;

public:
	SpotLight() :
		position(),
		direction(),

		cutOff(0.0f),
		outerCutOff(0.0f),
		attenuation(),

		ambient(),
		diffuse(),
		specular()
	{
	}


	SpotLight(const SpotLight& original) :
		position(original.position),
		direction(original.direction),

		cutOff(original.cutOff),
		outerCutOff(original.outerCutOff),
		attenuation(original.attenuation),

		ambient(original.ambient),
		diffuse(original.diffuse),
		specular(original.specular)
	{
	}


	SpotLight(SpotLight&& original) noexcept :
		position(std::move(original.position)),
		direction(std::move(original.direction)),

		cutOff(std::move(original.cutOff)),
		outerCutOff(std::move(original.outerCutOff)),
		attenuation(std::move(original.attenuation)),

		ambient(std::move(original.ambient)),
		diffuse(std::move(original.diffuse)),
		specular(std::move(original.specular))
	{
	}


	SpotLight& operator =(const SpotLight& original)
	{
		if (this != &original) {
			position = original.position;
			direction = original.direction;

			cutOff = original.cutOff;
			outerCutOff = original.outerCutOff;
			attenuation = original.attenuation;

			ambient = original.ambient;
			diffuse = original.diffuse;
			specular = original.specular;
		}
		return *this;
	}


	SpotLight& operator =(SpotLight&& original) noexcept
	{
		if (this != &original) {
			position = std::move(original.position);
			direction = std::move(original.direction);

			cutOff = std::move(original.cutOff);
			outerCutOff = std::move(original.outerCutOff);
			attenuation = std::move(original.attenuation);

			ambient = std::move(original.ambient);
			diffuse = std::move(original.diffuse);
			specular = std::move(original.specular);
		}
		return *this;
	}


	~SpotLight()
	{
	}


public:
	void set(const Vector3f& position, const Vector3f& direction,
			 float cutOff, float outerCutOff,
			 const Attenuation& attenuation,
			 const RgbaColorf& ambient,
			 const RgbaColorf& diffuse,
			 const RgbaColorf& specular)
	{
		this->position = position;
		this->direction = direction;

		this->cutOff = cutOff;
		this->outerCutOff = outerCutOff;
		this->attenuation = attenuation;

		this->ambient = ambient;
		this->diffuse = diffuse;
		this->specular = specular;
	}



public: // toString
	UString toString() const
	{
		UString str;
		str += "position=";
		str += position.toString();
		str += "direction=";
		str += direction.toString();

		str += "cutOff=";
		str.append2(cutOff);
		str += "outerCutOff=";
		str.append2(outerCutOff);
		str += ", attenuation=";
		str += attenuation.toString();

		str += ", ambient=";
		str += ambient.toString();
		str += ", diffuse=";
		str += diffuse.toString();
		str += ", specular=";
		str += specular.toString();
		return str;
	}


	UString toStringDebug() const
	{
		UString str("SpotLight(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const SpotLight& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso


#endif // End header guard

