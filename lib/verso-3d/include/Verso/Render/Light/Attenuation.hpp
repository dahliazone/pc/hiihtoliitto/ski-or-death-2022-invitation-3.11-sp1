#ifndef VERSO_3D_RENDER_LIGHT_ATTENUATION_HPP
#define VERSO_3D_RENDER_LIGHT_ATTENUATION_HPP

#include <Verso/verso-3d-common.hpp>
#include <Verso/System/UString.hpp>

namespace Verso {


class Attenuation
{
public:
	float constant;
	float linear;
	float quadratic;

public: // static
	static Attenuation maxDistance7()
	{
		return Attenuation(1.0f, 0.7f, 1.8f);
	}


	static Attenuation maxDistance13()
	{
		return Attenuation(1.0f, 0.35f, 0.44f);
	}


	static Attenuation maxDistance20()
	{
		return Attenuation(1.0f, 0.22f, 0.20f);
	}


	static Attenuation maxDistance32()
	{
		return Attenuation(1.0f, 0.14f, 0.07f);
	}


	static Attenuation maxDistance50()
	{
		return Attenuation(1.0f, 0.09f, 0.032f);
	}


	static Attenuation maxDistance65()
	{
		return Attenuation(1.0f, 0.07f, 0.017f);
	}


	static Attenuation maxDistance100()
	{
		return Attenuation(1.0f, 0.045f, 0.0075f);
	}


	static Attenuation maxDistance160()
	{
		return Attenuation(1.0f, 0.027f, 0.0028f);
	}


	static Attenuation maxDistance200()
	{
		return Attenuation(1.0f, 0.022f, 0.0019f);
	}


	static Attenuation maxDistance325()
	{
		return Attenuation(1.0f, 0.014f, 0.0007f);
	}


	static Attenuation maxDistance600()
	{
		return Attenuation(1.0f, 0.007f, 0.0002f);
	}


	static Attenuation maxDistance3250()
   {
	   return Attenuation(1.0f, 0.0014f, 0.000007f);
	}


public:
	Attenuation() :
		constant(0.0f),
		linear(0.0f),
		quadratic(0.0f)
	{
	}


	Attenuation(float constant, float linear, float quadratic) :
		constant(constant),
		linear(linear),
		quadratic(quadratic)
	{
	}


	Attenuation(const Attenuation& original) :
		constant(original.constant),
		linear(original.linear),
		quadratic(original.quadratic)
	{
	}


	Attenuation(Attenuation&& original) noexcept :
		constant(std::move(original.constant)),
		linear(std::move(original.linear)),
		quadratic(std::move(original.quadratic))
	{
	}


	Attenuation& operator =(const Attenuation& original)
	{
		if (this != &original) {
			constant = original.constant;
			linear = original.linear;
			quadratic = original.quadratic;
		}
		return *this;
	}


	Attenuation& operator =(Attenuation&& original) noexcept
	{
		if (this != &original) {
			constant = std::move(original.constant);
			linear = std::move(original.linear);
			quadratic = std::move(original.quadratic);
		}
		return *this;
	}


	~Attenuation()
	{
	}


public:
	void set(float constant, float linear, float quadratic)
	{
		this->constant = constant;
		this->linear = linear;
		this->quadratic = quadratic;
	}


public: // toString
	UString toString() const
	{
		UString str;
		str += ", constant=";
		str.append2(constant);
		str += ", linear=";
		str.append2(linear);
		str += ", quadratic=";
		str.append2(quadratic);
		return str;
	}


	UString toStringDebug() const
	{
		UString str("Attenuation(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const Attenuation& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso


#endif // End header guard

