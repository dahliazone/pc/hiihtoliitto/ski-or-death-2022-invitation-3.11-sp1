#ifndef VERSO_3D_RENDER_LIGHT_POINTLIGHT_HPP
#define VERSO_3D_RENDER_LIGHT_POINTLIGHT_HPP

#include <Verso/System/UString.hpp>
#include <Verso/Math/Vector3.hpp>
#include <Verso/Math/RgbaColorf.hpp>
#include <Verso/Render/Light/Attenuation.hpp>

namespace Verso {


class PointLight
{
public:
	Vector3f position;
	Attenuation attenuation;

	RgbaColorf ambient;
	RgbaColorf diffuse;
	RgbaColorf specular;

public:
	PointLight() :
		position(),
		attenuation(),

		ambient(),
		diffuse(),
		specular()
	{
	}


	PointLight(const Vector3f& position,
			   const Attenuation& attenuation,
			   const RgbaColorf& ambient,
			   const RgbaColorf& diffuse,
			   const RgbaColorf& specular) :
		position(position),
		attenuation(attenuation),

		ambient(ambient),
		diffuse(diffuse),
		specular(specular)
	{
	}


	PointLight(const PointLight& original) :
		position(original.position),
		attenuation(original.attenuation),

		ambient(original.ambient),
		diffuse(original.diffuse),
		specular(original.specular)
	{
	}


	PointLight(PointLight&& original) noexcept :
		position(std::move(original.position)),
		attenuation(std::move(original.attenuation)),

		ambient(std::move(original.ambient)),
		diffuse(std::move(original.diffuse)),
		specular(std::move(original.specular))
	{
	}


	PointLight& operator =(const PointLight& original)
	{
		if (this != &original) {
			position = original.position;
			attenuation = original.attenuation;

			ambient = original.ambient;
			diffuse = original.diffuse;
			specular = original.specular;
		}
		return *this;
	}


	PointLight& operator =(PointLight&& original) noexcept
	{
		if (this != &original) {
			position = std::move(original.position);
			attenuation = std::move(original.attenuation);

			ambient = std::move(original.ambient);
			diffuse = std::move(original.diffuse);
			specular = std::move(original.specular);
		}
		return *this;
	}


	~PointLight()
	{
	}


public:
	void set(const Vector3f& position,
			 const Attenuation& attenuation,
			 const RgbaColorf& ambient,
			 const RgbaColorf& diffuse,
			 const RgbaColorf& specular)
	{
		this->position = position;
		this->attenuation = attenuation;

		this->ambient = ambient;
		this->diffuse = diffuse;
		this->specular = specular;
	}


public: // toString
	UString toString() const
	{
		UString str;
		str += "position=";
		str += position.toString();
		str += ", attenuation=";
		str += attenuation.toString();

		str += ", ambient=";
		str += ambient.toString();
		str += ", diffuse=";
		str += diffuse.toString();
		str += ", specular=";
		str += specular.toString();
		return str;
	}


	UString toStringDebug() const
	{
		UString str("PointLight(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const PointLight& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso


#endif // End header guard

