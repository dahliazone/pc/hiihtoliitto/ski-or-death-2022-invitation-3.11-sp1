#ifndef VERSO_3D_RENDER_POLYGONWINDINGORDER_HPP
#define VERSO_3D_RENDER_POLYGONWINDINGORDER_HPP

#include <Verso/Render/Opengl.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>

namespace Verso {


enum class PolygonWindingOrder : GLenum
{
	Unset = GL_NONE,
	ClockWise = GL_CW, // Clockwise winding.
	CounterClockWise = GL_CCW // Counter clockwise window.
};


inline UString polygonWindingOrderToString(const PolygonWindingOrder& polygonWindingOrder)
{
	if (polygonWindingOrder == PolygonWindingOrder::Unset) {
		return "Unset";
	}
	else if (polygonWindingOrder == PolygonWindingOrder::ClockWise) {
		return "ClockWise";
	}
	else if (polygonWindingOrder == PolygonWindingOrder::CounterClockWise) {
		return "CounterClockWise";
	}
	else {
		return "Unknown value";
	}
}


inline GLenum polygonWindingOrderToGlenum(const PolygonWindingOrder& polygonWindingOrder)
{
	if (polygonWindingOrder != PolygonWindingOrder::Unset) {
		return static_cast<GLenum>(polygonWindingOrder);
	}
	else {
		UString error("Cannot convert given polygonWindingOrder::Unset(");
		error.append2(static_cast<GLenum>(polygonWindingOrder));
		error += ") to GLenum";
		VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), "");
	}
}


inline PolygonWindingOrder glenumToPolygonWindingOrder(GLenum polygonWindingOrder)
{
	if (polygonWindingOrder == static_cast<GLenum>(PolygonWindingOrder::Unset) ||
			polygonWindingOrder == static_cast<GLenum>(PolygonWindingOrder::ClockWise) ||
			polygonWindingOrder == static_cast<GLenum>(PolygonWindingOrder::CounterClockWise)) {
		return static_cast<PolygonWindingOrder>(polygonWindingOrder);
	}
	else {
		UString error("Invalid GLenum polygonWindingOrder(");
		error.append2(static_cast<GLenum>(polygonWindingOrder));
		error += ") given as parameter";
		VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), "");
	}
}


} // End namespace Verso

#endif // End header guard

