#ifndef VERSO_3D_RENDER_POLYGONFACECULLMODE_HPP
#define VERSO_3D_RENDER_POLYGONFACECULLMODE_HPP

#include <Verso/Render/Opengl.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>

namespace Verso {


enum class PolygonFaceCullMode : GLenum
{
	Unset = 0,
	Front = GL_FRONT,                 // Render only front sided polygons.
	Back = GL_BACK,                   // Render only back sided polygons.
	FrontAndBack = GL_FRONT_AND_BACK  // Render both sides of polygons.
};


inline UString polygonFaceCullModeToString(const PolygonFaceCullMode& polygonFaceCullMode)
{
	if (polygonFaceCullMode == PolygonFaceCullMode::Unset) {
		return "Unset";
	}
	else if (polygonFaceCullMode == PolygonFaceCullMode::Front) {
		return "Front";
	}
	else if (polygonFaceCullMode == PolygonFaceCullMode::Back) {
		return "Back";
	}
	else if (polygonFaceCullMode == PolygonFaceCullMode::FrontAndBack) {
		return "FrontAndBack";
	}
	else {
		return "Unknown value";
	}
}


inline GLenum polygonFaceCullModeToGlenum(const PolygonFaceCullMode& polygonFaceCullMode)
{
	if (polygonFaceCullMode != PolygonFaceCullMode::Unset) {
		return static_cast<GLenum>(polygonFaceCullMode);
	}
	else {
		UString error("Invalid polygonFaceCullMode(");
		error.append2(static_cast<uint8_t>(polygonFaceCullMode));
		error += ")";
		VERSO_ERROR("verso-3d", error.c_str(), "");
		return 0;
	}
}


inline PolygonFaceCullMode glenumToPolygonFaceCullMode(GLenum polygonFaceCullMode)
{
	if (polygonFaceCullMode == static_cast<GLenum>(PolygonFaceCullMode::Unset) ||
			polygonFaceCullMode == static_cast<GLenum>(PolygonFaceCullMode::Front) ||
			polygonFaceCullMode == static_cast<GLenum>(PolygonFaceCullMode::Back) ||
			polygonFaceCullMode == static_cast<GLenum>(PolygonFaceCullMode::FrontAndBack)) {
		return static_cast<PolygonFaceCullMode>(polygonFaceCullMode);
	}
	else {
		UString error("Invalid GLenum polygonFaceCullMode(");
		error.append2(static_cast<GLenum>(polygonFaceCullMode));
		error += ") given as parameter";
		VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), "");
	}
}


} // End namespace Verso

#endif // End header guard

