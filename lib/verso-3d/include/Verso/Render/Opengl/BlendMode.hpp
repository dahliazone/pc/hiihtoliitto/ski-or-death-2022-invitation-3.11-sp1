#ifndef VERSO_3D_RENDER_BLENDMODE_HPP
#define VERSO_3D_RENDER_BLENDMODE_HPP

#include <Verso/System/UString.hpp>
#include <ostream>
#include <cstdint>

namespace Verso {


enum class BlendMode : uint8_t
{
	Undefined,
	None,
	Transcluent,
	Additive,
	Subtractive,
	ReverseSubtractive
};


inline UString blendModeToString(BlendMode blendMode)
{
	if (blendMode == BlendMode::Undefined) {
		return "Undefined";
	}
	else if (blendMode == BlendMode::None) {
		return "None";
	}
	else if (blendMode == BlendMode::Transcluent) {
		return "Transcluent";
	}
	else if (blendMode == BlendMode::Additive) {
		return "Additive";
	}
	else if (blendMode == BlendMode::Subtractive) {
		return "Subtractive";
	}
	else if (blendMode == BlendMode::ReverseSubtractive) {
		return "ReverseSubtractive";
	}
	else {
		return "Unknown value";
	}
}


// returns BlendMode::Undefined for any erroneuos string
inline BlendMode stringToBlendMode(const UString& str)
{
	if (str.equals("None")) {
		return BlendMode::None;
	}
	else if (str.equals("Transcluent")) {
		return BlendMode::Transcluent;
	}
	else if (str.equals("Additive")) {
		return BlendMode::Additive;
	}
	else if (str.equals("Subtractive")) {
		return BlendMode::Subtractive;
	}
	else if (str.equals("ReverseSubtractive")) {
		return BlendMode::ReverseSubtractive;
	}
	else {
		return BlendMode::Undefined;
	}
}


} // End namespace Verso

#endif // End header guard

