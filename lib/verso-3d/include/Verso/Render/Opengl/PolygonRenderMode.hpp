#ifndef VERSO_3D_RENDER_POLYGONRENDERMODE_HPP
#define VERSO_3D_RENDER_POLYGONRENDERMODE_HPP

#include <Verso/Render/Opengl.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>

namespace Verso {


enum class PolygonRenderMode : GLenum
{
	Unset = GL_NONE,
	Point = GL_POINT, // Render polygon vertices as points.
	Line = GL_LINE,   // Render polygon edges as lines.
	Fill = GL_FILL    // Render polygon as filled.
};


inline UString polygonRenderModeToString(const PolygonRenderMode& polygonRenderMode)
{
	if (polygonRenderMode == PolygonRenderMode::Unset)
		return "Unset";
	else if (polygonRenderMode == PolygonRenderMode::Point)
		return "Point";
	else if (polygonRenderMode == PolygonRenderMode::Line)
		return "Line";
	else if (polygonRenderMode == PolygonRenderMode::Fill)
		return "Fill";
	else
		return "Unknown value";
}


inline GLenum polygonRenderModeToGlenum(const PolygonRenderMode& polygonRenderMode)
{
	if (polygonRenderMode != PolygonRenderMode::Unset)
		return static_cast<GLenum>(polygonRenderMode);
	else {
		UString error("Cannot convert given PolygonRenderMode::Unset(");
		error.append2(static_cast<GLenum>(polygonRenderMode));
		error += ") to GLenum";
		VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), "");
	}
}


inline PolygonRenderMode glenumToPolygonRenderMode(GLenum polygonRenderMode)
{
	if (polygonRenderMode == static_cast<GLenum>(PolygonRenderMode::Unset) ||
	        polygonRenderMode == static_cast<GLenum>(PolygonRenderMode::Point) ||
	        polygonRenderMode == static_cast<GLenum>(PolygonRenderMode::Line) ||
	        polygonRenderMode == static_cast<GLenum>(PolygonRenderMode::Fill)) {
		return static_cast<PolygonRenderMode>(polygonRenderMode);
	}
	else {
		UString error("Invalid GLenum polygonRenderMode(");
		error.append2(static_cast<GLenum>(polygonRenderMode));
		error += ") given as parameter";
		VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), "");
	}
}


} // End namespace Verso

#endif // End header guard

