#ifndef VERSO_3D_RENDER_SHADER_SHADERTYPE_HPP
#define VERSO_3D_RENDER_SHADER_SHADERTYPE_HPP

#include <Verso/Render/Opengl.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>

namespace Verso {


enum class ShaderType : uint8_t
{
	Unset = 0,
#if !(__APPLE__ && __MACH__)
	Compute,
#endif
	Vertex,
	TessellationControl,
	TessellationEvaluation,
	Geometry,
	Fragment
};


inline UString shaderTypeToString(const ShaderType& shaderType)
{
	if (shaderType ==  ShaderType::Unset)
		return "Unset";
#if !(__APPLE__ && __MACH__)
	else if (shaderType ==  ShaderType::Compute)
		return "Compute";
#endif
	else if (shaderType == ShaderType::Vertex)
		return "Vertex";
	else if (shaderType == ShaderType::TessellationControl)
		return "TessellationControl";
	else if (shaderType == ShaderType::TessellationEvaluation)
		return "TessellationEvaluation";
	else if (shaderType == ShaderType::Geometry)
		return "Geometry";
	else if (shaderType == ShaderType::Fragment)
		return "Fragment";
	else
		return "Unknown value";
}


inline GLenum shaderTypeToGlenum(const ShaderType& shaderType)
{
	if (shaderType == ShaderType::Vertex)
		return GL_VERTEX_SHADER;
#if !(__APPLE__ && __MACH__)
	else if (shaderType ==  ShaderType::Compute)
		return GL_COMPUTE_SHADER;
#endif
	else if (shaderType == ShaderType::TessellationControl)
		return GL_TESS_CONTROL_SHADER;
	else if (shaderType == ShaderType::TessellationEvaluation)
		return GL_TESS_EVALUATION_SHADER;
	else if (shaderType == ShaderType::Geometry)
		return GL_GEOMETRY_SHADER;
	else if (shaderType == ShaderType::Fragment)
		return GL_FRAGMENT_SHADER;
	else { // ShaderType::Unset and invalid values
		UString error("Verso::shaderTypeToGlenum(): Invalid shaderType=");
		error.append2(static_cast<unsigned int>(shaderType));
		throw std::runtime_error(error.c_str());
		return 0;
	}
}


} // End namespace Verso

#endif // End header guard

