#ifndef VERSO_3D_RENDER_SHADER_SHADERATTRIBUTE_HPP
#define VERSO_3D_RENDER_SHADER_SHADERATTRIBUTE_HPP

#include <Verso/verso-3d-common.hpp>
#include <Verso/Render/Opengl.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>

namespace Verso {

class ShaderAttribute
{
private:
	bool unset;
	GLuint index;
	UString name;

public: // static
	VERSO_3D_API static const ShaderAttribute Position;
	VERSO_3D_API static const ShaderAttribute Uv;
	VERSO_3D_API static const ShaderAttribute Normal;
	VERSO_3D_API static const ShaderAttribute Color;
	VERSO_3D_API static const ShaderAttribute UvHeightmap;

	VERSO_3D_API static GLuint PositionIndex;
	VERSO_3D_API static GLuint UvIndex;
	VERSO_3D_API static GLuint NormalIndex;
	VERSO_3D_API static GLuint ColorIndex;
	VERSO_3D_API static GLuint UvHeightmapIndex;
	VERSO_3D_API static GLuint LastDefaultIndex;

	VERSO_3D_API static UString PositionStr;
	VERSO_3D_API static UString UvStr;
	VERSO_3D_API static UString NormalStr;
	VERSO_3D_API static UString ColorStr;
	VERSO_3D_API static UString UvHeightmapStr;

public: // static
	VERSO_3D_API static std::vector<ShaderAttribute> getDefaultAttributes()
	{
		std::vector<ShaderAttribute> attributes;
		attributes.push_back(Position);
		attributes.push_back(Uv);
		attributes.push_back(Normal);
		attributes.push_back(Color);
		attributes.push_back(UvHeightmap);
		return attributes;
	}


public:
	VERSO_3D_API ShaderAttribute() :
		unset(true),
		index(0),
		name()
	{
	}


	VERSO_3D_API explicit ShaderAttribute(GLuint index)
	{
		set(index);
	}


	VERSO_3D_API ShaderAttribute(GLuint index, const UString& name)
	{
		set(index, name);
	}


	VERSO_3D_API void reset()
	{
		unset = true;
		index = 0;
		name = "";
	}


	VERSO_3D_API void set(GLuint index)
	{
		unset = false;
		this->index = index;
		name = getNameForIndex(index);
	}


	VERSO_3D_API void set(GLuint index, const UString& name)
	{
		unset = false;
		this->index = index;
		if (index <= LastDefaultIndex) {
			UString error("Verso::ShaderAttribute::ShaderAttribute(GLuint=");
			error.append2(index);
			error += ", name=\""+name+"\"): Trying to replace default shader attribute index (<= ";
			error.append2(LastDefaultIndex);
			error += ")!";
			throw std::runtime_error(error.c_str());
		}
		this->name = name;
	}


	VERSO_3D_API GLuint getIndex() const
	{
		return index;
	}


	VERSO_3D_API const UString& getName() const
	{
		return name;
	}


private:
	static const UString& getNameForIndex(GLuint index)
	{
		if (index == PositionIndex) {
			return PositionStr;
		}
		else if (index == UvIndex) {
			return UvStr;
		}
		else if (index == NormalIndex) {
			return NormalStr;
		}
		else if (index == ColorIndex) {
			return ColorStr;
		}
		else if (index == UvHeightmapIndex) {
			return UvHeightmapStr;
		}
		else {
			UString error("Verso::ShaderAttribute::getName(GLuint=");
			error.append2(index);
			error += "): Unknown index for default shader attribute!";
			throw std::runtime_error(error.c_str());
		}
	}


public:
	VERSO_3D_API bool isUnset() const
	{
		return unset;
	}


public: // toString
	VERSO_3D_API UString toString() const
	{
		UString str;
		str.append2(index);
		if (unset == false)
			str += ": ";
		else
			str += "unset";
		str.append2(name);
		return str;
	}


	VERSO_3D_API UString toStringDebug() const
	{
		UString str("ShaderAttribute(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const ShaderAttribute& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso

#endif // End header guard

