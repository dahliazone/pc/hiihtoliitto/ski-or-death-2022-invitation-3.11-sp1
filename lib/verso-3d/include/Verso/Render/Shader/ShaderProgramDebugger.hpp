#ifndef VERSO_3D_RENDER_SHADER_SHADERPROGRAMDEBUGGER_HPP
#define VERSO_3D_RENDER_SHADER_SHADERPROGRAMDEBUGGER_HPP

#include <Verso/Render/Shader/ShaderUniformType.hpp>
#include <Verso/Render/Shader/ShaderUniformDebugRow.hpp>
#include <Verso/Render/Opengl.hpp>
#include <Verso/System/UString.hpp>
#include <algorithm>
#include <map>
#include <iostream>
#include <iomanip>

namespace Verso {


class ShaderProgramDebugger
{
private:
	std::map<UString, ShaderUniformType> uniformTypes;
	GLuint handle;
	std::vector<UString> sourceFileNames;
	const size_t maxTypeSize = 64;
	GLfloat* floatValues;
	GLint* intValues;
	GLuint* uintValues;

public:
	VERSO_3D_API ShaderProgramDebugger();

	ShaderProgramDebugger(const ShaderProgramDebugger& original) = delete;

	VERSO_3D_API ShaderProgramDebugger(ShaderProgramDebugger&& original);

	ShaderProgramDebugger& operator =(const ShaderProgramDebugger& original) = delete;

	VERSO_3D_API ShaderProgramDebugger& operator =(ShaderProgramDebugger&& original);

	VERSO_3D_API ~ShaderProgramDebugger();

public:
	VERSO_3D_API ShaderUniformType getShaderUniformType(const UString& name) const;

	VERSO_3D_API void saveUniformType(const UString& name, const ShaderUniformType& type);

	VERSO_3D_API void printActiveUniforms(GLuint handle, const std::vector<UString>& sourceFileNames);

	VERSO_3D_API UString generateActiveUniforms(GLuint handle, const std::vector<UString>& sourceFileNames);

private:
	void calculateColumnMaxSizes(
			GLint& nUniforms, GLint& maxLen,
			size_t& typeColumnSize, size_t& nameColumnSize, size_t& valueColumnSize);

	UString generateColumnTitles(size_t typeColumnSize, size_t nameColumnSize, size_t valueColumnSize);

	UString generateHorizLine(size_t typeColumnSize, size_t nameColumnSize, size_t valueColumnSize);

	UString generateRowBeforeValue(size_t typeColumnSize, size_t nameColumnSize, const ShaderUniformDebugRow* row);

	UString generateEmptyRowBeforeValue(size_t typeColumnSize, size_t nameColumnSize);

	UString generateValueSingle(const ShaderUniformType& uniformType, size_t index);

	UString generateValueVec(const ShaderUniformType& uniformType);

	UString generateValueMat(size_t typeColumnSize, size_t nameColumnSize, const ShaderUniformType& uniformType);

	UString generateValue(size_t typeColumnSize, size_t nameColumnSize, const ShaderUniformDebugRow* row);

public: // toString
	VERSO_3D_API UString toString() const;

	VERSO_3D_API UString toStringDebug() const;


	friend std::ostream& operator <<(std::ostream& ost, const ShaderProgramDebugger& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso

#endif // End header guard

