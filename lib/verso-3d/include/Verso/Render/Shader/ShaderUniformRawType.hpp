#ifndef VERSO_3D_RENDER_SHADER_SHADERUNIFORMRAWTYPE_HPP
#define VERSO_3D_RENDER_SHADER_SHADERUNIFORMRAWTYPE_HPP

#include <Verso/Render/Opengl.hpp>
#include <Verso/System/UString.hpp>

namespace Verso {


enum class ShaderUniformRawType : uint8_t
{
	Unset = 0,
	Bool,
	Int,
	Uint,
	Float,
	Double,
	Bvec2,
	Bvec3,
	Bvec4,
	Ivec2,
	Ivec3,
	Ivec4,
	Uvec2,
	Uvec3,
	Uvec4,
	Vec2,
	Vec3,
	Vec4,
	Dvec2,
	Dvec3,
	Dvec4,
	Mat2,
	Mat3,
	Mat4,
	Sampler2d,
	SamplerCube,
	Rgb,
	Rgba
	//array?
	//struct?
	//Interface Block?
};


inline void shaderUniformMatrixSize(const ShaderUniformRawType& shaderUniformRawType, size_t& width, size_t& height)
{
	if (shaderUniformRawType == ShaderUniformRawType::Mat2) {
		width = 2;
		height = 2;
	}
	else if (shaderUniformRawType == ShaderUniformRawType::Mat3) {
		width = 3;
		height = 3;
	}
	else if (shaderUniformRawType == ShaderUniformRawType::Mat4) {
		width = 4;
		height = 4;
	}
	else {
		width = 0;
		height = 0;
	}
}


inline size_t shaderUniformVectorSize(const ShaderUniformRawType& shaderUniformRawType)
{
	if (shaderUniformRawType == ShaderUniformRawType::Bvec2) {
		return 2;
	}
	else if (shaderUniformRawType == ShaderUniformRawType::Bvec3) {
		return 3;
	}
	else if (shaderUniformRawType == ShaderUniformRawType::Bvec4) {
		return 4;
	}
	else if (shaderUniformRawType == ShaderUniformRawType::Ivec2) {
		return 2;
	}
	else if (shaderUniformRawType == ShaderUniformRawType::Ivec3) {
		return 3;
	}
	else if (shaderUniformRawType == ShaderUniformRawType::Ivec4) {
		return 4;
	}
	else if (shaderUniformRawType == ShaderUniformRawType::Uvec2) {
		return 2;
	}
	else if (shaderUniformRawType == ShaderUniformRawType::Uvec3) {
		return 3;
	}
	else if (shaderUniformRawType == ShaderUniformRawType::Uvec4) {
		return 4;
	}
	else if (shaderUniformRawType == ShaderUniformRawType::Vec2) {
		return 2;
	}
	else if (shaderUniformRawType == ShaderUniformRawType::Vec3) {
		return 3;
	}
	else if (shaderUniformRawType == ShaderUniformRawType::Vec4) {
		return 4;
	}
	else if (shaderUniformRawType == ShaderUniformRawType::Dvec2) {
		return 2;
	}
	else if (shaderUniformRawType == ShaderUniformRawType::Dvec3) {
		return 3;
	}
	else if (shaderUniformRawType == ShaderUniformRawType::Dvec4) {
		return 4;
	}
	else {
		return 1;
	}
}


inline UString shaderUniformRawTypeToString(const ShaderUniformRawType& shaderUniformRawType)
{
	if (shaderUniformRawType ==  ShaderUniformRawType::Unset)
		return "Unset";
	else if (shaderUniformRawType == ShaderUniformRawType::Bool)
		return "bool";
	else if (shaderUniformRawType == ShaderUniformRawType::Int)
		return "int";
	else if (shaderUniformRawType == ShaderUniformRawType::Uint)
		return "uint";
	else if (shaderUniformRawType == ShaderUniformRawType::Float)
		return "float";
	else if (shaderUniformRawType == ShaderUniformRawType::Double)
		return "double";
	else if (shaderUniformRawType == ShaderUniformRawType::Bvec2)
		return "bvec2";
	else if (shaderUniformRawType == ShaderUniformRawType::Bvec3)
		return "bvec3";
	else if (shaderUniformRawType == ShaderUniformRawType::Bvec4)
		return "bvec4";
	else if (shaderUniformRawType == ShaderUniformRawType::Ivec2)
		return "ivec2";
	else if (shaderUniformRawType == ShaderUniformRawType::Ivec3)
		return "ivec3";
	else if (shaderUniformRawType == ShaderUniformRawType::Ivec4)
		return "ivec4";
	else if (shaderUniformRawType == ShaderUniformRawType::Uvec2)
		return "uvec2";
	else if (shaderUniformRawType == ShaderUniformRawType::Uvec3)
		return "uvec3";
	else if (shaderUniformRawType == ShaderUniformRawType::Uvec4)
		return "uvec4";
	else if (shaderUniformRawType == ShaderUniformRawType::Vec2)
		return "vec2";
	else if (shaderUniformRawType == ShaderUniformRawType::Vec3)
		return "vec3";
	else if (shaderUniformRawType == ShaderUniformRawType::Vec4)
		return "vec4";
	else if (shaderUniformRawType == ShaderUniformRawType::Dvec2)
		return "dvec2";
	else if (shaderUniformRawType == ShaderUniformRawType::Dvec3)
		return "dvec3";
	else if (shaderUniformRawType == ShaderUniformRawType::Dvec4)
		return "dvec4";
	else if (shaderUniformRawType == ShaderUniformRawType::Mat2)
		return "mat2";
	else if (shaderUniformRawType == ShaderUniformRawType::Mat3)
		return "mat3";
	else if (shaderUniformRawType == ShaderUniformRawType::Mat4)
		return "mat4";
	else if (shaderUniformRawType == ShaderUniformRawType::Sampler2d)
		return "sampler2D";
	else if (shaderUniformRawType == ShaderUniformRawType::SamplerCube)
		return "samplerCube";
	else if (shaderUniformRawType == ShaderUniformRawType::Rgb)
		return "vec3(RGB)";
	else if (shaderUniformRawType == ShaderUniformRawType::Rgba)
		return "vec4(RGBA)";
	//array?
	//struct?
	//Interface Block?
	else
		return "Unknown value";
}


} // End namespace Verso

#endif // End header guard

