#ifndef VERSO_3D_RENDER_SHADER_SHADER_HPP
#define VERSO_3D_RENDER_SHADER_SHADER_HPP

#include <Verso/Display/Context.hpp>
#include <Verso/Render/Shader/ShaderType.hpp>
#include <Verso/System/File.hpp>
#include <Verso/System/UString.hpp>

namespace Verso {


class Shader
{
private:
	ShaderType type;
	bool created;
	GLuint handle;
	UString sourceFileName;
	UString source;

public:
	Shader() :
		type(ShaderType::Unset),
		created(false),
		handle(0),
		sourceFileName(),
		source()
	{
	}


	Shader(const Shader& original) = delete;


	Shader(Shader&& original) noexcept :
		type(std::move(original.type)),
		created(std::move(original.created)),
		handle(std::move(original.handle)),
		sourceFileName(std::move(original.sourceFileName)),
		source(std::move(original.source))
	{
		// Clear the original
		original.created = false;
		original.handle = 0;
	}


	Shader& operator =(const Shader& original) = delete;


	Shader& operator =(Shader&& original) noexcept
	{
		if (this != &original) {
			type = std::move(original.type);
			created = std::move(original.created);
			handle = std::move(original.handle);
			sourceFileName = std::move(original.sourceFileName);
			source = std::move(original.source);

			// Clear the original
			original.created = false;
			original.handle = 0;
		}
		return *this;
	}


	~Shader()
	{
		destroy();
	}


	void createFromFile(ShaderType type, const UString& sourceFileName)
	{
		if (!Context::isContextCreated()) {
			UString error("In function: void Verso::Shader::createFromFile(ShaderType="+shaderTypeToString(type)+", const UString&="+sourceFileName+"):\n\n");
			error += "Message: No OpenGL context created!";
			throw std::runtime_error(error.c_str());
		}

		this->sourceFileName = sourceFileName;

		File sourceFile(this->sourceFileName);
		if (sourceFile.exists() == false) {
			UString message("Cannot load "+shaderTypeToString(type)+" shader. File not found!");
			VERSO_FILENOTFOUND("verso-3d", message.c_str(), this->sourceFileName.c_str());
		}

		if (sourceFile.loadToString(this->source) == false) { // NULL terminated string
			UString message("Cannot load "+shaderTypeToString(type)+" shader. File found, but could not be loaded!");
			VERSO_IOERROR("verso-3d", message.c_str(), this->sourceFileName.c_str());
		}

		if (this->source.isEmpty()) {
			UString message("Cannot load "+shaderTypeToString(type)+" shader. File found, but was empty!");
			VERSO_IOERROR("verso-3d", message.c_str(), this->sourceFileName.c_str());
		}

		createFromString(type, this->source);
	}


	void createFromString(ShaderType type, const UString& source)
	{
		if (!Context::isContextCreated()) {
			UString error("In function: void Verso::Shader::createFromString(ShaderType="+shaderTypeToString(type)+", const UString&="+sourceFileName+"):\n\n");
			error += "Message: No OpenGL context created!";
			throw std::runtime_error(error.c_str());
		}

		this->type = type;
		this->handle = glCreateShader(shaderTypeToGlenum(this->type));
		GL_CHECK_FOR_ERRORS("", "");

		this->source = source;
		const GLchar* sourceCstr = this->source.c_str();
		glShaderSource(this->handle, 1, &sourceCstr, nullptr); // Don't need length with NULL terminated string
		GL_CHECK_FOR_ERRORS("", "");

		glCompileShader(this->handle);
		GL_CHECK_FOR_ERRORS("", "");

		int compileStatus;
		glGetShaderiv(this->handle, GL_COMPILE_STATUS, &compileStatus);
		GL_CHECK_FOR_ERRORS("", "");
		if (compileStatus == GL_FALSE) {
			int maxLength;
			glGetShaderiv(this->handle, GL_INFO_LOG_LENGTH, &maxLength);
			GL_CHECK_FOR_ERRORS("", "");

			// The maxLength includes the NULL character
			char* infoLog = new char[static_cast<size_t>(maxLength)];
			glGetShaderInfoLog(this->handle, maxLength, &maxLength, infoLog);
			GL_CHECK_FOR_ERRORS("", "");

			UString error("In function: void Verso::Shader::createFromString()\n\n");
			error += "Message: Could not compile "+shaderTypeToString(type)+"Shader\n\n";
			error += infoLog;
			error += "\n";
			error += "Filename: "+this->sourceFileName;

			// Handle the error in an appropriate way such as displaying a message or writing to a log file.
			// In this simple program, we'll just leave
			delete[] infoLog;

			throw std::runtime_error(error.c_str());
		}

		created = true;
	}


	void destroy() VERSO_NOEXCEPT
	{
		if (Context::isContextCreated()) {
			if (isCreated()) {
				glDeleteShader(handle);
				GL_CHECK_FOR_ERRORS("", "");
				created = false;
				handle = 0;
			}
		}
	}


	ShaderType getType() const
	{
		return type;
	}


	GLuint getHandle() const
	{
		return handle;
	}


	UString getSourceFileName() const
	{
		return sourceFileName;
	}


public:
	bool isCreated() const
	{
		return created;
	}


	UString getSource() const
	{
		return source;
	}


public: // toString
	UString toString(const UString& newLinePadding) const
	{
		(void)newLinePadding;

		UString str("{ ");
		str += "type=";
		str.append2(shaderTypeToString(type));
		str += ", handle=";
		str.append2(handle);
		str += ", sourceFileName="+sourceFileName;
		str += " }";
		return str;
	}


	UString toStringDebug(const UString& newLinePadding) const
	{
		UString str("Shader(");
		str += toString(newLinePadding);
		str += ", source=\n\"";
		str += source;
		str += "\")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const Shader& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

