#ifndef VERSO_3D_RENDER_SHADER_SHADERUNIFORMFETCHTYPE_HPP
#define VERSO_3D_RENDER_SHADER_SHADERUNIFORMFETCHTYPE_HPP

#include <Verso/Render/Opengl.hpp>
#include <Verso/System/UString.hpp>

namespace Verso {


enum class ShaderUniformFetchType : uint8_t
{
	Unset = 0,
	Float,
	Int,
	Uint
};


inline UString shaderUniformFetchTypeToString(const ShaderUniformFetchType& shaderUniformFetchType)
{
	if (shaderUniformFetchType ==  ShaderUniformFetchType::Unset)
		return "Unset";
	else if (shaderUniformFetchType == ShaderUniformFetchType::Float)
		return "Float";
	else if (shaderUniformFetchType == ShaderUniformFetchType::Int)
		return "Int";
	else if (shaderUniformFetchType == ShaderUniformFetchType::Uint)
		return "Uint";
	else
		return "Unknown value";
}


} // End namespace Verso

#endif // End header guard

