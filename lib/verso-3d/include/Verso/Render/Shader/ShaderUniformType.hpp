#ifndef VERSO_3D_RENDER_SHADER_SHADERUNIFORMTYPE_HPP
#define VERSO_3D_RENDER_SHADER_SHADERUNIFORMTYPE_HPP

#include <Verso/verso-3d-common.hpp>
#include <Verso/Render/Shader/ShaderUniformRawType.hpp>
#include <Verso/Render/Shader/ShaderUniformFetchType.hpp>

namespace Verso {


class VERSO_3D_API ShaderUniformType
{
public:
	ShaderUniformRawType rawType;
	ShaderUniformFetchType fetchType;
	size_t typeSize;
	size_t definedArraySize;
	size_t actualArraySize;

public: // static
	static const ShaderUniformType Unset;
	static const ShaderUniformType Bool;
	static const ShaderUniformType Int;
	static const ShaderUniformType Uint;
	static const ShaderUniformType Float;
	static const ShaderUniformType Double;
	static const ShaderUniformType Bvec2;
	static const ShaderUniformType Bvec3;
	static const ShaderUniformType Bvec4;
	static const ShaderUniformType Ivec2;
	static const ShaderUniformType Ivec3;
	static const ShaderUniformType Ivec4;
	static const ShaderUniformType Uvec2;
	static const ShaderUniformType Uvec3;
	static const ShaderUniformType Uvec4;
	static const ShaderUniformType Vec2;
	static const ShaderUniformType Vec3;
	static const ShaderUniformType Vec4;
	static const ShaderUniformType Dvec2;
	static const ShaderUniformType Dvec3;
	static const ShaderUniformType Dvec4;
	static const ShaderUniformType Mat2;
	static const ShaderUniformType Mat3;
	static const ShaderUniformType Mat4;
	static const ShaderUniformType Sampler2d;
	static const ShaderUniformType SamplerCube;
	static const ShaderUniformType Rgb;
	static const ShaderUniformType Rgba;
	//array?
	//struct?
	//Interface Block?

public:
	ShaderUniformType() :
		rawType(ShaderUniformRawType::Unset),
		fetchType(ShaderUniformFetchType::Unset),
		typeSize(0),
		definedArraySize(0),
		actualArraySize(0)
	{
	}

	ShaderUniformType(ShaderUniformRawType rawType, ShaderUniformFetchType fetchType, size_t typeSize, size_t definedArraySize = 1) :
		rawType(rawType),
		fetchType(fetchType),
		typeSize(typeSize),
		definedArraySize(definedArraySize),
		actualArraySize(0)
	{
	}


	void set(ShaderUniformRawType rawType, ShaderUniformFetchType fetchType, size_t typeSize, size_t definedArraySize = 1)
	{
		this->rawType = rawType;
		this->fetchType = fetchType;
		this->typeSize = typeSize;
		this->definedArraySize = definedArraySize;
	}


public: // toString
	UString toString() const
	{
		UString str(shaderUniformRawTypeToString(rawType));
		if (actualArraySize != 0 && actualArraySize != 1) {
			str += "[";
			str.append2(actualArraySize);
			str += "]";
		}
		else if (definedArraySize > 1) {
			str += "[1]";
		}
		if (definedArraySize != actualArraySize) {
			str += " of [";
			str.append2(definedArraySize);
			str += "]";
		}
		return str;
	}


	UString toStringDebug() const
	{
		UString str("ShaderUniformType(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const ShaderUniformType& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso

#endif // End header guard

