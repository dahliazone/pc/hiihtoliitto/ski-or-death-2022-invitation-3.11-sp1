#ifndef VERSO_3D_RENDER_SHADER_SHADERUNIFORMDEBUGROW_HPP
#define VERSO_3D_RENDER_SHADER_SHADERUNIFORMDEBUGROW_HPP

#include <Verso/Render/Shader/ShaderUniformType.hpp>
#include <Verso/Render/Opengl.hpp>
#include <Verso/System/UString.hpp>

namespace Verso {


class ShaderUniformDebugRow
{
public:
	UString name;
	UString newName;
	GLint location;
	ShaderUniformType uniformType;

public:
	ShaderUniformDebugRow() :
	    name(),
	    newName(),
	    location(-1),
	    uniformType()
	{
	}


	ShaderUniformDebugRow(const UString& name, const UString& newName,
	                      GLint location, const ShaderUniformType& uniformType) :
	    name(name),
	    newName(newName),
	    location(location),
	    uniformType(uniformType)
	{
	}


	static bool ptrCompare(const ShaderUniformDebugRow* left, const ShaderUniformDebugRow* right)
	{
		return *left < *right;
	}


	bool operator <(const ShaderUniformDebugRow& right) const
	{
		return newName < right.newName;
	}
};


} // End namespace Verso

#endif // End header guard

