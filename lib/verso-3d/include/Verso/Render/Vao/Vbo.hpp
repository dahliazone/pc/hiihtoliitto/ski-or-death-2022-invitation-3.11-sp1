#ifndef VERSO_3D_RENDER_VAO_VBO_HPP
#define VERSO_3D_RENDER_VAO_VBO_HPP

#include <Verso/Render/Vao/BufferUsagePattern.hpp>
#include <Verso/Render/Opengl.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/Render/Shader/ShaderAttribute.hpp>

namespace Verso {


// VBO = Vertex Buffer Object
class Vbo
{
private: // static
	static GLuint lastBoundHandle;

private:
	bool created;
	GLuint handle;
	ShaderAttribute shaderAttribute;
	int entryCount;
	int entryBytes;
	int perVertexCount;
	BufferUsagePattern bufferUsagePattern;
	std::vector<float> dataAppended;

public:
	VERSO_3D_API Vbo();

	Vbo(const Vbo& original) = delete;

	VERSO_3D_API Vbo(Vbo&& original) noexcept;

	Vbo& operator =(const Vbo& original) = delete;

	VERSO_3D_API Vbo& operator =(Vbo&& original) noexcept;

	VERSO_3D_API virtual ~Vbo();

public:
	VERSO_3D_API void create();

	VERSO_3D_API void destroy() VERSO_NOEXCEPT;

	VERSO_3D_API bool isCreated() const;

	VERSO_3D_API void bind();

	VERSO_3D_API static void unbind();

	// Note: binds the vbo to use.
	// Set entryCount amount of GLfloat items from data to vbo with expected usage pattern of the data store.
	VERSO_3D_API void setDataf(const ShaderAttribute& shaderAttribute, const GLvoid* data, int entryCount, int perVertexCount, const BufferUsagePattern& bufferUsagePattern = BufferUsagePattern::StaticDraw);

	// Note: binds the vbo to use.
	// Set entryCount amount of GLfloat items from data to vbo with expected usage pattern of the data store.
	// note: applyAppendedData() must be called before data is actually set to the vbo.
	VERSO_3D_API void appendDataf(const ShaderAttribute& shaderAttribute, const GLvoid* data, int entryCount, int perVertexCount);

	// Note: binds the vbo to use.
	// Set expected usage pattern of the data store.
	// note: use appendDataf() before calling this to actually add the data.
	// note: this clears all appended data after upload to gpu
	VERSO_3D_API void applyAppendedData(const BufferUsagePattern& bufferUsagePattern = BufferUsagePattern::StaticDraw);

public: // getters
	VERSO_3D_API ShaderAttribute getShaderAttribute() const;

	VERSO_3D_API int getEntryCount() const;

	VERSO_3D_API int getEntryBytes() const;

	VERSO_3D_API int getPerVertexCount() const;

	VERSO_3D_API int getAppendedEntryCount() const;

	VERSO_3D_API BufferUsagePattern getBufferUsagePattern() const;

public:
	VERSO_3D_API UString toString() const;

	VERSO_3D_API UString toStringDebug() const;


	friend std::ostream& operator <<(std::ostream& ost, const Vbo& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso

#endif // End header guard

