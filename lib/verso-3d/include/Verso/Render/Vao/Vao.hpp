#ifndef VERSO_3D_RENDER_VAO_VAO_HPP
#define VERSO_3D_RENDER_VAO_VAO_HPP

#include <Verso/Render/Vao/Vbo.hpp>
#include <Verso/Render/Vao/Ebo.hpp>
#include <Verso/Render/Vao/PrimitiveType.hpp>
#include <Verso/Render/Shader/ShaderAttribute.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>
#include <vector>
#include <cstdint>

namespace Verso {


// VAO = Vertex Array Object
class Vao
{
private: // static
	static GLuint lastBoundHandle;

private:
	bool created;
	UString id;
	GLuint handle;
	std::vector<Vbo> vbos;
	Ebo ebo;
	PrimitiveType primitiveType;
	int vertexCount;

public:
	VERSO_3D_API explicit Vao(const UString& id);

	Vao(const Vao& original) = delete;

	VERSO_3D_API Vao(Vao&& original);

	Vao& operator =(const Vao& original) = delete;

	VERSO_3D_API Vao& operator =(Vao&& original);

	VERSO_3D_API ~Vao();

public:
	VERSO_3D_API void create(PrimitiveType primitiveType);

	VERSO_3D_API void destroy() VERSO_NOEXCEPT;

	VERSO_3D_API bool isCreated() const;

	VERSO_3D_API void bind();

	VERSO_3D_API static void unbind();

	VERSO_3D_API void setDataf(const ShaderAttribute& attribute, const GLvoid* data, int vertexCount, int perVertexCount, const BufferUsagePattern& bufferUsagePattern = BufferUsagePattern::StaticDraw);

	VERSO_3D_API void appendDataf(const ShaderAttribute& attribute, const GLvoid* data, int vertexCount, int perVertexCount);

	VERSO_3D_API void setIndices(const GLuint* indices, int indicesCount, const BufferUsagePattern& bufferUsagePattern = BufferUsagePattern::StaticDraw);

	VERSO_3D_API void appendIndices(const GLuint* indices, int indicesCount);

	VERSO_3D_API void applyAppendedData(const BufferUsagePattern& bufferUsagePattern = BufferUsagePattern::StaticDraw);

	VERSO_3D_API void render();

	VERSO_3D_API void render(GLint first, GLsizei count);

	VERSO_3D_API UString getId() const;

	VERSO_3D_API void setId(const UString& id);

	VERSO_3D_API const PrimitiveType& getPrimitiveType() const;

	VERSO_3D_API int getVertexCount() const;

	VERSO_3D_API int getNextDatafAppendIndex(const ShaderAttribute& attribute) const;

	VERSO_3D_API int getNextIndexAppendIndex() const;

public:
	VERSO_3D_API UString toString() const;

	VERSO_3D_API UString toStringDebug() const;


	friend std::ostream& operator <<(std::ostream& ost, const Vao& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso

#endif // End header guard

