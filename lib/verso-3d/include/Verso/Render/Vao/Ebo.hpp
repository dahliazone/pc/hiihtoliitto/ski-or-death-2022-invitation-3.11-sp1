#ifndef VERSO_3D_RENDER_VAO_EBO_HPP
#define VERSO_3D_RENDER_VAO_EBO_HPP

#include <Verso/verso-3d-common.hpp>
#include <Verso/Render/Vao/BufferUsagePattern.hpp>
#include <Verso/Render/Opengl.hpp>
#include <Verso/System/UString.hpp>

namespace Verso {


// EBO = Element Buffer Object
class Ebo
{
private: // static
	static GLuint lastBoundHandle;

private:
	bool created;
	GLuint handle;
	int indicesCount;
	BufferUsagePattern bufferUsagePattern;
	std::vector<GLuint> dataAppended;

public:
	VERSO_3D_API Ebo();

	Ebo(const Ebo& original) = delete;

	VERSO_3D_API Ebo(Ebo&& original) noexcept;

	Ebo& operator =(const Ebo& original) = delete;

	VERSO_3D_API Ebo& operator =(Ebo&& original) noexcept;

	VERSO_3D_API ~Ebo();

public:
	VERSO_3D_API void create();

	VERSO_3D_API void destroy() VERSO_NOEXCEPT;

	VERSO_3D_API void bind();

	VERSO_3D_API static void unbind();

	// Note: binds the ebo to use.
	VERSO_3D_API void setIndices(const GLuint* indices, int indicesCount, const BufferUsagePattern& bufferUsagePattern = BufferUsagePattern::StaticDraw);

	VERSO_3D_API void appendIndices(const GLuint* indices, int entryCount);

	// Note: binds the ebo to use.
	VERSO_3D_API void applyAppendedData(const BufferUsagePattern& bufferUsagePattern = BufferUsagePattern::StaticDraw);

public: // getters
	VERSO_3D_API bool isCreated() const;

	VERSO_3D_API int getIndicesCount() const;

	VERSO_3D_API int getNextAppendIndex() const;

	VERSO_3D_API BufferUsagePattern getBufferUsagePattern() const;

public: // toString
	VERSO_3D_API UString toString() const;

	VERSO_3D_API UString toStringDebug() const;

	friend std::ostream& operator <<(std::ostream& ost, const Ebo& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso

#endif // End header guard

