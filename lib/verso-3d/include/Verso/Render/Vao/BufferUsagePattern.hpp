#ifndef VERSO_3D_RENDER_VAO_BUFFERUSAGEPATTERN_HPP
#define VERSO_3D_RENDER_VAO_BUFFERUSAGEPATTERN_HPP

#include <Verso/Render/Opengl.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>

namespace Verso {


enum class BufferUsagePattern : GLenum
{
	Unset = 0,
	StaticDraw = GL_STATIC_DRAW, // the data will most likely not change at all or very rarely.
	DynamicDraw = GL_DYNAMIC_DRAW, // the data is likely to change a lot.
	StreamDraw = GL_STREAM_DRAW, // the data will change every time it is drawn.
};


inline UString bufferUsagePatternToString(const BufferUsagePattern& bufferUsagePattern)
{
	if (bufferUsagePattern == BufferUsagePattern::Unset)
		return "Unset";
	else if (bufferUsagePattern == BufferUsagePattern::StaticDraw)
		return "StaticDraw";
	else if (bufferUsagePattern == BufferUsagePattern::DynamicDraw)
		return "DynamicDraw";
	else if (bufferUsagePattern == BufferUsagePattern::StreamDraw)
		return "StreamDraw";
	else
		return "Unknown value";
}


inline GLenum bufferUsagePatternToGlenum(const BufferUsagePattern& bufferUsagePattern)
{
	if (bufferUsagePattern != BufferUsagePattern::Unset)
		return static_cast<GLenum>(bufferUsagePattern);
	else {
		UString error("Cannot convert BufferUsagePattern::Unset(");
		error.append2(static_cast<GLenum>(bufferUsagePattern));
		error += ") to GLenum";
		VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), "");
	}
}


} // End namespace Verso

#endif // End header guard

