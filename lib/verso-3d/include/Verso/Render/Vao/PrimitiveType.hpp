#ifndef VERSO_3D_RENDER_VAO_PRIMITIVETYPE_HPP
#define VERSO_3D_RENDER_VAO_PRIMITIVETYPE_HPP

#include <Verso/Render/Opengl.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>
#include <climits>

namespace Verso {


enum class PrimitiveType : GLenum
{
	Points = GL_POINTS, // Treats each vertex as a single point. Vertex n defines point n. N points are drawn.
	Lines = GL_LINES, // Treats each pair of vertices as an independent line segment. Vertices 2 ⁢ n - 1 and 2 ⁢ n define line n. N 2 lines are drawn.
	LineStrip = GL_LINE_STRIP, // Draws a connected group of line segments from the first vertex to the last. Vertices n and n + 1 define line n. N - 1 lines are drawn.
	LineLoop = GL_LINE_LOOP, // Draws a connected group of line segments from the first vertex to the last, then back to the first. Vertices n and n + 1 define line n. The last line, however, is defined by vertices N and 1 . N lines are drawn.
	Triangles = GL_TRIANGLES, // Treats each triplet of vertices as an independent triangle. Vertices 3 ⁢ n - 2 , 3 ⁢ n - 1 , and 3 ⁢ n define triangle n. N 3 triangles are drawn.
	TriangleStrip = GL_TRIANGLE_STRIP, // Draws a connected group of triangles. One triangle is defined for each vertex presented after the first two vertices. For odd n, vertices n, n + 1 , and n + 2 define triangle n. For even n, vertices n + 1 , n, and n + 2 define triangle n. N - 2 triangles are drawn.
	TriangleFan = GL_TRIANGLE_FAN, // Draws a connected group of triangles. One triangle is defined for each vertex presented after the first two vertices. Vertices 1 , n + 1 , and n + 2 define triangle n. N - 2 triangles are drawn.
	//Quads = GL_QUADS, // DEPRECATED: Treats each group of four vertices as an independent quadrilateral. Vertices 4 ⁢ n - 3 , 4 ⁢ n - 2 , 4 ⁢ n - 1 , and 4 ⁢ n define quadrilateral n. N 4 quadrilaterals are drawn.
	//QuadStrip = GL_QUAD_STRIP, // DEPRECATED: Draws a connected group of quadrilaterals. One quadrilateral is defined for each pair of vertices presented after the first pair. Vertices 2 ⁢ n - 1 , 2 ⁢ n , 2 ⁢ n + 2 , and 2 ⁢ n + 1 define quadrilateral n. N 2 - 1 quadrilaterals are drawn. Note that the order in which vertices are used to construct a quadrilateral from strip data is different from that used with independent data.
	//Polygon = GL_POLYGON // DEPRECATED: Draws a single, convex polygon. Vertices 1 through N define this polygon.
	Unset = UINT_MAX
};


inline UString primitiveTypeToString(const PrimitiveType& primitiveType)
{
	if (primitiveType ==  PrimitiveType::Unset)
		return "Unset";
	else if (primitiveType ==  PrimitiveType::Points)
		return "Points";
	else if (primitiveType ==  PrimitiveType::Lines)
		return "Lines";
	else if (primitiveType ==  PrimitiveType::LineStrip)
		return "LineStrip";
	else if (primitiveType ==  PrimitiveType::LineLoop)
		return "LineLoop";
	else if (primitiveType ==  PrimitiveType::Triangles)
		return "Triangles";
	else if (primitiveType ==  PrimitiveType::TriangleStrip)
		return "TriangleStrip";
	else if (primitiveType ==  PrimitiveType::TriangleFan)
		return "TriangleFan";
	else
		return "Unknown value";
}


inline GLenum primitiveTypeToGlenum(const PrimitiveType& primitiveType)
{
	if (primitiveType != PrimitiveType::Unset) {
		return static_cast<GLenum>(primitiveType);
	}
	else {
		UString error("Cannot convert PrimitiveType::Unset(");
		error.append2(static_cast<GLenum>(primitiveType));
		error += ") to GLenum";
		VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), "");
	}
}


} // End namespace Verso

#endif // End header guard

