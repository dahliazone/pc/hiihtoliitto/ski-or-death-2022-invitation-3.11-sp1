#ifndef VERSO_3D_RENDER_OPENGL_HPP
#define VERSO_3D_RENDER_OPENGL_HPP

#include <Verso/verso-3d-common.hpp>
//#if defined (__APPLE_CC__)
//#include <OpenGL/gl3.h>
//#include <SDL2/SDL.h>
//#else
#include <glad/glad.h>
#include <Verso/System/Logger.hpp>
#include <Verso/System/stacktrace.hpp>
#include <Verso/Render/Opengl/BlendMode.hpp>
#include <Verso/Render/Opengl/PolygonRenderMode.hpp>
#include <Verso/Render/Opengl/PolygonWindingOrder.hpp>
#include <Verso/Render/Opengl/PolygonFaceCullMode.hpp>
#include <Verso/Math/math_defines.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/Math/Vector2.hpp>
#include <Verso/Image/ImageSaveFormat.hpp>
#include <cstdint>
#include <vector>


#ifdef NDEBUG
#	define GL_CHECK_FOR_ERRORS(error, identifier) \
		((void)0)
#else
#	define GL_CHECK_FOR_ERRORS(error, identifier) \
		(Opengl::checkForErrors(error, VERSO_FUNCTION, VERSO_FILE, VERSO_LINE, identifier))
#endif
// For disabling OpenGL checks in code, uncomment
//#define GL_CHECK_FOR_ERRORS(error, identifier) ((void)0);


namespace Verso {


typedef struct VERSO_3D_API Opengl
{
	// Note: generally should not be disabled because of performance reasons (disables early z optimization)
	static void depthWrite(bool enabled);

	static void depthTest(bool enabled);

	static void blend(BlendMode blendMode);

	static void blendNone();

	static void blendTranscluent();

	static void blendAdditive();

	static void blendSubtractive();

	static void blendReverseSubtractive();

	// Possible others to implement
	//glBlendFunc(GL_DST_COLOR, GL_ZERO);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	//glBlendFunc(GL_SRC_ALPHA, GL_ZERO);
	//glBlendEquation(GL_MAX);
	//glBlendEquation(GL_FUNC_ADD);
	//glBlendEquation(GL_FUNC_SUBTRACT);

	static PolygonRenderMode getPolygonRenderMode();

	static void setPolygonRenderMode(const PolygonRenderMode& polygonRenderMode);

	static PolygonFaceCullMode getPolygonFaceCullMode();

	static void setPolygonFaceCullMode(const PolygonFaceCullMode& polygonFaceCullMode);

	static PolygonWindingOrder getPolygonWindingOrder();

	static void setPolygonWindingOrder(const PolygonWindingOrder& polygonWindingOrder);

	static void readPixelsRgba(const Vector2i& pos, const Vector2i& size, std::uint8_t* data);

	static void readPixelsRgba(int x, int y, int width, int height, std::uint8_t* data);

	static bool saveFramebufferToFile(const Vector2i& resolution, const UString& fileName, const ImageSaveFormat& imageSaveFormat, bool discardAlpha);

	static void checkForErrors(const UString& message, const UString& method, const UString& file, int line, const std::vector<UString>& identifiers);

	static void checkForErrors(const UString& message, const UString& method, const UString& file, int line, const UString& identifier = "");

	static void printInfo(bool printExtensions);

} Opengl;


inline void Opengl::readPixelsRgba(const Vector2i& pos, const Vector2i& size, std::uint8_t* data)
{
	Opengl::readPixelsRgba(pos.x, pos.y, size.x, size.y, data);
}


inline void Opengl::readPixelsRgba(int x, int y, int width, int height, std::uint8_t* data)
{
	glReadPixels(x, y, width, height,
				 GL_RGBA, GL_UNSIGNED_BYTE, data);
	GL_CHECK_FOR_ERRORS("glReadPixels", "");
}


inline void Opengl::depthWrite(bool enabled)
{
	if (enabled == true) {
		glDepthMask(GL_TRUE);
		GL_CHECK_FOR_ERRORS("", "");
	}
	else {
		glDepthMask(GL_FALSE);
		GL_CHECK_FOR_ERRORS("", "");
	}
}


inline void Opengl::depthTest(bool enabled)
{
	if (enabled == true) {
		glDepthFunc(GL_LEQUAL);
		GL_CHECK_FOR_ERRORS("", "");

		glEnable(GL_DEPTH_TEST);
		GL_CHECK_FOR_ERRORS("", "");
	}
	else {
		glDisable(GL_DEPTH_TEST);
		GL_CHECK_FOR_ERRORS("", "");
	}
}


inline void Opengl::blend(BlendMode blendMode)
{
	switch (blendMode)
	{
	case BlendMode::None:
		blendNone();
		break;
	case BlendMode::Transcluent:
		blendTranscluent();
		break;
	case BlendMode::Additive:
		blendAdditive();
		break;
	case BlendMode::Subtractive:
		blendSubtractive();
		break;
	case BlendMode::ReverseSubtractive:
		blendReverseSubtractive();
		break;
	case BlendMode::Undefined:
		VERSO_ASSERT("verso-3d", false && "Blendmode::Undefined given!");
		break;
	default:
		VERSO_ASSERT("verso-3d", false && "Unknown Blendmode given!");
	}
}


inline void Opengl::blendNone()
{
	glDisable(GL_BLEND);
	GL_CHECK_FOR_ERRORS("", "");
	glBlendEquation(GL_FUNC_ADD);
	GL_CHECK_FOR_ERRORS("", "");
}


inline void Opengl::blendTranscluent()
{
	glEnable(GL_BLEND);
	GL_CHECK_FOR_ERRORS("", "");
	glBlendFunc(GL_DST_ALPHA, GL_ONE_MINUS_DST_ALPHA);
	GL_CHECK_FOR_ERRORS("", "");
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	GL_CHECK_FOR_ERRORS("", "");
	glBlendEquation(GL_FUNC_ADD);
	GL_CHECK_FOR_ERRORS("", "");
}


inline void Opengl::blendAdditive()
{
	glEnable(GL_BLEND);
	GL_CHECK_FOR_ERRORS("", "");
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	GL_CHECK_FOR_ERRORS("", "");
	glBlendEquation(GL_FUNC_ADD);
	GL_CHECK_FOR_ERRORS("", "");
}


inline void Opengl::blendSubtractive()
{
	glEnable(GL_BLEND);
	GL_CHECK_FOR_ERRORS("", "");
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	GL_CHECK_FOR_ERRORS("", "");
	glBlendEquation(GL_FUNC_SUBTRACT);
	GL_CHECK_FOR_ERRORS("", "");
}


inline void Opengl::blendReverseSubtractive()
{
	glEnable(GL_BLEND);
	GL_CHECK_FOR_ERRORS("", "");
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	GL_CHECK_FOR_ERRORS("", "");
	glBlendEquation(GL_FUNC_REVERSE_SUBTRACT);
	GL_CHECK_FOR_ERRORS("", "");
}


inline PolygonRenderMode Opengl::getPolygonRenderMode()
{
	GLint frontBack[2] = { 0, 0 };
	glGetIntegerv(GL_POLYGON_MODE, frontBack);
	return glenumToPolygonRenderMode(static_cast<GLenum>(frontBack[0])); // \TODO: check if this actually works
}


inline void Opengl::setPolygonRenderMode(const PolygonRenderMode& polygonRenderMode)
{
	glPolygonMode(GL_FRONT_AND_BACK, polygonRenderModeToGlenum(polygonRenderMode));
	GL_CHECK_FOR_ERRORS("", "");
}


inline PolygonFaceCullMode Opengl::getPolygonFaceCullMode()
{
	GLint faceFullModeGlenum = 0;
	glGetIntegerv(GL_CULL_FACE_MODE, &faceFullModeGlenum);
	GL_CHECK_FOR_ERRORS("", "");
	return glenumToPolygonFaceCullMode(faceFullModeGlenum);
}


inline void Opengl::setPolygonFaceCullMode(const PolygonFaceCullMode& polygonFaceCullMode)
{
	glCullFace(polygonFaceCullModeToGlenum(polygonFaceCullMode));
	GL_CHECK_FOR_ERRORS("", "");
}


inline PolygonWindingOrder Opengl::getPolygonWindingOrder()
{
	GLint windowOrderGlenum = 0;
	glGetIntegerv(GL_FRONT_FACE, &windowOrderGlenum);
	GL_CHECK_FOR_ERRORS("", "");
	return glenumToPolygonWindingOrder(windowOrderGlenum);
}


inline void Opengl::setPolygonWindingOrder(const PolygonWindingOrder& polygonWindingOrder)
{
	glFrontFace(polygonWindingOrderToGlenum(polygonWindingOrder));
	GL_CHECK_FOR_ERRORS("", "");
}


inline void Opengl::checkForErrors(const UString& message, const UString& method, const UString& file, int line, const std::vector<UString>& identifiers)
{
	UString identifiersStr;
	for (size_t i=0; i<identifiers.size(); ++i) {
		identifiersStr += identifiers[i];
		if (i != identifiers.size() - 1) {
			identifiersStr += ", ";
		}
	}
	Opengl::checkForErrors(message, method, file, line, identifiersStr);
}


} // End namespace Verso

#endif // End header guard


