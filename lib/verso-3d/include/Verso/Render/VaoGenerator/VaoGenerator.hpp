#ifndef VERSO_3D_RENDER_VAOGENERATOR_VAOGENERATOR_HPP
#define VERSO_3D_RENDER_VAOGENERATOR_VAOGENERATOR_HPP

#include <Verso/Render/Vao.hpp>
#include <Verso/Render/VaoGenerator/BufferType.hpp>
#include <Verso/Math/RgbaColorf.hpp>
#include <Verso/Math/Vector2.hpp>
#include <Verso/Math/Vector3.hpp>
#include <Verso/Render/Camera/ICamera.hpp>

namespace Verso {


class VERSO_3D_API VaoGenerator
{
public: // static
	static void cube3d(Vao& vao, const Vector3f& scale, const Vector3f& offsetPosition = Vector3f(0.0f, 0.0f, 0.0f), BufferTypes buffersToGenerate = static_cast<BufferTypes>(BufferType::Default), const RgbaColorf& color = RgbaColorf(1.0f, 0.5f, 0.2f), bool append = false);

	static void diamond2d(Vao& vao, const Vector2f& scale, const Vector2f& offsetPosition = Vector2f(0.0f, 0.0f), BufferTypes buffersToGenerate = static_cast<BufferTypes>(BufferType::Default), const RgbaColorf& color = RgbaColorf(1.0f, 0.5f, 0.2f));
	static void diamond3d(Vao& vao, const Vector3f& scale, const Vector3f& offsetPosition = Vector3f(0.0f, 0.0f, 0.0f), BufferTypes buffersToGenerate = static_cast<BufferTypes>(BufferType::Default), const RgbaColorf& color = RgbaColorf(1.0f, 0.5f, 0.2f));

	static void gridPlane3d(Vao& vao, const Vector2f& unitSize, const Vector2i& gridSize, const Vector2f& uvScale, const Vector3f& offsetPosition = Vector3f(0.0f, 0.0f, 0.0f), BufferTypes buffersToGenerate = static_cast<BufferTypes>(BufferType::Default), const RgbaColorf& color = RgbaColorf(1.0f, 0.5f, 0.2f));
	static void gridPlane3dLines(Vao& vao, const Vector2f& unitSize, const Vector2i& gridSize, const Vector2f& uvScale, const Vector3f& offsetPosition = Vector3f(0.0f, 0.0f, 0.0f), BufferTypes buffersToGenerate = static_cast<BufferTypes>(BufferType::Default), const RgbaColorf& color = RgbaColorf(1.0f, 0.5f, 0.2f));

	static void quad2d(Vao& vao, const Vector2f& size, const Vector2f& offsetPosition = Vector2f(0.0f, 0.0f), BufferTypes buffersToGenerate = static_cast<BufferTypes>(BufferType::Default), const RgbaColorf& color = RgbaColorf(1.0f, 0.5f, 0.2f), bool textureInvertY = false);
	static void quad3d(Vao& vao, const Vector2f& size, const Vector3f& offsetPosition = Vector3f(0.0f, 0.0f, 0.0f), BufferTypes buffersToGenerate = static_cast<BufferTypes>(BufferType::Default), const RgbaColorf& color = RgbaColorf(1.0f, 0.5f, 0.2f), bool textureInvertY = false);

	static void skybox3d(Vao& vaoPosX, Vao& vaoNegX, Vao& vaoPosY, Vao& vaoNegY, Vao& vaoPosZ, Vao& vaoNegZ, const ICamera& camera, const Vector3f& offsetPosition = Vector3f(0.0f, 0.0f, 0.0f));

	static void sphere3d(Vao& vao, float size, int stacks, int slices, const Vector3f& offsetPosition = Vector3f(0.0f, 0.0f, 0.0f), BufferTypes buffersToGenerate = static_cast<BufferTypes>(BufferType::Default));
};


} // End namespace Verso


#endif // End header guard

