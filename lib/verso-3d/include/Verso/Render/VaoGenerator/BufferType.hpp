#ifndef VERSO_3D_RENDER_VAOGENERATOR_BUFFERTYPE_HPP
#define VERSO_3D_RENDER_VAOGENERATOR_BUFFERTYPE_HPP

#include <Verso/System/UString.hpp>

#include <cstdint>

namespace Verso {


enum class BufferType : int8_t
{
	None = 0,
	Position = 1 << 0,
	Uv = 1 << 1,
	Normal = 1 << 2,
	Color = 1 << 3,
	UvHeightmap = 1 << 4,
	All = Position | Uv | Normal | Color | UvHeightmap,
	Default = Position | Uv | Normal
};

typedef int8_t BufferTypes;


inline BufferTypes operator |(BufferType left, BufferType right)
{
	return static_cast<BufferTypes>(static_cast<BufferTypes>(left) | static_cast<BufferTypes>(right));
}


inline BufferTypes operator |(BufferType left, BufferTypes right)
{
	return static_cast<BufferTypes>(static_cast<BufferTypes>(left) | right);
}


inline BufferTypes operator |(BufferTypes left, BufferType right)
{
	return static_cast<BufferTypes>(left | static_cast<BufferTypes>(right));
}


inline UString bufferTypesToString(BufferTypes types)
{
	if (types == static_cast<BufferTypes>(BufferType::None))
		return "None";

	UString result;
	size_t count = 0;
	if (types & static_cast<BufferTypes>(BufferType::Position)) {
		result += "Position";
		count++;
	}

	if (types & static_cast<BufferTypes>(BufferType::Uv)) {
		if (count > 0)
			result += " | ";
		result += "UV";
		count++;
	}

	if (types & static_cast<BufferTypes>(BufferType::Normal)) {
		if (count > 0)
			result += " | ";
		result += "Normal";
		count++;
	}

	if (types & static_cast<BufferTypes>(BufferType::Color)) {
		if (count > 0)
			result += " | ";
		result += "Color";
		count++;
	}

	if (types & static_cast<BufferTypes>(BufferType::UvHeightmap)) {
		if (count > 0)
			result += " | ";
		result += "UvHeightmap";
		count++;
	}

	return result;
}


inline UString bufferTypeToString(BufferType type)
{
	return bufferTypesToString(static_cast<BufferTypes>(type));
}


} // End namespace Verso

#endif // End header guard

