#ifndef VERSO_3D_RENDER_MATERIAL_MATERIALCOLOR3D_HPP
#define VERSO_3D_RENDER_MATERIAL_MATERIALCOLOR3D_HPP

#include <Verso/Render/Material/IMaterial.hpp>
#include <Verso/Render/ShaderProgram.hpp>

namespace Verso {


class MaterialColor3d : public IMaterial
{
private:
	bool created;
	RgbaColorf color;
	ShaderProgram shader3d;

public:
	VERSO_3D_API MaterialColor3d();

	MaterialColor3d(const MaterialColor3d& original) = delete;

	VERSO_3D_API MaterialColor3d(MaterialColor3d&& original);

	MaterialColor3d& operator =(const MaterialColor3d& original) = delete;

	VERSO_3D_API MaterialColor3d& operator =(MaterialColor3d&& original);

	VERSO_3D_API virtual ~MaterialColor3d() override;

public: // Interface IMaterial
	VERSO_3D_API virtual void create(IWindowOpengl& window,
									 const RgbaColorf& color,
									 const UString& vertexSourceFileName = "",
									 const UString& fragmentSourceFileName = "",
									 const UString& geometrySourceFileName = "");

	VERSO_3D_API virtual void destroy() VERSO_NOEXCEPT override;

	VERSO_3D_API virtual bool isCreated() const override;

	VERSO_3D_API virtual void changeShaders(const UString& vertexSourceFileName, const UString& fragmentSourceFileName) override;

	VERSO_3D_API virtual void changeShaders(const UString& vertexSourceFileName, const UString& fragmentSourceFileName, const UString& geometrySourceFileName) override;

	VERSO_3D_API virtual void update(const FrameTimestamp& time) override;

	VERSO_3D_API virtual void apply(IWindowOpengl& window,
									const FrameTimestamp& time, const ICamera& camera,
									const std::vector<DirectionalLight>& directionalLights,
									const std::vector<PointLight>& pointLights,
									const std::vector<SpotLight>& spotLights) override;

	VERSO_3D_API virtual void apply(IWindowOpengl& window,
									const FrameTimestamp& time,
									const Matrix4x4f& viewMatrix, const Matrix4x4f& projectionMatrix, const Vector3f& position,
									const std::vector<DirectionalLight>& directionalLights,
									const std::vector<PointLight>& pointLights,
									const std::vector<SpotLight>& spotLights) override;

	VERSO_3D_API virtual void updateModelMatrix(const Matrix4x4f& model) override;


	VERSO_3D_API virtual UString getType() const override
	{
		return getTypeStatic();
	}


	VERSO_3D_API static UString getTypeStatic()
	{
		return "color3d";
	}


public: // toString (Interface IMaterial)
	VERSO_3D_API virtual UString toString(const UString& newLinePadding) const override;

	VERSO_3D_API virtual UString toStringDebug(const UString& newLinePadding) const override;
};


} // End namespace Verso


#endif // End header guard

