#ifndef VERSO_3D_RENDER_MATERIAL_MATERIALTEXTUREPHONG3D_HPP
#define VERSO_3D_RENDER_MATERIAL_MATERIALTEXTUREPHONG3D_HPP

#include <Verso/Render/Material/IMaterial.hpp>
#include <Verso/GrinderKit/Params/PhongMaterialParam.hpp>
#include <Verso/Render/ShaderProgram.hpp>
#include <Verso/Render/Texture.hpp>

namespace Verso {


class MaterialTexturePhong3d : public IMaterial
{
private:
	bool created;
	Texture diffuseTexture;
	PhongMaterialParam phongMaterial;
	ShaderProgram shader3d;

public:
	VERSO_3D_API MaterialTexturePhong3d();

	MaterialTexturePhong3d(const MaterialTexturePhong3d& original) = delete;

	VERSO_3D_API MaterialTexturePhong3d(MaterialTexturePhong3d&& original);

	MaterialTexturePhong3d& operator =(const MaterialTexturePhong3d& original) = delete;

	VERSO_3D_API MaterialTexturePhong3d& operator =(MaterialTexturePhong3d&& original);

	VERSO_3D_API virtual ~MaterialTexturePhong3d() override;

public: // Interface IMaterial
	VERSO_3D_API virtual void create(IWindowOpengl& window,
									 const UString& diffuseTextureFileName,
									 const PhongMaterialParam& phongMaterialParam,
									 const UString& vertexSourceFileName = "",
									 const UString& fragmentSourceFileName = "",
									 const UString& geometrySourceFileName = "");

	VERSO_3D_API virtual void destroy() VERSO_NOEXCEPT override;

	VERSO_3D_API virtual bool isCreated() const override;

	VERSO_3D_API virtual void changeShaders(const UString& vertexSourceFileName, const UString& fragmentSourceFileName) override;

	VERSO_3D_API virtual void changeShaders(const UString& vertexSourceFileName, const UString& fragmentSourceFileName, const UString& geometrySourceFileName) override;

	VERSO_3D_API virtual void update(const FrameTimestamp& time) override;

	VERSO_3D_API virtual void apply(IWindowOpengl& window,
									const FrameTimestamp& time, const ICamera& camera,
									const std::vector<DirectionalLight>& directionalLights,
									const std::vector<PointLight>& pointLights,
									const std::vector<SpotLight>& spotLights) override;

	VERSO_3D_API virtual void apply(IWindowOpengl& window,
									const FrameTimestamp& time,
									const Matrix4x4f& viewMatrix, const Matrix4x4f& projectionMatrix, const Vector3f& position,
									const std::vector<DirectionalLight>& directionalLights,
									const std::vector<PointLight>& pointLights,
									const std::vector<SpotLight>& spotLights) override;

	VERSO_3D_API virtual void updateModelMatrix(const Matrix4x4f& model) override;


	VERSO_3D_API virtual UString getType() const override
	{
		return getTypeStatic();
	}


	VERSO_3D_API static UString getTypeStatic()
	{
		return "texturephong3d";
	}


public: // toString (Interface IMaterial)
	VERSO_3D_API virtual UString toString(const UString& newLinePadding) const override;

	VERSO_3D_API virtual UString toStringDebug(const UString& newLinePadding) const override;
};


} // End namespace Verso


#endif // End header guard

