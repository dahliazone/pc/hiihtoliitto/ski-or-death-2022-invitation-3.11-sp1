#ifndef VERSO_3D_RENDER_MATERIAL_MATERIALLIGHTMAPSPHONG3D_GRIDHEIGHTMAPDISPLACEMENT_SINWAVE_HPP
#define VERSO_3D_RENDER_MATERIAL_MATERIALLIGHTMAPSPHONG3D_GRIDHEIGHTMAPDISPLACEMENT_SINWAVE_HPP

#include <Verso/Render/Material/IMaterial.hpp>
#include <Verso/GrinderKit/Params/PhongMaterialParam.hpp>
#include <Verso/Render/ShaderProgram.hpp>
#include <Verso/Render/Texture.hpp>

namespace Verso {


class MaterialLightmapsPhong3d_GridHeightmapDisplacement_SinWave : public IMaterial
{
private:
	bool created;
	Texture diffuseTexture;
	Texture specularTexture;
	Texture heightmapTexture;
	float maxHeight;
	Vector2f waveSpeedXZ;
	float waveSpeedY;
	Vector2f waveOffsetXZ;
	PhongMaterialParam phongMaterial;
	ShaderProgram shader3d;

public:
	VERSO_3D_API MaterialLightmapsPhong3d_GridHeightmapDisplacement_SinWave();

	MaterialLightmapsPhong3d_GridHeightmapDisplacement_SinWave(const MaterialLightmapsPhong3d_GridHeightmapDisplacement_SinWave& original) = delete;

	VERSO_3D_API MaterialLightmapsPhong3d_GridHeightmapDisplacement_SinWave(MaterialLightmapsPhong3d_GridHeightmapDisplacement_SinWave&& original) noexcept;

	MaterialLightmapsPhong3d_GridHeightmapDisplacement_SinWave& operator =(const MaterialLightmapsPhong3d_GridHeightmapDisplacement_SinWave& original) = delete;

	VERSO_3D_API MaterialLightmapsPhong3d_GridHeightmapDisplacement_SinWave& operator =(MaterialLightmapsPhong3d_GridHeightmapDisplacement_SinWave&& original) noexcept;

	VERSO_3D_API virtual ~MaterialLightmapsPhong3d_GridHeightmapDisplacement_SinWave() override;

public: // MaterialLightmapsPhong3d_GridHeightmapDisplacement_SinWave specific
	VERSO_3D_API virtual void setWaveSpeedXZ(const Vector2f& waveSpeedXZ);

	VERSO_3D_API virtual void setWaveSpeedY(float waveSpeedY);

public: // Interface IMaterial
	VERSO_3D_API virtual void create(IWindowOpengl& window,
									 const UString& diffuseTextureFileName,
									 const UString& specularTextureFileName,
									 const UString& heightmapTextureFileName,
									 float maxHeight,
									 const Vector2f& waveSpeedXZ,
									 float waveSpeedY,
									 const PhongMaterialParam& phongMaterialParam,
									 const UString& vertexSourceFileName = "",
									 const UString& fragmentSourceFileName = "",
									 const UString& geometrySourceFileName = "");

	VERSO_3D_API virtual void destroy() VERSO_NOEXCEPT override;

	VERSO_3D_API virtual bool isCreated() const override;

	VERSO_3D_API virtual void changeShaders(const UString& vertexSourceFileName, const UString& fragmentSourceFileName) override;

	VERSO_3D_API virtual void changeShaders(const UString& vertexSourceFileName, const UString& fragmentSourceFileName, const UString& geometrySourceFileName) override;

	VERSO_3D_API virtual void update(const FrameTimestamp& time) override;

	VERSO_3D_API virtual void apply(IWindowOpengl& window,
									const FrameTimestamp& time, const ICamera& camera,
									const std::vector<DirectionalLight>& directionalLights,
									const std::vector<PointLight>& pointLights,
									const std::vector<SpotLight>& spotLights) override;

	VERSO_3D_API virtual void apply(IWindowOpengl& window,
									const FrameTimestamp& time,
									const Matrix4x4f& viewMatrix, const Matrix4x4f& projectionMatrix, const Vector3f& position,
									const std::vector<DirectionalLight>& directionalLights,
									const std::vector<PointLight>& pointLights,
									const std::vector<SpotLight>& spotLights) override;

	VERSO_3D_API virtual void updateModelMatrix(const Matrix4x4f& model) override;


	VERSO_3D_API virtual UString getType() const override
	{
		return getTypeStatic();
	}


	VERSO_3D_API static UString getTypeStatic()
	{
		return "lightmapsphong3d_gridheightmapdisplacement_sinwave";
	}


public: // toString (Interface IMaterial)
	VERSO_3D_API virtual UString toString(const UString& newLinePadding) const override;

	VERSO_3D_API virtual UString toStringDebug(const UString& newLinePadding) const override;
};


} // End namespace Verso


#endif // End header guard

