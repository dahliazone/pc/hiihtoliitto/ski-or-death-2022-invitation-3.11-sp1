#ifndef VERSO_3D_RENDER_MATERIAL_IMATERIAL_HPP
#define VERSO_3D_RENDER_MATERIAL_IMATERIAL_HPP

#include <Verso/System/UString.hpp>
#include <Verso/Render/Camera/ICamera.hpp>
#include <Verso/Render/Light.hpp>
#include <Verso/Time/FrameTimestamp.hpp>

namespace Verso {


class VERSO_3D_API IMaterial // \TODO: Warning: IMaterial has out-of-line virtual method definitions; its vtable will be emitted in every translation unit
{
public:
	virtual ~IMaterial() {}

	//virtual void create(IWindowOpengl& window, ...) = 0; // material dependent signature
	virtual void destroy() VERSO_NOEXCEPT = 0;

	virtual bool isCreated() const = 0;

	virtual void changeShaders(const UString& vertexSourceFileName, const UString& fragmentSourceFileName) = 0;

	virtual void changeShaders(const UString& vertexSourceFileName, const UString& fragmentSourceFileName, const UString& geometrySourceFileName) = 0;

	virtual void update(const FrameTimestamp& time) = 0;

	virtual void apply(IWindowOpengl& window,
					   const FrameTimestamp& time, const ICamera& camera,
					   const std::vector<DirectionalLight>& directionalLights,
					   const std::vector<PointLight>& pointLights,
					   const std::vector<SpotLight>& spotLights) = 0;

	virtual void apply(IWindowOpengl& window,
					   const FrameTimestamp& time,
					   const Matrix4x4f& viewMatrix, const Matrix4x4f& projectionMatrix, const Vector3f& position,
					   const std::vector<DirectionalLight>& directionalLights,
					   const std::vector<PointLight>& pointLights,
					   const std::vector<SpotLight>& spotLights) = 0;

	virtual void updateModelMatrix(const Matrix4x4f& model) = 0;

	virtual UString getType() const = 0;

public: // toString
	virtual UString toString(const UString& newLinePadding) const = 0;

	virtual UString toStringDebug(const UString& newLinePadding) const = 0;


	friend std::ostream& operator <<(std::ostream& ost, const IMaterial& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso


#endif // End header guard

