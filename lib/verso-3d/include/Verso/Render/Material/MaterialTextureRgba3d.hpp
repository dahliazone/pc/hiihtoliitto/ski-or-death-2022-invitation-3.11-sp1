#ifndef VERSO_3D_RENDER_MATERIAL_MATERIALTEXTURERGBA3D_HPP
#define VERSO_3D_RENDER_MATERIAL_MATERIALTEXTURERGBA3D_HPP

#include <Verso/Render/Material/IMaterial.hpp>
#include <Verso/Render/ShaderProgram.hpp>
#include <Verso/Render/Texture.hpp>

namespace Verso {


class MaterialTextureRgba3d : public IMaterial
{
private:
	bool created;
	Texture diffuseTexture;
	RgbaColorf diffuseColor;
	ShaderProgram shader3d;

public:
	VERSO_3D_API MaterialTextureRgba3d();

	MaterialTextureRgba3d(const MaterialTextureRgba3d& original) = delete;

	VERSO_3D_API MaterialTextureRgba3d(MaterialTextureRgba3d&& original);

	MaterialTextureRgba3d& operator =(const MaterialTextureRgba3d& original) = delete;

	VERSO_3D_API MaterialTextureRgba3d& operator =(MaterialTextureRgba3d&& original);

	VERSO_3D_API virtual ~MaterialTextureRgba3d() override;

public: // Interface IMaterial
	VERSO_3D_API virtual void create(IWindowOpengl& window,
									 const UString& diffuseTextureFileName,
									 const UString& vertexSourceFileName = "",
									 const UString& fragmentSourceFileName = "",
									 const UString& geometrySourceFileName = "");

	VERSO_3D_API virtual void destroy() VERSO_NOEXCEPT override;

	VERSO_3D_API virtual bool isCreated() const override;

	VERSO_3D_API virtual void changeShaders(const UString& vertexSourceFileName, const UString& fragmentSourceFileName) override;

	VERSO_3D_API virtual void changeShaders(const UString& vertexSourceFileName, const UString& fragmentSourceFileName, const UString& geometrySourceFileName) override;

	VERSO_3D_API virtual void update(const FrameTimestamp& time) override;

	VERSO_3D_API virtual void apply(IWindowOpengl& window,
									const FrameTimestamp& time, const ICamera& camera,
									const std::vector<DirectionalLight>& directionalLights,
									const std::vector<PointLight>& pointLights,
									const std::vector<SpotLight>& spotLights) override;

	VERSO_3D_API virtual void apply(IWindowOpengl& window,
									const FrameTimestamp& time, const Matrix4x4f& viewMatrix, const Matrix4x4f& projectionMatrix, const Vector3f& position,
									const std::vector<DirectionalLight>& directionalLights,
									const std::vector<PointLight>& pointLights,
									const std::vector<SpotLight>& spotLights) override;

	VERSO_3D_API virtual void updateModelMatrix(const Matrix4x4f& model) override;


	VERSO_3D_API virtual UString getType() const override
	{
		return getTypeStatic();
	}


	VERSO_3D_API static UString getTypeStatic()
	{
		return "texturergba3d";
	}


public: // toString (Interface IMaterial)
	VERSO_3D_API virtual UString toString(const UString& newLinePadding) const override;

	VERSO_3D_API virtual UString toStringDebug(const UString& newLinePadding) const override;

public: // Interface MaterialTextureRgba3d
	VERSO_3D_API virtual void updateDiffuseColor(const RgbaColorf& diffuseColor);

	VERSO_3D_API virtual void updateAlpha(float alpha);

public: // getters & setters
	VERSO_3D_API virtual const Texture& getDiffuseTexture() const;

	VERSO_3D_API virtual Texture& getDiffuseTexture();
};


} // End namespace Verso


#endif // End header guard

