#ifndef VERSO_3D_RENDER_FONT_BITMAPFONTFIXEDWIDTH_HPP
#define VERSO_3D_RENDER_FONT_BITMAPFONTFIXEDWIDTH_HPP

#include <Verso/Math/RefScale.hpp>
#include <Verso/Display/IWindowOpengl.hpp>
#include <Verso/Render/Texture.hpp>
#include <Verso/Render/Camera/ICamera.hpp>
#include <Verso/Render/ShaderProgram.hpp>
#include <Verso/Render/Vao/Vao.hpp>

namespace Verso {


class BitmapFontFixedWidth
{
private:
	bool created;
	Vao* vao;
	Texture* fontTexture;
	size_t firstFontCharacterIndex;
	Vector2f characterTextureUvSize;
	ShaderProgram* fontShader;
	ShaderProgram* fontShaderInvididual;
	UString text;
	size_t tabSize;

public:
	VERSO_3D_API BitmapFontFixedWidth();

	BitmapFontFixedWidth(const BitmapFontFixedWidth& original) = delete;

	VERSO_3D_API BitmapFontFixedWidth(BitmapFontFixedWidth&& original) noexcept;

	BitmapFontFixedWidth& operator =(const BitmapFontFixedWidth& original) = delete;

	VERSO_3D_API BitmapFontFixedWidth& operator =(BitmapFontFixedWidth&& original) noexcept;

	VERSO_3D_API virtual ~BitmapFontFixedWidth();

public:
	// Loads the given image and assumes ASCII front is spread out in 16x16 grid without borders (width can differ from height).
	VERSO_3D_API void createFromFile(IWindowOpengl& window, const UString& fileName,
						const MinFilter& minFilter = MinFilter::Linear,
						const MagFilter& magFilter = MagFilter::Linear,
						const Vector2f& characterTextureUvSize = Vector2f(1.0f / 16.0f, 1.0f / 16.0f),
						size_t firstFontCharacterIndex = 0,
						size_t tabSize = 4);

	VERSO_3D_API void destroy() VERSO_NOEXCEPT;

public:
	VERSO_3D_API const UString& getText() const;

	// Update text for renderText()
	VERSO_3D_API void updateText(const UString& text, const Vector2f& relativeSpacing = Vector2f(0.1f, 0.1f), bool textureInvertY = false);

	// Render text fast (you need to call updateText before calling this)
	VERSO_3D_API void renderText(
			IWindowOpengl& window, ICamera& camera,
			const Vector3f& point,
			const Vector2f& relativeDestCharacterSize = Vector2f(0.05f, 0.05f),
			RefScale refScale = RefScale::ViewportSize_KeepAspectRatio_FromX,
			float alpha = 1.0f,
			Degree angleZ = 0.0f);

	// Renders letters individually and rotate with angleZ happens on actual letters (very slow for large amount of text but immediate style)
	VERSO_3D_API void renderTextIndividualLetters(
			IWindowOpengl& window, ICamera& camera, const UString& text,
			const Vector3f& point,
			const Vector2f& relativeDestCharacterSize = Vector2f(0.05f, 0.05f),
			const Vector2f& relativeSpacing = Vector2f(0.01f, 0.01f),
			RefScale refScale = RefScale::ViewportSize_KeepAspectRatio_FromX,
			float alpha = 1.0f,
			Degree angleZ = 0.0f,
			bool textureInvertY = false);

	// Render single character (very slow for large amount of text but immediate style)
	VERSO_3D_API void renderCharacter(
			IWindowOpengl& window, ICamera& camera, int characterIndex,
			const Vector3f& point,
			const Vector2f& relativeDestCharacterSize = Vector2f(0.05f, 0.05f),
			RefScale refScale = RefScale::ViewportSize_KeepAspectRatio_FromX,
			float alpha = 1.0f,
			Degree angleZ = 0.0f,
			bool textureInvertY = false);

private:
	VERSO_3D_API void appendCharacterToVao(int characterIndex, const Vector3f& offsetPosition, const Vector2f& size, bool textureInvertY);
};


} // End namespace Verso

#endif // End header guard

