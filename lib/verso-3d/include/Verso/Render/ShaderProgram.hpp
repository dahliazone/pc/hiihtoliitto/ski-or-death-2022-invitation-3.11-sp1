#ifndef VERSO_3D_RENDER_SHADERPROGRAM_HPP
#define VERSO_3D_RENDER_SHADERPROGRAM_HPP

#include <Verso/Render/Shader/Shader.hpp>
#include <Verso/Render/Shader/ShaderAttribute.hpp>
#include <Verso/Render/Shader/ShaderUniformType.hpp>
#include <Verso/Render/Shader/ShaderProgramDebugger.hpp>
#include <Verso/Math/RgbaColorf.hpp>
#include <Verso/Math/Vector2.hpp>
#include <Verso/Math/Vector3.hpp>
#include <Verso/Math/Vector4.hpp>
#include <Verso/Math/Matrix4x4f.hpp>
#include <Verso/System/UString.hpp>
#include <vector>
#include <map>

namespace Verso {


class ShaderProgram
{
private:
	bool created;
	GLuint handle;
	Shader vertex;
	Shader fragment;
	Shader geometry;
	std::vector<UString> sourceFileNames;
	std::vector<ShaderAttribute> attributes;
	std::map<UString, GLint> uniforms;
	bool linked;
	ShaderProgramDebugger debugger;
	char* sourceFileNamesCStr;

public:
	VERSO_3D_API ShaderProgram();

	ShaderProgram(const ShaderProgram& original) = delete;

	VERSO_3D_API ShaderProgram(ShaderProgram&& original);

	ShaderProgram& operator =(const ShaderProgram& original) = delete;

	VERSO_3D_API ShaderProgram& operator =(ShaderProgram&& original);

	VERSO_3D_API ~ShaderProgram();

public:
	VERSO_3D_API void createFromFiles(const UString& vertexSourceFileName, const UString& fragmentSourceFileName, bool bindDefaultAttributes = true);

	VERSO_3D_API void createFromFiles(const UString& vertexSourceFileName, const UString& fragmentSourceFileName, const UString& geometrySourceFileName, bool bindDefaultAttributes = true);

	VERSO_3D_API void createFromStrings(const UString& vertexSource, const UString& fragmentSource, bool bindDefaultAttributes = true);

	VERSO_3D_API void createFromStrings(const UString& vertexSource, const UString& fragmentSource, const UString& geometrySource, bool bindDefaultAttributes = true);

	VERSO_3D_API void destroy() VERSO_NOEXCEPT;

	VERSO_3D_API bool isCreated() const;

	VERSO_3D_API void bindAttribLocation(const ShaderAttribute& attribute);

	VERSO_3D_API void bindFragDataLocation(GLuint location, const UString& name);

	VERSO_3D_API void linkProgram();

	VERSO_3D_API void useProgram();

public:
	VERSO_3D_API void setUniformi(const UString& name, GLint x, bool required = true);

	VERSO_3D_API void setUniformu(const UString& name, GLuint x, bool required = true);

	VERSO_3D_API void setUniformf(const UString& name, GLfloat x, bool required = true);

	VERSO_3D_API void setUniformi(const UString& name, GLint x, GLint y, bool required = true);

	VERSO_3D_API void setUniformu(const UString& name, GLuint x, GLuint y, bool required = true);

	VERSO_3D_API void setUniformf(const UString& name, GLfloat x, GLfloat y, bool required = true);

	VERSO_3D_API void setUniformi(const UString& name, GLint x, GLint y, GLint z, bool required = true);

	VERSO_3D_API void setUniformu(const UString& name, GLuint x, GLuint y, GLuint z, bool required = true);

	VERSO_3D_API void setUniformf(const UString& name, GLfloat x, GLfloat y, GLfloat z, bool required = true);

	VERSO_3D_API void setUniformi(const UString& name, GLint x, GLint y, GLint z, GLint w, bool required = true);

	VERSO_3D_API void setUniformu(const UString& name, GLuint x, GLuint y, GLuint z, GLuint w, bool required = true);

	VERSO_3D_API void setUniformf(const UString& name, GLfloat x, GLfloat y, GLfloat z, GLfloat w, bool required = true);

	VERSO_3D_API void setUniform(const UString& name, const Vector2i& vec, bool required = true);

	VERSO_3D_API void setUniform(const UString& name, const Vector2u& vec, bool required = true);

	VERSO_3D_API void setUniform(const UString& name, const Vector2f& vec, bool required = true);

	VERSO_3D_API void setUniform(const UString& name, const Vector3i& vec, bool required = true);

	VERSO_3D_API void setUniform(const UString& name, const Vector3u& vec, bool required = true);

	VERSO_3D_API void setUniform(const UString& name, const Vector3f& vec, bool required = true);

	VERSO_3D_API void setUniform(const UString& name, const Vector4i& vec, bool required = true);

	VERSO_3D_API void setUniform(const UString& name, const Vector4u& vec, bool required = true);

	VERSO_3D_API void setUniform(const UString& name, const Vector4f& vec, bool required = true);

	VERSO_3D_API void setUniform(const UString& name, const Matrix4x4f& mat, bool required = true);

	VERSO_3D_API void setUniformRgb(const UString& name, const RgbaColorf& color, bool required = true);

	VERSO_3D_API void setUniformRgba(const UString& name, const RgbaColorf& color, bool required = true);

	VERSO_3D_API void setUniform(const UString& name, const std::vector<Vector2f>& vecs, bool required = true);

	VERSO_3D_API void setUniform(const UString& name, const std::vector<Vector3f>& vecs, bool required = true);

	VERSO_3D_API void setUniform(const UString& name, const std::vector<Vector4f>& vecs, bool required = true);

	VERSO_3D_API void setUniform(const UString& name, const std::vector<Vector2i>& vecs, bool required = true);

	VERSO_3D_API void setUniform(const UString& name, const std::vector<Vector3i>& vecs, bool required = true);

	VERSO_3D_API void setUniform(const UString& name, const std::vector<Vector4i>& vecs, bool required = true);

	VERSO_3D_API void setUniform(const UString& name, const std::vector<Vector2u>& vecs, bool required = true);

	VERSO_3D_API void setUniform(const UString& name, const std::vector<Vector3u>& vecs, bool required = true);

	VERSO_3D_API void setUniform(const UString& name, const std::vector<Vector4u>& vecs, bool required = true);

	VERSO_3D_API void setUniformMatrix4fv(const UString& name, GLsizei count, bool transpose, const GLfloat* value, bool required = true);

	//VERSO_3D_API void setUniform(const UString& name, const sf::Transform& transform, bool required = true)

	//VERSO_3D_API void setUniform(const UString& name, const Texture& texture, bool required = true)

	//VERSO_3D_API void setUniform(const UString& name, CurrentTextureType, bool required = true)

	VERSO_3D_API void setUniform1fv(const UString& name, GLsizei count, const GLfloat* values, bool required = true);

	// glUniform2fv

	VERSO_3D_API void setUniform3fv(const UString& name, GLsizei count, const GLfloat* values, bool required = true);

	// glUniform4fv

	// glUniform1iv

	// glUniform2iv

	// glUniform3iv

	// glUniform4iv

	// glUniform1uiv

	// glUniform2uiv

	// glUniform3uiv

	// glUniform4uiv

	// glUniformMatrix2fv

	// glUniformMatrix3fv

	// glUniformMatrix2x3fv

	// glUniformMatrix3x2fv

	// glUniformMatrix2x4fv

	// glUniformMatrix3x4fv

	// glUniformMatrix4x3fv

	//VERSO_3D_API bool loadAndLinkDefaultShader()

	VERSO_3D_API void printActiveUniforms();

private:
	void createProgram(bool useGeometry, bool bindDefaultAttributes = true);

	bool validateProgram();

	GLint getUniformHandle(const UString& name);

	GLuint getUniformBlockIndex(const UString& name);

	const char* sourceFileNamesToCString();

public: // toString
	VERSO_3D_API UString toString(const UString& newLinePadding) const;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const ShaderProgram& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

