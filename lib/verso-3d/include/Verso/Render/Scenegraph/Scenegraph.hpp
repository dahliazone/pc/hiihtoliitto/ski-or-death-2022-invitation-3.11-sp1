#ifndef VERSO_3D_RENDER_SCENEGRAPH_SCENEGRAPH_HPP
#define VERSO_3D_RENDER_SCENEGRAPH_SCENEGRAPH_HPP

#include <Verso/Render/Scenegraph/Entity.hpp>

namespace Verso {


class VERSO_3D_API Scenegraph
{
public:
	Entity root;

public:
	Scenegraph()
	{
	}


	~Scenegraph()
	{
		destroy();
	}


	bool create()
	{
		VERSO_FAIL("verso-3d");
		return false;
	}


	void destroy() VERSO_NOEXCEPT
	{
		VERSO_FAIL("verso-3d");
	}


public: // toString
	UString toString() const
	{
		UString str;
		str += "TODO";
		return str;
	}


	UString toStringDebug() const
	{
		UString str("Scenegraph(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const Scenegraph& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso


#endif // End header guard

