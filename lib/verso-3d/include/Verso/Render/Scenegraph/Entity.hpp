#ifndef VERSO_3D_RENDER_SCENEGRAPH_ENTITY_HPP
#define VERSO_3D_RENDER_SCENEGRAPH_ENTITY_HPP

#include <Verso/verso-3d-common.hpp>
#include <Verso/System/UString.hpp>

namespace Verso {


class VERSO_3D_API Entity
{
public:
	Vector3f position;
	Vector2f size;
	// Eular Angles
	Degree yaw;
	Degree pitch;
	Degree roll;
	std::vector<Entity*> children;

	int camera;  // the index of the camera referenced by this node
	int mesh;

	std::string name;
	std::vector<int> children;
	std::vector<double> rotation;     // length must be 0 or 4
	std::vector<double> scale;        // length must be 0 or 3
	std::vector<double> translation;  // length must be 0 or 3
	std::vector<double> matrix;       // length must be 0 or 16

public:
	Entity()
	{
	}


	~Entity()
	{
		destroy();
	}


	bool create()
	{
		VERSO_FAIL("verso-3d");
		return false;
	}


	void destroy() VERSO_NOEXCEPT
	{
		VERSO_FAIL("verso-3d");
	}


public: // toString
	UString toString() const
	{
		UString str;
		str += "TODO";
		return str;
	}


	UString toStringDebug() const
	{
		UString str("Entity(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const Entity& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso


#endif // End header guard

