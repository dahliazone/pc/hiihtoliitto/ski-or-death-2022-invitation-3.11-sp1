#ifndef VERSO_3D_RENDER_MATERIAL_HPP
#define VERSO_3D_RENDER_MATERIAL_HPP


#include <Verso/Render/Material/IMaterial.hpp>
#include <Verso/Render/Material/MaterialColor3d.hpp>
#include <Verso/Render/Material/MaterialColorPhong3d.hpp>
#include <Verso/Render/Material/MaterialLightmapsPhong3d.hpp>
#include <Verso/Render/Material/MaterialLightmapsPhong3d_GridHeightmapDisplacement.hpp>
#include <Verso/Render/Material/MaterialLightmapsPhong3d_GridHeightmapDisplacement_SinWave.hpp>
#include <Verso/Render/Material/MaterialLightmapsPhong3d_GridHeightmapDisplacement_Rand.hpp>
#include <Verso/Render/Material/MaterialNormalVisualizer3d.hpp>
#include <Verso/Render/Material/MaterialTexture3d.hpp>
#include <Verso/Render/Material/MaterialTextureRgba3d.hpp>
#include <Verso/Render/Material/MaterialTexturePhong3d.hpp>


#endif // End header guard

