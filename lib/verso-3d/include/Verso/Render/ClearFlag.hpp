#ifndef VERSO_3D_RENDER_CLEARFLAG_HPP
#define VERSO_3D_RENDER_CLEARFLAG_HPP

#include <Verso/System/UString.hpp>
#include <cstdint>

namespace Verso {


enum class ClearFlag : std::int32_t
{
	Unset,
	None,
	ColorBuffer,
	DepthBuffer,
	SolidColor,
	Skybox
};


// returns ClearFlag::Unset for any erroneuos string
inline ClearFlag stringToClearFlag(const UString& str)
{
	if (str.equals("None")) {
		return ClearFlag::None;
	}
	else if (str.equals("ColorBuffer")) {
		return ClearFlag::ColorBuffer;
	}
	else if (str.equals("DepthBuffer")) {
		return ClearFlag::DepthBuffer;
	}
	else if (str.equals("SolidColor")) {
		return ClearFlag::SolidColor;
	}
	else if (str.equals("Skybox")) {
		return ClearFlag::Skybox;
	}
	else {
		return ClearFlag::Unset;
	}
}


inline UString clearFlagToString(ClearFlag clearFlag)
{
	switch (clearFlag) {
	case ClearFlag::Unset:
		return "Unset";
	case ClearFlag::None:
		return "None";
	case ClearFlag::ColorBuffer:
		return "ColorBuffer";
	case ClearFlag::DepthBuffer:
		return "DepthBuffer";
	case ClearFlag::SolidColor:
		return "SolidColor";
	case ClearFlag::Skybox:
		return "Skybox";
	default:
		return "unknown value";
	}
}


} // End namespace Verso

#endif // End header guard

