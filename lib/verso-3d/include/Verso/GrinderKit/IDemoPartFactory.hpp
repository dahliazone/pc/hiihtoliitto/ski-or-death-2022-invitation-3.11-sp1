#ifndef VERSO_3D_GRINDERKIT_IDEMOPARTFACTORY_HPP
#define VERSO_3D_GRINDERKIT_IDEMOPARTFACTORY_HPP

#include <Verso/GrinderKit/DemoPart.hpp>
#include <Verso/System/JsonHelper.hpp>

namespace Verso {


class VERSO_3D_API IDemoPartFactory
{
public:
	virtual ~IDemoPartFactory() = 0;

public:
	virtual DemoPart* instance(
			const DemoPaths* demoPaths, const JSONObject& demoPartJson,
			const UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator) const = 0;
};


} // End namespace Verso

#endif // End header guard

