#ifndef VERSO_3D_GRINDERKIT_DEBUGMODE_HPP
#define VERSO_3D_GRINDERKIT_DEBUGMODE_HPP

#include <Verso/System/UString.hpp>
#include <cstdint>

namespace Verso {


enum class DebugMode : std::int8_t {
	Unset = 0,
	Editor,
	TimeControls,
	Fullscreen
};


inline UString debugModeToString(const DebugMode& debugMode)
{
	if (debugMode ==  DebugMode::Editor) {
		return "Editor";
	}
	else if (debugMode == DebugMode::TimeControls) {
		return "TimeControls";
	}
	else if (debugMode == DebugMode::Fullscreen) {
		return "Fullscreen";
	}
	else if (debugMode == DebugMode::Unset) {
		return "Unset";
	}
	else {
		return "Unknown value";
	}
}


inline DebugMode stringToDebugMode(const UString& str)
{
	if (str.equals("Editor")) {
		return DebugMode::Editor;
	}
	else if (str.equals("TimeControls")) {
		return DebugMode::TimeControls;
	}
	else if (str.equals("Fullscreen")) {
		return DebugMode::Fullscreen;
	}
	else {
		return DebugMode::Unset;
	}
}


} // End namespace Verso

#endif // End header guard

