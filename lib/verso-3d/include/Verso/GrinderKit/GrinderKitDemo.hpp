#ifndef VERSO_3D_GRINDERKIT_GRINDERKITDEMO_HPP
#define VERSO_3D_GRINDERKIT_GRINDERKITDEMO_HPP

#include <Verso/GrinderKit/IGrinderKitApp.hpp>
#include <Verso/GrinderKit/GrinderKitUi.hpp>
#include <Verso/Render/Camera/CameraTarget.hpp>
#include <Verso/Render/ShaderProgram.hpp>

namespace Verso {


class GrinderKitDemo : public IGrinderKitApp
{
public:
	bool created;
	const IDemoPartFactory* demoPartFactory;
	UString sourceJsonFileName;
	IWindowOpengl* window;
	DemoPlayer demoPlayer;
	GrinderKitUi* grinderKitUi;

	CameraTarget* camera;
//	Texture testTexture;

	ShaderProgram shader;
	Rectf imageRect;

	Fbo* fbo;
//	Texture* iChannel0;
//	Texture* iDepth0;
//	Texture* iChannel1;
//	Texture* iChannel2;

	bool quitting;

public:
	VERSO_3D_API GrinderKitDemo(const IDemoPartFactory* demoPartFactory, const UString& sourceJsonFileName);

	GrinderKitDemo(const GrinderKitDemo& original) = delete;

	VERSO_3D_API GrinderKitDemo(GrinderKitDemo&& original);

	GrinderKitDemo& operator =(const GrinderKitDemo& original) = delete;

	VERSO_3D_API GrinderKitDemo& operator =(GrinderKitDemo&& original);

	VERSO_3D_API ~GrinderKitDemo() override;

public: // IProgramOpengl interface
	VERSO_3D_API void create(IWindowOpengl& window, Audio2d& audio2d) override;

	VERSO_3D_API void reset(IWindowOpengl& window) override;

	VERSO_3D_API void destroy() VERSO_NOEXCEPT override;

	VERSO_3D_API bool isCreated() const override;

	VERSO_3D_API void handleInput(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_3D_API void handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event) override;

	VERSO_3D_API void render(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_3D_API bool isQuitting() const override;

	VERSO_3D_API void quit() override;

public: // IGrinderKitApp interface
	VERSO_3D_API UString getSourceJsonFileName() const override;

	VERSO_3D_API const DemoPlayerSettings& getDemoPlayerSettings() const override;

	VERSO_3D_API bool saveRenderFboToFile(const UString& fileName, const ImageSaveFormat& imageSaveFormat) const override;
};


} // End namespace Verso

#endif // End header guard

