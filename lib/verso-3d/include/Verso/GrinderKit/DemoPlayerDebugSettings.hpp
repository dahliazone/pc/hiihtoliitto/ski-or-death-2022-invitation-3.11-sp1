#ifndef VERSO_3D_GRINDERKIT_DEMOPLAYERDEBUGSETTINGS_HPP
#define VERSO_3D_GRINDERKIT_DEMOPLAYERDEBUGSETTINGS_HPP

#include <Verso/verso-3d-common.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/System/JsonHelper3d.hpp>
#include <Verso/GrinderKit/PlayMode.hpp>
#include <Verso/GrinderKit/DebugMode.hpp>

namespace Verso {


class DemoPlayerDebugSettings
{
public:
	PlayMode playMode;
	DebugMode debugMode;
	UString fontFileName;
	float fontSizeSmaller;
	float fontSizeBigger;
	bool playbackRangeEnabled;
	Ranged playbackRange;

public:
	VERSO_3D_API DemoPlayerDebugSettings();

	VERSO_3D_API DemoPlayerDebugSettings(const DemoPlayerDebugSettings& original);

	VERSO_3D_API DemoPlayerDebugSettings(DemoPlayerDebugSettings&& original);

	VERSO_3D_API DemoPlayerDebugSettings& operator =(const DemoPlayerDebugSettings& original);

	VERSO_3D_API DemoPlayerDebugSettings& operator =(DemoPlayerDebugSettings&& original);

	VERSO_3D_API ~DemoPlayerDebugSettings();

public: // json
	VERSO_3D_API void importJson(
			const JSONObject& debugSettingsObj, const UString& currentPathJson);

	VERSO_3D_API JSONValue* exportJson();

public: // toString
	VERSO_3D_API UString toString(const UString& newLinePadding) const;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const DemoPlayerDebugSettings& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

