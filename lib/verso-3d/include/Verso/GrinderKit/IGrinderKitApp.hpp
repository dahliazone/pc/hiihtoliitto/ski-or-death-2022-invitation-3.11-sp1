#ifndef VERSO_3D_GRINDERKIT_IGRINDERKITAPP_HPP
#define VERSO_3D_GRINDERKIT_IGRINDERKITAPP_HPP

#include <Verso/GrinderKit/IDemoPartFactory.hpp>
#include <Verso/System/IProgramOpengl.hpp>
#include <Verso/GrinderKit/DemoPlayer.hpp>

namespace Verso {


class VERSO_3D_API IGrinderKitApp : public IProgramOpengl
{
public:
	virtual ~IGrinderKitApp() = 0;

//protected:
//	IGrinderKitApp(const IGrinderKitApp&) = default;
//	IGrinderKitApp& operator=(const IGrinderKitApp&) = default;

public: // IGrinderKitApp interface
	virtual UString getSourceJsonFileName() const = 0;

	virtual const DemoPlayerSettings& getDemoPlayerSettings() const = 0;

	virtual bool saveRenderFboToFile(const UString& fileName, const ImageSaveFormat& imageSaveFormat) const = 0;

public: // IProgramOpengl interface
	// ...
};


} // End namespace Verso

#endif // End header guard

