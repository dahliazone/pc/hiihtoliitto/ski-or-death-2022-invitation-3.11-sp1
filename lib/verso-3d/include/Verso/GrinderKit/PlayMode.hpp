#ifndef VERSO_3D_GRINDERKIT_PLAYMODE_HPP
#define VERSO_3D_GRINDERKIT_PLAYMODE_HPP

#include <Verso/System/UString.hpp>
#include <cstdint>

namespace Verso {


enum class PlayMode : std::int8_t {
	Unset = 0,
	Production,
	Development
};


inline UString playModeToString(const PlayMode& playMode)
{
	if (playMode ==  PlayMode::Production) {
		return "Production";
	}
	else if (playMode == PlayMode::Development) {
		return "Development";
	}
	else if (playMode == PlayMode::Unset) {
		return "Unset";
	}
	else {
		return "Unknown value";
	}
}


inline PlayMode stringToPlayMode(const UString& str)
{
	if (str.equals("Production")) {
		return PlayMode::Production;
	}
	else if (str.equals("Development")) {
		return PlayMode::Development;
	}
	else {
		return PlayMode::Unset;
	}
}


} // End namespace Verso

#endif // End header guard

