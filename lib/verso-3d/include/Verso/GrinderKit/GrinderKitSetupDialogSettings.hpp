#ifndef VERSO_3D_GRINDERKIT_GRINDERKITSETUPDIALOGSETTINGS_HPP
#define VERSO_3D_GRINDERKIT_GRINDERKITSETUPDIALOGSETTINGS_HPP

#include <Verso/verso-3d-common.hpp>
#include <Verso/System/JsonHelper.hpp>

namespace Verso {


class GrinderKitSetupDialogSettings
{
public:
	UString windowTitle;
	UString logoFileName;
	UString fontFileName;
	RgbaColorf backgroundColor;
	UString startButtonText;
	UString quitButtonText;

public:
	VERSO_3D_API GrinderKitSetupDialogSettings();

	VERSO_3D_API GrinderKitSetupDialogSettings(const GrinderKitSetupDialogSettings& original);

	VERSO_3D_API GrinderKitSetupDialogSettings(GrinderKitSetupDialogSettings&& original);

	VERSO_3D_API GrinderKitSetupDialogSettings& operator =(const GrinderKitSetupDialogSettings& original);

	VERSO_3D_API GrinderKitSetupDialogSettings& operator =(GrinderKitSetupDialogSettings&& original);

	VERSO_3D_API ~GrinderKitSetupDialogSettings();

public: // json
	VERSO_3D_API void importJson(
			const JSONObject& setupDialogSettingsObj, const UString& currentPathJson);

	VERSO_3D_API JSONValue* exportJson();

public: // toString
	VERSO_3D_API UString toString(const UString& newLinePadding) const;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const GrinderKitSetupDialogSettings& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard
