#ifndef VERSO_3D_GRINDERKIT_PARAMS_CLEARPARAM_HPP
#define VERSO_3D_GRINDERKIT_PARAMS_CLEARPARAM_HPP

#include <Verso/System/JsonHelper3d.hpp>
#include <Verso/Math/ColorGenerator.hpp>
#include <Verso/Render/ClearFlag.hpp>

namespace Verso {

//
// Example JSON
//
// Defaults to "Solid" clearing with gray color:
//
//	"params": {
//		"clear": { }
//
//
// Doesn't clear any buffers:
//
//	"params": {
//		"clear": { "clearFlag": "None" }
//
//
// Clears color and depth buffer with given color:
//
//	"params": {
//		"clear": { "clearFlag": "SolidColor", "color": [0.45, 0.56, 0.60, 1] }
//
//
// Clears only color buffer with given color:
//
//	"params": {
//		"clear": { "clearFlag": "ColorBuffer", "color": [0.45, 0.56, 0.60, 1] }
//
//
// Clears only depth buffer:
//
//	"params": {
//		"clear": { "clearFlag": "DepthBuffer" }
//

class ClearParam
{
public:
	ClearFlag clearFlag;
	RgbaColorfKeyframes colorKeyframes;

public:
	VERSO_3D_API ClearParam(const ClearFlag& clearFlag = ClearFlag::None,
							const RgbaColorfKeyframes& colorKeyframes = RgbaColorfKeyframes(ColorGenerator::getColor(ColorRgb::Black)));

	VERSO_3D_API ClearParam(const ClearParam& original);

	VERSO_3D_API ClearParam(const ClearParam&& original);

	VERSO_3D_API ClearParam& operator =(const ClearParam& original);

	VERSO_3D_API ClearParam& operator =(ClearParam&& original) noexcept;

	VERSO_3D_API ~ClearParam();

public:
	VERSO_3D_API bool parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name = "clear", bool required = false,
											 const ClearParam& defaults = ClearParam());

	VERSO_3D_API bool parseObject(const JSONObject& object, const UString& currentPathJson,
								  const ClearParam& defaults = ClearParam());

public: // toString
	VERSO_3D_API UString toString(const UString& newLinePadding) const;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const ClearParam& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

