#ifndef VERSO_3D_GRINDERKIT_PARAMS_CAMERAPARAM_HPP
#define VERSO_3D_GRINDERKIT_PARAMS_CAMERAPARAM_HPP

#include <Verso/verso-3d-common.hpp>
#include <Verso/Render/Camera/CameraType.hpp>
#include <Verso/Render/Camera/ProjectionType.hpp>
#include <Verso/Math/FloatKeyframes.hpp>
#include <Verso/Math/Vector3Keyframes.hpp>
#include <Verso/Math/Range.hpp>
#include <JSON.h>

namespace Verso {

//
// Example JSON
//
// TODO: example
//

class CameraParam
{
public:
	CameraType cameraType;
	ProjectionType projectionType;
	Vector3fKeyframes positionKeyframes;
	Vector3fKeyframes targetKeyframes;
	Vector3fKeyframes desiredUpKeyframes;
	FloatKeyframes fovYKeyframes;
	Rangef nearFarPlane;
	bool orthographicIsRelative;
	FloatKeyframes orthographicZoomLevelKeyframes;
	FloatKeyframes orthographicRotationKeyframes;
	float orthographicLeft;
	float orthographicRight;
	float orthographicTop;
	float orthographicBottom;

public:
	VERSO_3D_API CameraParam();

	VERSO_3D_API CameraParam(const CameraParam& original);

	VERSO_3D_API CameraParam(const CameraParam&& original);

	VERSO_3D_API CameraParam(
			CameraType cameraType, ProjectionType projectionType,
			const Vector3fKeyframes& positionKeyframes = Vector3fKeyframes(Vector3f(0.0f, 0.0f, 0.0f)),
			const Vector3fKeyframes& targetKeyframes = Vector3fKeyframes(Vector3f(0.0f, 0.0f, 10.0f)),
			const Vector3fKeyframes& desiredUpKeyframes = Vector3fKeyframes(Vector3f::up()),
			const FloatKeyframes& fovYKeyframes = FloatKeyframes(90.0f), const Rangef& nearFarPlane = Rangef(0.1f, 1000.0f),
			bool orthographicIsRelative = false,
			const FloatKeyframes& orthographicZoomLevelKeyframes = FloatKeyframes(1.0f),
			const FloatKeyframes& orthographicRotationKeyframes = FloatKeyframes(0.0f),
			float orthographicLeft = -1.0f, float orthographicRight = 1.0f,
			float orthographicTop = -1.0f, float orthographicBottom = 1.0f);

	VERSO_3D_API CameraParam& operator =(const CameraParam& original);

	VERSO_3D_API CameraParam& operator =(CameraParam&& original) noexcept;

	VERSO_3D_API ~CameraParam();

public:
	VERSO_3D_API bool parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const CameraParam& defaultValue);

	VERSO_3D_API bool parseObject(const JSONObject& object, const UString& currentPathJson, const CameraParam& defaultValue);

public: // toString
	VERSO_3D_API UString toString(const UString& newLinePadding) const;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const CameraParam& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

