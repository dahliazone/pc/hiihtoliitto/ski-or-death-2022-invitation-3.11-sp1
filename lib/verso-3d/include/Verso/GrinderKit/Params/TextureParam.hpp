#ifndef VERSO_3D_GRINDERKIT_PARAMS_TEXTUREPARAM_HPP
#define VERSO_3D_GRINDERKIT_PARAMS_TEXTUREPARAM_HPP

#include <Verso/System/JsonHelper3d.hpp>

namespace Verso {


class TextureParam
{
public:
	UString sourceFileName;
	UString typeForShader;
	TexturePixelFormat pixelFormat;
	MinFilter minFilter;
	MagFilter magFilter;
	WrapStyle wrapStyleS;
	WrapStyle wrapStyleT;

public:
	VERSO_3D_API TextureParam();

	VERSO_3D_API TextureParam(const TextureParam& original);

	VERSO_3D_API TextureParam(const TextureParam&& original);

	VERSO_3D_API TextureParam(const UString& sourceFileName, const UString& typeForShader,
							  const TexturePixelFormat& pixelFormat = TexturePixelFormat::Unset,
							  const MinFilter& minFilter = MinFilter::Default, const MagFilter& magFilter = MagFilter::Default,
							  const WrapStyle& wrapStyleS = WrapStyle::Default, const WrapStyle& wrapStyleT = WrapStyle::Default);

	VERSO_3D_API TextureParam& operator =(const TextureParam& original);

	VERSO_3D_API TextureParam& operator =(TextureParam&& original) noexcept;

	VERSO_3D_API ~TextureParam();

public:
	VERSO_3D_API bool parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name = "texture", bool required = false, const TextureParam& defaultValue = TextureParam());

	VERSO_3D_API bool parseObject(const JSONObject& object, const UString& currentPathJson, const TextureParam& defaultValue = TextureParam());

public: // toString
	VERSO_3D_API UString toString(const UString& newLinePadding) const;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const TextureParam& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

