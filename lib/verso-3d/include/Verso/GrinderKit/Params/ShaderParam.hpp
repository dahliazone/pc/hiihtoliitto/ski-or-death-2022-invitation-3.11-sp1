#ifndef VERSO_3D_GRINDERKIT_PARAMS_SHADERPARAM_HPP
#define VERSO_3D_GRINDERKIT_PARAMS_SHADERPARAM_HPP

#include <Verso/verso-3d-common.hpp>
#include <Verso/System/JsonHelper.hpp>

namespace Verso {


class ShaderParam
{
public:
	UString vert;
	UString frag;
	UString geom;

public:
	VERSO_3D_API ShaderParam();

	VERSO_3D_API ShaderParam(const ShaderParam& original);

	VERSO_3D_API ShaderParam(const ShaderParam&& original);

	VERSO_3D_API ShaderParam& operator =(const ShaderParam& original);

	VERSO_3D_API ShaderParam& operator =(ShaderParam&& original) noexcept;

	VERSO_3D_API ~ShaderParam();

public:
	VERSO_3D_API bool parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name = "shader", bool required = false);

	VERSO_3D_API bool parseObject(const JSONObject& object, const UString& currentPathJson);

public: // toString
	VERSO_3D_API UString toString(const UString& newLinePadding) const;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const ShaderParam& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

