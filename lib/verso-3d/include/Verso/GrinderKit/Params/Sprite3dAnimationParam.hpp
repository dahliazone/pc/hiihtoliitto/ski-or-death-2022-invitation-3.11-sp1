#ifndef VERSO_3D_GRINDERKIT_PARAMS_SPRITE3DANIMATIONPARAM_HPP
#define VERSO_3D_GRINDERKIT_PARAMS_SPRITE3DANIMATIONPARAM_HPP

#include <Verso/System/JsonHelper3d.hpp>
#include <Verso/Render/Sprite/AnimationType.hpp>

namespace Verso {

//
// Example JSON
//
// TODO: examples
//

class Sprite3dAnimationParam
{
public:
	AnimationType type;
	int frameCount;
	bool useManualFrameOrder;
	std::vector<float> autoTiming;
	IntKeyframes manualFrameOrder;

public:
	VERSO_3D_API Sprite3dAnimationParam();

	VERSO_3D_API Sprite3dAnimationParam(const Sprite3dAnimationParam& original);

	VERSO_3D_API Sprite3dAnimationParam(const Sprite3dAnimationParam&& original);

	VERSO_3D_API Sprite3dAnimationParam& operator =(const Sprite3dAnimationParam& original);

	VERSO_3D_API Sprite3dAnimationParam& operator =(Sprite3dAnimationParam&& original) noexcept;

	VERSO_3D_API ~Sprite3dAnimationParam();

public:
	VERSO_3D_API bool parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name = "animation", bool required = false);

	VERSO_3D_API bool parseObject(const JSONObject& object, const UString& currentPathJson);

public: // toString
	VERSO_3D_API UString toString(const UString& newLinePadding) const;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const Sprite3dAnimationParam& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

