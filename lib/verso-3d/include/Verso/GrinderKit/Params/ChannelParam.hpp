#ifndef VERSO_3D_GRINDERKIT_PARAMS_CHANNELPARAM_HPP
#define VERSO_3D_GRINDERKIT_PARAMS_CHANNELPARAM_HPP

#include <Verso/verso-3d-common.hpp>
#include <Verso/System/JsonHelper.hpp>

namespace Verso {


class ChannelParam
{
public:
	std::vector<UString> sources;

public:
	VERSO_3D_API ChannelParam();

	VERSO_3D_API ChannelParam(const ChannelParam& original);

	VERSO_3D_API ChannelParam(const ChannelParam&& original);

	VERSO_3D_API ChannelParam& operator =(const ChannelParam& original);

	VERSO_3D_API ChannelParam& operator =(ChannelParam&& original) noexcept;

	VERSO_3D_API ~ChannelParam();

public:
	VERSO_3D_API bool parseAttributeAsArray(const JSONObject& object, const UString& currentPathJson, const UString& name = "channel", bool required = false);

	VERSO_3D_API bool parseArray(const JSONArray& array, const UString& currentPathJson);

public: // toString
	VERSO_3D_API UString toString(const UString& newLinePadding) const;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const ChannelParam& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

