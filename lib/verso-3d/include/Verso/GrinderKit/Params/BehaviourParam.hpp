#ifndef VERSO_3D_GRINDERKIT_PARAMS_BEHAVIOURPARAM_HPP
#define VERSO_3D_GRINDERKIT_PARAMS_BEHAVIOURPARAM_HPP

#include <Verso/System/JsonHelper3d.hpp>

namespace Verso {

//
// Example JSON
//

class BehaviourParam
{
public:
	UString type;

	float gravity;

public:
	VERSO_3D_API BehaviourParam();

	VERSO_3D_API BehaviourParam(const BehaviourParam& original);

	VERSO_3D_API BehaviourParam(const BehaviourParam&& original);

	VERSO_3D_API BehaviourParam& operator =(const BehaviourParam& original);

	VERSO_3D_API BehaviourParam& operator =(BehaviourParam&& original) noexcept;

	VERSO_3D_API ~BehaviourParam();

public:
	VERSO_3D_API bool parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name = "behaviour", bool required = false);

	VERSO_3D_API bool parseObject(const JSONObject& object, const UString& currentPathJson);

	VERSO_3D_API ParticleBehaviour* createBehaviour() const;

public: // toString
	VERSO_3D_API UString toString(const UString& newLinePadding) const;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const BehaviourParam& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

