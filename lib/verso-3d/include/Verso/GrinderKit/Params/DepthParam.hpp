#ifndef VERSO_3D_GRINDERKIT_PARAMS_DEPTHPARAM_HPP
#define VERSO_3D_GRINDERKIT_PARAMS_DEPTHPARAM_HPP

#include <Verso/System/JsonHelper3d.hpp>

namespace Verso {

//
// Example JSON
//
// Defaults to depth testing on and depth writing on:
//
//	"params": {
//		"depth": { }
//
//
// Disable depth test and depth write:
//
//	"params": {
//		"depth": { "test": false, "write": false }
//

class DepthParam
{
public:
	bool depthTest;
	bool depthWrite;

public:
	VERSO_3D_API DepthParam(bool depthTest = true, bool depthWrite = true);

	VERSO_3D_API DepthParam(const DepthParam& original);

	VERSO_3D_API DepthParam(const DepthParam&& original);

	VERSO_3D_API DepthParam& operator =(const DepthParam& original);

	VERSO_3D_API DepthParam& operator =(DepthParam&& original) noexcept;

	VERSO_3D_API ~DepthParam();

public:
	VERSO_3D_API bool parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name = "depth", bool required = false,
											 const DepthParam& defaults = DepthParam());

	VERSO_3D_API bool parseObject(const JSONObject& object, const UString& currentPathJson,
								  const DepthParam& defaults = DepthParam());

public: // toString
	VERSO_3D_API UString toString(const UString& newLinePadding) const;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const DepthParam& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

