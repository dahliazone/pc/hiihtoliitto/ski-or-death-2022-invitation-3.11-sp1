#ifndef VERSO_3D_GRINDERKIT_PARAMS_FONTFIXEDWIDTHPARAM_HPP
#define VERSO_3D_GRINDERKIT_PARAMS_FONTFIXEDWIDTHPARAM_HPP

#include <Verso/System/JsonHelper3d.hpp>
#include <Verso/Math/RgbaColorf.hpp>
#include <Verso/Render/ClearFlag.hpp>
#include <Verso/GrinderKit/Params/TextureParam.hpp>

namespace Verso {

//
// Example JSON
//
// Just load the font with defaults:
//
//	"params": {
//		"fontFixedWidth": { texture: { "source": "path/to/font.png" } }
//
//
// Set various parameters to font:
//
//	"params": {
//		"fontFixedWidth": {
//        "texture": {
//          "source": "path/to/font.png",
//          "minFilter": "Nearest",
//          "magFilter": "Nearest"
//        },
//        "firstFontCharacterIndex": 32,
//        "tabSize": 2
//      }
//

class FontFixedWidthParam
{
public:
	TextureParam texture;
	size_t firstFontCharacterIndex;
	size_t tabSize;

public:
	VERSO_3D_API FontFixedWidthParam(
			const TextureParam& texture = TextureParam(),
			size_t firstFontCharacterIndex = 0,
			size_t tabSize = 4);

	VERSO_3D_API FontFixedWidthParam(const FontFixedWidthParam& original);

	VERSO_3D_API FontFixedWidthParam(const FontFixedWidthParam&& original);

	VERSO_3D_API FontFixedWidthParam& operator =(const FontFixedWidthParam& original);

	VERSO_3D_API FontFixedWidthParam& operator =(FontFixedWidthParam&& original) noexcept;

	VERSO_3D_API ~FontFixedWidthParam();

public:
	VERSO_3D_API bool parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name = "fontFixedWidth", bool required = false);

	VERSO_3D_API bool parseObject(const JSONObject& object, const UString& currentPathJson);

public: // toString
	VERSO_3D_API UString toString(const UString& newLinePadding) const;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const FontFixedWidthParam& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

