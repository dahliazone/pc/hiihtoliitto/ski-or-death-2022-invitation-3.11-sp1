#ifndef VERSO_3D_GRINDERKIT_PARAMS_EMITTERPARAM_HPP
#define VERSO_3D_GRINDERKIT_PARAMS_EMITTERPARAM_HPP

#include <Verso/System/JsonHelper3d.hpp>
#include <Verso/Render/Particle/Emitters/ParticleEmitter.hpp>

namespace Verso {

//
// Example JSON
//

class EmitterParam
{
public:
	UString type;

	// shared
	Rangef xRange;
	Rangef yRange;
	Rangef zRange;
	Rangef angleRange;
	Rangef angleVelocityRange;
	Rangef startSizeRange;
	Rangef endSizeRange;
	Rangef totalLifeTimeRange;
	Rangef redRange;
	Rangef greenRange;
	Rangef blueRange;
	Rangef startAlphaRange;
	Rangef endAlphaRange;

	// SimpleRandom
	Rangef xVelocityRange;
	Rangef yVelocityRange;
	Rangef zVelocityRange;

	// SphericalRandom
	Rangef radiusRange;

public:
	VERSO_3D_API EmitterParam();

	VERSO_3D_API EmitterParam(const EmitterParam& original);

	VERSO_3D_API EmitterParam(const EmitterParam&& original);

	VERSO_3D_API EmitterParam& operator =(const EmitterParam& original);

	VERSO_3D_API EmitterParam& operator =(EmitterParam&& original) noexcept;

	VERSO_3D_API ~EmitterParam();

public:
	VERSO_3D_API bool parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name = "emitter", bool required = false);

	VERSO_3D_API bool parseObject(const JSONObject& object, const UString& currentPathJson);

	VERSO_3D_API ParticleEmitter* createEmitter() const;

public: // toString
	VERSO_3D_API UString toString(const UString& newLinePadding) const;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const EmitterParam& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

