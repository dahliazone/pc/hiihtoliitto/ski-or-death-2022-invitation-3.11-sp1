#ifndef VERSO_3D_GRINDERKIT_PARAMS_SPRITE3DPARAM_HPP
#define VERSO_3D_GRINDERKIT_PARAMS_SPRITE3DPARAM_HPP

#include <Verso/System/JsonHelper3d.hpp>
#include <Verso/GrinderKit/Params/TextureParam.hpp>
#include <Verso/GrinderKit/Params/Sprite3dAnimationParam.hpp>

namespace Verso {

//
// Example JSON
//
// TODO: examples
//

class Sprite3dParam
{
public:
	TextureParam texture;
	BlendMode blendMode;
	FloatKeyframes alphaKeyframes;
	Vector3fKeyframes positionKeyframes;
	Vector2fKeyframes relSizeKeyframes;
	RefScale refScale;
	FloatKeyframes yawAngleKeyframes;
	FloatKeyframes pitchAngleKeyframes;
	FloatKeyframes rollAngleKeyframes;
	Vector2f relOffset;
	IntKeyframes currentAnimationIndex;
	std::vector<Sprite3dAnimationParam> animationParams;

public:
	VERSO_3D_API Sprite3dParam();

	VERSO_3D_API Sprite3dParam(const Sprite3dParam& original);

	VERSO_3D_API Sprite3dParam(const Sprite3dParam&& original);

	VERSO_3D_API Sprite3dParam& operator =(const Sprite3dParam& original);

	VERSO_3D_API Sprite3dParam& operator =(Sprite3dParam&& original) noexcept;

	VERSO_3D_API ~Sprite3dParam();

public:
	VERSO_3D_API static std::vector<Sprite3dParam> parseAttributeAsArray(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false);

	VERSO_3D_API bool parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name = "sprite3d", bool required = false);

	VERSO_3D_API bool parseObject(const JSONObject& object, const UString& currentPathJson);

public: // toString
	VERSO_3D_API UString toString(const UString& newLinePadding) const;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const Sprite3dParam& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

