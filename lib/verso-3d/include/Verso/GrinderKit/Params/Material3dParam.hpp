#ifndef VERSO_3D_GRINDERKIT_PARAMS_MATERIAL3DPARAM_HPP
#define VERSO_3D_GRINDERKIT_PARAMS_MATERIAL3DPARAM_HPP

#include <Verso/System/JsonHelper.hpp>
#include <Verso/GrinderKit/Params/PhongMaterialParam.hpp>
#include <Verso/GrinderKit/Params/ShaderParam.hpp>
#include <Verso/Render/Material/IMaterial.hpp>

namespace Verso {


class Material3dParam
{
public:
	UString type;

	RgbaColorf color;
	PhongMaterialParam phongMaterial;
	UString diffuseTexture;
	UString specularTexture;
	UString heightmapTexture;

	RgbaColorf normalColor;
	float normalLength;
	float maxHeight;
	Vector2f waveSpeedXZ;
	float waveSpeedY;

	ShaderParam shaderOverrideParam;

public:
	VERSO_3D_API Material3dParam();

	VERSO_3D_API Material3dParam(const Material3dParam& original);

	VERSO_3D_API Material3dParam(const Material3dParam&& original);

	VERSO_3D_API Material3dParam& operator =(const Material3dParam& original);

	VERSO_3D_API Material3dParam& operator =(Material3dParam&& original) noexcept;

	VERSO_3D_API ~Material3dParam();

public:
	VERSO_3D_API bool parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name = "material3d", bool required = false);

	VERSO_3D_API bool parseObject(const JSONObject& object, const UString& currentPathJson);

	VERSO_3D_API IMaterial* createMaterial(IWindowOpengl& window) const;

public: // toString
	VERSO_3D_API UString toString(const UString& newLinePadding) const;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const Material3dParam& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

