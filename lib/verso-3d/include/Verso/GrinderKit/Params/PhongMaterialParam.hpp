#ifndef VERSO_3D_RENDER_MATERIAL_PHONGMATERIALPARAM_HPP
#define VERSO_3D_RENDER_MATERIAL_PHONGMATERIALPARAM_HPP

#include <Verso/verso-3d-common.hpp>
#include <Verso/Math/RgbaColorf.hpp>
#include <Verso/System/JsonHelper.hpp>

namespace Verso {


class PhongMaterialParam
{
public: // member variables
	RgbaColorf ambient;
	RgbaColorf diffuse;
	RgbaColorf specular;
	float shininess;

public:
	VERSO_3D_API PhongMaterialParam();

	VERSO_3D_API PhongMaterialParam(
			const RgbaColorf& ambient,
			const RgbaColorf& diffuse,
			const RgbaColorf& specular,
			float shininess);

	VERSO_3D_API PhongMaterialParam(const PhongMaterialParam& original);

	VERSO_3D_API PhongMaterialParam(PhongMaterialParam&& original);

	VERSO_3D_API PhongMaterialParam& operator =(const PhongMaterialParam& original);

	VERSO_3D_API PhongMaterialParam& operator =(PhongMaterialParam&& original);

	VERSO_3D_API virtual ~PhongMaterialParam();

public:
	VERSO_3D_API bool parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name = "phongmaterial", bool required = false);

	VERSO_3D_API bool parseObject(const JSONObject& object, const UString& currentPathJson);

public: // static default materials
	VERSO_3D_API static PhongMaterialParam emerald();

	VERSO_3D_API static PhongMaterialParam jade();

	VERSO_3D_API static PhongMaterialParam obsidian();

	VERSO_3D_API static PhongMaterialParam pearl();

	VERSO_3D_API static PhongMaterialParam ruby();

	VERSO_3D_API static PhongMaterialParam turquoise();

	VERSO_3D_API static PhongMaterialParam brass();

	VERSO_3D_API static PhongMaterialParam bronze();

	VERSO_3D_API static PhongMaterialParam chrome();

	VERSO_3D_API static PhongMaterialParam copper();

	VERSO_3D_API static PhongMaterialParam gold();

	VERSO_3D_API static PhongMaterialParam silver();

	VERSO_3D_API static PhongMaterialParam blackPlastic();

	VERSO_3D_API static PhongMaterialParam cyanPlastic();

	VERSO_3D_API static PhongMaterialParam greenPlastic();

	VERSO_3D_API static PhongMaterialParam redPlastic();

	VERSO_3D_API static PhongMaterialParam whitePlastic();

	VERSO_3D_API static PhongMaterialParam yellowPlastic();

	VERSO_3D_API static PhongMaterialParam blackRubber();

	VERSO_3D_API static PhongMaterialParam cyanRubber();

	VERSO_3D_API static PhongMaterialParam greenRubber();

	VERSO_3D_API static PhongMaterialParam redRubber();

	VERSO_3D_API static PhongMaterialParam whiteRubber();

	VERSO_3D_API static PhongMaterialParam yellowRubber();

public: // toString
	VERSO_3D_API UString toString(const UString& newLinePadding) const;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const PhongMaterialParam& right)
	{
		return ost << right.toString("");
	}
};



} // End namespace Verso

#endif // End header guard

