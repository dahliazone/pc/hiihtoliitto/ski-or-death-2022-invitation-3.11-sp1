#ifndef VERSO_3D_GRINDERKIT_PARAMS_SEQUENCERPARAM_HPP
#define VERSO_3D_GRINDERKIT_PARAMS_SEQUENCERPARAM_HPP

#include <Verso/System/JsonHelper3d.hpp>
#include <Verso/Render/Particle/Sequencers/BurstInterval.hpp>

namespace Verso {

//
// Example JSON
//

class SequencerParam
{
public:
		UString type;

		// type: BurstInterval
		SequenceType sequenceType; // SequenceType::None
		double startTime; //0.0
		double endTime; // 0.0
		bool neverEnding; // false
		double interval; // 4.0
		double duration; // 2.0
		size_t burstSize; // 0
		size_t particlesPerSecond; // 0

public:
	VERSO_3D_API SequencerParam();

	VERSO_3D_API SequencerParam(const SequencerParam& original);

	VERSO_3D_API SequencerParam(const SequencerParam&& original);

	VERSO_3D_API SequencerParam& operator =(const SequencerParam& original);

	VERSO_3D_API SequencerParam& operator =(SequencerParam&& original) noexcept;

	VERSO_3D_API ~SequencerParam();

public:
	VERSO_3D_API bool parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name = "sequencer", bool required = false);

	VERSO_3D_API bool parseObject(const JSONObject& object, const UString& currentPathJson);

	VERSO_3D_API ParticleSequencer* createParticleSequencer() const;

public: // toString
	VERSO_3D_API UString toString(const UString& newLinePadding) const;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const SequencerParam& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

