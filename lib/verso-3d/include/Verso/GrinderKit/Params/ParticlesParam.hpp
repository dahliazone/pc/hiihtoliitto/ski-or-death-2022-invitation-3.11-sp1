#ifndef VERSO_3D_GRINDERKIT_PARAMS_PARTICLESPARAM_HPP
#define VERSO_3D_GRINDERKIT_PARAMS_PARTICLESPARAM_HPP

#include <Verso/System/JsonHelper3d.hpp>
#include <Verso/Render/Particle/ParticleSystem.hpp>

namespace Verso {

//
// Example JSON
//

class ParticlesParam
{
public:
	size_t particleLimit; // 1000
	Vector3f position; // [ 0.0, 0.0, 0.0 ]
	//Material3dParam material; // \TODO: maybe also can change material and not just texture?
	UString texture; // "spacemist.png"
	bool animatedTexture;
	size_t trailSize; // 100
	double trailSaveInterval; // 0.2
	BlendMode blend; // "Additive"

public:
	VERSO_3D_API ParticlesParam();

	VERSO_3D_API ParticlesParam(const ParticlesParam& original);

	VERSO_3D_API ParticlesParam(const ParticlesParam&& original);

	VERSO_3D_API ParticlesParam& operator =(const ParticlesParam& original);

	VERSO_3D_API ParticlesParam& operator =(ParticlesParam&& original) noexcept;

	VERSO_3D_API ~ParticlesParam();

public:
	VERSO_3D_API bool parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name = "particles", bool required = false);

	VERSO_3D_API bool parseObject(const JSONObject& object, const UString& currentPathJson);

	VERSO_3D_API ParticleSystem* createParticleSystem(IWindowOpengl& window, const UString& runtimeId) const;

public: // toString
	VERSO_3D_API UString toString(const UString& newLinePadding) const;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const ParticlesParam& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

