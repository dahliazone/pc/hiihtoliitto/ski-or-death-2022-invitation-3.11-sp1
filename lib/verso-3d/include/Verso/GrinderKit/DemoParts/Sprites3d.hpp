#ifndef VERSO_3D_GRINDERKIT_DEMOPARTS_SPRITES3D_HPP
#define VERSO_3D_GRINDERKIT_DEMOPARTS_SPRITES3D_HPP

#include <Verso/GrinderKit/DemoPart.hpp>
#include <Verso/Render/Camera.hpp>
#include <Verso/Render/Sprite/Sprite3d.hpp>
#include <Verso/GrinderKit/Params/ClearParam.hpp>
#include <Verso/GrinderKit/Params/DepthParam.hpp>
#include <Verso/GrinderKit/Params/CameraParam.hpp>
#include <Verso/GrinderKit/Params/Sprite3dParam.hpp>

namespace Verso {


class Sprites3d : public DemoPart
{
private:
	// Params
	ClearParam clearParam;
	DepthParam depthParam;
	CameraParam cameraParam;
	std::vector<Sprite3dParam> sprite3dParams;

	// State
	bool created;
	bool quitting;
	IWindowOpengl* window;
	CameraTarget camera;
	std::vector<Texture*> textures;
	std::vector<Sprite3d> sprites;

public:
	VERSO_3D_API Sprites3d(
			const DemoPaths* demoPaths, const JSONObject& demoPartJson,
			const UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator);

	Sprites3d(const Sprites3d& original) = delete;

	VERSO_3D_API Sprites3d(Sprites3d&& original) noexcept;

	Sprites3d& operator =(const Sprites3d& original) = delete;

	VERSO_3D_API virtual Sprites3d& operator =(Sprites3d&& original) noexcept;

	VERSO_3D_API virtual ~Sprites3d() override;

public: // IProgramOpengl interface
	VERSO_3D_API virtual void create(IWindowOpengl& window, Audio2d& audio2d) override;

	VERSO_3D_API virtual void reset(IWindowOpengl& window) override;

	VERSO_3D_API virtual void destroy() noexcept override;

	VERSO_3D_API virtual bool isCreated() const override;

	VERSO_3D_API virtual void handleInput(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_3D_API virtual void handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event) override;

	VERSO_3D_API virtual void render(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_3D_API virtual bool isQuitting() const override;

	VERSO_3D_API virtual void quit() override;

public: // toString
	VERSO_3D_API virtual UString toString(const UString& newLinePadding) const override;

	VERSO_3D_API virtual UString toStringDebug(const UString& newLinePadding) const override;


	friend std::ostream& operator <<(std::ostream& ost, const Sprites3d& right)
	{
		return ost << right.toString("");
	}

};


} // End namespace Verso

#endif // End header guard

