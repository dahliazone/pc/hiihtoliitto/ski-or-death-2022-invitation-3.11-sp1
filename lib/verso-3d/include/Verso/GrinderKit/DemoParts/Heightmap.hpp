#ifndef VERSO_3D_GRINDERKIT_DEMOPARTS_HEIGHTMAP_HPP
#define VERSO_3D_GRINDERKIT_DEMOPARTS_HEIGHTMAP_HPP

#include <Verso/GrinderKit/DemoPart.hpp>
#include <Verso/GrinderKit/Params/ClearParam.hpp>
#include <Verso/GrinderKit/Params/DepthParam.hpp>
#include <Verso/Render/Material/MaterialLightmapsPhong3d_GridHeightmapDisplacement.hpp>
#include <Verso/Render/Vao.hpp>

namespace Verso {

//
// example JSON (simple)
//

class Heightmap : public DemoPart
{
private:
	// Params
	ClearParam clearParam;
	DepthParam depthParam;
	BlendMode blendMode;
	Vector2f unitSize;
	Vector2i gridSize;
	Vector3f offsetPosition;
	//UString sourceFileName;

	// State
	bool created;
	bool quitting;
	Vao vao;
	MaterialLightmapsPhong3d_GridHeightmapDisplacement material;

public:
	VERSO_3D_API Heightmap(
			const DemoPaths* demoPaths, const JSONObject& demoPartJson,
			const UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator);

	Heightmap(const Heightmap& original) = delete;

	VERSO_3D_API Heightmap(Heightmap&& original) noexcept;

	VERSO_3D_API virtual ~Heightmap() override;

	Heightmap& operator =(const Heightmap& original) = delete;

	VERSO_3D_API virtual Heightmap& operator =(Heightmap&& original) noexcept;

public: // IProgramOpengl interface
	VERSO_3D_API virtual void create(IWindowOpengl& window, Audio2d& audio2d) override;

	VERSO_3D_API virtual void reset(IWindowOpengl& window) override;

	VERSO_3D_API virtual void destroy() VERSO_NOEXCEPT override;

	VERSO_3D_API virtual bool isCreated() const override;

	VERSO_3D_API virtual void handleInput(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_3D_API virtual void handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event) override;

	VERSO_3D_API virtual void render(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_3D_API virtual bool isQuitting() const override;

	VERSO_3D_API virtual void quit() override;
};


} // End namespace Verso

#endif // End header guard

