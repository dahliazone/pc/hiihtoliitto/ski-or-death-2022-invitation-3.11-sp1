#ifndef VERSO_3D_GRINDERKIT_DEMOPARTS_TEXT_HPP
#define VERSO_3D_GRINDERKIT_DEMOPARTS_TEXT_HPP

#include <Verso/GrinderKit/DemoPart.hpp>
#include <Verso/GrinderKit/Params/ClearParam.hpp>
#include <Verso/GrinderKit/Params/DepthParam.hpp>
#include <Verso/GrinderKit/Params/FontFixedWidthParam.hpp>
#include <Verso/Render/Camera/CameraTarget.hpp>
#include <Verso/Render/Font/BitmapFontFixedWidth.hpp>

namespace Verso {


class Text : public DemoPart
{
private:
	// Params
	ClearParam clearParam;
	DepthParam depthParam;
	BlendMode blendMode;
	FontFixedWidthParam fontParam;
	UString text;
	Vector2fKeyframes relativePositionKeyframes;
	FloatKeyframes alphaKeyframes;
	Vector2f relativeCharacterSize;
	Vector2f relativeSpacing;
	RefScale refScale;
	FloatKeyframes angleZKeyframes;
	float revealCharactersPerSecond;

	// State
	bool created;
	bool quitting;
	IWindowOpengl* window;
	CameraTarget camera;
	BitmapFontFixedWidth* font;
	UString textToBeRendered;

public:
	VERSO_3D_API Text(const DemoPaths* demoPaths, const JSONObject& demoPartJson,
		 const UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator);

	Text(const Text& original) = delete;

	VERSO_3D_API Text(Text&& original) noexcept;

	VERSO_3D_API virtual ~Text() override;

	Text& operator =(const Text& original) = delete;

	VERSO_3D_API virtual Text& operator =(Text&& original) noexcept;

public: // IProgramOpengl interface
	VERSO_3D_API virtual void create(IWindowOpengl& window, Audio2d& audio2d) override;

	VERSO_3D_API virtual void reset(IWindowOpengl& window) override;

	VERSO_3D_API virtual void destroy() noexcept override;

	VERSO_3D_API virtual bool isCreated() const override;

	VERSO_3D_API virtual void handleInput(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_3D_API virtual void handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event) override;

	VERSO_3D_API virtual void render(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_3D_API virtual bool isQuitting() const override;

	VERSO_3D_API virtual void quit() override;
};


} // End namespace Verso

#endif // End header guard
