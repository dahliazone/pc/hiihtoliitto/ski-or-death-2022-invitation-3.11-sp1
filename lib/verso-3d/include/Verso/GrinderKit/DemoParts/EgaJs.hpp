#ifndef VERSO_3D_GRINDERKIT_DEMOPARTS_EGAJS_HPP
#define VERSO_3D_GRINDERKIT_DEMOPARTS_EGAJS_HPP

#include <Verso/GrinderKit/DemoPart.hpp>
#include <Verso/GrinderKit/Params/ClearParam.hpp>
#include <Verso/GrinderKit/Params/DepthParam.hpp>
#include <Verso/GrinderKit/Params/CameraParam.hpp>
#include <Verso/Render/Camera/CameraTarget.hpp>
#include <duktape.h>

namespace Verso {


class EgaJs : public DemoPart
{
private:
	// Params
	ClearParam clearParam;
	DepthParam depthParam;
	CameraParam cameraParam;
	BlendMode blendMode;
	Vector2fKeyframes positionKeyframes;
	Align alignFromPosition;
	Vector2f relSize;
	RefScale refScale;
	FloatKeyframes rotationAngleKeyframes;
	FloatKeyframes alphaKeyframes;
	UString sourceFileName;
	UString initialFramebufferImageFileName;
	Vector2i resolution;
	int transparentColor;

	// State
	bool created;
	bool quitting;
	IWindowOpengl* window;
	CameraTarget camera;
	UString sourceCode;
	duk_context* jsContext;
	Texture* texture;
	std::uint8_t palette[16 * 3];
	std::uint8_t* initialFrameBufferEga;
	std::uint8_t* frameBufferEga;
	bool firstTimeJsRun;

public:
	VERSO_3D_API EgaJs(
			const DemoPaths* demoPaths, const JSONObject& demoPartJson,
			const UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator);

	EgaJs(const EgaJs& original) = delete;

	VERSO_3D_API EgaJs(EgaJs&& original) noexcept;

	VERSO_3D_API virtual ~EgaJs() override;

	EgaJs& operator =(const EgaJs& original) = delete;

	VERSO_3D_API virtual EgaJs& operator =(EgaJs&& original) noexcept;

public: // IProgramOpengl interface
	VERSO_3D_API virtual void create(IWindowOpengl& window, Audio2d& audio2d) override;

	VERSO_3D_API virtual void reset(IWindowOpengl& window) override;

	VERSO_3D_API virtual void destroy() noexcept override;

	VERSO_3D_API virtual bool isCreated() const override;

	VERSO_3D_API virtual void handleInput(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_3D_API virtual void handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event) override;

	VERSO_3D_API virtual void render(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_3D_API virtual bool isQuitting() const override;

	VERSO_3D_API virtual void quit() override;

private:
	void resetPalette();
};


} // End namespace Verso

#endif // End header guard

