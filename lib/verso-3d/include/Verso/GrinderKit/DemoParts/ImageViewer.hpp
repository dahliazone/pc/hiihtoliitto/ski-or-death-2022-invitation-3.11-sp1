#ifndef VERSO_3D_GRINDERKIT_DEMOPARTS_IMAGEVIEWER_HPP
#define VERSO_3D_GRINDERKIT_DEMOPARTS_IMAGEVIEWER_HPP

#include <Verso/GrinderKit/DemoPart.hpp>
#include <Verso/Render/ShaderProgram.hpp>
#include <Verso/Render/Texture.hpp>
#include <Verso/Render/Vao.hpp>
#include <Verso/Math/Align.hpp>
#include <Verso/Math/FloatKeyframes.hpp>
#include <Verso/Math/Vector2Keyframes.hpp>
#include <Verso/Math/RgbaColorf.hpp>
#include <Verso/GrinderKit/Params/ClearParam.hpp>
#include <Verso/GrinderKit/Params/DepthParam.hpp>
#include <Verso/GrinderKit/Params/CameraParam.hpp>
#include <Verso/GrinderKit/Params/TextureParam.hpp>

#include <Verso/Render/Camera/CameraTarget.hpp>
#include <Verso/Math/RefScale.hpp>

namespace Verso {


class ImageViewer : public DemoPart
{
private:
	// Params
	ClearParam clearParam;
	DepthParam depthParam;
	CameraParam cameraParam;
	BlendMode blendMode;
	TextureParam textureParam;
	Vector2fKeyframes positionKeyframes;
	Align alignFromPosition;
	Vector2fKeyframes relSizeKeyframes;
	RefScale refScale;
	FloatKeyframes rotationAngleKeyframes;
	FloatKeyframes alphaKeyframes;

	// State
	bool created;
	bool quitting;
	Texture texture;
	CameraTarget camera;

public:
	VERSO_3D_API ImageViewer(
			const DemoPaths* demoPaths, const JSONObject& demoPartJson,
			const UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator);

	ImageViewer(const ImageViewer& original) = delete;

	VERSO_3D_API ImageViewer(ImageViewer&& original) noexcept;

	ImageViewer& operator =(const ImageViewer& original) = delete;

	VERSO_3D_API virtual ImageViewer& operator =(ImageViewer&& original) noexcept;

	VERSO_3D_API virtual ~ImageViewer() override;

public: // IProgramOpengl interface
	VERSO_3D_API virtual void create(IWindowOpengl& window, Audio2d& audio2d) override;

	VERSO_3D_API virtual void reset(IWindowOpengl& window) override;

	VERSO_3D_API virtual void destroy() VERSO_NOEXCEPT override;

	VERSO_3D_API virtual bool isCreated() const override;

	VERSO_3D_API virtual void handleInput(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_3D_API virtual void handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event) override;

	VERSO_3D_API virtual void render(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_3D_API virtual bool isQuitting() const override;

	VERSO_3D_API virtual void quit() override;

public: // toString
	VERSO_3D_API virtual UString toString(const UString& newLinePadding) const override;

	VERSO_3D_API virtual UString toStringDebug(const UString& newLinePadding) const override;


	friend std::ostream& operator <<(std::ostream& ost, const ImageViewer& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

