#ifndef VERSO_3D_GRINDERKIT_DEMOPARTS_PARTICLES_HPP
#define VERSO_3D_GRINDERKIT_DEMOPARTS_PARTICLES_HPP

#include <Verso/GrinderKit/DemoPart.hpp>
#include <Verso/Render/Particle/Sequencers/ParticleSequencer.hpp>
#include <Verso/GrinderKit/Params/ClearParam.hpp>
#include <Verso/GrinderKit/Params/DepthParam.hpp>
#include <Verso/GrinderKit/Params/ParticlesParam.hpp>
#include <Verso/GrinderKit/Params/BehaviourParam.hpp>
#include <Verso/GrinderKit/Params/EmitterParam.hpp>
#include <Verso/GrinderKit/Params/SequencerParam.hpp>

namespace Verso {

//
// example JSON (simple)
//

class Particles : public DemoPart
{
private:
	// Params
	ClearParam clearParam;
	DepthParam depthParam;
	BlendMode blendMode;
	ParticlesParam particlesParam;
	BehaviourParam behaviourParam;
	EmitterParam emitterParam;
	SequencerParam sequencerParam;

	// State
	bool created;
	bool quitting;
	ParticleBehaviour* particleBehaviour;
	ParticleSystem* particleSystem;
	ParticleEmitter* particleEmitter;
	ParticleSequencer* particleSequencer;

public:
	VERSO_3D_API Particles(
			const DemoPaths* demoPaths, const JSONObject& demoPartJson,
			const UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator);

	Particles(const Particles& original) = delete;

	VERSO_3D_API Particles(Particles&& original) noexcept;

	VERSO_3D_API virtual ~Particles() override;

	Particles& operator =(const Particles& original) = delete;

	VERSO_3D_API virtual Particles& operator =(Particles&& original) noexcept;

public: // IProgramOpengl interface
	VERSO_3D_API virtual void create(IWindowOpengl& window, Audio2d& audio2d) override;

	VERSO_3D_API virtual void reset(IWindowOpengl& window) override;

	VERSO_3D_API virtual void destroy() VERSO_NOEXCEPT override;

	VERSO_3D_API virtual bool isCreated() const override;

	VERSO_3D_API virtual void handleInput(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_3D_API virtual void handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event) override;

	VERSO_3D_API virtual void render(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_3D_API virtual bool isQuitting() const override;

	VERSO_3D_API virtual void quit() override;
};


} // End namespace Verso

#endif // End header guard

