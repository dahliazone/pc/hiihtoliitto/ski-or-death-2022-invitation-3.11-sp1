#ifndef VERSO_3D_GRINDERKIT_DEMOPARTS_CLEARSCREEN_HPP
#define VERSO_3D_GRINDERKIT_DEMOPARTS_CLEARSCREEN_HPP

#include <Verso/GrinderKit/DemoPart.hpp>
#include <Verso/Render/Opengl.hpp>
#include <Verso/GrinderKit/Params/ClearParam.hpp>

namespace Verso {


class ClearScreen : public DemoPart
{
private:
	// Params
	ClearParam clearParam;

	// State
	bool created;
	bool quitting;

public:
	VERSO_3D_API ClearScreen(
			const DemoPaths* demoPaths, const JSONObject& demoPartJson,
			const UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator);

	ClearScreen(const ClearScreen& original) = delete;

	VERSO_3D_API ClearScreen(ClearScreen&& original) noexcept;

	ClearScreen& operator =(const ClearScreen& original) = delete;

	VERSO_3D_API virtual ClearScreen& operator =(ClearScreen&& original) noexcept;

	VERSO_3D_API virtual ~ClearScreen() override;

public: // IProgramOpengl interface
	VERSO_3D_API virtual void create(IWindowOpengl& window, Audio2d& audio2d) override;

	VERSO_3D_API virtual void reset(IWindowOpengl& window) override;

	VERSO_3D_API virtual void destroy() VERSO_NOEXCEPT override;

	VERSO_3D_API virtual bool isCreated() const override;

	VERSO_3D_API virtual void handleInput(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_3D_API virtual void handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event) override;

	VERSO_3D_API virtual void render(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_3D_API virtual bool isQuitting() const override;

	VERSO_3D_API virtual void quit() override;
};


} // End namespace Verso

#endif // End header guard

