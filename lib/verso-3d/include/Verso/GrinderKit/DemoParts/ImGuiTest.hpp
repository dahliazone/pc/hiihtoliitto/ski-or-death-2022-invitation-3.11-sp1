#ifndef VERSO_3D_GRINDERKIT_DEMOPARTS_IMGUITEST_HPP
#define VERSO_3D_GRINDERKIT_DEMOPARTS_IMGUITEST_HPP

#include <Verso/GrinderKit/DemoPart.hpp>
#include <Verso/GrinderKit/Params/ClearParam.hpp>
#include <Verso/GrinderKit/Params/DepthParam.hpp>
#include <Verso/Math/RgbaColorf.hpp>

namespace Verso {

//
// example JSON
//
//	{ "name": "ImGuiTest", "type": "imguitest",
//		"start": 0, "duration": 3000, "priority": 0, "params": {
//			"clear": { "clearFlag": "SolidColor", "color": [0.45, 0.56, 0.60, 1] } }}
//

class ImGuiTest : public DemoPart
{
private:
	// Params
	ClearParam clearParam;
	DepthParam depthParam;

	// State
	bool created;
	bool quitting;
	bool showTestWindow;
	bool showAnotherWindow;
	RgbaColorf bgColor;

public:
	VERSO_3D_API ImGuiTest(
			const DemoPaths* demoPaths, const JSONObject& demoPartJson,
			const UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator);

	ImGuiTest(const ImGuiTest& original) = delete;

	VERSO_3D_API ImGuiTest(ImGuiTest&& original);

	VERSO_3D_API virtual ~ImGuiTest() override;

	ImGuiTest& operator =(const ImGuiTest& original) = delete;

	VERSO_3D_API virtual ImGuiTest& operator =(ImGuiTest&& original);

public: // IProgramOpengl interface
	VERSO_3D_API virtual void create(IWindowOpengl& window, Audio2d& audio2d) override;

	VERSO_3D_API virtual void reset(IWindowOpengl& window) override;

	VERSO_3D_API virtual void destroy() VERSO_NOEXCEPT override;

	VERSO_3D_API virtual bool isCreated() const override;

	VERSO_3D_API virtual void handleInput(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_3D_API virtual void handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event) override;

	VERSO_3D_API virtual void render(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_3D_API virtual bool isQuitting() const override;

	VERSO_3D_API virtual void quit() override;
};


} // End namespace Verso

#endif // End header guard

