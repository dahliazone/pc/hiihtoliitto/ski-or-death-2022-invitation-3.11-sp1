#ifndef VERSO_3D_GRINDERKIT_DEMOPARTS_SIMPLESCROLLER_HPP
#define VERSO_3D_GRINDERKIT_DEMOPARTS_SIMPLESCROLLER_HPP

#include <Verso/GrinderKit/DemoPart.hpp>
#include <Verso/GrinderKit/Params/ClearParam.hpp>
#include <Verso/GrinderKit/Params/DepthParam.hpp>
#include <Verso/GrinderKit/Params/FontFixedWidthParam.hpp>
#include <Verso/Render/Camera.hpp>
#include <Verso/Render/Font/BitmapFontFixedWidth.hpp>

namespace Verso {


class SimpleScroller : public DemoPart
{
private:
	// Params
	ClearParam clearParam;
	DepthParam depthParam;
	BlendMode blendMode;
	FontFixedWidthParam fontParam;
	UString text;
	FloatKeyframes relativePositionYKeyframes;
	float scrollSpeedX;
	float scrollPosX;
	FloatKeyframes alphaKeyframes;
	Vector2fKeyframes relativeCharacterSizeKeyframes;
	Vector2fKeyframes relativeSpacingKeyframes;
	RefScale refScale;
	FloatKeyframes angleZKeyframes;

	// State
	bool created;
	bool quitting;
	IWindowOpengl* window;
	CameraTarget camera;
	float currentScrollPosX;
	BitmapFontFixedWidth* font;

public:
	VERSO_3D_API SimpleScroller(
			const DemoPaths* demoPaths, const JSONObject& demoPartJson,
			const UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator);

	SimpleScroller(const SimpleScroller& original) = delete;

	VERSO_3D_API SimpleScroller(SimpleScroller&& original) noexcept;

	VERSO_3D_API virtual ~SimpleScroller() override;

	SimpleScroller& operator =(const SimpleScroller& original) = delete;

	VERSO_3D_API virtual SimpleScroller& operator =(SimpleScroller&& original) noexcept;

public: // IProgramOpengl interface
	VERSO_3D_API virtual void create(IWindowOpengl& window, Audio2d& audio2d) override;

	VERSO_3D_API virtual void reset(IWindowOpengl& window) override;

	VERSO_3D_API virtual void destroy() noexcept override;

	VERSO_3D_API virtual bool isCreated() const override;

	VERSO_3D_API virtual void handleInput(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_3D_API virtual void handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event) override;

	VERSO_3D_API virtual void render(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_3D_API virtual bool isQuitting() const override;

	VERSO_3D_API virtual void quit() override;
};


} // End namespace Verso

#endif // End header guard

