#ifndef VERSO_3D_GRINDERKIT_DEMOPARTS_RANDOMBACKGROUND_HPP
#define VERSO_3D_GRINDERKIT_DEMOPARTS_RANDOMBACKGROUND_HPP

#include <Verso/GrinderKit/DemoPart.hpp>
#include <Verso/GrinderKit/Params/ClearParam.hpp>
#include <Verso/GrinderKit/Params/DepthParam.hpp>
#include <Verso/Render/Texture.hpp>
#include <Verso/Render/Camera/CameraTarget.hpp>
#include <Verso/Math/Random.hpp>

namespace Verso {

//
// minimal example JSON (grey background and paths.textures directory will be used)
// { "name": "Random background", "type": "verso.randombackground",
//		"start": 0, "duration": 60, "priority": 0, "params": {}},
//
// full example JSON (relative path from paths.textures will be used)
// { "name": "Random background", "type": "verso.randombackground",
//		"start": 0, "duration": 60, "priority": 0, "params": {
//			"sourcePath": "backgrounds/",
//			"clear": { "clearFlag": "SolidColor", "color": [ 1, 0, 1, 1 ] }
//		}},
//
class RandomBackground : public DemoPart
{
private:
	// Params
	ClearParam clearParam;
	DepthParam depthParam;
	BlendMode blendMode;
	UString sourcePath;

	// State
	bool created;
	bool quitting;
	Texture texBg;
	CameraTarget camera;

public:
	VERSO_3D_API RandomBackground(
			const DemoPaths* demoPaths, const JSONObject& demoPartJson,
			const UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator);

	RandomBackground(const RandomBackground& original) = delete;

	VERSO_3D_API RandomBackground(RandomBackground&& original) noexcept;

	RandomBackground& operator =(const RandomBackground& original) = delete;

	VERSO_3D_API virtual RandomBackground& operator =(RandomBackground&& original) noexcept;

	VERSO_3D_API virtual ~RandomBackground() override;

public: // IProgramOpengl interface
	VERSO_3D_API virtual void create(IWindowOpengl& window, Audio2d& audio2d) override;

	VERSO_3D_API virtual void reset(IWindowOpengl& window) override;

	VERSO_3D_API virtual void destroy() VERSO_NOEXCEPT override;

	VERSO_3D_API virtual bool isCreated() const override;

	VERSO_3D_API virtual void handleInput(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_3D_API virtual void handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event) override;

	VERSO_3D_API virtual void render(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_3D_API virtual bool isQuitting() const override;

	VERSO_3D_API virtual void quit() override;
};


} // End namespace Verso

#endif // End header guard

