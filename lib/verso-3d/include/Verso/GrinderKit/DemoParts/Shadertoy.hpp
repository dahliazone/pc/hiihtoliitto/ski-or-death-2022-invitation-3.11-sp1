#ifndef VERSO_3D_GRINDERKIT_DEMOPARTS_SHADERTOY_HPP
#define VERSO_3D_GRINDERKIT_DEMOPARTS_SHADERTOY_HPP

#include <Verso/Math/FloatKeyframes.hpp>
#include <Verso/GrinderKit/DemoPart.hpp>
#include <Verso/GrinderKit/Params/ClearParam.hpp>
#include <Verso/GrinderKit/Params/DepthParam.hpp>
#include <Verso/GrinderKit/Params/ShaderParam.hpp>
#include <Verso/GrinderKit/Params/ChannelParam.hpp>
#include <Verso/Render/Opengl/BlendMode.hpp>
#include <Verso/Render/ShaderProgram.hpp>
#include <Verso/Render/Vao/Vao.hpp>
#include <Verso/Render/Camera/CameraTarget.hpp>

namespace Verso {


class Shadertoy : public DemoPart
{
private:
	// Params
	ClearParam clearParam;
	DepthParam depthParam;
	BlendMode blendMode;
	ShaderParam shaderParam;
	ChannelParam channelParam;
	FloatKeyframes alphaKeyframes;

	// State
	bool created;
	bool quitting;
	ShaderProgram shader;
	Vao vao;
	std::vector<Texture*> channels;
	CameraTarget camera;

public:
	VERSO_3D_API Shadertoy(
			const DemoPaths* demoPaths, const JSONObject& demoPartJson,
			const UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator);

	Shadertoy(const Shadertoy& original) = delete;

	VERSO_3D_API Shadertoy(Shadertoy&& original) noexcept;

	VERSO_3D_API virtual ~Shadertoy() override;

	Shadertoy& operator =(const Shadertoy& original) = delete;

	VERSO_3D_API virtual Shadertoy& operator =(Shadertoy&& original) noexcept;

public: // IProgramOpengl interface
	VERSO_3D_API virtual void create(IWindowOpengl& window, Audio2d& audio2d) override;

	VERSO_3D_API virtual void reset(IWindowOpengl& window) override;

	VERSO_3D_API virtual void destroy() VERSO_NOEXCEPT override;

	VERSO_3D_API virtual bool isCreated() const override;

	VERSO_3D_API virtual void handleInput(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_3D_API virtual void handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event) override;

	VERSO_3D_API virtual void render(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_3D_API virtual bool isQuitting() const override;

	VERSO_3D_API virtual void quit() override;
};


} // End namespace Verso

#endif // End header guard

