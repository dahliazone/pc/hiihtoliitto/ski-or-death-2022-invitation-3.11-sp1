#ifndef VERSO_3D_GRINDERKIT_DEMOSETUPDIALOG_HPP
#define VERSO_3D_GRINDERKIT_DEMOSETUPDIALOG_HPP

//#include <Verso/Display/MultisampleAntialiasLevel.hpp>
#include <Verso/Display/DisplayMode.hpp>
#include <Verso/Display/Display.hpp>
#include <Verso/Display/RenderDownscaleRatio.hpp>
#include <Verso/Math/AspectRatio.hpp>
#include <Verso/Audio/AudioDevice.hpp>
#include <Verso/Math/RgbaColorf.hpp>
#include <Verso/Render/Texture.hpp>
#include <Verso/GrinderKit/IDemoPartFactory.hpp>
#include <Verso/GrinderKit/DemoPlayerSettings.hpp>

struct ImFont;

namespace Verso {


class GrinderKitSetupDialogSettings;


class GrinderKitSetupDialog
{
public:
	bool showDialog;
	UString sourceFileName;
	DemoPlayerSettings demoPlayerSettings;

	Display* display;
	DisplayMode* displayMode;
	RenderDownscaleRatio* downscaleRatio;
//	MultisampleAntialiasLevel* antialiasLevel;
	AudioDevice* audioDevice;

	bool fullscreen;
	bool customResolution; // note: dangerous/buggy in Linux & OS X!
//	bool loop;
	bool vsync;
	bool unrestrictedFps;
	bool muteAudio;
	bool record;
	Vector2i recordResolution;
	ImageSaveFormat recordSaveFormat;

private:
	ImFont* smallerFont;
	ImFont* biggerFont;
	const IDemoPartFactory* demoPartFactory;
	UString defaultSourceFileName;
	UString binaryName;
	Texture* logoTexture;
	float logoDrawWidth;
	float logoDrawHeight;
	int windowWidth;
	int windowHeight;
	float logoPaddingHorizontal;

	std::vector<Display> displays;
	std::vector<DisplayMode> displayModesFullscreen;
	std::vector<DisplayMode> displayModesWindowed;
	std::vector<RefreshRate> refreshRates;
	std::vector<RenderDownscaleRatio> downscaleRatios;
	//std::vector<MultisampleAntialiasLevel> antialiasLevels;
	std::vector<AudioDevice> audioDevices;
	bool extraDisplayModes;
	bool esotericDisplayModes;
	RefreshRate* refreshRateSelector;

public:
	VERSO_3D_API GrinderKitSetupDialog(const IDemoPartFactory* demoPartFactory, const UString& defaultSourceFileName, const UString& binaryName);

	VERSO_3D_API ~GrinderKitSetupDialog();

	VERSO_3D_API bool run(int argc, char* argv[]);

private:
	void init();

	bool findDisplayMode(const DisplayMode& selected, bool dieOnError = true);

	void findRefreshRate(const RefreshRate& selected);

	bool processArgs(const std::vector<UString>& args);

	void initDialog();

	bool runDialog();

	void runSetupGui(bool& quitted, bool& continueStart, const GrinderKitSetupDialogSettings& setupDialogSettings);
};


} // End namespace Verso

#endif // End header guard

