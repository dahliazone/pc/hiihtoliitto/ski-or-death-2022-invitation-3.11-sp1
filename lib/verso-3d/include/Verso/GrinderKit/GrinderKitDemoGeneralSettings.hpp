#ifndef VERSO_3D_GRINDERKIT_GRINDERKITDEMOGENERALSETTINGS_HPP
#define VERSO_3D_GRINDERKIT_GRINDERKITDEMOGENERALSETTINGS_HPP

#include <Verso/verso-3d-common.hpp>
#include <Verso/System/JsonHelper.hpp>

namespace Verso {


class GrinderKitDemoGeneralSettings
{
public:
	UString name;
	UString version;
	UString authors;

	UString musicFileName;
	AspectRatio contentAspectRatio;
	PixelAspectRatio pixelAspectRatio;
	RgbaColorf borderColor;
	double targetFps;
	double duration;
	bool loop;

public:
	VERSO_3D_API GrinderKitDemoGeneralSettings();

	VERSO_3D_API GrinderKitDemoGeneralSettings(const GrinderKitDemoGeneralSettings& original);

	VERSO_3D_API GrinderKitDemoGeneralSettings(GrinderKitDemoGeneralSettings&& original);

	VERSO_3D_API GrinderKitDemoGeneralSettings& operator =(const GrinderKitDemoGeneralSettings& original);

	VERSO_3D_API GrinderKitDemoGeneralSettings& operator =(GrinderKitDemoGeneralSettings&& original);

	VERSO_3D_API ~GrinderKitDemoGeneralSettings();

public: // json
	VERSO_3D_API void importJson(
			const JSONObject& demoGeneralSettingsObj, const UString& currentPathJson);

	VERSO_3D_API JSONValue* exportJson();

public: // toString
	VERSO_3D_API UString toString(const UString& newLinePadding) const;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const GrinderKitDemoGeneralSettings& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard
