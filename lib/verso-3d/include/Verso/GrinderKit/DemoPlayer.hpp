#ifndef VERSO_3D_GRINDERKIT_DEMOPLAYER_HPP
#define VERSO_3D_GRINDERKIT_DEMOPLAYER_HPP

#include <Verso/GrinderKit/DemoPlayerSettings.hpp>
#include <Verso/GrinderKit/PlayState.hpp>
#include <Verso/Audio/Audio2d.hpp>
#include <list>

namespace Verso {


class DemoPlayer
{
public:
	DemoPlayerSettings settings;

private:
	PlayState playState;
	PlayState previousRealPlayState;
	std::list<DemoPart*> pastDemoParts;
	std::list<DemoPart*> currentDemoParts;
	std::list<DemoPart*> futureDemoParts;
	double targetDeltaTime;
	double elapsedSeconds;
	Audio2d* audio2d;
	bool created;
	bool demoOver;

public:
	VERSO_3D_API DemoPlayer();

	DemoPlayer(const DemoPlayer& original) = delete;

	VERSO_3D_API DemoPlayer(DemoPlayer&& original);

	DemoPlayer& operator =(const DemoPlayer& original) = delete;

	VERSO_3D_API DemoPlayer& operator =(DemoPlayer&& original);

	VERSO_3D_API ~DemoPlayer();

public: // Player model
	VERSO_3D_API void startDemo(IWindowOpengl& window);

	VERSO_3D_API void stopDemo();

	VERSO_3D_API void createFromFile(const IDemoPartFactory* factory, const UString& fileName);

	VERSO_3D_API void createDemoParts(IWindowOpengl& window, Audio2d& audio2d);

	VERSO_3D_API void destroy() VERSO_NOEXCEPT;

	VERSO_3D_API void destroyDemoParts();


	VERSO_3D_API bool isCreated() const
	{
		return created;
	}


	VERSO_3D_API bool isDemoOver() const
	{
		return demoOver;
	}


	VERSO_3D_API void handleInput(IWindowOpengl& window, const FrameTimestamp& time);

	VERSO_3D_API void handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event);

	VERSO_3D_API void render(IWindowOpengl& window, const FrameTimestamp& time);

	VERSO_3D_API void gotoAbsolute(IWindowOpengl& window, double absoluteSeconds);

	VERSO_3D_API void gotoRelative(IWindowOpengl& window, double relativeSeconds);

private:
	VERSO_3D_API void rewindAudio(double absoluteSeconds);

	VERSO_3D_API bool updatePlayerModel(IWindowOpengl& window, double deltaTime);

	VERSO_3D_API void divideDeltaTime(double deltaTime, std::vector<double>& times);

	VERSO_3D_API bool updateFutureDemoPartsToCurrentDemoParts(IWindowOpengl& window, double secondsElapsedDemo);

	VERSO_3D_API bool updateCurrentDemoPartsToPastDemoParts(const IWindowOpengl& window, double secondsElapsedDemo);

	VERSO_3D_API void sortFutureDemoParts();

	VERSO_3D_API void sortActiveDemoParts();

	VERSO_3D_API void runActiveDemoParts(IWindowOpengl& window, double deltaTime, double secondsElapsedDemo);

	VERSO_3D_API UString getActiveDemoParts() const;

	VERSO_3D_API void printOutDemoParts() const;

public:

	VERSO_3D_API const DemoPlayerSettings& getDemoPlayerSettings() const
	{
		return settings;
	}


	VERSO_3D_API PlayState getPlayState() const
	{
		return playState;
	}


	VERSO_3D_API void setPlayState(const PlayState& playState)
	{
		// If previous play state was "real" then save it
		if (this->playState == PlayState::Stopped ||
				this->playState == PlayState::Paused ||
				this->playState == PlayState::Playing) {
			this->previousRealPlayState = this->playState;
		}

		if (playState == PlayState::Playing) {
			rewindAudio(elapsedSeconds);
			audio2d->startMusic();
		}
		else if (playState == PlayState::Paused ||
				 playState == PlayState::Stopped) {
			audio2d->pauseMusic();
		}

		this->playState = playState;
	}


	VERSO_3D_API PlayState getPreviousRealPlayState() const
	{
		return previousRealPlayState;
	}


	VERSO_3D_API double getElapsedSeconds() const
	{
		return elapsedSeconds;
	}


	VERSO_3D_API void addDemoPart(DemoPart* demoPart)
	{
		// Add to futureDemoParts
		futureDemoParts.push_back(demoPart);

		// Sort the futureDemoParts list
		futureDemoParts.sort();
	}


public: // toString
	VERSO_3D_API UString toString() const
	{
		UString str;

		str += "\n\n==== Player model ====";
		str += "playState = \"";
		str += playStateToString(playState);
		str += "\"";

		return str;
	}


	VERSO_3D_API UString toStringDebug() const
	{
		UString str("Verso::DemoPlayer(\n");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const DemoPlayer& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso

#endif // End header guard

