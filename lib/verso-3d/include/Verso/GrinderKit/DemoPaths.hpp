#ifndef VERSO_3D_GRINDERKIT_DEMOPATHS_HPP
#define VERSO_3D_GRINDERKIT_DEMOPATHS_HPP

#include <Verso/verso-3d-common.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/System/JsonHelper.hpp>

namespace Verso {


class DemoPaths
{
public:
	UString jsons;
	UString models;
	UString shaders;
	UString shadersVerso;
	UString music;
	UString textures;
	UString fonts;
	UString particles;
	UString gui;
	UString guiIcons;
	UString guiStyles;
	UString javascript;

public:
	VERSO_3D_API DemoPaths();

	DemoPaths(const DemoPaths& original) = delete;

	VERSO_3D_API DemoPaths(DemoPaths&& original);

	DemoPaths& operator =(const DemoPaths& original) = delete;

	VERSO_3D_API DemoPaths& operator =(DemoPaths&& original);

	VERSO_3D_API ~DemoPaths();

public: // json
	VERSO_3D_API void importJson(
			const JSONObject& paths, const UString& currentPathJson, const UString& jsonFileNamePath);

	VERSO_3D_API JSONValue* exportJson();

public: // getters & setters
	VERSO_3D_API UString pathJsons() const;

	VERSO_3D_API UString pathModels() const;

	VERSO_3D_API UString pathShaders() const;

	VERSO_3D_API UString pathShadersVerso() const;

	VERSO_3D_API UString pathMusic() const;

	VERSO_3D_API UString pathTextures() const;

	VERSO_3D_API UString pathFonts() const;

	VERSO_3D_API UString pathParticles() const;

	VERSO_3D_API UString pathGui() const;

	VERSO_3D_API UString pathGuiIcons() const;

	VERSO_3D_API UString pathGuiStyles() const;

	VERSO_3D_API UString pathJavascript() const;

private:
	UString addSlashToEndIfNeeded(const UString& path);

public: // toString
	VERSO_3D_API UString toString(const UString& newLinePadding) const;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const DemoPaths& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

