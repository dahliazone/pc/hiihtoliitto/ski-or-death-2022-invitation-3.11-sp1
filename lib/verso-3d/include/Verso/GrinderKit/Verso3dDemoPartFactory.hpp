#ifndef VERSO_3D_GRINDERKIT_VERSO3DDEMOPARTFACTORY_HPP
#define VERSO_3D_GRINDERKIT_VERSO3DDEMOPARTFACTORY_HPP

#include <Verso/GrinderKit/IDemoPartFactory.hpp>

namespace Verso {


class VERSO_3D_API Verso3dDemoPartFactory : public IDemoPartFactory
{
public:
	Verso3dDemoPartFactory();
	virtual ~Verso3dDemoPartFactory() override;

public:
	virtual DemoPart* instance(
			const DemoPaths* demoPaths, const JSONObject& demoPartJson,
			const UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator)  const override;
};


} // End namespace Verso

#endif // End header guard

