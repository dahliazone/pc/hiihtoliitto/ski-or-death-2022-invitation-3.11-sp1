#ifndef VERSO_3D_GRINDERKIT_DEMOPARTSETTINGS_HPP
#define VERSO_3D_GRINDERKIT_DEMOPARTSETTINGS_HPP

#include <Verso/verso-3d-common.hpp>
#include <Verso/System/JsonHelper.hpp>

namespace Verso {


class DemoPartSettings
{
public:
	UString name;
	UString type;
	double start;
	double duration;
	std::int32_t priority;
	std::int32_t subsequentPriority;
	std::int32_t duplicateLevel;
	std::int32_t jsonOrder;
	JSONValue* paramsJsonVal;
	JSONObject paramsJson;
	UString currentPathJson;

public:
	VERSO_3D_API DemoPartSettings(
			const JSONObject& demoPartJson, const UString& currentPathJson,
			std::int32_t jsonOrder, double startAccumulator);

	VERSO_3D_API DemoPartSettings(
			const UString& name = "<unset>", const UString& type = "<unset>",
			double start = 0.0, double duration = 1000.0,
			std::int32_t priority = 0, std::int32_t jsonOrder = 0,
			const JSONObject& paramsJson = JSONObject(),
			const std::string& currentPathJson = "");

	DemoPartSettings(const DemoPartSettings& original) = delete;

	VERSO_3D_API DemoPartSettings(DemoPartSettings&& original);

	DemoPartSettings& operator =(const DemoPartSettings& original) = delete;

	VERSO_3D_API DemoPartSettings& operator =(DemoPartSettings&& original);

	VERSO_3D_API ~DemoPartSettings();

public:
	VERSO_3D_API void importJson(const JSONObject& json, const UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator);

	VERSO_3D_API JSONValue* exportJson();

public: // toString
	VERSO_3D_API UString toString(const UString& newLinePadding) const;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const DemoPartSettings& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

