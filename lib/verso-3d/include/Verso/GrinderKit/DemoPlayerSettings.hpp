#ifndef VERSO_3D_GRINDERKIT_DEMOPLAYERSETTINGS_HPP
#define VERSO_3D_GRINDERKIT_DEMOPLAYERSETTINGS_HPP

#include <Verso/GrinderKit/DemoPaths.hpp>
#include <Verso/GrinderKit/GrinderKitSetupDialogSettings.hpp>
#include <Verso/GrinderKit/GrinderKitDemoGeneralSettings.hpp>
#include <Verso/GrinderKit/DemoPlayerDebugSettings.hpp>
#include <Verso/GrinderKit/Verso3dDemoPartFactory.hpp>

namespace Verso {


class DemoPlayerSettings
{
public: // static
	VERSO_3D_API static UString supportedJsonFormat;

public: // Demo model
	UString sourceFileName;

	// data
	UString format;
	GrinderKitDemoGeneralSettings general;
	DemoPaths demoPaths;
	GrinderKitSetupDialogSettings setupDialog;
	DemoPlayerDebugSettings debug;
	std::vector<DemoPart*> demoParts;

private:
	const IDemoPartFactory* demoPartFactory;
	JSONValue* rootValue;
	bool created;

public:
	VERSO_3D_API DemoPlayerSettings();

	DemoPlayerSettings(const DemoPlayerSettings& original) = delete;

	VERSO_3D_API DemoPlayerSettings(DemoPlayerSettings&& original);

	DemoPlayerSettings& operator =(const DemoPlayerSettings& original) = delete;

	VERSO_3D_API DemoPlayerSettings& operator =(DemoPlayerSettings&& original);

	VERSO_3D_API ~DemoPlayerSettings();

public: // Demo model
	VERSO_3D_API void createFromFile(const IDemoPartFactory* factory, const UString& fileName);

	VERSO_3D_API void destroy();

	VERSO_3D_API bool isCreated() const;

	VERSO_3D_API void createDemoParts(IWindowOpengl& window, Audio2d& audio2d);

	VERSO_3D_API void destroyDemoParts() VERSO_NOEXCEPT;

	VERSO_3D_API void loadFromJson(const UString& fileName);

	VERSO_3D_API void saveToJson(const UString& fileName);

private:
	VERSO_3D_API std::int32_t processDemoPartsRecursive(const JSONObject& object, const UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator);

public:
	VERSO_3D_API void importJson(const JSONObject& json, const UString& jsonFileNamePath);

	VERSO_3D_API JSONValue* exportJson();

	VERSO_3D_API std::int32_t findDemoPart(DemoPart* demoPart);

	VERSO_3D_API bool moveDemoPart(DemoPart* demoPart, double newStart);

	VERSO_3D_API bool resizeDemoPart(DemoPart* demoPart, double newDuration);

	// Must be called after changing any DemoPartSettings priority
	VERSO_3D_API void updateSubsequentPriorities();

public: // toString
	VERSO_3D_API UString toString(const UString& newLinePadding) const;

	VERSO_3D_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const DemoPlayerSettings& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

