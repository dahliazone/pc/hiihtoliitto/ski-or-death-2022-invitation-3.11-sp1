#ifndef VERSO_3D_GRINDERKIT_GRINDERKITUI_HPP
#define VERSO_3D_GRINDERKIT_GRINDERKITUI_HPP

#include <Verso/System/IProgramOpengl.hpp>
#include <Verso/Audio/Audio2d.hpp>
#include <Verso/Render/TextureAtlas.hpp>
#include <Verso/GrinderKit/IGrinderKitApp.hpp>
#include <Verso/Input/Midi/Midi.hpp>

struct ImFont;

namespace Verso {


class GrinderKitUi : public IProgramOpengl
{
public:
	bool created;
	bool quitting;
	InputController* inputController;
	IGrinderKitApp& grinderKitApp;
	DemoPlayer& demoPlayer;
	ImFont* smallerFont;
	ImFont* biggerFont;
	Audio2d* audio2d;
	TextureAtlas icons;
	UString selectedIconSet;
	RgbaColorf editorBgColor;
	Midi midi;
	AxisHandle a1;
	AxisHandle a2;
	AxisHandle a3;
	AxisHandle a4;
	ButtonHandle buttonPlayPause;
	ButtonHandle buttonRewindToStart;
	ButtonHandle buttonToggleFullscreen;
	ButtonHandle buttonToggleEditor;
	ButtonHandle b1;
	ButtonHandle b2;
	ButtonHandle b3;
	ButtonHandle b4;
	ButtonHandle b5;
	ButtonHandle b6;
	ButtonHandle b7;
	ButtonHandle b8;
	ButtonHandle b9;
	ButtonHandle b10;
	ButtonHandle b11;
	ButtonHandle b12;
	ButtonHandle b13;

	// state
	bool enableUi;
	bool viewFullscreen2;
	bool viewPanelsToggle;
	bool viewMainControls;
	bool viewLeftPanel;
	bool viewRightPanel;
	bool viewHorizontalPanel;
	bool viewImGuiDemoWindow;
	bool viewTestWindow;
	int mainMenuHeight;
	DemoPart* selectedDemoPart;
	Texture* renderTexture;

public:
	VERSO_3D_API GrinderKitUi(IGrinderKitApp& grinderKitApp, DemoPlayer& demoPlayer);

	GrinderKitUi(const GrinderKitUi& original) = delete;

	VERSO_3D_API GrinderKitUi(GrinderKitUi&& original) noexcept;

	GrinderKitUi& operator =(const GrinderKitUi& original) = delete;

	VERSO_3D_API GrinderKitUi& operator =(GrinderKitUi&& original) noexcept;

	VERSO_3D_API ~GrinderKitUi() override;

public:
	VERSO_3D_API bool getEnableUi() const;

	VERSO_3D_API void updateRenderTexture(Texture* renderTexture);

public: // IProgramOpengl interface
	VERSO_3D_API void create(IWindowOpengl& window, Audio2d& audio2d) override;

	VERSO_3D_API virtual void reset(IWindowOpengl& window) override;

	VERSO_3D_API void destroy() VERSO_NOEXCEPT override;

	VERSO_3D_API bool isCreated() const override;

	VERSO_3D_API void handleInput(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_3D_API void handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event) override;

	VERSO_3D_API void render(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_3D_API bool isQuitting() const override;

	VERSO_3D_API void quit() override;

private:
	bool imguiImageButton(
			const UString& subAtlasName, const UString& iconName, const UString& id, int framePadding = -1,
			const RgbaColorf& bgColor = RgbaColorf(0, 0, 0, 0), const RgbaColorf& tintColor = RgbaColorf(1, 1, 1, 1));

private:
	void menuBar(const IWindowOpengl& window);

	void menuBarFile(const IWindowOpengl& window);

	void menuBarEdit(const IWindowOpengl& window);

	void menuBarView(const IWindowOpengl& window);

	void panelMainView(const IWindowOpengl& window, const ImVec2& windowPos, const ImVec2& windowSize);

	void panelMainControls(IWindowOpengl& window);

	void panelLeft(const IWindowOpengl& window);

	void panelRight(const IWindowOpengl& window);

	void panelHorizontal(IWindowOpengl& window);

	void tabDemoPart(const IWindowOpengl& window);

	void tabMaterial(const IWindowOpengl& window);

	void tabPostProc(const IWindowOpengl& window);

	void tabDemo(const IWindowOpengl& window);

	void tabTimeline(IWindowOpengl& window);

	void showExampleAppConstrainedResize(bool* p_open);

	void showHelpMarker(const char* desc);

	void showExampleAppPropertyEditor(bool* p_open);

	void actionPlayPause(IWindowOpengl& window);

	void actionRewindToStart(IWindowOpengl& window);

	void actionToggleFullscreen(IWindowOpengl& window);

	void actionToggleEditor(IWindowOpengl& window);
};


} // End namespace Verso

#endif // End header guard

