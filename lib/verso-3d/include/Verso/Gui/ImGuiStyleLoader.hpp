#ifndef VERSO_3D_GUI_IMGUISTYLELOADER_HPP
#define VERSO_3D_GUI_IMGUISTYLELOADER_HPP

#include <Verso/verso-3d-common.hpp>
#include <Verso/System/UString.hpp>

namespace Verso {
namespace ImGuiStyleLoader {


VERSO_3D_API void loadAndApplyStyle(const UString& fileName);


} // End namespace ImGuiStyleLoader
} // End namespace Verso

#endif // End header guard

