#ifndef VERSO_3D_GUI_IMGUITIMELINE_HPP
#define VERSO_3D_GUI_IMGUITIMELINE_HPP

#include <Verso/verso-3d-common.hpp>

namespace Verso {


class DemoPlayerSettings;
class DemoPart;


VERSO_3D_API bool demoTimeLine(DemoPlayerSettings& settings, float* timeNowSeconds, float* viewScrollPosSeconds, float* viewWidthSeconds,
							   DemoPart** selected);


} // End namespace ImGui

#endif // End header guard

