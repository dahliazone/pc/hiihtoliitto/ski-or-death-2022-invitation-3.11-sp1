// dear imgui: Platform Binding for SDL2
// This needs to be used along with a Renderer (e.g. DirectX11, OpenGL3, Vulkan..)
// (Info: SDL2 is a cross-platform general purpose library for handling windows, inputs, graphics context creation, etc.)

// Implemented features:
//  [X] Platform: Mouse cursor shape and visibility. Disable with 'io.ConfigFlags |= ImGuiConfigFlags_NoMouseCursorChange'.
//  [X] Platform: Clipboard support.
//  [X] Platform: Keyboard arrays indexed using SDL_SCANCODE_* codes, e.g. ImGui::IsKeyPressed(SDL_SCANCODE_SPACE).
//  [X] Platform: Gamepad support. Enabled with 'io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad'.
//  [X] Platform: Multi-viewport support (multiple windows). Enable with 'io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable'.
// Missing features:
//  [ ] Platform: SDL2 handling of IME under Windows appears to be broken and it explicitly disable the regular Windows IME. You can restore Windows IME by compiling SDL with SDL_DISABLE_WINDOWS_IME.
//  [ ] Platform: Multi-viewport + Minimized windows seems to break mouse wheel events (at least under Windows).

// You can copy and use unmodified imgui_impl_* files in your project. See main.cpp for an example of using this.
// If you are new to dear imgui, read examples/README.txt and read the documentation at the top of imgui.cpp.
// https://github.com/ocornut/imgui

#pragma once

#include <Verso/verso-3d-common.hpp>

namespace Verso {


class IWindowOpengl;
struct Event;
class FrameTimestamp;


VERSO_3D_API bool ImGui_ImplSDL2_InitForOpenGL(IWindowOpengl& window/*, void* sdl_gl_context */); // \TODO: MULTI VIEWPORT
VERSO_3D_API bool ImGui_ImplSDL2_InitForVulkan(IWindowOpengl& window);
VERSO_3D_API bool ImGui_ImplSDL2_InitForD3D(IWindowOpengl& window);
VERSO_3D_API void ImGui_ImplSDL2_Shutdown();
VERSO_3D_API void ImGui_ImplSDL2_NewFrame(IWindowOpengl& window, const FrameTimestamp& time, bool useRenderResolution);
VERSO_3D_API bool ImGui_ImplSDL2_ProcessEvent(const Event& event);


} // End namespace Verso

