#ifndef VERSO_3D_GUI_IMGUIVERSO3DIMPL_HPP
#define VERSO_3D_GUI_IMGUIVERSO3DIMPL_HPP

#include <Verso/Display/IWindowOpengl.hpp>
#include <Verso/Time/FrameTimestamp.hpp>

#define IMGUI_DISABLE_OBSOLETE_FUNCTIONS
#include <imgui.h>

namespace Verso {


class VERSO_3D_API ImGuiVerso3dImpl
{
private:
	static bool created;
	static ImGuiContext* imGuiContext;

public:
	static bool create(IWindowOpengl& window);

	static void destroy() VERSO_NOEXCEPT;

	static bool isCreated();

	static bool handleEvent(const IWindowOpengl& window, const FrameTimestamp& time, const Event& event);

	static void newFrame(IWindowOpengl& window, const FrameTimestamp& time, bool useRenderResolution);

	static void render(const IWindowOpengl& window, const Recti& viewportRect);

	static ImGuiContext* getImGuiContext();

	static void enablKbHighlight();
};


} // End namespace Verso

#endif // End header guard

