#ifndef VERSO_3D_DISPLAY_IWINDOWOPENGL_HPP
#define VERSO_3D_DISPLAY_IWINDOWOPENGL_HPP

#include <Verso/Display/IWindow.hpp>
#include <Verso/Display/ContextSettings.hpp>
#include <Verso/Display/SwapInterval.hpp>
#include <Verso/Render/Fbo.hpp>
#include <Verso/Render/ResourceManager.hpp>

namespace Verso {


class IWindowOpengl : public IWindow
{
public:
	virtual ~IWindowOpengl() override = default;

	virtual ContextSettings getSettings() const = 0;

	virtual void setSettings(const ContextSettings& settings) = 0;

	virtual SwapInterval getSwapInterval() const = 0;

	virtual void setSwapInterval(const SwapInterval& swapInterval) = 0;

	virtual Fbo* getCurrentlyBoundReadFbo() = 0;

	virtual Fbo* getCurrentlyBoundDrawFbo() = 0;

	virtual void bindReadFbo(Fbo* fbo) = 0;

	virtual void unbindReadFbo(Fbo* fbo) = 0;

	virtual void bindDrawFbo(Fbo* fbo) = 0;

	virtual void unbindDrawFbo(Fbo* fbo) = 0;

	virtual ResourceManager& getResourceManager() = 0;

	virtual bool isActiveTextureUnit(unsigned int index) const = 0;

	virtual unsigned int getActiveTextureUnit() const = 0;

	virtual void setActiveTextureUnit(unsigned int index) = 0;

	virtual void bindTextureHandle(unsigned int textureHandle) = 0;

	virtual void bindResetTextureUnits(size_t count) = 0;

	virtual Vector2i getMaxTextureResolution() const = 0;

	virtual int getMaxCombinedTextureImageUnits() const = 0;

	virtual int getMaxVertexTextureImageUnits() const = 0;

	virtual int getMaxGeometryTextureImageUnits() const = 0;
};


} // End namespace Verso

#endif // End header guard

