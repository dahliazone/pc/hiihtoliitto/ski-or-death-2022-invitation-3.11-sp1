#ifndef VERSO_3D_DISPLAY_CONTEXT_HPP
#define VERSO_3D_DISPLAY_CONTEXT_HPP

#include <Verso/Display/ContextSettings.hpp>

namespace Verso {


class Context
{
private:
	ContextSettings settings;
	void* sdlWindow;
	void* sdlGlContext;
	bool windowOwned;

public:
	VERSO_3D_API Context();

	VERSO_3D_API Context(
			bool sharedWithCurrent, ContextSettings& settings, void* optionalSdlWindow = nullptr);

	Context(const Context& optionalSdlWindow) = delete;

	Context(Context&& original) = delete;

	VERSO_3D_API ~Context();

public:
	VERSO_3D_API void create(
			bool sharedWithCurrent, const ContextSettings& settings, void* optionalSdlWindow = nullptr);

	VERSO_3D_API void destroy() VERSO_NOEXCEPT;

	VERSO_3D_API bool isCreated() const;

	VERSO_3D_API void makeActive();

public:
	Context& operator =(const Context& original) = delete;

	Context& operator =(Context&& original) = delete;

public: // static
	VERSO_3D_API static bool isContextCreated();

	VERSO_3D_API static bool isCurrentContext(Context* context);

private: // static
	static void** getPreviousCreatedSdlGlContext();

	static Context** getCurrentContext();

public: // toString
	VERSO_3D_API UString toString() const;

	VERSO_3D_API UString toStringDebug() const;


	friend std::ostream& operator <<(std::ostream& ost, const Context& right)
	{
		return ost << right.toString();
	}
};



} // End namespace Verso

#endif // End header guard

