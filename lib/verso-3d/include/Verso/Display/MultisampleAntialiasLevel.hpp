#ifndef VERSO_3D_DISPLAY_MULTISAMPLEANTIALIASLEVEL_HPP
#define VERSO_3D_DISPLAY_MULTISAMPLEANTIALIASLEVEL_HPP

#include <Verso/verso-3d-common.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>
#include <Verso/System/Logger.hpp>
#include <ostream>
#include <cstdint>

namespace Verso {


class MultisampleAntialiasLevel
{
public:
	uint8_t ratio;

public:
	// ratio can be 0, 2, 4, 8 or 16
	VERSO_3D_API explicit MultisampleAntialiasLevel(uint8_t ratio = 0);

	// ratio can be "0x", "2x", "4x", "8x" or "16x"
	VERSO_3D_API explicit MultisampleAntialiasLevel(const UString& ratioStr);

	VERSO_3D_API void set(uint8_t ratio);

	VERSO_3D_API void set(const UString& ratioStr);

public: // static
	VERSO_3D_API static const MultisampleAntialiasLevel& MsaaOff();

	VERSO_3D_API static const MultisampleAntialiasLevel& Msaa2x();

	VERSO_3D_API static const MultisampleAntialiasLevel& Msaa4x();

	VERSO_3D_API static const MultisampleAntialiasLevel& Msaa8x();

	VERSO_3D_API static const MultisampleAntialiasLevel& Msaa16x();

	VERSO_3D_API static const std::vector<MultisampleAntialiasLevel>& getAntialiasLevels();

	VERSO_3D_API static MultisampleAntialiasLevel* findIdenticalFromList(
			const MultisampleAntialiasLevel& selected, std::vector<MultisampleAntialiasLevel>& antialiasLevels);

public: // toString
	VERSO_3D_API UString toString() const;

	VERSO_3D_API UString toStringDebug() const;


	friend std::ostream& operator <<(std::ostream& ost, const MultisampleAntialiasLevel& right)
	{
		return ost << right.toString();
	}
};


inline bool operator ==(const MultisampleAntialiasLevel& left, const MultisampleAntialiasLevel& right)
{
	return (left.ratio == right.ratio);
}


inline bool operator !=(const MultisampleAntialiasLevel& left, const MultisampleAntialiasLevel& right)
{
	return !(left == right);
}


inline bool operator <(const MultisampleAntialiasLevel& left, const MultisampleAntialiasLevel& right)
{
	return left.ratio < right.ratio;
}


inline bool operator >(const MultisampleAntialiasLevel& left, const MultisampleAntialiasLevel& right)
{
	return right < left;
}


inline bool operator <=(const MultisampleAntialiasLevel& left, const MultisampleAntialiasLevel& right)
{
	return !(right < left);
}


inline bool operator >=(const MultisampleAntialiasLevel& left, const MultisampleAntialiasLevel& right)
{
	return !(left < right);
}


} // End namespace Verso

#endif // End header guard

