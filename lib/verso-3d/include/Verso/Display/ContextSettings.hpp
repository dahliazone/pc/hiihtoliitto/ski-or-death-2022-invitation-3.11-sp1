#ifndef VERSO_3D_DISPLAY_CONTEXTSETTINGS_HPP
#define VERSO_3D_DISPLAY_CONTEXTSETTINGS_HPP

#include <Verso/Display/MultisampleAntialiasLevel.hpp>
#include <Verso/System/UString.hpp>

namespace Verso {


class ContextSettings
{
public:
	int majorVersion;
	int minorVersion;
	MultisampleAntialiasLevel antialiasLevel;
	int depthBits;
	int stencilBits;
	bool shareWithCurrentContext;
	UString glslVersionString;

public:
	VERSO_3D_API ContextSettings(
			int majorVersion = 3, int minorVersion = 3,
			MultisampleAntialiasLevel antialiasLevel = MultisampleAntialiasLevel::MsaaOff(),
			int depthBits = 24, int stencilBits = 8, bool shareWithCurrentContext = false);

	VERSO_3D_API void applySettings();

	VERSO_3D_API void retrieveSettings();

public: // static
	VERSO_3D_API static UString openglVersionToGlslVersion(int majorVersion, int minorVersion);

	VERSO_3D_API static UString openglVersionToGlslString(int majorVersion, int minorVersion);

public: // toString
	VERSO_3D_API UString toString() const;

	VERSO_3D_API UString toStringDebug() const;


	friend std::ostream& operator <<(std::ostream& ost, const ContextSettings& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso

#endif // End header guard

