#ifndef VERSO_3D_DISPLAY_WINDOWOPENGL_HPP
#define VERSO_3D_DISPLAY_WINDOWOPENGL_HPP

#include <Verso/Display/Context.hpp>
#include <Verso/Display/ContextSettings.hpp>
#include <Verso/Render/Fbo.hpp>
#include <Verso/Display/RenderDownscaleRatio.hpp>
#include <Verso/Display/SwapInterval.hpp>
#include <Verso/Display/WindowStyle.hpp>
#include <Verso/Display/IWindowOpengl.hpp>
#include <Verso/Display/Native.hpp>
#include <Verso/Input/InputManager.hpp>
#include <array>

namespace Verso {


class WindowOpengl : public IWindowOpengl
{
private: // static
	static std::vector<WindowOpengl*> windowsStillOpen;

private:
	bool created;
	UString id;
	DisplayMode displayMode;
	Display display;
	UString title;
	ContextSettings settings;
	Context context;
	void* sdlWindow;
	std::array<void*, static_cast<size_t>(SystemCursorType::EnumSize)> sdlSystemCursors;
	RenderDownscaleRatio renderDownscaleRatio;
	Vector2i renderResolution;
	AspectRatio renderDisplayAspectRatio;
	bool forcedRenderDisplayAspectRatio;
	PixelAspectRatio renderPixelAspectRatio;
	Vector2i overrideRecordRenderResolution;
	//UString iconFileName;
	Rectf relativeViewport;
	ResourceManager resourceManager;
	int maxTextureResolution;
	int maxCombinedTextureImageUnits;
	int maxVertexTextureImageUnits;
	int maxGeometryTextureImageUnits;
	std::vector<unsigned int> lastBoundTextureHandlesInTextureUnits;
	unsigned int activeTextureUnit;
	Fbo* currentlyBoundReadFbo;
	Fbo* currentlyBoundDrawFbo;
	UString cameraInputControllerId;
	InputController* cameraInputController;
	ButtonHandle cameraForward;
	ButtonHandle cameraBackward;
	ButtonHandle cameraLeft;
	ButtonHandle cameraRight;
	ButtonHandle cameraUp;
	ButtonHandle cameraDown;
	ButtonHandle cameraForwardFast;
	ButtonHandle cameraBackwardFast;
	ButtonHandle cameraLeftFast;
	ButtonHandle cameraRightFast;
	ButtonHandle cameraUpFast;
	ButtonHandle cameraDownFast;
	ButtonHandle cameraForwardReallyFast;
	ButtonHandle cameraBackwardReallyFast;
	ButtonHandle cameraLeftReallyFast;
	ButtonHandle cameraRightReallyFast;
	ButtonHandle cameraUpReallyFast;
	ButtonHandle cameraDownReallyFast;
	ButtonHandle cameraActionA;
	ButtonHandle cameraActionB;
	UString latestClipboardText;

public:
	VERSO_3D_API explicit WindowOpengl(const UString& id);

	VERSO_3D_API WindowOpengl(
			const UString& id, const DisplayMode& displayMode, const Display& display, const UString& title,
			WindowStyle style = WindowStyle::Default,
			const RenderDownscaleRatio& renderDownscaleRatio = RenderDownscaleRatio::Disabled(),
			const AspectRatio& renderDisplayAspectRatio = AspectRatio(),
			const PixelAspectRatio& renderPixelAspectRatio = PixelAspectRatio(),
			const ContextSettings& settings = ContextSettings(),
			const SwapInterval& swapInterval = SwapInterval::AdaptiveVsync,
			const Vector2i& overrideRecordRenderResolution = Vector2i());

	VERSO_3D_API WindowOpengl(
			const UString& id, const DisplayMode& displayMode, const Display& display, const UString& title,
			WindowStyles styles,
			const RenderDownscaleRatio& renderDownscaleRatio = RenderDownscaleRatio::Disabled(),
			const AspectRatio& renderDisplayAspectRatio = AspectRatio(),
			const PixelAspectRatio& renderPixelAspectRatio = PixelAspectRatio(),
			const ContextSettings& settings = ContextSettings(),
			const SwapInterval& swapInterval = SwapInterval::AdaptiveVsync,
			const Vector2i& overrideRecordRenderResolution = Vector2i());

	VERSO_3D_API void create(
			const DisplayMode& displayMode, const Display& display, const UString& title,
			WindowStyle style = WindowStyle::Default,
			const RenderDownscaleRatio& renderDownscaleRatio = RenderDownscaleRatio::Disabled(),
			const AspectRatio& renderDisplayAspectRatio = AspectRatio(),
			const PixelAspectRatio& renderPixelAspectRatio = PixelAspectRatio(),
			const ContextSettings& settings = ContextSettings(),
			const SwapInterval& swapInterval = SwapInterval::AdaptiveVsync,
			const Vector2i& overrideRecordRenderResolution = Vector2i());

	VERSO_3D_API void create(
			const DisplayMode& displayMode, const Display& display, const UString& title,
			WindowStyles styles,
			const RenderDownscaleRatio& renderDownscaleRatio = RenderDownscaleRatio::Disabled(),
			const AspectRatio& renderDisplayAspectRatio = AspectRatio(),
			const PixelAspectRatio& renderPixelAspectRatio = PixelAspectRatio(),
			const ContextSettings& requestSettings = ContextSettings(),
			const SwapInterval& swapInterval = SwapInterval::AdaptiveVsync,
			const Vector2i& overrideRecordRenderResolution = Vector2i());

	WindowOpengl(const WindowOpengl& original) = delete;

	WindowOpengl(WindowOpengl&& original) = delete;

	VERSO_3D_API virtual ~WindowOpengl() override;

public: // interface IWindow
	VERSO_3D_API virtual void destroy() VERSO_NOEXCEPT override;

	VERSO_3D_API virtual bool isCreated() const override;

	VERSO_3D_API virtual void close() VERSO_NOEXCEPT override;

	VERSO_3D_API virtual bool isOpen() const override;

	VERSO_3D_API virtual UString getId() const override;

	VERSO_3D_API virtual Vector2i getWindowResolutioni() const override;

	VERSO_3D_API virtual Vector2f getWindowResolutionf() const override;

	VERSO_3D_API virtual void setWindowResolution(const Vector2i& windowResolution) override;

	VERSO_3D_API virtual RenderDownscaleRatio getRenderDownscaleRatio() const override;

	VERSO_3D_API virtual Vector2i getRenderResolutioni() const override;

	VERSO_3D_API virtual Vector2f getRenderResolutionf() const override;

	VERSO_3D_API virtual Vector2i getDrawableResolutioni() const override;

	VERSO_3D_API virtual Vector2f getDrawableResolutionf() const override;

	VERSO_3D_API virtual AspectRatio getRenderDisplayAspectRatio() const override;

	VERSO_3D_API virtual void setRenderDisplayAspectRatio(const AspectRatio& renderDisplayAspectRatio) override;

	VERSO_3D_API virtual PixelAspectRatio getRenderPixelAspectRatio() const override;

	VERSO_3D_API virtual void setRenderPixelAspectRatio(const PixelAspectRatio& renderPixelAspectRatio) override;

	VERSO_3D_API virtual void makeActive() override;

	VERSO_3D_API virtual void swapScreenBuffer() const override;

	VERSO_3D_API virtual const UString& getClipboardText() override;

	VERSO_3D_API virtual void setClipboardText(const UString& text) override;

	VERSO_3D_API virtual bool isCursorShown() const override;

	VERSO_3D_API virtual void setCursorShown(bool shown) const override;

	VERSO_3D_API virtual void showCursor() const override;

	VERSO_3D_API virtual void hideCursor() const override;

	VERSO_3D_API virtual bool hasInputGrabbed() const override;

	VERSO_3D_API virtual bool hasInputFocus() const override;

	VERSO_3D_API virtual bool hasMouseFocus() const override;

	VERSO_3D_API virtual MouseState getMouseState() const override;

	VERSO_3D_API virtual bool captureMouse(bool enabled) const override;

	VERSO_3D_API virtual bool warpMouseGlobal(int x, int y) const override;

	VERSO_3D_API virtual void warpMouseInWindow(int x, int y) const override;

	VERSO_3D_API virtual bool createSystemCursor(SystemCursorType systemCursorType) override;

	VERSO_3D_API virtual void destroySystemCursor(SystemCursorType systemCursorType) override;

	VERSO_3D_API virtual bool setSystemCursor(SystemCursorType systemCursorType) const override;

	VERSO_3D_API virtual void* getNativeWindowHandle() const override;

	VERSO_3D_API virtual void* getRenderer() const override;

	VERSO_3D_API virtual void show() override;

	VERSO_3D_API virtual void hide() override;

	VERSO_3D_API virtual DisplayMode getDisplayMode() const override;

	VERSO_3D_API virtual void setDisplayMode(const DisplayMode& displayMode) override;

	VERSO_3D_API virtual Display getDisplay() const override;

	VERSO_3D_API virtual void setDisplay(const Display& display) override;

	VERSO_3D_API virtual UString getTitle() const override;

	VERSO_3D_API virtual void setTitle(const UString& windowTitle) override;

	VERSO_3D_API virtual Vector2i getPositioni() const override;

	VERSO_3D_API virtual Vector2f getPositionf() const override;

	VERSO_3D_API virtual void setPosition(const Vector2i& location) override;

	// Note: centers on display chosen at create()
	VERSO_3D_API virtual void setPositionCentered() override;

	VERSO_3D_API virtual void setPositionAlignedOnDisplay(const Display& anotherDisplay, const Align& align) override;

	VERSO_3D_API virtual Rectf getRelativeViewport() const override;

	VERSO_3D_API virtual void setRelativeViewport(const Rectf& relativeViewport) override;

	VERSO_3D_API virtual void resetRelativeViewport() override;

	VERSO_3D_API virtual Recti getRenderViewporti() const override;

	VERSO_3D_API virtual Rectf getRenderViewportf() const override;

	VERSO_3D_API virtual void applyRenderViewport() const override;

	VERSO_3D_API virtual Recti getDrawableViewporti() const override;

	VERSO_3D_API virtual Rectf getDrawableViewportf() const override;

	VERSO_3D_API virtual void applyDrawableViewport() const override;

	VERSO_3D_API virtual void loadIcon(const UString& iconFileName) override;

	VERSO_3D_API virtual bool pollEvent(Event& event) override;

	VERSO_3D_API virtual bool waitEvent(Event& event) override;

	VERSO_3D_API virtual InputController* getCameraInputController() const override;

	VERSO_3D_API virtual ButtonHandle getCameraForward() const override;

	VERSO_3D_API virtual ButtonHandle getCameraBackward() const override;

	VERSO_3D_API virtual ButtonHandle getCameraLeft() const override;

	VERSO_3D_API virtual ButtonHandle getCameraRight() const override;

	VERSO_3D_API virtual ButtonHandle getCameraUp() const override;

	VERSO_3D_API virtual ButtonHandle getCameraDown() const override;

	VERSO_3D_API virtual ButtonHandle getCameraForwardFast() const override;

	VERSO_3D_API virtual ButtonHandle getCameraBackwardFast() const override;

	VERSO_3D_API virtual ButtonHandle getCameraLeftFast() const override;

	VERSO_3D_API virtual ButtonHandle getCameraRightFast() const override;

	VERSO_3D_API virtual ButtonHandle getCameraUpFast() const override;

	VERSO_3D_API virtual ButtonHandle getCameraDownFast() const override;

	VERSO_3D_API virtual ButtonHandle getCameraForwardReallyFast() const override;

	VERSO_3D_API virtual ButtonHandle getCameraBackwardReallyFast() const override;

	VERSO_3D_API virtual ButtonHandle getCameraLeftReallyFast() const override;

	VERSO_3D_API virtual ButtonHandle getCameraRightReallyFast() const override;

	VERSO_3D_API virtual ButtonHandle getCameraUpReallyFast() const override;

	VERSO_3D_API virtual ButtonHandle getCameraDownReallyFast() const override;

	VERSO_3D_API virtual ButtonHandle getCameraActionA() const override;

	VERSO_3D_API virtual ButtonHandle getCameraActionB() const override;

	VERSO_3D_API virtual void onResize() override;

	VERSO_3D_API virtual UString toString() const override;

	VERSO_3D_API virtual UString toStringDebug() const override;

public: // interface IWindowOpengl
	VERSO_3D_API virtual ContextSettings getSettings() const override;

	VERSO_3D_API virtual void setSettings(const ContextSettings& settings) override;

	VERSO_3D_API virtual SwapInterval getSwapInterval() const override;

	VERSO_3D_API virtual void setSwapInterval(const SwapInterval& swapInterval) override;

	VERSO_3D_API virtual Fbo* getCurrentlyBoundReadFbo() override;

	VERSO_3D_API virtual Fbo* getCurrentlyBoundDrawFbo() override;

	VERSO_3D_API virtual void bindReadFbo(Fbo* fbo) override;

	VERSO_3D_API virtual void unbindReadFbo(Fbo* fbo) override;

	VERSO_3D_API virtual void bindDrawFbo(Fbo* fbo) override;

	VERSO_3D_API virtual void unbindDrawFbo(Fbo* fbo) override;

	VERSO_3D_API virtual ResourceManager& getResourceManager() override;

	VERSO_3D_API virtual bool isActiveTextureUnit(unsigned int index) const override;

	VERSO_3D_API virtual unsigned int getActiveTextureUnit() const override;

	VERSO_3D_API virtual void setActiveTextureUnit(unsigned int index) override;

	VERSO_3D_API virtual void bindTextureHandle(unsigned int textureHandle) override;

	VERSO_3D_API virtual void bindResetTextureUnits(size_t count) override;

	VERSO_3D_API virtual Vector2i getMaxTextureResolution() const override;

	VERSO_3D_API virtual int getMaxCombinedTextureImageUnits() const override;

	VERSO_3D_API virtual int getMaxVertexTextureImageUnits() const override;

	VERSO_3D_API virtual int getMaxGeometryTextureImageUnits() const override;

public:
	WindowOpengl& operator =(const WindowOpengl& original) = delete;

	WindowOpengl& operator =(WindowOpengl&& original) = delete;

public: // static
	VERSO_3D_API static void closeAll();

protected: // static
	VERSO_3D_API static int& getUniqueInstanceId();

	VERSO_3D_API static std::vector<WindowOpengl*>& getWindowsStillOpen();

protected:
	VERSO_3D_API virtual void onCreate();

private:
	void updateRenderResolution();
	bool filterEvent(const Event& event);
};


} // End namespace Verso

#endif // End header guard

