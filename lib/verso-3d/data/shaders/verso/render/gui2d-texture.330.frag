#version 330 core
precision highp float;

in vec2 ex_Uv;
out vec4 fragColor;

uniform sampler2D iChannel0;
uniform float alpha;


void main()
{
	vec4 color = texture(iChannel0, ex_Uv.xy);
	color.a = color.a * alpha;
	fragColor = color;
}

