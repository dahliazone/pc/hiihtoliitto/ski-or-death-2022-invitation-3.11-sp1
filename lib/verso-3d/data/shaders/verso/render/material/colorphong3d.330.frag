#version 330 core
precision mediump float;

in vec2 ex_Uv;
in vec3 ex_FragPos;
in vec3 ex_Normal;
out vec4 fragColor;

struct Material {
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float shininess;
};

struct DirectionalLight {
	vec3 direction;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct PointLight {
	vec3 position;

	float constant;
	float linear;
	float quadratic;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct SpotLight {
	vec3 position;
	vec3 direction;

	float cutOff;
	float outerCutOff;

	float constant;
	float linear;
	float quadratic;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

#define MAX_DIRECTIONAL_LIGHTS 2
#define MAX_POINT_LIGHTS 10
#define MAX_SPOT_LIGHTS 10

uniform vec3 viewPos;
uniform Material material;

uniform DirectionalLight directionalLights[MAX_DIRECTIONAL_LIGHTS];
uniform int directionalLightsCount;

uniform PointLight pointLights[MAX_POINT_LIGHTS];
uniform int pointLightsCount;

uniform SpotLight spotLights[MAX_SPOT_LIGHTS];
uniform int spotLightsCount;


// Function prototypes
vec3 calculateDirectionalLight(DirectionalLight light, vec3 normal, vec3 viewDir);
vec3 calculatePointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir);
vec3 calculateSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir);


void main()
{
	vec3 norm = normalize(ex_Normal);
	vec3 viewDir = normalize(viewPos - ex_FragPos);

	// Directional lighting
	vec3 result = vec3(0.0, 0.0, 0.0);
	for (int i=0; i<directionalLightsCount; i++)
		result += calculateDirectionalLight(directionalLights[i], norm, viewDir);

	// Pointlights
	for (int i=0; i<pointLightsCount; i++)
		result += calculatePointLight(pointLights[i], norm, ex_FragPos, viewDir);

	// Spotlight
	for (int i=0; i<spotLightsCount; i++)
		result += calculateSpotLight(spotLights[i], norm, ex_FragPos, viewDir);

	fragColor = vec4(result, 1.0);
}


vec3 calculateDirectionalLight(DirectionalLight light, vec3 normal, vec3 viewDir)
{
	vec3 lightDir = normalize(-light.direction);

	// Ambient
	vec3 ambient = light.ambient * material.ambient;

	// Diffuse
	float diffRatio = max(dot(normal, lightDir), 0.0);
	vec3 diffuse = light.diffuse * diffRatio * material.diffuse;

	// Specular
	vec3 reflectDir = reflect(-lightDir, normal);
	float specRatio = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
	vec3 specular = light.specular * specRatio * material.specular;

	return (ambient + diffuse + specular);
}


vec3 calculatePointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
	vec3 lightDir = normalize(light.position - fragPos);

	// Ambient
	vec3 ambient = light.ambient * material.ambient;

	// Diffuse
	float diffRatio = max(dot(normal, lightDir), 0.0);
	vec3 diffuse = light.diffuse * diffRatio * material.diffuse;

	// Specular
	vec3 reflectDir = reflect(-lightDir, normal);
	float specRatio = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
	vec3 specular = light.specular * specRatio * material.specular;

	// Attenuation
	float distance = length(light.position - fragPos);
	float attenuation = 1.0f / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
	ambient *= attenuation;
	diffuse *= attenuation;
	specular *= attenuation;

	return (ambient + diffuse + specular);
}


vec3 calculateSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
	vec3 lightDir = normalize(light.position - fragPos);

	// Ambient
	vec3 ambient = light.ambient * material.ambient;

	// Diffuse
	float diffRatio = max(dot(normal, lightDir), 0.0);
	vec3 diffuse = light.diffuse * diffRatio * material.diffuse;

	// Specular
	vec3 reflectDir = reflect(-lightDir, normal);
	float specRatio = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
	vec3 specular = light.specular * material.specular;

	// Spotlight intensity
	float theta = dot(lightDir, normalize(-light.direction));
	float epsilon = light.cutOff - light.outerCutOff;
	float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);

	// Attenuation
	float distance = length(light.position - fragPos);
	float attenuation = 1.0f / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
	ambient *= attenuation * intensity;
	diffuse *= attenuation * intensity;
	specular *= attenuation * intensity;

	return (ambient + diffuse + specular);
}

