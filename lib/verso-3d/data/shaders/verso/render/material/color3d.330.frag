#version 330 core
precision mediump float;

in vec2 ex_Uv;
in vec3 ex_Normal;
out vec4 fragColor;

struct Material {
	vec3 color;
};

uniform Material material;


void main()
{
	fragColor = vec4(material.color, 1.0);
}

