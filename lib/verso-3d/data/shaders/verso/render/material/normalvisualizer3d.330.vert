#version 330 core
precision highp float;

in vec3 position;
in vec2 uv;
in vec3 normal;

out Vertex
{
  vec4 normal;
  vec4 color;
} vertex;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform vec3 normalColor;


void main()
{
  gl_Position = vec4(position, 1.0);
  vertex.normal = vec4(mat3(transpose(inverse(model))) * normal, 1.0);
  vertex.color = vec4(normalColor, 1.0);
}

