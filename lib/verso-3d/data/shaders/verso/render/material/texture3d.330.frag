#version 330 core
precision mediump float;

in vec2 ex_Uv;
in vec3 ex_Normal;
out vec4 fragColor;

struct Material {
	sampler2D diffuseTexture;
};

uniform Material material;

void main()
{
	fragColor = texture(material.diffuseTexture, ex_Uv);
}

