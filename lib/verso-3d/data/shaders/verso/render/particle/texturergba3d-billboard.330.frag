#version 330 core
precision mediump float;

in vec2 ex_Uv;
in vec3 ex_Normal;
out vec4 fragColor;

struct Material {
	sampler2D diffuseTexture;
	vec4 diffuseColor;
};

uniform Material material;

void main()
{
	fragColor = vec4(material.diffuseColor.xyz, material.diffuseColor.w) * texture(material.diffuseTexture, ex_Uv);
}

