#version 330 core
precision highp float;

in vec3 position;
//in vec2 uv;
//in vec3 normal;

//out vec2 ex_Uv;
//out vec3 ex_Normal;
//out vec3 ex_FragPos;

uniform mat4 model;
uniform mat4 view;
//uniform mat4 projection;


void main()
{
	mat4 viewModel = view * model;

//	viewModel[0][0] = 1.0f;
//	viewModel[0][1] = 0.0f;
//	viewModel[0][2] = 0.0f;
//	viewModel[1][0] = 0.0f;
//	viewModel[1][1] = 1.0f;
//	viewModel[1][2] = 0.0f;
//	viewModel[2][0] = 0.0f;
//	viewModel[2][1] = 0.0f;
//	viewModel[2][2] = 1.0f;

//	gl_Position = projection * viewModel * vec4(position, 1.0);
//	ex_FragPos = vec3(model * vec4(position, 1.0f));
//	ex_Uv = uv;
//	ex_Normal = mat3(transpose(inverse(model))) * normal;

	gl_Position = viewModel * vec4(position, 1.0);
}
