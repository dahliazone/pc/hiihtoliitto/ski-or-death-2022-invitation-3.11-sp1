#include <Verso/Render/RenderStats.hpp>

namespace Verso {


int RenderStats::points = 0;
int RenderStats::lines = 0;
int RenderStats::triangles = 0;
int RenderStats::meshes = 0;


void RenderStats::addPoints(int amount)
{
	points += amount;
}


void RenderStats::addLines(int amount)
{
	lines += amount;
}


void RenderStats::addTriangles(int amount)
{
	triangles += amount;
}


void RenderStats::addPrimitives(const PrimitiveType& primitive, int vertices)
{
	switch (primitive) {
	case PrimitiveType::Points:
		addPoints(vertices);
		break;
	case PrimitiveType::Lines:
		addLines(vertices / 2);
		break;
	case PrimitiveType::LineStrip:
		if (vertices >= 2) {
			addLines(vertices - 1);
		}
		break;
	case PrimitiveType::LineLoop:
		if (vertices >= 2) {
			addLines(vertices);
		}
		break;
	case PrimitiveType::Triangles:
		addTriangles(vertices / 3);
		break;
	case PrimitiveType::TriangleStrip:
		if (vertices >= 3) {
			addTriangles(vertices - 2);
		}
		break;
	case PrimitiveType::TriangleFan:
		if (vertices >= 3) {
			addTriangles(vertices - 2);
		}
		break;
	case PrimitiveType::Unset:
	{
		UString error("Cannot add PrimitiveType::Unset primitives!");
		VERSO_ERROR("verso-3d", error.c_str(), "");
	}
	default:
	{
		UString error("Unknown PrimitiveType(");
		error.append2(static_cast<int>(primitive));
		error += ")";
		VERSO_ERROR("verso-3d", error.c_str(), "");
	}
	}
}


void RenderStats::addMeshes(int amount)
{
	meshes += amount;
}


int RenderStats::getPointsCount()
{
	return points;
}


int RenderStats::getLinesCount()
{
	return lines;
}


int RenderStats::getTrianglesCount()
{
	return triangles;
}



int RenderStats::getMeshesCount()
{
	return meshes;
}


void RenderStats::reset()
{
	points = 0;
	lines = 0;
	triangles = 0;
	meshes = 0;
}


UString RenderStats::toString()
{
	UString str("meshes=");
	str.append2(RenderStats::getMeshesCount());
	str += ", triangles=";
	str.append2(RenderStats::getTrianglesCount());
	str += ", lines=";
	str.append2(RenderStats::getLinesCount());
	str += ", points=";
	str.append2(RenderStats::getPointsCount());
	return str;
}


UString RenderStats::toStringDebug()
{
	UString str("RenderStats(");
	str += toString();
	str += ")";
	return str;
}


} // End namespace Verso


