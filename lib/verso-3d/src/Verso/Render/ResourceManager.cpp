#include <Verso/Render/ResourceManager.hpp>
#include <Verso/System/Logger.hpp>
#include <Verso/Display/IWindowOpengl.hpp>

namespace Verso {


ResourceManager::ResourceManager() :
	created(false),
	window(nullptr),

	defaultTexture(),
	textures(),
	texturesHashNames(),
	texturesRefCount(),

	cameras(),
	camerasRefCount(),

	fbos(),
	fbosRefCount()
{
}


ResourceManager::ResourceManager(ResourceManager&& original) :
	created(std::move(original.created)),
	window(std::move(original.window)),

	defaultTexture(std::move(original.defaultTexture)),
	textures(std::move(original.textures)),
	texturesHashNames(std::move(original.texturesHashNames)),
	texturesRefCount(std::move(original.texturesRefCount)),

	cameras(std::move(original.cameras)),
	camerasRefCount(std::move(original.camerasRefCount)),

	fbos(std::move(original.fbos)),
	fbosRefCount(std::move(original.fbosRefCount))
{
	original.created = false;
	original.window = nullptr;
	textures.clear();
	texturesHashNames.clear();
	texturesRefCount.clear();
	cameras.clear();
	camerasRefCount.clear();
	fbos.clear();
	fbosRefCount.clear();
}


ResourceManager& ResourceManager::operator =(ResourceManager&& original)
{
	if (this != &original) {
		created = std::move(original.created);
		window = std::move(original.window);

		defaultTexture = std::move(original.defaultTexture);
		textures = std::move(original.textures);
		texturesHashNames = std::move(original.texturesHashNames);
		texturesRefCount = std::move(original.texturesRefCount);

		cameras = std::move(original.cameras);
		camerasRefCount = std::move(original.camerasRefCount);

		fbos = std::move(original.fbos);
		fbosRefCount = std::move(original.fbosRefCount);

		original.created = false;
		original.window = nullptr;
		textures.clear();
		texturesHashNames.clear();
		texturesRefCount.clear();
		cameras.clear();
		camerasRefCount.clear();
		fbos.clear();
		fbosRefCount.clear();
	}
	return *this;
}


ResourceManager::~ResourceManager()
{
	destroy();
}


void ResourceManager::create(IWindowOpengl* window)
{
	VERSO_ASSERT_MSG("verso-3d", isCreated() == false, "Already created!");
	VERSO_ASSERT("verso-3d", window != nullptr);
	this->window = window;

	created = true;
}


void ResourceManager::setupDefaultTexture(const UString& defaultTextureFileName)
{
	VERSO_ASSERT_MSG("verso-3d", isCreated() == true, "Not yet created!");

	TextureParameters textureParameters(
				"texture", TexturePixelFormat::Rgba,
				MinFilter::Linear, MagFilter::Linear,
				WrapStyle::Repeat, WrapStyle::Repeat);

	if (defaultTexture != nullptr) {
		delete defaultTexture;
		defaultTexture = nullptr;
	}

	defaultTexture = new Texture();
	defaultTexture->createFromFile(*window, defaultTextureFileName, textureParameters);
}


void ResourceManager::destroy()
{
	if (isCreated() == false) {
		return;
	}

	// Textures
	delete defaultTexture;
	defaultTexture = nullptr;

	for (auto& it : textures) {
		if (it.second != nullptr) {
			delete it.second;
		}
	}
	textures.clear();
	texturesHashNames.clear();
	texturesRefCount.clear();

	// Cameras
	// Do not free memory as it's not owned by ResourceManager
	cameras.clear();
	camerasRefCount.clear();

	// Fbos
	for (auto& it : fbos) {
		if (it.second != nullptr) {
			delete it.second;
		}
	}
	fbos.clear();
	fbosRefCount.clear();

	window = nullptr;

	created = false;
}


bool ResourceManager::isCreated() const
{
	return created;
}


Texture* ResourceManager::getTexture(const UString& hashName)
{
	return getTexture(Hash32::fnv1(hashName));
}


Texture* ResourceManager::getTexture(const Hash32& hash)
{
	VERSO_ASSERT("verso-3d", isCreated() == true);
	const auto& it = textures.find(hash);
	if (it != textures.end()) {
		return it->second;
	}
	else {
		return defaultTexture;
	}
}


Texture* ResourceManager::loadTexture(const UString& fileName, const TextureParameters& parameters)
{
	VERSO_ASSERT_MSG("verso-3d", isCreated() == true, "Not yet created!");
	UString hashName = generateTextureHashName(fileName, parameters);
	Hash32 hash = Hash32::fnv1(hashName);

	const auto& it = textures.find(hash);
	if (it != textures.end()) {
		texturesRefCount[hash] = texturesRefCount[hash] + 1;
		return it->second;
	}
	else {
		Texture* texture = new Texture();
		if (defaultTexture == nullptr) {
			texture->createFromFile(*window, fileName, parameters);
		}
		else {
			try {
				texture->createFromFile(*window, fileName, parameters);
			}
			catch (FileNotFoundException&) {
				VERSO_LOG_ERR("verso-3d", "Cannot find texture");
				VERSO_LOG_ERR_VARIABLE("verso-3d", "fileName", fileName.c_str());
				return defaultTexture;
			}
		}

		textures[hash] = texture;
		texturesHashNames[hash] = hashName;
		texturesRefCount[hash] = 1;

		return texture;
	}
}


void ResourceManager::releaseTexture(Texture* texture)
{
	(void)texture;
	// \TODO: implement refcounting or create wrapper object for textureptr with hash
//	UString hashName = generateTextureHashName(texture->getSourceFileName(), texture->getParameters());
//	Hash32 hash = Hash32::fnv1(hashName);

//	const auto& it = textures.find(hash);
//	if (it != textures.end()) {
//		texturesRefCount[hash]--;
//		if (texturesRefCount[hash] >= 0) {
//			it->second->destroy();
//			delete it->second;
//			textures.erase(it);

//			const auto it2 = texturesHashNames.find(hash);
//			texturesHashNames.erase(it2);

//			const auto it3 = texturesRefCount.find(hash);
//			texturesRefCount.erase(it3);
//		}
//	}
}


void ResourceManager::onResize()
{
	VERSO_ASSERT_MSG("verso-3d", isCreated() == true, "Not yet created!");

	for (auto& it : cameras) {
		it.second->onResize();
	}

	for (auto& it : fbos) {
		it.second->onResize();
	}
}


ICamera* ResourceManager::getCamera(const UString& uniqueId)
{
	VERSO_ASSERT("verso-3d", isCreated() == true);
	const auto& it = cameras.find(uniqueId);
	if (it != cameras.end()) {
		return it->second;
	}
	else {
		return nullptr;
	}
}


bool ResourceManager::registerCamera(ICamera* camera)
{
	VERSO_ASSERT("verso-3d", camera != nullptr);
	UString uniqueId = camera->getUniqueId();
	VERSO_ASSERT("verso-3d", isCreated() == true);
	if (getCamera(uniqueId) != nullptr) {
		UString err("Duplicate camera unique id " + uniqueId);
		VERSO_LOG_WARN("verso-3d", err.c_str());
		return false;
	}

	cameras[uniqueId] = camera;
	camerasRefCount[uniqueId] = 1; // \TODO: some acquire free methods for managing this
	return true;
}


void ResourceManager::unregisterCamera(const UString& uniqueId)
{
	VERSO_ASSERT("verso-3d", isCreated() == true);
	if (getCamera(uniqueId) == nullptr) {
		UString err("Cannot find unique id (maybe duplicate?) " + uniqueId);
		VERSO_LOG_WARN("verso-3d", err.c_str());
		return;
	}

	cameras.erase(uniqueId);
	camerasRefCount.erase(uniqueId); // \TODO: some acquire free_methods for managing this
}


Fbo* ResourceManager::getFbo(const UString& uniqueId)
{
	VERSO_ASSERT("verso-3d", isCreated() == true);
	const auto& it = fbos.find(uniqueId);
	if (it == fbos.end()) {
		return fbos[uniqueId];
	}
	else {
		return it->second;
	}
}


Fbo* ResourceManager::createFbo(IWindowOpengl* window, const UString& uniqueId)
{
	VERSO_ASSERT("verso-3d", isCreated() == true);

	if (getFbo(uniqueId) != nullptr) {
		UString err("Duplicated Fbo unique id" + uniqueId);
		VERSO_LOG_WARN("verso-3d", err.c_str());
		return nullptr;
	}

	Fbo* fbo = new Fbo(uniqueId);
	fbo->create(window);
	fbos[uniqueId] = fbo;
	fbosRefCount[uniqueId] = 1; // \TODO: some acquire free_methods for managing this
	return fbo;
}


void ResourceManager::destroyFbo(Fbo* fbo)
{
	VERSO_ASSERT("verso-3d", fbo != nullptr);

	destroyFbo(fbo->getUniqueId());
}


void ResourceManager::destroyFbo(const UString& uniqueId)
{
	VERSO_ASSERT("verso-3d", isCreated() == true);

	Fbo* fbo = getFbo(uniqueId);
	if (fbo != nullptr) {
		fbos.erase(uniqueId);
		fbosRefCount.erase(uniqueId);
		delete[] fbo;
	}
}


UString ResourceManager::generateTextureHashName(const UString& fileName, const TextureParameters& parameters)
{
	VERSO_ASSERT("verso-3d", isCreated() == true);
	UString out;
	out += fileName;
	out += "?";
	out += parameters.typeForShader;
	out += "?";
	out.append2(minFilterNeedMipmaps(parameters.minFilter));
	out += "?";
	out.append2(wrapStyleToString(parameters.wrapStyleS));
	out += "?";
	out.append2(wrapStyleToString(parameters.wrapStyleT));
	return out;
}


UString ResourceManager::toString() const
{
	UString str("TODO");
	return str;
}


UString ResourceManager::toStringDebug() const
{
	UString str("ResourceManager(");
	str += toString();
	str += ")";
	return str;
}


} // End namespace Verso

