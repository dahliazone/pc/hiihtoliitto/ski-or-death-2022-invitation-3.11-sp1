#include <Verso/Render/Opengl.hpp>
#include <Verso/Image/Image.hpp>

namespace Verso {


bool Opengl::saveFramebufferToFile(const Vector2i& resolution, const UString& fileName, const ImageSaveFormat& imageSaveFormat, bool discardAlpha)
{
	Image frameBuffer;
	frameBuffer.create(resolution, true, AspectRatio(resolution, "")); // \TODO: fix aspect ratio if known

	Opengl::readPixelsRgba(Vector2i(0, 0), resolution, frameBuffer.getPixelData());

	frameBuffer.flipVertically();
	bool result = frameBuffer.saveToFile(fileName, imageSaveFormat, discardAlpha);
	frameBuffer.destroy();

	return result;
}


void Opengl::checkForErrors(const UString& message, const UString& method, const UString& file, int line, const UString& identifier)
{
	int maxErrorsToProcess = 10;
	int maxErrorsToPrint = 10;
	int errorsProcessed = 0;
	int errorsPrinted = 0;

	GLenum e = glGetError();
	while (e != GL_NO_ERROR) {
		if (errorsPrinted < maxErrorsToPrint) {
			UString msg;
			switch (e) {
			case GL_INVALID_ENUM:
				msg = "GL_INVALID_ENUM: An unacceptable value is specified for an enumerated argument. The offending command is ignored and has no other side effect than to set the error flag.";
				break;
			case GL_INVALID_VALUE:
				msg = "GL_INVALID_VALUE: A numeric argument is out of range. The offending command is ignored and has no other side effect than to set the error flag.";
				break;
			case GL_INVALID_OPERATION:
				msg = "GL_INVALID_OPERATION: The specified operation is not allowed in the current state. The offending command is ignored and has no other side effect than to set the error flag.";
				break;
			case GL_INVALID_FRAMEBUFFER_OPERATION:
				msg = "GL_INVALID_FRAMEBUFFER_OPERATION: The framebuffer object is not complete. The offending command is ignored and has no other side effect than to set the error flag.";
				break;
			case GL_OUT_OF_MEMORY:
				msg = "GL_OUT_OF_MEMORY: There is not enough memory left to execute the command. The state of the GL is undefined, except for the state of the error flags, after this error is recorded.";
				break;
#if !defined (__APPLE_CC__)
			case GL_STACK_OVERFLOW:
				msg = "GL_STACK_OVERFLOW: This command would cause a stack overflow. The offending command is ignored and has no other side effect than to set the error flag.";
				break;
			case GL_STACK_UNDERFLOW:
				msg = "GL_STACK_UNDERFLOW: This command would cause a stack underflow. The offending command is ignored and has no other side effect than to set the error flag.";
				break;
#endif
			default:
				msg = "UNKNOWN OPENGL ERROR! error code=";
				msg.append2(e);
				break;
			}

			UString error("OpenGL: " + msg);
			VERSO_LOG_WARN_INDIRECT("verso-3d", error.c_str(), method.c_str(), file.c_str(), line);
			if (message.isEmpty() == false) {
				VERSO_LOG_WARN_VARIABLE("verso-3d", "Message", message);
			}
			if (identifier.isEmpty() == false) {
				VERSO_LOG_WARN_VARIABLE("verso-3d", "Identifier", identifier);
			}
			//error += "  - GLU error: "+gluErrorString(e)+"\n"
			//VERSO_LOG_ERR("verso-3d", "EXITING ABRUPTLY BECAUSE OF OPENGL ERROR!");
			//throw std::runtime_error(error.toUtf8());
			//exit(1);
			VERSO_LOG_FLUSH();

			errorsPrinted++;
		}

		errorsProcessed++;
		if (errorsProcessed >= maxErrorsToProcess) {
			break;
		}

		e = glGetError();
	}

#ifndef NDEBUG
	if (errorsPrinted > 0) {
		logErrStacktrace();
	}
#endif
}


void Opengl::printInfo(bool printExtensions)
{
	const GLubyte* renderer = glGetString(GL_RENDERER);
	const GLubyte* vendor = glGetString(GL_VENDOR);
	const GLubyte* version = glGetString(GL_VERSION);
	const GLubyte* glslVersion = glGetString(GL_SHADING_LANGUAGE_VERSION);

	GLint major, minor;
	glGetIntegerv(GL_MAJOR_VERSION, &major);
	GL_CHECK_FOR_ERRORS("", "");
	glGetIntegerv(GL_MINOR_VERSION, &minor);
	GL_CHECK_FOR_ERRORS("", "");

	VERSO_LOG_INFO_VARIABLE("verso-3d", "OpenGL vendor", vendor);
	VERSO_LOG_INFO_VARIABLE("verso-3d", "OpenGL renderer", renderer);
	VERSO_LOG_INFO_VARIABLE("verso-3d", "OpenGL version", major<<"."<<minor<<" ("<<version <<")");
	VERSO_LOG_INFO_VARIABLE("verso-3d", "GLSL version", glslVersion);

	if (printExtensions) {
		GLint count;
		glGetIntegerv(GL_NUM_EXTENSIONS, &count);
		GL_CHECK_FOR_ERRORS("", "");
		for (GLuint i=0; i<static_cast<GLuint>(count); ++i) {
			VERSO_LOG_INFO_VARIABLE("verso-3d", "extension", glGetStringi(GL_EXTENSIONS, i));
			GL_CHECK_FOR_ERRORS("", "");
		}
	}
}


} // End namespace Verso

