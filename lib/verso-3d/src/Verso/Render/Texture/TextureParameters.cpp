#include <Verso/Render/Texture/TextureParameters.hpp>
#include <Verso/GrinderKit/Params/TextureParam.hpp>

namespace Verso {


TextureParameters::TextureParameters() :
	typeForShader("texture"),
	pixelFormat(TexturePixelFormat::Unset),
	minFilter(MinFilter::Default),
	magFilter(MagFilter::Default),
	wrapStyleS(WrapStyle::Default),
	wrapStyleT(WrapStyle::Default)
{
}


TextureParameters::TextureParameters(
		const UString& typeForShader,
		const TexturePixelFormat& pixelFormat,
		const MinFilter& minFilter, const MagFilter& magFilter,
		const WrapStyle& wrapStyleS, const WrapStyle& wrapStyleT) :
	typeForShader(typeForShader),
	pixelFormat(pixelFormat),
	minFilter(minFilter),
	magFilter(magFilter),
	wrapStyleS(wrapStyleS),
	wrapStyleT(wrapStyleT)
{
}


TextureParameters::TextureParameters(const TextureParam& textureParam) :
	typeForShader(textureParam.typeForShader),
	pixelFormat(textureParam.pixelFormat),
	minFilter(textureParam.minFilter),
	magFilter(textureParam.magFilter),
	wrapStyleS(textureParam.wrapStyleS),
	wrapStyleT(textureParam.wrapStyleT)
{
}


TextureParameters::TextureParameters(const TextureParameters& original) :
	typeForShader(original.typeForShader),
	pixelFormat(original.pixelFormat),
	minFilter(original.minFilter),
	magFilter(original.magFilter),
	wrapStyleS(original.wrapStyleS),
	wrapStyleT(original.wrapStyleT)
{
}


TextureParameters::TextureParameters(TextureParameters&& original) :
	typeForShader(std::move(original.typeForShader)),
	pixelFormat(std::move(original.pixelFormat)),
	minFilter(std::move(original.minFilter)),
	magFilter(std::move(original.magFilter)),
	wrapStyleS(std::move(original.wrapStyleS)),
	wrapStyleT(std::move(original.wrapStyleT))
{
}


TextureParameters& TextureParameters::operator =(const TextureParameters& original)
{
	if (this != &original) {
		typeForShader = original.typeForShader;
		pixelFormat = original.pixelFormat;
		minFilter = original.minFilter;
		magFilter = original.magFilter;
		wrapStyleS = original.wrapStyleS;
		wrapStyleT = original.wrapStyleT;
	}
	return *this;
}


TextureParameters& TextureParameters::operator =(TextureParameters&& original)
{
	if (this != &original) {
		typeForShader = std::move(original.typeForShader);
		pixelFormat = std::move(original.pixelFormat);
		minFilter = std::move(original.minFilter);
		magFilter = std::move(original.magFilter);
		wrapStyleS = std::move(original.wrapStyleS);
		wrapStyleT = std::move(original.wrapStyleT);
	}
	return *this;
}


TextureParameters::~TextureParameters()
{
}


UString TextureParameters::toString() const
{
	UString str;
	str += "typeForShader=";
	str += typeForShader;
	str += ", pixelFormat=";
	str += texturePixelFormatToString(pixelFormat);
	str += ", minFilter=";
	str += minFilterToString(minFilter);
	str += ", magFilter=";
	str += magFilterToString(magFilter);
	str += ", wrapStyleS=";
	str += wrapStyleToString(wrapStyleS);
	str += ", wrapStyleT=";
	str += wrapStyleToString(wrapStyleT);
	return str;
}


UString TextureParameters::toStringDebug() const
{
	UString str("TextureParameters(");
	str += toString();
	str += ")";
	return str;
}


} // End namespace Verso

