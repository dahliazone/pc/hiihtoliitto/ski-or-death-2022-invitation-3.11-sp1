#include <Verso/Render/Render.hpp>
#include <Verso/Math/ColorGenerator.hpp>

namespace Verso {


bool TODO_Render_should_be_based_on_WindowOpengl_and_NOT_static_global;


bool Render::created = false;
UString Render::shadersPath = "";
UString Render::shadersVersoPath = "";
Vao Render::quad2dVao = Vao("verso-3d/Render::quad2dVao");
Vao Render::quad2dTextureInvYVao = Vao("verso-3d/Render::quad2dTextureInvYVao");
Vao Render::quad3dVao = Vao("verso-3d/Render::quad3dVao");
Vao Render::quad3dTextureInvYVao = Vao("verso-3d/Render::quad3dTextureInvYVao");
ShaderProgram Render::defaultShaderGui2d = ShaderProgram();
ShaderProgram Render::defaultShaderGui3d = ShaderProgram();
ShaderProgram Render::defaultShaderSprite3d = ShaderProgram();
Vao Render::vaoDebugGridPlane1("verso-3d/Render::vaoDebugGridPlane1");
Vao Render::vaoDebugGridPlane5("verso-3d/Render::vaoDebugGridPlane5");
Vao Render::vaoDebugGridPlane10("verso-3d/Render::vaoDebugGridPlane10");
MaterialColor3d Render::matDebugGridPlane1;
MaterialColor3d Render::matDebugGridPlane5;
MaterialColor3d Render::matDebugGridPlane10;
MaterialNormalVisualizer3d Render::matNormalVisualizer;
Vao Render::vaoCamera("verso-3d/Render::vaoCamera");
MaterialColor3d Render::matCamera;
Vao Render::vaoLight("verso-3d/Render::vaoLight");
MaterialColor3d Render::matLight;


///////////////////////////////////////////////////////////////////////////////////////////
// static
///////////////////////////////////////////////////////////////////////////////////////////

void Render::create(IWindowOpengl& window, const UString& shadersPath, const UString& shadersVersoPath)
{
	VERSO_ASSERT_MSG("verso-3d", isCreated() == false, "Already created!");

	Render::shadersPath = shadersPath;
	Render::shadersVersoPath = shadersVersoPath;

	// Default quads
	VaoGenerator::quad2d(quad2dVao, Vector2f(1.0f, 1.0f));
	quad2dVao.bind();

	VaoGenerator::quad2d(quad2dTextureInvYVao, Vector2f(1.0f, 1.0f), Vector2f(0.0f, 0.0f),
						 static_cast<BufferTypes>(BufferType::Default), RgbaColorf(1.0f, 0.5f, 0.2f), true);
	quad2dTextureInvYVao.bind();

	VaoGenerator::quad3d(quad3dVao, Vector2f(1.0f, 1.0f));
	quad3dVao.bind();

	VaoGenerator::quad3d(quad3dTextureInvYVao, Vector2f(1.0f, 1.0f), Vector3f(0.0f, 0.0f, 0.0f),
						 static_cast<BufferTypes>(BufferType::Default), RgbaColorf(1.0f, 0.5f, 0.2f), true);
	quad3dTextureInvYVao.bind();

	// Default shaders
	defaultShaderGui2d.createFromFiles(Render::shadersVersoPath+"render/gui2d-texture.330.vert",
									   Render::shadersVersoPath+"render/gui2d-texture.330.frag");
	defaultShaderGui2d.linkProgram();
	// -> getDefaultGui2dShader()
	defaultShaderGui3d.createFromFiles(Render::shadersVersoPath+"render/gui3d-texture.330.vert",
									   Render::shadersVersoPath+"render/gui3d-texture.330.frag");
	defaultShaderGui3d.linkProgram();
	// -> getDefaultGui3dShader()
	defaultShaderSprite3d.createFromFiles(Render::shadersVersoPath+"render/sprite3d.330.vert",
										  Render::shadersVersoPath+"render/sprite3d.330.frag");
	defaultShaderSprite3d.linkProgram();

	// Debug grid plane
	matDebugGridPlane1.create(window, ColorGenerator::getColor(ColorRgb::BlenderGridPlaneGray));
	matDebugGridPlane5.create(window, ColorGenerator::getColor(ColorRgb::BlenderGridPlaneGray));
	matDebugGridPlane10.create(window, ColorGenerator::getColor(ColorRgb::BlenderGridPlaneGray));
	Vector2i gridSize(100, 100);
	VaoGenerator::gridPlane3dLines(vaoDebugGridPlane1, Vector2f(1.0f, 1.0f), Vector2i(gridSize.x, gridSize.y), Vector2f(1.0f, 1.0f), Vector3f(0.0f, 0.0f, 0.0f), static_cast<BufferTypes>(BufferType::All), RgbaColorf(1.0f, 0.0f, 0.0f));
	VaoGenerator::gridPlane3dLines(vaoDebugGridPlane5, Vector2f(5.0f, 5.0f), Vector2i(gridSize.x/5, gridSize.y/5), Vector2f(1.0f, 1.0f), Vector3f(0.0f, 0.0f, 0.0f), static_cast<BufferTypes>(BufferType::All), RgbaColorf(1.0f, 0.0f, 0.0f));
	VaoGenerator::gridPlane3dLines(vaoDebugGridPlane10, Vector2f(10.0f, 10.0f), Vector2i(gridSize.x/10, gridSize.y/10), Vector2f(1.0f, 1.0f), Vector3f(0.0f, 0.0f, 0.0f), static_cast<BufferTypes>(BufferType::All), RgbaColorf(1.0f, 0.0f, 0.0f));

	// Debug normals
	matNormalVisualizer.create(window, 1.0f);

	// Debug cameras
	matCamera.create(window, ColorGenerator::getColor(ColorRgb::DeepPink));
	VaoGenerator::cube3d(vaoCamera, Vector3f(0.5f, 0.5f, 0.5f), Vector3f(), static_cast<BufferTypes>(BufferType::Default), RgbaColorf(1.0f, 0.0f, 1.0f));

	// Debug lights
	matLight.create(window, ColorGenerator::getColor(ColorRgb::AntiqueWhite));
	VaoGenerator::cube3d(vaoLight, Vector3f(0.5f, 0.5f, 0.5f), Vector3f(), static_cast<BufferTypes>(BufferType::Default), RgbaColorf(1.0f, 1.0f, 0.0f));

	created = true;
}


void Render::destroy() VERSO_NOEXCEPT
{
	if (isCreated() == false) {
		return;
	}

	// Debug lights
	vaoLight.destroy();
	matLight.destroy();

	// Debug cameras
	vaoCamera.destroy();
	matCamera.destroy();

	// Debug normals
	matNormalVisualizer.destroy();

	// Debug grid plane
	vaoDebugGridPlane10.destroy();
	vaoDebugGridPlane5.destroy();
	vaoDebugGridPlane1.destroy();
	matDebugGridPlane10.destroy();
	matDebugGridPlane5.destroy();
	matDebugGridPlane1.destroy();

	// Default shaders
	defaultShaderSprite3d.destroy();
	defaultShaderGui3d.destroy();
	defaultShaderGui2d.destroy();

	// Default quads
	quad3dVao.destroy();
	quad3dTextureInvYVao.destroy();
	quad2dTextureInvYVao.destroy();
	quad2dVao.destroy();

	created = false;
}


bool Render::isCreated()
{
	return created;
}


void Render::clearScreen(ClearFlag clearFlag, RgbaColorf clearColor)
{
	switch (clearFlag) {
	case ClearFlag::Unset:
		break;
	case ClearFlag::None:
		break;
	case ClearFlag::ColorBuffer:
		Render::clearScreen(clearColor, true, false);
		break;
	case ClearFlag::DepthBuffer:
		Render::clearScreen(clearColor, false, true);
		break;
	case ClearFlag::SolidColor:
		Render::clearScreen(clearColor, true, true);
		break;
	case ClearFlag::Skybox:
		VERSO_FAIL("verso-3d"); // \TODO: implement
		//Render::clearScreen(clearColor, true, true);
		//break;
	default:
	{
		UString error("Unknown ClearFlag(");
		error.append2(static_cast<int>(clearFlag));
		error += ")";
		VERSO_ERROR("verso-3d", error.c_str(), "");
	}
	}
}


void Render::clearScreen(const RgbaColorf& color, bool clearColor, bool clearDepth)
{
	clearScreen(color.r, color.g, color.b, clearColor, clearDepth);
}


void Render::clearScreen(GLfloat red, GLfloat green, GLfloat blue, bool clearColor, bool clearDepth)
{
	glClearColor(red, green, blue, 0);
	GL_CHECK_FOR_ERRORS("", "");

	GLbitfield flags = 0;
	if (clearColor == true) {
		flags |= GL_COLOR_BUFFER_BIT;
	}
	if (clearDepth == true) {
		flags |= GL_DEPTH_BUFFER_BIT;
	}

	glClear(flags);
	GL_CHECK_FOR_ERRORS("", "");
}


void Render::debugGrid(IWindowOpengl& window, const FrameTimestamp& time, const ICamera& camera, bool grid1x1, bool grid5x5, bool grid10x10)
{
	PolygonRenderMode oldRenderMode = Opengl::getPolygonRenderMode();
	Opengl::setPolygonRenderMode(PolygonRenderMode::Line);

	Matrix4x4f model;

	if (grid1x1 == true) {
		matDebugGridPlane1.apply(window, time, camera, std::vector<DirectionalLight>(),
								 std::vector<PointLight>(), std::vector<SpotLight>());
		matDebugGridPlane1.updateModelMatrix(model);
		vaoDebugGridPlane1.render();
	}

	if (grid5x5 == true) {
		matDebugGridPlane5.apply(window, time, camera, std::vector<DirectionalLight>(),
								 std::vector<PointLight>(), std::vector<SpotLight>());
		matDebugGridPlane5.updateModelMatrix(model);
		vaoDebugGridPlane5.render();
	}

	if (grid10x10 == true) {
		matDebugGridPlane10.apply(window, time, camera, std::vector<DirectionalLight>(),
								  std::vector<PointLight>(), std::vector<SpotLight>());
		matDebugGridPlane10.updateModelMatrix(model);
		vaoDebugGridPlane10.render();
	}

	if (oldRenderMode != PolygonRenderMode::Unset) {
		Opengl::setPolygonRenderMode(oldRenderMode);
	}
}


void Render::debugNormals(IWindowOpengl& window, const FrameTimestamp& time, const ICamera& camera,
						  Vao& vao, const Matrix4x4f& modelMatrix)
{
	matNormalVisualizer.apply(window, time, camera, std::vector<DirectionalLight>(),
							  std::vector<PointLight>(), std::vector<SpotLight>());
	matNormalVisualizer.updateModelMatrix(modelMatrix);
	vao.render();
}


void Render::debugCamerasAndLights(IWindowOpengl& window, const FrameTimestamp& time, const ICamera& camera,
								   const std::vector<ICamera*>& cameras,
								   const std::vector<DirectionalLight>& directionalLights,
								   const std::vector<PointLight>& pointLights,
								   const std::vector<SpotLight>& spotLights)
{
	Matrix4x4f model;

	// Cameras
	for (size_t i=0; i<cameras.size(); ++i) {
		matCamera.apply(window, time, camera, std::vector<DirectionalLight>(),
						std::vector<PointLight>(), std::vector<SpotLight>());

		model.set(Matrix4x4f::Identity());
		//model = Matrix4x4f::scale(model, Vector3f(scale));
		model = Matrix4x4f::translate(model, cameras[i]->getPosition());
		//model = Matrix4x4f::rotate(model, angle, Vector3f(1.0f, 0.3f, 0.5f));

		matCamera.updateModelMatrix(model);
		vaoCamera.render();
	}

	// Directional lights
	for (size_t i=0; i<directionalLights.size(); ++i) {
		matLight.apply(window, time, camera, std::vector<DirectionalLight>(),
					   std::vector<PointLight>(), std::vector<SpotLight>());

		model.set(Matrix4x4f::Identity());
		//model = Matrix4x4f::scale(model, Vector3f(scale));
		model = Matrix4x4f::translate(model, Vector3f(0.0f, 0.0f, 0.0f)); // directional light doesn't have a position
		//model = Matrix4x4f::rotate(model, angle, Vector3f(1.0f, 0.3f, 0.5f));

		matLight.updateModelMatrix(model);
		vaoLight.render();
	}

	// Pointlights
	for (size_t i=0; i<pointLights.size(); ++i) {
		matLight.apply(window, time, camera, std::vector<DirectionalLight>(),
					   std::vector<PointLight>(), std::vector<SpotLight>());

		model.set(Matrix4x4f::Identity());
		//model = Matrix4x4f::scale(model, Vector3f(scale));
		model = Matrix4x4f::translate(model, pointLights[i].position);
		//model = Matrix4x4f::rotate(model, angle, Vector3f(1.0f, 0.3f, 0.5f));

		matLight.updateModelMatrix(model);
		vaoLight.render();
	}

	// Spotlights
	for (size_t i=0; i<spotLights.size(); ++i) {
		matLight.apply(window, time, camera, std::vector<DirectionalLight>(),
					   std::vector<PointLight>(), std::vector<SpotLight>());

		model.set(Matrix4x4f::Identity());
		//model = Matrix4x4f::scale(model, Vector3f(scale));
		model = Matrix4x4f::translate(model, spotLights[i].position);
		//model = Matrix4x4f::rotate(model, angle, Vector3f(1.0f, 0.3f, 0.5f));

		matLight.updateModelMatrix(model);
		vaoLight.render();
	}
}


void Render::draw2d(IWindowOpengl& window, ICamera& camera, Texture& texture, const Vector3f& point,
					const Align& align, const Vector2f& relativeDestSize, RefScale refScale,
					float alpha, Degree angleZ, bool textureInvertY)
{
	draw2d(window, camera, texture, point, align.pointOffsetCenteredQuad(), relativeDestSize, refScale, alpha, angleZ, textureInvertY);
}


void Render::draw2d(IWindowOpengl& window, ICamera& camera, Texture& texture, const Vector3f& point,
					const Vector2f& relOffset, const Vector2f& relativeDestSize, RefScale refScale,
					float alpha, Degree angleZ, bool textureInvertY)
{
	VERSO_ASSERT("verso-3d", isCreated() == true && "Render::create() must be called before using any other methods!");

	if (texture.isCreated() == false) {
		VERSO_ERROR("verso-3d", "Image texture not created", texture.getSourceFileName().c_str());
	}

	Vector2f refScaleSize = calculateRefScaleSize(
				refScale, relativeDestSize,
				texture.getResolutionf(), texture.getAspectRatioTarget().value,
				window.getRenderViewporti().size);

	Vector3f screenPoint = camera.viewportToScreenPoint(point, window.getRenderViewporti().size);

	Matrix4x4f model(Matrix4x4f::Identity());

	model = Matrix4x4f::translate(model, screenPoint);

	model = Matrix4x4f::rotate(model, angleZ, Vector3f(0.0f, 0.0f, 1.0f));

	model = Matrix4x4f::scale(model,
							  refScaleSize.x,
							  refScaleSize.y,
							  1.0f);

	// Setup shader
	defaultShaderGui2d.useProgram();
	defaultShaderGui2d.setUniform("model", model);
	defaultShaderGui2d.setUniform("view", camera.getViewMatrix());
	defaultShaderGui2d.setUniform("projection", camera.getProjectionMatrix());
	defaultShaderGui2d.setUniform("meshOffset", relOffset);
	defaultShaderGui2d.setUniformf("alpha", alpha);

	// Setup texture
	defaultShaderGui2d.setUniformi("iChannel0", 0);
	window.setActiveTextureUnit(0);
	texture.bind();

	// Render
	if (textureInvertY == false) {
		quad2dVao.render();
	}
	else {
		quad2dTextureInvYVao.render();
	}
}


void Render::draw2dDrawableResolution(
		IWindowOpengl& window, ICamera& camera, Texture& texture, const Vector3f& point,
		const Align& align, const Vector2f& relativeDestSize, RefScale refScale,
		float alpha, Degree angleZ, bool textureInvertY, bool applyPixelAspectRatio)
{
	draw2dDrawableResolution(
				window, camera, texture, point, align.pointOffsetCenteredQuad(),
				relativeDestSize, refScale, alpha, angleZ, textureInvertY,
				applyPixelAspectRatio);
}


void Render::draw2dDrawableResolution(
		IWindowOpengl& window, ICamera& camera, Texture& texture, const Vector3f& point,
		const Vector2f& relOffset, const Vector2f& relativeDestSize, RefScale refScale,
		float alpha, Degree angleZ, bool textureInvertY, bool applyPixelAspectRatio)
{
	VERSO_ASSERT("verso-3d", isCreated() == true && "Render::create() must be called before using any other methods!");

	if (texture.isCreated() == false) {
		VERSO_ERROR("verso-3d", "Image texture not created", texture.getSourceFileName().c_str());
	}

	float aspectRatioTexture = texture.getAspectRatioTarget().value;
	if (applyPixelAspectRatio == true) {
		aspectRatioTexture *= window.getRenderPixelAspectRatio().value;
	}

	Vector2f refScaleSize = calculateRefScaleSize(
				refScale, relativeDestSize,
				texture.getResolutionf(), aspectRatioTexture,
				window.getDrawableViewporti().size);

	Vector3f screenPoint = camera.viewportToScreenPoint(point, window.getDrawableViewporti().size);

	Matrix4x4f model(Matrix4x4f::Identity());

	model = Matrix4x4f::translate(model, screenPoint);

	model = Matrix4x4f::rotate(model, angleZ, Vector3f(0.0f, 0.0f, 1.0f));

	model = Matrix4x4f::scale(model,
							  refScaleSize.x,
							  refScaleSize.y,
							  1.0f);

	// Setup shader
	defaultShaderGui2d.useProgram();
	defaultShaderGui2d.setUniform("model", model);
	defaultShaderGui2d.setUniform("view", camera.getViewMatrix());
	defaultShaderGui2d.setUniform("projection", camera.getProjectionMatrix());
	defaultShaderGui2d.setUniform("meshOffset", relOffset);
	defaultShaderGui2d.setUniformf("alpha", alpha);

	// Setup texture
	defaultShaderGui2d.setUniformi("iChannel0", 0);
	window.setActiveTextureUnit(0);
	texture.bind();

	// Render
	if (textureInvertY == false) {
		quad2dVao.render();
	}
	else {
		quad2dTextureInvYVao.render();
	}
}


} // End namespace Verso

