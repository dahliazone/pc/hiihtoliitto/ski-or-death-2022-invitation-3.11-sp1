#include <Verso/Render/Font/BitmapFontFixedWidth.hpp>
#include <Verso/Render/Render.hpp>
#include <Verso/Render/VaoGenerator/VaoGenerator.hpp>

namespace Verso {


BitmapFontFixedWidth::BitmapFontFixedWidth() :
	created(false),
	vao(nullptr),
	fontTexture(nullptr),
	firstFontCharacterIndex(0),
	characterTextureUvSize(1.0f / 16.0f, 1.0f / 16.0f),
	fontShader(nullptr),
	fontShaderInvididual(nullptr),
	text(""),
	tabSize(4)
{
}


BitmapFontFixedWidth::BitmapFontFixedWidth(BitmapFontFixedWidth&& original) noexcept :
	created(std::move(original.created)),
	vao(std::move(original.vao)),
	fontTexture(std::move(original.fontTexture)),
	firstFontCharacterIndex(std::move(original.firstFontCharacterIndex)),
	characterTextureUvSize(std::move(original.characterTextureUvSize)),
	fontShader(std::move(original.fontShader)),
	fontShaderInvididual(std::move(original.fontShaderInvididual)),
	text(std::move(original.text)),
	tabSize(std::move(original.tabSize))
{
}


BitmapFontFixedWidth& BitmapFontFixedWidth::operator =(BitmapFontFixedWidth&& original) noexcept
{
	if (this != &original) {
		created = std::move(original.created);
		vao = std::move(original.vao);
		fontTexture = std::move(original.fontTexture);
		firstFontCharacterIndex = std::move(original.firstFontCharacterIndex);
		characterTextureUvSize = std::move(original.characterTextureUvSize);
		fontShader = std::move(original.fontShader);
		fontShaderInvididual = std::move(original.fontShaderInvididual);
		text = std::move(original.text);
		tabSize = std::move(original.tabSize);
	}
	return *this;
}


BitmapFontFixedWidth::~BitmapFontFixedWidth()
{
	destroy();
}


void BitmapFontFixedWidth::createFromFile(IWindowOpengl& window, const UString& fileName,
										  const MinFilter& minFilter, const MagFilter& magFilter,
										  const Vector2f& characterTextureUvSize, size_t firstFontCharacterIndex, size_t tabSize)
{
	if (this->fontTexture != nullptr) {
		VERSO_ERROR("verso-3d", "BitmapFontFixedWidth already initialized", "");
	}

	this->vao = new Vao("BitmapFontFixedWidth/vao/" + fileName);
	this->vao->create(PrimitiveType::Triangles);
	this->vao->bind();

	TextureParameters parameters("font", TexturePixelFormat::Unset,
								 minFilter, magFilter,
								 WrapStyle::ClampToEdge, WrapStyle::ClampToEdge);

	this->fontTexture = new Texture();
	this->fontTexture->createFromFile(window, fileName, parameters);

	this->characterTextureUvSize = characterTextureUvSize;

	this->fontShader = new ShaderProgram();
	this->fontShader->createFromFiles(
				Render::shadersVersoPath+"render/bitmapfontfixedwidth.330.vert",
				Render::shadersVersoPath+"render/bitmapfontfixedwidth.330.frag");
	this->fontShader->linkProgram();

	this->fontShaderInvididual = new ShaderProgram();
	this->fontShaderInvididual->createFromFiles(
				Render::shadersVersoPath+"render/bitmapfontfixedwidth_invididual.330.vert",
				Render::shadersVersoPath+"render/bitmapfontfixedwidth_invididual.330.frag");
	this->fontShaderInvididual->linkProgram();

	this->firstFontCharacterIndex = firstFontCharacterIndex;

	this->text.clear();
	this->tabSize = tabSize;
	this->created = true;
}


void BitmapFontFixedWidth::destroy() VERSO_NOEXCEPT
{
	text.clear();

	delete fontShaderInvididual;
	fontShaderInvididual = nullptr;

	delete fontShader;
	fontShader = nullptr;

	delete fontTexture;
	fontTexture = nullptr;

	delete vao;
	vao = nullptr;

	created = false;
}


const UString& BitmapFontFixedWidth::getText() const
{
	return text;
}


void BitmapFontFixedWidth::updateText(const UString& text, const Vector2f& relativeSpacing, bool textureInvertY)
{
	if (this->vao == nullptr || !this->vao->isCreated()) {
		VERSO_ERROR("verso-3d", "BitmapFontFixedWidth::vao not initialized", "");
	}
	if (this->text.equals(text)) {
		return;
	}

	Vector2f size(1, 1);
	Vector3f offsetPosition = Vector3f(0.0f, 0.0f, 0.0f);

	Vector2f relativeDestCharacterSize(1.0f, 1.0f);
	Vector2f spacing = relativeDestCharacterSize + relativeSpacing;

	Vector3f point(relativeDestCharacterSize.x/2.0f, relativeDestCharacterSize.y/2.0f, 0.0f);
	Vector3f currentPosition(point);

	Vector2f characterRelOffset(Align(HAlign::Left, VAlign::Top).pointOffsetCenteredQuad());

	for (size_t i = 0, columnIndex = 0; i < text.getLength(); ++i) {
		unsigned int characterIndex = text.charAt(static_cast<int>(i));
		if (characterIndex == '\n') {
			currentPosition.x = point.x;
			currentPosition.y += spacing.y;
			columnIndex = 0;
		}

		else if (characterIndex == '\t') {
			columnIndex += tabSize;
		}

		else {
			Vector3f offset(
						currentPosition.x + spacing.x * columnIndex,
						currentPosition.y,
						currentPosition.z);

			appendCharacterToVao(characterIndex, offset, relativeDestCharacterSize, textureInvertY);

			columnIndex++;
		}
	}

	if (text.getLength() == 0) {
		this->vao->destroy();
		this->vao->create(PrimitiveType::Triangles);
		this->vao->bind();
	}
	else {
		// \TODO: BUG: for empty lines \n in the beginning text.getLength() == 0 does not work!
		this->vao->applyAppendedData(BufferUsagePattern::DynamicDraw);
	}

	this->text = text;
}


void BitmapFontFixedWidth::renderText(
		IWindowOpengl& window, ICamera& camera,
		const Vector3f& point,
		const Vector2f& relativeDestCharacterSize,
		RefScale refScale,
		float alpha,
		Degree angleZ)
{
	if (this->fontTexture == nullptr) {
		VERSO_ERROR("verso-3d", "BitmapFontFixedWidth::fontTexture not initialized", "");
	}
	if (this->fontShader == nullptr) {
		VERSO_ERROR("verso-3d", "BitmapFontFixedWidth::fontShader not initialized", "");
	}
	if (text.getLength() == 0) {
		return;
	}

	Vector2f refScaleSize = calculateRefScaleSize(
				refScale, relativeDestCharacterSize,
				fontTexture->getResolutionf(), fontTexture->getAspectRatioTarget().value,
				window.getRenderViewporti().size);

	Vector3f screenPoint = camera.viewportToScreenPoint(point, window.getRenderViewporti().size);

	Matrix4x4f model(Matrix4x4f::Identity());

	model = Matrix4x4f::translate(model, screenPoint);

	model = Matrix4x4f::rotate(model, angleZ, Vector3f(0.0f, 0.0f, 1.0f));

	model = Matrix4x4f::scale(model,
							  refScaleSize.x,
							  refScaleSize.y,
							  1.0f);

	// Setup shader
	this->fontShader->useProgram();
	this->fontShader->setUniform("model", model);
	this->fontShader->setUniform("view", camera.getViewMatrix());
	this->fontShader->setUniform("projection", camera.getProjectionMatrix());
	this->fontShader->setUniformf("alpha", alpha);

	// Setup texture
	this->fontShader->setUniformi("material.diffuseTexture", 0);
	window.setActiveTextureUnit(0);
	fontTexture->bind();

	// Render
	this->vao->render();
}


void BitmapFontFixedWidth::renderTextIndividualLetters(
		IWindowOpengl& window, ICamera& camera, const UString& text, const Vector3f& point,
		const Vector2f& relativeDestCharacterSize, const Vector2f& relativeSpacing,
		RefScale refScale, float alpha, Degree angleZ, bool textureInvertY)
{
	if (this->fontTexture == nullptr) {
		VERSO_ERROR("verso-3d", "BitmapFontFixedWidth::fontTexture not initialized", "");
	}
	if (this->fontShader == nullptr) {
		VERSO_ERROR("verso-3d", "BitmapFontFixedWidth::fontShader not initialized", "");
	}

	Vector3f currentPosition(point);

	Vector2f characterRelOffset(Align(HAlign::Left, VAlign::Top).pointOffsetCenteredQuad());

	Vector2f spacing = relativeDestCharacterSize + relativeSpacing;

	for (size_t i = 0, columnIndex = 0; i < text.getLength(); ++i) {
		unsigned int characterIndex = text.charAt(static_cast<int>(i));
		if (characterIndex == '\n') {
			currentPosition.x = point.x;
			currentPosition.y += spacing.y;
			columnIndex = 0;
		}

		else if (characterIndex == '\t') {
			columnIndex += tabSize;
		}

		else {
			Vector3f offset(
						currentPosition.x + spacing.x * columnIndex,
						currentPosition.y,
						currentPosition.z);

			renderCharacter(window, camera, characterIndex, offset,
							relativeDestCharacterSize, refScale,
							alpha, angleZ, textureInvertY);

			columnIndex++;
		}
	}
}


void BitmapFontFixedWidth::renderCharacter(IWindowOpengl& window, ICamera& camera, int characterIndex,
										   const Vector3f& point,
										   const Vector2f& relativeDestCharacterSize, RefScale refScale,
										   float alpha, Degree angleZ, bool textureInvertY)
{
	Vector2f refScaleSize = calculateRefScaleSize(
				refScale, relativeDestCharacterSize,
				fontTexture->getResolutionf(), fontTexture->getAspectRatioTarget().value,
				window.getRenderViewporti().size);

	Vector2f relOffset(-0.5f, -0.5f);

	Vector3f screenPoint = camera.viewportToScreenPoint(point, window.getRenderViewporti().size);

	Matrix4x4f model(Matrix4x4f::Identity());

	model = Matrix4x4f::translate(model, screenPoint);

	model = Matrix4x4f::rotate(model, angleZ, Vector3f(0.0f, 0.0f, 1.0f));

	model = Matrix4x4f::scale(model,
							  refScaleSize.x,
							  refScaleSize.y,
							  1.0f);

	characterIndex -= static_cast<int>(firstFontCharacterIndex);
	int charPosY = static_cast<int>(characterIndex / 16.0f);
	int charPosX = characterIndex - static_cast<int>(charPosY * 16.0f);

	// Setup shader
	this->fontShaderInvididual->useProgram();
	this->fontShaderInvididual->setUniform("model", model);
	this->fontShaderInvididual->setUniform("view", camera.getViewMatrix());
	this->fontShaderInvididual->setUniform("projection", camera.getProjectionMatrix());
	this->fontShaderInvididual->setUniformf("alpha", alpha);
	this->fontShaderInvididual->setUniform("uvOffset", Vector2f(
									 charPosX * characterTextureUvSize.x,
									 charPosY * characterTextureUvSize.y));
	this->fontShaderInvididual->setUniform("meshOffset", relOffset);
	this->fontShaderInvididual->setUniform("uvScale", Vector2f(
									 characterTextureUvSize.x,
									 characterTextureUvSize.y));

	// Setup texture
	this->fontShaderInvididual->setUniformi("iChannel0", 0);
	window.setActiveTextureUnit(0);
	fontTexture->bind();

	// Render
	if (textureInvertY == false) {
		Render::quad2dVao.render();
	}
	else {
		Render::quad2dTextureInvYVao.render();
	}
}


void BitmapFontFixedWidth::appendCharacterToVao(int characterIndex, const Vector3f& offsetPosition, const Vector2f& size, bool textureInvertY)
{
	unsigned int nextAppendIndex = this->vao->getNextDatafAppendIndex(ShaderAttribute::Position);

	// Position
	Vector3f scaled = size * 0.5f;
	const Vector3f& op = offsetPosition;
	GLfloat positions[] = {
		op.x + scaled.x, op.y - scaled.y, op.z, // Top Right
		op.x + scaled.x, op.y + scaled.y, op.z, // Bottom Right
		op.x - scaled.x, op.y + scaled.y, op.z, // Bottom Left
		op.x - scaled.x, op.y - scaled.y, op.z  // Top Left
	};
	this->vao->appendDataf(ShaderAttribute::Position, positions, 4, 3);

	// Uv
	characterIndex -= static_cast<int>(firstFontCharacterIndex);
	int charPosY = static_cast<int>(characterIndex / 16.0f);
	int charPosX = characterIndex - static_cast<int>(charPosY * 16.0f);
	GLfloat minX = charPosX * characterTextureUvSize.x;
	GLfloat maxX = minX + characterTextureUvSize.x;
	GLfloat minY = charPosY * characterTextureUvSize.y;
	GLfloat maxY = minY + characterTextureUvSize.y;
	if (textureInvertY == false) {
		GLfloat uvs[] = {
			maxX, maxY, // Top Right
			maxX, minY, // Bottom Right
			minX, minY, // Bottom Left
			minX, maxY, // Top Left
		};
		this->vao->appendDataf(ShaderAttribute::Uv, uvs, 4, 2);
	}
	else {
		GLfloat uvs[] = {
			maxX, minY, // Top Right
			maxX, maxY, // Bottom Right
			minX, maxY, // Bottom Left
			minX, minY, // Top Left
		};
		this->vao->appendDataf(ShaderAttribute::Uv, uvs, 4, 2);
	}

	// Indices
	GLuint indices[] = {
		nextAppendIndex + 0, nextAppendIndex + 1, nextAppendIndex + 3,  // First Triangle
		nextAppendIndex + 1, nextAppendIndex + 2, nextAppendIndex + 3   // Second Triangle
	};
	this->vao->appendIndices(indices, 6);
}


} // End namespace Verso

