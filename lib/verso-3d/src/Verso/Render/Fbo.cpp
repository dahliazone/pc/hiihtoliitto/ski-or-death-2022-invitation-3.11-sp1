#include <Verso/Render/Fbo.hpp>
#include <Verso/Display/IWindowOpengl.hpp>

namespace Verso {


class TemporaryFboBind
{
private:
	GLint lastFrameBufferRead;
	GLint lastFrameBufferDraw;
	GLint handleToBeDeleted;

public:
	explicit TemporaryFboBind(GLint handle, GLint handleToBeDeleted = 0) :
		lastFrameBufferRead(0),
		lastFrameBufferDraw(0),
		handleToBeDeleted(handleToBeDeleted)
	{
		glGetIntegerv(GL_READ_FRAMEBUFFER_BINDING, &lastFrameBufferRead);
		GL_CHECK_FOR_ERRORS("", "");

		glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &lastFrameBufferDraw);
		GL_CHECK_FOR_ERRORS("", "");

		glBindFramebuffer(GL_FRAMEBUFFER, handle);
		GL_CHECK_FOR_ERRORS("", "");
	}


	~TemporaryFboBind()
	{
		unbind();
	}


	void unbind()
	{
		if (handleToBeDeleted == 0) {
			glBindFramebuffer(GL_READ_FRAMEBUFFER, lastFrameBufferRead);
			GL_CHECK_FOR_ERRORS("", "");

			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, lastFrameBufferDraw);
			GL_CHECK_FOR_ERRORS("", "");
		}
		else {
			if (handleToBeDeleted == lastFrameBufferRead) {
				glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
				GL_CHECK_FOR_ERRORS("", "");
			}
			else {
				glBindFramebuffer(GL_READ_FRAMEBUFFER, lastFrameBufferRead);
				GL_CHECK_FOR_ERRORS("", "");
			}

			if (handleToBeDeleted == lastFrameBufferDraw) {
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
				GL_CHECK_FOR_ERRORS("", "");
			}
			else {
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, lastFrameBufferDraw);
				GL_CHECK_FOR_ERRORS("", "");
			}
		}
	}
};


// \TODO: support for RenderBuffers


GLuint Fbo::lastBoundReadHandle = 0;
GLuint Fbo::lastBoundDrawHandle = 0;
const int Fbo::MaxColorAttachments = GL_MAX_COLOR_ATTACHMENTS;


Fbo::Fbo(const UString& uniqueId) :
	created(false),
	uniqueId(uniqueId),
	window(nullptr),
	handle(0),
	relativeViewport(0.0f, 0.0f, 1.0f, 1.0f),
	colorsRead(),
	colorsDraw(),
	depthRead(nullptr),
	depthDraw(nullptr),
	stencilRead(nullptr),
	stencilDraw(nullptr)
{
}


Fbo::Fbo(Fbo&& original) :
	created(std::move(original.created)),
	uniqueId(std::move(original.uniqueId)),
	window(std::move(original.window)),
	handle(std::move(original.handle)),
	relativeViewport(std::move(original.relativeViewport)),
	colorsRead(std::move(original.colorsRead)),
	colorsDraw(std::move(original.colorsDraw)),
	depthRead(std::move(original.depthRead)),
	depthDraw(std::move(original.depthDraw)),
	stencilRead(std::move(original.stencilRead)),
	stencilDraw(std::move(original.stencilDraw))
{
	original.created = false;
	original.uniqueId.clear();
	original.window = nullptr;
	original.handle = 0;
	original.colorsRead.clear();
	original.colorsDraw.clear();
	original.depthRead = nullptr;
	original.depthDraw = nullptr;
	original.stencilRead = nullptr;
	original.stencilDraw = nullptr;
}


Fbo& Fbo::operator =(Fbo&& original)
{
	if (this != &original) {
		created = std::move(original.created);
		uniqueId = std::move(original.uniqueId);
		window = original.window;
		handle = original.handle;
		relativeViewport = std::move(original.relativeViewport);
		colorsRead = std::move(original.colorsRead);
		colorsDraw = std::move(original.colorsDraw);
		depthRead = std::move(original.depthRead);
		depthDraw = std::move(original.depthDraw);
		stencilRead = std::move(original.stencilRead);
		stencilDraw = std::move(original.stencilDraw);

		original.created = false;
		original.uniqueId.clear();
		original.window = nullptr;
		original.handle = 0;
		original.colorsRead.clear();
		original.colorsDraw.clear();
		original.depthRead = nullptr;
		original.depthDraw = nullptr;
		original.stencilRead = nullptr;
		original.stencilDraw = nullptr;
	}
	return *this;
}


Fbo::~Fbo()
{
	destroy();
}


void Fbo::create(IWindowOpengl* window)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == false, "Already created!", uniqueId.c_str());
	VERSO_ASSERT("verso-3d", window != nullptr);

	this->window = window;

	colorsRead.assign(MaxColorAttachments, nullptr);
	colorsDraw.assign(MaxColorAttachments, nullptr);

	// Allocate and assign Framebuffer Object to our handle
	glGenFramebuffers(1, &handle);
	GL_CHECK_FOR_ERRORS("", "");

	created = true;
}


void Fbo::destroy() VERSO_NOEXCEPT
{
	if (isCreated() == false) {
		return;
	}

	{
		TemporaryFboBind temp(handle);

		for (size_t i = 0; i < MaxColorAttachments; ++i) {
			destroyColorTexture(i, 0);
		}
		destroyDepthTexture(0);
		destroyStencilTexture(0);
	}

	glDeleteFramebuffers(1, &handle);
	GL_CHECK_FOR_ERRORS("", "");

	colorsRead.clear();
	colorsDraw.clear();
	depthRead = nullptr;
	depthDraw = nullptr;
	stencilRead = nullptr;
	stencilDraw = nullptr;
	handle = 0;
	window = nullptr;
	created = false;
}


bool Fbo::isCreated() const
{
	if (window != nullptr) {
		return true;
	}
	else {
		return false;
	}
}


void Fbo::createColorTexture(const TextureParameters& textureParameters, size_t colorTextureIndex, int mipmapLevel)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());
	VERSO_ASSERT_MSG("verso-3d", colorTextureIndex < MaxColorAttachments, "You can use maximum of 'MaxColorAttachments' color attachements.");
	VERSO_ASSERT_MSG("verso-3d", mipmapLevel == 0, "Mipmap level 0 only supported at the moment.");
	VERSO_ASSERT_MSG("verso-3d", colorsRead[colorTextureIndex] == nullptr, "You must destroy before creating new!");
	VERSO_ASSERT_MSG("verso-3d", colorsDraw[colorTextureIndex] == nullptr, "You must destroy before creating new!");

	// Allocate texture
	Texture* colorTexture = new Texture();
	// \TODO: find out why rgb and buffer true is needed
	colorTexture->createEmpty(*window, window->getRenderResolutioni(), textureParameters, TexturePixelFormat::Rgb, true);

	TemporaryFboBind temp(handle);
	glFramebufferTexture(GL_FRAMEBUFFER, static_cast<GLenum>(GL_COLOR_ATTACHMENT0 + colorTextureIndex), colorTexture->getHandle(), mipmapLevel);
	GL_CHECK_FOR_ERRORS("", "");

	// \TODO: save color[colorTextureIndex][mipmaplevel] = colorTexture?
	colorsRead[colorTextureIndex] = colorTexture;
	colorsDraw[colorTextureIndex] = colorTexture;
}


void Fbo::createColorReadTexture(const TextureParameters& textureParameters, size_t colorTextureIndex, int mipmapLevel)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());
	VERSO_ASSERT_MSG("verso-3d", colorTextureIndex < MaxColorAttachments, "You can use maximum of 'MaxColorAttachments' color attachements.");
	VERSO_ASSERT_MSG("verso-3d", mipmapLevel == 0, "Mipmap level 0 only supported at the moment.");
	VERSO_ASSERT_MSG("verso-3d", colorsRead[colorTextureIndex] == nullptr, "You must destroy before creating new!");

	// Allocate texture
	Texture* colorTexture = new Texture();
	colorTexture->createEmpty(*window, window->getRenderResolutioni(), textureParameters);

	TemporaryFboBind temp(handle);
	glFramebufferTexture(GL_READ_FRAMEBUFFER, static_cast<GLenum>(GL_COLOR_ATTACHMENT0 + colorTextureIndex), colorTexture->getHandle(), mipmapLevel);
	GL_CHECK_FOR_ERRORS("", "");

	// \TODO: save color[colorTextureIndex][mipmaplevel] = colorTexture?
	colorsRead[colorTextureIndex] = colorTexture;
}


void Fbo::createColorDrawTexture(const TextureParameters& textureParameters, size_t colorTextureIndex, int mipmapLevel)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());
	VERSO_ASSERT_MSG("verso-3d", colorTextureIndex < MaxColorAttachments, "You can use maximum of 'MaxColorAttachments' color attachements.");
	VERSO_ASSERT_MSG("verso-3d", mipmapLevel == 0, "Mipmap level 0 only supported at the moment.");
	VERSO_ASSERT_MSG("verso-3d", colorsDraw[colorTextureIndex] == nullptr, "You must destroy before creating new!");

	// Allocate texture
	Texture* colorTexture = new Texture();
	colorTexture->createEmpty(*window, window->getRenderResolutioni(), textureParameters);

	TemporaryFboBind temp(handle);
	glFramebufferTexture(GL_DRAW_FRAMEBUFFER, static_cast<GLenum>(GL_COLOR_ATTACHMENT0 + colorTextureIndex), colorTexture->getHandle(), mipmapLevel);
	GL_CHECK_FOR_ERRORS("", "");

	// \TODO: save color[colorTextureIndex][mipmaplevel] = colorTexture?
	colorsDraw[colorTextureIndex] = colorTexture;
}


void Fbo::createDepthTexture(const TextureParameters& textureParameters, int mipmapLevel)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());
	VERSO_ASSERT_MSG("verso-3d", mipmapLevel == 0, "Mipmap level 0 only supported at the moment.");
	VERSO_ASSERT_MSG("verso-3d", depthRead == nullptr, "You must destroy before creating new!");
	VERSO_ASSERT_MSG("verso-3d", depthDraw == nullptr, "You must destroy before creating new!");
	VERSO_ASSERT_MSG("verso-3d", textureParameters.pixelFormat == TexturePixelFormat::DepthComponent ||
					 textureParameters.pixelFormat == TexturePixelFormat::DepthStencil, "");

	// Allocate texture
	Texture* depthTexture = new Texture();
	depthTexture->createEmpty(*window, window->getRenderResolutioni(), textureParameters,
							  textureParameters.pixelFormat, false);

	TemporaryFboBind temp(handle);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTexture->getHandle(), mipmapLevel);
	GL_CHECK_FOR_ERRORS("", "");

	// \TODO: save depth[mipmaplevel] = depthTexture?
	depthRead = depthTexture;
	depthDraw = depthTexture;
}


void Fbo::createDepthReadTexture(const TextureParameters& textureParameters, int mipmapLevel)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());
	VERSO_ASSERT_MSG("verso-3d", mipmapLevel == 0, "Mipmap level 0 only supported at the moment.");
	VERSO_ASSERT_MSG("verso-3d", depthRead == nullptr, "You must destroy before creating new!");

	// Allocate texture
	Texture* depthTexture = new Texture();
	depthTexture->createEmpty(*window, window->getRenderResolutioni(), textureParameters);

	TemporaryFboBind temp(handle);
	glFramebufferTexture(GL_READ_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTexture->getHandle(), mipmapLevel);
	GL_CHECK_FOR_ERRORS("", "");

	// \TODO: save depth[mipmaplevel] = depthTexture?
	depthRead = depthTexture;
}


void Fbo::createDepthDrawTexture(const TextureParameters& textureParameters, int mipmapLevel)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());
	VERSO_ASSERT_MSG("verso-3d", mipmapLevel == 0, "Mipmap level 0 only supported at the moment.");
	VERSO_ASSERT_MSG("verso-3d", depthDraw == nullptr, "You must destroy before creating new!");

	// Allocate texture
	Texture* depthTexture = new Texture();
	depthTexture->createEmpty(*window, window->getRenderResolutioni(), textureParameters);

	TemporaryFboBind temp(handle);
	glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTexture->getHandle(), mipmapLevel);
	GL_CHECK_FOR_ERRORS("", "");

	// \TODO: save depth[mipmaplevel] = depthTexture?
	depthDraw = depthTexture;
}


void Fbo::createStencilTexture(const TextureParameters& textureParameters, int mipmapLevel)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());
	VERSO_ASSERT_MSG("verso-3d", mipmapLevel == 0, "Mipmap level 0 only supported at the moment.");
	VERSO_ASSERT_MSG("verso-3d", stencilRead == nullptr, "You must destroy before creating new!");
	VERSO_ASSERT_MSG("verso-3d", stencilDraw == nullptr, "You must destroy before creating new!");

	// Allocate texture
	Texture* stencilTexture = new Texture();
	stencilTexture->createEmpty(*window, window->getRenderResolutioni(), textureParameters);

	TemporaryFboBind temp(handle);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, stencilTexture->getHandle(), mipmapLevel);
	GL_CHECK_FOR_ERRORS("", "");

	// \TODO: save stencil[mipmaplevel] = stencilTexture?
	stencilRead = stencilTexture;
	stencilDraw = stencilTexture;
}


void Fbo::createStencilReadTexture(const TextureParameters& textureParameters, int mipmapLevel)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());
	VERSO_ASSERT_MSG("verso-3d", mipmapLevel == 0, "Mipmap level 0 only supported at the moment.");
	VERSO_ASSERT_MSG("verso-3d", stencilRead == nullptr, "You must destroy before creating new!");

	// Allocate texture
	Texture* stencilTexture = new Texture();
	stencilTexture->createEmpty(*window, window->getRenderResolutioni(), textureParameters);

	TemporaryFboBind temp(handle);
	glFramebufferTexture(GL_READ_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, stencilTexture->getHandle(), mipmapLevel);
	GL_CHECK_FOR_ERRORS("", "");

	// \TODO: save stencil[mipmaplevel] = stencilTexture?
	stencilRead = stencilTexture;
}


void Fbo::createStencilDrawTexture(const TextureParameters& textureParameters, int mipmapLevel)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());
	VERSO_ASSERT_MSG("verso-3d", mipmapLevel == 0, "Mipmap level 0 only supported at the moment.");
	VERSO_ASSERT_MSG("verso-3d", stencilDraw == nullptr, "You must destroy before creating new!");

	// Allocate texture
	Texture* stencilTexture = new Texture();
	stencilTexture->createEmpty(*window, window->getRenderResolutioni(), textureParameters);

	TemporaryFboBind temp(handle);
	glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, stencilTexture->getHandle(), mipmapLevel);
	GL_CHECK_FOR_ERRORS("", "");

	// \TODO: save stencil[mipmaplevel] = stencilTexture?
	stencilDraw = stencilTexture;
}


void Fbo::destroyColorTexture(size_t colorTextureIndex, int mipmapLevel)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());
	VERSO_ASSERT_MSG("verso-3d", colorTextureIndex < MaxColorAttachments, "You can use maximum of 'MaxColorAttachments' color attachements.");
	VERSO_ASSERT_MSG("verso-3d", mipmapLevel == 0, "Mipmap level 0 only supported at the moment.");
	if (colorsRead[colorTextureIndex] == nullptr && colorsDraw[colorTextureIndex] == nullptr) {
		return;
	}

	TemporaryFboBind temp(handle);
	glFramebufferTexture(GL_FRAMEBUFFER, static_cast<GLenum>(GL_COLOR_ATTACHMENT0 + colorTextureIndex), 0, mipmapLevel);
	GL_CHECK_FOR_ERRORS("", "");

	if (colorsRead[colorTextureIndex] != nullptr) {
		if (colorsDraw[colorTextureIndex] != nullptr && colorsRead[colorTextureIndex] != colorsDraw[colorTextureIndex]) {
			delete colorsDraw[colorTextureIndex];
		}
		delete colorsRead[colorTextureIndex];
	}
	else if (colorsDraw[colorTextureIndex] != nullptr) {
		delete colorsDraw[colorTextureIndex];
	}

	// \TODO: remove color[colorTextureIndex][mipmaplevel]?
	colorsRead[colorTextureIndex] = nullptr;
	colorsDraw[colorTextureIndex] = nullptr;
}


void Fbo::destroyColorReadTexture(size_t colorTextureIndex, int mipmapLevel)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());
	VERSO_ASSERT_MSG("verso-3d", colorTextureIndex < MaxColorAttachments, "You can use maximum of 'MaxColorAttachments' color attachements.");
	VERSO_ASSERT_MSG("verso-3d", mipmapLevel == 0, "Mipmap level 0 only supported at the moment.");
	if (colorsRead[colorTextureIndex] == nullptr) {
		return;
	}

	TemporaryFboBind temp(handle);
	glFramebufferTexture(GL_READ_FRAMEBUFFER, static_cast<GLenum>(GL_COLOR_ATTACHMENT0 + colorTextureIndex), 0, mipmapLevel);
	GL_CHECK_FOR_ERRORS("", "");

	if (colorsRead[colorTextureIndex] != nullptr && colorsRead[colorTextureIndex] != colorsDraw[colorTextureIndex]) {
		delete colorsRead[colorTextureIndex];
	}

	// \TODO: remove color[colorTextureIndex][mipmaplevel]?
	colorsRead[colorTextureIndex] = nullptr;
}


void Fbo::destroyColorDrawTexture(size_t colorTextureIndex, int mipmapLevel)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());
	VERSO_ASSERT_MSG("verso-3d", colorTextureIndex < MaxColorAttachments, "You can use maximum of 'MaxColorAttachments' color attachements.");
	VERSO_ASSERT_MSG("verso-3d", mipmapLevel == 0, "Mipmap level 0 only supported at the moment.");
	if (colorsDraw[colorTextureIndex] == nullptr) {
		return;
	}

	TemporaryFboBind temp(handle);
	glFramebufferTexture(GL_DRAW_FRAMEBUFFER, static_cast<GLenum>(GL_COLOR_ATTACHMENT0 + colorTextureIndex), 0, mipmapLevel);
	GL_CHECK_FOR_ERRORS("", "");

	if (colorsDraw[colorTextureIndex] != nullptr && colorsDraw[colorTextureIndex] != colorsRead[colorTextureIndex]) {
		delete colorsDraw[colorTextureIndex];
	}

	// \TODO: remove color[colorTextureIndex][mipmaplevel]?
	colorsDraw[colorTextureIndex] = nullptr;
}


void Fbo::destroyDepthTexture(int mipmapLevel)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());
	VERSO_ASSERT_MSG("verso-3d", mipmapLevel == 0, "Mipmap level 0 only supported at the moment.");
	if (depthRead == nullptr && depthDraw == nullptr) {
		return;
	}

	TemporaryFboBind temp(handle);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, 0, mipmapLevel);
	GL_CHECK_FOR_ERRORS("", "");

	if (depthRead != nullptr) {
		if (depthDraw != nullptr && depthRead != depthDraw) {
			delete depthDraw;
		}
		delete depthRead;
	}
	else if (depthDraw != nullptr) {
		delete depthDraw;
	}

	// \TODO: remove depth[mipmaplevel]?
	depthRead = nullptr;
	depthDraw = nullptr;
}


void Fbo::destroyDepthReadTexture(int mipmapLevel)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());
	VERSO_ASSERT_MSG("verso-3d", mipmapLevel == 0, "Mipmap level 0 only supported at the moment.");
	if (depthRead == nullptr) {
		return;
	}

	TemporaryFboBind temp(handle);
	glFramebufferTexture(GL_READ_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, 0, mipmapLevel);
	GL_CHECK_FOR_ERRORS("", "");

	if (depthRead != nullptr && depthRead != depthDraw) {
		delete depthRead;
	}

	// \TODO: remove depth[mipmaplevel]?
	depthRead = nullptr;
}


void Fbo::destroyDepthDrawTexture(int mipmapLevel)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());
	VERSO_ASSERT_MSG("verso-3d", mipmapLevel == 0, "Mipmap level 0 only supported at the moment.");
	if (depthDraw == nullptr) {
		return;
	}

	TemporaryFboBind temp(handle);
	glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, 0, mipmapLevel);
	GL_CHECK_FOR_ERRORS("", "");

	if (depthDraw != nullptr && depthDraw != depthRead) {
		delete depthDraw;
	}

	// \TODO: remove depth[mipmaplevel]?
	depthDraw = nullptr;
}


void Fbo::destroyStencilTexture(int mipmapLevel)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());
	VERSO_ASSERT_MSG("verso-3d", mipmapLevel == 0, "Mipmap level 0 only supported at the moment.");
	if (stencilRead == nullptr && stencilDraw == nullptr) {
		return;
	}

	TemporaryFboBind temp(handle);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, 0, mipmapLevel);
	GL_CHECK_FOR_ERRORS("", "");

	if (stencilRead != nullptr) {
		if (stencilDraw != nullptr && stencilRead != stencilDraw) {
			delete stencilDraw;
		}
		delete stencilRead;
	}
	else if (stencilDraw != nullptr) {
		delete stencilDraw;
	}

	// \TODO: remove stencil[mipmaplevel]?
	stencilRead = nullptr;
	stencilDraw = nullptr;
}


void Fbo::destroyStencilReadTexture(int mipmapLevel)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());
	VERSO_ASSERT_MSG("verso-3d", mipmapLevel == 0, "Mipmap level 0 only supported at the moment.");
	if (stencilRead == nullptr) {
		return;
	}

	TemporaryFboBind temp(handle);
	glFramebufferTexture(GL_READ_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, 0, mipmapLevel);
	GL_CHECK_FOR_ERRORS("", "");

	if (stencilRead != nullptr && stencilRead != stencilDraw) {
		delete stencilRead;
	}

	// \TODO: remove stencil[mipmaplevel]?
	stencilRead = nullptr;
}


void Fbo::destroyStencilDrawTexture(int mipmapLevel)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());
	VERSO_ASSERT_MSG("verso-3d", mipmapLevel == 0, "Mipmap level 0 only supported at the moment.");
	if (stencilDraw == nullptr) {
		return;
	}

	TemporaryFboBind temp(handle);
	glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, 0, mipmapLevel);
	GL_CHECK_FOR_ERRORS("", "");

	if (stencilDraw != nullptr && stencilDraw != stencilRead) {
		delete stencilDraw;
	}

	// \TODO: remove stencil[mipmaplevel]?
	stencilDraw = nullptr;
}


bool Fbo::isComplete()
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());

	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	GL_CHECK_FOR_ERRORS("", "");
	if (status == GL_FRAMEBUFFER_COMPLETE) {
		return true;
	}
	else {
		UString error("OpenGL FBO error: ");
		if (status == GL_FRAMEBUFFER_UNDEFINED) {
			error += "GL_FRAMEBUFFER_UNDEFINED\n";
			error += "  The specified framebuffer is the default read or draw framebuffer, but the default framebuffer does not exist.";
		}
		else if (status == GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT) {
			error += "GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT\n";
			error += "  Any of the framebuffer attachment points are framebuffer incomplete.";
		}
		else if (status == GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT) {
			error += "GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT\n";
			error += "  The framebuffer does not have at least one image attached to it.";
		}
		else if (status == GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER) {
			error += "GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER\n";
			error += "  The value of GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE is GL_NONE for any color attachment point(s) named by GL_DRAW_BUFFERi.";
		}
		else if (status == GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER) {
			error += "GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER\n";
			error += "  GL_READ_BUFFER is not GL_NONE and the value of GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE is GL_NONE for the color attachment point named by GL_READ_BUFFER.";
		}
		else if (status == GL_FRAMEBUFFER_UNSUPPORTED) {
			error += "GL_FRAMEBUFFER_UNSUPPORTED\n";
			error += "  The combination of internal formats of the attached images violates an implementation-dependent set of restrictions.";
		}
		else if (status == GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE) {
			error += "GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE\n";
			error += "  The value of GL_RENDERBUFFER_SAMPLES is not the same for all attached renderbuffers; if the value of GL_TEXTURE_SAMPLES is the not same for all attached textures; or, if the attached images are a mix of renderbuffers and textures, the value of GL_RENDERBUFFER_SAMPLES does not match the value of GL_TEXTURE_SAMPLES.\n";
			error += "  OR\n";
			error += "  The value of GL_TEXTURE_FIXED_SAMPLE_LOCATIONS is not the same for all attached textures; or, if the attached images are a mix of renderbuffers and textures, the value of GL_TEXTURE_FIXED_SAMPLE_LOCATIONS is not GL_TRUE for all attached textures.";
		}
		else if (status == GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS) {
			error += "GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS\n";
			error += "  Any framebuffer attachment is layered, and any populated attachment is not layered, or if all populated color attachments are not from textures of the same target.";
		}
		else {
			error += "Unknown error ";
			error.append2(status);
		}
		VERSO_LOG_ERR("verso-3d", error);
		return false;
	}
}


Texture* Fbo::getColorReadTexture(size_t colorTextureIndex, int mipmapLevel)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());
	VERSO_ASSERT_MSG("verso-3d", mipmapLevel == 0, "Mipmap level 0 only supported at the moment.");
	VERSO_ASSERT_MSG("verso-3d", colorTextureIndex < colorsRead.size(), "colorTextureIndex too large.");
	return colorsRead[colorTextureIndex];
}


Texture* Fbo::getColorDrawTexture(size_t colorTextureIndex, int mipmapLevel)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());
	VERSO_ASSERT_MSG("verso-3d", mipmapLevel == 0, "Mipmap level 0 only supported at the moment.");
	VERSO_ASSERT_MSG("verso-3d", colorTextureIndex < colorsRead.size(), "colorTextureIndex too large.");
	return colorsDraw[colorTextureIndex];
}


Texture* Fbo::getDepthReadTexture(int mipmapLevel)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());
	VERSO_ASSERT_MSG("verso-3d", mipmapLevel == 0, "Mipmap level 0 only supported at the moment.");
	return depthRead;
}


Texture* Fbo::getDepthDrawTexture(int mipmapLevel)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());
	VERSO_ASSERT_MSG("verso-3d", mipmapLevel == 0, "Mipmap level 0 only supported at the moment.");
	return depthDraw;
}


Texture* Fbo::getStencilReadTexture(int mipmapLevel)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());
	VERSO_ASSERT_MSG("verso-3d", mipmapLevel == 0, "Mipmap level 0 only supported at the moment.");
	return stencilRead;
}


Texture* Fbo::getStencilDrawTexture(int mipmapLevel)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());
	VERSO_ASSERT_MSG("verso-3d", mipmapLevel == 0, "Mipmap level 0 only supported at the moment.");
	return stencilDraw;
}


void Fbo::bind()
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());

	if (lastBoundDrawHandle != handle || lastBoundReadHandle != handle) {
		glBindFramebuffer(GL_FRAMEBUFFER, handle);
		GL_CHECK_FOR_ERRORS("", "");

		window->bindReadFbo(this);
		lastBoundReadHandle = handle;

		window->bindDrawFbo(this);
		lastBoundDrawHandle = handle;
	}
}


void Fbo::bindRead()
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());

	if (lastBoundReadHandle != handle) {
		glBindFramebuffer(GL_READ_FRAMEBUFFER, handle);
		GL_CHECK_FOR_ERRORS("", "");

		window->bindReadFbo(this);
		lastBoundReadHandle = handle;
	}
}


void Fbo::bindDraw()
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Fbo not created!", uniqueId.c_str());

	if (lastBoundDrawHandle != handle) {
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, handle);
		GL_CHECK_FOR_ERRORS("", "");

		window->bindDrawFbo(this);
		lastBoundDrawHandle = handle;
	}
}


void Fbo::unbind()
{
	if (lastBoundDrawHandle != 0 || lastBoundReadHandle != 0) {
		if (isCreated() == true) {
			window->unbindReadFbo(this);
			window->unbindDrawFbo(this);
		}
		lastBoundReadHandle = 0;
		lastBoundDrawHandle = 0;

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		GL_CHECK_FOR_ERRORS("", "");
	}
}


void Fbo::unbindRead()
{
	if (lastBoundReadHandle != 0) {
		if (isCreated() == true) {
			window->unbindReadFbo(this);
		}
		lastBoundReadHandle = 0;

		glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
		GL_CHECK_FOR_ERRORS("", "");
	}
}


void Fbo::unbindDraw()
{
	if (lastBoundDrawHandle != 0) {
		if (isCreated() == true) {
			window->unbindDrawFbo(this);
		}
		lastBoundDrawHandle = 0;

		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
		GL_CHECK_FOR_ERRORS("", "");
	}
}


Rectf Fbo::getRelativeViewport() const
{
	return relativeViewport;
}


void Fbo::setRelativeViewport(const Rectf& relativeViewport)
{
	this->relativeViewport = relativeViewport;
}


void Fbo::resetRelativeViewport()
{
	setRelativeViewport(Rectf(0.0f, 0.0f, 1.0f, 1.0f));
}


void Fbo::applyViewport() const
{
	Texture* buffer = nullptr;
	if (colorsRead[0] != nullptr) {
		buffer = colorsRead[0];
	}
	else if (colorsDraw[0] != nullptr)  {
		buffer = colorsDraw[0];
	}
	else {
		// \TODO: maybe add assert for both color buffers being null
		//VERSO_ASSERT_MSG("verso-3d", colorsRead[0] != nullptr && colorsDraw[0] != nullptr, "There's no color attachement for index 0!");
		return;
	}
	//isbound() ??;

	glViewport(static_cast<GLint>(relativeViewport.pos.x * buffer->getResolutionf().x),
			   static_cast<GLint>(relativeViewport.pos.y * buffer->getResolutionf().y),
			   static_cast<GLsizei>(relativeViewport.size.x * buffer->getResolutionf().x),
			   static_cast<GLsizei>(relativeViewport.size.y * buffer->getResolutionf().y));
	GL_CHECK_FOR_ERRORS("", "");
}


void Fbo::onResize()
{
	VERSO_ASSERT_MSG("verso-3d", isCreated() == true, "Not yet created!");

	{
		TemporaryFboBind temp(0, handle);

		glDeleteFramebuffers(1, &handle);
		GL_CHECK_FOR_ERRORS("", "");
	}

	glGenFramebuffers(1, &handle);
	GL_CHECK_FOR_ERRORS("", "");

	{
		int mipmapLevel = 0;
		TemporaryFboBind temp(handle);

		// Color attachment
		for (size_t i = 0; i < colorsRead.size(); ++i) {
			if (colorsRead[i] != nullptr && colorsRead[i] == colorsDraw[i]) {
				colorsRead[i]->resize(window->getRenderResolutioni());
				colorsRead[i]->bind();
				glFramebufferTexture(GL_FRAMEBUFFER, static_cast<GLenum>(GL_COLOR_ATTACHMENT0 + i), colorsRead[i]->getHandle(), mipmapLevel);
				GL_CHECK_FOR_ERRORS("", "");
			}
			else {
				if (colorsRead[i] != nullptr) {
					colorsRead[i]->resize(window->getRenderResolutioni());
					colorsRead[i]->bind();
					glFramebufferTexture(GL_READ_FRAMEBUFFER, static_cast<GLenum>(GL_COLOR_ATTACHMENT0 + i), colorsRead[i]->getHandle(), mipmapLevel);
					GL_CHECK_FOR_ERRORS("", "");

				}
				if (colorsDraw[i] != nullptr) {
					colorsDraw[i]->resize(window->getRenderResolutioni());
					colorsDraw[i]->bind();
					glFramebufferTexture(GL_DRAW_FRAMEBUFFER, static_cast<GLenum>(GL_COLOR_ATTACHMENT0 + i), colorsDraw[i]->getHandle(), mipmapLevel);
					GL_CHECK_FOR_ERRORS("", "");
				}
			}
		}

		// Depth attachment
		if (depthRead != nullptr && depthRead == depthDraw) {
			depthRead->resize(window->getRenderResolutioni());
			depthRead->bind();
			glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthRead->getHandle(), mipmapLevel);
			GL_CHECK_FOR_ERRORS("", "");
		}
		else {
			if (depthRead != nullptr) {
				depthRead->resize(window->getRenderResolutioni());
				depthRead->bind();
				glFramebufferTexture(GL_READ_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthRead->getHandle(), mipmapLevel);
				GL_CHECK_FOR_ERRORS("", "");
			}
			if (depthDraw != nullptr) {
				depthDraw->resize(window->getRenderResolutioni());
				depthDraw->bind();
				glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthDraw->getHandle(), mipmapLevel);
				GL_CHECK_FOR_ERRORS("", "");
			}
		}

		// Stencil attachment
		if (stencilRead != nullptr && stencilRead == stencilDraw) {
			stencilRead->resize(window->getRenderResolutioni());
			stencilRead->bind();
			glFramebufferTexture(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, stencilRead->getHandle(), mipmapLevel);
			GL_CHECK_FOR_ERRORS("", "");
		}
		else {
			if (stencilRead != nullptr) {
				stencilRead->resize(window->getRenderResolutioni());
				stencilRead->bind();
				glFramebufferTexture(GL_READ_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, stencilRead->getHandle(), mipmapLevel);
				GL_CHECK_FOR_ERRORS("", "");
			}
			if (stencilDraw != nullptr) {
				stencilDraw->resize(window->getRenderResolutioni());
				stencilDraw->bind();
				glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, stencilDraw->getHandle(), mipmapLevel);
				GL_CHECK_FOR_ERRORS("", "");
			}
		}

		isComplete();
	}
}


UString Fbo::getUniqueId() const
{
	return uniqueId;
}


UString Fbo::toString(const UString& newLinePadding) const
{
	UString str("{ ");
	UString newLinePadding2 = newLinePadding + "  ";

	if (isCreated() == false) {
		str += "uniqueId=";
		str += uniqueId;
		str += ", isCreated() == false";
	}
	else {
		str += "handle=";
		str.append2(handle);

		str += ", relativeViewport=";
		str += relativeViewport.toString();

		if (colorsRead.size() > 0) {
			str += ", colorsRead=[ ";
			bool first = true;
			for (auto& tex : colorsRead) {
				if (tex != nullptr) {
					if (first == true) {
						first = false;
					}
					else {
						str += ", ";
					}
					str += tex->toString(newLinePadding2);
				}
			}
			str += " ]";
		}

		if (colorsDraw.size() > 0) {
			str += ", colorsDraw=[ ";
			bool first = true;
			for (auto& tex : colorsDraw) {
				if (tex != nullptr) {
					if (first == true) {
						first = false;
					}
					else {
						str += ", ";
					}
					str += tex->toString(newLinePadding2);
				}
			}
			str += " ]";
		}

		if (depthRead != nullptr) {
			str += ", depthRead=";
			str += depthRead->toString(newLinePadding2);
		}

		if (depthDraw != nullptr) {
			str += ", depthDraw=";
			str += depthDraw->toString(newLinePadding2);
		}

		if (stencilRead != nullptr) {
			str += ", stencilRead=";
			str += stencilRead->toString(newLinePadding2);
		}

		if (stencilDraw != nullptr) {
			str += ", stencilDraw=";
			str += stencilDraw->toString(newLinePadding2);
		}
	}
	return str;
}


UString Fbo::toStringDebug(const UString& newLinePadding) const
{
	UString str("Fbo(");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso

