#include <Verso/Render/Texture.hpp>
#include <Verso/System/File.hpp>
#include <Verso/Display/IWindowOpengl.hpp>
#include <Verso/Image/Image.hpp>

// Define stb_image implementation here
//#define STB_IMAGE_IMPLEMENTATION
//#define STBI_FAILURE_USERMSG
#include <stb/stb_image.h>

// Define stb_image_write implementation here
//#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb/stb_image_write.h>

// \TODO: implement api for STB image in verso-base

namespace Verso {


Texture::Texture() :
	window(nullptr),
	created(false),
	handle(0),
	resolution(0, 0),
	parameters(),
	pixelData(nullptr),
	pixelDataFormat(TexturePixelFormat::Unset),
	sourceFileName(""),
	aspectRatioTarget()
{
}


Texture::Texture(Texture&& original) noexcept :
	window(std::move(original.window)),
	created(std::move(original.created)),
	handle(std::move(original.handle)),
	resolution(std::move(original.resolution)),
	parameters(std::move(original.parameters)),
	pixelData(std::move(original.pixelData)),
	pixelDataFormat(std::move(original.pixelDataFormat)),
	sourceFileName(std::move(original.sourceFileName)),
	aspectRatioTarget(std::move(original.aspectRatioTarget))
{
	original.window = nullptr;
	original.created = false;
	original.handle = 0;
	original.pixelData = nullptr;
}


Texture& Texture::operator =(Texture&& original) noexcept
{
	if (this != &original) {
		window = std::move(original.window);
		created = std::move(original.created);
		handle = std::move(original.handle);
		resolution = std::move(original.resolution);
		parameters = std::move(original.parameters);
		pixelData = std::move(original.pixelData);
		pixelDataFormat = std::move(original.pixelDataFormat);
		sourceFileName = std::move(original.sourceFileName);
		aspectRatioTarget = std::move(original.aspectRatioTarget);

		original.window = nullptr;
		original.created = false;
		original.handle = 0;
		original.pixelData = nullptr;
	}
	return *this;
}


Texture::~Texture()
{
	destroy();
}


bool Texture::createEmpty(IWindowOpengl& window,
						  const Vector2i& resolution,
						  const TextureParameters& parameters,
						  const TexturePixelFormat& cpuRamPixelFormat,
						  bool keepCpuRamCopy,
						  const AspectRatio& aspectRatioTarget)
{
	VERSO_ASSERT_MSG("verso-3d", isCreated() == false, "Already created!");
	VERSO_ASSERT_MSG_AND_RESOURCES(
				"verso-3d",
				keepCpuRamCopy == false ||
				(keepCpuRamCopy == true &&
				 (cpuRamPixelFormat == TexturePixelFormat::Rgb ||
				  cpuRamPixelFormat == TexturePixelFormat::Rgba ||
				  cpuRamPixelFormat == TexturePixelFormat::DepthComponent ||
				  cpuRamPixelFormat == TexturePixelFormat::DepthStencil)),
				"Unsupported pixelFormat!", texturePixelFormatToString(cpuRamPixelFormat).c_str());

	reserveGpuHandleAndSetup(window, resolution, parameters, aspectRatioTarget);

	if (keepCpuRamCopy == true) {
		size_t pixelDataSize = allocatePixelData(cpuRamPixelFormat);
		pixelDataFormat = cpuRamPixelFormat;
		memset(pixelData, 0, pixelDataSize);
		updateGpuTexture(pixelData, pixelDataFormat, this->parameters.pixelFormat);
	}
	else {
		updateGpuTexture(nullptr, TexturePixelFormat::Unset, this->parameters.pixelFormat);
	}

	return true;
}


bool Texture::createCopyBuffer(IWindowOpengl& window,
							   const Vector2i& resolution,
							   const TextureParameters& parameters,
							   std::uint8_t* cpuRamPixelData,
							   const TexturePixelFormat& cpuRamPixelFormat,
							   bool keepCpuRamCopy,
							   const AspectRatio& aspectRatioTarget)
{
	VERSO_ASSERT_MSG("verso-3d", isCreated() == false, "Already created!");
	VERSO_ASSERT_MSG_AND_RESOURCES(
				"verso-3d",
				keepCpuRamCopy == true &&
				(cpuRamPixelFormat == TexturePixelFormat::Rgb ||
				cpuRamPixelFormat == TexturePixelFormat::Rgba),
				"Unsupported pixelFormat!", texturePixelFormatToString(cpuRamPixelFormat).c_str());

	reserveGpuHandleAndSetup(window, resolution, parameters, aspectRatioTarget);

	if (keepCpuRamCopy == true) {
		size_t pixelDataSize = allocatePixelData(cpuRamPixelFormat);
		memcpy(pixelData, cpuRamPixelData, pixelDataSize);
		updateGpuTexture(pixelData, pixelDataFormat, this->parameters.pixelFormat);
	}
	else {
		updateGpuTexture(cpuRamPixelData, cpuRamPixelFormat, this->parameters.pixelFormat);
	}

	return true;
}


bool Texture::createFromFile(IWindowOpengl& window,
							 const UString& fileName,
							 const TextureParameters& parameters,
							 const TexturePixelFormat& cpuRamPixelFormat,
							 bool keepCpuRamCopy,
							 const AspectRatio& aspectRatioTarget)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == false, "Already created!", sourceFileName.c_str());

	if (!File::exists(fileName)) {
		VERSO_FILENOTFOUND("verso-3d", "Cannot load texture", fileName.c_str());
	}

	UString potentialError("Unsupported pixelFormat!");
	potentialError.append(texturePixelFormatToString(cpuRamPixelFormat).c_str());
	VERSO_ASSERT_MSG_AND_RESOURCES(
				"verso-3d",
				cpuRamPixelFormat == TexturePixelFormat::Unset ||
				cpuRamPixelFormat == TexturePixelFormat::Rgb ||
				cpuRamPixelFormat == TexturePixelFormat::Rgba,
				potentialError.c_str(), fileName.c_str());

	int channelsRequested = static_cast<int>(texturePixelFormatToChannelsCount(parameters.pixelFormat));
	UString tmp; tmp.append2(channelsRequested);
	int tempImageComponentsPerPixel = 0;
	unsigned char* tempImagePixelData = stbi_load(fileName.c_str(), &resolution.x, &resolution.y, &tempImageComponentsPerPixel, channelsRequested);
	if (tempImagePixelData == nullptr) {
		UString error("Failed to load file. Reason: ");
		error += stbi_failure_reason();
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), fileName.c_str());
	}
	if (channelsRequested != 0) {
		tempImageComponentsPerPixel = channelsRequested;
	}

	TexturePixelFormat tempImagePixelFormat;
	if (tempImageComponentsPerPixel == 4) {
		tempImagePixelFormat = TexturePixelFormat::Rgba;
	}
	else if (tempImageComponentsPerPixel == 3) {
		tempImagePixelFormat = TexturePixelFormat::Rgb;
	}
	else {
		stbi_image_free(tempImagePixelData);

		// \TODO: add support for grayscale 8 bpp textures!
		UString error("Image was not 24 or 32 bpp truecolor. componentsPerPixel=");
		error.append2(tempImageComponentsPerPixel);
		error += ", TextureParameters=\"" + parameters.toString() + "\"";
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), fileName.c_str());
	}

	reserveGpuHandleAndSetup(window, resolution, parameters, aspectRatioTarget);

	// Set texture pixel format by image format if unset
	if (this->parameters.pixelFormat == TexturePixelFormat::Unset) {
		this->parameters.pixelFormat = tempImagePixelFormat;
	}

	int pixelDataComponentsPerPixel = static_cast<int>(texturePixelFormatToChannelsCount(this->parameters.pixelFormat));

	if (keepCpuRamCopy == true) {
		size_t pixelDataSize = allocatePixelData(cpuRamPixelFormat);
		size_t tempImagePixelDataSize = resolution.x * resolution.y * tempImageComponentsPerPixel;

		if (pixelDataComponentsPerPixel == tempImageComponentsPerPixel) {
			VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", pixelDataSize == tempImagePixelDataSize, "", fileName.c_str());
			memcpy(pixelData, tempImagePixelData, pixelDataSize);
		}
//		else if (pixelDataComponentsPerPixel == 3) {
//			for (int i = 0; i < size.x * size.y; ++i) {
//				size_t targetIndex = i * pixelDataComponentsPerPixel;
//				size_t sourceIndex = i * tempImageComponentsPerPixel;
//				if (tempImagePixelData[sourceIndex + 2] != 0) {
//					pixelData[targetIndex] = tempImagePixelData[sourceIndex];
//					pixelData[targetIndex + 1] = tempImagePixelData[sourceIndex + 1];
//					pixelData[targetIndex + 2] = tempImagePixelData[sourceIndex + 2];
//				}
//				else {
//					pixelData[targetIndex] = 0;
//					pixelData[targetIndex + 1] = 0;
//					pixelData[targetIndex + 2] = 0;
//				}
//			}
//		}
//		else if (pixelDataComponentsPerPixel == 4) {
//			for (int i = 0; i < size.x * size.y; ++i) {
//				size_t targetIndex = i * pixelDataComponentsPerPixel;
//				size_t sourceIndex = i * tempImageComponentsPerPixel;
//				pixelData[targetIndex] = tempImagePixelData[sourceIndex];
//				pixelData[targetIndex + 1] = tempImagePixelData[sourceIndex + 1];
//				pixelData[targetIndex + 2] = tempImagePixelData[sourceIndex + 2];
//				pixelData[targetIndex + 2] = 255;
//			}
//		}
		else {
			VERSO_FAIL("verso-3d");
		}

		updateGpuTexture(pixelData, pixelDataFormat, this->parameters.pixelFormat);
	}
	else {
		if (pixelDataComponentsPerPixel == tempImageComponentsPerPixel) {
			updateGpuTexture(tempImagePixelData, tempImagePixelFormat, this->parameters.pixelFormat);
		}
//		else if (pixelDataComponentsPerPixel == 3) {
//			VERSO_FAIL("verso-3d");
//		}
//		else if (pixelDataComponentsPerPixel == 4) {
//			updateGpuTexture(tempImagePixelData, tempImagePixelFormat, this->parameters.pixelFormat);
//			/*for (int i = 0; i < size.x * size.y; ++i) {
//				size_t targetIndex = i * pixelDataComponentsPerPixel;
//				size_t sourceIndex = i * tempImageComponentsPerPixel;
//				pixelData[targetIndex] = tempImagePixelData[sourceIndex];
//				pixelData[targetIndex + 1] = tempImagePixelData[sourceIndex + 1];
//				pixelData[targetIndex + 2] = tempImagePixelData[sourceIndex + 2];
//				pixelData[targetIndex + 2] = 255;
//			}*/
//		}
		else {
			VERSO_FAIL("verso-3d");
		}
	}

	stbi_image_free(tempImagePixelData);

	sourceFileName = fileName;
	return true;
}

/////
// private, remove
/////

void Texture::reserveGpuHandleAndSetup(IWindowOpengl& window, const Vector2i& resolution,
									   const TextureParameters& parameters,
									   const AspectRatio& aspectRatioTarget)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == false, "Already created!", sourceFileName.c_str());
	this->window = &window;
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", this->window != nullptr, "window pointer was nullptr!", sourceFileName.c_str());

	Vector2i maxResolution = window.getMaxTextureResolution();
	if (resolution > maxResolution) {
		UString error("In function: void Verso::Texture_::setupAndReserveTextureHandle(const Vector2i& resolution="+resolution.toString()+", const TextureParameters& parameters="+parameters.toString()+")\n\n");
		error += "Message: Texture resolution is larger than maximum supported texture resolution(="+maxResolution.toString()+")!";
		std::cout << error << std::endl;
		throw std::runtime_error(error.c_str());
	}

	window.setActiveTextureUnit(0);

	glGenTextures(1, &handle);
	GL_CHECK_FOR_ERRORS("", sourceFileName);

	this->resolution = resolution;
	this->parameters = parameters;

	if (aspectRatioTarget.value == 0.0f) {
		this->aspectRatioTarget = AspectRatio(resolution, ""); // \TODO: fix aspect ratio textual
	}
	else {
		this->aspectRatioTarget = aspectRatioTarget;
	}

	this->created = true;

	bind();

	setMinFilter(this->parameters.minFilter);
	setMagFilter(this->parameters.magFilter);
	setWrapStyle(this->parameters.wrapStyleS, this->parameters.wrapStyleT);
}


size_t Texture::allocatePixelData(const TexturePixelFormat& cpuRamPixelFormat)
{
	VERSO_ASSERT("verso-3d", pixelData == nullptr);
	size_t components = texturePixelFormatToChannelsCount(cpuRamPixelFormat);
	VERSO_ASSERT("verso-3d", components > 0);
	size_t pixelDataSize = resolution.x * resolution.y * components;
	pixelData = new std::uint8_t[pixelDataSize];
	pixelDataFormat = cpuRamPixelFormat;
	return pixelDataSize;
}


void Texture::updateGpuTexture(const std::uint8_t* bufferPixelData,
							   const TexturePixelFormat& bufferTexturePixelFormat,
							   const TexturePixelFormat& internalTexturePixelFormat) const
{
	UString error("Unsupported pixelFormat!");
	error.append(texturePixelFormatToString(bufferTexturePixelFormat).c_str());
	VERSO_ASSERT_MSG_AND_RESOURCES(
				"verso-3d",
				(bufferPixelData == nullptr &&
				 bufferTexturePixelFormat == TexturePixelFormat::Unset) ||
				(bufferPixelData != nullptr &&
				 (bufferTexturePixelFormat == TexturePixelFormat::Rgb ||
				  bufferTexturePixelFormat == TexturePixelFormat::Rgba ||
				  bufferTexturePixelFormat == TexturePixelFormat::DepthComponent ||
				  bufferTexturePixelFormat == TexturePixelFormat::DepthStencil)),
				error.c_str(), sourceFileName.c_str());

	if (bufferTexturePixelFormat == TexturePixelFormat::Rgba) {
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		GL_CHECK_FOR_ERRORS("", sourceFileName);
	}
	else if (bufferTexturePixelFormat == TexturePixelFormat::Rgb) {
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		GL_CHECK_FOR_ERRORS("", sourceFileName);
	}
	else if (internalTexturePixelFormat == TexturePixelFormat::DepthComponent) {
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		GL_CHECK_FOR_ERRORS("", sourceFileName);
	}

	GLenum bufferTexturePixelFormatOpengl = 0;
	if (bufferTexturePixelFormat != TexturePixelFormat::Unset) {
		bufferTexturePixelFormatOpengl = texturePixelFormatToGlenum(bufferTexturePixelFormat);
	}

	GLenum bufferDataType = 0;
	if (this->parameters.pixelFormat == TexturePixelFormat::DepthComponent) {
		bufferDataType = GL_UNSIGNED_INT;
		bufferTexturePixelFormatOpengl = texturePixelFormatToGlenum(TexturePixelFormat::DepthComponent);
	}
	else {
		bufferDataType = GL_UNSIGNED_BYTE;
	}

	int mipmapLevel = 0;
	glTexImage2D(GL_TEXTURE_2D, mipmapLevel,
				 texturePixelFormatToGlenum(internalTexturePixelFormat),
				 resolution.x, resolution.y, 0,
				 bufferTexturePixelFormatOpengl,
				 bufferDataType, bufferPixelData);
	GL_CHECK_FOR_ERRORS("", sourceFileName);

	if (minFilterNeedMipmaps(parameters.minFilter) == true) {
		glGenerateMipmap(GL_TEXTURE_2D);
		GL_CHECK_FOR_ERRORS("", sourceFileName);
	}
}


/////
// public, remove
/////
void Texture::destroy() VERSO_NOEXCEPT
{
	if (isCreated()) {
		unbind();

		glDeleteTextures(1, &handle);
		GL_CHECK_FOR_ERRORS("", sourceFileName);
		handle = 0;

		if (pixelData != nullptr) {
			delete[] pixelData;
			pixelData = nullptr;
		}

		window = nullptr;
		created = false;
	}
}


void Texture::bind()
{
	VERSO_ASSERT_MSG("verso-3d", isCreated() == true, "Not yet created!");
	window->bindTextureHandle(handle);
}


void Texture::unbind()
{
	VERSO_ASSERT_MSG("verso-3d", isCreated() == true, "Not yet created!");
	window->bindTextureHandle(0);
}


void Texture::resize(const Vector2i& newResolution)
{
	VERSO_ASSERT_MSG("verso-3d", isCreated() == true, "Not yet created!");
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", this->window != nullptr, "window pointer was nullptr!", sourceFileName.c_str());

	Vector2i maxResolution = window->getMaxTextureResolution();
	if (resolution > maxResolution) {
		UString error("In function: void Verso::Texture_::setupAndReserveTextureHandle(const Vector2i& resolution="+resolution.toString()+", const TextureParameters& parameters="+parameters.toString()+")\n\n");
		error += "Message: Texture resolution is larger than maximum supported texture resolution(="+maxResolution.toString()+")!";
		std::cout << error << std::endl;
		throw std::runtime_error(error.c_str());
	}

	IWindowOpengl* prevWindow = window;
	bool prevKeepCpuRamCopy = false;
	if (pixelData != nullptr) {
		prevKeepCpuRamCopy = true;
	}
	TexturePixelFormat prevCpuRamPixelFormat = pixelDataFormat;
	const TextureParameters& prevParameters = parameters;

	destroy();

	createEmpty(*prevWindow, newResolution, prevParameters, prevCpuRamPixelFormat, prevKeepCpuRamCopy, AspectRatio(newResolution, ""));
	// \TODO: fix aspect ratio if known
}


void Texture::updateFromInternal()
{
	bind();
	updateGpuTexture(pixelData, pixelDataFormat, this->parameters.pixelFormat);
}


// when inputPixelFormat=TexturePixelFormat::Unset => assumes same as internally used format
// \TODO: fix this ugly pieces of spaghetti scheisse
void Texture::update(std::uint8_t* pixelData, int x, int y, int width, int height, const TexturePixelFormat& pixelFormat)
{
	VERSO_ASSERT_MSG("verso-3d", isCreated() == true, "Not yet created!");
	VERSO_FAIL("verso-3d"); // \TODO: implement check implementation and interface
	(void)x; (void)y; // \TODO: Implement actually using x and y in here
	(void)pixelData; (void)width; (void)height; (void)pixelFormat;

	//TexturePixelFormat inputPixelFormat = pixelFormat;
	//TexturePixelFormat outputPixelFormat = parameters.pixelFormat;
	//if (inputPixelFormat == TexturePixelFormat::Unset) {
	//	inputPixelFormat = outputPixelFormat;
	//}

	//bind();

	////glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	////VERSO_LOG_DEBUG("verso-3d", "Updating ("<<x<<","<<y<<")-("<<width<<"x"<<height<<")");
	////VERSO_LOG_DEBUG_VARIABLE("verso-3d", "alpha", useAlpha);
	////VERSO_LOG_DEBUG_VARIABLE("verso-3d", "pixeldata[0,3]", static_cast<unsigned int>(pixelData[0])<<","<<static_cast<unsigned int>(pixelData[1])<<","<<static_cast<unsigned int>(pixelData[2])<<","<<static_cast<unsigned int>(pixelData[3]));

	//GLint mipmapLevel = 0;
	//GLint border = 0;

	///*glTexSubImage2D(GL_TEXTURE_2D,
	//level, x, y, width, height,
	//texturePixelFormatToGlenum(outputPixelFormat),
	//GL_UNSIGNED_BYTE,
	//pixelData);*/

	//glTexImage2D(GL_TEXTURE_2D,
	//			 mipmapLevel, texturePixelFormatToGlenum(outputPixelFormat),
	//			 width, height, border,
	//			 texturePixelFormatToGlenum(inputPixelFormat),
	//			 GL_UNSIGNED_BYTE, pixelData);
	//GL_CHECK_FOR_ERRORS("", sourceFileName);

	////glEnable (GL_TEXTURE_RECTANGLE_ARB);
	////glTexSubImage2D(GL_TEXTURE_2D, level, x, y, width, height,
	////	texturePixelFormatToGlenum(outputPixelFormat), GL_UNSIGNED_BYTE, pixelData);
	////glTexImage2D(GL_TEXTURE_RECTANGLE_ARB, 0, )
	////glTexImage2D(GL_TEXTURE_RECTANGLE_ARB,
	////			 level, GL_RGBA, width, height, 0,
	////			 GL_BGRA, GL_UNSIGNED_BYTE, pixelData);
}


bool Texture::saveToFile(const UString& fileName, const ImageSaveFormat& imageSaveFormat, int mipmapLevel) const
{
	Image image;
	image.create(resolution, true, AspectRatio(resolution, "")); // \TODO: fix aspect ratio if known

	glGetTexImage(GL_TEXTURE_2D, mipmapLevel, GL_RGBA, GL_UNSIGNED_BYTE, image.getPixelData());
	GL_CHECK_FOR_ERRORS("glGetTexImage", "");

	image.flipVertically();
	bool result = image.saveToFile(fileName, imageSaveFormat, true);

	image.destroy();

	return result;
}


///////////////////////////////////////////////////////////////////////////////////////////
// getters & setters
///////////////////////////////////////////////////////////////////////////////////////////
bool Texture::isCreated() const
{
	return created;
}


GLuint Texture::getHandle() const
{
	return handle;
}


Vector2i Texture::getResolutioni() const
{
	return resolution;
}


Vector2f Texture::getResolutionf() const
{
	return Vector2f(static_cast<float>(resolution.x), static_cast<float>(resolution.y));
}


const TextureParameters& Texture::getParameters() const
{
	return parameters;
}


std::uint8_t* Texture::getPixelData()
{
	return pixelData;
}


TexturePixelFormat Texture::getPixelDataFormat() const
{
	return pixelDataFormat;
}


const UString& Texture::getSourceFileName() const
{
	return sourceFileName;
}


void Texture::setMinFilter(const MinFilter& minFilter)
{
	VERSO_ASSERT("verso-3d", isCreated() == true);

	parameters.minFilter = minFilter;
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilterToGlenum(parameters.minFilter));
	GL_CHECK_FOR_ERRORS("", sourceFileName);
}


void Texture::setMagFilter(const MagFilter& magFilter)
{
	VERSO_ASSERT("verso-3d", isCreated() == true);

	parameters.magFilter = magFilter;
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magFilterToGlenum(parameters.magFilter));
	GL_CHECK_FOR_ERRORS("", sourceFileName);
}


void Texture::setWrapStyle(const WrapStyle& wrapStyleS, const WrapStyle& wrapStyleT)
{
	VERSO_ASSERT("verso-3d", isCreated() == true);

	parameters.wrapStyleS = wrapStyleS;
	parameters.wrapStyleT = wrapStyleT;
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapStyleToGlenum(parameters.wrapStyleS));
	GL_CHECK_FOR_ERRORS("", sourceFileName);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapStyleToGlenum(parameters.wrapStyleT));
	GL_CHECK_FOR_ERRORS("", sourceFileName);
}


AspectRatio Texture::getAspectRatioTarget() const
{
	return aspectRatioTarget;
}


void Texture::setAspectRatioTarget(const AspectRatio& aspectRatio)
{
	aspectRatioTarget = aspectRatio;
}


AspectRatio Texture::getAspectRatioActual() const
{
	return AspectRatio(resolution, "");  // \TODO: fix aspect ratio if known
}


///////////////////////////////////////////////////////////////////////////////////////////
// Private
///////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////
// Static
///////////////////////////////////////////////////////////////////////////////////////////

bool Texture::isSupportedByFileExtension(const UString& fileName)
{
	if (!File::exists(fileName)) {
		VERSO_FILENOTFOUND("verso-3d", "Could not load texture", fileName.c_str());
	}

	size_t lastPeriod = fileName.findLastOf(".");
	if (lastPeriod == std::string::npos) {
		return false;
	}

	UString fileExtension = fileName.substring(static_cast<int>(lastPeriod));
	fileExtension.toLowerCase();
	if (fileExtension.equals(".jpg")) {
		return true;
	}
	else if (fileExtension.equals(".jpeg")) {
		return true;
	}
	else if (fileExtension.equals(".png")) {
		return true;
	}
	else if (fileExtension.equals(".tga")) {
		return true;
	}
	else if (fileExtension.equals(".bmp")) {
		return true;
	}
	else if (fileExtension.equals(".psd")) {
		return true;
	}
	else if (fileExtension.equals(".gif")) {
		return true;
	}
	else if (fileExtension.equals(".hdr")) {
		return true;
	}
	else if (fileExtension.equals(".pic")) {
		return true;
	}
	else if (fileExtension.equals(".pnm")) {
		return true;
	}
	return false;
}


///////////////////////////////////////////////////////////////////////////////////////////
// toString
///////////////////////////////////////////////////////////////////////////////////////////

UString Texture::toString(const UString& newLinePadding) const
{
	(void)newLinePadding;

	// \TODO: check and add missing
	UString str("{ ");
	if (isCreated() == false) {
		str += "isCreated() == false";
	}
	else {
		str += "[";
		str += resolution.toString();
		str += "], sourceFileName=";
		str += sourceFileName;
	}
	str += ", handle=";
	str.append2(handle);
	str += " }";
	return str;
}


UString Texture::toStringDebug(const UString& newLinePadding) const
{
	UString str("Texture(");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso

