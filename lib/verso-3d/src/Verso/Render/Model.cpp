//#include <assimp/Importer.hpp>
//#include <assimp/scene.h>
//#include <assimp/postprocess.h>
#include <Verso/Render/Model.hpp>
#include <Verso/Render/Render.hpp>
//#include "tiny_gltf.h"
//#include "cgltf.h"

#define TINYOBJLOADER_IMPLEMENTATION // define once
#define TINYOBJLOADER_USE_MAPBOX_EARCUT // Optional: robust trinagulation. Requires C++11
#include <tiny_obj_loader.h>

namespace Verso {


Model::Model(const UString& id) :
	created(false),
	id(id),
	meshes(),
	materials(),
	sourceFileName(""),
	directory("")
{
}


Model::Model(Model&& original) noexcept :
	created(std::move(original.created)),
	meshes(std::move(original.meshes)),
	materials(std::move(original.materials)),
	sourceFileName(std::move(original.sourceFileName)),
	directory(std::move(original.directory))
{
	//original.ptr = 0;
}


Model::~Model()
{
	destroy();
}


Model& Model::operator =(Model&& original) noexcept
{
	if (this != &original) {
		created = std::move(original.created);
		meshes = std::move(original.meshes);
		materials = std::move(original.materials);
		sourceFileName = std::move(original.sourceFileName);
		directory = std::move(original.directory);

		//original.ptr = 0;
	}
	return *this;
}


bool Model::createFromFileObj(IWindowOpengl& window, const UString& fileName, const UString& materialPath)
{
	if (isCreated()) {
		VERSO_ERROR("verso-3d", "Model already created!", toString().c_str());
	}

	tinyobj::ObjReaderConfig reader_config;
	reader_config.mtl_search_path = materialPath.c_str(); // Path to material files

	tinyobj::ObjReader reader;

	if (!reader.ParseFromFile(fileName.c_str(), reader_config)) {
		if (!reader.Error().empty()) {
			std::cerr << "TinyObjReader: " << reader.Error();
		}
		exit(1);
	}

	if (!reader.Warning().empty()) {
		std::cout << "TinyObjReader: " << reader.Warning();
	}

	auto& attrib = reader.GetAttrib();
	auto& shapes = reader.GetShapes();
	auto& materials = reader.GetMaterials();

	//RgbaColorf ambient(materials[0].ambient[0], materials[0].ambient[1], materials[0].ambient[2]);
	//RgbaColorf diffuse(materials[0].diffuse[0], materials[0].diffuse[1], materials[0].diffuse[2]);
	//RgbaColorf specular(materials[0].specular[0], materials[0].specular[1], materials[0].specular[2]);
	//float shininess = materials[0].shininess;
	//PhongMaterialParam phongMaterialParam(ambient, diffuse, specular, shininess);
//	matPaatti.create(
//				window,
//				demoPaths->pathModels()+"paatti.jpg",
//				PhongMaterialParam::whiteRubber());

	std::cout << "shapes " << shapes.size() << std::endl;
	std::cout << "faces " << shapes[0].mesh.num_face_vertices.size() << std::endl;

	// Loop over shapes
	for (size_t s = 0; s<shapes.size(); s++) { // 1; s++) {
		UString meshId(id+".");
		meshId.append2(s);
		meshes.push_back(Mesh(meshId));
		meshes[s].create(PrimitiveType::Triangles);
		Vao& vao = meshes[s].vao;

		// Loop over faces(polygon)
		size_t runningIndex = 0;
		size_t indexOffset = 0;
		for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) {
			size_t fv = size_t(shapes[s].mesh.num_face_vertices[f]);

			GLuint indices[3] = {0, 0, 0};
			float pos[3*3] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
			float normal[3*3] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
			float uv[3*2] = {0, 0, 0, 0, 0, 0};

			// Loop over vertices in the face.
			for (size_t v = 0; v < fv; v++) {
				tinyobj::index_t idx = shapes[s].mesh.indices[indexOffset + v];
				indices[v] = runningIndex++;//idx.vertex_index;

				// access to vertex
				tinyobj::real_t vx = attrib.vertices[3*size_t(idx.vertex_index)+0];
				tinyobj::real_t vy = attrib.vertices[3*size_t(idx.vertex_index)+1];
				tinyobj::real_t vz = attrib.vertices[3*size_t(idx.vertex_index)+2];
				pos[v*3 + 0] = vx;
				pos[v*3 + 1] = vy;
				pos[v*3 + 2] = vz;

				// Check if `normal_index` is zero or positive. negative = no normal data
				if (idx.normal_index >= 0) {
					tinyobj::real_t nx = attrib.normals[3*size_t(idx.normal_index)+0];
					tinyobj::real_t ny = attrib.normals[3*size_t(idx.normal_index)+1];
					tinyobj::real_t nz = attrib.normals[3*size_t(idx.normal_index)+2];
					normal[v*3 + 0] = nx;
					normal[v*3 + 1] = ny;
					normal[v*3 + 2] = nz;
				}

				// Check if `texcoord_index` is zero or positive. negative = no texcoord data
				if (idx.texcoord_index >= 0) {
					tinyobj::real_t tx = attrib.texcoords[2*size_t(idx.texcoord_index)+0];
					tinyobj::real_t ty = attrib.texcoords[2*size_t(idx.texcoord_index)+1];
					normal[v*3 + 0] = tx;
					normal[v*3 + 1] = ty;
				}

				// Optional: vertex colors
				// tinyobj::real_t red   = attrib.colors[3*size_t(idx.vertex_index)+0];
				// tinyobj::real_t green = attrib.colors[3*size_t(idx.vertex_index)+1];
				// tinyobj::real_t blue  = attrib.colors[3*size_t(idx.vertex_index)+2];
			}

			vao.appendDataf(
						ShaderAttribute::Position,
						pos, 3, 3);

			vao.appendDataf(
						ShaderAttribute::Normal,
						normal, 3, 3);

			vao.appendDataf(
						ShaderAttribute::Uv,
						uv, 3, 2);
			//std::cout << "uv = ["<<uv[0]<<" "<<uv[1]<<"] ["<<uv[2]<<" "<<uv[3]<<"] ["<<uv[4]<<" "<<uv[5]<<"]" << std::endl;

			vao.appendIndices(indices, 3);
			indexOffset += fv;

			// per-face material
			shapes[s].mesh.material_ids[f];
		}

		vao.applyAppendedData(BufferUsagePattern::StaticDraw);
	}

	created = true;
	return true;
}


bool Model::createFromFileGltf(IWindowOpengl& window, const UString& fileName)
{
	(void)window; (void)fileName;
	if (isCreated()) {
		VERSO_ERROR("verso-3d", "Model already created!", toString().c_str());
	}

//	tinygltf::Model model;
//	tinygltf::TinyGLTF loader;
//	std::string err;
//	std::string warn;

//	bool ret = loader.LoadASCIIFromFile(&model, &err, &warn, fileName.toUtf8String());
//	//bool ret = loader.LoadBinaryFromFile(&model, &err, &warn, fileName.toUtf8String()); // for binary glTF(.glb)

//	if (!warn.empty()) {
//		UString warning("Warning loading glTF file: ");
//		warning.append2(warn.c_str());
//		VERSO_LOG_WARN("verso-3d", warning.c_str());
//		VERSO_LOG_WARN_VARIABLE("verso-3d", "fileName", fileName.c_str());
//	}

//	if (!err.empty()) {
//	  UString error("Error loading glTF file: ");
//	  error.append2(err.c_str());
//	  VERSO_LOG_WARN("verso-3d", error.c_str());
//	  VERSO_LOG_WARN_VARIABLE("verso-3d", "fileName", fileName.c_str());
//	}

//	if (!ret) {
//	  return false;
//	}

//	std::vector<tinygltf::Accessor> accessors;
//	std::vector<tinygltf::Animation> animations;
//	std::vector<tinygltf::Buffer> buffers;
//	std::vector<tinygltf::BufferView> bufferViews;
//	std::vector<tinygltf::Material> materials;
//	std::vector<tinygltf::Mesh> meshes;
//	std::vector<tinygltf::Node> nodes;

//	std::vector<tinygltf::Texture> textures;
//	std::vector<tinygltf::Image> images;
//	std::vector<tinygltf::Sampler> samplers;

//	//std::vector<tinygltf::Skin> skins;
//	//std::vector<tinygltf::Camera> cameras;
//	//std::vector<tinygltf::Scene> scenes;
//	//std::vector<tinygltf::Light> lights;

//	int defaultScene = -1;
//	std::vector<std::string> extensionsUsed;
//	std::vector<std::string> extensionsRequired;

//	tinygltf::Asset asset;

//	tinygltf::Value extras;
//	tinygltf::ExtensionMap extensions;


	created = true;
	return true;
}


//void processNode(aiNode* node, const aiScene* scene, std::vector<Mesh>& outMeshes);
//Mesh processMesh(aiMesh* mesh, const aiScene* scene);
//std::vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, const UString& typeName);
bool Model::createFromFileAssimp(IWindowOpengl& window, const UString& fileName)
{
	if (isCreated()) {
		VERSO_ERROR("verso-3d", "Model already created!", toString().c_str());
	}

	(void)window; (void)fileName;
	// \TODO: implement Model class with also assimp
//	Assimp::Importer import;
//	const aiScene* scene = import.ReadFile(fileName.c_str(), aiProcess_Triangulate | aiProcess_FlipUVs);

//	if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)  {
//		UString error("Error Verso::Model::createFromFile(): Could not load model \""+fileName+"\"\n");
//		error += "  AssImp error: "+import.GetErrorString();
//		return false;
//	}
//	this->sourceFileName = fileName;
//	size_t slashIndex = fileName.findLastOf("/");
//	if (slashIndex != std::string::npos) {
//		this->directory = fileName.substring(0, slashIndex);
//	}
//	else {
//		this->directory = "./";
//	}

//	processNode(scene->mRootNode, scene, meshes);

//	created = true;
	VERSO_FAIL("verso-3d");
}


void Model::destroy() VERSO_NOEXCEPT
{
	for (auto&& i : materials) {
		delete i;
	}
	materials.clear();

	for (auto&& i : meshes) {
		i.destroy();
	}
	meshes.clear();
}


bool Model::isCreated() const
{
	return created;
}


void Model::render(IWindowOpengl& window)
{
	for (size_t i=0; i<meshes.size(); ++i) {
		meshes[i].render(window);
	}
}


void Model::render(IWindowOpengl& window, ShaderProgram& shaderProgram)
{
	for (size_t i=0; i<meshes.size(); ++i) {
		meshes[i].render(window, shaderProgram);
	}
}


void Model::renderDebugNormals(IWindowOpengl& window, const FrameTimestamp& time, const ICamera& camera)
{
	for (size_t i=0; i<meshes.size(); ++i) {
		Render::debugNormals(window, time, camera, meshes[i].vao, camera.getViewMatrix());
	}
}


UString Model::toString() const
{
	UString str;
	str += "TODO";
	return str;
}


UString Model::toStringDebug() const
{
	UString str("Model(");
	str += toString();
	str += ")";
	return str;
}


//void processNode(aiNode* node, const aiScene* scene, std::vector<Mesh>& outMeshes)
//{
//	// Process all the node's meshes (if any)
//	for (size_t i=0; i<node->mNumMeshes; i++) {
//		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
//		outMeshes.push_back(processMesh(mesh, scene));
//	}
//	// Then do the same for each of its children
//	for (size_t i=0; i<node->mNumChildren; i++) {
//		processNode(node->mChildren[i], scene, outMeshes);
//	}
//}


//Mesh processMesh(aiMesh* mesh, const aiScene* scene)
//{
//	std::vector<Vertex> vertices;
//	std::vector<GLuint> indices;
//	std::vector<Texture> textures;

//	for (size_t i=0; i<mesh->mNumVertices; i++) {
//		// Process vertex positions, normals and texture coordinates
//		Vertex vertex;
//		// We declare a placeholder vector since assimp uses its own vector class that doesn't directly
//		// convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.
//		Vector3f vector;
//		// Positions
//		vector.x = mesh->mVertices[i].x;
//		vector.y = mesh->mVertices[i].y;
//		vector.z = mesh->mVertices[i].z;
//		vertex.position = vector;
//		// Normals
//		vector.x = mesh->mNormals[i].x;
//		vector.y = mesh->mNormals[i].y;
//		vector.z = mesh->mNormals[i].z;
//		vertex.normal = vector;
//		// Texture Coordinates
//		// - Does the mesh contain texture coordinates?
//		if (mesh->mTextureCoords[0]) {
//			Vector2f vec;
//			// A vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't
//			// use models where a vertex can have multiple texture coordinates so we always take the first set (0).
//			vec.x = mesh->mTextureCoords[0][i].x;
//			vec.y = mesh->mTextureCoords[0][i].y;
//			vertex.texCoords = vec;
//		}
//		else {
//			vertex.texCoords = Vector2f(0.0f, 0.0f);
//		}
//		vertices.push_back(vertex);
//	}

//	// Process indices
//	// Now wak through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
//	for (size_t i=0; i<mesh->mNumFaces; i++) {
//		aiFace face = mesh->mFaces[i];
//		// Retrieve all indices of the face and store them in the indices vector
//		for (size_t j=0; j<face.mNumIndices; j++) {
//			indices.push_back(face.mIndices[j]);
//		}
//	}

//	// Process materials
//	//aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
//	// We assume a convention for sampler names in the shaders. Each diffuse texture should be named
//	// as 'texture_diffuseN' where N is a sequential number ranging from 1 to MAX_SAMPLER_NUMBER.
//	// Same applies to other texture as the following list summarizes:
//	// Diffuse: texture_diffuseN
//	// Specular: texture_specularN
//	// Normal: texture_normalN

//	// \TODO!
//	// 1. Diffuse maps
//	/*
//	vector<Texture> diffuseMaps = this->loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
//	textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
//	// 2. Specular maps
//	vector<Texture> specularMaps = this->loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
//	textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
//	*/

//	Mesh out;//(vertices, indices, textures);
//	return out;
//}


//// Checks all material textures of a given type and loads the textures if they're not loaded yet.
//// The required info is returned as a Texture struct.
//std::vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, const UString& typeName)
//{
//	(void)typeName; VERSO_FAIL("verso-3d"); // \TODO: finish implementation
//	std::vector<Texture> textures;
//	for (size_t i=0; i<mat->GetTextureCount(type); i++) {
//		aiString str;
//		mat->GetTexture(type, i, &str);
//		// Check if texture was loaded before and if so, continue to next iteration: skip loading a new texture
//		// \TODO
//		/*Texture texture;
//		texture.id = TextureFromFile(str.C_Str(), this->directory);
//		texture.type = typeName;
//		texture.path = str;
//		textures.push_back(texture);*/
//	}
//	return textures;
//}


} // End namespace Verso


