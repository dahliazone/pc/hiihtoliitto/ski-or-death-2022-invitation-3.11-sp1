#include <Verso/Render/Skybox.hpp>
#include <Verso/Render/VaoGenerator/VaoGenerator.hpp>
#include <Verso/Display/IWindowOpengl.hpp>

namespace Verso {


Skybox::Skybox() :
	vaoPosX("verso-3d/Skybox"),
	vaoNegX("verso-3d/Skybox"),
	vaoPosY("verso-3d/Skybox"),
	vaoNegY("verso-3d/Skybox"),
	vaoPosZ("verso-3d/Skybox"),
	vaoNegZ("verso-3d/Skybox"),
	matPosX(),
	matNegX(),
	matPosY(),
	matNegY(),
	matPosZ(),
	matNegZ(),
	created(false)
{
}


Skybox::Skybox(Skybox&& original) :
	vaoPosX(std::move(original.vaoPosX)),
	vaoNegX(std::move(original.vaoNegX)),
	vaoPosY(std::move(original.vaoPosY)),
	vaoNegY(std::move(original.vaoNegY)),
	vaoPosZ(std::move(original.vaoPosZ)),
	vaoNegZ(std::move(original.vaoNegZ)),
	matPosX(std::move(original.matPosX)),
	matNegX(std::move(original.matNegX)),
	matPosY(std::move(original.matPosY)),
	matNegY(std::move(original.matNegY)),
	matPosZ(std::move(original.matPosZ)),
	matNegZ(std::move(original.matNegZ)),
	created(std::move(original.created))
{
}


Skybox& Skybox::operator =(Skybox&& original)
{
	if (this != &original) {
		vaoPosX = std::move(original.vaoPosX);
		vaoNegX = std::move(original.vaoNegX);
		vaoPosY = std::move(original.vaoPosY);
		vaoNegY = std::move(original.vaoNegY);
		vaoPosZ = std::move(original.vaoPosZ);
		vaoNegZ = std::move(original.vaoNegZ);
		matPosX = std::move(original.matPosX);
		matNegX = std::move(original.matNegX);
		matPosY = std::move(original.matPosY);
		matNegY = std::move(original.matNegY);
		matPosZ = std::move(original.matPosZ);
		matNegZ = std::move(original.matNegZ);
		created = std::move(original.created);
	}
	return *this;
}


Skybox::~Skybox()
{
	destroy();
}


void Skybox::createFromPath(IWindowOpengl& window,
							const UString& skyboxPath, const ICamera& camera,
							bool swapX, bool swapY, bool swapZ)
{
	VERSO_ASSERT_MSG("verso-3d", isCreated() == false, "Already created!");

	UString posXFileName(skyboxPath), negXFileName(skyboxPath);
	if (swapX == false) {
		posXFileName += "posx.jpg";
		negXFileName += "negx.jpg";
	}
	else {
		posXFileName += "negx.jpg";
		negXFileName += "posx.jpg";
	}

	UString posYFileName(skyboxPath), negYFileName(skyboxPath);
	if (swapY == false) {
		posYFileName += "posy.jpg";
		negYFileName += "negy.jpg";
	}
	else {
		posYFileName += "negy.jpg";
		negYFileName += "posy.jpg";
	}

	UString posZFileName(skyboxPath), negZFileName(skyboxPath);
	if (swapZ == false) {
		posZFileName += "posz.jpg";
		negZFileName += "negz.jpg";
	}
	else {
		posZFileName += "negz.jpg";
		negZFileName += "posz.jpg";
	}

	createFromFiles(window,
					posXFileName, negXFileName,
					posYFileName, negYFileName,
					posZFileName, negZFileName,
					camera);
}


void Skybox::createFromFiles(IWindowOpengl& window,
							 const UString& posXFileName, const UString& negXFileName,
							 const UString& posYFileName, const UString& negYFileName,
							 const UString& posZFileName, const UString& negZFileName,
							 const ICamera& camera)
{
	if (isCreated()) {
		VERSO_ERROR("verso-3d", "Skybox already created!", toString("").c_str());
	}

	VaoGenerator::skybox3d(vaoPosX, vaoNegX, vaoPosY, vaoNegY, vaoPosZ, vaoNegZ, camera);

	matPosX.create(window, posXFileName);
	matPosX.getDiffuseTexture().setWrapStyle(WrapStyle::ClampToEdge, WrapStyle::ClampToEdge);
	vaoPosX.setId("verso-3d/Skybox: " + posXFileName);

	matNegX.create(window, negXFileName);
	matNegX.getDiffuseTexture().setWrapStyle(WrapStyle::ClampToEdge, WrapStyle::ClampToEdge);
	vaoNegX.setId("verso-3d/Skybox: " + negXFileName);

	matPosY.create(window, posYFileName);
	matPosY.getDiffuseTexture().setWrapStyle(WrapStyle::ClampToEdge, WrapStyle::ClampToEdge);
	vaoPosY.setId("verso-3d/Skybox: " + posYFileName);

	matNegY.create(window, negYFileName);
	matNegY.getDiffuseTexture().setWrapStyle(WrapStyle::ClampToEdge, WrapStyle::ClampToEdge);
	vaoNegY.setId("verso-3d/Skybox: " + negYFileName);

	matPosZ.create(window, posZFileName);
	matPosZ.getDiffuseTexture().setWrapStyle(WrapStyle::ClampToEdge, WrapStyle::ClampToEdge);
	vaoPosZ.setId("verso-3d/Skybox: " + posZFileName);

	matNegZ.create(window, negZFileName);
	matNegZ.getDiffuseTexture().setWrapStyle(WrapStyle::ClampToEdge, WrapStyle::ClampToEdge);
	vaoNegZ.setId("verso-3d/Skybox: " + negZFileName);

	created = true;
}


void Skybox::destroy() VERSO_NOEXCEPT
{
	if (isCreated() == false) {
		return;
	}

	matPosX.destroy();
	matNegX.destroy();
	matPosY.destroy();
	matNegY.destroy();
	matPosZ.destroy();
	matNegZ.destroy();

	vaoPosX.destroy();
	vaoNegX.destroy();
	vaoPosY.destroy();
	vaoNegY.destroy();
	vaoPosZ.destroy();
	vaoNegZ.destroy();

	created = false;
}


bool Skybox::isCreated() const
{
	return created;
}


void Skybox::changeShaders(const UString& vertexSourceFileName, const UString& fragmentSourceFileName)
{
	matPosX.changeShaders(vertexSourceFileName, fragmentSourceFileName);
	matNegX.changeShaders(vertexSourceFileName, fragmentSourceFileName);
	matPosY.changeShaders(vertexSourceFileName, fragmentSourceFileName);
	matNegY.changeShaders(vertexSourceFileName, fragmentSourceFileName);
	matPosZ.changeShaders(vertexSourceFileName, fragmentSourceFileName);
	matNegZ.changeShaders(vertexSourceFileName, fragmentSourceFileName);
}


void Skybox::changeShaders(const UString& vertexSourceFileName, const UString& fragmentSourceFileName, const UString& geometrySourceFileName)
{
	matPosX.changeShaders(vertexSourceFileName, fragmentSourceFileName, geometrySourceFileName);
	matNegX.changeShaders(vertexSourceFileName, fragmentSourceFileName, geometrySourceFileName);
	matPosY.changeShaders(vertexSourceFileName, fragmentSourceFileName, geometrySourceFileName);
	matNegY.changeShaders(vertexSourceFileName, fragmentSourceFileName, geometrySourceFileName);
	matPosZ.changeShaders(vertexSourceFileName, fragmentSourceFileName, geometrySourceFileName);
	matNegZ.changeShaders(vertexSourceFileName, fragmentSourceFileName, geometrySourceFileName);
}


void Skybox::render(IWindowOpengl& window, const FrameTimestamp& time, const ICamera& camera)
{
	(void)window;

	Matrix4x4f model;
	Matrix4x4f view = camera.getViewMatrix();
	Matrix4x4f projection = camera.getProjectionMatrix();
	Vector3f position(0.0f, 0.0f, 0.0f);

	// Remove translation from the view matrix
	view.data[0][3] = 0.0f;
	view.data[1][3] = 0.0f;
	view.data[2][3] = 0.0f;

	Opengl::depthWrite(false);
	Opengl::depthTest(false);

	// posX
	matPosX.apply(window, time, view, projection, position, std::vector<DirectionalLight>(),
				  std::vector<PointLight>(), std::vector<SpotLight>());
	matPosX.updateModelMatrix(model);
	vaoPosX.render();

	// negX
	matNegX.apply(window, time, view, projection, position, std::vector<DirectionalLight>(),
				  std::vector<PointLight>(), std::vector<SpotLight>());
	matNegX.updateModelMatrix(model);
	vaoNegX.render();

	// posY
	matPosY.apply(window, time, view, projection, position, std::vector<DirectionalLight>(),
				  std::vector<PointLight>(), std::vector<SpotLight>());
	matPosY.updateModelMatrix(model);
	vaoPosY.render();

	// negY
	matNegY.apply(window, time, view, projection, position, std::vector<DirectionalLight>(),
				  std::vector<PointLight>(), std::vector<SpotLight>());
	matNegY.updateModelMatrix(model);
	vaoNegY.render();

	// posZ
	matPosZ.apply(window, time, view, projection, position, std::vector<DirectionalLight>(),
				  std::vector<PointLight>(), std::vector<SpotLight>());
	matPosZ.updateModelMatrix(model);
	vaoPosZ.render();

	// negZ
	matNegZ.apply(window, time, view, projection, position, std::vector<DirectionalLight>(),
				  std::vector<PointLight>(), std::vector<SpotLight>());
	matNegZ.updateModelMatrix(model);
	vaoNegZ.render();

	Opengl::depthWrite(true);
	Opengl::depthTest(true);
}


UString Skybox::toString(const UString& newLinePadding) const
{
	UString str("{ ");
	UString newLinePadding2 = newLinePadding + "  ";
	if (isCreated() == false) {
		str += "created=false";
	}
	else {
		str += "created=true";
		str += ", matPosX=";
		str += matPosX.toString(newLinePadding2);
		str += ", matNegX=";
		str += matNegX.toString(newLinePadding2);
		str += ", matPosY=";
		str += matPosY.toString(newLinePadding2);
		str += ", matNegY=";
		str += matNegY.toString(newLinePadding2);
		str += ", matPosZ=";
		str += matPosZ.toString(newLinePadding2);
		str += ", matNegZ=";
		str += matNegZ.toString(newLinePadding2);
	}
	str += " }";
	return str;
}


UString Skybox::toStringDebug(const UString& newLinePadding) const
{
	UString str("Skybox(");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso

