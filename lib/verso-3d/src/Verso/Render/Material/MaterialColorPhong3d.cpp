#include <Verso/Render/Material/MaterialColorPhong3d.hpp>
#include <Verso/Render/Render.hpp>

namespace Verso {


MaterialColorPhong3d::MaterialColorPhong3d() :
	created(false),
	phongMaterial(),
	shader3d()
{
}


MaterialColorPhong3d::MaterialColorPhong3d(MaterialColorPhong3d&& original) :
	created(std::move(original.created)),
	phongMaterial(std::move(original.phongMaterial)),
	shader3d(std::move(original.shader3d))
{
}


MaterialColorPhong3d& MaterialColorPhong3d::operator =(MaterialColorPhong3d&& original)
{
	if (this != &original) {
		created = std::move(original.created);
		phongMaterial = std::move(original.phongMaterial);
		shader3d = std::move(original.shader3d);
	}
	return *this;
}


MaterialColorPhong3d::~MaterialColorPhong3d()
{
	destroy();
}


///////////////////////////////////////////////////////////////////////////////////////////
// Interface IMaterial
///////////////////////////////////////////////////////////////////////////////////////////

void MaterialColorPhong3d::create(
		IWindowOpengl& window,
		const PhongMaterialParam& phongMaterialParam,
		const UString& vertexSourceFileName,
		const UString& fragmentSourceFileName,
		const UString& geometrySourceFileName)
{
	(void)window;
	VERSO_ASSERT_MSG("verso-3d", isCreated() == false, "Already created!");
	this->phongMaterial = phongMaterialParam;

	// Load material shader
	UString tmpVertexSourceFileName(Render::shadersVersoPath+"render/material/colorphong3d.330.vert");
	if (vertexSourceFileName.size() != 0) {
		tmpVertexSourceFileName.set(vertexSourceFileName);
	}

	UString tmpFragmentSourceFileName(Render::shadersVersoPath+"render/material/colorphong3d.330.frag");
	if (fragmentSourceFileName.size() != 0) {
		tmpFragmentSourceFileName.set(fragmentSourceFileName);
	}

	UString tmpGeometrySourceFileName;
	if (geometrySourceFileName.size() != 0) {
		tmpGeometrySourceFileName.set(geometrySourceFileName);
	}

	if (tmpGeometrySourceFileName.size() == 0) {
		shader3d.createFromFiles(tmpVertexSourceFileName, tmpFragmentSourceFileName);
	}
	else {
		shader3d.createFromFiles(tmpVertexSourceFileName, tmpFragmentSourceFileName, tmpGeometrySourceFileName);
	}

	shader3d.linkProgram();
	created = true;
}


void MaterialColorPhong3d::destroy() VERSO_NOEXCEPT
{
	if (isCreated() == false) {
		return;
	}

	shader3d.destroy();
	created = false;
}


bool MaterialColorPhong3d::isCreated() const
{
	return created;
}


void MaterialColorPhong3d::changeShaders(const UString& vertexSourceFileName, const UString& fragmentSourceFileName)
{
	shader3d.destroy();
	shader3d.createFromFiles(vertexSourceFileName, fragmentSourceFileName);
	shader3d.linkProgram();
}


void MaterialColorPhong3d::changeShaders(const UString& vertexSourceFileName, const UString& fragmentSourceFileName, const UString& geometrySourceFileName)
{
	shader3d.destroy();
	shader3d.createFromFiles(vertexSourceFileName, fragmentSourceFileName, geometrySourceFileName);
	shader3d.linkProgram();
}


void MaterialColorPhong3d::update(const FrameTimestamp& time)
{
	(void)time;
}


void MaterialColorPhong3d::apply(
		IWindowOpengl& window,
		const FrameTimestamp& time, const ICamera& camera,
		const std::vector<DirectionalLight>& directionalLights,
		const std::vector<PointLight>& pointLights,
		const std::vector<SpotLight>& spotLights)
{
	apply(window, time, camera.getViewMatrix(), camera.getProjectionMatrix(), camera.getPosition(),
		  directionalLights, pointLights, spotLights);
}


void MaterialColorPhong3d::apply(
		IWindowOpengl& window,
		const FrameTimestamp& time, const Matrix4x4f& viewMatrix, const Matrix4x4f& projectionMatrix, const Vector3f& position,
		const std::vector<DirectionalLight>& directionalLights,
		const std::vector<PointLight>& pointLights,
		const std::vector<SpotLight>& spotLights)
{
	(void)window; (void)time;
	VERSO_ASSERT("verso-3d", pointLights.size()>0 && "You must provide at least one pointlight!");

	const int MAX_DIRECTIONAL_LIGHTS = 2; // These must match the shader!
	const int MAX_POINT_LIGHTS = 10;
	const int MAX_SPOT_LIGHTS = 10;

	shader3d.useProgram();

	// Vertex
	shader3d.setUniform("view", viewMatrix);
	shader3d.setUniform("projection", projectionMatrix);

	// Fragment
	shader3d.setUniform("viewPos", position);

	// - material
	shader3d.setUniformRgb("material.ambient", phongMaterial.ambient);
	shader3d.setUniformRgb("material.diffuse", phongMaterial.diffuse);
	shader3d.setUniformRgb("material.specular", phongMaterial.specular);
	shader3d.setUniformf("material.shininess", phongMaterial.shininess);

	// - directionalLights
	int directionalLightsCount = Math::minValue<int>(static_cast<int>(directionalLights.size()), MAX_DIRECTIONAL_LIGHTS);
	shader3d.setUniformi("directionalLightsCount", directionalLightsCount);

	for (int i=0; i<directionalLightsCount; ++i) {
		UString variable("directionalLights[");
		variable.append2(i);
		variable += "]";

		shader3d.setUniform(variable+".direction", directionalLights[i].direction);

		shader3d.setUniformRgb(variable+".ambient", directionalLights[i].ambient);
		shader3d.setUniformRgb(variable+".diffuse", directionalLights[i].diffuse);
		shader3d.setUniformRgb(variable+".specular", directionalLights[i].specular);
	}

	// - pointLights
	int pointLightsCount = Math::minValue<int>(static_cast<int>(pointLights.size()), MAX_POINT_LIGHTS);
	shader3d.setUniformi("pointLightsCount", pointLightsCount);

	for (int i=0; i<pointLightsCount; ++i) {
		UString variable("pointLights[");
		variable.append2(i);
		variable += "]";

		shader3d.setUniform(variable+".position", pointLights[i].position);

		shader3d.setUniformf(variable+".quadratic", pointLights[i].attenuation.constant);
		shader3d.setUniformf(variable+".linear", pointLights[i].attenuation.linear);
		shader3d.setUniformf(variable+".quadratic", pointLights[i].attenuation.quadratic);

		shader3d.setUniformRgb(variable+".ambient", pointLights[i].ambient);
		shader3d.setUniformRgb(variable+".diffuse", pointLights[i].diffuse);
		shader3d.setUniformRgb(variable+".specular", pointLights[i].specular);
	}

	// - spotLights
	int spotLightsCount = Math::minValue<int>(static_cast<int>(spotLights.size()), MAX_SPOT_LIGHTS);
	shader3d.setUniformi("spotLightsCount", spotLightsCount);

	for (int i=0; i<spotLightsCount; ++i) {
		UString variable("spotLights[");
		variable.append2(i);
		variable += "]";

		shader3d.setUniform(variable+".position", spotLights[i].position);
		shader3d.setUniform(variable+".direction", spotLights[i].direction);

		shader3d.setUniformf(variable+".cutOff", spotLights[i].cutOff);
		shader3d.setUniformf(variable+".outerCutOff", spotLights[i].outerCutOff);

		shader3d.setUniformf(variable+".constant", spotLights[i].attenuation.constant);
		shader3d.setUniformf(variable+".linear", spotLights[i].attenuation.linear);
		shader3d.setUniformf(variable+".quadratic", spotLights[i].attenuation.quadratic);

		shader3d.setUniformRgb(variable+".ambient", spotLights[i].ambient);
		shader3d.setUniformRgb(variable+".diffuse", spotLights[i].diffuse);
		shader3d.setUniformRgb(variable+".specular", spotLights[i].specular);
	}
}


void MaterialColorPhong3d::updateModelMatrix(const Matrix4x4f& model)
{
	shader3d.setUniform("model", model);
}


UString MaterialColorPhong3d::toString(const UString& newLinePadding) const
{
	UString str("{ ");
	UString newLinePadding2 = newLinePadding + "  ";
	str += "phongMaterial=";
	str += phongMaterial.toString(newLinePadding2);
	str += ", shader3d=";
	str += shader3d.toString(newLinePadding2);
	str += " }";
	return str;
}


UString MaterialColorPhong3d::toStringDebug(const UString& newLinePadding) const
{
	UString str("MaterialColorPhong3d(");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso

