#include <Verso/Render/Material/MaterialLightmapsPhong3d_GridHeightmapDisplacement.hpp>
#include <Verso/Render/Render.hpp>

namespace Verso {


MaterialLightmapsPhong3d_GridHeightmapDisplacement::MaterialLightmapsPhong3d_GridHeightmapDisplacement() :
	created(false),
	diffuseTexture(),
	specularTexture(),
	heightmapTexture(),
	maxHeight(1.0f),
	phongMaterial(),
	shader3d()
{
}


MaterialLightmapsPhong3d_GridHeightmapDisplacement::MaterialLightmapsPhong3d_GridHeightmapDisplacement(MaterialLightmapsPhong3d_GridHeightmapDisplacement&& original) :
	created(std::move(original.created)),
	diffuseTexture(std::move(original.diffuseTexture)),
	specularTexture(std::move(original.specularTexture)),
	heightmapTexture(std::move(original.heightmapTexture)),
	maxHeight(std::move(original.maxHeight)),
	phongMaterial(std::move(original.phongMaterial)),
	shader3d(std::move(original.shader3d))
{
}


MaterialLightmapsPhong3d_GridHeightmapDisplacement& MaterialLightmapsPhong3d_GridHeightmapDisplacement::operator =(MaterialLightmapsPhong3d_GridHeightmapDisplacement&& original)
{
	if (this != &original) {
		created = std::move(original.created);
		diffuseTexture = std::move(original.diffuseTexture);
		specularTexture = std::move(original.specularTexture);
		heightmapTexture = std::move(original.heightmapTexture);
		maxHeight = std::move(original.maxHeight);
		phongMaterial = std::move(original.phongMaterial);
		shader3d = std::move(original.shader3d);
	}
	return *this;
}


MaterialLightmapsPhong3d_GridHeightmapDisplacement::~MaterialLightmapsPhong3d_GridHeightmapDisplacement()
{
	destroy();
}


///////////////////////////////////////////////////////////////////////////////////////////
// Interface IMaterial
///////////////////////////////////////////////////////////////////////////////////////////

void MaterialLightmapsPhong3d_GridHeightmapDisplacement::create(
		IWindowOpengl& window,
		const UString& diffuseTextureFileName,
		const UString& specularTextureFileName,
		const UString& heightmapTextureFileName,
		float maxHeight,
		const PhongMaterialParam& phongMaterialParam,
		const UString& vertexSourceFileName,
		const UString& fragmentSourceFileName,
		const UString& geometrySourceFileName)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == false, "Already created!", diffuseTexture.getSourceFileName().c_str());

	this->phongMaterial = phongMaterialParam;

	// Load diffuse texture
	diffuseTexture.createFromFile(
				window, diffuseTextureFileName,
				TextureParameters("material.diffuseTexture", TexturePixelFormat::Rgb));

	// Load specular texture
	specularTexture.createFromFile(
				window, specularTextureFileName,
				TextureParameters("material.specularTexture", TexturePixelFormat::Rgb));

	// Load heightmap texture
	heightmapTexture.createFromFile(
				window, heightmapTextureFileName,
				TextureParameters("material.heightmapTexture", TexturePixelFormat::Rgb));

	this->maxHeight = maxHeight;

	// Load material shader
	Render::quad3dVao.bind();
	{
		UString tmpVertexSourceFileName(Render::shadersVersoPath+"render/material/lightmapsphong3d_gridheightmapdisplacement.330.vert");
		if (vertexSourceFileName.size() != 0) {
			tmpVertexSourceFileName.set(vertexSourceFileName);
		}

		UString tmpFragmentSourceFileName(Render::shadersVersoPath+"render/material/lightmapsphong3d_gridheightmapdisplacement.330.frag");
		if (fragmentSourceFileName.size() != 0) {
			tmpFragmentSourceFileName.set(fragmentSourceFileName);
		}

		UString tmpGeometrySourceFileName;
		if (geometrySourceFileName.size() != 0) {
			tmpGeometrySourceFileName.set(geometrySourceFileName);
		}

		if (tmpGeometrySourceFileName.size() == 0) {
			shader3d.createFromFiles(tmpVertexSourceFileName, tmpFragmentSourceFileName);
		} else {
			shader3d.createFromFiles(tmpVertexSourceFileName, tmpFragmentSourceFileName, tmpGeometrySourceFileName);
		}

		shader3d.linkProgram();
	}
	Render::quad3dVao.unbind();


	created = true;
}


void MaterialLightmapsPhong3d_GridHeightmapDisplacement::destroy() VERSO_NOEXCEPT
{
	if (isCreated() == false) {
		return;
	}

	diffuseTexture.destroy();
	specularTexture.destroy();
	heightmapTexture.destroy();
	shader3d.destroy();
	created = false;
}


bool MaterialLightmapsPhong3d_GridHeightmapDisplacement::isCreated() const
{
	return created;
}


void MaterialLightmapsPhong3d_GridHeightmapDisplacement::changeShaders(const UString& vertexSourceFileName, const UString& fragmentSourceFileName)
{
	shader3d.destroy();
	shader3d.createFromFiles(vertexSourceFileName, fragmentSourceFileName);
	shader3d.linkProgram();
}


void MaterialLightmapsPhong3d_GridHeightmapDisplacement::changeShaders(const UString& vertexSourceFileName, const UString& fragmentSourceFileName, const UString& geometrySourceFileName)
{
	shader3d.destroy();
	shader3d.createFromFiles(vertexSourceFileName, fragmentSourceFileName, geometrySourceFileName);
	shader3d.linkProgram();
}


void MaterialLightmapsPhong3d_GridHeightmapDisplacement::update(const FrameTimestamp& time)
{
	(void)time;
}


void MaterialLightmapsPhong3d_GridHeightmapDisplacement::apply(
		IWindowOpengl& window,
		const FrameTimestamp& time, const ICamera& camera,
		const std::vector<DirectionalLight>& directionalLights,
		const std::vector<PointLight>& pointLights,
		const std::vector<SpotLight>& spotLights)
{
	apply(window, time, camera.getViewMatrix(), camera.getProjectionMatrix(), camera.getPosition(),
		  directionalLights, pointLights, spotLights);
}


void MaterialLightmapsPhong3d_GridHeightmapDisplacement::apply(
		IWindowOpengl& window,
		const FrameTimestamp& time,
		const Matrix4x4f& viewMatrix, const Matrix4x4f& projectionMatrix, const Vector3f& position,
		const std::vector<DirectionalLight>& directionalLights,
		const std::vector<PointLight>& pointLights,
		const std::vector<SpotLight>& spotLights)
{
	(void)time;

	VERSO_ASSERT("verso-3d", pointLights.size()>0 && "You must provide at least one pointlight!");

	const int MAX_DIRECTIONAL_LIGHTS = 2; // These must match the shader!
	const int MAX_POINT_LIGHTS = 10;
	const int MAX_SPOT_LIGHTS = 10;

	shader3d.useProgram();

	// Vertex
	shader3d.setUniform("view", viewMatrix);
	shader3d.setUniform("projection", projectionMatrix);

	// Fragment
	shader3d.setUniform("viewPos", position);

	// - material
	window.setActiveTextureUnit(0);
	shader3d.setUniformi("material.diffuseTexture", 0);
	diffuseTexture.bind();

	window.setActiveTextureUnit(1);
	shader3d.setUniformi("material.specularTexture", 1);
	specularTexture.bind();

	shader3d.setUniformf("material.shininess", phongMaterial.shininess);

	// heightmap
	window.setActiveTextureUnit(2);
	shader3d.setUniformi("heightmap.texture", 2);
	heightmapTexture.bind();

	window.setActiveTextureUnit(0);

	shader3d.setUniformf("heightmap.maxHeight", maxHeight);

	// - directionalLights
	int directionalLightsCount = Math::minValue<int>(static_cast<int>(directionalLights.size()), MAX_DIRECTIONAL_LIGHTS);
	shader3d.setUniformi("directionalLightsCount", directionalLightsCount);

	for (int i=0; i<directionalLightsCount; ++i) {
		UString variable("directionalLights[");
		variable.append2(i);
		variable += "]";

		shader3d.setUniform(variable+".direction", directionalLights[i].direction);

		shader3d.setUniformRgb(variable+".ambient", directionalLights[i].ambient);
		shader3d.setUniformRgb(variable+".diffuse", directionalLights[i].diffuse);
		shader3d.setUniformRgb(variable+".specular", directionalLights[i].specular);
	}

	// - pointLights
	int pointLightsCount = Math::minValue<int>(static_cast<int>(pointLights.size()), MAX_POINT_LIGHTS);
	shader3d.setUniformi("pointLightsCount", pointLightsCount);

	for (int i=0; i<pointLightsCount; ++i) {
		UString variable("pointLights[");
		variable.append2(i);
		variable += "]";

		shader3d.setUniform(variable+".position", pointLights[i].position);

		shader3d.setUniformf(variable+".quadratic", pointLights[i].attenuation.constant);
		shader3d.setUniformf(variable+".linear", pointLights[i].attenuation.linear);
		shader3d.setUniformf(variable+".quadratic", pointLights[i].attenuation.quadratic);

		shader3d.setUniformRgb(variable+".ambient", pointLights[i].ambient);
		shader3d.setUniformRgb(variable+".diffuse", pointLights[i].diffuse);
		shader3d.setUniformRgb(variable+".specular", pointLights[i].specular);
	}

	// - spotLights
	int spotLightsCount = Math::minValue<int>(static_cast<int>(spotLights.size()), MAX_SPOT_LIGHTS);
	shader3d.setUniformi("spotLightsCount", spotLightsCount);

	for (int i=0; i<spotLightsCount; ++i) {
		UString variable("spotLights[");
		variable.append2(i);
		variable += "]";

		shader3d.setUniform(variable+".position", spotLights[i].position);
		shader3d.setUniform(variable+".direction", spotLights[i].direction);

		shader3d.setUniformf(variable+".cutOff", spotLights[i].cutOff);
		shader3d.setUniformf(variable+".outerCutOff", spotLights[i].outerCutOff);

		shader3d.setUniformf(variable+".constant", spotLights[i].attenuation.constant);
		shader3d.setUniformf(variable+".linear", spotLights[i].attenuation.linear);
		shader3d.setUniformf(variable+".quadratic", spotLights[i].attenuation.quadratic);

		shader3d.setUniformRgb(variable+".ambient", spotLights[i].ambient);
		shader3d.setUniformRgb(variable+".diffuse", spotLights[i].diffuse);
		shader3d.setUniformRgb(variable+".specular", spotLights[i].specular);
	}
}


void MaterialLightmapsPhong3d_GridHeightmapDisplacement::updateModelMatrix(const Matrix4x4f& model)
{
	shader3d.setUniform("model", model);
}


UString MaterialLightmapsPhong3d_GridHeightmapDisplacement::toString(const UString& newLinePadding) const
{
	UString str("{ ");
	UString newLinePadding2 = newLinePadding + "  ";
	str += "diffuseTexture=";
	str += diffuseTexture.toString(newLinePadding2);
	str += ", specularTexture=";
	str += specularTexture.toString(newLinePadding2);
	str += ", heightmapTexture=";
	str += heightmapTexture.toString(newLinePadding2);
	str += ", maxHeight=";
	str.append2(maxHeight);
	str += ", phongMaterial=";
	str += phongMaterial.toString(newLinePadding2);
	str += ", shader3d=";
	str += shader3d.toString(newLinePadding2);
	str += " }";
	return str;
}


UString MaterialLightmapsPhong3d_GridHeightmapDisplacement::toStringDebug(const UString& newLinePadding) const
{
	UString str("MaterialLightmapsPhong3d_GridHeightmapDisplacement(");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso

