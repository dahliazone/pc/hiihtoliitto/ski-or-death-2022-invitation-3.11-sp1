#include <Verso/Render/Material/MaterialTexture3d.hpp>
#include <Verso/Render/Render.hpp>

namespace Verso {


MaterialTexture3d::MaterialTexture3d() :
	created(false),
	diffuseTexture(),
	shader3d()
{
}


MaterialTexture3d::MaterialTexture3d(MaterialTexture3d&& original) :
	created(std::move(original.created)),
	diffuseTexture(std::move(original.diffuseTexture)),
	shader3d(std::move(original.shader3d))
{
}


MaterialTexture3d& MaterialTexture3d::operator =(MaterialTexture3d&& original)
{
	if (this != &original) {
		created = std::move(original.created);
		diffuseTexture = std::move(original.diffuseTexture);
		shader3d = std::move(original.shader3d);
	}
	return *this;
}


MaterialTexture3d::~MaterialTexture3d()
{
	destroy();
}


///////////////////////////////////////////////////////////////////////////////////////////
// Interface IMaterial
///////////////////////////////////////////////////////////////////////////////////////////

void MaterialTexture3d::create(
		IWindowOpengl& window,
		const UString& diffuseTextureFileName,
		const UString& vertexSourceFileName,
		const UString& fragmentSourceFileName,
		const UString& geometrySourceFileName)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == false, "Already created!", diffuseTexture.getSourceFileName().c_str());

	// Load diffuse texture
	diffuseTexture.createFromFile(
				window, diffuseTextureFileName,
				TextureParameters("material.diffuseTexture", TexturePixelFormat::Rgb));

	// Load material shader
	Render::quad3dVao.bind();
	{
		UString tmpVertexSourceFileName(Render::shadersVersoPath+"render/material/texture3d.330.vert");
		if (vertexSourceFileName.size() != 0) {
			tmpVertexSourceFileName.set(vertexSourceFileName);
		}

		UString tmpFragmentSourceFileName(Render::shadersVersoPath+"render/material/texture3d.330.frag");
		if (fragmentSourceFileName.size() != 0) {
			tmpFragmentSourceFileName.set(fragmentSourceFileName);
		}

		UString tmpGeometrySourceFileName;
		if (geometrySourceFileName.size() != 0) {
			tmpGeometrySourceFileName.set(geometrySourceFileName);
		}

		if (tmpGeometrySourceFileName.size() == 0) {
			shader3d.createFromFiles(tmpVertexSourceFileName, tmpFragmentSourceFileName);
		} else {
			shader3d.createFromFiles(tmpVertexSourceFileName, tmpFragmentSourceFileName, tmpGeometrySourceFileName);
		}

		shader3d.linkProgram();
	}
	Render::quad3dVao.unbind();

	created = true;
}


void MaterialTexture3d::destroy() VERSO_NOEXCEPT
{
	if (isCreated() == false) {
		return;
	}

	diffuseTexture.destroy();
	shader3d.destroy();
	created = false;
}


bool MaterialTexture3d::isCreated() const
{
	return created;
}


void MaterialTexture3d::changeShaders(const UString& vertexSourceFileName, const UString& fragmentSourceFileName)
{
	shader3d.destroy();
	shader3d.createFromFiles(vertexSourceFileName, fragmentSourceFileName);
	shader3d.linkProgram();
}


void MaterialTexture3d::changeShaders(const UString& vertexSourceFileName, const UString& fragmentSourceFileName, const UString& geometrySourceFileName)
{
	shader3d.destroy();
	shader3d.createFromFiles(vertexSourceFileName, fragmentSourceFileName, geometrySourceFileName);
	shader3d.linkProgram();
}


void MaterialTexture3d::update(const FrameTimestamp& time)
{
	(void)time;
}


void MaterialTexture3d::apply(
		IWindowOpengl& window,
		const FrameTimestamp& time, const ICamera& camera,
		const std::vector<DirectionalLight>& directionalLights,
		const std::vector<PointLight>& pointLights,
		const std::vector<SpotLight>& spotLights)
{
	apply(window, time, camera.getViewMatrix(), camera.getProjectionMatrix(), camera.getPosition(),
		  directionalLights, pointLights, spotLights);
}


void MaterialTexture3d::apply(
		IWindowOpengl& window,
		const FrameTimestamp& time,
		const Matrix4x4f& viewMatrix, const Matrix4x4f& projectionMatrix, const Vector3f& position,
		const std::vector<DirectionalLight>& directionalLights,
		const std::vector<PointLight>& pointLights,
		const std::vector<SpotLight>& spotLights)
{
	(void)time; (void)position; (void)directionalLights; (void)pointLights; (void)spotLights;

	shader3d.useProgram();

	// Vertex
	shader3d.setUniform("view", viewMatrix);
	shader3d.setUniform("projection", projectionMatrix);

	// Fragment
	// - material
	window.setActiveTextureUnit(0);
	shader3d.setUniformi("material.diffuseTexture", 0);
	diffuseTexture.bind();
}


void MaterialTexture3d::updateModelMatrix(const Matrix4x4f& model)
{
	// Vertex
	shader3d.setUniform("model", model);
}


UString MaterialTexture3d::toString(const UString& newLinePadding) const
{
	UString str("{ ");
	UString newLinePadding2 = newLinePadding + "  ";
	str += "diffuseTexture=";
	str += diffuseTexture.toString(newLinePadding2);
	str += ", shader3d=";
	str += shader3d.toString(newLinePadding2);
	str += " }";
	return str;
}


UString MaterialTexture3d::toStringDebug(const UString& newLinePadding) const
{
	UString str("MaterialTexture3d(");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


///////////////////////////////////////////////////////////////////////////////////////////
// getters & setters
///////////////////////////////////////////////////////////////////////////////////////////

const Texture& MaterialTexture3d::getDiffuseTexture() const
{
	return diffuseTexture;
}


Texture& MaterialTexture3d::getDiffuseTexture()
{
	return diffuseTexture;
}


} // End namespace Verso

