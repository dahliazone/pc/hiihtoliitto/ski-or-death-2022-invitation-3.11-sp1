#include <Verso/Render/Material/MaterialNormalVisualizer3d.hpp>
#include <Verso/Render/Render.hpp>

namespace Verso {


MaterialNormalVisualizer3d::MaterialNormalVisualizer3d() :
	created(false),
	normalLength(1.0f),
	normalColor(1.0f, 1.0f, 0.0f),
	shader3d()
{
}


MaterialNormalVisualizer3d::MaterialNormalVisualizer3d(MaterialNormalVisualizer3d&& original) :
	created(std::move(original.created)),
	normalLength(std::move(original.normalLength)),
	normalColor(std::move(original.normalColor)),
	shader3d(std::move(original.shader3d))
{
}


MaterialNormalVisualizer3d& MaterialNormalVisualizer3d::operator =(MaterialNormalVisualizer3d&& original)
{
	if (this != &original) {
		created = std::move(original.created);
		normalLength = std::move(original.normalLength);
		normalColor = std::move(original.normalColor);
		shader3d = std::move(original.shader3d);
	}
	return *this;
}


MaterialNormalVisualizer3d::~MaterialNormalVisualizer3d()
{
	destroy();
}


///////////////////////////////////////////////////////////////////////////////////////////
// Interface IMaterial
///////////////////////////////////////////////////////////////////////////////////////////

void MaterialNormalVisualizer3d::create(
		IWindowOpengl& window,
		float normalLength, const RgbaColorf& normalColor,
		const UString& vertexSourceFileName,
		const UString& fragmentSourceFileName,
		const UString& geometrySourceFileName)
{
	(void)window;
	VERSO_ASSERT_MSG("verso-3d", isCreated() == false, "Already created!");

	this->normalLength = normalLength;
	this->normalColor = normalColor;

	// Load material shader
	Render::quad3dVao.bind();
	{
		UString tmpVertexSourceFileName(Render::shadersVersoPath+"render/material/normalvisualizer3d.330.vert");
		if (vertexSourceFileName.size() != 0) {
			tmpVertexSourceFileName.set(vertexSourceFileName);
		}

		UString tmpFragmentSourceFileName(Render::shadersVersoPath+"render/material/normalvisualizer3d.330.frag");
		if (fragmentSourceFileName.size() != 0) {
			tmpFragmentSourceFileName.set(fragmentSourceFileName);
		}

		UString tmpGeometrySourceFileName(Render::shadersVersoPath+"render/material/normalvisualizer3d.330.geom");
		if (geometrySourceFileName.size() != 0) {
			tmpGeometrySourceFileName.set(geometrySourceFileName);
		}

		if (tmpGeometrySourceFileName.size() == 0) {
			shader3d.createFromFiles(tmpVertexSourceFileName, tmpFragmentSourceFileName);
		} else {
			shader3d.createFromFiles(tmpVertexSourceFileName, tmpFragmentSourceFileName, tmpGeometrySourceFileName);
		}

		shader3d.linkProgram();
	}
	Render::quad3dVao.unbind();

	created = true;
}


void MaterialNormalVisualizer3d::destroy() VERSO_NOEXCEPT
{
	if (isCreated() == false) {
		return;
	}

	shader3d.destroy();
	created = false;
}


bool MaterialNormalVisualizer3d::isCreated() const
{
	return created;
}


void MaterialNormalVisualizer3d::changeShaders(const UString& vertexSourceFileName, const UString& fragmentSourceFileName)
{
	shader3d.destroy();
	shader3d.createFromFiles(vertexSourceFileName, fragmentSourceFileName);
	shader3d.linkProgram();
}


void MaterialNormalVisualizer3d::changeShaders(const UString& vertexSourceFileName, const UString& fragmentSourceFileName, const UString& geometrySourceFileName)
{
	shader3d.destroy();
	shader3d.createFromFiles(vertexSourceFileName, fragmentSourceFileName, geometrySourceFileName);
	shader3d.linkProgram();
}


void MaterialNormalVisualizer3d::update(const FrameTimestamp& time)
{
	(void)time;
}


void MaterialNormalVisualizer3d::apply(
		IWindowOpengl& window,
		const FrameTimestamp& time, const ICamera& camera,
		const std::vector<DirectionalLight>& directionalLights,
		const std::vector<PointLight>& pointLights,
		const std::vector<SpotLight>& spotLights)
{
	apply(window, time, camera.getViewMatrix(), camera.getProjectionMatrix(), camera.getPosition(),
		  directionalLights, pointLights, spotLights);
}


void MaterialNormalVisualizer3d::apply(
		IWindowOpengl& window,
		const FrameTimestamp& time,
		const Matrix4x4f& viewMatrix, const Matrix4x4f& projectionMatrix, const Vector3f& position,
		const std::vector<DirectionalLight>& directionalLights,
		const std::vector<PointLight>& pointLights,
		const std::vector<SpotLight>& spotLights)
{
	(void)window; (void)time; (void)position; (void)directionalLights; (void)pointLights; (void)spotLights;

	shader3d.useProgram();

	// Vertex
	shader3d.setUniform("view", viewMatrix);
	shader3d.setUniform("projection", projectionMatrix);
	shader3d.setUniformf("normalLength", normalLength);

	// Fragment
	shader3d.setUniformRgb("normalColor", normalColor);
}


void MaterialNormalVisualizer3d::updateModelMatrix(const Matrix4x4f& model)
{
	shader3d.setUniform("model", model);
}


UString MaterialNormalVisualizer3d::toString(const UString& newLinePadding) const
{
	UString str("{ ");
	UString newLinePadding2 = newLinePadding + "  ";
	str += "normalLength=";
	str.append2(normalLength);
	str += "normalColor=";
	str += normalColor.toString();
	str += ", shader3d=";
	str += shader3d.toString(newLinePadding2);
	str += " }";
	return str;
}


UString MaterialNormalVisualizer3d::toStringDebug(const UString& newLinePadding) const
{
	UString str("MaterialNormalVisualizer3d(");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso

