#include <Verso/Render/Material/MaterialColor3d.hpp>
#include <Verso/Render/Render.hpp>

namespace Verso {


MaterialColor3d::MaterialColor3d() :
	created(false),
	color(1.0f, 1.0f, 1.0f, 1.0f),
	shader3d()
{
}


MaterialColor3d::MaterialColor3d(MaterialColor3d&& original) :
	created(std::move(original.created)),
	color(std::move(original.color)),
	shader3d(std::move(original.shader3d))
{
}


MaterialColor3d& MaterialColor3d::operator =(MaterialColor3d&& original)
{
	if (this != &original) {
		created = std::move(original.created);
		color = std::move(original.color);
		shader3d = std::move(original.shader3d);
	}
	return *this;
}


MaterialColor3d::~MaterialColor3d()
{
	destroy();
}


///////////////////////////////////////////////////////////////////////////////////////////
// Interface IMaterial
///////////////////////////////////////////////////////////////////////////////////////////

void MaterialColor3d::create(
		IWindowOpengl& window,
		const RgbaColorf& color,
		const UString& vertexSourceFileName,
		const UString& fragmentSourceFileName,
		const UString& geometrySourceFileName)
{
	(void)window;
	VERSO_ASSERT_MSG("verso-3d", isCreated() == false, "Already created!");

	this->color = color;

	// Load material shader
	Render::quad3dVao.bind();
	{
		UString tmpVertexSourceFileName(Render::shadersVersoPath+"render/material/color3d.330.vert");
		if (vertexSourceFileName.size() != 0) {
			tmpVertexSourceFileName.set(vertexSourceFileName);
		}

		UString tmpFragmentSourceFileName(Render::shadersVersoPath+"render/material/color3d.330.frag");
		if (fragmentSourceFileName.size() != 0) {
			tmpFragmentSourceFileName.set(fragmentSourceFileName);
		}

		UString tmpGeometrySourceFileName;
		if (geometrySourceFileName.size() != 0) {
			tmpGeometrySourceFileName.set(geometrySourceFileName);
		}

		if (tmpGeometrySourceFileName.size() == 0) {
			shader3d.createFromFiles(tmpVertexSourceFileName, tmpFragmentSourceFileName);
		}
		else {
			shader3d.createFromFiles(tmpVertexSourceFileName, tmpFragmentSourceFileName, tmpGeometrySourceFileName);
		}

		shader3d.linkProgram();
	}
	Render::quad3dVao.unbind();

	created = true;
}


void MaterialColor3d::destroy() VERSO_NOEXCEPT
{
	if (isCreated() == false) {
		return;
	}

	shader3d.destroy();
	created = false;
}


bool MaterialColor3d::isCreated() const
{
	return created;
}


void MaterialColor3d::changeShaders(const UString& vertexSourceFileName, const UString& fragmentSourceFileName)
{
	shader3d.destroy();
	shader3d.createFromFiles(vertexSourceFileName, fragmentSourceFileName);
	shader3d.linkProgram();
}


void MaterialColor3d::changeShaders(const UString& vertexSourceFileName, const UString& fragmentSourceFileName, const UString& geometrySourceFileName)
{
	shader3d.destroy();
	shader3d.createFromFiles(vertexSourceFileName, fragmentSourceFileName, geometrySourceFileName);
	shader3d.linkProgram();
}


void MaterialColor3d::update(const FrameTimestamp& time)
{
	(void)time;
}


void MaterialColor3d::apply(
		IWindowOpengl& window,
		const FrameTimestamp& time, const ICamera& camera,
		const std::vector<DirectionalLight>& directionalLights,
		const std::vector<PointLight>& pointLights,
		const std::vector<SpotLight>& spotLights)
{
	apply(window, time, camera.getViewMatrix(), camera.getProjectionMatrix(), camera.getPosition(),
		  directionalLights, pointLights, spotLights);
}


void MaterialColor3d::apply(
		IWindowOpengl& window,
		const FrameTimestamp& time,
		const Matrix4x4f& viewMatrix, const Matrix4x4f& projectionMatrix, const Vector3f& position,
		const std::vector<DirectionalLight>& directionalLights,
		const std::vector<PointLight>& pointLights,
		const std::vector<SpotLight>& spotLights)
{
	(void)window; (void)time; (void)position; (void)directionalLights; (void)pointLights; (void)spotLights;

	shader3d.useProgram();

	// Vertex
	shader3d.setUniform("view", viewMatrix);
	shader3d.setUniform("projection", projectionMatrix);

	// Fragment
	shader3d.setUniformRgb("material.color", color);
}


void MaterialColor3d::updateModelMatrix(const Matrix4x4f& model)
{
	shader3d.setUniform("model", model);
}


UString MaterialColor3d::toString(const UString& newLinePadding) const
{
	UString str("{ ");
	UString newLinePadding2 = newLinePadding + "  ";
	str += "color=";
	str += color.toString();
	str += ", shader3d=";
	str += shader3d.toString(newLinePadding2);
	str += " }";
	return str;
}


UString MaterialColor3d::toStringDebug(const UString& newLinePadding) const
{
	UString str("MaterialColor3d(");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso

