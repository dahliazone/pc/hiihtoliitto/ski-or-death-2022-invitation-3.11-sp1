#include <Verso/Render/ShaderProgram.hpp>
#include <Verso/System/Exception.hpp>

namespace Verso {


ShaderProgram::ShaderProgram() :
	created(false),
	handle(0),
	vertex(),
	fragment(),
	geometry(),
	sourceFileNames(),
	attributes(),
	uniforms(),
	linked(false),
	debugger(),
	sourceFileNamesCStr(nullptr)
{
}


ShaderProgram::ShaderProgram(ShaderProgram&& original) :
	created(std::move(original.created)),
	handle(std::move(original.handle)),
	vertex(std::move(original.vertex)),
	fragment(std::move(original.fragment)),
	geometry(std::move(original.geometry)),
	sourceFileNames(std::move(original.sourceFileNames)),
	attributes(std::move(original.attributes)),
	uniforms(std::move(original.uniforms)),
	linked(std::move(original.linked)),
	debugger(std::move(original.debugger)),
	sourceFileNamesCStr(std::move(original.sourceFileNamesCStr))
{
	original.created = false;
	original.handle = 0;
	original.sourceFileNames.clear();
	original.attributes.clear();
	original.uniforms.clear();
	original.linked = false;
	original.sourceFileNamesCStr = nullptr;
}

ShaderProgram& ShaderProgram::operator =(ShaderProgram&& original)
{
	if (this != &original) {
		created = std::move(original.created);
		handle = std::move(original.handle);
		vertex = std::move(original.vertex);
		fragment = std::move(original.fragment);
		geometry = std::move(original.geometry);
		sourceFileNames = std::move(original.sourceFileNames);
		attributes = std::move(original.attributes);
		uniforms = std::move(original.uniforms);
		linked = std::move(original.linked);
		debugger = std::move(original.debugger);
		sourceFileNamesCStr = std::move(original.sourceFileNamesCStr);

		original.created = false;
		original.handle = 0;
		original.sourceFileNames.clear();
		original.attributes.clear();
		original.uniforms.clear();
		original.linked = false;
		original.sourceFileNamesCStr = nullptr;
	}
	return *this;
}


ShaderProgram::~ShaderProgram()
{
	destroy();
}


void ShaderProgram::createFromFiles(const UString& vertexSourceFileName, const UString& fragmentSourceFileName, bool bindDefaultAttributes)
{
	VERSO_ASSERT_MSG("verso-3d", isCreated() == false, "Already created!");

	if (!Context::isContextCreated()) {
		UString resources = "\""+vertexSourceFileName+"\", \""+fragmentSourceFileName+"\"";
		VERSO_ERROR("verso-3d", "No OpenGL context created!", resources.c_str());
	}

	if (vertexSourceFileName.isEmpty()) {
		UString resources = "\""+vertexSourceFileName+"\", \""+fragmentSourceFileName+"\"";
		VERSO_ILLEGALPARAMETERS("verso-3d", "vertexSourceFileName is empty string!", resources.c_str());
	}

	if (fragmentSourceFileName.isEmpty()) {
		UString resources = "\""+vertexSourceFileName+"\", \""+fragmentSourceFileName+"\"";
		VERSO_ILLEGALPARAMETERS("verso-3d", "fragmentSourceFilename is empty string!", resources.c_str());
	}

	vertex.createFromFile(ShaderType::Vertex, vertexSourceFileName);
	fragment.createFromFile(ShaderType::Fragment, fragmentSourceFileName);

	sourceFileNames.clear();
	sourceFileNames.push_back(vertexSourceFileName);
	sourceFileNames.push_back(fragmentSourceFileName);

	createProgram(false, bindDefaultAttributes);
}


void ShaderProgram::createFromFiles(const UString& vertexSourceFileName, const UString& fragmentSourceFileName, const UString& geometrySourceFileName, bool bindDefaultAttributes)
{
	VERSO_ASSERT_MSG("verso-3d", isCreated() == false, "Already created!");

	if (!Context::isContextCreated()) {
		UString resources = "\""+vertexSourceFileName+"\", \""+fragmentSourceFileName+"\", \""+geometrySourceFileName+"\"";
		VERSO_ERROR("verso-3d", "No OpenGL context created!", resources.c_str());
	}

	if (vertexSourceFileName.isEmpty()) {
		UString resources = "\""+vertexSourceFileName+"\", \""+fragmentSourceFileName+"\", \""+geometrySourceFileName+"\"";
		VERSO_ILLEGALPARAMETERS("verso-3d", "vertexSourceFileName is empty string!", resources.c_str());
	}

	if (fragmentSourceFileName.isEmpty()) {
		UString resources = "\""+vertexSourceFileName+"\", \""+fragmentSourceFileName+"\", \""+geometrySourceFileName+"\"";
		VERSO_ILLEGALPARAMETERS("verso-3d", "fragmentSourceFilename is empty string!", resources.c_str());
	}

	if (geometrySourceFileName.isEmpty()) {
		UString resources = "\""+vertexSourceFileName+"\", \""+fragmentSourceFileName+"\", \""+geometrySourceFileName+"\"";
		VERSO_ILLEGALPARAMETERS("verso-3d", "geometrySourceFilename is empty string!", resources.c_str());
	}

	vertex.createFromFile(ShaderType::Vertex, vertexSourceFileName);
	fragment.createFromFile(ShaderType::Fragment, fragmentSourceFileName);
	geometry.createFromFile(ShaderType::Geometry, geometrySourceFileName);

	sourceFileNames.clear();
	sourceFileNames.push_back(vertexSourceFileName);
	sourceFileNames.push_back(fragmentSourceFileName);
	sourceFileNames.push_back(geometrySourceFileName);

	createProgram(true, bindDefaultAttributes);
	created = true;
}


void ShaderProgram::createFromStrings(const UString& vertexSource, const UString& fragmentSource, bool bindDefaultAttributes)
{
	VERSO_ASSERT_MSG("verso-3d", isCreated() == false, "Already created!");

	if (!Context::isContextCreated()) {
		UString resources = "\""+vertexSource+"\", \""+fragmentSource+"\"";
		VERSO_ERROR("verso-3d", "No OpenGL context created!", resources.c_str());
	}

	if (vertexSource.isEmpty()) {
		UString resources = "\""+vertexSource+"\", \""+fragmentSource+"\"";
		VERSO_ILLEGALPARAMETERS("verso-3d", "vertexSource is empty string!", resources.c_str());
	}

	if (fragmentSource.isEmpty()) {
		UString resources = "\""+vertexSource+"\", \""+fragmentSource+"\"";
		VERSO_ILLEGALPARAMETERS("verso-3d", "fragmentSource is empty string!", resources.c_str());
	}

	vertex.createFromString(ShaderType::Vertex, vertexSource);
	fragment.createFromString(ShaderType::Fragment, fragmentSource);

	createProgram(false, bindDefaultAttributes);
	created = true;
}


void ShaderProgram::createFromStrings(const UString& vertexSource, const UString& fragmentSource, const UString& geometrySource, bool bindDefaultAttributes)
{
	VERSO_ASSERT_MSG("verso-3d", isCreated() == false, "Already created!");

	if (!Context::isContextCreated()) {
		UString resources = "\""+vertexSource+"\", \""+fragmentSource+"\", \""+geometrySource+"\"";
		VERSO_ERROR("verso-3d", "No OpenGL context created!", resources.c_str());
	}

	if (vertexSource.isEmpty()) {
		UString resources = "\""+vertexSource+"\", \""+fragmentSource+"\", \""+geometrySource+"\"";
		VERSO_ILLEGALPARAMETERS("verso-3d", "vertexSource is empty string!", resources.c_str());
	}

	if (fragmentSource.isEmpty()) {
		UString resources = "\""+vertexSource+"\", \""+fragmentSource+"\", \""+geometrySource+"\"";
		VERSO_ILLEGALPARAMETERS("verso-3d", "fragmentSource is empty string!", resources.c_str());
	}

	if (geometrySource.isEmpty()) {
		UString resources = "\""+vertexSource+"\", \""+fragmentSource+"\", \""+geometrySource+"\"";
		VERSO_ILLEGALPARAMETERS("verso-3d", "geometrySource is empty string!", resources.c_str());
	}

	vertex.createFromString(ShaderType::Vertex, vertexSource);
	fragment.createFromString(ShaderType::Fragment, fragmentSource);
	geometry.createFromString(ShaderType::Geometry, geometrySource);

	createProgram(true, bindDefaultAttributes);
	created = true;
}


void ShaderProgram::destroy() VERSO_NOEXCEPT
{
	if (isCreated() == false) {
		return;
	}

	if (Context::isContextCreated()) {
		glUseProgram(0);
		GL_CHECK_FOR_ERRORS("", "");

		if (isCreated()) {
			glDeleteProgram(handle);
			GL_CHECK_FOR_ERRORS("", "");
			created = false;
			handle = 0;
			sourceFileNames.clear();
			attributes.clear();
			uniforms.clear();
			linked = false;
			vertex.destroy();
			fragment.destroy();
			geometry.destroy();
		}
	}

	if (sourceFileNamesCStr != nullptr) {
		delete[] sourceFileNamesCStr;
	}
}


bool ShaderProgram::isCreated() const
{
	return created;
}


// Note: Attribute locations must be bound before calling link().
void ShaderProgram::bindAttribLocation(const ShaderAttribute& attribute)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Shader program has not been created!", sourceFileNamesToCString());
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == false, "ShaderProgram already linked!", sourceFileNamesToCString());

	glBindAttribLocation(handle, attribute.getIndex(), attribute.getName().c_str());
	GL_CHECK_FOR_ERRORS("", "");
	while (attributes.size() < attribute.getIndex()+1) {
		attributes.push_back(ShaderAttribute());
	}
	attributes[attribute.getIndex()] = attribute;
}


// \TODO: better interface for this?
// Note: Frag data locations must be bound before calling link().
void ShaderProgram::bindFragDataLocation(GLuint location, const UString& name)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	glBindFragDataLocation(handle, location, name.c_str());
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


// Must be called after create*() and bindAttribLocation() and before useProgram().
void ShaderProgram::linkProgram()
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "Shader program has not been created!", sourceFileNamesToCString());

	// Link our program
	// At this stage, the vertex, fragment and possibly geometry programs are inspected, optimized and a binary code is generated for the shader.
	// The binary code is uploaded to the GPU, if there is no error.
	glLinkProgram(handle);
	GL_CHECK_FOR_ERRORS("", "");

	// Again, we must check and make sure that it linked. If it fails, it would mean either there is a mismatch between the vertex / fragment / geometry
	// shaders. It might be that you have surpassed your GPU's abilities. Perhaps too many ALU operations or
	// too many texel fetch instructions or too many interpolators or dynamic loops.
	int linkStatus;
	glGetProgramiv(handle, GL_LINK_STATUS, &linkStatus);
	GL_CHECK_FOR_ERRORS("", "");
	if (linkStatus == GL_FALSE) {
		int maxLength;
		glGetProgramiv(handle, GL_INFO_LOG_LENGTH, &maxLength);
		GL_CHECK_FOR_ERRORS("", "");

		// The maxLength includes the NULL character
		char* infoLog = new char[static_cast<size_t>(maxLength)];
		glGetProgramInfoLog(handle, maxLength, &maxLength, infoLog);
		GL_CHECK_FOR_ERRORS("", "");

		UString error("Could not link Shader program. Infolog: \"");
		error += infoLog;
		error += "\"";
		delete[] infoLog;
		VERSO_ERROR("verso-3d", error.c_str(), sourceFileNamesToCString());
	}

	validateProgram();

	// Once the shader program has the shaders attached and linked, the shaders are no longer required.
	// If the linking failed, then we're going to abort anyway so we still detach the shaders.
	glDetachShader(handle, vertex.getHandle());
	GL_CHECK_FOR_ERRORS("", "");
	glDetachShader(handle, fragment.getHandle());
	GL_CHECK_FOR_ERRORS("", "");
	if (geometry.isCreated()) {
		glDetachShader(handle, geometry.getHandle());
		GL_CHECK_FOR_ERRORS("", "");
	}

	// clear uniform location ids (linking can change them)
	uniforms.clear();

	linked = true;
}


void ShaderProgram::useProgram()
{
	if (isCreated() == false) {
		VERSO_ERROR("verso-3d", "Shader program has not been created!", sourceFileNamesToCString());
	}
	if (linked == false) {
		VERSO_ERROR("verso-3d", "Shader program is not yet linked!", sourceFileNamesToCString());
	}

	glUseProgram(handle);
	GL_CHECK_FOR_ERRORS("", "");
}


void ShaderProgram::setUniformi(const UString& name, GLint x, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	debugger.saveUniformType(name, ShaderUniformType::Int);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}

	glUniform1i(uniformHandle, x);

	// \TODO: Fix this error in some way
	//UString potentialError("Could not set shader uniform for \"");
	//potentialError += name;
	//potentialError += "\" (handle=";
	//potentialError.append2(handle);
	//potentialError += ") (uniformLocation=";
	//potentialError.append2(uniformHandle);
	//potentialError += ")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniformu(const UString& name, GLuint x, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	debugger.saveUniformType(name, ShaderUniformType::Uint);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform1ui(uniformHandle, x);
	// \TODO: Fix this error in some way
	//UString potentialError("Could not set shader uniform for \"");
	//potentialError += name;
	//potentialError += "\" (handle=";
	//potentialError.append2(handle);
	//potentialError += ") (uniformLocation=";
	//potentialError.append2(uniformHandle);
	//potentialError += ")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniformf(const UString& name, GLfloat x, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	debugger.saveUniformType(name, ShaderUniformType::Float);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform1f(uniformHandle, x);
	// \TODO: Fix this error in some way
	//UString potentialError("Could not set shader uniform for \"");
	//potentialError += name;
	//potentialError += "\" (handle=";
	//potentialError.append2(handle);
	//potentialError += ") (uniformLocation=";
	//potentialError.append2(uniformHandle);
	//potentialError += ")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniformi(const UString& name, GLint x, GLint y, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	debugger.saveUniformType(name, ShaderUniformType::Vec2);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform2i(uniformHandle, x, y);
	// \TODO: Fix this error in some way
	//UString potentialError("Could not set shader uniform for \"");
	//potentialError += name;
	//potentialError += "\" (handle=";
	//potentialError.append2(handle);
	//potentialError += ") (uniformLocation=";
	//potentialError.append2(uniformHandle);
	//potentialError += ")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniformu(const UString& name, GLuint x, GLuint y, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	debugger.saveUniformType(name, ShaderUniformType::Uvec2);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform2ui(uniformHandle, x, y);
	// \TODO: Fix this error in some way
	//UString potentialError("Could not set shader uniform for \"");
	//potentialError += name;
	//potentialError += "\" (handle=";
	//potentialError.append2(handle);
	//potentialError += ") (uniformLocation=";
	//potentialError.append2(uniformHandle);
	//potentialError += ")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniformf(const UString& name, GLfloat x, GLfloat y, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	debugger.saveUniformType(name, ShaderUniformType::Vec2);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform2f(uniformHandle, x, y);
	// \TODO: Fix this error in some way
	//UString potentialError("Could not set shader uniform for \"");
	//potentialError += name;
	//potentialError += "\" (handle=";
	//potentialError.append2(handle);
	//potentialError += ") (uniformLocation=";
	//potentialError.append2(uniformHandle);
	//potentialError += ")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniformi(const UString& name, GLint x, GLint y, GLint z, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	debugger.saveUniformType(name, ShaderUniformType::Ivec3);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform3i(uniformHandle, x, y, z);
	// \TODO: Fix this error in some way
	//UString potentialError("Could not set shader uniform for \"");
	//potentialError += name;
	//potentialError += "\" (handle=";
	//potentialError.append2(handle);
	//potentialError += ") (uniformLocation=";
	//potentialError.append2(uniformHandle);
	//potentialError += ")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniformu(const UString& name, GLuint x, GLuint y, GLuint z, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	debugger.saveUniformType(name, ShaderUniformType::Uvec3);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform3ui(uniformHandle, x, y, z);
	// \TODO: Fix this error in some way
	//UString potentialError("Could not set shader uniform for \"");
	//potentialError += name;
	//potentialError += "\" (handle=";
	//potentialError.append2(handle);
	//potentialError += ") (uniformLocation=";
	//potentialError.append2(uniformHandle);
	//potentialError += ")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniformf(const UString& name, GLfloat x, GLfloat y, GLfloat z, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	debugger.saveUniformType(name, ShaderUniformType::Vec3);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform3f(uniformHandle, x, y, z);
	// \TODO: Fix this error in some way
	//UString potentialError("Could not set shader uniform for \"");
	//potentialError += name;
	//potentialError += "\" (handle=";
	//potentialError.append2(handle);
	//potentialError += ") (uniformLocation=";
	//potentialError.append2(uniformHandle);
	//potentialError += ")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniformi(const UString& name, GLint x, GLint y, GLint z, GLint w, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	debugger.saveUniformType(name, ShaderUniformType::Ivec4);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform4i(uniformHandle, x, y, z, w);
	// \TODO: Fix this error in some way
	//UString potentialError("Could not set shader uniform for \"");
	//potentialError += name;
	//potentialError += "\" (handle=";
	//potentialError.append2(handle);
	//potentialError += ") (uniformLocation=";
	//potentialError.append2(uniformHandle);
	//potentialError += ")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniformu(const UString& name, GLuint x, GLuint y, GLuint z, GLuint w, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	debugger.saveUniformType(name, ShaderUniformType::Uvec4);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform4ui(uniformHandle, x, y, z, w);
	// \TODO: Fix this error in some way
	//UString potentialError("Could not set shader uniform for \"");
	//potentialError += name;
	//potentialError += "\" (handle=";
	//potentialError.append2(handle);
	//potentialError += ") (uniformLocation=";
	//potentialError.append2(uniformHandle);
	//potentialError += ")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniformf(const UString& name, GLfloat x, GLfloat y, GLfloat z, GLfloat w, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	debugger.saveUniformType(name, ShaderUniformType::Vec4);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform4f(uniformHandle, x, y, z, w);
	// \TODO: Fix this error in some way
	//UString potentialError("Could not set shader uniform for \"");
	//potentialError += name;
	//potentialError += "\" (handle=";
	//potentialError.append2(handle);
	//potentialError += ") (uniformLocation=";
	//potentialError.append2(uniformHandle);
	//potentialError += ")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniform(const UString& name, const Vector2i& vec, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	debugger.saveUniformType(name, ShaderUniformType::Ivec2);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform2i(uniformHandle, vec.x, vec.y);
	// \TODO: Fix this error in some way
	//UString potentialError("Could not set shader uniform for \"");
	//potentialError += name;
	//potentialError += "\" (handle=";
	//potentialError.append2(handle);
	//potentialError += ") (uniformLocation=";
	//potentialError.append2(uniformHandle);
	//potentialError += ")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniform(const UString& name, const Vector2u& vec, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	debugger.saveUniformType(name, ShaderUniformType::Uvec2);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform2ui(uniformHandle, vec.x, vec.y);
	// \TODO: Fix this error in some way
	//UString potentialError("Could not set shader uniform for \"");
	//potentialError += name;
	//potentialError += "\" (handle=";
	//potentialError.append2(handle);
	//potentialError += ") (uniformLocation=";
	//potentialError.append2(uniformHandle);
	//potentialError += ")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniform(const UString& name, const Vector2f& vec, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	debugger.saveUniformType(name, ShaderUniformType::Vec2);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform2f(uniformHandle, vec.x, vec.y);
	// \TODO: Fix this error in some way
	//UString potentialError("In function: void setUniform(const UString& name=");
	//potentialError += name;
	//potentialError += ", const Vector2f& vec=";
	//potentialError += vec.toString();
	//potentialError += ")\n\n";
	//potentialError += "Message: Could not set shader uniform for \""+name+"\" (handle="+handle+") (uniformLocation="+uniformHandle+")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniform(const UString& name, const Vector3i& vec, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	debugger.saveUniformType(name, ShaderUniformType::Ivec3);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform3i(uniformHandle, vec.x, vec.y, vec.z);
	// \TODO: Fix this error in some way
	//UString potentialError("In function: void setUniform(const UString& name=");
	//potentialError += name;
	//potentialError += ", const Vector3i& vec=";
	//potentialError += vec.toString();
	//potentialError += ")\n\n";
	//potentialError += "Message: Could not set shader uniform for \""+name+"\" (handle="+handle+") (uniformLocation="+uniformHandle+")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniform(const UString& name, const Vector3u& vec, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	debugger.saveUniformType(name, ShaderUniformType::Uvec3);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform3ui(uniformHandle, vec.x, vec.y, vec.z);
	// \TODO: Fix this error in some way
	//UString potentialError("In function: void setUniform(const UString& name=");
	//potentialError += name;
	//potentialError += ", const Vector3u& vec=";
	//potentialError += vec.toString();
	//potentialError += ")\n\n";
	//potentialError += "Message: Could not set shader uniform for \""+name+"\" (handle="+handle+") (uniformLocation="+uniformHandle+")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniform(const UString& name, const Vector3f& vec, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	debugger.saveUniformType(name, ShaderUniformType::Vec3);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform3f(uniformHandle, vec.x, vec.y, vec.z);
	// \TODO: Fix this error message in some way
	//UString potentialError("In function: void setUniform(const UString& name=");
	//potentialError += name;
	//potentialError += ", const Vector3f& vec=";
	//potentialError += vec.toString();
	//potentialError += ")\n\n";
	//potentialError += "Message: Could not set shader uniform for \""+name+"\" (handle="+handle+") (uniformLocation="+uniformHandle+")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniform(const UString& name, const Vector4i& vec, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	debugger.saveUniformType(name, ShaderUniformType::Ivec4);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform4i(uniformHandle, vec.x, vec.y, vec.z, vec.w);
	// \TODO: Fix this error in some way
	//UString potentialError("In function: void setUniform(const UString& name=");
	//potentialError += name;
	//potentialError += ", const Vector4i& vec=";
	//potentialError += vec.toString();
	//potentialError += ")\n\n";
	//potentialError += "Message: Could not set shader uniform for \""+name+"\" (handle="+handle+") (uniformLocation="+uniformHandle+")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniform(const UString& name, const Vector4u& vec, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	debugger.saveUniformType(name, ShaderUniformType::Uvec4);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform4ui(uniformHandle, vec.x, vec.y, vec.z, vec.w);
	// \TODO: Fix this error in some way
	//UString potentialError("In function: void setUniform(const UString& name=");
	//potentialError += name;
	//potentialError += ", const Vector4u& vec=";
	//potentialError += vec.toString();
	//potentialError += ")\n\n";
	//potentialError += "Message: Could not set shader uniform for \""+name+"\" (handle="+handle+") (uniformLocation="+uniformHandle+")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniform(const UString& name, const Vector4f& vec, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	debugger.saveUniformType(name, ShaderUniformType::Vec4);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform4f(uniformHandle, vec.x, vec.y, vec.z, vec.w);
	// \TODO: Fix this error in some way
	//UString potentialError("In function: void setUniform(const UString& name=");
	//potentialError += name;
	//potentialError += ", const Vector4f& vec=";
	//potentialError += vec.toString();
	//potentialError += ")\n\n";
	//potentialError += "Message: Could not set shader uniform for \""+name+"\" (handle="+handle+") (uniformLocation="+uniformHandle+")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniform(const UString& name, const Matrix4x4f& mat, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	debugger.saveUniformType(name, ShaderUniformType::Mat4);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}

	glUniformMatrix4fv(uniformHandle, 1, true, reinterpret_cast<const float*>(mat.data));
	// \TODO: Fix this in some way
	//UString potentialError("In function: void setUniform(const UString& name=");
	//potentialError += name;
	//potentialError += ", Matrix4x4f mat)\n";
	//potentialError += mat.toString();
	//potentialError += ")\n";
	//potentialError += "Message: Could not set shader uniform for \""+name+"\" (handle="+handle+") (uniformLocation="+uniformHandle+")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniformRgb(const UString& name, const RgbaColorf& color, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	debugger.saveUniformType(name, ShaderUniformType::Rgb);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform3f(uniformHandle, color.r, color.g, color.b);
	// \TODO: Fix this error in some way
	//UString potentialError("In function: void setUniformRgb(const UString& name=");
	//potentialError += name;
	//potentialError += ", const RgbaColorf& color)\n\n";//=[";
	//potentialError += color.toString();
	//potentialError += "])\n\n";
	//potentialError += "Message: Could not set shader uniform for \""+name+"\" (handle="+handle+") (uniformLocation="+uniformHandle+")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniformRgba(const UString& name, const RgbaColorf& color, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	debugger.saveUniformType(name, ShaderUniformType::Rgba);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform4f(uniformHandle, color.r, color.g, color.b, color.a);
	// \TODO: Fix this error in some way
	//UString potentialError("In function: void setUniformRgba(const UString& name=");
	//potentialError += name;
	//potentialError += ", const RgbaColorf& color=[";
	//potentialError += color.toString();
	//potentialError += "])\n\n";
	//potentialError += "Message: Could not set shader uniform for \""+name+"\" (handle="+handle+") (uniformLocation="+uniformHandle+")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniform(const UString& name, const std::vector<Vector2f>& vecs, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	ShaderUniformType uniformType = ShaderUniformType::Vec2;
	uniformType.definedArraySize = vecs.size();
	UString nameArray(name);
	nameArray += "[0]";
	debugger.saveUniformType(nameArray, uniformType);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform2fv(uniformHandle, static_cast<GLsizei>(uniformType.definedArraySize), reinterpret_cast<const float*>(vecs.data()));
	// \TODO: Fix this error in some way
	//UString potentialError("In function: void setUniform(const UString& name=");
	//potentialError += name;
	//potentialError += ", const std::vector<Vector2f>& vecs=[ ";
	//for (size_t i=0; i<uniformType.definedArraySize; ++i) {
	//	potentialError += vecs[i].toString();
	//	if (i < uniformType.definedArraySize-1)
	//		potentialError += ", ";
	//}
	//potentialError += " ])\n\n";
	//potentialError += "Message: Could not set shader uniform for \""+name+"\" (handle="+handle+") (uniformLocation="+uniformHandle+")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniform(const UString& name, const std::vector<Vector3f>& vecs, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	ShaderUniformType uniformType = ShaderUniformType::Vec3;
	uniformType.definedArraySize = vecs.size();
	UString nameArray(name);
	nameArray += "[0]";
	debugger.saveUniformType(nameArray, uniformType);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform3fv(uniformHandle, static_cast<GLsizei>(uniformType.definedArraySize), reinterpret_cast<const float*>(vecs.data()));
	// \TODO: Fix this error in some way
	//UString potentialError("In function: void setUniform(const UString& name=");
	//potentialError += name;
	//potentialError += ", const std::vector<Vector3f>& vecs=[ ";
	//for (size_t i=0; i<uniformType.definedArraySize; ++i) {
	//	potentialError += vecs[i].toString();
	//	if (i < uniformType.definedArraySize-1)
	//		potentialError += ", ";
	//}
	//potentialError += " ])\n\n";
	//potentialError += "Message: Could not set shader uniform for \""+name+"\" (handle="+handle+") (uniformLocation="+uniformHandle+")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniform(const UString& name, const std::vector<Vector4f>& vecs, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	ShaderUniformType uniformType = ShaderUniformType::Vec4;
	uniformType.definedArraySize = vecs.size();
	UString nameArray(name);
	nameArray += "[0]";
	debugger.saveUniformType(nameArray, uniformType);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform4fv(uniformHandle, static_cast<GLsizei>(uniformType.definedArraySize), reinterpret_cast<const float*>(vecs.data()));
	// \TODO: Fix this error in some way
	//UString potentialError("In function: void setUniform(const UString& name=");
	//potentialError += name;
	//potentialError += ", const std::vector<Vector4f>& vecs=[ ";
	//for (size_t i=0; i<uniformType.definedArraySize; ++i) {
	//	potentialError += vecs[i].toString();
	//	if (i < uniformType.definedArraySize-1)
	//		potentialError += ", ";
	//}
	//potentialError += " ])\n\n";
	//potentialError += "Message: Could not set shader uniform for \""+name+"\" (handle="+handle+") (uniformLocation="+uniformHandle+")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniform(const UString& name, const std::vector<Vector2i>& vecs, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	ShaderUniformType uniformType = ShaderUniformType::Ivec2;
	uniformType.definedArraySize = vecs.size();
	UString nameArray(name);
	nameArray += "[0]";
	debugger.saveUniformType(nameArray, uniformType);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform2iv(uniformHandle, static_cast<GLsizei>(uniformType.definedArraySize), reinterpret_cast<const int*>(vecs.data()));
	// \TODO: Fix this error in some way
	//UString potentialError("In function: void setUniform(const UString& name=");
	//potentialError += name;
	//potentialError += ", const std::vector<Vector2i>& vecs=[ ";
	//for (size_t i=0; i<uniformType.definedArraySize; ++i) {
	//	potentialError += vecs[i].toString();
	//	if (i < uniformType.definedArraySize-1)
	//		potentialError += ", ";
	//}
	//potentialError += " ])\n\n";
	//potentialError += "Message: Could not set shader uniform for \""+name+"\" (handle="+handle+") (uniformLocation="+uniformHandle+")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniform(const UString& name, const std::vector<Vector3i>& vecs, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	ShaderUniformType uniformType = ShaderUniformType::Ivec3;
	uniformType.definedArraySize = vecs.size();
	UString nameArray(name);
	nameArray += "[0]";
	debugger.saveUniformType(nameArray, uniformType);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform3iv(uniformHandle, static_cast<GLsizei>(uniformType.definedArraySize), reinterpret_cast<const int*>(vecs.data()));
	// \TODO: Fix this error in some way
	//UString potentialError("In function: void setUniform(const UString& name=");
	//potentialError += name;
	//potentialError += ", const std::vector<Vector3i>& vecs=[ ";
	//for (size_t i=0; i<uniformType.definedArraySize; ++i) {
	//	potentialError += vecs[i].toString();
	//	if (i < uniformType.definedArraySize-1)
	//		potentialError += ", ";
	//}
	//potentialError += " ])\n\n";
	//potentialError += "Message: Could not set shader uniform for \""+name+"\" (handle="+handle+") (uniformLocation="+uniformHandle+")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniform(const UString& name, const std::vector<Vector4i>& vecs, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	ShaderUniformType uniformType = ShaderUniformType::Ivec4;
	uniformType.definedArraySize = vecs.size();
	UString nameArray(name);
	nameArray += "[0]";
	debugger.saveUniformType(nameArray, uniformType);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform4iv(uniformHandle, static_cast<GLsizei>(uniformType.definedArraySize), reinterpret_cast<const int*>(vecs.data()));
	// \TODO: Fix this error in some way
	//UString potentialError("In function: void setUniform(const UString& name=");
	//potentialError += name;
	//potentialError += ", const std::vector<Vector4i>& vecs=[ ";
	//for (size_t i=0; i<uniformType.definedArraySize; ++i) {
	//	potentialError += vecs[i].toString();
	//	if (i < uniformType.definedArraySize-1)
	//		potentialError += ", ";
	//}
	//potentialError += " ])\n\n";
	//potentialError += "Message: Could not set shader uniform for \""+name+"\" (handle="+handle+") (uniformLocation="+uniformHandle+")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniform(const UString& name, const std::vector<Vector2u>& vecs, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	ShaderUniformType uniformType = ShaderUniformType::Uvec2;
	uniformType.definedArraySize = vecs.size();
	UString nameArray(name);
	nameArray += "[0]";
	debugger.saveUniformType(nameArray, uniformType);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform2uiv(uniformHandle, static_cast<GLsizei>(uniformType.definedArraySize), reinterpret_cast<const unsigned int*>(vecs.data()));
	// \TODO: Fix this error in some way
	//UString potentialError("In function: void setUniform(const UString& name=");
	//potentialError += name;
	//potentialError += ", const std::vector<Vector2u>& vecs=[ ";
	//for (size_t i=0; i<uniformType.definedArraySize; ++i) {
	//	potentialError += vecs[i].toString();
	//	if (i < uniformType.definedArraySize-1)
	//		potentialError += ", ";
	//}
	//potentialError += " ])\n\n";
	//potentialError += "Message: Could not set shader uniform for \""+name+"\" (handle="+handle+") (uniformLocation="+uniformHandle+")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniform(const UString& name, const std::vector<Vector3u>& vecs, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	ShaderUniformType uniformType = ShaderUniformType::Uvec3;
	uniformType.definedArraySize = vecs.size();
	UString nameArray(name);
	nameArray += "[0]";
	debugger.saveUniformType(nameArray, uniformType);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform3uiv(uniformHandle, static_cast<GLsizei>(uniformType.definedArraySize), reinterpret_cast<const unsigned int*>(vecs.data()));
	// \TODO: Fix this error in some way
	//UString potentialError("In function: void setUniform(const UString& name=");
	//potentialError += name;
	//potentialError += ", const std::vector<Vector3u>& vecs=[ ";
	//for (size_t i=0; i<uniformType.definedArraySize; ++i) {
	//	potentialError += vecs[i].toString();
	//	if (i < uniformType.definedArraySize-1)
	//		potentialError += ", ";
	//}
	//potentialError += " ])\n\n";
	//potentialError += "Message: Could not set shader uniform for \""+name+"\" (handle="+handle+") (uniformLocation="+uniformHandle+")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniform(const UString& name, const std::vector<Vector4u>& vecs, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	ShaderUniformType uniformType = ShaderUniformType::Uvec4;
	uniformType.definedArraySize = vecs.size();
	UString nameArray(name);
	nameArray += "[0]";
	debugger.saveUniformType(nameArray, uniformType);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform4uiv(uniformHandle, static_cast<GLsizei>(uniformType.definedArraySize), reinterpret_cast<const unsigned int*>(vecs.data()));
	// \TODO: Fix this error in some way
	//UString potentialError("In function: void setUniform(const UString& name=");
	//potentialError += name;
	//potentialError += ", const std::vector<Vector4u>& vecs=[ ";
	//for (size_t i=0; i<uniformType.definedArraySize; ++i) {
	//	potentialError += vecs[i].toString();
	//	if (i < uniformType.definedArraySize-1)
	//		potentialError += ", ";
	//}
	//potentialError += " ])\n\n";
	//potentialError += "Message: Could not set shader uniform for \""+name+"\" (handle="+handle+") (uniformLocation="+uniformHandle+")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}


void ShaderProgram::setUniformMatrix4fv(const UString& name, GLsizei count, bool transpose, const GLfloat* value, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	ShaderUniformType uniformType = ShaderUniformType::Mat4;
	uniformType.definedArraySize = count;
	UString nameArray(name);
	if (count > 1) {
		nameArray += "[0]";
	}
	debugger.saveUniformType(nameArray, uniformType);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}

	glUniformMatrix4fv(uniformHandle, count, transpose, value);
	// \TODO: Fix this error in some way
	//UString potentialError("In function: void setUniformMatrix(const UString& name=");
	//potentialError += name;
	//potentialError += ", count=";
	//potentialError.append2(count);
	//potentialError += ", transpose=";
	//potentialError.append2(transpose);
	//potentialError += ", value=[ ";
	//for (size_t i = 0; i<static_cast<size_t>(count); ++i) {
	//	potentialError += "[ ";
	//	for (size_t j=0; j<4*4; ++j) {
	//		potentialError.append2(value[i*4*4 + j]);
	//		potentialError += ", ";
	//	}
	//	potentialError += "], ";
	//}
	//potentialError += " ])\n\n";
	//potentialError += "Message: Could not set shader uniform for \""+name+"\" (handle="+handle+") (uniformLocation="+uniformHandle+")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}

//void ShaderProgram::setUniform(const UString& name, const sf::Transform& transform)
//void ShaderProgram::setUniform(const UString& name, const Texture& texture)
//void ShaderProgram::setUniform(const UString& name, CurrentTextureType)

void ShaderProgram::setUniform1fv(const UString& name, GLsizei count, const GLfloat* values, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	ShaderUniformType uniformType = ShaderUniformType::Float;
	uniformType.definedArraySize = count;
	UString nameArray(name);
	if (count > 1) {
		nameArray += "[0]";
	}
	debugger.saveUniformType(nameArray, uniformType);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform1fv(uniformHandle, count, values);
	// \TODO: Fix this error in some way
	//UString potentialError("In function: void setUniform1fv(const UString& name=");
	//potentialError += name;
	//potentialError += ", GLsizei count=";
	//potentialError.append2(count);
	//potentialError += ", float* values=[ ";
	//for (GLsizei i=0; i<count; ++i) {
	//	potentialError.append2(values[i]);
	//	if (i != count-1)
	//		potentialError += ", ";
	//}
	//potentialError += " ])\n\n";
	//potentialError += "Message: Could not set shader uniform for \""+name+"\" (handle="+handle+") (uniformLocation="+uniformHandle+")";
	//GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
}

// ShaderProgram::glUniform2fv

void ShaderProgram::setUniform3fv(const UString& name, GLsizei count, const GLfloat* values, bool required)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	ShaderUniformType uniformType = ShaderUniformType::Vec3;
	uniformType.definedArraySize = count;
	UString nameArray(name);
	nameArray += "[0]";
	debugger.saveUniformType(nameArray, uniformType);

	GLint uniformHandle = getUniformHandle(name);
	if (uniformHandle == -1) {
		UString error("Could not get uniform for string \""+name+"\"");
		GL_CHECK_FOR_ERRORS(error, sourceFileNames);

		if (required == true) {
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), sourceFileNamesToCString());
		}

		return;
	}
	glUniform3fv(uniformHandle, count, values);
	// \TODO: Fix this error in some way
//		UString potentialError("In function: void setUniform3fv(const UString& name=");
//		potentialError += name;
//		potentialError += ", GLsizei count=";
//		potentialError.append2(count);
//		potentialError += ", float* values=[ ";
//		for (GLsizei i=0; i<count; ++i) {
//			potentialError += "[ ";
//			for (size_t j=0; j<3; ++j) {
//				potentialError.append2(values[i*3 + j]);
//				if (j != 2)
//					potentialError += ", ";
//			}
//			if (i != count-1)
//				potentialError += " ], ";
//		}
//		potentialError += " ])\n\n";
//		potentialError += "Message: Could not set shader uniform for \""+name+"\" (handle="+handle+") (uniformLocation="+uniformHandle+")";
	GL_CHECK_FOR_ERRORS("", sourceFileNames);

	/*for (size_t i=0; i<count; ++i) {
		UString nameArray(name);
		nameArray += "[";
		nameArray.append2(i);
		nameArray += "]";
		glUniform3fv(uniformHandle, count, values);
		GL_CHECK_FOR_ERRORS(potentialError, sourceFileNames);
	}*/
}

// ShaderProgram::glUniform4fv
// ShaderProgram::glUniform1iv
// ShaderProgram::glUniform2iv
// ShaderProgram::glUniform3iv
// ShaderProgram::glUniform4iv
// ShaderProgram::glUniform1uiv
// ShaderProgram::glUniform2uiv
// ShaderProgram::glUniform3uiv
// ShaderProgram::glUniform4uiv
// ShaderProgram::glUniformMatrix2fv
// ShaderProgram::glUniformMatrix3fv
// ShaderProgram::glUniformMatrix2x3fv
// ShaderProgram::glUniformMatrix3x2fv
// ShaderProgram::glUniformMatrix2x4fv
// ShaderProgram::glUniformMatrix3x4fv
// ShaderProgram::glUniformMatrix4x3fv

// \TODO: implement maybe!
/*bool ShaderProgram::loadAndLinkDefaultShader()
{
	UString vertexShaderSource =
			"#version 330 core\n"
			"layout (location = 0) in vec3 VertexPosition;\n"
			"layout (location = 1) in vec2 VertexTexCoord;\n"
			"layout (location = 2) in vec3 VertexNormal;\n"
			"layout (location = 3) in vec3 VertexColor;\n"
			"out vec2 TexCoord;\n"
			"out vec3 Color;\n"
			"uniform mat4 ModelViewMatrix;\n"
			"uniform mat3 NormalMatrix;\n"
			"uniform mat4 ProjectionMatrix;\n"
			"uniform mat4 MVP; // ProjectionMatrix * ModelViewMatrix\n"
			"void main()\n"
			"{\n"
			"	TexCoord = VertexTexCoord;\n"
			"	gl_Position = MVP * vec4(VertexPosition, 1.0);\n"
			"	Color = VertexColor;\n"
			"}\n";
	bool result	= compileAndAddShaderFromString(vertexShaderSource, Shader::VERTEX_SHADER);
	//bool result	= compileAndAddShaderFromFile(Render::shadersVersoPath+"standard.330.vert", Shader::VERTEX_SHADER);
	if (result == false) {
		VERSO_ERROR("verso-3d", "Error compiling default vertex shader", sourceFileNamesToCString());
	}

	UString fragmentShaderSource =
			"#version 330 core\n"
			"in vec2 TexCoord;\n"
			"in vec3 Color;\n"
			"uniform sampler2D Tex1;\n"
			"uniform float Alpha;\n"
			"out vec4 FragColor;\n"
			"void main() {\n"
			"	vec4 texColor = texture(Tex1, TexCoord.st);\n"
			"	FragColor = vec4(1.0, 1.0, 1.0, 1.0 * Alpha) * texColor;\n"
			"}\n";
	//result	= compileAndAddShaderFromFile(Render::shadersVersoPath+"standard.330.frag", Shader::FRAGMENT_SHADER);
	result = compileAndAddShaderFromString(fragmentShaderSource, Shader::FRAGMENT_SHADER);
	if (result == false) {
		VERSO_ERROR("verso-3d", "Error compiling default fragment shader!", sourceFileNamesToCString());
	}


	link();
	//validate();
	use();
	setUniform1i("Tex1", 0);

	// Create shader
	//shaderProgram.compileAndAddShaderFromFile(
	//			getDemoApp()->getDataShadersPath()+VERTEX_SHADER_FILENAME, Shader::VERTEX_SHADER);
	//shaderProgram.compileAndAddShaderFromFile(
	//			getDemoApp()->getDataShadersPath()+FRAGMENT_SHADER_FILENAME, Shader::FRAGMENT_SHADER);

	return true;
}*/


void ShaderProgram::printActiveUniforms()
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	debugger.printActiveUniforms(handle, sourceFileNames);
}


///////////////////////////////////////////////////////////////////////////////////////////
// Private
///////////////////////////////////////////////////////////////////////////////////////////
void ShaderProgram::createProgram(bool useGeometry, bool bindDefaultAttributes)
{
	// If we reached this point it means the vertex / fragment / geometry shaders compiled and are syntax error free.
	// We must link them together to make a GL shader program
	// GL shader programs are monolithic. It is a single piece made of 1 vertex shader and 1 fragment shader and possibly 1 geometry shader.
	// Assign our program handle a "name"
	handle = glCreateProgram();
	GL_CHECK_FOR_ERRORS("", "");

	// Attach our shaders to our program
	glAttachShader(handle, vertex.getHandle());
	GL_CHECK_FOR_ERRORS("", "");
	glAttachShader(handle, fragment.getHandle());
	GL_CHECK_FOR_ERRORS("", "");
	if (useGeometry == true) {
		glAttachShader(handle, geometry.getHandle());
		GL_CHECK_FOR_ERRORS("", "");
	}

	// note: bindAttribLocation() requires ShaderProgram to be "created".
	created = true;

	// Bind default attribute locations?
	if (bindDefaultAttributes == true) {
		std::vector<ShaderAttribute> attributes = ShaderAttribute::getDefaultAttributes();
		for (auto& attribute : attributes) {
			bindAttribLocation(attribute);
		}
	}
}


// Note: must be called after linkProgram()
bool ShaderProgram::validateProgram()
{
	glValidateProgram(handle);
	GL_CHECK_FOR_ERRORS("", "");

	int validateStatus;
	glGetProgramiv(handle, GL_VALIDATE_STATUS, &validateStatus);
	GL_CHECK_FOR_ERRORS("", "");
	if (validateStatus ==  GL_FALSE) {
		int maxLength;
		glGetProgramiv(handle, GL_INFO_LOG_LENGTH, &maxLength);
		GL_CHECK_FOR_ERRORS("", "");

		// The maxLength includes the NULL character
		char* infoLog = new char[maxLength];
		glGetProgramInfoLog(handle, maxLength, &maxLength, infoLog);
		GL_CHECK_FOR_ERRORS("", "");

		UString error("Could not link Shader program. Infolog: \"");
		error += infoLog;
		error += "\"";
		delete[] infoLog;
		VERSO_ERROR("verso-3d", error.c_str(), sourceFileNamesToCString());
	}
	return true;
}


GLint ShaderProgram::getUniformHandle(const UString& name)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	const auto& it = uniforms.find(name);
	if (it == uniforms.end()) {
		GLint uniformLocation = glGetUniformLocation(handle, name.c_str());
		if (uniformLocation == -1) {
			return -1;
		}
		GL_CHECK_FOR_ERRORS("", sourceFileNames);

		uniforms[name] = uniformLocation;
		return uniformLocation;
	}
	else {
		return it->second;
	}
}


GLuint ShaderProgram::getUniformBlockIndex(const UString& name)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", linked == true, "ShaderProgram not yet linked!", sourceFileNamesToCString());

	// \TODO: make a better implementation than this dummy one
	GLuint index = glGetUniformBlockIndex(handle, name.c_str());
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
	if (index == GL_INVALID_INDEX) {
		VERSO_ILLEGALPARAMETERS("verso-3d", "Unknown uniform block name", name.c_str());
	}

	return index;
}


const char* ShaderProgram::sourceFileNamesToCString()
{
	std::ostringstream oss;
	for (size_t i=0; i<sourceFileNames.size(); ++i) {
		if (i != 0) {
			oss << ", ";
		}
		oss << "\""<<sourceFileNames[i]<<"\"";
	}
	std::string tmpOutput = oss.str();

	if (sourceFileNamesCStr != nullptr) {
		delete[] sourceFileNamesCStr;
	}
	sourceFileNamesCStr = new char[tmpOutput.size()+1];
	strcpy(sourceFileNamesCStr, tmpOutput.c_str());
	return sourceFileNamesCStr;
}


UString ShaderProgram::toString(const UString& newLinePadding) const
{
	UString str("{ ");
	UString newLinePadding2 = newLinePadding + "  ";
	str += "handle=";
	str.append2(handle);
	str += ", Shader(";
	str += vertex.toString(newLinePadding2);
	str += "), Shader(";
	str += fragment.toString(newLinePadding2);
	str += "), Shader(";
	str += geometry.toString(newLinePadding2);
	str += "), attributes=";
	for (auto& attribute : attributes) {
		str += attribute.toStringDebug();
		str += ", ";
	}
	str += "linked=";
	str.append2(linked);
	str += " }";

	return str;
}


UString ShaderProgram::toStringDebug(const UString& newLinePadding) const
{
	UString str("ShaderProgram(");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso

