#include <Verso/Render/Mesh.hpp>
#include <Verso/Display/IWindowOpengl.hpp>
#include <Verso/Render/Texture.hpp>
#include <Verso/Render/ShaderProgram.hpp>

namespace Verso {


Mesh::Mesh(const UString& id) :
	vao(id),
	material(nullptr),
	textures()
{
}


Mesh::Mesh(Mesh&& original) noexcept :
	vao(std::move(original.vao)),
	material(std::move(original.material)),
	textures(std::move(original.textures))
{
	original.material = nullptr;
	original.textures.clear();
}


Mesh& Mesh::operator =(Mesh&& original) noexcept
{
	if (this != &original) {
		vao = std::move(original.vao);
		material = std::move(original.material);
		textures = std::move(original.textures);

		original.material = nullptr;
		original.textures.clear();
	}
	return *this;
}


Mesh::~Mesh()
{
	destroy();
}


void Mesh::create(PrimitiveType primitiveType)
{
	vao.create(primitiveType);
}


void Mesh::destroy() VERSO_NOEXCEPT
{
	for (auto&& i : textures) {
		if (i != nullptr) {
			i->destroy();
		}
	}
	textures.clear();

	if (material != nullptr) {
		material->destroy();
		material = nullptr;
	}

	vao.destroy();
}


void Mesh::render(IWindowOpengl& window)
{
	vao.render();
}


void Mesh::render(IWindowOpengl& window, ShaderProgram& shaderProgram)
{
//	for (int i = 0; i < static_cast<int>(textures.size()); ++i) {
//		window.setActiveTextureUnit(i);

//		Texture* tex = textures[i];
//		UString textureVarName("material.");
//		textureVarName += tex->getParameters().typeForShader;
//		textureVarName.append2(i);
//		shaderProgram.setUniformi(textureVarName, static_cast<GLint>(i));
//		tex->bind();
//	}
//	window.setActiveTextureUnit(0);

//	// Render the mesh
//	vao.render();

//	// Return the defaults after render
//	window.bindResetTextureUnits(textures.size());
}


UString Mesh::toString(const UString& newLinePadding) const
{
	UString str("{ ");
	UString newLinePadding2 = newLinePadding + "  ";
	str += "vao=";
	str += vao.toString();
	str += ", textures=[ ";
	for (size_t i=0; i<textures.size(); ++i) {
		str += textures[i]->toString(newLinePadding2);
		if (i != textures.size()-1)
			str += ", ";
	}
	str += " ]";
	str += " }";
	return str;
}


UString Mesh::toStringDebug(const UString& newLinePadding) const
{
	UString str("Mesh(");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso

