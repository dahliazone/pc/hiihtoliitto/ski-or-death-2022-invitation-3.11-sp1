#include <Verso/Render/Particle/Emitters/SphericalRandom.hpp>

namespace Verso {


SphericalRandom::SphericalRandom() :
	radiusRange(1.0f, 5.0f)
{
}


SphericalRandom::~SphericalRandom()
{
}


void SphericalRandom::reset()
{
}


void SphericalRandom::emitter(size_t count)
{
	ParticleSystem* particleSystem = getParticleSystem();

	for (size_t i=0; i<count; ++i) {
		float angleX = Random::floatRange(0.0f, 180.0f);
		float angleY = Random::floatRange(0.0f, 360.0f);
		float radius = Random::floatRange(radiusRange);
		particleSystem->addParticle(
			Vector3f(Random::floatRange(xRange) + radius * std::sin(angleX) * std::cos(angleY),
					 Random::floatRange(yRange) + radius * std::sin(angleX) * std::sin(angleY),
					 Random::floatRange(zRange) + radius * std::cos(angleX)),
			Random::floatRange(angleRange),
			Vector3f(0.0f, 0.0f, 0.0f),
			Random::floatRange(angleVelocityRange),
			Random::floatRange(startSizeRange),
			Random::floatRange(endSizeRange),
			Random::floatRange(totalLifeTimeRange),
			RgbaColorf(Random::floatRange(redRange),
					   Random::floatRange(greenRange),
					   Random::floatRange(blueRange)),
			Random::floatRange(startAlphaRange),
			Random::floatRange(endAlphaRange));
	}
}


UString SphericalRandom::toString() const
{
	return UString("radiusRange="+radiusRange.toString());
}


UString SphericalRandom::toStringDebug() const
{
	return UString(getType()+"("+toString()+")");
}


const Rangef& SphericalRandom::getRadiusRange() const
{
	return radiusRange;
}


void SphericalRandom::setRadiusRange(const Rangef& radiusRange)
{
	this->radiusRange = radiusRange;
}


} // End namespace Verso

