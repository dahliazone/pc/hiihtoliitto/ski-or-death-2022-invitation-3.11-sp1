
#include <Verso/Render/Particle/ParticleManager.hpp>

namespace Verso {


ParticleManager::ParticleManager(const UString& id) :
	id(id),
	particleEffects(),
	particleSystems(),
	timer(0.0f),
	interval(1.0f)
{
	// Initially reserve space for 30 particle effects and systems
	particleEffects.reserve(30);
	particleSystems.reserve(30);
}


ParticleManager::ParticleManager(ParticleManager&& original) :
	particleEffects(std::move(original.particleEffects)),
	particleSystems(std::move(original.particleSystems)),
	timer(std::move(original.timer)),
	interval(std::move(original.interval))
{
	original.particleEffects.clear();
	original.particleSystems.clear();
}


ParticleManager& ParticleManager::operator =(ParticleManager&& original)
{
	if (this != &original) {
		particleEffects = std::move(original.particleEffects);
		particleSystems = std::move(original.particleSystems);
		timer = std::move(original.timer);
		interval = std::move(original.interval);

		original.particleEffects.clear();
		original.particleSystems.clear();
	}
	return *this;
}


ParticleManager::~ParticleManager()
{
}


void ParticleManager::update(IWindowOpengl& window, const FrameTimestamp& time, ICamera& camera)
{
	calculateAndRender(window, time, camera);
	removeOld(time);
}


void ParticleManager::calculate(const FrameTimestamp& time)
{
	for (auto it : particleEffects) {
		it->calculate(time);
	}

	for (auto it : particleSystems) {
		it->calculate(time);
	}
}


void ParticleManager::render(IWindowOpengl& window, const FrameTimestamp& time, ICamera& camera)
{
	for (auto it : particleEffects) {
		it->render(window, time, camera);
	}

	for (auto it : particleSystems) {
		it->render(window, time, camera);
	}
}


void ParticleManager::calculateAndRender(IWindowOpengl& window, const FrameTimestamp& time, ICamera& camera)
{
	for (auto it : particleEffects) {
		it->calculateAndRender(window, time, camera);
	}

	for (auto it : particleSystems) {
		it->calculateAndRender(window, time, camera);
	}
}


void ParticleManager::removeOld(const FrameTimestamp& time)
{
	timer += time.getDt().asSeconds();
	if (timer >= interval)
	{
		timer -= interval;

		for (auto it=particleEffects.begin();
				it!=particleEffects.end(); )
		{
			if ((*it)->isDone() == true) {
				it = particleEffects.erase(it);
			}
			else {
				++it;
			}
		}

		for (auto it = particleSystems.begin();
				it!=particleSystems.end(); )
		{
			if ((*it)->isEmpty() == true) {
				it = particleSystems.erase(it);
			}
			else {
				++it;
			}
		}
	}
}


size_t ParticleManager::addParticleEffect(ParticleEffect* particleEffect)
{
	particleEffects.push_back(particleEffect);
	return particleEffects.size()-1;
}


ParticleEffect* ParticleManager::getParticleEffect(size_t index)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", index < particleEffects.size(), "", id.c_str());
	return particleEffects[index];
}


void ParticleManager::removeParticleEffect(size_t particleEffectId)
{
	particleEffects.erase(particleEffects.begin() + particleEffectId);
}


size_t ParticleManager::addParticleSystem(ParticleSystem* particleSystem)
{
	particleSystems.push_back(particleSystem);
	return particleSystems.size()-1;
}


ParticleSystem* ParticleManager::getParticleSystem(size_t index)
{
	VERSO_ASSERT("verso-3d", index < particleSystems.size());
	return particleSystems[index];
}


void ParticleManager::removeParticleSystem(size_t particleSystemId)
{
	particleSystems.erase(particleSystems.begin() + particleSystemId);
}


} // End namespace Verso

