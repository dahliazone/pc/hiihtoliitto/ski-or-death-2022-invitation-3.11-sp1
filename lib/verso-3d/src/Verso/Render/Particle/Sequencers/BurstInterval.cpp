#include <Verso/Render/Particle/Sequencers/BurstInterval.hpp>

namespace Verso {


BurstInterval::BurstInterval() :
	sequenceType(SequenceType::None),
	startTime(0.0f),
	endTime(0.0f),
	neverEnding(false),
	currentTime(0.0f),
	interval(0.0f),
	intervalPosition(0.0f),
	duration(0.0f),
	durationPosition(0.0f),
	durationEmitted(0.0f),
	burstSize(0),
	particlesPerSecond(0)
{
}


BurstInterval::~BurstInterval()
{
}


void BurstInterval::reset()
{
	currentTime = 0.0f;
	durationPosition = 0.0f;
	durationEmitted = 0.0f;
}


void BurstInterval::sequence(const FrameTimestamp& time)
{
	// By default emit zero particles
	size_t count = 0;
	float deltaSeconds = static_cast<float>(time.getDt().asSeconds());

	if (sequenceType == SequenceType::Continuous) {
		// If it's time for the initial burst
		if (currentTime <= startTime && currentTime+deltaSeconds >= startTime) {
			count = burstSize;
			durationPosition = 0.0f;
			durationEmitted = 0.0f;
			durationPosition += (currentTime + deltaSeconds) - startTime;
			intervalPosition += interval;
		}

		// If it's time interval burst (and neverEnding or endTime is not yet met)
		else if (currentTime <= intervalPosition && currentTime+deltaSeconds >= intervalPosition && (neverEnding==true || currentTime <= endTime)) {
			count = burstSize;
			durationPosition = 0.0f;
			durationEmitted = 0.0f;
			durationPosition += (currentTime + deltaSeconds) - intervalPosition;
			intervalPosition += interval;
		}

		// If time is between startTime and endTime
		else if (currentTime >= startTime && (neverEnding==true || currentTime <= endTime)) {
			if (interval == 0.0f && particlesPerSecond != 0) {
				//if (durationPosition > endTime && neverEnding==false)
				//	durationPosition = endTime;

				count = static_cast<size_t>(floor((durationPosition - durationEmitted) * static_cast<double>(particlesPerSecond)));
				durationEmitted += static_cast<double>(count) / static_cast<double>(particlesPerSecond);
				durationPosition += deltaSeconds;
			}

			else {
				if (durationPosition > duration && neverEnding == false) {
					durationPosition = duration;
				}

				if (particlesPerSecond != 0) {
					count = static_cast<size_t>(floor((durationPosition - durationEmitted) * static_cast<double>(particlesPerSecond)));
					durationEmitted += static_cast<double>(count) / static_cast<double>(particlesPerSecond);
					durationPosition += deltaSeconds;
				}
			}
		}
	}


	else if (sequenceType == SequenceType::Single) {
		// If it's time for the burst
		if (currentTime <= startTime && currentTime+deltaSeconds >= startTime) {
			count = burstSize;
			durationPosition += (currentTime + deltaSeconds) - startTime;
		}

		// If time is during the duration
		else if (currentTime >= startTime) {
			if (durationPosition > duration && neverEnding==false)
				durationPosition = duration;

			if (particlesPerSecond != 0) {
				count = static_cast<size_t>(floor((durationPosition - durationEmitted) * static_cast<double>(particlesPerSecond)));
				durationEmitted += static_cast<double>(count) / static_cast<double>(particlesPerSecond);
				durationPosition += deltaSeconds;
			}
		}
	}

	//if (count != 0)
		//cout << "time=" << currentTime << ", durationPosition=" << durationPosition << ", emitted=" << durationEmitted << endl;
		//cout << count << endl;

	getParticleEmitter()->emitter(count);

	currentTime += deltaSeconds;
}


} // End namespace Verso

