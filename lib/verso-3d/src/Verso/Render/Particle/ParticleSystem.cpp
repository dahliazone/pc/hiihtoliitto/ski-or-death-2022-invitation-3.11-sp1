
#include <Verso/Render/Particle/ParticleSystem.hpp>
#include <Verso/Render/Render.hpp>

namespace Verso {


const size_t ParticleSystem::DEFAULT_PARTICLE_LIMIT = 5;


ParticleSystem::ParticleSystem(const UString& id) :
	created(false),
	id(id),
	position(),
	particleBehaviour(),
	particles(),
	material(),
	animatedTexture(false),
	trailSize(0),
	trailSaveTime(0.0f),
	trailSaveInterval(0.1f),
	nextParticleIndex(0),
	firstActiveParticleIndex(0),
	lastActiveParticleIndex(0),
	blendMode(),
	passedTime(0.0f),
	vao("verso-3d/ParticleSystem: " + id)
{
}


ParticleSystem::ParticleSystem(ParticleSystem&& original) noexcept :
	created(std::move(original.created)),
	id(std::move(original.id)),
	position(std::move(original.position)),
	particleBehaviour(std::move(original.particleBehaviour)),
	particles(std::move(original.particles)),
	material(std::move(original.material)),
	animatedTexture(std::move(original.animatedTexture)),
	trailSize(std::move(original.trailSize)),
	trailSaveTime(std::move(original.trailSaveTime)),
	trailSaveInterval(std::move(original.trailSaveInterval)),
	nextParticleIndex(std::move(original.nextParticleIndex)),
	firstActiveParticleIndex(std::move(original.firstActiveParticleIndex)),
	lastActiveParticleIndex(std::move(original.lastActiveParticleIndex)),
	blendMode(std::move(original.blendMode)),
	passedTime(std::move(original.passedTime)),
	vao(std::move(original.vao))
{
	original.created = false;
	original.id.clear();
	original.particleBehaviour = nullptr;
	original.particles.clear();
}


ParticleSystem& ParticleSystem::operator =(ParticleSystem&& original) noexcept
{
	if (this != &original) {
		created = std::move(original.created);
		id = std::move(original.id);
		position = std::move(original.position);
		particleBehaviour = std::move(original.particleBehaviour);
		particles = std::move(original.particles);
		material = std::move(original.material);
		animatedTexture = std::move(original.animatedTexture);
		trailSize = std::move(original.trailSize);
		trailSaveTime = std::move(original.trailSaveTime);
		trailSaveInterval = std::move(original.trailSaveInterval);
		nextParticleIndex = std::move(original.nextParticleIndex);
		firstActiveParticleIndex = std::move(original.firstActiveParticleIndex);
		lastActiveParticleIndex = std::move(original.lastActiveParticleIndex);
		blendMode = std::move(original.blendMode);
		passedTime = std::move(original.passedTime);
		vao = std::move(original.vao);

		original.created = false;
		original.id.clear();
		original.particleBehaviour = nullptr;
		original.particles.clear();
	}
	return *this;
}


ParticleSystem::~ParticleSystem()
{
	destroy();
}


bool ParticleSystem::create(size_t particleLimit)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == false, "Already created!", id.c_str());

	vao.create(PrimitiveType::Points);
	//	void setDataf(const ShaderAttribute& attribute, const GLvoid* data, size_t vertexCount, size_t perVertexCount, const BufferUsagePattern& bufferUsagePattern = BufferUsagePattern::StaticDraw);
	//	void appendDataf(const ShaderAttribute& attribute, const GLvoid* data, size_t vertexCount, size_t perVertexCount);
	//	void applyAppendedData(const BufferUsagePattern& bufferUsagePattern = BufferUsagePattern::StaticDraw);
	//	void setIndices(const GLuint* indices, size_t indicesCount, const BufferUsagePattern& bufferUsagePattern = BufferUsagePattern::StaticDraw);

	raiseParticleLimit(particleLimit);

	created = true;
	return true;
}


void ParticleSystem::destroy() VERSO_NOEXCEPT
{
	if (isCreated() == false) {
		return;
	}

	// \TODO: free all the memory thats not now freed

	created = false;
}


bool ParticleSystem::isCreated() const
{
	return created;
}


void ParticleSystem::calculate(const FrameTimestamp& time)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	bool behaviourSet = false;
	if (particleBehaviour != nullptr) {
		behaviourSet = true;
	}

	passedTime += time.getDt().asSeconds();

	bool saveTrailLocation = isTimeToSaveTrailLocation(time);

	size_t loopStart = firstActiveParticleIndex;
	size_t loopEnd = lastActiveParticleIndex;
	bool found = false;
	lastActiveParticleIndex = 0;
	for (size_t i=loopStart; i<=loopEnd; ++i)
	{
		Particle* p = &particles[i];

		if (p->lifetimePassed > p->totalLifetime) {
			continue;
		}

		if (found == false) {
			firstActiveParticleIndex = i;
			found = true;
		}

		lastActiveParticleIndex = i;

		calculateParticle(time, p, behaviourSet, i, loopStart, loopEnd);

		if (saveTrailLocation == true) {
			saveTrailLocations(p);
		}
	}
}


void ParticleSystem::render(IWindowOpengl& window, const FrameTimestamp& time, ICamera& camera)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	bindRenderingFlags(window, time, camera);

	for (size_t i=firstActiveParticleIndex; i<=lastActiveParticleIndex; ++i)
	{
		Particle* p = &particles[i];

		if (p->lifetimePassed > p->totalLifetime) {
			continue;
		}

		renderParticleWithTrail(window, time, camera, p);
	}

	unbindRenderingFlags(window, time, camera);
}


void ParticleSystem::calculateAndRender(IWindowOpengl& window, const FrameTimestamp& time, ICamera& camera)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	bindRenderingFlags(window, time, camera);

	// Find out whether it's time to save trail location
	bool saveTrailLocation = isTimeToSaveTrailLocation(time);

	bool behaviourSet = false;
	if (particleBehaviour != nullptr) {
		behaviourSet = true;
	}

	size_t loopStart = firstActiveParticleIndex;
	size_t loopEnd = lastActiveParticleIndex;
	bool found = false;
	lastActiveParticleIndex = 0;
	for (size_t i=loopStart; i<=loopEnd; ++i)
	{
		Particle* p = &particles[i];

		if (p->lifetimePassed > p->totalLifetime) {
			continue;
		}

		if (found == false) {
			firstActiveParticleIndex = i;
			found = true;
		}

		lastActiveParticleIndex = i;

		calculateParticle(time, p, behaviourSet,
						  i, loopStart, loopEnd);

		renderParticleWithTrail(window, time, camera, p);

		if (saveTrailLocation == true) {
			saveTrailLocations(p);
		}
	}

	// Model matrix
	Matrix4x4f model;

	// Translate image to viewport point
	//model = Matrix4x4f::translate(model, x, y, z);


	// Billboard the particle (fast cheap hack)
	//float modelview[16];

	// get the current modelview matrix
	//glGetFloatv(GL_MODELVIEW_MATRIX, modelview);
	//GL_CHECK_FOR_ERRORS("", "");

	// Undo all rotations
	//  beware all scaling is lost as well!
	/*	model.data[0][0] = 1.0f;
	model.data[0][1] = 0.0f;
	model.data[0][2] = 0.0f;
	model.data[1][0] = 0.0f;
	model.data[1][1] = 1.0f;
	model.data[1][2] = 0.0f;
	model.data[2][0] = 0.0f;
	model.data[2][1] = 0.0f;
	model.data[2][2] = 1.0f;*/

	//	model.data[0][0] = 1.0f;
	//	model.data[1][0] = 0.0f;
	//	model.data[2][0] = 0.0f;
	//	model.data[0][1] = 0.0f;
	//	model.data[1][1] = 1.0f;
	//	model.data[2][1] = 0.0f;
	//	model.data[0][2] = 0.0f;
	//	model.data[1][2] = 0.0f;
	//	model.data[2][2] = 1.0f;

	// Rotate by Z axis
	//model = Matrix4x4f::rotate(model, angleZ, Vector3f(0.0f, 0.0f, 1.0f));

	// Scale
	//model = Matrix4x4f::scale(model, size, size, 1.0f);

	// Color & alpha
	//material.updateDiffuseColor(RgbaColorf(red, green, blue, alpha));

	material.updateModelMatrix(model);
	vao.applyAppendedData(BufferUsagePattern::DynamicDraw);
	vao.render();

	unbindRenderingFlags(window, time, camera);
}


bool ParticleSystem::isEmpty() const
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	if (firstActiveParticleIndex > lastActiveParticleIndex) {
		return true;
	}

	else if (firstActiveParticleIndex == lastActiveParticleIndex &&
			 particles[firstActiveParticleIndex].lifetimePassed >
			 particles[firstActiveParticleIndex].totalLifetime) {
		return true;
	}

	return false;
}


void ParticleSystem::reset()
{
	//std::vector<Particle> particles;
	nextParticleIndex = 0;
	firstActiveParticleIndex = 0;
	lastActiveParticleIndex = 0;
	passedTime = 0.0f;
}


size_t ParticleSystem::addParticle(const Vector3f& position, float angle,
								   const Vector3f& velocity, float angleVelocity,
								   float startSize, float endSize, float totalLifetime,
								   const RgbaColorf& color,
								   float startAlpha, float endAlpha)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	Particle* p = &particles[nextParticleIndex];

	p->position = this->position + position;
	p->angle = angle;
	for (size_t i=0; i<trailSize; ++i) {
		p->oldPosition[i] = this->position + position;
		p->oldAngle[i] = angle;
		p->oldSize[i] = startSize;
		p->oldAnimationFrame[i] = 0;
	}
	p->trailIndex = 0;
	p->trailSize = 0;
	p->velocity = velocity;
	p->angleVelocity = angleVelocity;
	p->size = startSize;
	p->startSize = startSize;
	p->deltaSize = endSize - startSize;
	p->totalLifetime = totalLifetime;
	if (totalLifetime != 0.0f) {
		p->totalLifetimeInverse = 1.0f / totalLifetime;
	}
	else {
		p->totalLifetimeInverse = 1.0f / 0.00001f;
	}
	p->lifetimePassed = 0.0f;
	p->color = color;
	p->color.a = startAlpha;
	p->startAlpha = startAlpha;
	p->deltaAlpha = endAlpha - startAlpha;
	p->animationFrame = 0;

	size_t currentIndex = nextParticleIndex;
	nextParticleIndex = (nextParticleIndex+1)%particles.size();
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", nextParticleIndex < particles.size(), "", id.c_str());

	if (currentIndex < firstActiveParticleIndex) {
		firstActiveParticleIndex = currentIndex;
	}

	if (currentIndex > lastActiveParticleIndex) {
		lastActiveParticleIndex = currentIndex;
	}

	return currentIndex;
}


void ParticleSystem::removeParticle(size_t index)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", index < particles.size(), "", id.c_str());

	particles[index].lifetimePassed = particles[index].totalLifetime;
}


void ParticleSystem::setTexture(IWindowOpengl& window, const UString& fileName, bool animatedTexture)
{
	this->material.destroy();
	//this->material.changeShaders(
	//			Render::shadersVersoPath+"render/particle/texturergba3d-billboard.330.vert",
	//			Render::shadersVersoPath+"render/material/texturergba3d.330.frag");
	this->material.create(
				window, fileName,
				Render::shadersVersoPath+"render/particle/texturergba3d-billboard.330.vert",
				Render::shadersVersoPath+"render/particle/texturergba3d-billboard.330.frag",
				Render::shadersVersoPath+"render/particle/texturergba3d-billboard.330.geom");
	//this->texture->setWrapStyle(WrapStyle::ClampToEdge, WrapStyle::ClampToEdge);
	this->animatedTexture = animatedTexture;
}


void ParticleSystem::setBlendMode(BlendMode blendMode)
{
	this->blendMode = blendMode;
}


const Vector3f& ParticleSystem::getPosition() const
{
	return position;
}


void ParticleSystem::setPosition(const Vector3f& position)
{
	this->position = position;
}


ParticleBehaviour* ParticleSystem::getParticleBehaviour() const
{
	return particleBehaviour;
}


void ParticleSystem::setParticleBehaviour(ParticleBehaviour* particleBehaviour)
{
	this->particleBehaviour = particleBehaviour;
}


size_t ParticleSystem::getParticleLimit() const
{
	return particles.size();
}


void ParticleSystem::raiseParticleLimit(size_t newMaxCount)
{
	size_t oldMaxCount = particles.size();
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", oldMaxCount < newMaxCount, "", id.c_str());

	Particle p;
	p.angle = 0.0f;

	p.angleVelocity = 0.0f;

	p.size = 0.0f;
	p.startSize = 0.0f;
	p.deltaSize = 0.0f;

	p.totalLifetime = 0.0f;
	p.totalLifetimeInverse = 0.00001f;
	p.lifetimePassed = 0.0f;

	p.startAlpha = 0.0f;
	p.deltaAlpha = 0.0f;

	p.animationFrame = 0;

	if (trailSize > 0)
	{
		p.oldPosition.reserve(trailSize);
		p.oldAngle.reserve(trailSize);
		p.oldSize.reserve(trailSize);
		p.oldAnimationFrame.reserve(trailSize);
		for (size_t i=0; i<trailSize; ++i)
		{
			p.oldPosition.push_back(Vector3f());
			p.oldAngle.push_back(0.0f);
			p.oldSize.push_back(0.0f);
			p.oldAnimationFrame.push_back(0);
		}
	}
	p.trailIndex = 0;
	p.trailSize = 0;

	particles.reserve(newMaxCount);
	for (size_t i=oldMaxCount; i<newMaxCount; ++i)
		particles.push_back(p);
}

size_t ParticleSystem::getTrailSize() const
{
	return trailSize;
}


void ParticleSystem::raiseTrailSize(size_t newTrailSize)
{
	size_t oldTrailSize = trailSize;
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", newTrailSize > oldTrailSize, "", id.c_str());

	for (size_t i = 0; i < particles.size(); ++i)
	{
		Particle* p = &particles[i];
		p->oldPosition.reserve(newTrailSize);
		p->oldAngle.reserve(newTrailSize);
		p->oldSize.reserve(newTrailSize);
		p->oldAnimationFrame.reserve(newTrailSize);
		for (size_t j = oldTrailSize; j < newTrailSize; ++j)
		{
			p->oldPosition.push_back(Vector3f());
			p->oldAngle.push_back(0.0f);
			p->oldSize.push_back(0.0f);
			p->oldAnimationFrame.push_back(0);
		}
	}

	this->trailSize = newTrailSize;
}


double ParticleSystem::getTrailSaveInterval() const
{
	return this->trailSaveInterval;
}


void ParticleSystem::setTrailSaveInterval(double trailSaveInterval)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", trailSaveInterval > 0.0f, "", id.c_str());
	this->trailSaveInterval = trailSaveInterval;
}


void ParticleSystem::bindRenderingFlags(IWindowOpengl& window, const FrameTimestamp& time, const ICamera& camera)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	(void)window;

	glDepthMask(GL_FALSE);
	GL_CHECK_FOR_ERRORS("", "");

	Opengl::blend(blendMode);

	//texture->bind();
	material.apply(window, time, camera, std::vector<DirectionalLight>(),
				   std::vector<PointLight>(), std::vector<SpotLight>());
}


void ParticleSystem::unbindRenderingFlags(IWindowOpengl& window, const FrameTimestamp& time, const ICamera& camera)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	(void)window; (void)time; (void)camera;

	glDepthMask(GL_TRUE);
	GL_CHECK_FOR_ERRORS("", "");
}


bool ParticleSystem::isTimeToSaveTrailLocation(const FrameTimestamp& time)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	bool saveTrailLocation = false;
	if (trailSize != 0)
	{
		trailSaveTime += time.getDt().asSeconds();
		if (trailSaveTime > trailSaveInterval)
		{
			trailSaveTime -= trailSaveInterval;
			saveTrailLocation = true;
		}
	}
	return saveTrailLocation;
}


void ParticleSystem::calculateParticle(const FrameTimestamp& time, Particle* p,
									   bool behaviourSet, size_t index, size_t startIndex, size_t endIndex)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	float deltaSecondsFloat = static_cast<float>(time.getDt().asSeconds());
	p->lifetimePassed += deltaSecondsFloat;

	// Apply behaviour to the particle if one is set
	if (behaviourSet == true) {
		particleBehaviour->behave(time, p, particles,
								  index, startIndex, endIndex);
	}

	// Move the particle using velocity
	p->position += p->velocity * deltaSecondsFloat;
	p->angle += p->angleVelocity * deltaSecondsFloat;

	// Calculate a coefficient for calculating interpolation of size and alpha
	float coefficient = p->lifetimePassed * p->totalLifetimeInverse;

	// Calculate the alpha-value and size
	p->color.a = p->startAlpha + p->deltaAlpha * coefficient;
	p->size = p->startSize + p->deltaSize * coefficient;
	p->animationFrame = static_cast<size_t>(15.0f * coefficient);
}


void ParticleSystem::saveTrailLocations(Particle* p)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	// Save the old position
	p->oldPosition[p->trailSize] = p->position;
	p->oldAngle[p->trailIndex] = p->angle;
	p->oldSize[p->trailIndex] = p->size;
	p->oldAnimationFrame[p->trailIndex] = p->animationFrame;
	int newIndex = static_cast<int>(p->trailIndex) - 1;
	if (newIndex >= 0) {
		p->trailIndex = static_cast<size_t>(newIndex);
	}
	else {
		p->trailIndex = static_cast<uint32_t>(trailSize - 1);
	}
	p->trailSize++;
	if (p->trailSize > trailSize) {
		p->trailSize = static_cast<uint32_t>(trailSize);
	}
}


void ParticleSystem::renderParticleWithTrail(IWindowOpengl& window, const FrameTimestamp& time, const ICamera& camera, Particle* p)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	//glColor4f(p->red, p->green, p->blue, p->alpha); // \TODO: glColor needs to be implemented here using modern opengl

	// Render the particle
	if (animatedTexture == false) {
		for (size_t i = 0; i < p->trailSize; ++i) {
			size_t j = (static_cast<size_t>(p->trailIndex) + 1 + i) % trailSize;
			renderSingleParticle(window, time, camera, p->oldPosition[j],
								 p->oldSize[j], p->oldAngle[j],
								 p->color);
		}

		renderSingleParticle(window, time, camera, p->position,
							 p->size, p->angle,
							 p->color);
	}

	else {
		for (size_t i = 0; i < p->trailSize; ++i) {
			size_t j = (static_cast<size_t>(p->trailIndex) + 1 + i) % trailSize;
			renderSingleParticleAnimated(window, time, camera, p->oldPosition[j],
										 p->oldSize[j], p->oldAngle[j],
										 p->color,
										 p->oldAnimationFrame[j]);
		}

		renderSingleParticleAnimated(window, time, camera, p->position,
									 p->size, p->angle,
									 p->color,
									 p->animationFrame);
	}
}


void ParticleSystem::renderSingleParticle(IWindowOpengl& window, const FrameTimestamp& time, const ICamera& camera, const Vector3f& position,
										  float size, float angleZ, const RgbaColorf& color)
{
	(void)window; (void)time; (void)camera;
	(void)position; (void)size; (void)angleZ; (void)color;
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str()); // \TODO: implement

	vao.appendDataf(ShaderAttribute::Position, &position, 1, 1);
	//std::cout << "rendering particle (" << x << ", " << y << ", " << z << ")" << std::endl;
	/*
	// Model matrix
	Matrix4x4f model;

	// Translate image to viewport point
	model = Matrix4x4f::translate(model, x, y, z);


	// Billboard the particle (fast cheap hack)
	//float modelview[16];

	// get the current modelview matrix
	//glGetFloatv(GL_MODELVIEW_MATRIX, modelview);
	//GL_CHECK_FOR_ERRORS("", "");

	// Undo all rotations
	//  beware all scaling is lost as well!
	model.data[0][0] = 1.0f;
	model.data[0][1] = 0.0f;
	model.data[0][2] = 0.0f;
	model.data[1][0] = 0.0f;
	model.data[1][1] = 1.0f;
	model.data[1][2] = 0.0f;
	model.data[2][0] = 0.0f;
	model.data[2][1] = 0.0f;
	model.data[2][2] = 1.0f;

//	model.data[0][0] = 1.0f;
//	model.data[1][0] = 0.0f;
//	model.data[2][0] = 0.0f;
//	model.data[0][1] = 0.0f;
//	model.data[1][1] = 1.0f;
//	model.data[2][1] = 0.0f;
//	model.data[0][2] = 0.0f;
//	model.data[1][2] = 0.0f;
//	model.data[2][2] = 1.0f;

	// Rotate by Z axis
	model = Matrix4x4f::rotate(model, angleZ, Vector3f(0.0f, 0.0f, 1.0f));

	// Scale
	model = Matrix4x4f::scale(model, size, size, 1.0f);

	// Color & alpha
	material.updateDiffuseColor(RgbaColorf(red, green, blue, alpha));

	material.updateModelMatrix(model);

	Render::quadVao.render();*/


	/*
	float size2 = size / 2.0f;
	glPushMatrix();
	GL_CHECK_FOR_ERRORS("", "");
	{
		glTranslatef(x, y, z);
		GL_CHECK_FOR_ERRORS("", "");

		// Billboard the particle (fast cheap hack)
		float modelview[16];

		// get the current modelview matrix
		glGetFloatv(GL_MODELVIEW_MATRIX, modelview);
		GL_CHECK_FOR_ERRORS("", "");

		// Undo all rotations
		//  beware all scaling is lost as well!
		modelview[0] = 1.0f;
		modelview[1] = 0.0f;
		modelview[2] = 0.0f;
		modelview[4] = 0.0f;
		modelview[5] = 1.0f;
		modelview[6] = 0.0f;
		modelview[8] = 0.0f;
		modelview[9] = 0.0f;
		modelview[10] = 1.0f;

		// set the modelview with no rotations and scaling
		glLoadMatrixf(modelview);
		GL_CHECK_FOR_ERRORS("", "");

		glRotatef(angle, 0.0f, 0.0f, 1.0f);
		GL_CHECK_FOR_ERRORS("", "");

		// Render the particle
		glBegin(GL_TRIANGLE_STRIP);
		GL_CHECK_FOR_ERRORS("", "");
		{
			glTexCoord2f(1.0f, 0.0f);
			GL_CHECK_FOR_ERRORS("", "");
			glVertex3f( size2,  size2, 0);
			GL_CHECK_FOR_ERRORS("", "");

			glTexCoord2f(0.0f, 0.0f);
			GL_CHECK_FOR_ERRORS("", "");
			glVertex3f(-size2,  size2, 0);
			GL_CHECK_FOR_ERRORS("", "");

			glTexCoord2f(1.0f, 1.0f);
			GL_CHECK_FOR_ERRORS("", "");
			glVertex3f( size2, -size2, 0);
			GL_CHECK_FOR_ERRORS("", "");

			glTexCoord2f(0.0f, 1.0f);
			GL_CHECK_FOR_ERRORS("", "");
			glVertex3f(-size2, -size2, 0);
			GL_CHECK_FOR_ERRORS("", "");
		}
		glEnd();
		GL_CHECK_FOR_ERRORS("", "");

	}
	glPopMatrix();
	GL_CHECK_FOR_ERRORS("", "");
	*/
}


void ParticleSystem::renderSingleParticleAnimated(
		IWindowOpengl& window, const FrameTimestamp& time, const ICamera& camera, const Vector3f& position,
		float size, float angleZ,
		const RgbaColorf& color,
		size_t animationFrame)
{
	(void)window; (void)time; (void)camera; // \TODO: these parameters need to be used somewhere. Code broken!
	(void)position; (void)size; (void)angleZ; (void)color; (void)animationFrame;
	// \TODO: implement and/or fix ParticleSystem
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());
	VERSO_FAIL("verso-3d");
	/*
	float size2 = size / 2.0f;
	glPushMatrix();
	GL_CHECK_FOR_ERRORS("", "");
	{
		glTranslatef(x, y, z);
		GL_CHECK_FOR_ERRORS("", "");

		// Billboard the particle (fast cheap hack)
		float modelview[16];

		// get the current modelview matrix
		glGetFloatv(GL_MODELVIEW_MATRIX, modelview);
		GL_CHECK_FOR_ERRORS("", "");

		// Undo all rotations
		//  beware all scaling is lost as well!
		modelview[0] = 1.0f;
		modelview[1] = 0.0f;
		modelview[2] = 0.0f;
		modelview[4] = 0.0f;
		modelview[5] = 1.0f;
		modelview[6] = 0.0f;
		modelview[8] = 0.0f;
		modelview[9] = 0.0f;
		modelview[10] = 1.0f;

		// set the modelview with no rotations and scaling
		glLoadMatrixf(modelview);
		GL_CHECK_FOR_ERRORS("", "");

		glRotatef(angleZ, 0.0f, 0.0f, 1.0f);
		GL_CHECK_FOR_ERRORS("", "");

		const size_t aniwidth = 4;
		const size_t animHeight = 4;

		float texSizeS = 1.0f / static_cast<float>(aniwidth);
		float texSizeT = 1.0f / static_cast<float>(animHeight);
		float x0 = texSizeS * static_cast<float>(animationFrame % aniwidth);
		float x1 = x0 + texSizeS;
		float y0 = texSizeT * static_cast<float>(animationFrame / aniwidth);
		float y1 = y0 + texSizeT;

		// Render the particle
		glBegin(GL_TRIANGLE_STRIP);
		GL_CHECK_FOR_ERRORS("", "");
		{
			glTexCoord2f(x1, y0);
			GL_CHECK_FOR_ERRORS("", "");
			glVertex3f( size2,  size2, 0);
			GL_CHECK_FOR_ERRORS("", "");

			glTexCoord2f(x0, y0);
			GL_CHECK_FOR_ERRORS("", "");
			glVertex3f(-size2,  size2, 0);
			GL_CHECK_FOR_ERRORS("", "");

			glTexCoord2f(x1, y1);
			GL_CHECK_FOR_ERRORS("", "");
			glVertex3f( size2, -size2, 0);
			GL_CHECK_FOR_ERRORS("", "");

			glTexCoord2f(x0, y1);
			GL_CHECK_FOR_ERRORS("", "");
			glVertex3f(-size2, -size2, 0);
			GL_CHECK_FOR_ERRORS("", "");

		}
		glEnd();
		GL_CHECK_FOR_ERRORS("", "");

	}
	glPopMatrix();
	GL_CHECK_FOR_ERRORS("", "");*/
}


} // End namespace Verso

