#include <Verso/Render/Particle/Effects/Stars.hpp>

namespace Verso {


Stars::Stars(const UString& id) :
	created(false),
	id(id),
	particleBehaviour(nullptr),
	particleSystem(nullptr),
	particleEmitter(nullptr),
	particleSequencer(nullptr),

	particleLimit(1000),
	radiusRange(100.0f, 150.0f),
	angleRange(0),
	angleVelocityRange(0),
	endSize(100.0f),
	totalLifeTimeRange(5.0f, 10.0f),
	startAlpha(0.6f),
	burstSize(400),
	particlesPerSecond(100),
	whiteParticles(false)
{
}


Stars::~Stars()
{
	destroy();
}


void Stars::create(IWindowOpengl& window, const UString& textureFileName, bool animated)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == false, "Already created!", id.c_str());

	// setup new particle system and load textures
	particleSystem = new ParticleSystem("verso-3d/ParticleEffects/" + getType() + ": " + id);
	particleSystem->create(particleLimit);
	particleSystem->setTexture(window, textureFileName, animated);

	// Initialize the effect
	particleSystem->setBlendMode(BlendMode::Additive);

	// setup ParticleEmitter
	SphericalRandom* sphericalRandom = new SphericalRandom();
	sphericalRandom->setParticleSystem(getParticleSystem());
	sphericalRandom->setXRange(getPosition().x);
	sphericalRandom->setYRange(getPosition().y);
	sphericalRandom->setZRange(getPosition().z);
	sphericalRandom->setRadiusRange(radiusRange);

	/*
	sphericalRandom->setXVelocity(0.0f);
	sphericalRandom->setYVelocity(0.0f);
	sphericalRandom->setZVelocity(0.0f);

*/
	/*
	sphericalRandom->setX(getX() -distance, getX() + distance);
	sphericalRandom->setY(getY() -distance, getY() + distance);
	sphericalRandom->setZ(getZ() -distance, getZ() + distance);
*/
	/*
	sphericalRandom->setX(getX()-100.0f, getX()+100.0f);
	sphericalRandom->setY(getY() + -100.0f, getY() + 100.0f);
	sphericalRandom->setZ(getZ()-100.0f, getZ() + 100.0f);
*/
	/*sphericalRandom->setAngle(0.0f);
	sphericalRandom->setXVelocity(10.0f);
	sphericalRandom->setYVelocity(0.0f);
	sphericalRandom->setZVelocity(0.0f);*/

	/*sphericalRandom->setXVelocity(-15.0f, 15.0f);
	sphericalRandom->setYVelocity(-15.0f, 15.0f);
	sphericalRandom->setZVelocity(-15.0f, 15.0f);*/

	sphericalRandom->setAngleRange(angleRange);
	sphericalRandom->setAngleVelocityRange(angleVelocityRange);

	sphericalRandom->setStartSizeRange(0.0f);
	sphericalRandom->setEndSizeRange(endSize);
	sphericalRandom->setTotalLifeTimeRange(totalLifeTimeRange);

	if (whiteParticles == false) {
		sphericalRandom->setRedRange(Rangef(0.2f, 0.5f));
		sphericalRandom->setGreenRange(Rangef(0.2f, 0.5f));
		sphericalRandom->setBlueRange(Rangef(0.5f, 1.0f));
	}
	else {
		sphericalRandom->setRedRange(Rangef(1.0f));
		sphericalRandom->setGreenRange(Rangef(1.0f));
		sphericalRandom->setBlueRange(Rangef(1.0f));
	}

	sphericalRandom->setStartAlphaRange(startAlpha);
	sphericalRandom->setEndAlphaRange(0.0f);
	setParticleEmitter(sphericalRandom);

	// setup ParticleSequencer
	BurstInterval* burstInterval = new BurstInterval();
	burstInterval->setSequenceType(SequenceType::Single);
	burstInterval->setStartTime(0.0f);
	burstInterval->setBurstSize(burstSize);
	burstInterval->setInterval(0.0f);
	burstInterval->setParticlesPerSecond(particlesPerSecond);
	burstInterval->setParticleEmitter(sphericalRandom);
	burstInterval->setNeverEnding(true);
	setParticleSequencer(burstInterval);

	created = true;
}


void Stars::destroy()
{
	if (isCreated() == false) {
		return;
	}

	delete particleBehaviour;
	particleBehaviour = nullptr;

	delete particleSystem;
	particleSystem = nullptr;

	delete particleEmitter;
	particleEmitter = nullptr;

	delete particleSequencer;
	particleSequencer = nullptr;

	created = false;
}


bool Stars::isCreated() const
{
	return created;
}


void Stars::reset()
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	particleBehaviour->reset();
	particleSystem->reset();
	particleEmitter->reset();
	particleSequencer->reset();
}


void Stars::calculate(const FrameTimestamp& time)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	particleSequencer->sequence(time);
	particleSystem->calculate(time);
}


void Stars::render(IWindowOpengl& window, const FrameTimestamp& time, ICamera& camera)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	particleSystem->render(window, time, camera);
}


void Stars::calculateAndRender(IWindowOpengl& window, const FrameTimestamp& time, ICamera& camera)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	particleSequencer->sequence(time);
	particleSystem->calculateAndRender(window, time, camera);
}


bool Stars::isDone() const
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	if (particleSequencer->isDone() == true
			&& particleSystem->isEmpty() == true) {
		return true;
	}
	else {
		return false;
	}
}


UString Stars::toString() const
{
	UString str("particleLimit=");
	str.append2(particleLimit);
	str += ", radiusRange="+radiusRange.toString();
	str += ", angleRange="+angleRange.toString();
	str += ", angleVelocityRange="+angleVelocityRange.toString();
	str += "endSize=";
	str.append2(endSize);
	str += ", totalLifeTimeRange="+totalLifeTimeRange.toString();
	str += "startAlpha=";
	str.append2(startAlpha);
	str += "burstSize=";
	str.append2(burstSize);
	str += "particlesPerSecond=";
	str.append2(particlesPerSecond);
	str += "whiteParticles=";
	str.append2(whiteParticles);
	return str;
}


UString Stars::toStringDebug() const
{
	return UString(getType()+"("+toString()+")");
}


} // End namespace Verso

