#include <Verso/Render/Particle/Effects/Explosion.hpp>

namespace Verso {


Explosion::Explosion(const UString& id) :
	created(false),
	id(id),
	particleBehaviour(nullptr),
	particleSystem(nullptr),
	particleEmitter(nullptr),
	particleSequencer(nullptr),

	particleBehaviour2(nullptr),
	particleSystem2(nullptr),
	particleEmitter2(nullptr),
	particleSequencer2(nullptr)
{
}


Explosion::~Explosion()
{
	destroy();
}


void Explosion::create(IWindowOpengl& window,
					   const UString& textureFileName1,
					   const UString& textureFileName2,
					   bool animated, bool animated2)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == false, "Already created!", id.c_str());

	// setup new particle system and load textures
	particleSystem = new ParticleSystem("verso-3d/ParticleEffects/" + getType() + "::particleSystem: " + id);
	particleSystem->create(15);
	particleSystem->setTexture(window, textureFileName1, animated);

	particleSystem2 = new ParticleSystem("verso-3d/ParticleEffects/" + getType() + "::particleSystem2: " + id);
	particleSystem2->setTexture(window, textureFileName2, animated2);
	particleSystem2->create();

	// Initialize the effect
	// setup particlesystem for fire
	// - particle behaviour
	SimpleGravity* simpleGravity = new SimpleGravity();
	simpleGravity->setGravity(Vector3f(0.0f, 5.0f, 0.0f));
	setParticleBehaviour(simpleGravity);

	// - ParticleSystem
	particleSystem->setParticleBehaviour(getParticleBehaviour());
	//particleSystem->setTrailSize(50);
	//particleSystem->setTrailSaveInterval(0.01f);
	//BlendMode blendMode(BlendMode::SUBTRACTIVE);
	//BlendMode blendMode(BlendMode::TRANSCLUENT);
	particleSystem->setBlendMode(BlendMode::Additive);

	// - ParticleEmitter
	SimpleRandom* simpleRandom = new SimpleRandom();
	simpleRandom->setParticleSystem(getParticleSystem());
	float positionRange1 = 0.3f;
	simpleRandom->setXRange(Rangef(getPosition().x - positionRange1, getPosition().x + positionRange1));
	simpleRandom->setYRange(Rangef(getPosition().y - positionRange1, getPosition().y + positionRange1));
	simpleRandom->setZRange(Rangef(getPosition().z - positionRange1, getPosition().z + positionRange1));
	simpleRandom->setAngleRange(0.0f);
	//float velocity = 1.5f;
	//simpleRandom->setXVelocity(-velocity, velocity);
	//simpleRandom->setYVelocity( 0.0f);
	//simpleRandom->setZVelocity(-velocity, velocity);
	simpleRandom->setXVelocityRange(0.0f);
	simpleRandom->setYVelocityRange(0.0f);
	simpleRandom->setZVelocityRange(0.0f);
	simpleRandom->setAngleVelocityRange(Rangef(-45.0f, 45.0f));
	simpleRandom->setStartSizeRange(2.0f);
	simpleRandom->setEndSizeRange(6.0f);
	simpleRandom->setTotalLifeTimeRange(Rangef(0.3f, 0.6f));
	simpleRandom->setRedRange(0.9f);
	simpleRandom->setGreenRange(0.5f);
	simpleRandom->setBlueRange(0.1f);
	simpleRandom->setStartAlphaRange(1.0f);
	simpleRandom->setEndAlphaRange(0.0f);
	setParticleEmitter(simpleRandom);

	// - ParticleSequencer
	BurstInterval* burstInterval = new BurstInterval();
	burstInterval->setSequenceType(SequenceType::Single);
	burstInterval->setStartTime(0.0f);
	burstInterval->setEndTime(0.1f);
	burstInterval->setBurstSize(15);
	burstInterval->setInterval(3.0f);
	burstInterval->setDuration(0.0f);
	burstInterval->setParticlesPerSecond(0);
	burstInterval->setParticleEmitter(getParticleEmitter());
	setParticleSequencer(burstInterval);


	// setup particlesystem for smoke
	// - particle behaviour
	SimpleGravity* simpleGravity2 = new SimpleGravity();
	simpleGravity2->setGravity(Vector3f(0.0f, 2.0f, 0.0f));
	particleBehaviour2 = simpleGravity2;

	// - ParticleSystem
	particleSystem2->setParticleBehaviour(particleBehaviour2);
	particleSystem2->raiseTrailSize(10);
	particleSystem2->setTrailSaveInterval(0.01f);
	particleSystem2->setBlendMode(BlendMode::Subtractive);

	// - ParticleEmitter
	SimpleRandom* simpleRandom2 = new SimpleRandom();
	simpleRandom2->setParticleSystem(particleSystem2);
	float positionRange2 = 0.3f;
	simpleRandom2->setXRange(Rangef(getPosition().x - positionRange2, getPosition().x + positionRange2));
	simpleRandom2->setYRange(Rangef(getPosition().y - positionRange2, getPosition().y + positionRange2));
	simpleRandom2->setZRange(Rangef(getPosition().z - positionRange2, getPosition().z + positionRange2));
	simpleRandom2->setAngleRange(0.0f);
	float velocityRange = 1.5f;
	simpleRandom2->setXVelocityRange(Rangef(-velocityRange, velocityRange));
	simpleRandom2->setYVelocityRange(0.0f);
	simpleRandom2->setZVelocityRange(Rangef(-velocityRange, velocityRange));

	simpleRandom2->setAngleVelocityRange(Rangef(-45.0f, 45.0f));
	simpleRandom2->setStartSizeRange(3.0f);
	simpleRandom2->setEndSizeRange(6.0f);
	simpleRandom2->setTotalLifeTimeRange(Rangef(2.5f, 3.0f));
	simpleRandom2->setRedRange(1.0f);
	simpleRandom2->setGreenRange(1.0f);
	simpleRandom2->setBlueRange(1.0f);
	simpleRandom2->setStartAlphaRange(1.0f);
	simpleRandom2->setEndAlphaRange(0.0f);
	particleEmitter2 = simpleRandom2;

	// - ParticleSequencer
	BurstInterval* burstInterval2 = new BurstInterval();
	burstInterval2->setSequenceType(SequenceType::Single);
	burstInterval2->setStartTime(0.3f);
	burstInterval2->setEndTime(0.4f);
	burstInterval2->setBurstSize(5);
	burstInterval2->setInterval(3.0f);
	burstInterval2->setDuration(0.0f);
	burstInterval2->setParticlesPerSecond(0);
	burstInterval2->setParticleEmitter(particleEmitter2);
	particleSequencer2 = burstInterval2;

	created = true;
}


void Explosion::destroy()
{
	if (isCreated() == false) {
		return;
	}

	delete particleBehaviour;
	particleBehaviour = nullptr;

	delete particleSystem;
	particleSystem = nullptr;

	delete particleEmitter;
	particleEmitter = nullptr;

	delete particleSequencer;
	particleSequencer = nullptr;

	delete particleBehaviour2;
	particleBehaviour2 = nullptr;

	delete particleSystem2;
	particleSystem2 = nullptr;

	delete particleEmitter2;
	particleEmitter2 = nullptr;

	delete particleSequencer2;
	particleSequencer2 = nullptr;

	created = false;
}


bool Explosion::isCreated() const
{
	return created;
}


void Explosion::reset()
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	particleBehaviour->reset();
	particleSystem->reset();
	particleEmitter->reset();
	particleSequencer->reset();
}


void Explosion::calculate(const FrameTimestamp& time)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	particleSequencer->sequence(time);
	particleSystem->calculate(time);

	particleSequencer2->sequence(time);
	particleSystem2->calculate(time);
}


void Explosion::render(IWindowOpengl& window, const FrameTimestamp& time, ICamera& camera)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	particleSystem->render(window, time, camera);
	particleSystem2->render(window, time, camera);
}


void Explosion::calculateAndRender(IWindowOpengl& window, const FrameTimestamp& time, ICamera& camera)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	particleSequencer->sequence(time);
	particleSystem->calculateAndRender(window, time, camera);

	particleSequencer2->sequence(time);
	particleSystem2->calculateAndRender(window, time, camera);
}


bool Explosion::isDone() const
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	if (particleSequencer->isDone() == true &&
			particleSystem->isEmpty() == true &&
			particleSequencer2->isDone() == true &&
			particleSystem2->isEmpty() == true) {
		return true;
	}
	else {
		return false;
	}
}


UString Explosion::toString() const
{
	return UString("TODO");
}


UString Explosion::toStringDebug() const
{
	return UString(getType()+"("+toString()+")");
}


} // End namespace Verso

