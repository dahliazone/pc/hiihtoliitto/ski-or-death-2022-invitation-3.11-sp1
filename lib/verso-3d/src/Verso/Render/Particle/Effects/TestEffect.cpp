#include <Verso/Render/Particle/Effects/TestEffect.hpp>

namespace Verso {


TestEffect::TestEffect(const UString& id) :
	created(false),
	id(id),
	particleBehaviour(nullptr),
	particleSystem(nullptr),
	particleEmitter(nullptr),
	particleSequencer(nullptr),

	amount(1)
{
}


TestEffect::~TestEffect()
{
	destroy();
}


void TestEffect::create(IWindowOpengl& window, const UString& textureFileName, bool animated)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == false, "Already created!", id.c_str());

	// setup new particle system and load textures
	particleSystem = new ParticleSystem("verso-3d/ParticleEffects/" + getType() + ": " + id);
	particleSystem->create();
	particleSystem->setTexture(window, textureFileName, animated);

	// Initialize the effect
	// setup particlesystem for smoke
	// - particle behaviour
	SimpleGravity* simpleGravity = new SimpleGravity();
	simpleGravity->setGravity(Vector3f(0.0f, 2.0f, 0.0f));
	setParticleBehaviour(simpleGravity);

	// - ParticleSystem
	particleSystem->setParticleBehaviour(getParticleBehaviour());
	//particleSystem->setTrailSize(10);
	//particleSystem->setTrailSaveInterval(0.01f);
	particleSystem->setBlendMode(BlendMode::Subtractive);

	// - ParticleEmitter
	SimpleRandom* simpleRandom = new SimpleRandom();
	simpleRandom->setParticleSystem(particleSystem);
	float value = 0.3f;
	simpleRandom->setXRange(Rangef(getPosition().x - value, getPosition().x + value));
	simpleRandom->setYRange(Rangef(getPosition().y - value, getPosition().y + value));
	simpleRandom->setZRange(Rangef(getPosition().z - value, getPosition().z + value));
	simpleRandom->setAngleRange(0.0f);
	float value2 = 1.5f;
	simpleRandom->setXVelocityRange(Rangef(-value2, value2));
	simpleRandom->setYVelocityRange( 0.0f);
	simpleRandom->setZVelocityRange(Rangef(-value2, value2));

	simpleRandom->setAngleVelocityRange(Rangef(-45.0f, 45.0f));
	simpleRandom->setStartSizeRange(2.5f);
	simpleRandom->setEndSizeRange(5.0f);
	simpleRandom->setTotalLifeTimeRange(Rangef(1.5f, 2.0f));
	simpleRandom->setRedRange(0.3f);
	simpleRandom->setGreenRange(0.3f);
	simpleRandom->setBlueRange(0.3f);
	simpleRandom->setStartAlphaRange(0.8f);
	simpleRandom->setEndAlphaRange(0.0f);
	setParticleEmitter(simpleRandom);

	// - ParticleSequencer
	BurstInterval* burstInterval = new BurstInterval();
	burstInterval->setSequenceType(SequenceType::Single);
	burstInterval->setStartTime(0.0f);
	burstInterval->setEndTime(0.1f);
	burstInterval->setBurstSize(amount);
	burstInterval->setInterval(3.0f);
	burstInterval->setDuration(0.0f);
	burstInterval->setParticlesPerSecond(0);
	burstInterval->setParticleEmitter(getParticleEmitter());
	setParticleSequencer(burstInterval);

	created = true;
}


void TestEffect::destroy()
{
	if (isCreated() == false) {
		return;
	}

	delete particleBehaviour;
	particleBehaviour = nullptr;

	delete particleSystem;
	particleSystem = nullptr;

	delete particleEmitter;
	particleEmitter = nullptr;

	delete particleSequencer;
	particleSequencer = nullptr;

	created = false;
}


bool TestEffect::isCreated() const
{
	return created;
}


void TestEffect::reset()
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	particleBehaviour->reset();
	particleSystem->reset();
	particleEmitter->reset();
	particleSequencer->reset();
}


void TestEffect::calculate(const FrameTimestamp& time)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	particleSequencer->sequence(time);
	particleSystem->calculate(time);
}


void TestEffect::render(IWindowOpengl& window, const FrameTimestamp& time, ICamera& camera)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	particleSystem->render(window, time, camera);
}


void TestEffect::calculateAndRender(IWindowOpengl& window, const FrameTimestamp& time, ICamera& camera)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	particleSequencer->sequence(time);
	particleSystem->calculateAndRender(window, time, camera);
}


bool TestEffect::isDone() const
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	if (particleSequencer->isDone() == true
			&& particleSystem->isEmpty() == true) {
		return true;
	}
	else {
		return false;
	}
}


UString TestEffect::toString() const
{
	UString str("amount=");
	str.append2(amount);
	return str;
}


UString TestEffect::toStringDebug() const
{
	return UString(getType()+"("+toString()+")");
}


} // End namespace Verso

