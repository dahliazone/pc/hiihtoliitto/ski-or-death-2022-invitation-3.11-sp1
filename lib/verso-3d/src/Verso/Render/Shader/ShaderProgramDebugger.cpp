#include <Verso/Render/Shader/ShaderProgramDebugger.hpp>

namespace Verso {


ShaderProgramDebugger::ShaderProgramDebugger() :
	uniformTypes(),
	handle(0),
	sourceFileNames(),
	maxTypeSize(64),
	floatValues(new GLfloat[maxTypeSize]),
	intValues(new GLint[maxTypeSize]),
	uintValues(new GLuint[maxTypeSize])
{
	for (size_t i = 0; i < maxTypeSize; ++i) {
		floatValues[i] = 0.0f;
	}

	for (size_t i = 0; i < maxTypeSize; ++i) {
		intValues[i] = 0;
	}

	for (size_t i = 0; i < maxTypeSize; ++i) {
		uintValues[i] = 0;
	}
}


ShaderProgramDebugger::ShaderProgramDebugger(ShaderProgramDebugger&& original) :
	uniformTypes(std::move(original.uniformTypes)),
	handle(std::move(original.handle)),
	sourceFileNames(std::move(original.sourceFileNames)),
	maxTypeSize(std::move(original.maxTypeSize)),
	floatValues(std::move(original.floatValues)),
	intValues(std::move(original.intValues)),
	uintValues(std::move(original.uintValues))
{
	original.uniformTypes.clear();
	original.sourceFileNames.clear();
	original.floatValues = nullptr;
	original.intValues = nullptr;
	original.uintValues = nullptr;
}


ShaderProgramDebugger& ShaderProgramDebugger::operator =(ShaderProgramDebugger&& original)
{
	if (this != &original) {
		uniformTypes = std::move(original.uniformTypes);
		handle = std::move(original.handle);
		sourceFileNames = std::move(original.sourceFileNames);
		//maxTypeSize READ-ONLY
		floatValues = std::move(original.floatValues);
		intValues = std::move(original.intValues);
		uintValues = std::move(original.uintValues);

		original.uniformTypes.clear();
		original.sourceFileNames.clear();
		original.floatValues = nullptr;
		original.intValues = nullptr;
		original.uintValues = nullptr;
	}
	return *this;
}


ShaderProgramDebugger::~ShaderProgramDebugger()
{
	if (floatValues != nullptr) {
		delete[] floatValues;
	}

	if (intValues != nullptr) {
		delete[] intValues;
	}

	if (uintValues != nullptr) {
		delete[] uintValues;
	}
}


ShaderUniformType ShaderProgramDebugger::getShaderUniformType(const UString& name) const
{
	const auto& it = uniformTypes.find(name);
	if (it != uniformTypes.end()) {
		return it->second;
	}
	else {
		return ShaderUniformType::Unset;
	}
}


void ShaderProgramDebugger::saveUniformType(const UString& name, const ShaderUniformType& type)
{
	uniformTypes[name] = type;
}


void ShaderProgramDebugger::printActiveUniforms(GLuint handle, const std::vector<UString>& sourceFileNames)
{
	VERSO_LOG_INFO("verso-3d", generateActiveUniforms(handle, sourceFileNames).c_str());
}


UString ShaderProgramDebugger::generateActiveUniforms(GLuint handle, const std::vector<UString>& sourceFileNames)
{
	this->handle = handle;
	this->sourceFileNames = sourceFileNames;

	GLint nUniforms, maxLen;
	size_t typeColumnSize, nameColumnSize, valueColumnSize;
	calculateColumnMaxSizes(nUniforms, maxLen, typeColumnSize, nameColumnSize, valueColumnSize);

	UString str;
	str += generateColumnTitles(typeColumnSize, nameColumnSize, valueColumnSize);
	str += generateHorizLine(typeColumnSize, nameColumnSize, valueColumnSize);

	// Allocate temporary buffer for uniform name
	GLchar* name = new GLchar[static_cast<size_t>(maxLen)];

	std::vector<ShaderUniformDebugRow*> rows;
	for (GLuint i=0; i<static_cast<GLuint>(nUniforms); ++i) {
		GLsizei written;
		GLint size;
		GLenum type;

		glGetActiveUniform(handle, i, maxLen, &written, &size, &type, name);
		GL_CHECK_FOR_ERRORS("", sourceFileNames);

		GLint location = glGetUniformLocation(handle, name);
		GL_CHECK_FOR_ERRORS("", sourceFileNames);

		ShaderUniformType uniformType = getShaderUniformType(name);
		uniformType.actualArraySize = static_cast<size_t>(size);

		UString newName(name);
		if (uniformType.definedArraySize > 1) {
			newName = newName.substring(0, static_cast<int>(newName.size())-3);
			newName += "[]";
		}

		rows.push_back(new ShaderUniformDebugRow(name, newName, location, uniformType));
	}

	std::sort(rows.begin(), rows.end(), ShaderUniformDebugRow::ptrCompare);

	for (auto it = rows.begin(); it != rows.end(); it++) {
		if ((*it)->uniformType.typeSize > maxTypeSize) {
			str += generateRowBeforeValue(typeColumnSize, nameColumnSize, *it);
			str += "uniformType.typeSize > maxSize!";
		}

		else if ((*it)->uniformType.fetchType == ShaderUniformFetchType::Unset) {
			str += generateRowBeforeValue(typeColumnSize, nameColumnSize, *it);
			str += "Cannot fetch as type is unknown!";
		}

		else {
			str += generateRowBeforeValue(typeColumnSize, nameColumnSize, *it);
			str += generateValue(typeColumnSize, nameColumnSize, *it);
		}

		str += "\n";
	}

	str += generateHorizLine(typeColumnSize, nameColumnSize, valueColumnSize);

	delete[] name;

	return str;
}


///////////////////////////////////////////////////////////////////////////////////////////
// Private
///////////////////////////////////////////////////////////////////////////////////////////

void ShaderProgramDebugger::calculateColumnMaxSizes(
		GLint& nUniforms, GLint& maxLen,
		size_t& typeColumnSize, size_t& nameColumnSize, size_t& valueColumnSize)
{
	typeColumnSize = 4;
	nameColumnSize = 3;
	valueColumnSize = 35;

	glGetProgramiv(handle, GL_ACTIVE_UNIFORM_MAX_LENGTH, &maxLen);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);
	glGetProgramiv(handle, GL_ACTIVE_UNIFORMS, &nUniforms);
	GL_CHECK_FOR_ERRORS("", sourceFileNames);

	// Allocate temporary buffer for uniform name
	GLchar* name = new GLchar[static_cast<size_t>(maxLen)];

	// calculate column actual sizes
	for (GLuint i = 0; i < static_cast<GLuint>(nUniforms); ++i) {
		GLsizei written;
		GLint size;
		GLenum type;
		glGetActiveUniform(handle, i, maxLen, &written, &size, &type, name);
		GL_CHECK_FOR_ERRORS("", sourceFileNames);

		UString origName(name);
		UString newName = origName.substring(0, static_cast<int>(origName.size())-3);
		newName += "[]";

		size_t typeLen = getShaderUniformType(name).toString().size();
		if (typeLen > typeColumnSize) {
			typeColumnSize = typeLen;
		}

		size_t nameLen = newName.size();
		if (nameLen > nameColumnSize) {
			nameColumnSize = nameLen;
		}
	}

	delete[] name;
}


UString ShaderProgramDebugger::generateColumnTitles(size_t typeColumnSize, size_t nameColumnSize, size_t valueColumnSize)
{
	(void)valueColumnSize;

	UString typeColumnName("Type");
	UString nameColumnName("Name");

	UString str(" Location");

	str += " | ";
	str += typeColumnName;
	for (size_t i = 0; i < typeColumnSize - typeColumnName.size(); ++i) {
		str += " ";
	}

	str += " | ";
	str += nameColumnName;
	for (size_t i = 0; i < nameColumnSize - nameColumnName.size(); ++i) {
		str += " ";
	}

	str += " | Value(s)\n";
	return str;
}


UString ShaderProgramDebugger::generateHorizLine(size_t typeColumnSize, size_t nameColumnSize, size_t valueColumnSize)
{
	UString str("----------+-");
	for (size_t i=0; i<typeColumnSize; ++i)
		str += "-";
	str += "-+-";
	for (size_t i=0; i<nameColumnSize; ++i)
		str += "-";
	str += "-+-";
	for (size_t i=0; i<valueColumnSize; ++i)
		str += "-";
	str += "\n";
	return str;
}


UString ShaderProgramDebugger::generateRowBeforeValue(size_t typeColumnSize, size_t nameColumnSize,
													  const ShaderUniformDebugRow* row)
{
	std::stringstream ss;
	ss << " "<<std::setw(8)<<std::right<<row->location;
	ss << " | "<<std::setw(static_cast<int>(typeColumnSize))<<std::left<<row->uniformType.toString();
	ss << " | "<<std::setw(static_cast<int>(nameColumnSize))<<std::left<<row->newName;
	ss << " | ";
	return ss.str();
}


UString ShaderProgramDebugger::generateEmptyRowBeforeValue(size_t typeColumnSize, size_t nameColumnSize)
{
	UString str("\n          | ");
	for (size_t i=0; i<typeColumnSize; ++i)
		str += " ";

	str += " | ";
	for (size_t i=0; i<nameColumnSize; ++i)
		str += " ";

	str += " | ";
	return str;
}


UString ShaderProgramDebugger::generateValueSingle(const ShaderUniformType& uniformType, size_t index)
{
	if (uniformType.fetchType == ShaderUniformFetchType::Float) {
		UString val;
		val.append2(floatValues[index]);
		return val;
	}

	else if (uniformType.fetchType == ShaderUniformFetchType::Int) {
		UString val;
		val.append2(intValues[index]);
		return val;
	}

	else if (uniformType.fetchType == ShaderUniformFetchType::Uint) {
		UString val;
		val.append2(uintValues[index]);
		return val;
	}

	else {
		return UString("*Unknown*");
	}
}


UString ShaderProgramDebugger::generateValueVec(const ShaderUniformType& uniformType)
{
	UString str("( ");
	str += generateValueSingle(uniformType, 0);

	for (size_t i=1; i<uniformType.typeSize; ++i) {
		str += ", ";
		str += generateValueSingle(uniformType, i);
	}

	str += " )";
	return str;
}


UString ShaderProgramDebugger::generateValueMat(size_t typeColumnSize, size_t nameColumnSize, const ShaderUniformType& uniformType)
{
	size_t matrixWidth, matrixHeight;
	shaderUniformMatrixSize(uniformType.rawType, matrixWidth, matrixHeight);

	UString str("[   ");
	str += generateValueSingle(uniformType, 0);
	for (size_t j=0; j<matrixHeight; ++j) {
		for (size_t k=1; k<matrixWidth; ++k) {
			str += ", ";
			str += generateValueSingle(uniformType, j*matrixWidth+k);
			if (k == 3) {
				str += " ]";
			}
		}
		if (j != 3) {
			str += generateEmptyRowBeforeValue(typeColumnSize, nameColumnSize);
			str += "  [ ";
			str += generateValueSingle(uniformType, (j+1)*matrixWidth+0);
		}
	}

	return str;
}


UString ShaderProgramDebugger::generateValue(size_t typeColumnSize, size_t nameColumnSize,
											 const ShaderUniformDebugRow* row)
{
	size_t vecSize = shaderUniformVectorSize(row->uniformType.rawType);
	size_t matrixWidth, matrixHeight;
	shaderUniformMatrixSize(row->uniformType.rawType, matrixWidth, matrixHeight);

	UString str;
	if (row->uniformType.definedArraySize > 1) {
		str += "[ ";
	}

	for (size_t i=0; i<row->uniformType.actualArraySize; ++i) {
		if (row->uniformType.fetchType == ShaderUniformFetchType::Float) {
			for (size_t j=0; j<row->uniformType.typeSize; ++j)
				floatValues[j] = 0.0f;

			GLint newLocation = row->location;
			if (row->uniformType.actualArraySize > 1 && i>0) {
				UString newName = row->name.substring(0, static_cast<int>(row->name.size())-3);
				newName += "[";
				newName.append2(i);
				newName += "]";
				newLocation = glGetUniformLocation(handle, newName.c_str());
				GL_CHECK_FOR_ERRORS("", sourceFileNames);
			}

			glGetUniformfv(handle, newLocation, floatValues);
			GL_CHECK_FOR_ERRORS("", sourceFileNames);
		}

		else if (row->uniformType.fetchType == ShaderUniformFetchType::Int) {
			for (size_t j=0; j<row->uniformType.typeSize; ++j)
				intValues[j] = 0;

			GLint newLocation = row->location;
			if (row->uniformType.actualArraySize > 1 && i>0) {
				UString newName = row->name.substring(0, static_cast<int>(row->name.size())-3);
				newName += "[";
				newName.append2(i);
				newName += "]";
				newLocation = glGetUniformLocation(handle, newName.c_str());
				GL_CHECK_FOR_ERRORS("", sourceFileNames);
			}

			glGetUniformiv(handle, newLocation, intValues);
			GL_CHECK_FOR_ERRORS("", sourceFileNames);
		}

		else if (row->uniformType.fetchType == ShaderUniformFetchType::Uint) {
			for (size_t j=0; j<row->uniformType.typeSize; ++j)
				uintValues[j] = 0;

			GLint newLocation = row->location;
			if (row->uniformType.actualArraySize > 1 && i>0) {
				UString newName = row->name.substring(0, static_cast<int>(row->name.size())-3);
				newName += "[";
				newName.append2(i);
				newName += "]";
				newLocation = glGetUniformLocation(handle, newName.c_str());
				GL_CHECK_FOR_ERRORS("", sourceFileNames);
			}

			glGetUniformuiv(handle, newLocation, uintValues);
			GL_CHECK_FOR_ERRORS("", sourceFileNames);
		}

		if (matrixWidth > 1) {
			str += generateValueMat(typeColumnSize, nameColumnSize, row->uniformType);

			if (i != row->uniformType.actualArraySize-1) {
				str += ",";
				str += generateEmptyRowBeforeValue(typeColumnSize, nameColumnSize);
			}
		}

		else if (vecSize > 1) {
			str += generateValueVec(row->uniformType);

			if (i != row->uniformType.actualArraySize-1) {
				str += ",";
				str += generateEmptyRowBeforeValue(typeColumnSize, nameColumnSize);
				str += "  ";
			}
		}

		else {
			const size_t valuesPerRow = 5;
			if (i%valuesPerRow != 0)
				str += ", ";

			str += generateValueSingle(row->uniformType, 0);

			if (i%valuesPerRow == valuesPerRow-1)
				str += generateEmptyRowBeforeValue(typeColumnSize, nameColumnSize);
		}
	}

	if (row->uniformType.definedArraySize > 1) {
		str += " ]";
	}

	return str;
}


///////////////////////////////////////////////////////////////////////////////////////////
// toString
///////////////////////////////////////////////////////////////////////////////////////////

UString ShaderProgramDebugger::toString() const
{
	return "TODO";
}


UString ShaderProgramDebugger::toStringDebug() const
{
	UString str("ShaderProgramDebugger(");
	str += toString();
	str += ")";
	return str;
}


} // End namespace Verso

