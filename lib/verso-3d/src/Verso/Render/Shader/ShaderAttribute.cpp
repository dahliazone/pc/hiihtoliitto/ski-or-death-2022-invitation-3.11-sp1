﻿
#include <Verso/Render/Shader/ShaderAttribute.hpp>

namespace Verso {


GLuint ShaderAttribute::PositionIndex = 0;
GLuint ShaderAttribute::UvIndex = 1;
GLuint ShaderAttribute::NormalIndex = 2;
GLuint ShaderAttribute::ColorIndex = 3;
GLuint ShaderAttribute::UvHeightmapIndex = 4;
GLuint ShaderAttribute::LastDefaultIndex = ShaderAttribute::UvHeightmapIndex;

UString ShaderAttribute::PositionStr("position");
UString ShaderAttribute::UvStr("uv");
UString ShaderAttribute::NormalStr("normal");
UString ShaderAttribute::ColorStr("color");
UString ShaderAttribute::UvHeightmapStr("uvheightmap");

const ShaderAttribute ShaderAttribute::Position(ShaderAttribute::PositionIndex);
const ShaderAttribute ShaderAttribute::Uv(ShaderAttribute::UvIndex);
const ShaderAttribute ShaderAttribute::Normal(ShaderAttribute::NormalIndex);
const ShaderAttribute ShaderAttribute::Color(ShaderAttribute::ColorIndex);
const ShaderAttribute ShaderAttribute::UvHeightmap(ShaderAttribute::UvHeightmapIndex);


} // End namespace Verso

