
#include <Verso/Render/Shader/ShaderUniformType.hpp>

namespace Verso {


const ShaderUniformType ShaderUniformType::Unset(ShaderUniformRawType::Unset, ShaderUniformFetchType::Unset, 0);
const ShaderUniformType ShaderUniformType::Bool(ShaderUniformRawType::Bool, ShaderUniformFetchType::Uint, 1);
const ShaderUniformType ShaderUniformType::Int(ShaderUniformRawType::Int, ShaderUniformFetchType::Int, 1);
const ShaderUniformType ShaderUniformType::Uint(ShaderUniformRawType::Uint, ShaderUniformFetchType::Uint, 1);
const ShaderUniformType ShaderUniformType::Float(ShaderUniformRawType::Float, ShaderUniformFetchType::Float, 1);
const ShaderUniformType ShaderUniformType::Double(ShaderUniformRawType::Double, ShaderUniformFetchType::Float, 1);
const ShaderUniformType ShaderUniformType::Bvec2(ShaderUniformRawType::Bvec2, ShaderUniformFetchType::Uint, 2);
const ShaderUniformType ShaderUniformType::Bvec3(ShaderUniformRawType::Bvec3, ShaderUniformFetchType::Uint, 3);
const ShaderUniformType ShaderUniformType::Bvec4(ShaderUniformRawType::Bvec4, ShaderUniformFetchType::Uint, 4);
const ShaderUniformType ShaderUniformType::Ivec2(ShaderUniformRawType::Ivec2, ShaderUniformFetchType::Int, 2);
const ShaderUniformType ShaderUniformType::Ivec3(ShaderUniformRawType::Ivec3, ShaderUniformFetchType::Int, 3);
const ShaderUniformType ShaderUniformType::Ivec4(ShaderUniformRawType::Ivec4, ShaderUniformFetchType::Int, 4);
const ShaderUniformType ShaderUniformType::Uvec2(ShaderUniformRawType::Uvec2, ShaderUniformFetchType::Uint, 2);
const ShaderUniformType ShaderUniformType::Uvec3(ShaderUniformRawType::Uvec3, ShaderUniformFetchType::Uint, 3);
const ShaderUniformType ShaderUniformType::Uvec4(ShaderUniformRawType::Uvec4, ShaderUniformFetchType::Uint, 4);
const ShaderUniformType ShaderUniformType::Vec2(ShaderUniformRawType::Vec2, ShaderUniformFetchType::Float, 2);
const ShaderUniformType ShaderUniformType::Vec3(ShaderUniformRawType::Vec3, ShaderUniformFetchType::Float, 3);
const ShaderUniformType ShaderUniformType::Vec4(ShaderUniformRawType::Vec4, ShaderUniformFetchType::Float, 4);
const ShaderUniformType ShaderUniformType::Dvec2(ShaderUniformRawType::Dvec2, ShaderUniformFetchType::Float, 2);
const ShaderUniformType ShaderUniformType::Dvec3(ShaderUniformRawType::Dvec3, ShaderUniformFetchType::Float, 3);
const ShaderUniformType ShaderUniformType::Dvec4(ShaderUniformRawType::Dvec4, ShaderUniformFetchType::Float, 4);
const ShaderUniformType ShaderUniformType::Mat2(ShaderUniformRawType::Mat2, ShaderUniformFetchType::Float, 4);
const ShaderUniformType ShaderUniformType::Mat3(ShaderUniformRawType::Mat3, ShaderUniformFetchType::Float, 9);
const ShaderUniformType ShaderUniformType::Mat4(ShaderUniformRawType::Mat4, ShaderUniformFetchType::Float, 16);
const ShaderUniformType ShaderUniformType::Sampler2d(ShaderUniformRawType::Sampler2d, ShaderUniformFetchType::Uint, 1);
const ShaderUniformType ShaderUniformType::SamplerCube(ShaderUniformRawType::SamplerCube, ShaderUniformFetchType::Uint, 1);
const ShaderUniformType ShaderUniformType::Rgb(ShaderUniformRawType::Rgb, ShaderUniformFetchType::Float, 3);
const ShaderUniformType ShaderUniformType::Rgba(ShaderUniformRawType::Rgba, ShaderUniformFetchType::Float, 4);
//array?
//struct?
//Interface Block?


} // End namespace Verso

