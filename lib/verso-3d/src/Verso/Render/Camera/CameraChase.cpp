#include <Verso/Render/Camera/CameraChase.hpp>
#include <Verso/Display/IWindowOpengl.hpp>

namespace Verso {


CameraChase::CameraChase() :
	CameraTarget()
{
}


CameraChase::CameraChase(CameraChase&& original) noexcept :
	CameraTarget(std::move(original))
{
}


CameraChase& CameraChase::operator =(CameraChase&& original) noexcept
{
	CameraTarget::operator =(std::move(original));
	return *this;
}


CameraChase::~CameraChase()
{
}


///////////////////////////////////////////////////////////////////////////////////////////
// Interface ICamera
///////////////////////////////////////////////////////////////////////////////////////////

void CameraChase::set(const CameraParam& cameraParam, float seconds)
{
	(void)cameraParam; (void)seconds;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


Vector3f CameraChase::getPosition() const
{
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


void CameraChase::setPosition(const Vector3f& position)
{
	(void)position;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


void CameraChase::movePosition(const Vector3f& delta, float deltaTime)
{
	(void)delta; (void)deltaTime;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


Matrix4x4f CameraChase::getViewMatrix() const
{
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


Matrix4x4f CameraChase::getProjectionMatrix() const
{
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


ProjectionType CameraChase::getProjectionType() const
{
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


void CameraChase::setProjectionOrthographicRelative(
		float left, float right, float top, float bottom,
		const Rangef& nearFarPlane)
{
	(void)left; (void)right; (void)top; (void)bottom; (void)nearFarPlane;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


void CameraChase::setProjectionOrthographic(const Rangef& nearFarPlane)
{
	(void)nearFarPlane;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


void CameraChase::setProjectionPerspective(Degree fovY, const Rangef& nearFarPlane)

{
	(void)fovY; (void)nearFarPlane;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


float CameraChase::getFovY() const
{
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


void CameraChase::setFovY(float fovY)
{
	(void)fovY;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


Rangef CameraChase::getNearFarPlane() const
{
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


void CameraChase::setNearFarPlane(const Rangef& nearFarPlane)
{
	(void)nearFarPlane;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


float CameraChase::getOrthographicZoomLevel() const
{
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


void CameraChase::setOrthographicZoomLevel(float zoomLevel)
{
	(void)zoomLevel;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


void CameraChase::moveOrthographicZoomLevel(float delta, float deltaTime)
{
	(void)delta; (void)deltaTime;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


Degree CameraChase::getOrthographicRotation() const
{
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


void CameraChase::setOrthographicRotation(Degree orthographicRotation)
{
	(void)orthographicRotation;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


void CameraChase::changeOrthographicRotation(Degree delta, float deltaTime)
{
	(void)delta; (void)deltaTime;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


Vector2f CameraChase::screenToViewportPoint(
		const Vector2f& screenPointPixels, const Vector2i& viewportResolution) const
{
	(void)screenPointPixels; (void)viewportResolution;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


Vector3f CameraChase::screenToViewportPoint(
		const Vector3f& screenPointPixels, const Vector2i& viewportResolution) const
{
	(void)screenPointPixels; (void)viewportResolution;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


Vector2f CameraChase::viewportToScreenPoint(
		const Vector2f& viewportPointRelative, const Vector2i& viewportResolution) const
{
	(void)viewportPointRelative; (void)viewportResolution;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


Vector3f CameraChase::viewportToScreenPoint(
		const Vector3f& viewportPointRelative, const Vector2i& viewportResolution) const
{
	(void)viewportPointRelative; (void)viewportResolution;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


void CameraChase::onResize()
{
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


UString CameraChase::toString(const UString& newLinePadding) const
{
	(void)newLinePadding;

	UString str;
	VERSO_FAIL("verso-3d"); // \TODO: implement
	//str += hAlignToString(hAlign);
	//str += " | ";
	//str += vAlignToString(vAlign);
	//return str;
}


UString CameraChase::toStringDebug(const UString& newLinePadding) const
{
	UString str("CameraChase(");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso

