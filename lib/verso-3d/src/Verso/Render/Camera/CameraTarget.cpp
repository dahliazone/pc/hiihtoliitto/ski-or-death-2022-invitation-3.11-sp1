#include <Verso/Render/Camera/CameraTarget.hpp>
#include <Verso/Display/IWindowOpengl.hpp>

namespace Verso {


CameraTarget::CameraTarget() :
	window(nullptr),
	uniqueId(),
	useDrawableResolution(false),

	position(0.0f, 0.0f, -10.0f),
	target(0.0f, 0.0f, 0.0f),
	desiredUp(),
	projection(Matrix4x4f::Identity()),
	projectionType(ProjectionType::Unset),
	fovY(90.0f),
	nearFarPlane(0.1f, 100.0f),
	orthographicIsRelative(false),
	orthographicZoomLevel(1.0f),
	orthographicLeft(-1.0f),
	orthographicRight(1.0f),
	orthographicTop(-1.0f),
	orthographicBottom(1.0f),

	orthographicRotation(0.0f)
{
}


CameraTarget::CameraTarget(CameraTarget&& original) noexcept :
	window(std::move(original.window)),
	uniqueId(std::move(original.uniqueId)),
	useDrawableResolution(original.useDrawableResolution),

	position(std::move(original.position)),
	target(std::move(original.target)),
	desiredUp(std::move(original.desiredUp)),
	projection(std::move(original.projection)),
	projectionType(std::move(original.projectionType)),
	fovY(std::move(original.fovY)),
	nearFarPlane(std::move(original.nearFarPlane)),
	orthographicIsRelative(std::move(original.orthographicIsRelative)),
	orthographicZoomLevel(std::move(original.orthographicZoomLevel)),
	orthographicLeft(std::move(original.orthographicLeft)),
	orthographicRight(std::move(original.orthographicRight)),
	orthographicTop(std::move(original.orthographicTop)),
	orthographicBottom(std::move(original.orthographicBottom)),

	orthographicRotation(std::move(original.orthographicRotation))
{
}


CameraTarget& CameraTarget::operator =(CameraTarget&& original) noexcept
{
	if (this != &original) {
		window = std::move(original.window);
		uniqueId = std::move(original.uniqueId);
		useDrawableResolution = std::move(original.useDrawableResolution);

		position = std::move(original.position);
		target = std::move(original.target);
		desiredUp = std::move(original.desiredUp);
		projection = std::move(original.projection);
		projectionType = std::move(original.projectionType);
		fovY = std::move(original.fovY);
		nearFarPlane = std::move(original.nearFarPlane);
		orthographicIsRelative = std::move(original.orthographicIsRelative);
		orthographicZoomLevel = std::move(original.orthographicZoomLevel);
		orthographicLeft = std::move(original.orthographicLeft);
		orthographicRight = std::move(original.orthographicRight);
		orthographicTop = std::move(original.orthographicTop);
		orthographicBottom = std::move(original.orthographicBottom);

		orthographicRotation = std::move(original.orthographicRotation);
	}
	return *this;
}


CameraTarget::~CameraTarget()
{
}


void CameraTarget::reset(const Vector3f& position, const Vector3f& target, const Vector3f& desiredUp)
{
	this->position = position;
	this->target = target;
	this->desiredUp = desiredUp;
	this->projection = Matrix4x4f::Identity();
	this->projectionType = ProjectionType::Unset;
	this->fovY = 0.0f;
	this->nearFarPlane = Rangef(0.1f, 100.0f);
	this->orthographicIsRelative = false;
	this->orthographicZoomLevel = 1.0f;
	this->orthographicLeft = -1.0f;
	this->orthographicRight = 1.0f;
	this->orthographicTop = -1.0f;
	this->orthographicBottom = 1.0f;

	this->orthographicRotation = 0.0f;
}


///////////////////////////////////////////////////////////////////////////////////////////
// Interface ICamera
///////////////////////////////////////////////////////////////////////////////////////////

bool CameraTarget::create(IWindowOpengl* window, const UString& uniqueId, bool useDrawableResolution)
{
	VERSO_ASSERT("verso-3d", window != nullptr);
	this->window = window;
	this->uniqueId = uniqueId;
	this->useDrawableResolution = useDrawableResolution;

	window->getResourceManager().registerCamera(this);
	return true;
}


void CameraTarget::destroy()
{
	window->getResourceManager().unregisterCamera(uniqueId);
}


void CameraTarget::set(const CameraParam& cameraParam, float seconds)
{
	VERSO_ASSERT("verso-3d", cameraParam.cameraType == CameraType::Target);

	this->position = cameraParam.positionKeyframes.getValueInterpolated(seconds);
	this->target = cameraParam.targetKeyframes.getValueInterpolated(seconds);
	this->desiredUp = cameraParam.desiredUpKeyframes.getValueInterpolated(seconds);

	if (cameraParam.projectionType == ProjectionType::Orthographic) {
		this->orthographicZoomLevel = cameraParam.orthographicZoomLevelKeyframes.getValueInterpolated(seconds);
		this->orthographicRotation = cameraParam.orthographicRotationKeyframes.getValueInterpolated(seconds);

		if (cameraParam.orthographicIsRelative == false) {
			setProjectionOrthographic(cameraParam.nearFarPlane);
		}
		else {
			setProjectionOrthographicRelative(
						cameraParam.orthographicLeft,
						cameraParam.orthographicRight,
						cameraParam.orthographicTop,
						cameraParam.orthographicBottom,
						cameraParam.nearFarPlane);
		}
	}
	else if (cameraParam.projectionType == ProjectionType::Perspective) {
		setProjectionPerspective(
					cameraParam.fovYKeyframes.getValueInterpolated(seconds),
					cameraParam.nearFarPlane);
	}
}


UString CameraTarget::getUniqueId() const
{
	return uniqueId;
}


Vector3f CameraTarget::getPosition() const
{
	return position;
}


void CameraTarget::setPosition(const Vector3f& position)
{
	this->position = position;
}


void CameraTarget::movePosition(const Vector3f& delta, float deltaTime)
{
	this->position += delta * deltaTime;
}


Matrix4x4f CameraTarget::getViewMatrix() const
{
	if (projectionType == ProjectionType::Perspective) {
		return Matrix4x4f::lookAt(position, target, desiredUp);
	}
	else {
		// Rotate desiredUp base on orthographicRotation
		Matrix4x4f rotationMatrix(Matrix4x4f::Identity());
		rotationMatrix = Matrix4x4f::rotate(rotationMatrix, orthographicRotation, target - position);
		Vector3f v = rotationMatrix * desiredUp;
		return Matrix4x4f::lookAt(position, target, v);
	}
}


Matrix4x4f CameraTarget::getProjectionMatrix() const
{
	return projection;
}


ProjectionType CameraTarget::getProjectionType() const
{
	return projectionType;
}


void CameraTarget::setProjectionOrthographicRelative(
		float left, float right, float top, float bottom, const Rangef& nearFarPlane)
{
	this->orthographicLeft = left;
	this->orthographicRight = right;
	this->orthographicTop = top;
	this->orthographicBottom = bottom;
	this->nearFarPlane = nearFarPlane;
	this->orthographicIsRelative = true;

	projection = Matrix4x4f::orthographicProjection(
				orthographicLeft * orthographicZoomLevel,
				orthographicRight * orthographicZoomLevel,
				orthographicTop * orthographicZoomLevel,
				orthographicBottom * orthographicZoomLevel,
				nearFarPlane.minValue, nearFarPlane.maxValue);

	projectionType = ProjectionType::Orthographic;
}


void CameraTarget::setProjectionOrthographic(const Rangef& nearFarPlane)
{
	VERSO_ASSERT("verso-3d", window != nullptr && "create() must be called before using the class.");
	Vector2f resolution;
	if (useDrawableResolution == false) {
		resolution = window->getRenderResolutionf();
	}
	else {
		resolution = window->getDrawableResolutionf();
	}

	this->orthographicLeft = 0.0f;
	this->orthographicRight = resolution.x;
	this->orthographicTop = 0.0f;
	this->orthographicBottom = resolution.y;
	this->nearFarPlane = nearFarPlane;
	this->orthographicIsRelative = false;

	projection = Matrix4x4f::orthographicProjection(
				orthographicLeft * orthographicZoomLevel,
				orthographicRight * orthographicZoomLevel,
				orthographicTop * orthographicZoomLevel,
				orthographicBottom * orthographicZoomLevel,
				nearFarPlane.minValue, nearFarPlane.maxValue);

	projectionType = ProjectionType::Orthographic;
}


void CameraTarget::setProjectionPerspective(Degree fovY, const Rangef& nearFarPlane)
{
	VERSO_ASSERT("verso-3d", window != nullptr && "create() must be called before using the class.");

	this->fovY = fovY;
	this->nearFarPlane = nearFarPlane;

	AspectRatio aspectRatio;
	if (useDrawableResolution == false) {
		aspectRatio = window->getRenderDisplayAspectRatio();
	}
	else {
		aspectRatio = AspectRatio(window->getDrawableResolutioni(), ""); // \TODO: fix aspect ratio if known
	}
	projection = Matrix4x4f::perspectiveProjection(aspectRatio, fovY, nearFarPlane.minValue, nearFarPlane.maxValue);
	projectionType = ProjectionType::Perspective;
}


float CameraTarget::getFovY() const
{
	return this->fovY;
}


void CameraTarget::setFovY(float fovY)
{
	this->fovY = fovY;
	setProjectionPerspective(this->fovY, this->nearFarPlane);
}


Rangef CameraTarget::getNearFarPlane() const
{
	return nearFarPlane;
}


void CameraTarget::setNearFarPlane(const Rangef& nearFarPlane)
{
	this->nearFarPlane = nearFarPlane;
	setProjectionPerspective(this->fovY, this->nearFarPlane);
}


float CameraTarget::getOrthographicZoomLevel() const
{
	return orthographicZoomLevel;
}


void CameraTarget::setOrthographicZoomLevel(float zoomLevel)
{
	orthographicZoomLevel = zoomLevel;
	projection = Matrix4x4f::orthographicProjection(
				orthographicLeft * orthographicZoomLevel,
				orthographicRight * orthographicZoomLevel,
				orthographicTop * orthographicZoomLevel,
				orthographicBottom * orthographicZoomLevel,
				nearFarPlane.minValue, nearFarPlane.maxValue);
	projectionType = ProjectionType::Orthographic;
}


void CameraTarget::moveOrthographicZoomLevel(float delta, float deltaTime)
{
	setOrthographicZoomLevel(orthographicZoomLevel + delta * deltaTime);
}


Vector2f CameraTarget::screenToViewportPoint(const Vector2f& screenPointPixels, const Vector2i& viewportResolution) const
{
	return Vector2f(screenPointPixels.x / static_cast<float>(viewportResolution.x),
					screenPointPixels.y / static_cast<float>(viewportResolution.y));
}


Vector3f CameraTarget::screenToViewportPoint(const Vector3f& screenPointPixels, const Vector2i& viewportResolution) const
{
	return Vector3f(screenPointPixels.x / static_cast<float>(viewportResolution.x),
					screenPointPixels.y / static_cast<float>(viewportResolution.y),
					screenPointPixels.z);
}


Vector2f CameraTarget::viewportToScreenPoint(const Vector2f& viewportPointRelative, const Vector2i& viewportResolution) const
{
	return Vector2f(viewportPointRelative.x * viewportResolution.x,
					viewportPointRelative.y * viewportResolution.y);
}


Vector3f CameraTarget::viewportToScreenPoint(const Vector3f& viewportPointRelative, const Vector2i& viewportResolution) const
{
	return Vector3f(viewportPointRelative.x * viewportResolution.x,
					viewportPointRelative.y * viewportResolution.y,
					viewportPointRelative.z);
}


void CameraTarget::onResize()
{
	if (projectionType == ProjectionType::Perspective) {
		setProjectionPerspective(fovY, nearFarPlane);
	}

	else if (projectionType == ProjectionType::Orthographic && orthographicIsRelative == false) {
		setProjectionOrthographic(nearFarPlane);
	}
}


///////////////////////////////////////////////////////////////////////////////////////////
// public
///////////////////////////////////////////////////////////////////////////////////////////

const Vector3f& CameraTarget::getTarget() const
{
	return target;
}


void CameraTarget::setTarget(const Vector3f& target)
{
	this->target = target;
}


void CameraTarget::moveTarget(const Vector3f& delta)
{
	target += delta;
}


void CameraTarget::movePositionAndTarget(const Vector3f& delta, float deltaTime)
{
	position += delta * deltaTime;
	target += delta * deltaTime;
}


const Vector3f& CameraTarget::getDesiredUp() const
{
	return desiredUp;
}


void CameraTarget::setDesiredUp(const Vector3f& desiredUp)
{
	this->desiredUp = desiredUp;
}


Degree CameraTarget::getOrthographicRotation() const
{
	return orthographicRotation;
}


void CameraTarget::setOrthographicRotation(Degree orthographicRotation)
{
	this->orthographicRotation = orthographicRotation;
}


void CameraTarget::changeOrthographicRotation(Degree delta, float deltaTime)
{
	setOrthographicRotation(orthographicRotation + delta * deltaTime);
}


///////////////////////////////////////////////////////////////////////////////////////////
// toString (interface ICamera)
///////////////////////////////////////////////////////////////////////////////////////////

UString CameraTarget::toString(const UString& newLinePadding) const
{
	UString str("{");
	{
		UString newLinePadding2 = newLinePadding + "  ";
		str += "\n" + newLinePadding2 + "window=";
		if (window != nullptr) {
			str += "existing";
		}
		else {
			str += "null";
		}
		str += ",\n" + newLinePadding2 + "uniqueId=" + uniqueId;

		str += ",\n" + newLinePadding2 + "useDrawableResolution=";
		if (useDrawableResolution == true) {
			str += "true";
		}
		else {
			str += "false";
		}

		str += ",\n" + newLinePadding2 + "position=" + position.toString();
		str += ",\n" + newLinePadding2 + "target=" + target.toString();
		str += ",\n" + newLinePadding2 + "desiredUp=" + desiredUp.toString();

		str += ",\n" + newLinePadding2 + "projection=";
		str += projection.toString(newLinePadding2);

		str += ",\n" + newLinePadding2 + "projectionType=";
		str += projectionTypeToString(projectionType);

		str += ",\n" + newLinePadding2 + "fovY=";
		str.append2(fovY);

		str += ",\n" + newLinePadding2 + "nearFarPlane=";
		str += nearFarPlane.toString();

		str += ",\n" + newLinePadding2 + "orthographicIsRelative=";
		if (orthographicIsRelative == true) {
			str += "true";
		}
		else {
			str += "false";
		}

		str += ",\n" + newLinePadding2 + "orthographicZoomLevel=";
		str.append2(orthographicZoomLevel);

		str += ",\n" + newLinePadding2 + "orthographicLeft=";
		str.append2(orthographicLeft);

		str += ",\n" + newLinePadding2 + "orthographicRight=";
		str.append2(orthographicRight);

		str += ",\n" + newLinePadding2 + "orthographicTop=";
		str.append2(orthographicTop);

		str += ",\n" + newLinePadding2 + "orthographicBottom=";
		str.append2(orthographicBottom);

		str += ",\n" + newLinePadding2 + "orthographicRotation=";
		str.append2(orthographicRotation);

		str += "\n" + newLinePadding + "}";
	}

	return str;
}


UString CameraTarget::toStringDebug(const UString& newLinePadding) const
{
	UString str("CameraTarget(");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso

