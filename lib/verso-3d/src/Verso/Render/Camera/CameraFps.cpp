#include <Verso/Render/Camera/CameraFps.hpp>
#include <Verso/Display/IWindowOpengl.hpp>

namespace Verso {


CameraFps::CameraFps() :
	window(nullptr),
	uniqueId(),
	useDrawableResolution(false),

	position(),
	front(Vector3f(0.0f, 0.0f, -1.0f)),
	up(),
	right(),
	worldUp(up),
	yaw(),
	pitch(),
	//roll(roll),
	mouseSensitivity(0.25f),
	mouseFov(45.0f),
	projection(Matrix4x4f::Identity()),
	projectionType(ProjectionType::Unset),
	fovY(0),
	nearFarPlane(0.1f, 100.0f),
	orthographicIsRelative(false),
	orthographicZoomLevel(1.0f),
	orthographicLeft(-1.0f),
	orthographicRight(1.0f),
	orthographicTop(-1.0f),
	orthographicBottom(1.0f)
{
	updateCameraVectors();
}


CameraFps::CameraFps(CameraFps&& original) noexcept :
	window(std::move(original.window)),
	uniqueId(std::move(original.uniqueId)),
	useDrawableResolution(original.useDrawableResolution),

	position(std::move(original.position)),
	front(std::move(original.front)),
	up(std::move(original.up)),
	right(std::move(original.right)),
	worldUp(std::move(original.worldUp)),

	// Eular Angles
	yaw(std::move(original.yaw)),
	pitch(std::move(original.pitch)),
	//roll(std::move(original.roll)),

	// Camera options
	mouseSensitivity(std::move(original.mouseSensitivity)),
	mouseFov(std::move(original.mouseFov)),

	projection(std::move(original.projection)),
	projectionType(std::move(original.projectionType)),
	fovY(std::move(original.fovY)),

	nearFarPlane(std::move(original.nearFarPlane)),
	orthographicIsRelative(std::move(original.orthographicIsRelative)),
	orthographicZoomLevel(std::move(original.orthographicZoomLevel)),
	orthographicLeft(std::move(original.orthographicLeft)),
	orthographicRight(std::move(original.orthographicRight)),
	orthographicTop(std::move(original.orthographicTop)),
	orthographicBottom(std::move(original.orthographicBottom))
{
	original.window = nullptr;
}


CameraFps& CameraFps::operator =(CameraFps&& original) noexcept
{
	if (this != &original) {
		window = std::move(original.window);
		uniqueId = std::move(original.uniqueId);
		useDrawableResolution = std::move(original.useDrawableResolution);

		position = std::move(original.position);
		front = std::move(original.front);
		up = std::move(original.up);
		right = std::move(original.right);
		worldUp = std::move(original.worldUp);

		// Eular Angles
		yaw = std::move(original.yaw);
		pitch = std::move(original.pitch);
		//roll = std::move(original.roll);

		// Camera options
		mouseSensitivity = std::move(original.mouseSensitivity);
		mouseFov = std::move(original.mouseFov);

		projection = std::move(original.projection);
		projectionType = std::move(original.projectionType);
		fovY = std::move(original.fovY);

		nearFarPlane = std::move(original.nearFarPlane);
		orthographicIsRelative = std::move(original.orthographicIsRelative);
		orthographicZoomLevel = std::move(original.orthographicZoomLevel);
		orthographicLeft = std::move(original.orthographicLeft);
		orthographicRight = std::move(original.orthographicRight);
		orthographicTop = std::move(original.orthographicTop);
		orthographicBottom = std::move(original.orthographicBottom);


		original.window = nullptr;
	}
	return *this;
}


CameraFps::~CameraFps()
{
}


void CameraFps::reset(const Vector3f& position, const Vector3f& up, Degree yaw, Degree pitch)
{
	this->position = position;
	this->worldUp = up;
	this->yaw = yaw;
	this->pitch = pitch;
	//this->pitch(roll),
	this->mouseSensitivity = 0.25f;
	this->mouseFov = 45.0f;
	this->projection = Matrix4x4f::Identity();
	this->projectionType = ProjectionType::Unset;
	this->fovY = 0;
	this->nearFarPlane = Rangef(0.1f, 100.0f);
	this->orthographicIsRelative = false;
	this->orthographicZoomLevel = 1.0f;
	this->orthographicLeft = -1.0f;
	this->orthographicRight = 1.0f;
	this->orthographicTop = -1.0f;
	this->orthographicBottom = 1.0f;

	updateCameraVectors();
}


///////////////////////////////////////////////////////////////////////////////////////////
// Interface ICamera
///////////////////////////////////////////////////////////////////////////////////////////

bool CameraFps::create(IWindowOpengl* window, const UString& uniqueId, bool useDrawableResolution)
{
	VERSO_ASSERT("verso-3d", window != nullptr);
	this->window = window;
	this->uniqueId = uniqueId;
	this->useDrawableResolution = useDrawableResolution;

	window->getResourceManager().registerCamera(this);
	return true;
}


void CameraFps::destroy()
{
	window->getResourceManager().unregisterCamera(uniqueId);
}


void CameraFps::set(const CameraParam& cameraParam, float seconds)
{
	VERSO_ASSERT("verso-3d", cameraParam.cameraType == CameraType::Fps);
	VERSO_FAIL("verso-3d");
	(void)seconds;

	//this->position = cameraParam.positionKeyframes.getValueInterpolated(seconds);
	//// \TODO: this->front = cameraParam.asd;
	//// \TODO: this->up = cameraParam.asd;
	//// \TODO: this->right = cameraParam.asd;
	//// \TODO: this->worldUp = cameraParam.asd;

	//// \TODO: this->yaw = cameraParam.yaw;
	//// \TODO: this->pitch = cameraParam.pitch;
	////this->roll = cameraParam.roll;

	//// \TODO: this->projection = cameraParam.p;
	//this->projectionType = cameraParam.projectionType;
	//this->fovY = cameraParam.fovYKeyframes.getValueInterpolated(seconds);

	//this->nearFarPlane = cameraParam.nearFarPlane;
	//this->orthographicIsRelative = cameraParam.orthographicIsRelative;
	//this->orthographicZoomLevel = cameraParam.orthographicZoomLevelKeyframes.getValueInterpolated(seconds);
	//this->orthographicLeft = cameraParam.orthographicLeft;
	//this->orthographicRight = cameraParam.orthographicRight;
	//this->orthographicTop = cameraParam.orthographicTop;
	//this->orthographicBottom = cameraParam.orthographicBottom;
}


UString CameraFps::getUniqueId() const
{
	return uniqueId;
}


Vector3f CameraFps::getPosition() const
{
	return position;
}


void CameraFps::setPosition(const Vector3f& position)
{
	this->position = position;
	updateCameraVectors();
}


// x=left/right velocity, y=up/down, z=forward/backward
void CameraFps::movePosition(const Vector3f& delta, float deltaTime)
{
	// default way to move would be:
	// 	position += delta * deltaTime;
	// but we should move into the direction where camera is facing.
	position += right * delta.x * deltaTime;
	position += up * delta.y * deltaTime;
	position += -front * delta.z * deltaTime;
}


Matrix4x4f CameraFps::getViewMatrix() const
{
	return Matrix4x4f::lookAt(position, position + front, up);
}


Matrix4x4f CameraFps::getProjectionMatrix() const
{
	VERSO_ASSERT("verso-3d", projectionType != ProjectionType::Unset && "setProjection*() must be called!");

	return projection;
}


ProjectionType CameraFps::getProjectionType() const
{
	return projectionType;
}


void CameraFps::setProjectionOrthographicRelative(
		float left, float right, float top, float bottom, const Rangef& nearFarPlane)
{
	this->orthographicLeft = left;
	this->orthographicRight = right;
	this->orthographicTop = top;
	this->orthographicBottom = bottom;
	this->nearFarPlane = nearFarPlane;
	this->orthographicIsRelative = true;

	projection = Matrix4x4f::orthographicProjection(
					 orthographicLeft * orthographicZoomLevel,
					 orthographicRight * orthographicZoomLevel,
					 orthographicTop * orthographicZoomLevel,
					 orthographicBottom * orthographicZoomLevel,
					 nearFarPlane.minValue, nearFarPlane.maxValue);

	projectionType = ProjectionType::Orthographic;
}


void CameraFps::setProjectionOrthographic(const Rangef& nearFarPlane)
{
	VERSO_ASSERT("verso-3d", window != nullptr && "create() must be called before using the class.");
	Vector2f resolution;
	if (useDrawableResolution == false) {
		resolution = window->getRenderResolutionf();
	}
	else {
		resolution = window->getDrawableResolutionf();
	}

	this->orthographicLeft = 0.0f;
	this->orthographicRight = resolution.x;
	this->orthographicTop = 0.0f;
	this->orthographicBottom = resolution.y;
	this->nearFarPlane = nearFarPlane;
	this->orthographicIsRelative = false;

	projection = Matrix4x4f::orthographicProjection(
					 orthographicLeft * orthographicZoomLevel,
					 orthographicRight * orthographicZoomLevel,
					 orthographicTop * orthographicZoomLevel,
					 orthographicBottom * orthographicZoomLevel,
					 nearFarPlane.minValue, nearFarPlane.maxValue);

	projectionType = ProjectionType::Orthographic;
}


void CameraFps::setProjectionPerspective(Degree fovY, const Rangef& nearFarPlane)
{
	VERSO_ASSERT("verso-3d", window != nullptr && "create() must be called before using the class.");

	this->fovY = fovY;
	this->nearFarPlane = nearFarPlane;

	AspectRatio aspectRatio;
	if (useDrawableResolution == false) {
		aspectRatio = window->getRenderDisplayAspectRatio();
	}
	else {
		aspectRatio = AspectRatio(window->getDrawableResolutioni(), ""); // \TODO: fix aspect ratio if known
	}
	projection = Matrix4x4f::perspectiveProjection(aspectRatio, fovY, nearFarPlane.minValue, nearFarPlane.maxValue);
	projectionType = ProjectionType::Perspective;
}


float CameraFps::getFovY() const
{
	return this->fovY;
}


void CameraFps::setFovY(float fovY)
{
	this->fovY = fovY;
	setProjectionPerspective(this->fovY, this->nearFarPlane);
}


Rangef CameraFps::getNearFarPlane() const
{
	return nearFarPlane;
}


void CameraFps::setNearFarPlane(const Rangef& nearFarPlane)
{
	this->nearFarPlane = nearFarPlane;
	setProjectionPerspective(this->fovY, this->nearFarPlane);
}


float CameraFps::getOrthographicZoomLevel() const
{
	return orthographicZoomLevel;
}


void CameraFps::setOrthographicZoomLevel(float zoomLevel)
{
	orthographicZoomLevel = zoomLevel;
	projection = Matrix4x4f::orthographicProjection(
				orthographicLeft * orthographicZoomLevel,
				orthographicRight * orthographicZoomLevel,
				orthographicTop * orthographicZoomLevel,
				orthographicBottom * orthographicZoomLevel,
				nearFarPlane.minValue, nearFarPlane.maxValue);
	projectionType = ProjectionType::Orthographic;
}


void CameraFps::moveOrthographicZoomLevel(float delta, float deltaTime)
{
	setOrthographicZoomLevel(orthographicZoomLevel + delta * deltaTime);
}


Vector2f CameraFps::screenToViewportPoint(const Vector2f& screenPointPixels, const Vector2i& viewportResolution) const
{
	return Vector2f(screenPointPixels.x / static_cast<float>(viewportResolution.x),
					screenPointPixels.y / static_cast<float>(viewportResolution.y));
}


Vector3f CameraFps::screenToViewportPoint(const Vector3f& screenPointPixels, const Vector2i& viewportResolution) const
{
	return Vector3f(screenPointPixels.x / static_cast<float>(viewportResolution.x),
					screenPointPixels.y / static_cast<float>(viewportResolution.y),
					screenPointPixels.z);
}


Vector2f CameraFps::viewportToScreenPoint(const Vector2f& viewportPointRelative, const Vector2i& viewportResolution) const
{
	return Vector2f(viewportPointRelative.x * viewportResolution.x,
					viewportPointRelative.y * viewportResolution.y);
}


Vector3f CameraFps::viewportToScreenPoint(const Vector3f& viewportPointRelative, const Vector2i& viewportResolution) const
{
	return Vector3f(viewportPointRelative.x * viewportResolution.x,
					viewportPointRelative.y * viewportResolution.y,
					viewportPointRelative.z);
}


void CameraFps::onResize()
{
	if (projectionType == ProjectionType::Perspective) {
		setProjectionPerspective(fovY, nearFarPlane);
	}

	else if (projectionType == ProjectionType::Orthographic && orthographicIsRelative == false) {
		setProjectionOrthographic(nearFarPlane);
	}
}


///////////////////////////////////////////////////////////////////////////////////////////
// public
///////////////////////////////////////////////////////////////////////////////////////////

// @return x=yaw in degrees, y=pitch in degrees, z=roll in degrees (always zero)
Vector3f CameraFps::getOrientation() const
{
	return Vector3f(yaw, pitch, 0.0f);
}


void CameraFps::setOrientation(Degree yaw, Degree pitch, bool constrainPitch)
{
	this->yaw = yaw;
	this->pitch = pitch;

	// Make sure that when pitch is out of bounds, screen doesn't get flipped
	if (constrainPitch) {
		if (this->pitch > 89.0f) {
			this->pitch = 89.0f;
		}
		if (this->pitch < -89.0f) {
			this->pitch = -89.0f;
		}
	}

	updateCameraVectors();
}


// Processes input received from a mouse input system. Expects the offset value in both the x and y direction.
void CameraFps::processMouseMovement(float xoffset, float yoffset, bool constrainPitch)
{
	xoffset *= mouseSensitivity;
	yoffset *= mouseSensitivity;

	yaw   += xoffset;
	pitch += yoffset;

	// Make sure that when pitch is out of bounds, screen doesn't get flipped
	if (constrainPitch) {
		if (pitch > 89.0f) {
			pitch = 89.0f;
		}
		if (pitch < -89.0f) {
			pitch = -89.0f;
		}
	}

	// Update Front, Right and Up Vectors using the updated Eular angles
	updateCameraVectors();
}


// Processes input received from a mouse scroll-wheel event. Only requires input on the vertical wheel-axis
void CameraFps::processMouseScroll(float yoffset)
{
	if (mouseFov >= 1.0f && mouseFov <= 45.0f) {
		mouseFov -= yoffset;
	}
	if (mouseFov <= 1.0f) {
		mouseFov = 1.0f;
	}
	if (mouseFov >= 45.0f) {
		mouseFov = 45.0f;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////
// private
///////////////////////////////////////////////////////////////////////////////////////////
void CameraFps::updateCameraVectors()
{
	// Calculate the new Front vector
	front.x = std::cos(Math::degreesToRadians(yaw));// * std::cos(Math::degreesToRadians(pitch));
	front.y = std::sin(Math::degreesToRadians(pitch));
	front.z = std::sin(Math::degreesToRadians(yaw));// * std::cos(Math::degreesToRadians(pitch));
	front.normalize();

	// Also re-calculate the Right and Up vector
	right = front.crossProduct(worldUp).getNormalized();
	up    = right.crossProduct(front).getNormalized();
}


///////////////////////////////////////////////////////////////////////////////////////////
// toString (interface ICamera)
///////////////////////////////////////////////////////////////////////////////////////////

UString CameraFps::toString(const UString& newLinePadding) const
{
	UString str("{");
	{
		UString newLinePadding2 = newLinePadding + "  ";
		str += "\n" + newLinePadding2 + "window=";
		if (window != nullptr) {
			str += "existing";
		}
		else {
			str += "null";
		}
		str += ",\n" + newLinePadding2 + "uniqueId=" + uniqueId;

		str += ",\n" + newLinePadding2 + "useDrawableResolution=";
		if (useDrawableResolution == true) {
			str += "true";
		}
		else {
			str += "false";
		}

		str += ",\n" + newLinePadding2 + "position=" + position.toString();
		str += ",\n" + newLinePadding2 + "front=" + front.toString();
		str += ",\n" + newLinePadding2 + "up=" + up.toString();
		str += ",\n" + newLinePadding2 + "right=" + right.toString();
		str += ",\n" + newLinePadding2 + "worldUp=" + worldUp.toString();

		str += ",\n" + newLinePadding2 + "yaw=";
		str.append2(yaw);
		str += ",\n" + newLinePadding2 + "pitch=";
		str.append2(pitch);
		//str += ",\n" + newLinePadding2 + "roll=";
		//str.append2(roll);

		str += ",\n" + newLinePadding2 + "mouseSensitivity=";
		str.append2(mouseSensitivity);
		str += ",\n" + newLinePadding2 + "mouseFov=";
		str.append2(mouseFov);

		str += ",\n" + newLinePadding2 + "projection=" + projection.toString(newLinePadding2);

		str += ",\n" + newLinePadding2 + "projectionType=";
		str += projectionTypeToString(projectionType);

		str += ",\n" + newLinePadding2 + "fovY=";
		str.append2(fovY);

		str += ",\n" + newLinePadding2 + "nearFarPlane=";
		str += nearFarPlane.toString();

		str += ",\n" + newLinePadding2 + "orthographicIsRelative=";
		if (orthographicIsRelative == true) {
			str += "true";
		}
		else {
			str += "false";
		}

		str += ",\n" + newLinePadding2 + "orthographicZoomLevel=";
		str.append2(orthographicZoomLevel);

		str += ",\n" + newLinePadding2 + "orthographicLeft=";
		str.append2(orthographicLeft);

		str += ",\n" + newLinePadding2 + "orthographicRight=";
		str.append2(orthographicRight);

		str += ",\n" + newLinePadding2 + "orthographicTop=";
		str.append2(orthographicTop);

		str += ",\n" + newLinePadding2 + "orthographicBottom=";
		str.append2(orthographicBottom);

		str += "\n" + newLinePadding + "}";
	}

	return str;
}


UString CameraFps::toStringDebug(const UString& newLinePadding) const
{
	UString str("CameraFps(");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso

