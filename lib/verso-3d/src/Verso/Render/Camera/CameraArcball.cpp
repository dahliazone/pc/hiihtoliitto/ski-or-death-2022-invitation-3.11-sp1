#include <Verso/Render/Camera/CameraArcball.hpp>
#include <Verso/Display/IWindowOpengl.hpp>

namespace Verso {


CameraArcball::CameraArcball():
	CameraTarget()
{
}


CameraArcball::CameraArcball(CameraArcball&& original) noexcept :
	CameraTarget(std::move(original))
{
}


CameraArcball& CameraArcball::operator =(CameraArcball&& original) noexcept
{
	CameraTarget::operator =(std::move(original));
	return *this;
}


CameraArcball::~CameraArcball()
{
}


///////////////////////////////////////////////////////////////////////////////////////////
// Interface ICamera
///////////////////////////////////////////////////////////////////////////////////////////

void CameraArcball::set(const CameraParam& cameraParam, float seconds)
{
	(void)cameraParam; (void)seconds;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


Vector3f CameraArcball::getPosition() const
{
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


void CameraArcball::setPosition(const Vector3f& position)
{
	(void)position;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


void CameraArcball::movePosition(const Vector3f& delta, float deltaTime)
{
	(void)delta; (void)deltaTime;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


Matrix4x4f CameraArcball::getViewMatrix() const
{
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


Matrix4x4f CameraArcball::getProjectionMatrix() const
{
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


ProjectionType CameraArcball::getProjectionType() const
{
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class
}


void CameraArcball::setProjectionOrthographicRelative(
		float left, float right, float top, float bottom,
		const Rangef& nearFarPlane)
{
	(void)left; (void)right; (void)top; (void)bottom; (void)nearFarPlane;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


void CameraArcball::setProjectionOrthographic(const Rangef& nearFarPlane)
{
	(void)nearFarPlane;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


void CameraArcball::setProjectionPerspective(Degree fovY, const Rangef& nearFarPlane)
{
	(void)fovY; (void)nearFarPlane;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


float CameraArcball::getFovY() const
{
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


void CameraArcball::setFovY(float fovY)
{
	(void)fovY;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


Rangef CameraArcball::getNearFarPlane() const
{
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


void CameraArcball::setNearFarPlane(const Rangef& nearFarPlane)
{
	(void)nearFarPlane;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


float CameraArcball::getOrthographicZoomLevel() const
{
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


void CameraArcball::setOrthographicZoomLevel(float zoomLevel)
{
	(void)zoomLevel;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


void CameraArcball::moveOrthographicZoomLevel(float delta, float deltaTime)
{
	(void)delta; (void)deltaTime;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


Vector2f CameraArcball::screenToViewportPoint(
		const Vector2f& screenPointPixels, const Vector2i& viewportResolution) const
{
	(void)screenPointPixels; (void)viewportResolution;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


Vector3f CameraArcball::screenToViewportPoint(
		const Vector3f& screenPointPixels, const Vector2i& viewportResolution) const
{
	(void)screenPointPixels; (void)viewportResolution;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


Vector2f CameraArcball::viewportToScreenPoint(
		const Vector2f& viewportPointRelative, const Vector2i& viewportResolution) const
{
	(void)viewportPointRelative; (void)viewportResolution;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


Vector3f CameraArcball::viewportToScreenPoint(
		const Vector3f& viewportPointRelative, const Vector2i& viewportResolution) const
{
	(void)viewportPointRelative; (void)viewportResolution;
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


void CameraArcball::onResize()
{
	VERSO_FAIL("verso-3d"); // \TODO: Implement this camera class!
}


UString CameraArcball::toString(const UString& newLinePadding) const
{
	(void)newLinePadding;

	UString str;
	VERSO_FAIL("verso-3d"); // \TODO: implement
	//str += hAlignToString(hAlign);
	//str += " | ";
	//str += vAlignToString(vAlign);
	//return str;
}


UString CameraArcball::toStringDebug(const UString& newLinePadding) const
{
	UString str("CameraArcball(");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso

