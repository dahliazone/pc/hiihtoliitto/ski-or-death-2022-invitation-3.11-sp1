#include <Verso/Render/Vao.hpp>
#include <Verso/Render/RenderStats.hpp>

namespace Verso {


GLuint Vao::lastBoundHandle = 0;


Vao::Vao(const UString& id) :
	created(false),
	id(id),
	handle(0),
	vbos(),
	ebo(),
	primitiveType(PrimitiveType::Unset),
	vertexCount(0)
{
}


Vao::Vao(Vao&& original) :
	created(std::move(original.created)),
	id(std::move(original.id)),
	handle(std::move(original.handle)),
	vbos(std::move(original.vbos)),
	ebo(std::move(original.ebo)),
	primitiveType(std::move(original.primitiveType)),
	vertexCount(std::move(original.vertexCount))
{
	original.created = false;
	original.id.clear();
	original.handle = 0;
	original.vbos.clear();
	//original.ebo // move semantics in class Ebo should handle this
	original.primitiveType = PrimitiveType::Unset;
	original.vertexCount = 0;
}


Vao& Vao::operator =(Vao&& original)
{
	if (this != &original) {
		created = std::move(original.created);
		id = std::move(original.id);
		handle = std::move(original.handle);
		vbos = std::move(original.vbos);
		ebo = std::move(original.ebo);
		primitiveType = std::move(original.primitiveType);
		vertexCount = std::move(original.vertexCount);

		original.created = false;
		original.id.clear();
		original.handle = 0;
		original.vbos.clear();
		//original.ebo.clear(); // Move semantics in class Ebo should handle this
		original.primitiveType = PrimitiveType::Unset;
		original.vertexCount = 0;
	}
	return *this;
}


Vao::~Vao()
{
	destroy();
}


void Vao::create(PrimitiveType primitiveType)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == false, "Already created!", id.c_str());

	glGenVertexArrays(1, &handle);
	GL_CHECK_FOR_ERRORS("", id);

	this->primitiveType = primitiveType;

	this->vertexCount = 0;

	this->created = true;
}


void Vao::destroy() VERSO_NOEXCEPT
{
	if (isCreated() == false) {
		return;
	}

	vertexCount = 0;
	primitiveType = PrimitiveType::Unset;

	bind();
	{
		ebo.unbind();
		ebo.destroy();
		for (size_t i=0; i<vbos.size(); ++i) {
			if (vbos[i].isCreated()) {
				glDisableVertexAttribArray(static_cast<GLuint>(i));
				GL_CHECK_FOR_ERRORS("", id);
			}
			vbos[i].destroy();
		}
		vbos.clear();
	}
	unbind();

	glDeleteVertexArrays(1, &handle);
	GL_CHECK_FOR_ERRORS("", id);

	handle = 0;
	created = false;
}


bool Vao::isCreated() const
{
	return created;
}


void Vao::bind()
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	if (lastBoundHandle != handle) { // \TODO: fatal bug VAOs are OpenGL context sensitive so single global var doesn't just cut it
		glBindVertexArray(handle);
		GL_CHECK_FOR_ERRORS("", id);

		lastBoundHandle = handle;
	}
}


void Vao::unbind()
{
	if (lastBoundHandle != 0) { // \TODO: fatal bug VAOs are OpenGL context sensitive so single global var doesn't just cut it
		glBindVertexArray(0);
		GL_CHECK_FOR_ERRORS("", "");

		lastBoundHandle = 0;
	}
}


void Vao::setDataf(const ShaderAttribute& attribute, const GLvoid* data, int vertexCount, int perVertexCount, const BufferUsagePattern& bufferUsagePattern)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", vertexCount > 0, "", id.c_str());
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", perVertexCount > 0, "", id.c_str());

	if (this->vertexCount == 0) {
		this->vertexCount = vertexCount;
	}
	else if (this->vertexCount != vertexCount) {
		UString error("Current internal vertexCount=");
		error.append2(this->vertexCount);
		error += " and given vertexCount=";
		error.append2(vertexCount);
		error += " don't match!";
		VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), "");
	}

	bind();

	size_t vboIndex = attribute.getIndex();
	while (vbos.size() < vboIndex+1) {
		vbos.push_back(Vbo());
	}
	if (vbos[vboIndex].isCreated() == false)
		vbos[vboIndex].create();

	vbos[vboIndex].setDataf(attribute, data, vertexCount * perVertexCount, perVertexCount, bufferUsagePattern);

	// Specify that our coordinate data is going into attribute index, and contains perVertexCount floats per vertex
	GLboolean normalize = GL_FALSE; // \TODO: add support for normalization
	glVertexAttribPointer(attribute.getIndex(), static_cast<GLint>(perVertexCount), GL_FLOAT, normalize, 0, nullptr);
	GL_CHECK_FOR_ERRORS("", id);

	// Enable attribute index as being used
	glEnableVertexAttribArray(attribute.getIndex());
	GL_CHECK_FOR_ERRORS("", id);

	// It is common practice to unbind OpenGL objects when we're done configuring them
	// so we don't mistakenly (mis)configure them elsewhere.
	unbind();
}


void Vao::appendDataf(const ShaderAttribute& attribute, const GLvoid* data, int vertexCount, int perVertexCount)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", vertexCount > 0, "", id.c_str());
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", perVertexCount > 0, "", id.c_str());

	bind();

	size_t vboIndex = attribute.getIndex();
	while (vbos.size() < vboIndex+1) {
		vbos.push_back(Vbo());
	}
	if (vbos[vboIndex].isCreated() == false) {
		vbos[vboIndex].create();
	}

	vbos[vboIndex].appendDataf(attribute, data, vertexCount * perVertexCount, perVertexCount);

	// It is common practice to unbind OpenGL objects when we're done configuring them
	// so we don't mistakenly (mis)configure them elsewhere.
	unbind();
}


void Vao::setIndices(const GLuint* indices, int indicesCount, const BufferUsagePattern& bufferUsagePattern)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", indicesCount > 0, "", id.c_str());

	bind();

	if (!ebo.isCreated()) {
		ebo.create();
		ebo.bind();
	}
	ebo.setIndices(indices, indicesCount, bufferUsagePattern);

	unbind();
}


void Vao::appendIndices(const GLuint* indices, int indicesCount)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", indicesCount > 0, "", id.c_str());

	if (!ebo.isCreated()) {
		// Create and bind Ebo to Vao
		bind();
		ebo.create();
		ebo.bind();
		unbind();
	}

	ebo.appendIndices(indices, indicesCount);
}


void Vao::applyAppendedData(const BufferUsagePattern& bufferUsagePattern)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	bind();

	for (size_t i=0; i<vbos.size(); ++i) {
		if (vbos[i].isCreated() == true) {
			vbos[i].applyAppendedData(bufferUsagePattern);
			int count= vbos[i].getEntryCount();
			if (count > this->vertexCount) {
				this->vertexCount = count;
			}

			// Specify that our coordinate data is going into attribute index, and contains perVertexCount floats per vertex
			GLboolean normalize = GL_FALSE; // \TODO: add support for normalization
			glVertexAttribPointer(vbos[i].getShaderAttribute().getIndex(), static_cast<GLint>(vbos[i].getPerVertexCount()), GL_FLOAT, normalize, 0, nullptr);
			GL_CHECK_FOR_ERRORS("", id);

			// Enable attribute index as being used
			glEnableVertexAttribArray(vbos[i].getShaderAttribute().getIndex());
			GL_CHECK_FOR_ERRORS("", id);
		}
	}

	ebo.applyAppendedData(bufferUsagePattern);

	unbind();
}


void Vao::render()
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());

	bind();
	if (ebo.isCreated() && ebo.getIndicesCount() > 0 && vbos.size() > 0) { // Indexed drawing
		glDrawElements(primitiveTypeToGlenum(primitiveType), ebo.getIndicesCount(), GL_UNSIGNED_INT, nullptr);
		GL_CHECK_FOR_ERRORS("", id);
		RenderStats::addPrimitives(primitiveType, ebo.getIndicesCount());
	}
	else if (vbos.size() > 0) { // Array drawing
		glDrawArrays(primitiveTypeToGlenum(primitiveType), 0, vertexCount);
		GL_CHECK_FOR_ERRORS("", id);
		RenderStats::addPrimitives(primitiveType, vertexCount);
	}
	RenderStats::addMeshes(1);
	GL_CHECK_FOR_ERRORS("", id);
	unbind();
}


void Vao::render(GLint first, GLsizei count)
{
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", isCreated() == true, "", id.c_str());
	VERSO_ASSERT_MSG_AND_RESOURCES("verso-3d", first+count > static_cast<GLint>(ebo.getIndicesCount()), "", id.c_str());

	bind();
	if (ebo.isCreated() && ebo.getIndicesCount() > 0 && vbos.size() > 0) { // Indexed drawing
		char* nullPtr = nullptr;
		glDrawElements(primitiveTypeToGlenum(primitiveType), count, GL_UNSIGNED_INT, &nullPtr + first);
		RenderStats::addPrimitives(primitiveType, count);
	}
	else if (vbos.size() > 0) { // Array drawing
		glDrawArrays(primitiveTypeToGlenum(primitiveType), first, count);
		RenderStats::addPrimitives(primitiveType, count);
	}
	GL_CHECK_FOR_ERRORS("", id);
	unbind();
}



UString Vao::getId() const
{
	return id;
}



void Vao::setId(const UString& id)
{
	this->id = id;
}


const PrimitiveType& Vao::getPrimitiveType() const
{
	return primitiveType;
}


int Vao::getVertexCount() const
{
	return vertexCount;
}


int Vao::getNextDatafAppendIndex(const ShaderAttribute& attribute) const
{
	size_t vboIndex = attribute.getIndex();
	if (vboIndex < vbos.size()) {
		return vbos[vboIndex].getAppendedEntryCount();
	}
	return 0;
}


int Vao::getNextIndexAppendIndex() const
{
	return ebo.getNextAppendIndex();
}


// toString

UString Vao::toString() const
{
	UString str;
	if (isCreated() == false) {
		str += "isCreated() == false";
	}
	else {
		str += "handle=";
		str.append2(handle);
		str += ", primitiveType=";
		str += primitiveTypeToString(primitiveType);
		str += ", vertexCount=";
		str.append2(vertexCount);
		str += ", vbos=[ ";
		for (const auto& vbo : vbos) {
			str += vbo.toStringDebug();
			str += ", ";
		}
		str += " ]";
		if (ebo.isCreated()) {
			str += ", ebo=";
			str += ebo.toStringDebug();
		}
	}
	return str;
}


UString Vao::toStringDebug() const
{
	UString str("Vao(");
	str += toString();
	str += ")";
	return str;
}


} // End namespace Verso

