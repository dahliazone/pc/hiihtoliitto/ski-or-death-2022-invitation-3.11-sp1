
#include <Verso/Render/Vao/Vbo.hpp>

namespace Verso {


GLuint Vbo::lastBoundHandle = 0;


Vbo::Vbo() :
	created(false),
	handle(0),
	shaderAttribute(),
	entryCount(0),
	entryBytes(0),
	perVertexCount(0),
	bufferUsagePattern(BufferUsagePattern::Unset),
	dataAppended()
{
}


Vbo::Vbo(Vbo&& original) noexcept :
	created(std::move(original.created)),
	handle(std::move(original.handle)),
	shaderAttribute(std::move(original.shaderAttribute)),
	entryCount(std::move(original.entryCount)),
	entryBytes(std::move(original.entryBytes)),
	perVertexCount(std::move(original.perVertexCount)),
	bufferUsagePattern(std::move(original.bufferUsagePattern)),
	dataAppended(std::move(original.dataAppended))
{
	original.created = false;
	original.handle = 0;
	original.shaderAttribute.reset();
	original.dataAppended.clear();
}


Vbo& Vbo::operator =(Vbo&& original) noexcept
{
	if (this != &original) {
		created = std::move(original.created);
		handle = std::move(original.handle);
		shaderAttribute = std::move(original.shaderAttribute);
		entryCount = std::move(original.entryCount);
		entryBytes = std::move(original.entryBytes);
		perVertexCount = std::move(original.perVertexCount);
		bufferUsagePattern = std::move(original.bufferUsagePattern);
		dataAppended = std::move(original.dataAppended);

		original.created = false;
		original.handle = 0;
		original.shaderAttribute.reset();
		original.dataAppended.clear();
	}
	return *this;
}


Vbo::~Vbo()
{
	destroy();
}


void Vbo::create()
{
	VERSO_ASSERT_MSG("verso-3d", isCreated() == false, "Already created!");

	// Allocate and assign two Vertex Buffer Objects to our handle
	glGenBuffers(1, &handle);
	GL_CHECK_FOR_ERRORS("", "");

	created = true;
}


void Vbo::destroy() VERSO_NOEXCEPT
{
	if (isCreated() == false) {
		return;
	}

	glDeleteBuffers(1, &handle);
	GL_CHECK_FOR_ERRORS("", "");

	handle = 0;
	shaderAttribute.reset();
	entryCount = 0;
	entryBytes = 0;
	perVertexCount = 0;
	bufferUsagePattern = BufferUsagePattern::Unset;
	dataAppended.clear();
	created = false;
}


bool Vbo::isCreated() const
{
	return created;
}


void Vbo::bind()
{
	VERSO_ASSERT("verso-3d", isCreated() == true);

	if (lastBoundHandle != handle) {
		// Bind our first VBO as being the active buffer and storing vertex attributes (coordinates)
		glBindBuffer(GL_ARRAY_BUFFER, handle);
		GL_CHECK_FOR_ERRORS("", "");

		lastBoundHandle = handle;
	}
}


void Vbo::unbind()
{
	if (lastBoundHandle != 0) {
		// Unbind our first VBO as being the active buffer and storing vertex attributes (coordinates)
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		GL_CHECK_FOR_ERRORS("", "");

		lastBoundHandle = 0;
	}
}


void Vbo::setDataf(const ShaderAttribute& shaderAttribute, const GLvoid* data, int entryCount, int perVertexCount, const BufferUsagePattern& bufferUsagePattern)
{
	VERSO_ASSERT("verso-3d", isCreated() == true);
	bind();

	// Copy the vertex data to vbo
	this->entryBytes = sizeof(GLfloat);
	glBufferData(GL_ARRAY_BUFFER, entryCount * this->entryBytes, data, bufferUsagePatternToGlenum(bufferUsagePattern));
	GL_CHECK_FOR_ERRORS("", "");

	this->shaderAttribute = shaderAttribute;
	this->entryCount = entryCount;
	this->perVertexCount = perVertexCount;
	this->bufferUsagePattern = bufferUsagePattern;
}


void Vbo::appendDataf(const ShaderAttribute& shaderAttribute, const GLvoid* data, int entryCount, int perVertexCount)
{
	VERSO_ASSERT("verso-3d", isCreated() == true);

	// Copy the vertex data to temporary buffer
	const float* dataf = reinterpret_cast<const float*>(data);
	dataAppended.insert(dataAppended.end(), &dataf[0], &dataf[entryCount]);

	this->shaderAttribute = shaderAttribute;
	this->perVertexCount = perVertexCount;
}


void Vbo::applyAppendedData(const BufferUsagePattern& bufferUsagePattern)
{
	VERSO_ASSERT("verso-3d", isCreated() == true);

	bind();

	this->entryBytes = sizeof(GLfloat);
	int entryCount = static_cast<int>(dataAppended.size());
	if (entryCount == 0) {
		return;
	}

	// Copy the vertex data to vbo
	glBufferData(GL_ARRAY_BUFFER, entryCount * this->entryBytes, dataAppended.data(), bufferUsagePatternToGlenum(bufferUsagePattern));
	GL_CHECK_FOR_ERRORS("", "");

	this->entryCount = entryCount;
	this->bufferUsagePattern = bufferUsagePattern;

	// Clear data
	dataAppended.clear();
}


ShaderAttribute Vbo::getShaderAttribute() const
{
	return shaderAttribute;
}


int Vbo::getEntryCount() const
{
	return entryCount;
}


int Vbo::getEntryBytes() const
{
	return entryBytes;
}


int Vbo::getPerVertexCount() const
{
	return perVertexCount;
}


int Vbo::getAppendedEntryCount() const
{
	return static_cast<int>(dataAppended.size()) / perVertexCount;
}


BufferUsagePattern Vbo::getBufferUsagePattern() const
{
	return bufferUsagePattern;
}


UString Vbo::toString() const
{
	UString str;
	if (isCreated() == false) {
		str += "isCreated() == false";
	}
	else {
		str += "type=";
		str += shaderAttribute.toString();
		str += ", handle=";
		str.append2(handle);
		str += ", entryCount=";
		str.append2(entryCount);
		str += ", entryBytes=";
		str.append2(entryBytes);
		str += ", perVertexCount=";
		str.append2(perVertexCount);
		str += ", bufferUsagePattern=";
		str += bufferUsagePatternToString(bufferUsagePattern);
	}
	return str;
}


UString Vbo::toStringDebug() const
{
	UString str("Vbo(");
	str += toString();
	str += ")";
	return str;
}


} // End namespace Verso


