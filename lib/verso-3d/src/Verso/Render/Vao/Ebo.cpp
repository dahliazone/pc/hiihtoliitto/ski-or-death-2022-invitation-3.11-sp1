
#include <Verso/Render/Vao/Ebo.hpp>

namespace Verso {


GLuint Ebo::lastBoundHandle = 0;


Ebo::Ebo() :
	created(false),
	handle(0),
	indicesCount(0),
	bufferUsagePattern(BufferUsagePattern::Unset),
	dataAppended()
{
}


Ebo::Ebo(Ebo&& original) noexcept :
	created(std::move(original.created)),
	handle(std::move(original.handle)),
	indicesCount(std::move(original.indicesCount)),
	bufferUsagePattern(std::move(original.bufferUsagePattern)),
	dataAppended(std::move(original.dataAppended))
{
	original.created = false;
	original.handle = 0;
	original.indicesCount = 0;
	original.bufferUsagePattern = BufferUsagePattern::Unset;
	original.dataAppended.clear();
}


Ebo& Ebo::operator =(Ebo&& original) noexcept
{
	if (this != &original) {
		created = original.created;
		handle = original.handle;
		indicesCount = original.indicesCount;
		bufferUsagePattern = std::move(original.bufferUsagePattern);
		dataAppended = std::move(original.dataAppended);

		original.created = false;
		original.handle = 0;
		original.indicesCount = 0;
		original.bufferUsagePattern = BufferUsagePattern::Unset;
		original.dataAppended.clear();
	}
	return *this;
}


Ebo::~Ebo()
{
	destroy();
}


void Ebo::create()
{
	VERSO_ASSERT("verso-3d", isCreated() == false);

	// Allocate and assign Vertex Buffer Object to our handle
	glGenBuffers(1, &handle);
	GL_CHECK_FOR_ERRORS("", "");

	created = true;
}


void Ebo::destroy() VERSO_NOEXCEPT
{
	if (isCreated()) {
		unbind();

		glDeleteBuffers(1, &handle);
		GL_CHECK_FOR_ERRORS("", "");

		indicesCount = 0;
		bufferUsagePattern = BufferUsagePattern::Unset;
		dataAppended.clear();
		handle = 0;
		created = false;
	}

	dataAppended.clear();
}


void Ebo::bind()
{
	VERSO_ASSERT("verso-3d", isCreated() == true);

	if (lastBoundHandle != handle) {
		// Bind our first ebo as being the active buffer and storing vertex attributes (coordinates)
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle);
		GL_CHECK_FOR_ERRORS("", "");

		lastBoundHandle = handle;
	}
}


void Ebo::unbind()
{
	if (lastBoundHandle != 0) {
		// Bind our first ebo as being the active buffer and storing vertex attributes (coordinates)
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		GL_CHECK_FOR_ERRORS("", "");

		lastBoundHandle = 0;
	}
}


// Note: binds the ebo to use.
// Set indicesCount amount of GLfloat items from data to ebo with expected usage pattern of the data store.
void Ebo::setIndices(const GLuint* indices, int indicesCount, const BufferUsagePattern& bufferUsagePattern)
{
	VERSO_ASSERT("verso-3d", isCreated() == true);

	// Copy the index data to ebo
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		static_cast<size_t>(indicesCount) * static_cast<size_t>(sizeof(GLuint)),
		indices,
		bufferUsagePatternToGlenum(bufferUsagePattern));
	GL_CHECK_FOR_ERRORS("", "");

	this->indicesCount = indicesCount;
	this->bufferUsagePattern = bufferUsagePattern;
}


void Ebo::appendIndices(const GLuint* indices, int entryCount)
{
	VERSO_ASSERT("verso-3d", isCreated() == true);

	// Copy the vertex data to temporary buffer
	dataAppended.insert(dataAppended.end(), &indices[0], &indices[entryCount]);
}


void Ebo::applyAppendedData(const BufferUsagePattern& bufferUsagePattern)
{
	if (dataAppended.data() == 0) {
		return;
	}
	VERSO_ASSERT("verso-3d", isCreated() == true);

	bind();
	setIndices(dataAppended.data(), static_cast<int>(dataAppended.size()), bufferUsagePattern);

	// Clear data
	dataAppended.clear();
}


bool Ebo::isCreated() const
{
	return created;
}


int Ebo::getIndicesCount() const
{
	return indicesCount;
}


int Ebo::getNextAppendIndex() const
{
	return static_cast<int>(dataAppended.size());
}


BufferUsagePattern Ebo::getBufferUsagePattern() const
{
	return bufferUsagePattern;
}


UString Ebo::toString() const
{
	UString str;
	if (isCreated() == false) {
		str += "isCreated() == false";
	}
	else {
		str += "handle=";
		str.append2(handle);
		str += ", indicesCount=";
		str.append2(indicesCount);
		str += ", bufferUsagePattern=";
		str += bufferUsagePatternToString(bufferUsagePattern);
	}
	return str;
}


UString Ebo::toStringDebug() const
{
	UString str("Ebo(");
	str += toString();
	str += ")";
	return str;
}


} // End namespace Verso

