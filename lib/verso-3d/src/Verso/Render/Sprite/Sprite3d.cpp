#include <Verso/Render/Sprite/Sprite3d.hpp>
#include <Verso/Render/Render.hpp>
#include <Verso/Display/IWindowOpengl.hpp>

namespace Verso {


Sprite3d::Sprite3d() :
	created(false),
	refScale(RefScale::ViewportSize_KeepAspectRatio_FromX),
	position(),
	relSize(1.0f, 1.0f),
	// Eular Angles
	yaw(0.0f),
	pitch(0.0f),
	roll(0.0f),
	relOffset(),
	texture(nullptr),
	alpha(1.0f),
	spriteUvSize(1.0f, 1.0f),
	currentAnimationIndex(-1),
	animations(),
	children()
{
}


Sprite3d::Sprite3d(const Sprite3d& original) :
	created(original.created),
	refScale(original.refScale),
	position(original.position),
	relSize(original.relSize),
	// Eular Angles
	yaw(original.yaw),
	pitch(original.pitch),
	roll(original.roll),
	relOffset(original.relOffset),
	texture(original.texture),
	alpha(original.alpha),
	spriteUvSize(original.spriteUvSize),
	currentAnimationIndex(original.currentAnimationIndex),
	animations(original.animations),
	children(original.children)
{
}


Sprite3d::Sprite3d(Sprite3d&& original) noexcept :
	created(std::move(original.created)),
	refScale(std::move(original.refScale)),
	position(std::move(original.position)),
	relSize(std::move(original.relSize)),
	// Eular Angles
	yaw(std::move(original.yaw)),
	pitch(std::move(original.pitch)),
	roll(std::move(original.roll)),
	relOffset(std::move(original.relOffset)),
	texture(std::move(original.texture)),
	alpha(std::move(original.alpha)),
	spriteUvSize(std::move(original.spriteUvSize)),
	currentAnimationIndex(std::move(original.currentAnimationIndex)),
	animations(std::move(original.animations)),
	children(std::move(original.children))
{
	original.texture = nullptr;
}


Sprite3d& Sprite3d::operator =(const Sprite3d& original)
{
	if (this != &original) {
		created = original.created;
		refScale = original.refScale;
		position = original.position;
		relSize = original.relSize;
		// Eular Angles
		yaw = original.yaw;
		pitch = original.pitch;
		roll = original.roll;
		relOffset = original.relOffset;
		texture = original.texture;
		alpha = original.alpha;
		spriteUvSize = original.spriteUvSize;
		currentAnimationIndex = original.currentAnimationIndex;
		animations = original.animations;
		children = original.children;
	}
	return *this;
}


Sprite3d& Sprite3d::operator =(Sprite3d&& original) noexcept
{
	if (this != &original) {
		created = std::move(original.created);
		refScale = std::move(original.refScale);
		position = std::move(original.position);
		relSize = std::move(original.relSize);
		// Eular Angles
		yaw = std::move(original.yaw);
		pitch = std::move(original.pitch);
		roll = std::move(original.roll);
		relOffset = std::move(original.relOffset);
		texture = std::move(original.texture);
		alpha = std::move(original.alpha);
		spriteUvSize = std::move(original.spriteUvSize);
		currentAnimationIndex = std::move(original.currentAnimationIndex);
		animations = std::move(original.animations);
		children = std::move(original.children);

		original.texture = nullptr;
	}
	return *this;
}


Sprite3d::~Sprite3d()
{
	destroy();
}


void Sprite3d::createFromTexture(Texture* texture)
{
	VERSO_ASSERT("verso-3d", texture != nullptr);

	if (isCreated()) {
		VERSO_ERROR("verso-3d", "Skybox already created!", toString("").c_str());
	}

	this->texture = texture;
	this->spriteUvSize = Vector2f(1.0f, 1.0f);
	created = true;
}


void Sprite3d::destroy() VERSO_NOEXCEPT
{
	if (isCreated() == false) {
		return;
	}

	texture = nullptr; // no ownership so no delete
	animations.clear();
	children.clear(); // no ownership so no delete
	created = false;
}


bool Sprite3d::isCreated() const
{
	return created;
}


void Sprite3d::setAnimationsFromTexture(const std::vector<Sprite3dAnimationParam>& animationParams)
{
	VERSO_ASSERT_MSG("verso-3d", animationParams.size() > 0,
					 "animationParams must contain at least one animation!");

	this->animations.clear();

	int animationsPerRow = static_cast<int>(animationParams.size());
	int spritesPerColumn = 0;
	for (auto& param : animationParams) {
		if (param.frameCount > spritesPerColumn) {
			spritesPerColumn = param.frameCount;
		}
	}

	spriteUvSize.x = 1.0f / static_cast<float>(spritesPerColumn);
	spriteUvSize.y = 1.0f / static_cast<float>(animationsPerRow);

	int row = 0;
	for (auto& animParam : animationParams) {
		VERSO_ASSERT_MSG("verso-3d", animParam.frameCount > 0,
						 "frameCount must be larger than zero!");

		Sprite3dAnimation anim(animParam.type);
		if (animParam.useManualFrameOrder == false) {
			for (int column = 0; column < animParam.frameCount; ++column) {
				anim.addFrame(animParam.autoTiming[column],
							  Rectf(
								  static_cast<float>(column) * spriteUvSize.x,
								  static_cast<float>(row) * spriteUvSize.y,
								  spriteUvSize.x,
								  spriteUvSize.y));
			}
		}
		else {
			float prevSeconds = 0.0f;
			for (IntKeyframeElement frameElement : animParam.manualFrameOrder.data) {
				anim.addFrame(frameElement.seconds - prevSeconds,
							  Rectf(
								  static_cast<float>(frameElement.value) * spriteUvSize.x,
								  static_cast<float>(row) * spriteUvSize.y,
								  spriteUvSize.x,
								  spriteUvSize.y));
				prevSeconds = frameElement.seconds;
			}
		}

		addAnimation(anim);
		row++;
	}

	setCurrentAnimationIndex(0);
}


void Sprite3d::setAnimationsFromTexture(
		int spritesPerColumn, int animationsPerRow,
		const std::vector<AnimationType>& animationTypes,
		const std::vector<int>& animationFrameCounts,
		const std::vector<float>& staticAnimationTimesPerAnimation)
{
	VERSO_ASSERT_MSG("verso-3d", animationTypes.size() == animationFrameCounts.size(),
					 "animationTypes and animationFrameCounts must be the same size!");
	VERSO_ASSERT_MSG("verso-3d", animationFrameCounts.size() == staticAnimationTimesPerAnimation.size(),
					 "animationFrameCounts and staticAnimationTimesPerAnimation must be the same size!");
	VERSO_ASSERT_MSG("verso-3d", animationFrameCounts.size() > 0,
					 "You must provides at least one animation row (animationTypes & animationFrameCounts & staticAnimationTimesPerAnimation are zero size)!");

	std::vector< std::vector<float> > animationTimes;
	for (int row = 0; row < animationsPerRow; ++row) {
		std::vector<float> times;
		for (int column = 0; column < animationFrameCounts[row]; ++column) {
			times.push_back(staticAnimationTimesPerAnimation[row]);
		}
		animationTimes.push_back(times);
	}

	setAnimationsFromTexture(
			spritesPerColumn, animationsPerRow,
			animationTypes,
			animationFrameCounts,
			animationTimes);
}


void Sprite3d::setAnimationsFromTexture(
		int spritesPerColumn, int animationsPerRow,
		const std::vector<AnimationType>& animationTypes,
		const std::vector<int>& animationFrameCounts,
		const std::vector< std::vector<float> >& animationTimes)
{
	VERSO_ASSERT_MSG("verso-3d", animationTypes.size() == animationFrameCounts.size(),
					 "animationTypes and animationFrameCounts must be the same size!");
	VERSO_ASSERT_MSG("verso-3d", animationFrameCounts.size() == animationTimes.size(),
					 "animationFrameCounts and animationTimes must be the same size!");
	VERSO_ASSERT_MSG("verso-3d", animationFrameCounts.size() > 0,
					 "You must provides at least one animation row (animationTypes & animationFrameCounts & animationTimes are zero size)!");

	spriteUvSize.x = 1.0f / static_cast<float>(spritesPerColumn);
	spriteUvSize.y = 1.0f / static_cast<float>(animationsPerRow);

	for (int row = 0; row < animationsPerRow; ++row) {
		VERSO_ASSERT_MSG("verso-3d", animationFrameCounts[row] > 0,
						 "animationFrameCounts must contain values larger than zero!");
		VERSO_ASSERT_MSG("verso-3d", animationFrameCounts[row] <= spritesPerColumn,
						 "animationFrameCounts must contain values smaller or same than given spritesPerColumn!");

		Sprite3dAnimation anim(animationTypes[row]);
		for (int column = 0; column < animationFrameCounts[row]; ++column) {
			anim.addFrame(animationTimes[row][column],
						  Rectf(
							  static_cast<float>(column) * spriteUvSize.x,
							  static_cast<float>(row) * spriteUvSize.y,
							  spriteUvSize.x,
							  spriteUvSize.y));
		}
		addAnimation(anim);
	}

	setCurrentAnimationIndex(0);
}


int Sprite3d::getAnimationsCount() const
{
	return static_cast<int>(animations.size());
}


void Sprite3d::addAnimation(const Sprite3dAnimation& animation)
{
	this->animations.push_back(animation);
}


void Sprite3d::clearAnimations()
{
	Sprite3d::disableAnimationIndex();
	this->animations.clear();
}


int Sprite3d::getCurrentAnimationIndex() const
{
	return currentAnimationIndex;
}


void Sprite3d::disableAnimationIndex()
{
	this->currentAnimationIndex = -1;
}


void Sprite3d::setCurrentAnimationIndex(int index)
{
	VERSO_ASSERT_MSG("verso-3d", index >= 0, "currentAnimationIndex must be zero or positive!");
	VERSO_ASSERT_MSG("verso-3d", index < static_cast<int>(animations.size()), "currentAnimationIndex must be within range or animations count!");
	this->currentAnimationIndex = index;
}


void Sprite3d::render(IWindowOpengl& window, const FrameTimestamp& time, const ICamera& camera)
{
	Matrix4x4f modelMatrix;
	render(window, time, camera, modelMatrix, Vector2f(1.0f, 1.0f));
}


void Sprite3d::render(IWindowOpengl& window, const FrameTimestamp& time, const ICamera& camera, const Matrix4x4f& modelMatrix, const Vector2f& scale)
{
	Vector2f spriteResolution(texture->getResolutionf().x * spriteUvSize.x, texture->getResolutionf().y * spriteUvSize.y);
	float spriteAspectRatio = spriteResolution.x / spriteResolution.y;

	Vector2f refScaleSize = calculateRefScaleSize(
				refScale, relSize,
				spriteResolution, spriteAspectRatio,
				window.getRenderViewporti().size);

	Vector3f refScalePosition = camera.viewportToScreenPoint(position, window.getRenderViewporti().size);

	Matrix4x4f model(modelMatrix);
	model = Matrix4x4f::translate(model, refScalePosition);

	model = Matrix4x4f::rotate(model, yaw, Vector3f(0, 1, 0));
	model = Matrix4x4f::rotate(model, pitch, Vector3f(0, 0, 1));
	model = Matrix4x4f::rotate(model, roll, Vector3f(1, 0, 0));

	model = Matrix4x4f::scale(model,
							  refScaleSize.x,
							  refScaleSize.y,
							  1.0f);

	// Setup shader
	Render::defaultShaderSprite3d.useProgram();
	Render::defaultShaderSprite3d.setUniform("model", model);
	Render::defaultShaderSprite3d.setUniform("view", camera.getViewMatrix());
	Render::defaultShaderSprite3d.setUniform("projection", camera.getProjectionMatrix());
	Render::defaultShaderSprite3d.setUniform("meshOffset", Vector3f(relOffset.x, relOffset.y, 0.0f));
	Render::defaultShaderSprite3d.setUniformf("alpha", alpha);
	if (currentAnimationIndex < 0) {
		Render::defaultShaderSprite3d.setUniform("uvOffset", Vector2f());
		Render::defaultShaderSprite3d.setUniform("uvScale", Vector2f(1, 1));
	}
	else {
		Rectf uv = animations[currentAnimationIndex].getFrameUvBySeconds(static_cast<float>(time.getElapsed().asSeconds()));
		Render::defaultShaderSprite3d.setUniform("uvOffset", uv.pos);
		Render::defaultShaderSprite3d.setUniform("uvScale", uv.size);
	}

	// Setup texture
	Render::defaultShaderSprite3d.setUniformi("iChannel0", 0);
	window.setActiveTextureUnit(0);
	texture->bind();

	// Render
	Render::quad3dVao.render();

	// Render children
	for (auto* child : children) {
		child->render(window, time, camera, model, scale * relSize);
	}
}


UString Sprite3d::toString(const UString& newLinePadding) const
{
	UString str("{ ");
	UString newLinePadding2 = newLinePadding + "  ";
	if (isCreated() == false) {
		str += "created=false";
	}
	else {
		str += "created=true";
		str += ", refScale=\"";
		str += refScaleToString(refScale);
		str += "\", position=";
		str += position.toString();
		str += ", relSize=";
		str += relSize.toString();
		str += ", yaw=";
		str.append2(yaw);
		str += ", pitch=";
		str.append2(pitch);
		str += ", roll=";
		str.append2(roll);
		str += ", relOffset=";
		str += relOffset.toString();
		str += ", texture=";
		if (texture != nullptr) {
			str += texture->toString(newLinePadding2);
		}
		else {
			str += "nullptr";
		}
		str += ", alpha=";
		str.append2(alpha);
		str += ", spriteUvSize=";
		str += spriteUvSize.toString();
		str += ", children=[\n";
		{
			UString newLinePadding3 = newLinePadding2 + "  ";
			for (auto* child : children) {
				str += "    ";
				if (child != nullptr) {
					str += "(";
					str += child->toString(newLinePadding3);
					str += "), ";
				}
				else {
					str += "nullptr, ";
				}
			}
		}
		str += "]\n";
	}
	str += " }";
	return str;
}


UString Sprite3d::toStringDebug(const UString& newLinePadding) const
{
	UString str("Sprite3d(");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso

