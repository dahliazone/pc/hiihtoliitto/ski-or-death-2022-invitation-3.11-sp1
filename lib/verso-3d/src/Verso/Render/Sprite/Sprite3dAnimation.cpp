#include <Verso/Render/Sprite/Sprite3dAnimation.hpp>

namespace Verso {


Sprite3dAnimation::Sprite3dAnimation() :
	animationType(AnimationType::Unset),
	frameTimes(),
	frameUvs(),
	animationLengthSeconds(0.0f)
{
}


Sprite3dAnimation::Sprite3dAnimation(
		const AnimationType& animationType,
		const std::vector<float>& frameTimes,
		const std::vector<Rectf>& frameUvs) :
	animationType(animationType),
	frameTimes(frameTimes),
	frameUvs(frameUvs),
	animationLengthSeconds(0.0f)
{
	updateAnimationLengthSeconds();
}


Sprite3dAnimation::Sprite3dAnimation(const Sprite3dAnimation& original) :
	animationType(original.animationType),
	frameTimes(original.frameTimes),
	frameUvs(original.frameUvs),
	animationLengthSeconds(original.animationLengthSeconds)
{
}


Sprite3dAnimation::Sprite3dAnimation(Sprite3dAnimation&& original) :
	animationType(std::move(original.animationType)),
	frameTimes(std::move(original.frameTimes)),
	frameUvs(std::move(original.frameUvs)),
	animationLengthSeconds(std::move(original.animationLengthSeconds))
{
}


Sprite3dAnimation& Sprite3dAnimation::operator =(const Sprite3dAnimation& original)
{
	if (this != &original) {
		animationType = original.animationType;
		frameTimes = original.frameTimes;
		frameUvs = original.frameUvs;
		animationLengthSeconds = original.animationLengthSeconds;
	}
	return *this;
}


Sprite3dAnimation& Sprite3dAnimation::operator =(Sprite3dAnimation&& original)
{
	if (this != &original) {
		animationType = std::move(original.animationType);
		frameTimes = std::move(original.frameTimes);
		frameUvs = std::move(original.frameUvs);
		animationLengthSeconds = std::move(original.animationLengthSeconds);
	}
	return *this;
}


Sprite3dAnimation::~Sprite3dAnimation()
{
}


AnimationType Sprite3dAnimation::getAnimationType() const
{
	return animationType;
}


void Sprite3dAnimation::setAnimationType(const AnimationType& animationType)
{
	this->animationType = animationType;
}


Rectf Sprite3dAnimation::getFrameUvBySeconds(float seconds) const
{
	VERSO_ASSERT_MSG("verso-3d", frameTimes.size() > 0, "frameTimes must contain at least one frame!");
	VERSO_ASSERT_MSG("verso-3d", frameUvs.size() > 0, "frameUvs must contain at least one frame!");
	VERSO_ASSERT_MSG("verso-3d", frameTimes.size() == frameUvs.size(), "frameTimes and frameUvs must contain same amount of frames!");

	switch (animationType) {
	case AnimationType::Unset:
	{
		return frameUvs[0];
	}
	case AnimationType::Once:
	{
		return calculateFrameUvBySeconds(seconds);
	}
	case AnimationType::Loop:
	{
		float secondsAdjusted = seconds - floor(seconds / animationLengthSeconds) * animationLengthSeconds;
		return calculateFrameUvBySeconds(secondsAdjusted);
	}
	}

	return frameUvs[0];
}


float Sprite3dAnimation::getFrameTime(int frameIndex) const
{
	VERSO_ASSERT_MSG("verso-3d", frameIndex < 0, "frameIndex must be positive!");
	VERSO_ASSERT_MSG("verso-3d", frameIndex >= static_cast<int>(frameTimes.size()), "frameIndex must not exceed amount of frames!");
	return frameTimes[frameIndex];
}


Rectf Sprite3dAnimation::getFrameUv(int frameIndex) const
{
	VERSO_ASSERT_MSG("verso-3d", frameIndex < 0, "frameIndex must be positive!");
	VERSO_ASSERT_MSG("verso-3d", frameIndex >= static_cast<int>(frameUvs.size()), "frameIndex must not exceed amount of frames!");
	return frameUvs[frameIndex];
}


void Sprite3dAnimation::setFrames(
		const std::vector<float>& frameTimes,
		const std::vector<Rectf>& frameUvs)
{
	this->frameTimes = frameTimes;
	this->frameUvs = frameUvs;
	updateAnimationLengthSeconds();
}


void Sprite3dAnimation::clearFrames()
{
	this->frameTimes.clear();
	this->frameUvs.clear();
	this->animationLengthSeconds = 0.0f;
}


void Sprite3dAnimation::addFrame(float frameTime, const Rectf& frameUv)
{
	this->frameTimes.push_back(frameTime);
	this->frameUvs.push_back(frameUv);
	updateAnimationLengthSeconds();
}


void Sprite3dAnimation::updateAnimationLengthSeconds()
{
	animationLengthSeconds = 0.0f;
	for (auto& seconds : frameTimes) {
		animationLengthSeconds += seconds;
	}
}


Rectf Sprite3dAnimation::calculateFrameUvBySeconds(float seconds) const
{
	if (seconds < 0.0f) {
		return frameUvs[0];
	}
	else if (seconds >= animationLengthSeconds) {
		return frameUvs[frameUvs.size() - 1];
	}

	float accumulator = 0.0f;
	for (size_t i = 0; i < frameTimes.size(); ++i) {

		if (seconds > accumulator && seconds < accumulator + frameTimes[i]) {
			return frameUvs[i];
		}

		if (seconds >= accumulator + frameTimes[i]) {
		}
		accumulator += frameTimes[i];
	}
	return frameUvs[frameUvs.size() - 1];
}


UString Sprite3dAnimation::toString() const
{
	UString str;
	str += "animationType=";
	str += animationTypeToString(animationType);
	str += ", animationLengthSeconds=[";
	str.append2(animationLengthSeconds);
	str += ", frames=[";
	for (size_t i = 0; i < frameUvs.size(); ++i) {
		str.append2(frameTimes[i]);
		str += "s ";
		str += frameUvs[i].toString();
		str += ", ";
	}
	str += "]";
	return str;
}



UString Sprite3dAnimation::toStringDebug() const
{
	UString str("Sprite3dAnimation(");
	str += toString();
	str += ")";
	return str;
}


} // End namespace Verso
