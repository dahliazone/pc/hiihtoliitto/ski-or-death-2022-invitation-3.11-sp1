#include <Verso/Render/TextureAtlas/TextureAtlas.hpp>
#include <Verso/System/File.hpp>
#include <Verso/System/Exception.hpp>
#include <Verso/System/JsonHelper.hpp>

namespace Verso {


TextureAtlas::TextureAtlas() :
	created(false),
	nextSubAtlasHandle(1),
	subAtlasNameToSubAtlasHandle(),
	subAtlasSourceJsonFileName(),
	subAtlasSourceTextureFileName(),
	subAtlas(),
	nextTextureAreaHandle(1),
	subAtlasAndTextureAreaNameToTextureAreaHandle(),
	textureAreas()
{
}


TextureAtlas::TextureAtlas(TextureAtlas&& original) :
	created(std::move(original.created)),
	nextSubAtlasHandle(std::move(original.nextSubAtlasHandle)),
	subAtlasNameToSubAtlasHandle(std::move(original.subAtlasNameToSubAtlasHandle)),
	subAtlasSourceJsonFileName(std::move(original.subAtlasSourceJsonFileName)),
	subAtlasSourceTextureFileName(std::move(original.subAtlasSourceTextureFileName)),
	subAtlas(std::move(original.subAtlas)),
	nextTextureAreaHandle(std::move(original.nextTextureAreaHandle)),
	subAtlasAndTextureAreaNameToTextureAreaHandle(std::move(original.subAtlasAndTextureAreaNameToTextureAreaHandle)),
	textureAreas(std::move(original.textureAreas))
{
	// Clear the original
	original.created = false;
	original.subAtlasNameToSubAtlasHandle.clear();
	original.subAtlasSourceJsonFileName.clear();
	original.subAtlasSourceTextureFileName.clear();
	original.subAtlas.clear();

	original.subAtlasAndTextureAreaNameToTextureAreaHandle.clear();
	original.textureAreas.clear();
}


TextureAtlas& TextureAtlas::operator =(TextureAtlas&& original)
{
	if (this != &original) {
		created = std::move(original.created);
		nextSubAtlasHandle = std::move(original.nextSubAtlasHandle);
		subAtlasNameToSubAtlasHandle = std::move(original.subAtlasNameToSubAtlasHandle);
		subAtlasSourceJsonFileName = std::move(original.subAtlasSourceJsonFileName);
		subAtlasSourceTextureFileName = std::move(original.subAtlasSourceTextureFileName);
		subAtlas = std::move(original.subAtlas);
		nextTextureAreaHandle = std::move(original.nextTextureAreaHandle);
		subAtlasAndTextureAreaNameToTextureAreaHandle = std::move(original.subAtlasAndTextureAreaNameToTextureAreaHandle);
		textureAreas = std::move(original.textureAreas);

		// Clear the original
		original.subAtlasNameToSubAtlasHandle.clear();
		original.subAtlasSourceJsonFileName.clear();
		original.subAtlasSourceTextureFileName.clear();
		original.subAtlas.clear();

		original.subAtlasAndTextureAreaNameToTextureAreaHandle.clear();
		original.textureAreas.clear();
	}
	return *this;
}


TextureAtlas::~TextureAtlas()
{
	destroy();
}


SubAtlasHandle TextureAtlas::loadTextureCssJson(IWindowOpengl& window, const UString& fileName)
{
	// Possible to call multiple times, no explicit create*()

	JSONValue* rootValue = JsonHelper::loadAndParse(fileName);
	const JSONObject& root = rootValue->AsObject();
	UString rootPath = "root.";

	if (root.size() > 0) {
		// Find out subAtlas name and texture file name
		UString subAtlasName;
		UString textureFileName;
		for (auto item = root.begin(); item != root.end(); ++item) {
			// \TODO: use more helper functions!
			const JSONObject& textureInfo = item->second->AsObject();
			for (auto subItem = textureInfo.begin(); subItem != textureInfo.end(); ++subItem) {
				UString currentKey(subItem->first);
				if (currentKey.equals("background-image")) {
					subAtlasName.set(item->first);
					textureFileName = JsonHelper::readString(textureInfo, "TODO:.", currentKey, true);
					textureFileName = File::getFileNamePath(fileName) + textureFileName;
					break;
				}
			}
		}

		if (textureFileName.isEmpty()) {
			UString message("Could not find \"background-image\" attribute-value in the any root child object in the JSON!");
			VERSO_ILLEGALFORMAT("verso-3d", message.c_str(), fileName.c_str());
		}

		Texture* texture = new Texture();
		texture->createFromFile(window, textureFileName,
								TextureParameters(
									"texture", TexturePixelFormat::Unset,
									MinFilter::Linear, MagFilter::Linear,
									WrapStyle::ClampToEdge, WrapStyle::ClampToEdge));

		// Save sub-atlas info
		subAtlasSourceTextureFileName[nextSubAtlasHandle] = textureFileName;
		subAtlasSourceJsonFileName[nextSubAtlasHandle] = fileName;
		subAtlas[nextSubAtlasHandle] = texture;
		subAtlasNameToSubAtlasHandle[subAtlasName] = nextSubAtlasHandle;

		// Save texture areas
		for (auto it = root.begin(); it != root.end(); ++it) {
			UString textureAreaName(it->first);

			// Skip texture info block
			if (textureAreaName.equals(subAtlasName)) {
				continue;
			}

			const JSONObject& textureAreaJsonObj = it->second->AsObject();

			const JSONArray& pos = JsonHelper::readArray(textureAreaJsonObj, "TODO:.", "background-position", true);
			if (pos.size() != 2) {
				UString message("'background-position' for '"+textureAreaName+"' doesn't contain exactly two values.");
				VERSO_ILLEGALFORMAT("verso-3d", message.c_str(), fileName.c_str());
			}
			int x = static_cast<int>(pos.at(0)->AsNumber());
			int y = static_cast<int>(pos.at(1)->AsNumber());
			x = -x;
			y = -y;

			int width = static_cast<int>(JsonHelper::readNumberi(textureAreaJsonObj, "TODO:.", "width", true, 0));
			int height = static_cast<int>(JsonHelper::readNumberi(textureAreaJsonObj, "TODO:.", "height", true, 0));

			subAtlasAndTextureAreaNameToTextureAreaHandle[nextSubAtlasHandle].insert(std::make_pair(textureAreaName, nextTextureAreaHandle));
			TextureArea textureArea(texture, Rect<float>(static_cast<float>(x), static_cast<float>(y),
				static_cast<float>(width), static_cast<float>(height)));
			textureAreas[nextTextureAreaHandle] = textureArea;
			nextTextureAreaHandle++;
		}
	}
	else {
		VERSO_ILLEGALFORMAT("verso-3d", "Cannot find any values in the JSON root object!", fileName.c_str());
	}

	JsonHelper::free(rootValue);

	created = true;

	return nextSubAtlasHandle++;
}


void TextureAtlas::destroy()
{
	if (isCreated() == false) {
		return;
	}

	textureAreas.clear();
	subAtlasAndTextureAreaNameToTextureAreaHandle.clear();
	nextTextureAreaHandle = 1;

	subAtlasSourceJsonFileName.clear();
	subAtlasSourceTextureFileName.clear();

	for (auto const& it : subAtlas) {
		delete it.second;
	}
	subAtlas.clear();

	subAtlasNameToSubAtlasHandle.clear();
	nextSubAtlasHandle = 1;

	created = false;
}


bool TextureAtlas::isCreated() const
{
	return created;
}


std::vector<UString> TextureAtlas::getSubAtlasNames() const
{
	std::vector<UString> temp;
	for(auto const& it : subAtlasNameToSubAtlasHandle) {
		temp.push_back(it.first);
	}
	return temp;
}


std::vector<SubAtlasHandle> TextureAtlas::getSubAtlasHandles() const
{
	std::vector<SubAtlasHandle> temp;
	for(auto const& it : subAtlasNameToSubAtlasHandle) {
		temp.push_back(it.second);
	}
	return temp;
}


SubAtlasHandle TextureAtlas::getSubAtlasHandle(const UString& subAtlasName) const
{
	const auto& it = subAtlasNameToSubAtlasHandle.find(subAtlasName);
	if (it != subAtlasNameToSubAtlasHandle.end()) {
		return it->second;
	}
	else {
		return 0;
	}
}


UString TextureAtlas::getSubAtlasName(SubAtlasHandle subAtlasHandle) const
{
	for (auto it = subAtlasNameToSubAtlasHandle.begin(); it != subAtlasNameToSubAtlasHandle.end(); ++it) {
		if (it->second == subAtlasHandle) {
			return it->first;
		}
	}

	UString message("Cannot find sub-atlas with handle='");
	message.append2(subAtlasHandle);
	message += "'";
	VERSO_OBJECTNOTFOUND("verso-3d", message.c_str(), "");
}


std::vector<TextureAreaHandle> TextureAtlas::getTextureAreaHandles(const UString& subAtlasName) const
{
	SubAtlasHandle subAtlasHandle = getSubAtlasHandle(subAtlasName);
	if (subAtlasHandle == 0) {
		UString message("Cannot find texture sub-atlas by '"+subAtlasName+"'! (1)");
		VERSO_OBJECTNOTFOUND("verso-3d", message.c_str(), "");
	}

	const auto& it = subAtlasAndTextureAreaNameToTextureAreaHandle.find(subAtlasHandle);
	if (it == subAtlasAndTextureAreaNameToTextureAreaHandle.end()) {
		UString message("Cannot find texture sub-atlas by '"+subAtlasName+"' -> subAtlasHandle='");
		message.append2(subAtlasHandle);
		message += "'! (2)";
		VERSO_OBJECTNOTFOUND("verso-3d", message.c_str(), "");
	}

	const auto& subAtlasTextureAreas = it->second;
	std::vector<TextureAreaHandle> temp;
	if (subAtlasTextureAreas.size() > 0) {
		for (const auto& it2 : subAtlasTextureAreas) {
			temp.push_back(it2.second);
		}
	}
	return temp;
}


std::vector<UString> TextureAtlas::getTextureAreaNames(const UString& subAtlasName) const
{
	SubAtlasHandle subAtlasHandle = getSubAtlasHandle(subAtlasName);
	if (subAtlasHandle == 0) {
		UString message("Cannot find texture sub-atlas by '"+subAtlasName+"'!");
		VERSO_OBJECTNOTFOUND("verso-3d", message.c_str(), "");
	}

	const auto& textureAreaMapIt = subAtlasAndTextureAreaNameToTextureAreaHandle.find(subAtlasHandle);
	if (textureAreaMapIt == subAtlasAndTextureAreaNameToTextureAreaHandle.end()) {
		UString message("Cannot find TextureAreas map by subAtlasHandle='");
		message.append2(subAtlasHandle);
		message += "' (subAtlasName='"+subAtlasName+"')!";
		VERSO_OBJECTNOTFOUND("verso-3d", message.c_str(), "");
	}

	std::vector<UString> temp;
	for(auto const& it : textureAreaMapIt->second) {
		temp.push_back(it.first);
	}
	return temp;
}


TextureAreaHandle TextureAtlas::getTextureAreaHandle(const UString& subAtlasName, const UString& textureAreaName) const
{
	SubAtlasHandle subAtlasHandle = getSubAtlasHandle(subAtlasName);
	if (subAtlasHandle == 0) {
		UString message("Cannot find texture sub-atlas by '"+subAtlasName+"'!");
		VERSO_OBJECTNOTFOUND("verso-3d", message.c_str(), "");
	}

	const auto& subAtlasTextureAreas = subAtlasAndTextureAreaNameToTextureAreaHandle.find(subAtlasHandle)->second;
	const auto& it = subAtlasTextureAreas.find(textureAreaName);
	if (it != subAtlasTextureAreas.end()) {
		return it->second;
	}
	else {
		UString message("Cannot find TextureArea object by subAtlasName='"+subAtlasName+"', textureAreaName='"+textureAreaName+"'!");
		VERSO_OBJECTNOTFOUND("verso-3d", message.c_str(), "");
	}
}


UString TextureAtlas::getTextureAreaName(TextureAreaHandle textureAreaHandle) const
{
	for (const auto& subAtlas : subAtlasAndTextureAreaNameToTextureAreaHandle) {
		for (const auto& nameHandle : subAtlas.second) {
			if (nameHandle.second == textureAreaHandle)
				return nameHandle.first;
		}
	}

	UString message("Cannot find TextureArea by textureAreaHandle='");
	message.append2(textureAreaHandle);
	message += "'!";
	VERSO_OBJECTNOTFOUND("verso-3d", message.c_str(), "");
}


TextureArea TextureAtlas::getTextureArea(TextureAreaHandle textureAreaHandle) const
{
	auto it = textureAreas.find(textureAreaHandle);
	if (it != textureAreas.end()) {
		return it->second;
	}
	else {
		UString message("Cannot find TextureArea by textureAreaHandle='");
		message.append2(textureAreaHandle);
		message += "'!";
		VERSO_OBJECTNOTFOUND("verso-3d", message.c_str(), "");
	}
}


TextureArea TextureAtlas::getTextureArea(const UString& subAtlasName, const UString& textureAreaName) const
{
	TextureAreaHandle textureAreaHandle = getTextureAreaHandle(subAtlasName, textureAreaName);

	auto it = textureAreas.find(textureAreaHandle);
	if (it != textureAreas.end()) {
		return it->second;
	}
	else {
		UString message("Cannot find TextureArea by textureAreaHandle='");
		message.append2(textureAreaHandle);
		message += "'! (subAtlasName='"+subAtlasName+"', textureAreaName='"+textureAreaName+"')!";
		VERSO_OBJECTNOTFOUND("verso-3d", message.c_str(), "");
	}
}


UString TextureAtlas::toString() const
{
	UString str;
	std::vector<UString> subAtlasNames = getSubAtlasNames();
	for (const auto& subAtlasName : subAtlasNames) {
		str += "sub-atlas \""+subAtlasName+"\"\n";
		const std::vector<std::uint32_t> textureAreaHandles = getTextureAreaHandles(subAtlasName);
		for (const auto& textureAreaHandle : textureAreaHandles) {
			TextureArea textureArea = getTextureArea(textureAreaHandle);
			str += "\""+getTextureAreaName(textureAreaHandle)+"\" [ "+textureArea.getArea().toString()+" ]";
		}
	}
	return str;
}


UString TextureAtlas::toStringDebug() const
{
	UString str("TextureAtlas(");
	str += toString();
	str += ")";
	return str;
}


} // End namespace Verso

