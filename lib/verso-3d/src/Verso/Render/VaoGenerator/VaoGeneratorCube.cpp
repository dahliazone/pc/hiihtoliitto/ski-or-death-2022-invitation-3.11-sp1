#include <Verso/Render/VaoGenerator/VaoGenerator.hpp>
#include <Verso/System/Exception/ErrorException.hpp>

namespace Verso {


void VaoGenerator::cube3d(Vao& vao, const Vector3f& scale, const Vector3f& offsetPosition, BufferTypes buffersToGenerate, const RgbaColorf& color, bool append)
{
	if (!vao.isCreated()) {
		vao.create(PrimitiveType::Triangles);
	}
	else {
		if (vao.getPrimitiveType() != PrimitiveType::Triangles) {
			VERSO_ERROR("verso-3d", "Given Vao was created before with PrimitiveType that is not PrimitiveType::Triangles", vao.toString().c_str());
		}
	}

	// Position
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Position)) {
		Vector3f scaled = scale * 0.5f;
		const Vector3f& op = offsetPosition;
		const GLfloat positions[36][3] = {
			{ op.x-scaled.x, op.y-scaled.y, op.z-scaled.z },
			{ op.x+scaled.x, op.y-scaled.y, op.z-scaled.z },
			{ op.x+scaled.x, op.y+scaled.y, op.z-scaled.z },
			{ op.x+scaled.x, op.y+scaled.y, op.z-scaled.z },
			{ op.x-scaled.x, op.y+scaled.y, op.z-scaled.z },
			{ op.x-scaled.x, op.y-scaled.y, op.z-scaled.z },

			{ op.x-scaled.x, op.y-scaled.y, op.z+scaled.z },
			{ op.x+scaled.x, op.y-scaled.y, op.z+scaled.z },
			{ op.x+scaled.x, op.y+scaled.y, op.z+scaled.z },
			{ op.x+scaled.x, op.y+scaled.y, op.z+scaled.z },
			{ op.x-scaled.x, op.y+scaled.y, op.z+scaled.z },
			{ op.x-scaled.x, op.y-scaled.y, op.z+scaled.z },

			{ op.x-scaled.x, op.y+scaled.y, op.z+scaled.z },
			{ op.x-scaled.x, op.y+scaled.y, op.z-scaled.z },
			{ op.x-scaled.x, op.y-scaled.y, op.z-scaled.z },
			{ op.x-scaled.x, op.y-scaled.y, op.z-scaled.z },
			{ op.x-scaled.x, op.y-scaled.y, op.z+scaled.z },
			{ op.x-scaled.x, op.y+scaled.y, op.z+scaled.z },

			{ op.x+scaled.x, op.y+scaled.y, op.z+scaled.z },
			{ op.x+scaled.x, op.y+scaled.y, op.z-scaled.z },
			{ op.x+scaled.x, op.y-scaled.y, op.z-scaled.z },
			{ op.x+scaled.x, op.y-scaled.y, op.z-scaled.z },
			{ op.x+scaled.x, op.y-scaled.y, op.z+scaled.z },
			{ op.x+scaled.x, op.y+scaled.y, op.z+scaled.z },

			{ op.x-scaled.x, op.y-scaled.y, op.z-scaled.z },
			{ op.x+scaled.x, op.y-scaled.y, op.z-scaled.z },
			{ op.x+scaled.x, op.y-scaled.y, op.z+scaled.z },
			{ op.x+scaled.x, op.y-scaled.y, op.z+scaled.z },
			{ op.x-scaled.x, op.y-scaled.y, op.z+scaled.z },
			{ op.x-scaled.x, op.y-scaled.y, op.z-scaled.z },

			{ op.x-scaled.x, op.y+scaled.y, op.z-scaled.z },
			{ op.x+scaled.x, op.y+scaled.y, op.z-scaled.z },
			{ op.x+scaled.x, op.y+scaled.y, op.z+scaled.z },
			{ op.x+scaled.x, op.y+scaled.y, op.z+scaled.z },
			{ op.x-scaled.x, op.y+scaled.y, op.z+scaled.z },
			{ op.x-scaled.x, op.y+scaled.y, op.z-scaled.z } };

		if (append == false) {
			vao.setDataf(ShaderAttribute::Position, positions, 36, 3);
		}
		else {
			vao.appendDataf(ShaderAttribute::Position, positions, 36, 3);
		}
	}

	// Uv
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Uv)) {
		GLfloat uvs[36][2] = {
			{ 0.0f, 0.0f },
			{ 1.0f, 0.0f },
			{ 1.0f, 1.0f },
			{ 1.0f, 1.0f },
			{ 0.0f, 1.0f },
			{ 0.0f, 0.0f },

			{ 0.0f, 0.0f },
			{ 1.0f, 0.0f },
			{ 1.0f, 1.0f },
			{ 1.0f, 1.0f },
			{ 0.0f, 1.0f },
			{ 0.0f, 0.0f },

			{ 1.0f, 0.0f },
			{ 1.0f, 1.0f },
			{ 0.0f, 1.0f },
			{ 0.0f, 1.0f },
			{ 0.0f, 0.0f },
			{ 1.0f, 0.0f },

			{ 1.0f, 0.0f },
			{ 1.0f, 1.0f },
			{ 0.0f, 1.0f },
			{ 0.0f, 1.0f },
			{ 0.0f, 0.0f },
			{ 1.0f, 0.0f },

			{ 0.0f, 1.0f },
			{ 1.0f, 1.0f },
			{ 1.0f, 0.0f },
			{ 1.0f, 0.0f },
			{ 0.0f, 0.0f },
			{ 0.0f, 1.0f },

			{ 0.0f, 1.0f },
			{ 1.0f, 1.0f },
			{ 1.0f, 0.0f },
			{ 1.0f, 0.0f },
			{ 0.0f, 0.0f },
			{ 0.0f, 1.0f } };

		if (append == false) {
			vao.setDataf(ShaderAttribute::Uv, uvs, 36, 2);
		}
		else {
			vao.appendDataf(ShaderAttribute::Uv, uvs, 36, 2);
		}
	}

	// Normal
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Normal)) {
		const GLfloat normals[36][3] = {
			{  0.0f,  0.0f, -1.0f },
			{  0.0f,  0.0f, -1.0f },
			{  0.0f,  0.0f, -1.0f },
			{  0.0f,  0.0f, -1.0f },
			{  0.0f,  0.0f, -1.0f },
			{  0.0f,  0.0f, -1.0f },

			{  0.0f,  0.0f,  1.0f },
			{  0.0f,  0.0f,  1.0f },
			{  0.0f,  0.0f,  1.0f },
			{  0.0f,  0.0f,  1.0f },
			{  0.0f,  0.0f,  1.0f },
			{  0.0f,  0.0f,  1.0f },

			{ -1.0f,  0.0f,  0.0f },
			{ -1.0f,  0.0f,  0.0f },
			{ -1.0f,  0.0f,  0.0f },
			{ -1.0f,  0.0f,  0.0f },
			{ -1.0f,  0.0f,  0.0f },
			{ -1.0f,  0.0f,  0.0f },

			{  1.0f,  0.0f,  0.0f },
			{  1.0f,  0.0f,  0.0f },
			{  1.0f,  0.0f,  0.0f },
			{  1.0f,  0.0f,  0.0f },
			{  1.0f,  0.0f,  0.0f },
			{  1.0f,  0.0f,  0.0f },

			{  0.0f, -1.0f,  0.0f },
			{  0.0f, -1.0f,  0.0f },
			{  0.0f, -1.0f,  0.0f },
			{  0.0f, -1.0f,  0.0f },
			{  0.0f, -1.0f,  0.0f },
			{  0.0f, -1.0f,  0.0f },

			{  0.0f,  1.0f,  0.0f },
			{  0.0f,  1.0f,  0.0f },
			{  0.0f,  1.0f,  0.0f },
			{  0.0f,  1.0f,  0.0f },
			{  0.0f,  1.0f,  0.0f },
			{  0.0f,  1.0f,  0.0f } };

		if (append == false) {
			vao.setDataf(ShaderAttribute::Normal, normals, 36, 3);
		}
		else {
			vao.appendDataf(ShaderAttribute::Normal, normals, 36, 3);
		}
	}


	// Color
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Color)) {
		const GLfloat colors[36][3] = {
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b },
			{ color.r, color.g, color.b } };

		if (append == false) {
			vao.setDataf(ShaderAttribute::Color, colors, 36, 3);
		}
		else {
			vao.appendDataf(ShaderAttribute::Color, colors, 36, 3);
		}
	}
}


} // End namespace Verso

