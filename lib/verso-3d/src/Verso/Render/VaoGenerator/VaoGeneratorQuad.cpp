#include <Verso/Render/VaoGenerator/VaoGenerator.hpp>

namespace Verso {


void VaoGenerator::quad2d(Vao& vao, const Vector2f& size, const Vector2f& offsetPosition, BufferTypes buffersToGenerate, const RgbaColorf& color, bool textureInvertY)
{
	VERSO_ASSERT("verso-3d", buffersToGenerate != 0 && "You must defined at least one BufferType to generate!");

	vao.create(PrimitiveType::Triangles);

	// Position
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Position)) {
		Vector2f scaled = size * 0.5f;
		const Vector2f& op = offsetPosition;
		GLfloat positions[] = {
			op.x + scaled.x, op.y - scaled.y, // Top Right
			op.x + scaled.x, op.y + scaled.y, // Bottom Right
			op.x - scaled.x, op.y + scaled.y, // Bottom Left
			op.x - scaled.x, op.y - scaled.y  // Top Left
		};

		vao.setDataf(ShaderAttribute::Position, positions, 4, 2);
	}

	// Uv
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Uv)) {
		if (textureInvertY == false) {
			GLfloat uvs[] = {
				1.0f, 0.0f, // Top Right
				1.0f, 1.0f, // Bottom Right
				0.0f, 1.0f, // Bottom Left
				0.0f, 0.0f, // Top Left
			};
			vao.setDataf(ShaderAttribute::Uv, uvs, 4, 2);
		}
		else {
			GLfloat uvs[] = {
				1.0f, 1.0f, // Top Right
				1.0f, 0.0f, // Bottom Right
				0.0f, 0.0f, // Bottom Left
				0.0f, 1.0f, // Top Left
			};
			vao.setDataf(ShaderAttribute::Uv, uvs, 4, 2);
		}
	}

	// Normal
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Normal)) {
		const GLfloat normalDir = 1.0f;
		GLfloat normals[] = {
			0.0f, 0.0f, normalDir, // Top Right
			0.0f, 0.0f, normalDir, // Bottom Right
			0.0f, 0.0f, normalDir, // Bottom Left
			0.0f, 0.0f, normalDir  // Top Left
		};
		vao.setDataf(ShaderAttribute::Normal, normals, 4, 3);
	}

	// Color
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Color)) {
		const GLfloat colors[4][3] = {
			{ color.r, color.g, color.b },   // Top Right
			{ color.r, color.g, color.b },   // Bottom Right
			{ color.r, color.g, color.b },   // Bottom Left
			{ color.r, color.g, color.b } }; // Top Left
		vao.setDataf(ShaderAttribute::Color, colors, 4, 3);
	}

	// Indices
	GLuint indices[] = {
		0, 1, 3,  // First Triangle
		1, 2, 3   // Second Triangle
	};
	vao.setIndices(indices, 6);
}


void VaoGenerator::quad3d(Vao& vao, const Vector2f& size, const Vector3f& offsetPosition, BufferTypes buffersToGenerate, const RgbaColorf& color, bool textureInvertY)
{
	VERSO_ASSERT("verso-3d", buffersToGenerate != 0 && "You must defined at least one BufferType to generate!");

	vao.create(PrimitiveType::Triangles);

	// Position
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Position)) {
		Vector3f scaled = size * 0.5f;
		const Vector3f& op = offsetPosition;
		GLfloat positions[] = {
			op.x + scaled.x, op.y - scaled.y, op.z, // Top Right
			op.x + scaled.x, op.y + scaled.y, op.z, // Bottom Right
			op.x - scaled.x, op.y + scaled.y, op.z, // Bottom Left
			op.x - scaled.x, op.y - scaled.y, op.z  // Top Left
		};
		vao.setDataf(ShaderAttribute::Position, positions, 4, 3);
	}

	// Uv
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Uv)) {
		if (textureInvertY == false) {
			GLfloat uvs[] = {
				0.0f, 1.0f, // Top Right
				0.0f, 0.0f, // Bottom Right
				1.0f, 0.0f, // Bottom Left
				1.0f, 1.0f, // Top Left
			};
			vao.setDataf(ShaderAttribute::Uv, uvs, 4, 2);
		}
		else {
			GLfloat uvs[] = {
				0.0f, 0.0f, // Top Right
				0.0f, 1.0f, // Bottom Right
				1.0f, 1.0f, // Bottom Left
				1.0f, 0.0f, // Top Left
			};
			vao.setDataf(ShaderAttribute::Uv, uvs, 4, 2);
		}
	}

	// Normal
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Normal)) {
		const GLfloat normalDir = 1.0f;
		GLfloat normals[] = {
			0.0f, 0.0f, normalDir, // Top Right
			0.0f, 0.0f, normalDir, // Bottom Right
			0.0f, 0.0f, normalDir, // Bottom Left
			0.0f, 0.0f, normalDir  // Top Left
		};
		vao.setDataf(ShaderAttribute::Normal, normals, 4, 3);
	}

	// Color
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Color)) {
		const GLfloat colors[4][3] = {
			{ color.r, color.g, color.b },   // Top Right
			{ color.r, color.g, color.b },   // Bottom Right
			{ color.r, color.g, color.b },   // Bottom Left
			{ color.r, color.g, color.b } }; // Top Left
		vao.setDataf(ShaderAttribute::Color, colors, 4, 3);
	}

	// Indices
	GLuint indices[] = {
		0, 1, 3,  // First Triangle
		1, 2, 3   // Second Triangle
	};
	vao.setIndices(indices, 6);
}


} // End namespace Verso

