#include <Verso/Render/VaoGenerator/VaoGenerator.hpp>

namespace Verso {


void VaoGenerator::skybox3d(Vao& vaoPosX, Vao& vaoNegX, Vao& vaoPosY, Vao& vaoNegY, Vao& vaoPosZ, Vao& vaoNegZ, const ICamera& camera, const Vector3f& offsetPosition)
{
	vaoPosX.create(PrimitiveType::Triangles);
	vaoNegX.create(PrimitiveType::Triangles);
	vaoPosY.create(PrimitiveType::Triangles);
	vaoNegY.create(PrimitiveType::Triangles);
	vaoPosZ.create(PrimitiveType::Triangles);
	vaoNegZ.create(PrimitiveType::Triangles);

	// Position
	auto nearFar = camera.getNearFarPlane();
	float scaled = 2.0f / 3.0f * std::sqrt(3.0f) * nearFar.maxValue;
	const Vector3f& op = offsetPosition;

	const GLfloat positionsPosX[6][3] = {
		{ op.x+scaled, op.y+scaled, op.y+scaled },
		{ op.x+scaled, op.y+scaled, op.y-scaled },
		{ op.x+scaled, op.y-scaled, op.y-scaled },
		{ op.x+scaled, op.y-scaled, op.y-scaled },
		{ op.x+scaled, op.y-scaled, op.y+scaled },
		{ op.x+scaled, op.y+scaled, op.y+scaled } };

	vaoPosX.setDataf(ShaderAttribute::Position, positionsPosX, 6, 3);

	const GLfloat positionsNegX[6][3] = {
		{ op.x-scaled, op.y+scaled, op.y+scaled },
		{ op.x-scaled, op.y+scaled, op.y-scaled },
		{ op.x-scaled, op.y-scaled, op.y-scaled },
		{ op.x-scaled, op.y-scaled, op.y-scaled },
		{ op.x-scaled, op.y-scaled, op.y+scaled },
		{ op.x-scaled, op.y+scaled, op.y+scaled } };

	vaoNegX.setDataf(ShaderAttribute::Position, positionsNegX, 6, 3);

	const GLfloat positionsPosY[6][3] = {
		{ op.x-scaled, op.y+scaled, op.y-scaled },
		{ op.x+scaled, op.y+scaled, op.y-scaled },
		{ op.x+scaled, op.y+scaled, op.y+scaled },
		{ op.x+scaled, op.y+scaled, op.y+scaled },
		{ op.x-scaled, op.y+scaled, op.y+scaled },
		{ op.x-scaled, op.y+scaled, op.y-scaled } };

	vaoPosY.setDataf(ShaderAttribute::Position, positionsPosY, 6, 3);

	const GLfloat positionsNegY[6][3] = {
		{ op.x-scaled, op.y-scaled, op.y-scaled },
		{ op.x+scaled, op.y-scaled, op.y-scaled },
		{ op.x+scaled, op.y-scaled, op.y+scaled },
		{ op.x+scaled, op.y-scaled, op.y+scaled },
		{ op.x-scaled, op.y-scaled, op.y+scaled },
		{ op.x-scaled, op.y-scaled, op.y-scaled } };

	vaoNegY.setDataf(ShaderAttribute::Position, positionsNegY, 6, 3);

	const GLfloat positionsPosZ[6][3] = {
		{ op.x-scaled, op.y-scaled, op.y+scaled },
		{ op.x+scaled, op.y-scaled, op.y+scaled },
		{ op.x+scaled, op.y+scaled, op.y+scaled },
		{ op.x+scaled, op.y+scaled, op.y+scaled },
		{ op.x-scaled, op.y+scaled, op.y+scaled },
		{ op.x-scaled, op.y-scaled, op.y+scaled } };

	vaoPosZ.setDataf(ShaderAttribute::Position, positionsPosZ, 6, 3);

	const GLfloat positionsNegZ[6][3] = {
		{ op.x-scaled, op.y-scaled, op.y-scaled },
		{ op.x+scaled, op.y-scaled, op.y-scaled },
		{ op.x+scaled, op.y+scaled, op.y-scaled },
		{ op.x+scaled, op.y+scaled, op.y-scaled },
		{ op.x-scaled, op.y+scaled, op.y-scaled },
		{ op.x-scaled, op.y-scaled, op.y-scaled } };

	vaoNegZ.setDataf(ShaderAttribute::Position, positionsNegZ, 6, 3);


	// Uv
	GLfloat uvsPosX[6][2] = {
		{ 1.0f, 0.0f },
		{ 0.0f, 0.0f },
		{ 0.0f, 1.0f },
		{ 0.0f, 1.0f },
		{ 1.0f, 1.0f },
		{ 1.0f, 0.0f } };

	vaoPosX.setDataf(ShaderAttribute::Uv, uvsPosX, 6, 2);

	GLfloat uvsNegX[6][2] = {
		{ 0.0f, 0.0f },
		{ 1.0f, 0.0f },
		{ 1.0f, 1.0f },
		{ 1.0f, 1.0f },
		{ 0.0f, 1.0f },
		{ 0.0f, 0.0f } };

	vaoNegX.setDataf(ShaderAttribute::Uv, uvsNegX, 6, 2);

	GLfloat uvsPosY[6][2] = {
		{ 0.0f, 1.0f },
		{ 1.0f, 1.0f },
		{ 1.0f, 0.0f },
		{ 1.0f, 0.0f },
		{ 0.0f, 0.0f },
		{ 0.0f, 1.0f } };

	vaoPosY.setDataf(ShaderAttribute::Uv, uvsPosY, 6, 2);

	GLfloat uvsNegY[6][2] = {
		{ 0.0f, 0.0f },
		{ 1.0f, 0.0f },
		{ 1.0f, 1.0f },
		{ 1.0f, 1.0f },
		{ 0.0f, 1.0f },
		{ 0.0f, 0.0f } };

	vaoNegY.setDataf(ShaderAttribute::Uv, uvsNegY, 6, 2);

	GLfloat uvsvaoPosZ[6][2] = {
		{ 1.0f, 1.0f },
		{ 0.0f, 1.0f },
		{ 0.0f, 0.0f },
		{ 0.0f, 0.0f },
		{ 1.0f, 0.0f },
		{ 1.0f, 1.0f } };

	vaoPosZ.setDataf(ShaderAttribute::Uv, uvsvaoPosZ, 6, 2);

	GLfloat uvsNegZ[6][2] = {
		{ 0.0f, 1.0f },
		{ 1.0f, 1.0f },
		{ 1.0f, 0.0f },
		{ 1.0f, 0.0f },
		{ 0.0f, 0.0f },
		{ 0.0f, 1.0f } };

	vaoNegZ.setDataf(ShaderAttribute::Uv, uvsNegZ, 6, 2);
}


} // End namespace Verso

