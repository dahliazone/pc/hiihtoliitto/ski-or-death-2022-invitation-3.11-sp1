#include <Verso/Render/VaoGenerator/VaoGenerator.hpp>

namespace Verso {

void VaoGenerator::diamond2d(Vao& vao, const Vector2f& scale, const Vector2f& offsetPosition, BufferTypes buffersToGenerate, const RgbaColorf& color)
{
	VERSO_ASSERT("verso-3d", buffersToGenerate != 0 && "You must defined at least one BufferType to generate!");

	vao.create(PrimitiveType::LineLoop); // \TODO: Triangles?

	// Position
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Position)) {
		Vector2f scaled = scale * 0.5f;
		const Vector2f& op = offsetPosition;
		const GLfloat positions[4][2] = {
		    { op.x+0.0f,      op.y+scaled.y },   // Top point
		    { op.x+scaled.x,  op.y+0.0f     },   // Right point
		    { op.x+0.0f,      op.y-scaled.y },   // Bottom point
		    { op.x-scaled.x,  op.y+0.0f     } }; // Left point
		vao.setDataf(ShaderAttribute::Position, positions, 4, 2);
	}

	// Uv
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Uv)) {
		const GLfloat uvs[4][2] = {
		    { 0.0f, 0.0f },   // Top point
		    { 1.0f, 0.0f },   // Right point
		    { 1.0f, 1.0f },   // Bottom point
		    { 0.0f, 1.0f } }; // Left point
		vao.setDataf(ShaderAttribute::Uv, uvs, 4, 2);
	}

	// Normal
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Normal)) {
		const GLfloat normalDir = 1.0f;
		const GLfloat normals[4][3] = {
		    { 0.0f, 0.0f, normalDir },   // Top point
		    { 0.0f, 0.0f, normalDir },   // Right point
		    { 0.0f, 0.0f, normalDir },   // Bottom point
		    { 0.0f, 0.0f, normalDir } }; // Left point
		vao.setDataf(ShaderAttribute::Normal, normals, 4, 3);
	}

	// Color
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Color)) {
		const GLfloat colors[4][3] = {
		    {  color.r, color.g, color.b  },   // Top point
		    {  color.r, color.g, color.b  },   // Right point
		    {  color.r, color.g, color.b  },   // Bottom point
		    {  color.r, color.g, color.b  } }; // Left point
		vao.setDataf(ShaderAttribute::Color, colors, 4, 3);
	}
}


void VaoGenerator::diamond3d(Vao& vao, const Vector3f& scale, const Vector3f& offsetPosition, BufferTypes buffersToGenerate, const RgbaColorf& color)
{
	VERSO_ASSERT("verso-3d", buffersToGenerate != 0 && "You must defined at least one BufferType to generate!");

	vao.create(PrimitiveType::LineLoop); // \TODO: Triangles?

	// Position
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Position)) {
		Vector3f scaled = scale * 0.5f;
		const Vector3f& op = offsetPosition;
		const GLfloat positions[4][3] = {
		    { op.x+0.0f,      op.y+scaled.y, op.z+0.0f },   // Top point
		    { op.x+scaled.x,  op.y+0.0f,     op.z+0.0f },   // Right point
		    { op.x+0.0f,      op.y-scaled.y, op.z+0.0f },   // Bottom point
		    { op.x+-scaled.x, op.y+0.0f,     op.z+0.0f } }; // Left point
		vao.setDataf(ShaderAttribute::Position, positions, 4, 3);
	}

	// Uv
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Uv)) {
		const GLfloat uvs[4][2] = {
		    { 0.0f, 0.0f },   // Top point
		    { 1.0f, 0.0f },   // Right point
		    { 1.0f, 1.0f },   // Bottom point
		    { 0.0f, 1.0f } }; // Left point
		vao.setDataf(ShaderAttribute::Uv, uvs, 4, 2);
	}

	// Normal
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Normal)) {
		const GLfloat normalDir = 1.0f;
		const GLfloat normals[4][3] = {
		    { 0.0f, 0.0f, normalDir },   // Top point
		    { 0.0f, 0.0f, normalDir },   // Right point
		    { 0.0f, 0.0f, normalDir },   // Bottom point
		    { 0.0f, 0.0f, normalDir } }; // Left point
		vao.setDataf(ShaderAttribute::Normal, normals, 4, 3);
	}

	// Color
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Color)) {
		const GLfloat colors[4][3] = {
		    {  color.r, color.g, color.b  },   // Top point
		    {  color.r, color.g, color.b  },   // Right point
		    {  color.r, color.g, color.b  },   // Bottom point
		    {  color.r, color.g, color.b  } }; // Left point
		vao.setDataf(ShaderAttribute::Color, colors, 4, 3);
	}
}


} // End namespace Verso

