/**
 * Sphere generators by Tapani Niemi (exigo / Dahlia).
 */
#include <Verso/Render/VaoGenerator/VaoGenerator.hpp>
#include <cmath>

namespace Verso {


void VaoGenerator::sphere3d(Vao& vao, float size, int stacks, int slices, const Vector3f& offsetPosition, BufferTypes buffersToGenerate)
{
	// \TODO: bind() will cause problems because create() has not been called.
	// FIX THIS eitherway: 1) bind() to not cause problems (preferred?), 2) create "empty" VAO
	if (stacks <= 0 || slices <= 0) return;

	vao.create(PrimitiveType::Triangles);

	std::vector<Vector3f> positions;
	std::vector<Vector3f> normals;
	std::vector<Vector2f> uvs;

	static const float PI = static_cast<float>(M_PI);
	static const float TWO_PI = static_cast<float>(M_PI) * 2.0f;

	for (int stack = 0; stack < stacks; stack++) {
		for (int slice = 0; slice < slices; slice++) {
			// Y
			const float hc = float(stack) / float(stacks);
			const float hn = float(stack + 1) / float(stacks);
			const float rc = size * std::sin(hc * PI);
			const float rn = size * std::sin(hn * PI);
			const float yc = std::cos(hc * PI) * size;
			const float yn = std::cos(hn * PI) * size;

			// XZ
			const float s0 = float(slice) / float(slices);
			const float s1 = float(slice + 1) / float(slices);
			const float angle0 = s0 * TWO_PI;
			const float angle1 = s1 * TWO_PI;

			const float xc0 = rc * std::sin(angle0);
			const float zc0 = rc * std::cos(angle0);
			const float xc1 = rc * std::sin(angle1);
			const float zc1 = rc * std::cos(angle1);
			const float xn0 = rn * std::sin(angle0);
			const float zn0 = rn * std::cos(angle0);
			const float xn1 = rn * std::sin(angle1);
			const float zn1 = rn * std::cos(angle1);

			if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Position)) {
				positions.push_back(offsetPosition + Vector3f(xc0, yc, zc0));
				positions.push_back(offsetPosition + Vector3f(xc1, yc, zc1));
				positions.push_back(offsetPosition + Vector3f(xn0, yn, zn0));

				positions.push_back(offsetPosition + Vector3f(xn0, yn, zn0));
				positions.push_back(offsetPosition + Vector3f(xn1, yn, zn1));
				positions.push_back(offsetPosition + Vector3f(xc1, yc, zc1));
			}

			if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Normal)) {
				normals.push_back(Vector3f(xc0, yc, zc0).getNormalized());
				normals.push_back(Vector3f(xc1, yc, zc1).getNormalized());
				normals.push_back(Vector3f(xn0, yn, zn0).getNormalized());

				normals.push_back(Vector3f(xn0, yn, zn0).getNormalized());
				normals.push_back(Vector3f(xn1, yn, zn1).getNormalized());
				normals.push_back(Vector3f(xc1, yc, zc1).getNormalized());
			}

			if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Uv)) {
				Vector3f n1 = Vector3f(xc0, yc, zc0).getNormalized();
				Vector3f n2 = Vector3f(xc1, yc, zc1).getNormalized();
				Vector3f n3 = Vector3f(xn0, yn, zn0).getNormalized();

				Vector3f n4 = Vector3f(xn0, yn, zn0).getNormalized();
				Vector3f n5 = Vector3f(xn1, yn, zn1).getNormalized();
				Vector3f n6 = Vector3f(xc1, yc, zc1).getNormalized();

				uvs.push_back(Vector2f(0.5f + std::atan2(n1.z, n1.x) / Math::TwoPi(), 0.5f - std::asin(n1.y) / Math::Pi()));
				uvs.push_back(Vector2f(0.5f + std::atan2(n2.z, n2.x) / Math::TwoPi(), 0.5f - std::asin(n2.y) / Math::Pi()));
				uvs.push_back(Vector2f(0.5f + std::atan2(n3.z, n3.x) / Math::TwoPi(), 0.5f - std::asin(n3.y) / Math::Pi()));

				uvs.push_back(Vector2f(0.5f + std::atan2(n4.z, n4.x) / Math::TwoPi(), 0.5f - std::asin(n4.y) / Math::Pi()));
				uvs.push_back(Vector2f(0.5f + std::atan2(n5.z, n5.x) / Math::TwoPi(), 0.5f - std::asin(n5.y) / Math::Pi()));
				uvs.push_back(Vector2f(0.5f + std::atan2(n6.z, n6.x) / Math::TwoPi(), 0.5f - std::asin(n6.y) / Math::Pi()));

//				uvs.push_back(Vector2f(static_cast<float>(slice+1) / static_cast<float>(slices), static_cast<float>(stack+1) / static_cast<float>(stacks)));
//				uvs.push_back(Vector2f(static_cast<float>(slice+1) / static_cast<float>(slices), static_cast<float>(stack+1) / static_cast<float>(stacks)));
//				uvs.push_back(Vector2f(static_cast<float>(slice+1) / static_cast<float>(slices), static_cast<float>(stack+1) / static_cast<float>(stacks)));

//				uvs.push_back(Vector2f(static_cast<float>(slice+1) / static_cast<float>(slices), static_cast<float>(stack+1) / static_cast<float>(stacks)));
//				uvs.push_back(Vector2f(static_cast<float>(slice+1) / static_cast<float>(slices), static_cast<float>(stack+1) / static_cast<float>(stacks)));
//				uvs.push_back(Vector2f(static_cast<float>(slice+1) / static_cast<float>(slices), static_cast<float>(stack+1) / static_cast<float>(stacks)));
			}
		}
	}

	// Position
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Position)) {
		vao.setDataf(ShaderAttribute::Position, &positions[0], static_cast<int>(positions.size()), 3);
	}

	// Normal
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Normal)) {
		vao.setDataf(ShaderAttribute::Normal, &normals[0], static_cast<int>(normals.size()), 3);
	}

	// Uv
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Uv)) {
		vao.setDataf(ShaderAttribute::Uv, &uvs[0], static_cast<int>(uvs.size()), 3);
	}
}


/*void createSphere(Object& object, float size, int stacks, int slices)
{
	object.vertices.clear();
	object.faces.clear();

	static const float PI = M_PI;
	static const float TWO_PI = M_PI * 2.0f;

	typedef std::pair<int,int> Pair;
	std::map<Pair,int> m;

	// Create vertices
	for (int stack = 0; stack <= stacks; stack++) {
		for (int slice = 0; slice <= slices; slice++) {
			// Y
			const float h = float(stack) / float(stacks);
			const float r = size * sin(h * PI);
			const float y = std::cos(h * PI) * size;

			// XZ
			const float s = float(slice) / float(slices);
			const float a = s * TWO_PI;
			const float x = r * std::sin(a);
			const float z = r * std::cos(a);

			// Vertex
			Xyz vertex(x, y, z);

			// Add vertex to vertex vector
			int index = object.vertices.size();
			object.vertices.push_back(vertex);

			// Index vertex
			Pair ip(stack, slice);
			m[ip] = index;
		}
	}

	// Create faces that are based on indices to vertices
	for (int stack = 0; stack < stacks; stack++) {
		for (int slice = 0; slice < slices; slice++) {
			const int ic0 = m[Pair(stack, slice)];
			const int ic1 = m[Pair(stack, slice + 1)];
			const int in0 = m[Pair(stack + 1, slice)];
			const int in1 = m[Pair(stack + 1, slice + 1)];

			Face f1(ic1, ic0, in0);
			Xyz v1(object.vertices[ic1]);
			Xyz v2(object.vertices[ic0]);
			Xyz v3(object.vertices[in0]);
			f1.normal = (v1 + v2 + v3).normalized();
			object.faces.push_back(f1);

			Face f2(ic1, in0, in1);
			Xyz v4(object.vertices[ic1]);
			Xyz v5(object.vertices[in0]);
			Xyz v6(object.vertices[in1]);
			f2.normal = (v4 + v5 + v6).normalized();
			object.faces.push_back(f2);
		}
	}
}*/


/**
 * Find or create index of a vertex that lies between two edge vertices pointed
 * by indices given as parameters.
 *
 * @param vertices [in,out] Vertices possibly appended with a new vertex.
 * @param indexPairs [in,out] Searched and possibly appended index pair map.
 *        This can be thought of as an "edge to vertex index map".
 * @param i1 [in] Index of vertex 1.
 * @param i2 [in] Index of vertex 2.
 */
/*int findMiddle(std::vector<Xyz>& vertices, std::map<std::pair<int,int>,int>& indexPairs, int i1, int i2)
{
	// Note: the vertex indices in the map MUST BE ordered (because the edge
	// can be sought either way)
	const int larger = (i1 < i2 ? i2 : i1);
	const int smaller = (i1 < i2 ? i1 : i2);
	const std::pair<int,int> indexPair(larger, smaller);

	// Return existing vertex index
	const std::map<std::pair<int,int>,int>::const_iterator i = indexPairs.find(indexPair);
	if (i != indexPairs.end()) {
		return i->second;
	}

	// ..or create new vertex, new vertex index, new "edge", and save them
	const Xyz& v1 = vertices[i1];
	const Xyz& v2 = vertices[i2];
	Xyz newVertex = (v1 + v2) * 0.5f;

	// Make the new vertex have the sphere radius
	newVertex = newVertex.normalized() * v1.length();

	const int newIndex = vertices.size();
	vertices.push_back(newVertex);
	indexPairs[indexPair] = newIndex;

	return newIndex;
}*/

/**
 * Creates an icosphere mesh with given number of refinement levels. Note that
 * the levels affect the number of polygons (triangles) in the mesh quite
 * drastically. The levels go as: R0=20, R1=80, R2=320, R3=1280, R4=5120, and
 * so on. The number after the equal sign denotes the number of polygons.
 *
 * @param object [in,out] Object to create (mainly vertices and faces).
 * @param size [in] Size, not radius actually.
 * @param level [in] Number of refinement levels.
 */
/*void createIcosphere(Object& object, float size, int level)
{
	// Create 20-sided icosahedron
	const float s = size;
	const float t = (1 + sqrtf(5.0f)) * 0.5f * s;

	object.vertices.push_back(Xyz(-s, +t, 0));
	object.vertices.push_back(Xyz(+s, +t, 0));
	object.vertices.push_back(Xyz(-s, -t, 0));
	object.vertices.push_back(Xyz(+s, -t, 0));

	object.vertices.push_back(Xyz(0, -s, +t));
	object.vertices.push_back(Xyz(0, +s, +t));
	object.vertices.push_back(Xyz(0, -s, -t));
	object.vertices.push_back(Xyz(0, +s, -t));

	object.vertices.push_back(Xyz(+t, 0, -s));
	object.vertices.push_back(Xyz(+t, 0, +s));
	object.vertices.push_back(Xyz(-t, 0, -s));
	object.vertices.push_back(Xyz(-t, 0, +s));

	object.faces.push_back(Face(0, 11, 5));
	object.faces.push_back(Face(0, 5, 1));
	object.faces.push_back(Face(0, 1, 7));
	object.faces.push_back(Face(0, 7, 10));
	object.faces.push_back(Face(0, 10, 11));

	object.faces.push_back(Face(1, 5, 9));
	object.faces.push_back(Face(5, 11, 4));
	object.faces.push_back(Face(11, 10, 2));
	object.faces.push_back(Face(10, 7, 6));
	object.faces.push_back(Face(7, 1, 8));

	object.faces.push_back(Face(3, 9, 4));
	object.faces.push_back(Face(3, 4, 2));
	object.faces.push_back(Face(3, 2, 6));
	object.faces.push_back(Face(3, 6, 8));
	object.faces.push_back(Face(3, 8, 9));

	object.faces.push_back(Face(4, 9, 5));
	object.faces.push_back(Face(2, 4, 11));
	object.faces.push_back(Face(6, 2, 10));
	object.faces.push_back(Face(8, 6, 7));
	object.faces.push_back(Face(9, 8, 1));

	// Refine the icosahedron to icosphere by N levels
	std::map<std::pair<int,int>,int> indexPairs;

	for (int i = 0; i < level; i++) {
		std::vector<Face> newFaces;
		for (std::vector<Face>::const_iterator fi = object.faces.begin(); fi != object.faces.end(); ++fi) {
			const Face& face = *fi;
			int m12 = findMiddle(object.vertices, indexPairs, face.v1, face.v2);
			int m13 = findMiddle(object.vertices, indexPairs, face.v1, face.v3);
			int m23 = findMiddle(object.vertices, indexPairs, face.v2, face.v3);

			Face f1(face.v1, m12, m13); f1.normal = (object.vertices[face.v1] + object.vertices[m12] + object.vertices[m13]).normalized();
			Face f2(face.v2, m23, m12); f2.normal = (object.vertices[face.v2] + object.vertices[m23] + object.vertices[m12]).normalized();
			Face f3(face.v3, m13, m23); f3.normal = (object.vertices[face.v3] + object.vertices[m13] + object.vertices[m23]).normalized();
			Face f4(m12, m23, m13); f4.normal = (object.vertices[m12] + object.vertices[m23] + object.vertices[m13]).normalized();

			newFaces.push_back(f1);
			newFaces.push_back(f2);
			newFaces.push_back(f3);
			newFaces.push_back(f4);
		}
		object.faces = newFaces;
	}
}*/





/*
// REALLY OLD BUGGY FIRST EXIGO VERSION FROM THE WEEKEND
void VaoGenerator::sphere3d(Vao &vao, float size, int stacks, int slices, BufferTypes buffers)
{
	// \TODO: bind() will cause problems because create() has not been called.
	// FIX THIS eitherway: 1) bind() to not cause problems (preferred?), 2) create "empty" VAO
	if (stacks <= 0 || slices <= 0) return;

	vao.create(PrimitiveType::Triangles);

	const float halfSize = size * 0.5f;
	const float stackHeight = size / float(stacks);

	struct Xyz {
		float x, y, z;
	};

	std::vector<Xyz> positions; // \TODO: reserve

	for (int h = 0; h < stacks; h++) {
		for (int s = 0; s < slices; s++) {
			// Explanation: r=radius (from origin in xz-plane), p=position, x=x, z=z, y=y, 1=this, 2=next
			const float r1 = std::sin(float(h) / float(stacks) * M_PI) * size;
			const float r2 = std::sin(float(h + 1) / float(stacks) * M_PI) * size;
			const float p1 = float(s) / float(slices) * M_2_PI;
			const float p2 = float(s + 1) / float(slices) * M_2_PI;
			// Explanation: 11=this on y1, 21=this on y2, 22=next on y2
			const float x11 = std::sin(p1) * r1;
			const float x12 = std::sin(p2) * r1;
			const float z11 = std::cos(p1) * r1;
			const float z12 = std::cos(p2) * r1;
			const float x21 = std::sin(p1) * r2;
			const float x22 = std::sin(p2) * r2;
			const float z21 = std::cos(p1) * r2;
			const float z22 = std::cos(p2) * r2;
			const float y1 = h * stackHeight - halfSize;
			const float y2 = (h + 1) * stackHeight - halfSize;

			// \TODO: fix clockwiseness below after backface culling has been enabled
			// Hint: x and z always come in pairs (e.g. x11+z11 or x12+z12), y differs

			Xyz v1;
			v1.x = x11;
			v1.z = z11;
			v1.y = y1;

			Xyz v2;
			v2.x = x21;
			v2.z = z21;
			v2.y = y2;

			Xyz v3;
			v3.x = x22;
			v3.z = z22;
			v3.y = y2;

			positions.push_back(v1);
			positions.push_back(v2);
			positions.push_back(v3);
		}
	}

	vao.setDataf(ShaderAttribute::Position, &positions[0], positions.size(), 3);
}
*/


} // End namespace Verso

