#include <Verso/Render/VaoGenerator/VaoGenerator.hpp>

namespace Verso {


namespace Private {


void gridPlane3dHelper(Vao& vao, const Vector2f& unitSize, const Vector2i& gridSize, const Vector2f& uvScale, const Vector3f& offsetPosition, BufferTypes buffersToGenerate, const RgbaColorf& color)
{
	// Position
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Position)) {
		std::vector<GLfloat> positions;
		positions.reserve(gridSize.x * gridSize.y * 4 * 3);

		// See ascii grid below for detail on the generation

		Vector3f pos;
		Vector3f gridOffset(offsetPosition);
		gridOffset.x -= (gridSize.x / 2.0f) * unitSize.x;
		gridOffset.z -= (gridSize.y / 2.0f) * unitSize.y;

		for (int y=0; y<gridSize.y+1; ++y) {
			for (int x=0; x<gridSize.x+1; ++x) {
				pos.set(gridOffset);
				pos.x += x * unitSize.x;
				pos.z += y * unitSize.y;

				positions.push_back(pos.x);
				positions.push_back(pos.y);
				positions.push_back(pos.z);
			}
		}

		vao.setDataf(ShaderAttribute::Position, positions.data(), (gridSize.x+1)*(gridSize.y+1), 3);
	}

	// Uv
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Uv)) {
		std::vector<GLfloat> uvs;
		uvs.reserve(gridSize.x * gridSize.y * 4 * 2);

		for (int y=0; y<gridSize.y+1; ++y) {
			for (int x=0; x<gridSize.x+1; ++x) {
				uvs.push_back(static_cast<float>(x) / static_cast<float>(gridSize.x) * uvScale.x);
				uvs.push_back(static_cast<float>(y) / static_cast<float>(gridSize.y) * uvScale.y);
			}
		}

		vao.setDataf(ShaderAttribute::Uv, uvs.data(), (gridSize.x+1)*(gridSize.y+1), 2);
	}

	// Normal
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Normal)) {
		const GLfloat normalDir = 1.0f;
		std::vector<GLfloat> normals;
		normals.reserve(gridSize.x * gridSize.y * 4 * 3);

		for (int y=0; y<gridSize.y+1; ++y) {
			for (int x=0; x<gridSize.x+1; ++x) {
				normals.push_back(0.0f); // same for each corner
				normals.push_back(0.0f);
				normals.push_back(normalDir);
			}
		}
		vao.setDataf(ShaderAttribute::Normal, normals.data(), (gridSize.x+1)*(gridSize.y+1), 3);
	}

	// Color
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::Color)) {
		std::vector<GLfloat> colors;
		colors.reserve(gridSize.x * gridSize.y * 4 * 3);

		for (int y=0; y<gridSize.y+1; ++y) {
			for (int x=0; x<gridSize.x+1; ++x) {
				colors.push_back(color.r); // same for each corner
				colors.push_back(color.g);
				colors.push_back(color.b);
			}
		}

		vao.setDataf(ShaderAttribute::Color, colors.data(), (gridSize.x+1)*(gridSize.y+1), 3);
	}

	// UvHeightmap
	if (buffersToGenerate & static_cast<BufferTypes>(BufferType::UvHeightmap)) {
		std::vector<GLfloat> uvsHeightmap;
		uvsHeightmap.reserve(gridSize.x * gridSize.y * 4 * 2);

		for (int y=0; y<gridSize.y+1; ++y) {
			for (int x=0; x<gridSize.x+1; ++x) {
				uvsHeightmap.push_back(static_cast<float>(x) / static_cast<float>(gridSize.x));
				uvsHeightmap.push_back(static_cast<float>(y) / static_cast<float>(gridSize.y));
			}
		}

		vao.setDataf(ShaderAttribute::UvHeightmap, uvsHeightmap.data(), (gridSize.x+1)*(gridSize.y+1), 2);
	}
}


} // End namespace Private


void VaoGenerator::gridPlane3d(Vao& vao, const Vector2f& unitSize, const Vector2i& gridSize, const Vector2f& uvScale, const Vector3f& offsetPosition, BufferTypes buffersToGenerate, const RgbaColorf& color)
{
	VERSO_ASSERT("verso-3d", buffersToGenerate != 0 && "You must defined at least one BufferType to generate!");

	vao.create(PrimitiveType::Triangles);

	Private::gridPlane3dHelper(vao, unitSize, gridSize, uvScale, offsetPosition, buffersToGenerate, color);

	// Generate grid like
	//
	//     0------1------2------3-----(w)
	//     |    / |    / |    / |    / |
	//     |  /   |  /   |  /   |  /   |
	//   (w+1)--(w+2)--(w+3)--(w+4)--(w+1+w)
	//     |    / |    / |    / |    / |
	//     |  /   |  /   |  /   |  /   |
	//     +------+------+------+------+
	//     |    / |    / |    / |    / |
	//     |  /   |  /   |  /   |  /   |
	//  (w*h+1)---+------+------+----((w+1)*(h+1)-1)
	//

	// Indices
	std::vector<GLuint> indices;
	indices.reserve(gridSize.x * gridSize.y * 2 * 3);
	GLuint baseIndex = 0;
	GLuint lineBelow = gridSize.x+1;
	for (int y=0; y<gridSize.y; ++y) {
		for (int x=0; x<gridSize.x; ++x) {
			indices.push_back(baseIndex + 1); // First Triangle
			indices.push_back(baseIndex + 1 + lineBelow);
			indices.push_back(baseIndex + 1 + lineBelow - 1);

			indices.push_back(baseIndex + lineBelow); // Second Triangle
			indices.push_back(baseIndex + 0);
			indices.push_back(baseIndex + 1);
			baseIndex++;
		}
		baseIndex++;
	}
	vao.setIndices(indices.data(), gridSize.x*gridSize.y*6);
}


void VaoGenerator::gridPlane3dLines(Vao& vao, const Vector2f& unitSize, const Vector2i& gridSize, const Vector2f& uvScale, const Vector3f& offsetPosition, BufferTypes buffersToGenerate, const RgbaColorf& color)
{
	VERSO_ASSERT("verso-3d", buffersToGenerate != 0 && "You must defined at least one BufferType to generate!");

	vao.create(PrimitiveType::Lines);

	Private::gridPlane3dHelper(vao, unitSize, gridSize, uvScale, offsetPosition, buffersToGenerate, color);

	// Generate grid plane
	//
	//     0------1------2------3-----(w)
	//     |      |      |      |      |
	//     |      |      |      |      |
	//   (w+1)--(w+2)--(w+3)--(w+4)--(w+1+w)
	//     |      |      |      |      |
	//     |      |      |      |      |
	//     +------+------+------+------+
	//     |      |      |      |      |
	//     |      |      |      |      |
	//  (w*h+1)---+------+------+----((w+1)*(h+1)-1)
	//

	// Indices
	std::vector<GLuint> indices;
	indices.reserve(gridSize.x * gridSize.y * 2 * 3);
	GLuint baseIndex = 0;
	GLuint lineBelow = gridSize.x+1;
	for (int y=0; y<gridSize.y; ++y) {
		for (int x=0; x<gridSize.x; ++x) {
			indices.push_back(baseIndex + 0);
			indices.push_back(baseIndex + 1);
			indices.push_back(baseIndex + 0);
			indices.push_back(baseIndex + lineBelow);
			baseIndex++;
		}
		baseIndex++;
	}

	baseIndex = gridSize.x;
	for (int y=0; y<gridSize.y; ++y) {
		indices.push_back(baseIndex + 0);
		indices.push_back(baseIndex + lineBelow);
		baseIndex += lineBelow;
	}

	baseIndex = gridSize.y * lineBelow;
	for (int x=0; x<gridSize.x; ++x) {
		indices.push_back(baseIndex + 0);
		indices.push_back(baseIndex + 1);
		baseIndex++;
	}

	vao.setIndices(indices.data(), gridSize.x*gridSize.y*4 + gridSize.x*2 + gridSize.y*2);
}


} // End namespace Verso

