#include <Verso/Display/ContextSettings.hpp>
#include <SDL2/SDL.h>

namespace Verso {


ContextSettings::ContextSettings(
		int majorVersion, int minorVersion, MultisampleAntialiasLevel antialiasLevel,
		int depthBits, int stencilBits, bool shareWithCurrentContext) :
	majorVersion(majorVersion),
	minorVersion(minorVersion),
	antialiasLevel(antialiasLevel),
	depthBits(depthBits),
	stencilBits(stencilBits),
	shareWithCurrentContext(shareWithCurrentContext),
	glslVersionString()
{
}


void ContextSettings::applySettings()
{
#if defined (__APPLE_CC__)
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG); // Always required on OS X
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	glslVersionString = openglVersionToGlslString(3, 2);
#else
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, majorVersion);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, minorVersion);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	glslVersionString = openglVersionToGlslString(majorVersion, minorVersion);
#endif

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, true);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, depthBits);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, stencilBits);

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);

	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, antialiasLevel.ratio == 0 ? 0 : 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, static_cast<int>(antialiasLevel.ratio));

	//SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);

	SDL_GL_SetAttribute(SDL_GL_SHARE_WITH_CURRENT_CONTEXT, shareWithCurrentContext);
}


void ContextSettings::retrieveSettings()
{
	SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &majorVersion);
	SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &minorVersion);
	glslVersionString = openglVersionToGlslString(majorVersion, minorVersion);

	int msaaBuffers = 0;
	int msaaSamples = 0;
	SDL_GL_GetAttribute(SDL_GL_MULTISAMPLEBUFFERS, &msaaBuffers);
	SDL_GL_GetAttribute(SDL_GL_MULTISAMPLESAMPLES, &msaaSamples);
	if (msaaBuffers == 0) {
		antialiasLevel = MultisampleAntialiasLevel::MsaaOff();
	}
	else {
		antialiasLevel = MultisampleAntialiasLevel(static_cast<uint8_t>(msaaSamples));
	}

	SDL_GL_GetAttribute(SDL_GL_DEPTH_SIZE, &depthBits);
	SDL_GL_GetAttribute(SDL_GL_STENCIL_SIZE, &stencilBits);

	int intShareWithCurrentContext;
	SDL_GL_GetAttribute(SDL_GL_SHARE_WITH_CURRENT_CONTEXT, &intShareWithCurrentContext);
	if (intShareWithCurrentContext == 0) {
		shareWithCurrentContext = false;
	}
	else {
		shareWithCurrentContext = true;
	}
}


UString ContextSettings::openglVersionToGlslVersion(int majorVersion, int minorVersion)
{
	if (majorVersion == 2 && minorVersion == 0) {
		return "1.10.59"; // 30 April 2004
	}
	else if (majorVersion == 2 && minorVersion == 1) {
		return "1.20.8"; // 7 September 2006
	}
	else if (majorVersion == 3 && minorVersion == 0) {
		return "1.30.10"; // 22 November 2009
	}
	else if (majorVersion == 3 && minorVersion == 1) {
		return "1.40.08"; // 22 November 2009
	}
	else if (majorVersion == 3 && minorVersion == 2) {
		return "1.50.11"; // 4 December 2009
	}
	else if (majorVersion == 3 && minorVersion == 3) {
		return "3.30.6"; // 11 March 2010
	}
	else if (majorVersion == 4 && minorVersion == 0) {
		return "4.00.9"; // 24 July 2010
	}
	else if (majorVersion == 4 && minorVersion == 1) {
		return "4.10.6"; // 24 July 2010
	}
	else if (majorVersion == 4 && minorVersion == 2) {
		return "4.20.11"; // 12 December 2011
	}
	else if (majorVersion == 4 && minorVersion == 3) {
		return "4.30.8"; // 7 February 2013
	}
	else if (majorVersion == 4 && minorVersion == 4) {
		return "4.40.9"; // 16 June 2014
	}
	else if (majorVersion == 4 && minorVersion == 5) {
		return "4.50.7"; // 9 May 2017
	}
	else if (majorVersion == 4 && minorVersion == 6) {
		return "4.60.5"; // 14 June 2018
	}
	else {
		UString resources;
		resources.append2(majorVersion);
		resources += ".";
		resources.append2(minorVersion);
		VERSO_OBJECTNOTFOUND("verso-3d", "Unknown OpenGL version", resources.c_str());
	}
}


UString ContextSettings::openglVersionToGlslString(int majorVersion, int minorVersion)
{
	if (majorVersion == 2 && minorVersion == 0) {
		return "#version 110"; // 30 April 2004
	}
	else if (majorVersion == 2 && minorVersion == 1) {
		return "#version 120"; // 7 September 2006
	}
	else if (majorVersion == 3 && minorVersion == 0) {
		return "#version 130"; // 22 November 2009
	}
	else if (majorVersion == 3 && minorVersion == 1) {
		return "#version 140"; // 22 November 2009
	}
	else if (majorVersion == 3 && minorVersion == 2) {
		return "#version 150"; // 4 December 2009
	}
	else if (majorVersion == 3 && minorVersion == 3) {
		return "#version 330"; // 11 March 2010
	}
	else if (majorVersion == 4 && minorVersion == 0) {
		return "#version 400"; // 24 July 2010
	}
	else if (majorVersion == 4 && minorVersion == 1) {
		return "#version 410"; // 24 July 2010
	}
	else if (majorVersion == 4 && minorVersion == 2) {
		return "#version 420"; // 12 December 2011
	}
	else if (majorVersion == 4 && minorVersion == 3) {
		return "#version 430"; // 7 February 2013
	}
	else if (majorVersion == 4 && minorVersion == 4) {
		return "#version 440"; // 16 June 2014
	}
	else if (majorVersion == 4 && minorVersion == 5) {
		return "#version 450"; // 9 May 2017
	}
	else if (majorVersion == 4 && minorVersion == 6) {
		return "#version 460"; // 14 June 2018
	}
	else {
		UString resources;
		resources.append2(majorVersion);
		resources += ".";
		resources.append2(minorVersion);
		VERSO_OBJECTNOTFOUND("verso-3d", "Unknown OpenGL version", resources.c_str());
	}
}


UString ContextSettings::toString() const
{
	UString str("Context ");
	str.append2(majorVersion);
	str += ".";
	str.append2(minorVersion);
	str += ", ";
	str += antialiasLevel.toString();
	str += " MSAA";
	return str;
}


UString ContextSettings::toStringDebug() const
{
	UString str("ContextSettings(");
	str += toString();
	str += ", ";
	str.append2(depthBits);
	str += " bpp depth, ";
	str.append2(stencilBits);
	str += " bpp stencil";
	if (shareWithCurrentContext == true) {
		str += ", shared with other context";
	}
	str += ")";
	return str;
}


} // End namespace Verso


