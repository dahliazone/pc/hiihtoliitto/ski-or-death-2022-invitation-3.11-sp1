#include <Verso/Display/WindowOpengl.hpp>
#include <Verso/System/InitWrapperSdl2.hpp>
#include <Verso/System/Logger.hpp>
#include <algorithm>

namespace Verso {


std::vector<WindowOpengl*> WindowOpengl::windowsStillOpen = std::vector<WindowOpengl*>();


SDL_SystemCursor systemCursorTypeToSdlSystemCursorType(SystemCursorType systemCursorType)
{
	switch (systemCursorType)
	{
	case SystemCursorType::Arrow:
		return SDL_SYSTEM_CURSOR_ARROW;
	case SystemCursorType::IBeam:
		return SDL_SYSTEM_CURSOR_IBEAM;
	case SystemCursorType::Wait:
		return SDL_SYSTEM_CURSOR_WAIT;
	case SystemCursorType::Crosshair:
		return SDL_SYSTEM_CURSOR_CROSSHAIR;
	case SystemCursorType::WaitArrow:
		return SDL_SYSTEM_CURSOR_WAITARROW;
	case SystemCursorType::SizeNwSe:
		return SDL_SYSTEM_CURSOR_SIZENWSE;
	case SystemCursorType::SizeNeSw:
		return SDL_SYSTEM_CURSOR_SIZENESW;
	case SystemCursorType::SizeWe:
		return SDL_SYSTEM_CURSOR_SIZEWE;
	case SystemCursorType::SizeNs:
		return SDL_SYSTEM_CURSOR_SIZENS;
	case SystemCursorType::SizeAll:
		return SDL_SYSTEM_CURSOR_SIZEALL;
	case SystemCursorType::No:
		return SDL_SYSTEM_CURSOR_NO;
	case SystemCursorType::Hand:
		return SDL_SYSTEM_CURSOR_HAND;
	case SystemCursorType::Unset:
		break;
	case SystemCursorType::EnumSize:
		break;
	}
	return SDL_NUM_SYSTEM_CURSORS;
}


WindowOpengl::WindowOpengl(const UString& id) :
	created(false),
	id(id),
	displayMode(),
	display(),
	title(""),
	settings(),
	context(),
	sdlWindow(nullptr),
	sdlSystemCursors(),
	renderDownscaleRatio(),
	renderResolution(),
	renderDisplayAspectRatio(),
	forcedRenderDisplayAspectRatio(false),
	renderPixelAspectRatio(),
	overrideRecordRenderResolution(),
	//iconFileName(),
	relativeViewport(0.0f, 0.0f, 1.0f, 1.0f),
	resourceManager(),
	maxTextureResolution(0),
	maxCombinedTextureImageUnits(0),
	maxVertexTextureImageUnits(0),
	maxGeometryTextureImageUnits(0),
	lastBoundTextureHandlesInTextureUnits(),
	activeTextureUnit(0),
	currentlyBoundReadFbo(nullptr),
	currentlyBoundDrawFbo(nullptr),
	cameraInputControllerId(),
	cameraInputController(nullptr),
	cameraForward(0),
	cameraBackward(0),
	cameraLeft(0),
	cameraRight(0),
	cameraUp(0),
	cameraDown(0),
	cameraForwardFast(0),
	cameraBackwardFast(0),
	cameraLeftFast(0),
	cameraRightFast(0),
	cameraUpFast(0),
	cameraDownFast(0),
	cameraForwardReallyFast(0),
	cameraBackwardReallyFast(0),
	cameraLeftReallyFast(0),
	cameraRightReallyFast(0),
	cameraUpReallyFast(0),
	cameraDownReallyFast(0),
	cameraActionA(0),
	cameraActionB(0),
	latestClipboardText()
{
	sdlSystemCursors.fill(nullptr);
}


WindowOpengl::WindowOpengl(
		const UString& id, const DisplayMode& displayMode, const Display& display, const UString& title,
		WindowStyle style,
		const RenderDownscaleRatio& renderDownscaleRatio,
		const AspectRatio& renderDisplayAspectRatio,
		const PixelAspectRatio& renderPixelAspectRatio,
		const ContextSettings& settings,
		const SwapInterval& swapInterval,
		const Vector2i& overrideRecordRenderResolution) :
	created(false),
	id(id),
	displayMode(),
	display(),
	title(""),
	settings(),
	context(),
	sdlWindow(nullptr),
	sdlSystemCursors(),
	renderDownscaleRatio(),
	renderResolution(),
	renderDisplayAspectRatio(),
	forcedRenderDisplayAspectRatio(false),
	renderPixelAspectRatio(renderPixelAspectRatio),
	overrideRecordRenderResolution(),
	//iconFileName(),
	relativeViewport(0.0f, 0.0f, 1.0f, 1.0f),
	resourceManager(),
	maxTextureResolution(0),
	maxCombinedTextureImageUnits(0),
	maxVertexTextureImageUnits(0),
	maxGeometryTextureImageUnits(0),
	lastBoundTextureHandlesInTextureUnits(),
	activeTextureUnit(0),
	currentlyBoundReadFbo(nullptr),
	currentlyBoundDrawFbo(nullptr),
	cameraInputControllerId(),
	cameraInputController(nullptr),
	cameraForward(0),
	cameraBackward(0),
	cameraLeft(0),
	cameraRight(0),
	cameraUp(0),
	cameraDown(0),
	cameraForwardFast(0),
	cameraBackwardFast(0),
	cameraLeftFast(0),
	cameraRightFast(0),
	cameraUpFast(0),
	cameraDownFast(0),
	cameraForwardReallyFast(0),
	cameraBackwardReallyFast(0),
	cameraLeftReallyFast(0),
	cameraRightReallyFast(0),
	cameraUpReallyFast(0),
	cameraDownReallyFast(0),
	cameraActionA(0),
	cameraActionB(0),
	latestClipboardText()
{
	sdlSystemCursors.fill(nullptr);
	create(displayMode, display, title, static_cast<WindowStyles>(style),
		   renderDownscaleRatio, renderDisplayAspectRatio, renderPixelAspectRatio,
		   settings, swapInterval,
		   overrideRecordRenderResolution);
}


WindowOpengl::WindowOpengl(
		const UString& id, const DisplayMode& displayMode, const Display& display, const UString& title,
		WindowStyles styles,
		const RenderDownscaleRatio& renderDownscaleRatio,
		const AspectRatio& renderDisplayAspectRatio,
		const PixelAspectRatio& renderPixelAspectRatio,
		const ContextSettings& settings,
		const SwapInterval& swapInterval,
		const Vector2i& overrideRecordRenderResolution) :
	created(false),
	id(id),
	displayMode(),
	display(),
	title(""),
	settings(),
	context(),
	sdlWindow(nullptr),
	sdlSystemCursors(),
	renderDownscaleRatio(),
	renderResolution(),
	renderDisplayAspectRatio(),
	forcedRenderDisplayAspectRatio(false),
	renderPixelAspectRatio(renderPixelAspectRatio),
	overrideRecordRenderResolution(),
	//iconFileName(),
	relativeViewport(0.0f, 0.0f, 1.0f, 1.0f),
	resourceManager(),
	maxTextureResolution(0),
	maxCombinedTextureImageUnits(0),
	maxVertexTextureImageUnits(0),
	maxGeometryTextureImageUnits(0),
	lastBoundTextureHandlesInTextureUnits(),
	activeTextureUnit(0),
	currentlyBoundReadFbo(nullptr),
	currentlyBoundDrawFbo(nullptr),
	cameraInputControllerId(),
	cameraInputController(nullptr),
	cameraForward(0),
	cameraBackward(0),
	cameraLeft(0),
	cameraRight(0),
	cameraUp(0),
	cameraDown(0),
	cameraForwardFast(0),
	cameraBackwardFast(0),
	cameraLeftFast(0),
	cameraRightFast(0),
	cameraUpFast(0),
	cameraDownFast(0),
	cameraForwardReallyFast(0),
	cameraBackwardReallyFast(0),
	cameraLeftReallyFast(0),
	cameraRightReallyFast(0),
	cameraUpReallyFast(0),
	cameraDownReallyFast(0),
	cameraActionA(0),
	cameraActionB(0),
	latestClipboardText()
{
	sdlSystemCursors.fill(nullptr);
	create(displayMode, display, title, styles, renderDownscaleRatio,
		   renderDisplayAspectRatio, renderPixelAspectRatio,
		   settings, swapInterval, overrideRecordRenderResolution);
}


void WindowOpengl::create(
		const DisplayMode& displayMode, const Display& display, const UString& title,
		WindowStyle style,
		const RenderDownscaleRatio& renderDownscaleRatio,
		const AspectRatio& renderDisplayAspectRatio,
		const PixelAspectRatio& renderPixelAspectRatio,
		const ContextSettings& settings,
		const SwapInterval& swapInterval,
		const Vector2i& overrideRecordRenderResolution)
{
	create(displayMode, display, title, static_cast<WindowStyles>(style),
		   renderDownscaleRatio, renderDisplayAspectRatio, renderPixelAspectRatio,
		   settings, swapInterval,
		   overrideRecordRenderResolution);
}


void WindowOpengl::create(
		const DisplayMode& displayMode, const Display& display, const UString& title,
		WindowStyles styles,
		const RenderDownscaleRatio& renderDownscaleRatio,
		const AspectRatio& renderDisplayAspectRatio,
		const PixelAspectRatio& renderPixelAspectRatio,
		const ContextSettings& requestSettings,
		const SwapInterval& swapInterval,
		const Vector2i& overrideRecordRenderResolution)
{
	VERSO_ASSERT_MSG("verso-3d", isCreated() == false, "Already created!");

	InitWrapperSdl2::instance().createSubsystem(SubsystemSdl2::Video);

	this->displayMode = displayMode;
	this->display = display;
	this->title = title;
	this->sdlWindow = nullptr;
	//this->windowId = -1;

	Uint32 flags = SDL_WINDOW_OPENGL;

	if (styles & static_cast<WindowStyles>(WindowStyle::Fullscreen)) {
		flags |= SDL_WINDOW_FULLSCREEN; // note: dangerous/buggy in Linux & OS X!
	}

	if (styles & static_cast<WindowStyles>(WindowStyle::FullscreenDesktop)) {
		flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
	}

	if (styles & static_cast<WindowStyles>(WindowStyle::Hidden)) {
		flags |=  SDL_WINDOW_HIDDEN;
	}

	if (styles & static_cast<WindowStyles>(WindowStyle::Borderless)) {
		flags |= SDL_WINDOW_BORDERLESS;
	}

	if (styles & static_cast<WindowStyles>(WindowStyle::Resizable)) {
		flags |=  SDL_WINDOW_RESIZABLE;
	}

	if (styles & static_cast<WindowStyles>(WindowStyle::Minimized)) {
		flags |=  SDL_WINDOW_MINIMIZED;
	}

	if (styles & static_cast<WindowStyles>(WindowStyle::Maximized)) {
		flags |=  SDL_WINDOW_MAXIMIZED;
	}

	if (styles & static_cast<WindowStyles>(WindowStyle::InputGrabbed)) {
		flags |=  SDL_WINDOW_INPUT_GRABBED;
	}

	if (styles & static_cast<WindowStyles>(WindowStyle::AllowHighDpi)) {
		flags |=  SDL_WINDOW_ALLOW_HIGHDPI;
	}

	this->settings = requestSettings;
	this->settings.applySettings();

	int windowX = SDL_WINDOWPOS_UNDEFINED;
	int windowY = SDL_WINDOWPOS_UNDEFINED;
	if (display.index != -1) {
		windowX = display.rect.pos.x;
		windowY = display.rect.pos.y;
	}
	if (!(styles & static_cast<WindowStyles>(WindowStyle::ForceOnTop))) {
		SDL_SetHint(SDL_HINT_ALLOW_TOPMOST, "0");
	}
	sdlWindow = SDL_CreateWindow(
				this->title.c_str(), windowX, windowY,
				displayMode.resolution.x, displayMode.resolution.y,
				flags);

	// Check that the window was successfully made
	if (sdlWindow == nullptr){
		UString error("Could not create window. Reason: ");
		error += SDL_GetError();
		VERSO_ERROR("verso-3d", error.c_str(), requestSettings.toStringDebug().c_str());
	}

	context.create(false, this->settings, this->sdlWindow);
	setSwapInterval(swapInterval);

	if (!gladLoadGL()) {
		UString error("GLAD initialization failed. Probably because your system doesn't support OpenGL >= 2.");
		VERSO_ERROR("verso-3d", error.c_str(), requestSettings.toStringDebug().c_str());
	}
	// \TODO: Add pre/post-hooks to GLAD
	GLenum glError = glGetError();
	while (glError != GL_NO_ERROR) {
		glError = glGetError();
	}

	this->settings.retrieveSettings();

	if (settings.antialiasLevel > MultisampleAntialiasLevel::MsaaOff()) {
		glEnable(GL_MULTISAMPLE);
	}

	// Retrieve OpenGL texture limits
	glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxTextureResolution);
	GL_CHECK_FOR_ERRORS("", "");

	glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &maxCombinedTextureImageUnits);
	GL_CHECK_FOR_ERRORS("", "");

	glGetIntegerv(GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS, &maxVertexTextureImageUnits);
	GL_CHECK_FOR_ERRORS("", "");

	glGetIntegerv(GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS, &maxGeometryTextureImageUnits);
	GL_CHECK_FOR_ERRORS("", "");

	created = true; // early flag set to internal methods can be used here

	// Clear texture bind state
	lastBoundTextureHandlesInTextureUnits.clear();
	lastBoundTextureHandlesInTextureUnits.assign(maxCombinedTextureImageUnits, 0);
	bindResetTextureUnits(lastBoundTextureHandlesInTextureUnits.size());

	this->renderDownscaleRatio = renderDownscaleRatio;

	// No custom renderDisplayAspectRatio given => use displayMode aspect ratio
	if (renderDisplayAspectRatio.value == 0) {
		this->renderDisplayAspectRatio = AspectRatio(getDrawableResolutioni(), ""); // \TODO: fix aspect ratio if known
		this->forcedRenderDisplayAspectRatio = false;
	}
	else {
		this->renderDisplayAspectRatio = renderDisplayAspectRatio;
		this->forcedRenderDisplayAspectRatio = true;
	}

	this->renderPixelAspectRatio = renderPixelAspectRatio;

	this->overrideRecordRenderResolution = overrideRecordRenderResolution;

	resourceManager.create(this);

	onResize();

	// Create camera input controller
	cameraInputControllerId = "verso3d-windowopengl-camera";
	cameraInputControllerId.append2(WindowOpengl::getUniqueInstanceId());
	cameraInputController = InputManager::instance().createInputController(cameraInputControllerId);
	cameraForward = cameraInputController->mapKeyboardToButton("forward", KeyCode::Up);
	cameraInputController->mapKeyboardToButton("forward", KeyCode::W);
	cameraBackward = cameraInputController->mapKeyboardToButton("backward", KeyCode::Down);
	cameraInputController->mapKeyboardToButton("backward", KeyCode::S);
	cameraLeft = cameraInputController->mapKeyboardToButton("left", KeyCode::Left);
	cameraInputController->mapKeyboardToButton("left", KeyCode::A);
	cameraRight = cameraInputController->mapKeyboardToButton("right", KeyCode::Right);
	cameraInputController->mapKeyboardToButton("right", KeyCode::D);
	cameraUp = cameraInputController->mapKeyboardToButton("up", KeyCode::PageUp);
	cameraInputController->mapKeyboardToButton("up", KeyCode::R);
	cameraDown = cameraInputController->mapKeyboardToButton("down", KeyCode::PageDown);
	cameraInputController->mapKeyboardToButton("down", KeyCode::F);

	cameraForwardFast = cameraInputController->mapKeyboardToButton("forwardFast", KeyCode::Up, static_cast<Keymodifiers>(Keymodifier::AnyCtrl));
	cameraInputController->mapKeyboardToButton("forwardFast", KeyCode::W, static_cast<Keymodifiers>(Keymodifier::AnyCtrl));
	cameraBackwardFast = cameraInputController->mapKeyboardToButton("backwardFast", KeyCode::Down, static_cast<Keymodifiers>(Keymodifier::AnyCtrl));
	cameraInputController->mapKeyboardToButton("backwardFast", KeyCode::S, static_cast<Keymodifiers>(Keymodifier::AnyCtrl));
	cameraLeftFast = cameraInputController->mapKeyboardToButton("leftFast", KeyCode::Left, static_cast<Keymodifiers>(Keymodifier::AnyCtrl));
	cameraInputController->mapKeyboardToButton("leftFast", KeyCode::A, static_cast<Keymodifiers>(Keymodifier::AnyCtrl));
	cameraRightFast = cameraInputController->mapKeyboardToButton("rightFast", KeyCode::Right, static_cast<Keymodifiers>(Keymodifier::AnyCtrl));
	cameraInputController->mapKeyboardToButton("rightFast", KeyCode::D, static_cast<Keymodifiers>(Keymodifier::AnyCtrl));
	cameraUpFast = cameraInputController->mapKeyboardToButton("upFast", KeyCode::PageUp, static_cast<Keymodifiers>(Keymodifier::AnyCtrl));
	cameraInputController->mapKeyboardToButton("upFast", KeyCode::R, static_cast<Keymodifiers>(Keymodifier::AnyCtrl));
	cameraDownFast = cameraInputController->mapKeyboardToButton("downFast", KeyCode::PageDown, static_cast<Keymodifiers>(Keymodifier::AnyCtrl));
	cameraInputController->mapKeyboardToButton("downFast", KeyCode::F, static_cast<Keymodifiers>(Keymodifier::AnyCtrl));

	cameraForwardReallyFast = cameraInputController->mapKeyboardToButton("forwardReallyFast", KeyCode::Up, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraInputController->mapKeyboardToButton("forwardReallyFast", KeyCode::W, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraBackwardReallyFast = cameraInputController->mapKeyboardToButton("backwardReallyFast", KeyCode::Down, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraInputController->mapKeyboardToButton("backwardReallyFast", KeyCode::S, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraLeftReallyFast = cameraInputController->mapKeyboardToButton("leftReallyFast", KeyCode::Left, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraInputController->mapKeyboardToButton("leftReallyFast", KeyCode::A, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraRightReallyFast = cameraInputController->mapKeyboardToButton("rightReallyFast", KeyCode::Right, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraInputController->mapKeyboardToButton("rightReallyFast", KeyCode::D, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraUpReallyFast = cameraInputController->mapKeyboardToButton("upReallyFast", KeyCode::PageUp, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraInputController->mapKeyboardToButton("upReallyFast", KeyCode::R, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraDownReallyFast = cameraInputController->mapKeyboardToButton("downReallyFast", KeyCode::PageDown, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraInputController->mapKeyboardToButton("downReallyFast", KeyCode::F, static_cast<Keymodifiers>(Keymodifier::AnyShift));

	cameraActionA = cameraInputController->mapKeyboardToButton("action-a", KeyCode::N1);
	cameraActionB = cameraInputController->mapKeyboardToButton("action-b", KeyCode::N2);

	latestClipboardText = "";

	WindowOpengl::windowsStillOpen.push_back(this);
	WindowOpengl::getUniqueInstanceId()++;
}


WindowOpengl::~WindowOpengl()
{
	destroy();
}


///////////////////////////////////////////////////////////////////////////////////////////
// interface IWindow
///////////////////////////////////////////////////////////////////////////////////////////

void WindowOpengl::destroy() VERSO_NOEXCEPT
{
	if (isCreated() == false) {
		return;
	}

	if (sdlWindow != nullptr) {
		resourceManager.destroy();

		SDL_DestroyWindow(reinterpret_cast<SDL_Window*>(this->sdlWindow));
		sdlWindow = nullptr;
		renderResolution.x = 0;
		renderResolution.y = 0;

		auto it = std::find(windowsStillOpen.begin(), windowsStillOpen.end(), this);
		if (it != windowsStillOpen.end()) {
			*it = nullptr;
			windowsStillOpen.erase(it);
		}
	}

	for (auto sdlSystemCursor : sdlSystemCursors) {
		if (sdlSystemCursor != nullptr) {
			SDL_FreeCursor(reinterpret_cast<SDL_Cursor*>(sdlSystemCursor));
		}
	}

	created = false;
}


bool WindowOpengl::isCreated() const
{
	return created;
}


void WindowOpengl::close() VERSO_NOEXCEPT
{
	destroy();
}


bool WindowOpengl::isOpen() const
{
	if (sdlWindow != nullptr) {
		return true;
	}
	else {
		return false;
	}
}


UString WindowOpengl::getId() const
{
	return id;
}


Vector2i WindowOpengl::getWindowResolutioni() const
{
	Vector2i windowResolution;
	SDL_GetWindowSize(reinterpret_cast<SDL_Window*>(this->sdlWindow),
					  &windowResolution.x, &windowResolution.y);
	return windowResolution;
}


Vector2f WindowOpengl::getWindowResolutionf() const
{
	Vector2i windowResolution;
	SDL_GetWindowSize(reinterpret_cast<SDL_Window*>(this->sdlWindow),
					  &windowResolution.x, &windowResolution.y);
	return Vector2f(
				static_cast<float>(windowResolution.x),
				static_cast<float>(windowResolution.y));
}


void WindowOpengl::setWindowResolution(const Vector2i& windowResolution)
{
	SDL_SetWindowSize(reinterpret_cast<SDL_Window*>(this->sdlWindow),
					  windowResolution.x, windowResolution.y);
}


RenderDownscaleRatio WindowOpengl::getRenderDownscaleRatio() const
{
	return renderDownscaleRatio;
}


Vector2i WindowOpengl::getRenderResolutioni() const
{
	return renderResolution;
}


Vector2f WindowOpengl::getRenderResolutionf() const
{
	return Vector2f(
				static_cast<float>(renderResolution.x),
				static_cast<float>(renderResolution.y));
}


Vector2i WindowOpengl::getDrawableResolutioni() const
{
	Vector2i s;
	SDL_GL_GetDrawableSize(reinterpret_cast<SDL_Window*>(sdlWindow), &s.x, &s.y);
	return s;
}


Vector2f WindowOpengl::getDrawableResolutionf() const
{
	Vector2i ds(getDrawableResolutioni());
	return Vector2f(
				static_cast<float>(ds.x),
				static_cast<float>(ds.y));
}


AspectRatio WindowOpengl::getRenderDisplayAspectRatio() const
{
	return renderDisplayAspectRatio;
}


void WindowOpengl::setRenderDisplayAspectRatio(const AspectRatio& renderDisplayAspectRatio)
{
	// No custom renderDisplayAspectRatio given => use displayMode aspect ratio
	if (renderDisplayAspectRatio.value == 0) {
		this->renderDisplayAspectRatio = AspectRatio(getDrawableResolutioni(), ""); // \TODO: fix aspect ratio if known
		this->forcedRenderDisplayAspectRatio = false;
	}
	else {
		this->renderDisplayAspectRatio = renderDisplayAspectRatio;
		this->forcedRenderDisplayAspectRatio = true;
	}

	// \TODO: need to resize render buffer?
	//onResize();
}


PixelAspectRatio WindowOpengl::getRenderPixelAspectRatio() const
{
	return renderPixelAspectRatio;
}


void WindowOpengl::setRenderPixelAspectRatio(const PixelAspectRatio& renderPixelAspectRatio)
{
	this->renderPixelAspectRatio = renderPixelAspectRatio;

	// \TODO: need to resize render buffer?
	//onResize();
}


void WindowOpengl::makeActive()
{
	VERSO_ASSERT("verso-3d", context.isCreated() == true && "OpenGL context must be created");
	context.makeActive();
}


void WindowOpengl::swapScreenBuffer() const
{
	if (sdlWindow != nullptr) {
		SDL_GL_SwapWindow(reinterpret_cast<SDL_Window*>(this->sdlWindow));
	}
}


const UString& WindowOpengl::getClipboardText()
{
	char* tempStr = SDL_GetClipboardText();
	latestClipboardText.set(tempStr);
	free(tempStr);
	return latestClipboardText;
}


void WindowOpengl::setClipboardText(const UString& text)
{
	SDL_SetClipboardText(text.c_str());
}


bool WindowOpengl::isCursorShown() const
{
	return SDL_ShowCursor(SDL_QUERY) != 0;
}


void WindowOpengl::setCursorShown(bool shown) const
{
	if (shown == true) {
		showCursor();
	}
	else {
		hideCursor();
	}
}


void WindowOpengl::showCursor() const
{
	SDL_ShowCursor(SDL_ENABLE);
}


void WindowOpengl::hideCursor() const
{
	SDL_ShowCursor(SDL_DISABLE);
}


bool WindowOpengl::hasInputGrabbed() const
{
	return (SDL_GetWindowFlags(reinterpret_cast<SDL_Window*>(this->sdlWindow)) & SDL_WINDOW_INPUT_GRABBED) != 0;
}


bool WindowOpengl::hasInputFocus() const
{
	return (SDL_GetWindowFlags(reinterpret_cast<SDL_Window*>(this->sdlWindow)) & SDL_WINDOW_INPUT_FOCUS) != 0;
}


bool WindowOpengl::hasMouseFocus() const
{
	return (SDL_GetWindowFlags(reinterpret_cast<SDL_Window*>(this->sdlWindow)) & SDL_WINDOW_MOUSE_FOCUS) != 0;
}


MouseState WindowOpengl::getMouseState() const
{
	MouseState state = InputManager::instance().getMouseState();
	state.windowId = SDL_GetWindowID(reinterpret_cast<SDL_Window*>(this->sdlWindow));

	// Set mouse location relative to window to -1,-1 if no mouse / on another screen, etc.
	if (!hasMouseFocus()) {
		state.x = -1;
		state.y = -1;
	}

	return state;
}


bool WindowOpengl::captureMouse(bool enabled) const
{
	return (SDL_CaptureMouse((enabled == true ? SDL_TRUE : SDL_FALSE)) == 0 ? true : false);
}


bool WindowOpengl::warpMouseGlobal(int x, int y) const
{
	return SDL_WarpMouseGlobal(x, y) == 0 ? true : false;
}


void WindowOpengl::warpMouseInWindow(int x, int y) const
{
	SDL_WarpMouseInWindow(reinterpret_cast<SDL_Window*>(sdlWindow), x, y);
}


bool WindowOpengl::createSystemCursor(SystemCursorType systemCursorType)
{
	SDL_SystemCursor sdlSystemCursorType = systemCursorTypeToSdlSystemCursorType(systemCursorType);
	if (sdlSystemCursorType == SDL_NUM_SYSTEM_CURSORS) {
		return false;
	}

	destroySystemCursor(systemCursorType);

	sdlSystemCursors[static_cast<size_t>(sdlSystemCursorType)] =
			static_cast<void*>(
				SDL_CreateSystemCursor(sdlSystemCursorType));

	return true;
}


void WindowOpengl::destroySystemCursor(SystemCursorType systemCursorType)
{
	void* sdlSystemCursor = sdlSystemCursors[static_cast<size_t>(systemCursorType)];
	if (sdlSystemCursor != nullptr) {
		SDL_FreeCursor(reinterpret_cast<SDL_Cursor*>(sdlSystemCursor));
	}
}


bool WindowOpengl::setSystemCursor(SystemCursorType systemCursorType) const
{
	void* sdlSystemCursor = sdlSystemCursors[static_cast<size_t>(systemCursorType)];
	if (sdlSystemCursor == nullptr) {
		return false;
	}

	SDL_SetCursor(reinterpret_cast<SDL_Cursor*>(sdlSystemCursor));
	return true;
}


void* WindowOpengl::getNativeWindowHandle() const
{
	return Native::getWindowHandle(reinterpret_cast<SDL_Window*>(this->sdlWindow));
}


void* WindowOpengl::getRenderer() const
{
	return nullptr;
}


void WindowOpengl::show()
{
	SDL_ShowWindow(reinterpret_cast<SDL_Window*>(this->sdlWindow));
	setWindowResolution(displayMode.resolution);

	// \TODO: need to resize render buffer?
	//onResize();
}


void WindowOpengl::hide()
{
	SDL_HideWindow(reinterpret_cast<SDL_Window*>(this->sdlWindow));
}


DisplayMode WindowOpengl::getDisplayMode() const
{
	return displayMode;
}


void WindowOpengl::setDisplayMode(const DisplayMode& displayMode)
{
	this->displayMode = displayMode;
}


Display WindowOpengl::getDisplay() const
{
	return display;
}


void WindowOpengl::setDisplay(const Display& display)
{
	this->display = display;
}

UString WindowOpengl::getTitle() const
{
	return this->title;
}


void WindowOpengl::setTitle(const UString& windowTitle)
{
	this->title = windowTitle;
	SDL_SetWindowTitle(reinterpret_cast<SDL_Window*>(this->sdlWindow), this->title.c_str());
}


Vector2i WindowOpengl::getPositioni() const
{
	Vector2i point;
	SDL_GetWindowPosition(reinterpret_cast<SDL_Window*>(this->sdlWindow), &point.x, &point.y);
	return point;
}


Vector2f WindowOpengl::getPositionf() const
{
	Vector2i pos(getPositioni());
	return Vector2f(
				static_cast<float>(pos.x),
				static_cast<float>(pos.y));
}


void WindowOpengl::setPosition(const Vector2i& location)
{
	SDL_SetWindowPosition(reinterpret_cast<SDL_Window*>(this->sdlWindow), location.x, location.y);
}


void WindowOpengl::setPositionCentered()
{
	setPosition(Align(HAlign::Center, VAlign::Center).objectInContainerOffseti(displayMode.resolution, display.rect));
}


void WindowOpengl::setPositionAlignedOnDisplay(const Display& anotherDisplay, const Align& align)
{
	setPosition(align.objectInContainerOffseti(displayMode.resolution, anotherDisplay.rect));
}


Rectf WindowOpengl::getRelativeViewport() const
{
	if (currentlyBoundDrawFbo != nullptr) {
		return currentlyBoundDrawFbo->getRelativeViewport();
	}
	else if (currentlyBoundReadFbo != nullptr) {
		return currentlyBoundReadFbo->getRelativeViewport();
	}
	else {
		return relativeViewport;
	}
}


void WindowOpengl::setRelativeViewport(const Rectf& relativeViewport)
{
	if (currentlyBoundDrawFbo != nullptr) {
		currentlyBoundDrawFbo->setRelativeViewport(relativeViewport);
	}
	else if (currentlyBoundReadFbo != nullptr) {
		currentlyBoundReadFbo->setRelativeViewport(relativeViewport);
	}
	else {
		this->relativeViewport = relativeViewport;
	}
}


void WindowOpengl::resetRelativeViewport()
{
	setRelativeViewport(Rectf(0.0f, 0.0f, 1.0f, 1.0f));
}


Recti WindowOpengl::getRenderViewporti() const
{
	Rectf relative = getRelativeViewport();
	Vector2i resolution(getRenderResolutioni());
	return Recti(static_cast<int>(relative.pos.x * resolution.x),
				 static_cast<int>(relative.pos.y * resolution.y),
				 static_cast<int>(relative.size.x * resolution.x),
				 static_cast<int>(relative.size.y * resolution.y));
}


Rectf WindowOpengl::getRenderViewportf() const
{
	Recti vp = getRenderViewporti();
	return Rectf(static_cast<float>(vp.pos.x),
				 static_cast<float>(vp.pos.y),
				 static_cast<float>(vp.size.x),
				 static_cast<float>(vp.size.y));
}


void WindowOpengl::applyRenderViewport() const
{
	Vector2i resolution = getRenderResolutioni();
	glViewport(static_cast<GLint>(relativeViewport.pos.x * resolution.x),
			   static_cast<GLint>(relativeViewport.pos.y * resolution.y),
			   static_cast<GLsizei>(relativeViewport.size.x * resolution.x),
			   static_cast<GLsizei>(relativeViewport.size.y * resolution.y));
}


Recti WindowOpengl::getDrawableViewporti() const
{
	Rectf relative = getRelativeViewport();
	Vector2i resolution(getDrawableResolutioni());
	return Recti(static_cast<int>(relative.pos.x * resolution.x),
				 static_cast<int>(relative.pos.y * resolution.y),
				 static_cast<int>(relative.size.x * resolution.x),
				 static_cast<int>(relative.size.y * resolution.y));
}


Rectf WindowOpengl::getDrawableViewportf() const
{
	Recti vp = getRenderViewporti();
	return Rectf(static_cast<float>(vp.pos.x),
				 static_cast<float>(vp.pos.y),
				 static_cast<float>(vp.size.x),
				 static_cast<float>(vp.size.y));
}


void WindowOpengl::applyDrawableViewport() const
{
	Vector2i resolution = getDrawableResolutioni();
	glViewport(static_cast<GLint>(relativeViewport.pos.x * resolution.x),
			   static_cast<GLint>(relativeViewport.pos.y * resolution.y),
			   static_cast<GLsizei>(relativeViewport.size.x * resolution.x),
			   static_cast<GLsizei>(relativeViewport.size.y * resolution.y));
}


void WindowOpengl::loadIcon(const UString& iconFileName)
{
	(void)iconFileName;
	// Load icon
	/*if (iconFileName.compare("") != 0) {
		SDL_Surface* icon = SDL_LoadBMP(iconFileName.c_str());
		SDL_SetColorKey(icon, SDL_SRCCOLORKEY, SDL_MapRGB(icon->format, 0, 0, 0));
		SDL_WM_SetIcon(icon, nullptr);
	}
	this->iconFileName = iconFilename;*/
	VERSO_FAIL("verso-3d"); // \TODO: implement
}


bool WindowOpengl::pollEvent(Event& event)
{
	/*if (m_impl && m_impl->popEvent(event, false)) { // \TODO: implement
		return filterEvent(event);
	}
	else {
		return false;
	}*/
	bool result = InputManager::instance().pollEvent(event);

	//if (event.type == EventType::GainedFocus)
	//else if (event.type == EventType::LostFocus)

	return result;
}


bool WindowOpengl::waitEvent(Event& event)
{
	(void)event;
	VERSO_FAIL("verso-3d");// \TODO: Implement WindowOpengl::waitEvent(Event& event)
	/*if (m_impl && m_impl->popEvent(event, true)) { // \TODO: implement
		return filterEvent(event);
	}
	else {
		return false;
	}*/
}


InputController* WindowOpengl::getCameraInputController() const
{
	return cameraInputController;
}


ButtonHandle WindowOpengl::getCameraForward() const
{
	return cameraForward;
}


ButtonHandle WindowOpengl::getCameraBackward() const
{
	return cameraBackward;
}


ButtonHandle WindowOpengl::getCameraLeft() const
{
	return cameraLeft;
}


ButtonHandle WindowOpengl::getCameraRight() const
{
	return cameraRight;
}


ButtonHandle WindowOpengl::getCameraUp() const
{
	return cameraUp;
}


ButtonHandle WindowOpengl::getCameraDown() const
{
	return cameraDown;
}


ButtonHandle WindowOpengl::getCameraForwardFast() const
{
	return cameraForwardFast;
}


ButtonHandle WindowOpengl::getCameraBackwardFast() const
{
	return cameraBackwardFast;
}


ButtonHandle WindowOpengl::getCameraLeftFast() const
{
	return cameraLeftFast;
}


ButtonHandle WindowOpengl::getCameraRightFast() const
{
	return cameraRightFast;
}


ButtonHandle WindowOpengl::getCameraUpFast() const
{
	return cameraUpFast;
}


ButtonHandle WindowOpengl::getCameraDownFast() const
{
	return cameraDownFast;
}


ButtonHandle WindowOpengl::getCameraForwardReallyFast() const
{
	return cameraForwardReallyFast;
}


ButtonHandle WindowOpengl::getCameraBackwardReallyFast() const
{
	return cameraBackwardReallyFast;
}


ButtonHandle WindowOpengl::getCameraLeftReallyFast() const
{
	return cameraLeftReallyFast;
}


ButtonHandle WindowOpengl::getCameraRightReallyFast() const
{
	return cameraRightReallyFast;
}


ButtonHandle WindowOpengl::getCameraUpReallyFast() const
{
	return cameraUpReallyFast;
}


ButtonHandle WindowOpengl::getCameraDownReallyFast() const
{
	return cameraDownReallyFast;
}


ButtonHandle WindowOpengl::getCameraActionA() const
{
	return cameraActionA;
}


ButtonHandle WindowOpengl::getCameraActionB() const
{
	return cameraActionB;
}


void WindowOpengl::onResize()
{
	displayMode.setResolution(getWindowResolutioni());
	updateRenderResolution();

	resourceManager.onResize();
}


UString WindowOpengl::toString() const
{
	return "TODO";
}


UString WindowOpengl::toStringDebug() const
{
	UString str("WindowOpengl(");
	str += toString();
	str += ")";
	return str;
}


///////////////////////////////////////////////////////////////////////////////////////////
// interface IWindowOpengl
///////////////////////////////////////////////////////////////////////////////////////////

ContextSettings WindowOpengl::getSettings() const
{
	return settings;
}


void WindowOpengl::setSettings(const ContextSettings& settings)
{
	this->settings = settings;
}


SwapInterval WindowOpengl::getSwapInterval() const
{
	int swap = SDL_GL_GetSwapInterval();
	if (swap == static_cast<int>(SwapInterval::Immediate)) {
		return SwapInterval::Immediate;
	}
	else if (swap == static_cast<int>(SwapInterval::Vsync)) {
		return SwapInterval::Vsync;
	}
	else if (swap == static_cast<int>(SwapInterval::AdaptiveVsync)) {
		return SwapInterval::AdaptiveVsync;
	}
	return SwapInterval::Unset;
}


void WindowOpengl::setSwapInterval(const SwapInterval& swapInterval)
{
	if (swapInterval == SwapInterval::AdaptiveVsync) {
		// Try to set adaptive vsync and fallback to vsync on failure
		if (SDL_GL_SetSwapInterval(static_cast<int>(swapInterval)) == -1) {
			SDL_GL_SetSwapInterval(static_cast<int>(SwapInterval::Vsync));
		}
	}
	else {
		SDL_GL_SetSwapInterval(static_cast<int>(swapInterval));
	}
}


Fbo* WindowOpengl::getCurrentlyBoundReadFbo()
{
	return currentlyBoundReadFbo;
}


Fbo* WindowOpengl::getCurrentlyBoundDrawFbo()
{
	return currentlyBoundDrawFbo;
}


void WindowOpengl::bindReadFbo(Fbo* fbo)
{
	VERSO_ASSERT("verso-3d", fbo != nullptr);
	currentlyBoundReadFbo = fbo;
}


void WindowOpengl::unbindReadFbo(Fbo* fbo)
{
	if (currentlyBoundReadFbo != fbo) {
		UString message("Tried to unbind wrong read ");
		message += fbo->toStringDebug("");
		message += ", currently bound read ";
		message += fbo->toStringDebug("");
		VERSO_ILLEGALSTATE("verso-3d", message.c_str(), getId().c_str());
	}
	else {
		currentlyBoundReadFbo = nullptr;
	}
}


void WindowOpengl::bindDrawFbo(Fbo* fbo)
{
	VERSO_ASSERT("verso-3d", fbo != nullptr);
	currentlyBoundDrawFbo = fbo;
}


void WindowOpengl::unbindDrawFbo(Fbo* fbo)
{
	if (currentlyBoundDrawFbo != fbo) {
		VERSO_LOG_WARN("verso-3d", "Tried to unbind wrong write FBO.");
	}
	else {
		currentlyBoundDrawFbo = nullptr;
	}
}


ResourceManager& WindowOpengl::getResourceManager()
{
	return resourceManager;
}


bool WindowOpengl::isActiveTextureUnit(unsigned int index) const
{
	VERSO_ASSERT("verso-3d", isCreated() == true);
	return index == activeTextureUnit;
}


unsigned int WindowOpengl::getActiveTextureUnit() const
{
	VERSO_ASSERT("verso-3d", isCreated() == true);
	return activeTextureUnit;
}


void WindowOpengl::setActiveTextureUnit(unsigned int index)
{
	VERSO_ASSERT("verso-3d", isCreated() == true);
	VERSO_ASSERT("verso-3d", index < static_cast<size_t>(maxCombinedTextureImageUnits));
	glActiveTexture(static_cast<GLenum>(GL_TEXTURE0 + index));
	GL_CHECK_FOR_ERRORS("", "");
	activeTextureUnit = index;
}


void WindowOpengl::bindTextureHandle(unsigned int textureHandle)
{
	VERSO_ASSERT("verso-3d", isCreated() == true);
	if (textureHandle == lastBoundTextureHandlesInTextureUnits[activeTextureUnit]) {
		return;
	}

	glBindTexture(GL_TEXTURE_2D, textureHandle);
	GL_CHECK_FOR_ERRORS("", "");
	lastBoundTextureHandlesInTextureUnits[activeTextureUnit] = textureHandle;
}


void WindowOpengl::bindResetTextureUnits(size_t count)
{
	VERSO_ASSERT("verso-3d", isCreated() == true);
	VERSO_ASSERT("verso-3d", static_cast<int>(count) <= maxCombinedTextureImageUnits);
	for (int i = 0; i < static_cast<int>(count); i++) {
		setActiveTextureUnit(i);
		bindTextureHandle(0);
	}
	setActiveTextureUnit(0);
}


Vector2i WindowOpengl::getMaxTextureResolution() const
{
	VERSO_ASSERT("verso-3d", isCreated() == true);
	return Vector2i(maxTextureResolution, maxTextureResolution);
}


int WindowOpengl::getMaxCombinedTextureImageUnits() const
{
	VERSO_ASSERT("verso-3d", isCreated() == true);
	return maxCombinedTextureImageUnits;
}


int WindowOpengl::getMaxVertexTextureImageUnits() const
{
	VERSO_ASSERT("verso-3d", isCreated() == true);
	return maxVertexTextureImageUnits;
}


int WindowOpengl::getMaxGeometryTextureImageUnits() const
{
	VERSO_ASSERT("verso-3d", isCreated() == true);
	return maxGeometryTextureImageUnits;
}


///////////////////////////////////////////////////////////////////////////////////////////
// static (public)
///////////////////////////////////////////////////////////////////////////////////////////

void WindowOpengl::closeAll()
{
	while (windowsStillOpen.size() > 0) {
		if (windowsStillOpen[0] != nullptr) {
			windowsStillOpen[0]->close();
		}
	}
}


///////////////////////////////////////////////////////////////////////////////////////////
// static (protected)
///////////////////////////////////////////////////////////////////////////////////////////

int& WindowOpengl::getUniqueInstanceId()
{
	static int uniqueInstanceId = 0;
	return uniqueInstanceId;
}


std::vector<WindowOpengl*>& WindowOpengl::getWindowsStillOpen()
{
	return windowsStillOpen;
}


///////////////////////////////////////////////////////////////////////////////////////////
// protected
///////////////////////////////////////////////////////////////////////////////////////////

void WindowOpengl::onCreate()
{
}


///////////////////////////////////////////////////////////////////////////////////////////
// private
///////////////////////////////////////////////////////////////////////////////////////////

void WindowOpengl::updateRenderResolution()
{
	Vector2i drawableResolution = getDrawableResolutioni();
	if (overrideRecordRenderResolution.x > 0 && overrideRecordRenderResolution.y > 0) {
		drawableResolution = overrideRecordRenderResolution;
	}

	if (this->forcedRenderDisplayAspectRatio == true) {
		if (AspectRatio(drawableResolution, "").value > renderDisplayAspectRatio.value) {
			this->renderResolution.y = drawableResolution.y;
			this->renderResolution.x = static_cast<int>(static_cast<float>(drawableResolution.y) * renderDisplayAspectRatio.value);
		}
		else if (AspectRatio(drawableResolution, "").value < renderDisplayAspectRatio.value) {
			this->renderResolution.x = drawableResolution.x;
			this->renderResolution.y = static_cast<int>(static_cast<float>(drawableResolution.x) / renderDisplayAspectRatio.value);
		}
		else {
			this->renderResolution.x = drawableResolution.x;
			this->renderResolution.y = static_cast<int>(static_cast<float>(drawableResolution.x) / renderDisplayAspectRatio.value);
		}
	}
	else {
		this->renderResolution = drawableResolution;
		this->renderDisplayAspectRatio = AspectRatio(this->renderResolution, ""); // \TODO: fix aspect ratio if known
	}

	this->renderResolution = this->renderPixelAspectRatio.fitToResolution(this->renderResolution);

	// Downscale renderResolution if requested
	if (overrideRecordRenderResolution.x == 0 && overrideRecordRenderResolution.y == 0) {
		if (renderDownscaleRatio != RenderDownscaleRatio::Disabled()) {
			this->renderResolution /= renderDownscaleRatio.ratio;
		}
	}
}


bool WindowOpengl::filterEvent(const Event& event)
{
	(void)event;
	/*if (event.type == Event::Resized) { // \TODO: implement
		this->size = event.size;

		onResize();
	}*/

	return true;
}


} // End namespace Verso

