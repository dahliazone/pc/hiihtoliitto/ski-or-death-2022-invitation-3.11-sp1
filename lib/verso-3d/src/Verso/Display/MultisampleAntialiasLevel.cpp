#include <Verso/Display/MultisampleAntialiasLevel.hpp>

namespace Verso {


MultisampleAntialiasLevel::MultisampleAntialiasLevel(uint8_t ratio)
{
	set(ratio);
}


MultisampleAntialiasLevel::MultisampleAntialiasLevel(const UString& ratioStr)
{
	set(ratioStr);
}


void MultisampleAntialiasLevel::set(uint8_t ratio)
{
	if (ratio == 0) {
		this->ratio = ratio;
	}
	else if (ratio == 2) {
		this->ratio = ratio;
	}
	else if (ratio == 4) {
		this->ratio = ratio;
	}
	else if (ratio == 8) {
		this->ratio = ratio;
	}
	else if (ratio == 16) {
		this->ratio = ratio;
	}
	else {
		UString error("Unknown ratio! Valid ratios are 0x, 2x, 4x, 8x and 16x.");
		UString resources("ratio=");
		error.append2(static_cast<int>(ratio));
		VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), resources.c_str());
	}
}


void MultisampleAntialiasLevel::set(const UString& ratioStr)
{
	if (ratioStr.endsWith("x")) {
		UString valueString(ratioStr.substring(0, static_cast<int>(ratioStr.size()) - 1));
		int value = valueString.toValue<int>();
		set(static_cast<uint8_t>(value));
	}
	else {
		UString error("Ratio string \"");
		error.append2(ratioStr);
		error.append2("\" must end with letter \"x\"");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}
}


// static


const MultisampleAntialiasLevel& MultisampleAntialiasLevel::MsaaOff()
{
	static const MultisampleAntialiasLevel msaaOff(0);
	return msaaOff;
}


const MultisampleAntialiasLevel& MultisampleAntialiasLevel::Msaa2x()
{
	static const MultisampleAntialiasLevel msaa2x(2);
	return msaa2x;
}


const MultisampleAntialiasLevel& MultisampleAntialiasLevel::Msaa4x()
{
	static const MultisampleAntialiasLevel msaa4x(4);
	return msaa4x;
}


const MultisampleAntialiasLevel& MultisampleAntialiasLevel::Msaa8x()
{
	static const MultisampleAntialiasLevel msaa8x(8);
	return msaa8x;
}


const MultisampleAntialiasLevel& MultisampleAntialiasLevel::Msaa16x()
{
	static const MultisampleAntialiasLevel msaa16x(16);
	return msaa16x;
}


const std::vector<MultisampleAntialiasLevel>& MultisampleAntialiasLevel::getAntialiasLevels()
{
	static std::vector<MultisampleAntialiasLevel> levels({ MsaaOff(), Msaa2x(), Msaa4x(), Msaa8x(), Msaa16x() });
	return levels;
}


MultisampleAntialiasLevel* MultisampleAntialiasLevel::findIdenticalFromList(
		const MultisampleAntialiasLevel& selected, std::vector<MultisampleAntialiasLevel>& antialiasLevels)
{
	for (size_t i=0; i<antialiasLevels.size(); ++i) {
		if (antialiasLevels[i] == selected) {
			return &antialiasLevels[i];
		}
	}
	return nullptr;
}


// toString


UString MultisampleAntialiasLevel::toString() const
{
	UString result;
	result.append2(static_cast<unsigned int>(ratio));
	result += "x";
	return result;
}


UString MultisampleAntialiasLevel::toStringDebug() const
{
	return UString("MultisampleAntialiasLevel(") + toString() + ")";
}


} // End namespace Verso

