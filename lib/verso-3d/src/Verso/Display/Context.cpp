#include <Verso/Display/Context.hpp>
#include <Verso/Render/Opengl.hpp>
#include <Verso/System/InitWrapperSdl2.hpp>
#include <SDL2/SDL.h>

namespace Verso {


// public

Context::Context() :
	settings(),
	sdlWindow(nullptr),
	sdlGlContext(nullptr),
	windowOwned(false)
{
}


Context::Context(bool sharedWithCurrent, ContextSettings& settings, void* optionalSdlWindow) :
	sdlWindow(nullptr),
	sdlGlContext(nullptr),
	windowOwned(false)
{
	if ((*getCurrentContext()) == nullptr) {
		VERSO_ILLEGALPARAMETERS("verso-3d", "No context available to share with", "");
	}
	create(sharedWithCurrent, settings, optionalSdlWindow);
}


Context::~Context()
{
	destroy();
}


void Context::create(bool sharedWithCurrent, const ContextSettings& settings, void* optionalSdlWindow)
{
	InitWrapperSdl2::instance().createSubsystem(SubsystemSdl2::Video);

	// Check context sharing
	if (sharedWithCurrent == true) {
		if ((*getPreviousCreatedSdlGlContext()) == nullptr) {
			throw std::runtime_error("Verso::Context::Context(true): No previous context created with Verso-3d. Cannot share context.");
		}
		SDL_GL_SetAttribute(SDL_GL_SHARE_WITH_CURRENT_CONTEXT, 1);
	}
	else {
		SDL_GL_SetAttribute(SDL_GL_SHARE_WITH_CURRENT_CONTEXT, 0);
	}

	// We need a window for the context
	if (optionalSdlWindow == nullptr) {
		this->sdlWindow = SDL_CreateWindow(
							  "Verso-3d Hidden Dummy OpenGL Context Window",
							  SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1, 1,
							  SDL_WINDOW_OPENGL | SDL_WINDOW_HIDDEN | SDL_WINDOW_BORDERLESS);
		this->windowOwned = true;
	}
	else {
		this->sdlWindow = optionalSdlWindow;
		this->windowOwned = false;
	}

	// Create the context
	this->sdlGlContext = SDL_GL_CreateContext(reinterpret_cast<SDL_Window*>(this->sdlWindow));
	(*getPreviousCreatedSdlGlContext()) = this->sdlGlContext;
	if (this->sdlGlContext == nullptr) {
		throw std::runtime_error("Verso::Context::create(): Could not create OpenGL context");
	}

	// Update & save the context settings
	this->settings = settings;

	// Make it active
	makeActive();
}


void Context::destroy() VERSO_NOEXCEPT
{
	if (sdlGlContext != nullptr) {
		SDL_GL_DeleteContext(reinterpret_cast<SDL_GLContext>(sdlGlContext));
		sdlGlContext = nullptr;
	}

	if (windowOwned == true && sdlWindow != nullptr) {
		SDL_DestroyWindow(reinterpret_cast<SDL_Window*>(sdlWindow));
	}
	sdlWindow = nullptr;
	windowOwned = false;
}


bool Context::isCreated() const
{
	return (sdlWindow != nullptr) &&
			(sdlGlContext != nullptr);
}


void Context::makeActive()
{
	VERSO_ASSERT("verso-3d", sdlWindow != nullptr && "OpenGL window not created/given!");
	VERSO_ASSERT("verso-3d", sdlGlContext != nullptr && "OpenGL context not created!");

	SDL_GL_MakeCurrent(reinterpret_cast<SDL_Window*>(sdlWindow), reinterpret_cast<SDL_GLContext>(sdlGlContext));

	(*getCurrentContext()) = this;
}


UString Context::toString() const
{
	UString str;
	if (windowOwned == true) {
		str += "Hidden Window, ";
	}
	else {
		str += "Given Window, ";
	}
	str.append2(settings.toString());
	return str;
}


UString Context::toStringDebug() const
{
	UString str("Context(");
	str += toString();
	str += ", window=";
	str.append2(sdlWindow);
	str += ", context=";
	str.append2(sdlGlContext);
	str += ")";
	return str;
}


// public static

bool Context::isContextCreated()
{
	if ((*getCurrentContext()) != nullptr) {
		return true;
	}
	else {
		return false;
	}
}


bool Context::isCurrentContext(Context* context)
{
	if (context == (*getCurrentContext())) {
		return true;
	}
	else {
		return false;
	}
}


// private static

void** Context::getPreviousCreatedSdlGlContext()
{
	static void* previousCreatedSdlGlContext = nullptr;
	return &previousCreatedSdlGlContext;
}


Context** Context::getCurrentContext()
{
	static Context* currentContext = nullptr;
	return &currentContext;
}


} // End namespace Verso


