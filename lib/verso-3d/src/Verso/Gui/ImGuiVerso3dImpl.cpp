#include <Verso/Gui/ImGuiVerso3dImpl.hpp>
#include <Verso/Gui/imgui/imgui_impl_opengl3_verso3d.hpp>
#include <Verso/Gui/imgui/imgui_impl_sdl_verso3d.hpp>
#include <imgui_internal.h>

namespace Verso {


///////////////////////////////////////////////////////////////////////////////////////////
// ImGuiVerso3dImpl class implementation
///////////////////////////////////////////////////////////////////////////////////////////

bool ImGuiVerso3dImpl::created = false;
ImGuiContext* ImGuiVerso3dImpl::imGuiContext = nullptr;


bool ImGuiVerso3dImpl::create(IWindowOpengl& window)
{
	if (isCreated() == true) {
		return false;
	}

	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	imGuiContext = ImGui::CreateContext(); // \TODO: ImFontAtlas* shared_font_atlas = NULL use shared context maybe?
	ImGuiIO& io = ImGui::GetIO();
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;       // Enable Keyboard Controls
	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
	io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;           // Enable Docking
	//io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;       // Enable Multi-Viewport / Platform Windows
	//io.ConfigViewportsNoAutoMerge = true;
	//io.ConfigViewportsNoTaskBarIcon = true;

	// Setup Dear ImGui style, \TODO: not here!
	ImGui::StyleColorsDark();
	//ImGui::StyleColorsLight();
	//ImGui::StyleColorsClassic();

	// When viewports are enabled we tweak WindowRounding/WindowBg so platform windows can look identical to regular ones.
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
	{
		ImGuiStyle& style = ImGui::GetStyle();
		style.WindowRounding = 0.0f;
		style.Colors[ImGuiCol_WindowBg].w = 1.0f;
	}

	// Setup Platform/Renderer bindings
	ImGui_ImplSDL2_InitForOpenGL(window/*, gl_context*/); // \TODO: MULTI VIEWPORT
	ImGui_ImplOpenGL3_Init(window.getSettings().glslVersionString.c_str());

	// Load Fonts
	// - If no fonts are loaded, dear imgui will use the default font. You can also load multiple fonts and use ImGui::PushFont()/PopFont() to select them.
	// - AddFontFromFileTTF() will return the ImFont* so you can store it if you need to select the font among multiple.
	// - If the file cannot be loaded, the function will return NULL. Please handle those errors in your application (e.g. use an assertion, or display an error and quit).
	// - The fonts will be rasterized at a given size (w/ oversampling) and stored into a texture when calling ImFontAtlas::Build()/GetTexDataAsXXXX(), which ImGui_ImplXXXX_NewFrame below will call.
	// - Read 'misc/fonts/README.txt' for more instructions and details.
	// - Remember that in C/C++ if you want to include a backslash \ in a string literal you need to write a double backslash \\ !
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/Roboto-Medium.ttf", 16.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/Cousine-Regular.ttf", 15.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/DroidSans.ttf", 16.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/ProggyTiny.ttf", 10.0f);
	//ImFont* font = io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesJapanese());
	//IM_ASSERT(font != NULL);

	created = true;
	return true;
}


void ImGuiVerso3dImpl::destroy() VERSO_NOEXCEPT
{
	if (isCreated() == false) {
		return;
	}

	// Cleanup
	ImGuiIO& io = ImGui::GetIO();
	io.Fonts->Clear();

	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplSDL2_Shutdown();
	ImGui::DestroyContext(imGuiContext);

	created = false;
}


bool ImGuiVerso3dImpl::isCreated()
{
	return created;
}


bool ImGuiVerso3dImpl::handleEvent(const IWindowOpengl& window, const FrameTimestamp& time, const Event& event)
{
	(void)window; (void)time;
	return ImGui_ImplSDL2_ProcessEvent(event);
}


void ImGuiVerso3dImpl::newFrame(IWindowOpengl& window, const FrameTimestamp& time, bool useRenderResolution)
{
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplSDL2_NewFrame(window, time, useRenderResolution);
	ImGui::NewFrame();
}


void ImGuiVerso3dImpl::render(const IWindowOpengl& window, const Recti& viewportRect)
{
	(void)window; (void)viewportRect;

	ImGuiIO& io = ImGui::GetIO();
	ImGui::Render();
	glViewport(0, 0, static_cast<int>(io.DisplaySize.x), static_cast<int>(io.DisplaySize.y));
	// glViewport(viewportRect.pos.x, viewportRect.pos.y, viewportRect.size.x, viewportRect.size.y);
	// \TODO: use viewportrect maybe to limit imgui usable area
	GL_CHECK_FOR_ERRORS("", "");
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

	/* // Update and Render additional Platform Windows // \TODO: MULTI VIEWPORT
	// (Platform functions may change the current OpenGL context, so we save/restore it to make it easier to paste this code elsewhere.
	//  For this specific demo app we could also call SDL_GL_MakeCurrent(window, gl_context) directly)
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
	{
		SDL_Window* backup_current_window = SDL_GL_GetCurrentWindow();
		SDL_GLContext backup_current_context = SDL_GL_GetCurrentContext();
		ImGui::UpdatePlatformWindows();
		ImGui::RenderPlatformWindowsDefault();
		SDL_GL_MakeCurrent(backup_current_window, backup_current_context);
	}*/
}


void ImGuiVerso3dImpl::enablKbHighlight()
{
	imGuiContext->NavDisableHighlight = false;
}


} // End namespace Verso

