#include <Verso/Gui/ImGuiStyleLoader.hpp>
#include <Verso/System/JsonHelper3d.hpp>

#define IMGUI_DISABLE_OBSOLETE_FUNCTIONS
#include <imgui.h>

namespace Verso {
namespace ImGuiStyleLoader {


void loadAndApplyStyle(const UString& fileName)
{
	JSONValue* rootValue = JsonHelper::loadAndParse(fileName);
	const JSONObject& root = rootValue->AsObject();
	UString rootPath = "root.";

	ImGuiStyle& style = ImGui::GetStyle();
	style.WindowRounding = JsonHelper::readNumberf(root, rootPath, "WindowRounding", false, 5.3f);
	style.FrameRounding = JsonHelper::readNumberf(root, rootPath, "FrameRounding", false, 2.3f);
	style.ScrollbarRounding = JsonHelper::readNumberf(root, rootPath, "ScrollbarRounding", false, 0);

	JSONValue *colorsValue = JsonHelper::readValue(root, rootPath, "Colors", true);
	const JSONObject& colors = colorsValue->AsObject();
	UString colorsPath(rootPath+"Colors.");

	style.Colors[ImGuiCol_Text]                  = JsonHelper::readImVec4(colors, colorsPath, "Text", false, style.Colors[ImGuiCol_Text]);
	style.Colors[ImGuiCol_TextDisabled]          = JsonHelper::readImVec4(colors, colorsPath, "TextDisabled", false, style.Colors[ImGuiCol_TextDisabled]);
	style.Colors[ImGuiCol_WindowBg]              = JsonHelper::readImVec4(colors, colorsPath, "WindowBg", false, style.Colors[ImGuiCol_WindowBg]);
	style.Colors[ImGuiCol_ChildBg]               = JsonHelper::readImVec4(colors, colorsPath, "ChildBg", false, style.Colors[ImGuiCol_ChildBg]);
	style.Colors[ImGuiCol_PopupBg]               = JsonHelper::readImVec4(colors, colorsPath, "PopupBg", false, style.Colors[ImGuiCol_PopupBg]);
	style.Colors[ImGuiCol_Border]                = JsonHelper::readImVec4(colors, colorsPath, "Border", false, style.Colors[ImGuiCol_Border]);
	style.Colors[ImGuiCol_BorderShadow]          = JsonHelper::readImVec4(colors, colorsPath, "BorderShadow", false, style.Colors[ImGuiCol_BorderShadow]);
	style.Colors[ImGuiCol_FrameBg]               = JsonHelper::readImVec4(colors, colorsPath, "FrameBg", false, style.Colors[ImGuiCol_FrameBg]);
	style.Colors[ImGuiCol_FrameBgHovered]        = JsonHelper::readImVec4(colors, colorsPath, "FrameBgHovered", false, style.Colors[ImGuiCol_FrameBgHovered]);
	style.Colors[ImGuiCol_FrameBgActive]         = JsonHelper::readImVec4(colors, colorsPath, "FrameBgActive", false, style.Colors[ImGuiCol_FrameBgActive]);
	style.Colors[ImGuiCol_TitleBg]               = JsonHelper::readImVec4(colors, colorsPath, "TitleBg", false, style.Colors[ImGuiCol_TitleBg]);
	style.Colors[ImGuiCol_TitleBgCollapsed]      = JsonHelper::readImVec4(colors, colorsPath, "TitleBgCollapsed", false, style.Colors[ImGuiCol_TitleBgCollapsed]);
	style.Colors[ImGuiCol_TitleBgActive]         = JsonHelper::readImVec4(colors, colorsPath, "TitleBgActive", false, style.Colors[ImGuiCol_TitleBgActive]);
	style.Colors[ImGuiCol_MenuBarBg]             = JsonHelper::readImVec4(colors, colorsPath, "MenuBarBg", false, style.Colors[ImGuiCol_MenuBarBg]);
	style.Colors[ImGuiCol_ScrollbarBg]           = JsonHelper::readImVec4(colors, colorsPath, "ScrollbarBg", false, style.Colors[ImGuiCol_ScrollbarBg]);
	style.Colors[ImGuiCol_ScrollbarGrab]         = JsonHelper::readImVec4(colors, colorsPath, "ScrollbarGrab", false, style.Colors[ImGuiCol_ScrollbarGrab]);
	style.Colors[ImGuiCol_ScrollbarGrabHovered]  = JsonHelper::readImVec4(colors, colorsPath, "ScrollbarGrabHovered", false, style.Colors[ImGuiCol_ScrollbarGrabHovered]);
	style.Colors[ImGuiCol_ScrollbarGrabActive]   = JsonHelper::readImVec4(colors, colorsPath, "ScrollbarGrabActive", false, style.Colors[ImGuiCol_ScrollbarGrabActive]);
	style.Colors[ImGuiCol_CheckMark]             = JsonHelper::readImVec4(colors, colorsPath, "CheckMark", false, style.Colors[ImGuiCol_CheckMark]);
	style.Colors[ImGuiCol_SliderGrab]            = JsonHelper::readImVec4(colors, colorsPath, "SliderGrab", false, style.Colors[ImGuiCol_SliderGrab]);
	style.Colors[ImGuiCol_SliderGrabActive]      = JsonHelper::readImVec4(colors, colorsPath, "SliderGrabActive", false, style.Colors[ImGuiCol_SliderGrabActive]);
	style.Colors[ImGuiCol_Button]                = JsonHelper::readImVec4(colors, colorsPath, "Button", false, style.Colors[ImGuiCol_Button]);
	style.Colors[ImGuiCol_ButtonHovered]         = JsonHelper::readImVec4(colors, colorsPath, "ButtonHovered", false, style.Colors[ImGuiCol_ButtonHovered]);
	style.Colors[ImGuiCol_ButtonActive]          = JsonHelper::readImVec4(colors, colorsPath, "ButtonActive", false, style.Colors[ImGuiCol_ButtonActive]);
	style.Colors[ImGuiCol_Header]                = JsonHelper::readImVec4(colors, colorsPath, "Header", false, style.Colors[ImGuiCol_Header]);
	style.Colors[ImGuiCol_HeaderHovered]         = JsonHelper::readImVec4(colors, colorsPath, "HeaderHovered", false, style.Colors[ImGuiCol_HeaderHovered]);
	style.Colors[ImGuiCol_HeaderActive]          = JsonHelper::readImVec4(colors, colorsPath, "HeaderActive", false, style.Colors[ImGuiCol_HeaderActive]);
	style.Colors[ImGuiCol_Separator]             = JsonHelper::readImVec4(colors, colorsPath, "Separator", false, style.Colors[ImGuiCol_Separator]);
	style.Colors[ImGuiCol_SeparatorHovered]      = JsonHelper::readImVec4(colors, colorsPath, "SeparatorHovered", false, style.Colors[ImGuiCol_SeparatorHovered]);
	style.Colors[ImGuiCol_SeparatorActive]       = JsonHelper::readImVec4(colors, colorsPath, "SeparatorActive", false, style.Colors[ImGuiCol_SeparatorActive]);
	style.Colors[ImGuiCol_ResizeGrip]            = JsonHelper::readImVec4(colors, colorsPath, "ResizeGrip", false, style.Colors[ImGuiCol_ResizeGrip]);
	style.Colors[ImGuiCol_ResizeGripHovered]     = JsonHelper::readImVec4(colors, colorsPath, "ResizeGripHovered", false, style.Colors[ImGuiCol_ResizeGripHovered]);
	style.Colors[ImGuiCol_ResizeGripActive]      = JsonHelper::readImVec4(colors, colorsPath, "ResizeGripActive", false, style.Colors[ImGuiCol_ResizeGripActive]);
	style.Colors[ImGuiCol_PlotLines]             = JsonHelper::readImVec4(colors, colorsPath, "PlotLines", false, style.Colors[ImGuiCol_PlotLines]);
	style.Colors[ImGuiCol_PlotLinesHovered]      = JsonHelper::readImVec4(colors, colorsPath, "PlotLinesHovered", false, style.Colors[ImGuiCol_PlotLinesHovered]);
	style.Colors[ImGuiCol_PlotHistogram]         = JsonHelper::readImVec4(colors, colorsPath, "ImGuiCol_PlotHistogram", false, style.Colors[ImGuiCol_PlotHistogram]);
	style.Colors[ImGuiCol_PlotHistogramHovered]  = JsonHelper::readImVec4(colors, colorsPath, "ImGuiCol_PlotHistogramHovered", false, style.Colors[ImGuiCol_PlotHistogramHovered]);
	style.Colors[ImGuiCol_TextSelectedBg]        = JsonHelper::readImVec4(colors, colorsPath, "TextSelectedBg", false, style.Colors[ImGuiCol_TextSelectedBg]);
	style.Colors[ImGuiCol_ModalWindowDimBg]  = JsonHelper::readImVec4(colors, colorsPath, "ModalWindowDarkening", false, style.Colors[ImGuiCol_ModalWindowDimBg]);

	// Load user colors for ImGui tabs addon
//	ImGui::UserStyle.Colors[ImGui::ImGuiUserCol_TabNormal] = JsonHelper::readImVec4(colors, colorsPath, "ImGuiUserCol_TabNormal", false, ImGui::UserStyle.Colors[ImGui::ImGuiUserCol_TabNormal]);
//	ImGui::UserStyle.Colors[ImGui::ImGuiUserCol_TabBorder] = JsonHelper::readImVec4(colors, colorsPath, "ImGuiUserCol_TabBorder", false, ImGui::UserStyle.Colors[ImGui::ImGuiUserCol_TabBorder]);
//	ImGui::UserStyle.Colors[ImGui::ImGuiUserCol_TabBorderShadow] = JsonHelper::readImVec4(colors, colorsPath, "ImGuiUserCol_TabBorderShadow", false, ImGui::UserStyle.Colors[ImGui::ImGuiUserCol_TabBorderShadow]);
//	ImGui::UserStyle.Colors[ImGui::ImGuiUserCol_TabHover] = JsonHelper::readImVec4(colors, colorsPath, "ImGuiUserCol_TabHover", false, ImGui::UserStyle.Colors[ImGui::ImGuiUserCol_TabHover]);
//	ImGui::UserStyle.Colors[ImGui::ImGuiUserCol_TabTitleTextNormal] = JsonHelper::readImVec4(colors, colorsPath, "ImGuiUserCol_TabTitleTextNormal", false, ImGui::UserStyle.Colors[ImGui::ImGuiUserCol_TabTitleTextNormal]);
//	ImGui::UserStyle.Colors[ImGui::ImGuiUserCol_TabTitleTextSelected] = JsonHelper::readImVec4(colors, colorsPath, "ImGuiUserCol_TabTitleTextSelected", false, ImGui::UserStyle.Colors[ImGui::ImGuiUserCol_TabTitleTextSelected]);

	JsonHelper::free(rootValue);
}


} // End namespace ImGuiStyleLoader
} // End namespace Verso

