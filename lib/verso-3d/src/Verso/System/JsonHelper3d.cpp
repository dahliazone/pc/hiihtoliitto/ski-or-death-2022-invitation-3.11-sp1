#include <Verso/System/JsonHelper3d.hpp>

namespace Verso {


namespace JsonHelper {


BlendMode readBlendMode(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const BlendMode& defaultValue)
{
	UString blendModeStr = JsonHelper::readString(object, currentPathJson, name, required, "");
	if (!blendModeStr.isEmpty()) {
		BlendMode blendMode = stringToBlendMode(blendModeStr);
		if (blendMode == BlendMode::Undefined) {
			UString error("Unknown value ("+blendModeStr+") in field \""+currentPathJson+name+"\"! Must be one of: \"None\", \"Transcluent\", \"Additive\", \"Subtractive\", \"ReverseSubtractive\".");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}
		return blendMode;
	}
	else if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return defaultValue;
}


ClearFlag readClearFlag(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const ClearFlag& defaultValue)
{
	UString clearFlagStr = JsonHelper::readString(object, currentPathJson, name, required, "");
	if (!clearFlagStr.isEmpty()) {
		ClearFlag clearFlag = stringToClearFlag(clearFlagStr);
		if (clearFlag == ClearFlag::Unset) {
			UString error("Unknown value ("+clearFlagStr+") in field \""+currentPathJson+name+"\"! Must be one of: \"None\", \"ColorBuffer\", \"DepthBuffer\", \"SolidColor\", \"Skybox\".");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}
		return clearFlag;
	}
	else if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return defaultValue;
}


TexturePixelFormat readTexturePixelFormat(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const TexturePixelFormat& defaultValue)
{
	UString texturePixelFormatStr = JsonHelper::readString(object, currentPathJson, name, required, "");
	if (!texturePixelFormatStr.isEmpty()) {
		TexturePixelFormat texturePixelFormat = stringToTexturePixelFormat(texturePixelFormatStr);
		return texturePixelFormat;
	}
	else if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return defaultValue;
}


MinFilter readMinFilter(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const MinFilter& defaultValue)
{
	UString minFilterStr = JsonHelper::readString(object, currentPathJson, name, required, "");
	if (!minFilterStr.isEmpty()) {
		MinFilter minFilter = stringToMinFilter(minFilterStr);
		if (minFilter == MinFilter::Unset) {
			UString error("Unknown value ("+minFilterStr+") in field \""+currentPathJson+name+"\"! Must be one of: \"Unset\", \"Red\", \"Rg\", \"Rgb\", \"Bgr\", \"Rgba\", \"Bgra\", \"RedInteger\", \"RgInteger\", \"RgbInteger\", \"BgrInteger\", \"RgbaInteger\", \"BgraInteger\", \"DepthComponent\", \"DepthStencil\".");  // disabled: StencilIndex
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}
		return minFilter;
	}
	else if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\": \"Unset\", \"Red\", \"Rg\", \"Rgb\", \"Bgr\", \"Rgba\", \"Bgra\", \"RedInteger\", \"RgInteger\", \"RgbInteger\", \"BgrInteger\", \"RgbaInteger\", \"BgraInteger\", \"DepthComponent\", \"DepthStencil\"."); // disabled: StencilIndex
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return defaultValue;
}


MagFilter readMagFilter(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const MagFilter& defaultValue)
{
	UString magFilterStr = JsonHelper::readString(object, currentPathJson, name, required, "");
	if (!magFilterStr.isEmpty()) {
		MagFilter magFilter = stringToMagFilter(magFilterStr);
		if (magFilter == MagFilter::Unset) {
			UString error("Unknown value ("+magFilterStr+") in field \""+currentPathJson+name+"\"! Must be one of: \"Nearest\", \"Linear\".");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}
		return magFilter;
	}
	else if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return defaultValue;
}


WrapStyle readWrapStyle(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const WrapStyle& defaultValue)
{
	UString wrapStyleStr = JsonHelper::readString(object, currentPathJson, name, required, "");
	if (!wrapStyleStr.isEmpty()) {
		WrapStyle wrapStyle = stringToWrapStyle(wrapStyleStr);
		if (wrapStyle == WrapStyle::Unset) {
			UString error("Unknown value ("+wrapStyleStr+") in field \""+currentPathJson+name+"\"! Must be one of: \"Repeat\", \"MirroredRepeat\", \"ClampToEdge\", \"ClampToBorder\"."); // disabled: MirroredClampToEdge
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}
		return wrapStyle;
	}
	else if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\": ! Must be one of: \"Repeat\", \"MirroredRepeat\", \"ClampToEdge\", \"ClampToBorder\""); // disabled: MirroredClampToEdge
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return defaultValue;
}


PlayMode readPlayMode(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const PlayMode& defaultValue)
{
	UString playModeStr = JsonHelper::readString(object, currentPathJson, name, required, "");
	if (!playModeStr.isEmpty()) {
		PlayMode playMode = stringToPlayMode(playModeStr);
		if (playMode == PlayMode::Unset) {
			UString error("Unknown value ("+playModeStr+") in field \""+currentPathJson+name+"\"! Must be one of: \"Production\", \"Development\".");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}
		return playMode;
	}
	else if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return defaultValue;
}


DebugMode readDebugMode(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const DebugMode& defaultValue)
{
	UString debugModeStr = JsonHelper::readString(object, currentPathJson, name, required, "");
	if (!debugModeStr.isEmpty()) {
		DebugMode debugMode = stringToDebugMode(debugModeStr);
		if (debugMode == DebugMode::Unset) {
			UString error("Unknown value ("+debugModeStr+") in field \""+currentPathJson+name+"\"! Must be one of: \"Editor\", \"TimeControls\", \"Fullscreen\".");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}
		return debugMode;
	}
	else if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return defaultValue;
}


SequenceType readSequenceType(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const SequenceType& defaultValue)
{
	UString sequenceTypeStr = JsonHelper::readString(object, currentPathJson, name, required, "");
	if (!sequenceTypeStr.isEmpty()) {
		SequenceType sequenceType = stringToSequenceType(sequenceTypeStr);
		if (sequenceType == SequenceType::Unset) {
			UString error("Unknown value ("+sequenceTypeStr+") in field \""+currentPathJson+name+"\"! Must be one of: \"Production\", \"Development\".");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}
		return sequenceType;
	}
	else if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return defaultValue;
}


AnimationType readAnimationType(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const AnimationType& defaultValue )
{
	UString animationTypeStr = JsonHelper::readString(object, currentPathJson, name, required, "");
	if (!animationTypeStr.isEmpty()) {
		AnimationType animationType = stringToAnimationType(animationTypeStr);
		if (animationType == AnimationType::Unset) {
			UString error("Unknown value ("+animationTypeStr+") in field \""+currentPathJson+name+"\"! Must be one of: \"Loop\", \"Once\".");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}
		return animationType;
	}
	else if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return defaultValue;
}


CameraType readCameraType(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const CameraType& defaultValue)
{
	UString cameraTypeStr = JsonHelper::readString(object, currentPathJson, name, required, "");
	if (!cameraTypeStr.isEmpty()) {
		CameraType cameraType = stringToCameraType(cameraTypeStr);
		if (cameraType == CameraType::Unset) {
			UString error("Unknown value ("+cameraTypeStr+") in field \""+currentPathJson+name+"\"! Must be one of: \"Arcball\", \"Chase\", \"Fps\", \"Target\".");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}
		return cameraType;
	}
	else if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"! Must be one of: \"Arcball\", \"Chase\", \"Fps\", \"Target\".");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return defaultValue;
}


ProjectionType readProjectionType(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const ProjectionType& defaultValue)
{
	UString projectionTypeStr = JsonHelper::readString(object, currentPathJson, name, required, "");
	if (!projectionTypeStr.isEmpty()) {
		ProjectionType projectionType = stringToProjectionType(projectionTypeStr);
		if (projectionType == ProjectionType::Unset) {
			UString error("Unknown value (\""+projectionTypeStr+"\") in field \""+currentPathJson+name+"\"! Must be one of: \"Orthographic\", \"Perspective\".");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}
		return projectionType;
	}
	else if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\": Must be one of: \"Orthographic\", \"Perspective\".");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return defaultValue;
}


ImVec4 readImVec4(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const ImVec4& defaultValue)
{
	const JSONArray& vectorArr = JsonHelper::readArray(object, currentPathJson, name, required);
	if (vectorArr.size() <= 0) {
		if (required == false) {
			return defaultValue;
		}
		else {
			UString error("Cannot read required field \""+currentPathJson+name+"\"!");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}
	}
	if (vectorArr.size() != 4) {
		UString error("Invalid type of values in field \""+currentPathJson+name+"\"! ImVec4 array must contain exactly 4 values.");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	bool typeError = false;
	double x = 0.0f, y = 0.0f, z = 0.0f, k = 0.0f;
	if (vectorArr[0]->IsNumber())
		x = vectorArr[0]->AsNumber();
	else
		typeError = true;

	if (vectorArr[1]->IsNumber())
		y = vectorArr[1]->AsNumber();
	else
		typeError = true;

	if (vectorArr[2]->IsNumber())
		z = vectorArr[2]->AsNumber();
	else
		typeError = true;

	if (vectorArr[3]->IsNumber())
		k = vectorArr[3]->AsNumber();
	else
		typeError = true;

	if (typeError == true) {
		UString error("Invalid type of values in field \""+name+"\"! ImVec4 array must contain only number values.");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return ImVec4(static_cast<float>(x), static_cast<float>(y), static_cast<float>(z), static_cast<float>(k));
}


} // End namespace JsonHelper


} // End namespace Verso

