#include <Verso/System/errorhandlerWindowOpengl.hpp>
#include <Verso/Display/MsgBox.hpp>
#include <Verso/Display/WindowOpengl.hpp>
#include <Verso/System/Logger.hpp>
#include <Verso/System/stacktrace.hpp>

namespace Verso {


static UString staticTitle = "";


[[ noreturn ]] void versoTerminate()
{
	WindowOpengl::closeAll();

	std::exception_ptr current = std::current_exception();
	try {
		std::rethrow_exception(current);
	}
	catch (ExceptionInterface& e) {
		UString exceptionType(e.getType());
		UString message("Application died because of ");
		message.append(exceptionType);
		message.append("!\n");
		message.append(e.toString());
#ifndef NDEBUG
		message.append("\n\n");
		message.append(getStacktrace());
#endif
		MsgBox::run(MsgBoxType::Error, staticTitle, message);
	}
	catch (std::runtime_error& e) {
		UString exceptionType("std::runtime_error");
		UString message("Application died because of ");
		message.append(exceptionType);
		message.append("!\n");
		message.append(e.what());
#ifndef NDEBUG
		message.append("\n\n");
		message.append(getStacktrace());
#endif
		MsgBox::run(MsgBoxType::Error, staticTitle, message);
	}
	catch (std::exception& e) {
		UString exceptionType("std::exception");
		UString message("Application died because of ");
		message.append(exceptionType);
		message.append("!\n");
		message.append(e.what());
#ifndef NDEBUG
		message.append("\n\n");
		message.append(getStacktrace());
#endif
		MsgBox::run(MsgBoxType::Error, staticTitle, message);
	}
	catch (...) {
		UString exceptionType("unknown exception");
		UString message("Application died because of ");
		message.append(exceptionType);
		message.append("!\n");
		message.append("Check debugger for more information.");
#ifndef NDEBUG
		message.append("\n\n");
		message.append(getStacktrace());
#endif
		MsgBox::run(MsgBoxType::Error, staticTitle, message);
	}

	std::abort();
}


void setTerminateHandlerWindowOpengl(const UString& title)
{
	staticTitle = title;
	std::set_terminate(versoTerminate);
}


[[ noreturn ]] void runTerminateHandlerWindowOpengl(const UString& title)
{
	staticTitle = title;
	versoTerminate();
}


} // End namespace Verso

