#include <Verso/GrinderKit/DemoPartSettings.hpp>

namespace Verso {


DemoPartSettings::DemoPartSettings(const JSONObject& json, const UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator) :
	name(),
	type(),
	start(0),
	duration(0),
	priority(0),
	subsequentPriority(0), // needs to be set by using updateSubsequentPriority()
	duplicateLevel(0), // needs to be set by using updateSubsequentPriority()
	jsonOrder(jsonOrder),
	paramsJsonVal(nullptr),
	paramsJson(),
	currentPathJson(currentPathJson)
{
	importJson(json, currentPathJson, jsonOrder, startAccumulator);
}


DemoPartSettings::DemoPartSettings(const UString& name, const UString& type,
	double start, double duration,
	std::int32_t priority, std::int32_t jsonOrder,
	const JSONObject& paramsJson,
	const std::string& currentPathJson) :
	name(name),
	type(type),
	start(start),
	duration(duration),
	priority(priority),
	subsequentPriority(0), // needs to be set by using updateSubsequentPriority()
	duplicateLevel(0), // needs to be set by using updateSubsequentPriority()
	jsonOrder(jsonOrder),
	paramsJsonVal(new JSONValue(paramsJson)),
	paramsJson(paramsJsonVal->AsObject()),
	currentPathJson(currentPathJson)
{
}


DemoPartSettings::DemoPartSettings(DemoPartSettings&& original) :
	name(std::move(original.name)),
	type(std::move(original.type)),
	start(std::move(original.start)),
	duration(std::move(original.duration)),
	priority(std::move(original.priority)),
	subsequentPriority(std::move(original.subsequentPriority)),
	duplicateLevel(std::move(original.duplicateLevel)),
	jsonOrder(std::move(original.jsonOrder)),
	paramsJsonVal(std::move(original.paramsJsonVal)),
	paramsJson(std::move(original.paramsJson)),
	currentPathJson(std::move(original.currentPathJson))
{
	original.paramsJson.clear();
	original.paramsJsonVal = nullptr;
}


DemoPartSettings& DemoPartSettings::operator =(DemoPartSettings&& original)
{
	if (this != &original) {
		name = std::move(original.name);
		type = std::move(original.type);
		start = std::move(original.start);
		duration = std::move(original.duration);
		priority = std::move(original.priority);
		subsequentPriority = std::move(original.subsequentPriority);
		duplicateLevel = std::move(original.duplicateLevel);
		jsonOrder = std::move(original.jsonOrder);
		paramsJsonVal = std::move(original.paramsJsonVal);
		paramsJson = std::move(original.paramsJson);
		currentPathJson = std::move(original.currentPathJson);

		// Clear the original
		original.paramsJson.clear();
		original.paramsJsonVal = nullptr;
	}
	return *this;
}


DemoPartSettings::~DemoPartSettings()
{
	//delete this->paramsJsonVal; // \TODO: double free corruption because of some reason!
	//paramsJsonVal = nullptr;
}


void DemoPartSettings::importJson(const JSONObject& json, const UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator)
{
	this->name = JsonHelper::readString(json, currentPathJson, "name");
	this->type = JsonHelper::readString(json, currentPathJson, "type", true);
	this->start = JsonHelper::readNumberd(json, currentPathJson, "start", true) + startAccumulator;
	this->duration = JsonHelper::readNumberd(json, currentPathJson, "duration", true);
	this->priority = JsonHelper::readInt(json, currentPathJson, "priority", true);
	this->jsonOrder = jsonOrder;
	const JSONObject& params = JsonHelper::readObject(json, currentPathJson, "params");
	delete this->paramsJsonVal;
	this->paramsJsonVal = new JSONValue(params);
	this->paramsJson = paramsJsonVal->AsObject();
	this->currentPathJson = currentPathJson + ".params.";
}


JSONValue* DemoPartSettings::exportJson()
{
	JSONObject obj;
	obj[L"name"] = new JSONValue(name.toUtf8Wstring());
	obj[L"type"] = new JSONValue(type.toUtf8Wstring());
	obj[L"start"] = new JSONValue(start);
	obj[L"duration"] = new JSONValue(duration);
	obj[L"priority"] = new JSONValue(priority);
	//obj[L"jsonOrder"] = new JSONValue(jsonOrder);
	obj[L"paramsJson"] = new JSONValue(paramsJson);

	return new JSONValue(obj);
}


UString DemoPartSettings::toString(const UString& newLinePadding) const
{
	(void)newLinePadding;

	UString str("{ name=\"");
	str += name;
	str += ", type=\"";
	str += type;
	str += ",\n  start=";
	str.append2(start);
	str += ", duration=";
	str.append2(duration);
	str += ", priority=";
	str.append2(priority);
	str += ", jsonOrder=";
	str.append2(jsonOrder);
	str += " }";
	return str;
}


UString DemoPartSettings::toStringDebug(const UString& newLinePadding) const
{
	UString str("DemoPartSettings(");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso

