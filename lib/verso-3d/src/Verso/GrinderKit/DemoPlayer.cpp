#include <Verso/GrinderKit/DemoPlayer.hpp>

namespace Verso {


DemoPlayer::DemoPlayer() :
	settings(),
	playState(PlayState::Playing),
	previousRealPlayState(playState),
	pastDemoParts(),
	currentDemoParts(),
	futureDemoParts(),
	targetDeltaTime(0.0),
	elapsedSeconds(0.0),
	audio2d(nullptr),
	created(false),
	demoOver(false)
{
}


DemoPlayer::DemoPlayer(DemoPlayer&& original) :
	settings(std::move(original.settings)),
	playState(std::move(original.playState)),
	previousRealPlayState(std::move(original.previousRealPlayState)),
	pastDemoParts(std::move(original.pastDemoParts)),
	currentDemoParts(std::move(original.currentDemoParts)),
	futureDemoParts(std::move(original.futureDemoParts)),
	targetDeltaTime(std::move(original.targetDeltaTime)),
	elapsedSeconds(std::move(original.elapsedSeconds)),
	audio2d(std::move(original.audio2d)),
	created(std::move(original.created)),
	demoOver(std::move(original.demoOver))
{
	original.audio2d = nullptr;
}


DemoPlayer& DemoPlayer::operator =(DemoPlayer&& original)
{
	if (this != &original) {
		settings = std::move(original.settings);
		playState = std::move(original.playState);
		previousRealPlayState = std::move(original.previousRealPlayState);
		pastDemoParts = std::move(original.pastDemoParts);
		currentDemoParts = std::move(original.currentDemoParts);
		futureDemoParts = std::move(original.futureDemoParts);
		targetDeltaTime = std::move(original.targetDeltaTime);
		elapsedSeconds = std::move(original.elapsedSeconds);
		audio2d = std::move(original.audio2d);
		created = std::move(original.created);
		demoOver = std::move(original.demoOver);

		// Clear the original
		original.audio2d = nullptr;
	}
	return *this;
}


DemoPlayer::~DemoPlayer()
{
	destroy();
}


void DemoPlayer::createFromFile(const IDemoPartFactory* demoPartFactory, const UString& fileName)
{
	settings.createFromFile(demoPartFactory, fileName);

	created = true;
}


void DemoPlayer::createDemoParts(IWindowOpengl& window, Audio2d& audio2d)
{
	if (isCreated() == false) {
		VERSO_ERROR("verso-3d", "createFromFile() must be called before this method.", "");
	}

	this->audio2d = &audio2d;

	settings.createDemoParts(window, audio2d);

	std::copy(settings.demoParts.begin(),
			  settings.demoParts.end(),
			  std::back_inserter(futureDemoParts));
	sortFutureDemoParts();

	// Load music
	this->audio2d->loadMusic("main", settings.demoPaths.pathMusic()+settings.general.musicFileName);

	targetDeltaTime = 1.0 / static_cast<double>(settings.general.targetFps);
}


void DemoPlayer::destroy() VERSO_NOEXCEPT
{
	if (isCreated() == false) {
		return;
	}

	audio2d = nullptr;

	destroyDemoParts();

	settings.destroy();

	created = false;
}


void DemoPlayer::destroyDemoParts()
{
	if (isCreated() == false) {
		VERSO_ERROR("verso-3d", "createFromFile() must be called before this method.", "");
	}

	// Clear lists that contain only copies from demoParts
	futureDemoParts.clear();
	currentDemoParts.clear();
	pastDemoParts.clear();

	// Clear & destroy demoParts
	settings.destroyDemoParts();

	elapsedSeconds = 0.0;
}


///////////////////////////////////////////////////////////////////////////////////////////
// Player model
///////////////////////////////////////////////////////////////////////////////////////////

void DemoPlayer::startDemo(IWindowOpengl& window)
{
	// Load and fade in music
	this->audio2d->playMusic("main", true); // \TODO: make this as variable
	//this->audio2d->fadeInMusic("main", -1);

	// Do debug rewind if needed
	if (settings.debug.playMode == PlayMode::Development && settings.debug.playbackRangeEnabled && settings.debug.playbackRange.minValue != 0.0) {
		gotoAbsolute(window, settings.debug.playbackRange.minValue);
	}
	else {
		gotoAbsolute(window, 0);
	}

	demoOver = false;
}


void DemoPlayer::stopDemo()
{
	this->audio2d->stopMusic();
	demoOver = true;
}


void DemoPlayer::handleInput(IWindowOpengl& window, const FrameTimestamp& time)
{
	if (isCreated() == false) {
		VERSO_ERROR("verso-3d", "createFromFile() must be called before this method.", "");
	}

	// Simple model
	/*for (auto it = demoParts.begin(); it != demoParts.end(); ++it) {
		(*it)->handleInput(time, window);
	}*/

	// Full model
	updatePlayerModel(window, time.getDt().asSeconds());
}


void DemoPlayer::handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event)
{
	if (isCreated() == false) {
		VERSO_ERROR("verso-3d", "createFromFile() must be called before this method.", "");
	}

	// Simple model
	/*for (auto it = demoParts.begin(); it != demoParts.end(); ++it) {
		(*it)->handleEvent(event, time, window);
	}*/

	// Full model
	// \TODO: fix frame and previousAverageDt
	double deltaTime = time.getDt().asSeconds();
	FrameTimestamp newTime(
				0,
				0,
				Timestamp::seconds(deltaTime),
				Timestamp::seconds(elapsedSeconds),
				Timestamp::seconds(targetDeltaTime),
				deltaTime);

	for (auto it = currentDemoParts.begin(); it != currentDemoParts.end(); ++it) {
		(*it)->handleEvent(window, newTime, event);
	}
}


void DemoPlayer::render(IWindowOpengl& window, const FrameTimestamp& time)
{
	if (isCreated() == false) {
		VERSO_ERROR("verso-3d", "createFromFile() must be called before this method.", "");
	}

	// Simple model
	/*for (auto it = demoParts.begin(); it != demoParts.end(); ++it) {
		UString error("Before "+(*it)->getSettings().name);
		GL_CHECK_FOR_ERRORS(error.c_str(), "");

		(*it)->render(window, time);

		error ="After "+(*it)->getSettings().name;
		GL_CHECK_FOR_ERRORS(error.c_str(), "");
	}*/

	// Full model
	// \TODO: fix frame and previousAverageDt
	double deltaTime = time.getDt().asSeconds();

	for (auto it = currentDemoParts.begin(); it != currentDemoParts.end(); ++it) {
		UString error("Before "+(*it)->getSettings().name);
		GL_CHECK_FOR_ERRORS(error.c_str(), "");

		window.resetRelativeViewport();
		window.applyRenderViewport();

		FrameTimestamp newTime(
					0,
					0,
					Timestamp::seconds(deltaTime),
					Timestamp::seconds(elapsedSeconds - (*it)->getStartSeconds()),
					Timestamp::seconds(targetDeltaTime),
					deltaTime);

		(*it)->render(window, newTime);

		error = "After "+(*it)->getSettings().name;
		GL_CHECK_FOR_ERRORS(error.c_str(), "");
	}
}


void DemoPlayer::gotoAbsolute(IWindowOpengl& window, double absoluteSeconds)
{
	if (absoluteSeconds < 0.0) {
		elapsedSeconds = 0.0;
	}
	else {
		elapsedSeconds = absoluteSeconds;
	}

	// what demoparts are active? update lists
	// run each demopart to correct place

	// alternative way
	// - move everything to futureDemoParts and reset their timers
	for (auto it = currentDemoParts.begin();
		 it != currentDemoParts.end();)
	{
		futureDemoParts.push_back((*it));
		currentDemoParts.erase(it++);
	}

	for (auto it = pastDemoParts.begin();
		 it != pastDemoParts.end();)
	{
		futureDemoParts.push_back((*it));
		pastDemoParts.erase(it++);
	}

	for (auto it = futureDemoParts.begin();
		 it != futureDemoParts.end();
		 it++)
	{
		// reset timers
		(*it)->setElapsedSeconds(0.0);
	}
	sortFutureDemoParts();

	updateFutureDemoPartsToCurrentDemoParts(window, elapsedSeconds);
	updateCurrentDemoPartsToPastDemoParts(window, elapsedSeconds);
	sortActiveDemoParts();
	runActiveDemoParts(window, elapsedSeconds, elapsedSeconds);

	rewindAudio(elapsedSeconds);

	//VERSO_LOG_DEBUG("verso-3d", "REWINDED to " << elapsedSeconds << "s. demoparts now:");
	//printOutDemoParts();
}


void DemoPlayer::gotoRelative(IWindowOpengl& window, double relativeSeconds)
{
	// Going forward
	if (relativeSeconds > 0.0)
	{
		elapsedSeconds += relativeSeconds;

		updateFutureDemoPartsToCurrentDemoParts(window, elapsedSeconds);
		updateCurrentDemoPartsToPastDemoParts(window, elapsedSeconds);
		sortActiveDemoParts();
		runActiveDemoParts(window, relativeSeconds, elapsedSeconds);

		rewindAudio(elapsedSeconds);
	}

	// Going backward
	else {
		gotoAbsolute(window, elapsedSeconds + relativeSeconds);
	}
}


void DemoPlayer::rewindAudio(double absoluteSeconds)
{
	double position = absoluteSeconds;
	double maxLength = audio2d->getDurationSeconds("main") - 0.1f;
	if (position > maxLength) {
		position = maxLength;
	}
	audio2d->rewind("main", position);
}


bool DemoPlayer::updatePlayerModel(IWindowOpengl& window, double deltaTime)
{
	if (playState == PlayState::Paused) {
		return true;
	}

	double startTime = 0.0;
	double endTime = settings.general.duration;
	if (settings.debug.playbackRangeEnabled) {
		startTime = settings.debug.playbackRange.minValue;
		endTime = settings.debug.playbackRange.maxValue;
	}

	if (elapsedSeconds < startTime) {
		elapsedSeconds = startTime;
		rewindAudio(elapsedSeconds);
	}

	if (settings.general.loop == true && elapsedSeconds >= endTime) {
		gotoAbsolute(window, startTime);
		return true;
	}

	elapsedSeconds += deltaTime;
	if (elapsedSeconds > endTime) {
		double overTime = elapsedSeconds - endTime;
		deltaTime -= overTime; // \TODO: add rounding to zero when really close?
		if (deltaTime < 0.0) {
			deltaTime = 0.0;
		}
		elapsedSeconds = endTime;
	}

	if (settings.debug.playMode == PlayMode::Production &&
		settings.general.loop == false &&
		elapsedSeconds >= settings.general.duration)
	{
		VERSO_LOG_DEBUG("verso-3d", "Demo over");
		demoOver = true;
		return false;
	}

	bool changed = false;
	if (updateFutureDemoPartsToCurrentDemoParts(window, elapsedSeconds) == true) {
		changed = true;
	}

	if (updateCurrentDemoPartsToPastDemoParts(window, elapsedSeconds) == true) {
		changed = true;
	}

	sortActiveDemoParts();
	runActiveDemoParts(window, deltaTime, elapsedSeconds);

//	if (changed == true) {
//		printOutDemoParts();
//	}

	// If demo parts run out return false
	/*if (settings.playMode == PlayMode::Production
			&& futureDemoParts.empty()
			&& currentDemoParts.empty()) {
		demoOver = true;
		return false;
	}*/

	// Otherwise demo runs on
	return true;
}


void DemoPlayer::divideDeltaTime(double deltaTime, std::vector<double>& times)
{
	while (deltaTime > targetDeltaTime) {
		times.push_back(targetDeltaTime);
		deltaTime -= targetDeltaTime;
	}
	times.push_back(deltaTime);
}


// @brief Check for parts in futureDemoParts that are now current and move them to
// currentDemoParts list.
bool DemoPlayer::updateFutureDemoPartsToCurrentDemoParts(IWindowOpengl& window, double secondsElapsedDemo)
{
	bool changed = false;

	for (auto it = futureDemoParts.begin(); it != futureDemoParts.end(); )
	{
		if ((*it)->hasStarted(secondsElapsedDemo) == true)
		{
			//VERSO_LOG_DEBUG("verso-3d", "Moving futurepart \""<<(*it)->getName()<<"\" to currentDemoParts...");

			(*it)->addSurplusSeconds(secondsElapsedDemo - (*it)->getStartSeconds());
			(*it)->reset(window);

			currentDemoParts.push_back((*it));
			futureDemoParts.erase(it++);

			changed = true;
		}

		else {
			it++;
			break;
		}
	}

	return changed;
}


// @brief Check for parts in currentDemoParts that are now outdated and move them to
// pastDemoParts list.
bool DemoPlayer::updateCurrentDemoPartsToPastDemoParts(const IWindowOpengl& window, double secondsElapsedDemo)
{
	(void)window;
	bool changed = false;

	for (auto it = currentDemoParts.begin();
		 it != currentDemoParts.end();)
	{
		if ((*it)->hasEnded(secondsElapsedDemo) == true)
		{
			double secondsLeft = (*it)->getEndSeconds() - (*it)->getElapsedSeconds();
			if (secondsLeft != (*it)->getEndSeconds() && secondsLeft > 0.0) {
				(*it)->addSurplusSeconds(secondsLeft);
				it++;
				// \TODO: maybe not a good idea show from expired demopart for one frame after: prolly should remove it here
			}

			else {
				//VERSO_LOG_DEBUG("verso-3d", "Moving currentpart \""<<(*it)->getName()<<"\" to pastDemoParts...");

				pastDemoParts.push_back((*it));
				currentDemoParts.erase(it++);

				changed = true;
			}
		} else {
			it++;
		}
	}

	return changed;
}


// @brief Sort current demoparts list by priority.
void DemoPlayer::sortFutureDemoParts()
{
	// Sort the future demoparts list
	futureDemoParts.sort(DemoPartPtrSortPredicateForStartSeconds);
}


// @brief Sort current demoparts list by priority.
void DemoPlayer::sortActiveDemoParts()
{
	currentDemoParts.sort(DemoPartPtrSortPredicateForPriority);
}


// @brief Run currently active demoparts.
void DemoPlayer::runActiveDemoParts(IWindowOpengl& window, double deltaTime, double secondsElapsedDemo)
{
	std::vector<double> times;
	//divideDeltaTime(deltaTime, times);

	//bool verbose = false;
	//if (deltaTime == secondsElapsedDemo)
	//	verbose = true;
	//
	//if (verbose == true) {
	//	VERSO_LOG_DEBUG("verso-3d", "##### TIMES for " << deltaTime << " CONTAINS: ";
	//	for (vector<double>::iterator itTimes=times.begin();
	//		itTimes!=times.end();
	//		++itTimes)
	//	{
	//		VERSO_LOG_DEBUG_VARIABLE("verso-3d", "time", *itTimes);
	//	}
	//}

	for (auto it = currentDemoParts.begin();
		 it != currentDemoParts.end();
		 ++it)
	{
		double demoPartDeltaSeconds = deltaTime;
		if (secondsElapsedDemo-deltaTime < (*it)->getStartSeconds()) {
			demoPartDeltaSeconds =
					deltaTime - ((*it)->getStartSeconds()
								 - (secondsElapsedDemo - deltaTime));
			//VERSO_LOG_DEBUG("verso-3d", (*it)->getName() << ": original DT=" << deltaTime << " adjusted=" << demoPartDeltaSeconds);
		}

		// \TODO: fix frame and previousAverageDt
		(*it)->update(
					window,
					FrameTimestamp(
						0,
						0,
						Timestamp::seconds(demoPartDeltaSeconds),
						Timestamp::seconds(secondsElapsedDemo),
						Timestamp::seconds(targetDeltaTime),
						demoPartDeltaSeconds)
					);

		//if ((*it)->isIncrementalRun() == false) {
		//	(*it)->update(demoPartDeltaSeconds, secondsElapsedDemo);
		//}
		//
		//else {
		//	if (verbose == true)
		//		VERSO_LOG_DEBUG("verso-3d", "##### TIMES for delta=" << deltaTime << ", elapsed=" << secondsElapsedDemo << " CONTAINS: ");
		//	double elapsed = secondsElapsedDemo - deltaTime;
		//	for (vector<double>::iterator itTimes=times.begin();
		//			itTimes!=times.end();
		//			++itTimes)
		//	{
		//		elapsed += *itTimes;
		//		(*it)->update(*itTimes, elapsed);
		//		if (verbose == true)
		//			VERSO_LOG_DEBUG_VARIABLE("verso-3d", "time,elapsed", "(" << *itTimes << ", " << elapsed << ")");
		//	}
		//}
	}
}


UString DemoPlayer::getActiveDemoParts() const
{
	UString result = "";

	for (std::list<DemoPart*>::const_iterator it = currentDemoParts.begin();
		 it != currentDemoParts.end();
		 ++it)
	{
		result += "* "+(*it)->getName()+" (";
		result.append2((*it)->getStartSeconds());
		result += "-";
		result.append2((*it)->getEndSeconds());
		result += ")\n";
	}

	return result;
}


void DemoPlayer::printOutDemoParts() const
{
	VERSO_LOG_DEBUG("verso-3d", "DemoParts listing at " << elapsedSeconds << "s ==");
	VERSO_LOG_DEBUG_VARIABLE("verso-3d", ":: Past parts :", "");
	for (std::list<DemoPart*>::const_iterator it = pastDemoParts.begin();
		 it != pastDemoParts.end();
		 ++it)
	{
		VERSO_LOG_DEBUG_VARIABLE("verso-3d", (*it)->getName()<<" ("<<(*it)->getStartSeconds()<<"-"
								 <<(*it)->getEndSeconds()<<")", "total running time="<<(*it)->getElapsedSeconds());
	}

	VERSO_LOG_DEBUG_VARIABLE("verso-3d", ":: Current parts :", "");
	for (std::list<DemoPart*>::const_iterator it = currentDemoParts.begin();
		 it != currentDemoParts.end();
		 ++it)
	{
		VERSO_LOG_DEBUG_VARIABLE("verso-3d", (*it)->getName(),
								 "("<<(*it)->getStartSeconds()<<"-"<<(*it)->getEndSeconds()<<")");
	}

	VERSO_LOG_DEBUG_VARIABLE("verso-3d", ":: Future parts :", "");
	for (std::list<DemoPart*>::const_iterator it = futureDemoParts.begin();
		 it != futureDemoParts.end(); ++it) {
		VERSO_LOG_DEBUG_VARIABLE("verso-3d", (*it)->getName(),
								 "("<<(*it)->getStartSeconds()<<"-"<<(*it)->getEndSeconds()<<")");
	}
}


} // End namespace Verso

