#include <Verso/GrinderKit/DemoPlayerSettings.hpp>
#include <Verso/System/JsonHelper3d.hpp>

namespace Verso {


UString DemoPlayerSettings::supportedJsonFormat("GrinderKit04");


DemoPlayerSettings::DemoPlayerSettings() :
	sourceFileName(),

	// data
	format(),
	general(),
	demoPaths(),
	setupDialog(),
	debug(),
	demoParts(),

	// private
	demoPartFactory(nullptr),
	rootValue(nullptr),
	created(false)
{
}


DemoPlayerSettings::DemoPlayerSettings(DemoPlayerSettings&& original) :
	// Demo model
	sourceFileName(std::move(original.sourceFileName)),

	// data
	format(std::move(original.format)),
	general(std::move(original.general)),
	demoPaths(std::move(original.demoPaths)),
	setupDialog(std::move(original.setupDialog)),
	debug(std::move(original.debug)),
	demoParts(std::move(original.demoParts)),

	// private
	demoPartFactory(std::move(original.demoPartFactory)),
	rootValue(std::move(original.rootValue)),
	created(std::move(original.created))
{
	original.demoParts.clear();
	original.demoPartFactory = nullptr;
	original.rootValue = nullptr;
	original.created = false;
}


DemoPlayerSettings& DemoPlayerSettings::operator =(DemoPlayerSettings&& original)
{
	if (this != &original) {
		// Demo model
		sourceFileName = std::move(original.sourceFileName);

		// data
		format = std::move(original.format);
		general = std::move(original.general);
		demoPaths = std::move(original.demoPaths);
		setupDialog = std::move(original.setupDialog);
		debug = std::move(original.debug);
		demoParts = std::move(original.demoParts);

		// private
		demoPartFactory = std::move(original.demoPartFactory);
		rootValue = std::move(original.rootValue);
		created = std::move(original.created);

		// Clear the original
		original.demoParts.clear();
		original.demoPartFactory = nullptr;
		original.rootValue = nullptr;
		original.created = false;
	}
	return *this;
}


DemoPlayerSettings::~DemoPlayerSettings()
{
	destroy();
}


///////////////////////////////////////////////////////////////////////////////////////////
// Demo model
///////////////////////////////////////////////////////////////////////////////////////////

void DemoPlayerSettings::createFromFile(const IDemoPartFactory* demoPartFactory, const UString& fileName)
{
	VERSO_ASSERT_MSG("verso-3d", isCreated() == false, "Already created!");

	this->demoPartFactory = demoPartFactory;

	loadFromJson(fileName);

	created = true;
}


void DemoPlayerSettings::destroy()
{
	if (isCreated() == false) {
		return;
	}

	destroyDemoParts();

	this->demoPartFactory = nullptr;

	JsonHelper::free(rootValue);
	created = false;
}

bool DemoPlayerSettings::isCreated() const
{
	return created;
}


void DemoPlayerSettings::createDemoParts(IWindowOpengl& window, Audio2d& audio2d)
{
	if (isCreated() == false) {
		VERSO_ERROR("verso-3d", "createFromFile() must be called before this method.", "");
	}

	for (auto it = demoParts.begin(); it != demoParts.end(); ++it) {
		(*it)->create(window, audio2d);
	}
}


void DemoPlayerSettings::destroyDemoParts() VERSO_NOEXCEPT
{
	if (isCreated() == false) {
		return;
	}

	// Clear & destroy demoParts
	for (auto it = demoParts.begin(); it != demoParts.end(); ++it) {
		(*it)->destroy();
		if ((*it) != nullptr) {
			delete (*it);
		}
	}
	demoParts.clear();
}


void DemoPlayerSettings::loadFromJson(const UString& fileName)
{
	sourceFileName = fileName;
	if (rootValue != nullptr) {
		JsonHelper::free(rootValue);
		rootValue = nullptr;
	}
	rootValue = JsonHelper::loadAndParse(File::getBasePath() + sourceFileName);
	UString jsonFileNamePath = sourceFileName.substring(0, static_cast<int>(fileName.findLastOf("/"))+1);
	importJson(rootValue->AsObject(), jsonFileNamePath);
}


void DemoPlayerSettings::saveToJson(const UString& fileName)
{
	JSONValue* root = exportJson();
	JsonHelper::saveToFile(fileName, root, true);
	//delete root;
}


std::int32_t DemoPlayerSettings::processDemoPartsRecursive(const JSONObject& object, const UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator)
{
	const JSONArray& jsonDemoParts = JsonHelper::readArray(object, currentPathJson, "demoparts", true);

	if (jsonDemoParts.size() < 1) {
		VERSO_ILLEGALFORMAT("verso-3d", "demoparts array is empty!", sourceFileName.c_str());
	}

	for (JSONArray::const_iterator demopartIt = jsonDemoParts.begin(); demopartIt != jsonDemoParts.end(); ++demopartIt) {
		if ((*demopartIt) && (*demopartIt)->IsObject()) {
			const JSONObject& demoPartObj = (*demopartIt)->AsObject();
			UString demoPartCurrentPathJson(currentPathJson+"demoparts[");
			demoPartCurrentPathJson.append2(jsonOrder);
			demoPartCurrentPathJson += "].";

			// Special group for DemoParts
			if (JsonHelper::readString(demoPartObj, currentPathJson, "type", true) == "verso.group") {
				double start = startAccumulator + JsonHelper::readNumberd(demoPartObj, demoPartCurrentPathJson, "start", false, 0.0);
				jsonOrder = processDemoPartsRecursive(demoPartObj, demoPartCurrentPathJson, jsonOrder, start);
			}

			// Normal DemoPart
			else {
				DemoPart* part = demoPartFactory->instance(&demoPaths, demoPartObj, demoPartCurrentPathJson, jsonOrder, startAccumulator);
				demoParts.push_back(part);
				jsonOrder++;
			}
		}
		else {
			VERSO_ILLEGALFORMAT("verso-3d", "\"demoparts\" array contains other than objects", sourceFileName.c_str());
		}
	}

	return jsonOrder;
}


void DemoPlayerSettings::importJson(const JSONObject& json, const UString& jsonFileNamePath)
{
	UString currentPathJson("root.");

	// Format check
	format = JsonHelper::readString(json, currentPathJson, "format", true);
	if (!format.equals(supportedJsonFormat)) {
		UString error("Unsupported GrinderKit file format version ('"+format+"')! Supported formats: "+supportedJsonFormat);
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), sourceFileName.c_str());
	}

	// General
	const JSONObject& generalSettingsObj = JsonHelper::readObject(json, currentPathJson, "general");
	general.importJson(generalSettingsObj, currentPathJson);

	// Paths
	const JSONObject& paths = JsonHelper::readObject(json, currentPathJson, "paths");
	demoPaths.importJson(paths, currentPathJson, jsonFileNamePath);

	// Setup dialog
	const JSONObject& setupDialogSettingsObj = JsonHelper::readObject(json, currentPathJson, "setupDialog");
	setupDialog.importJson(setupDialogSettingsObj, currentPathJson);

	// Debug
	const JSONObject& debugSettingsObj = JsonHelper::readObject(json, currentPathJson, "debug");
	debug.importJson(debugSettingsObj, currentPathJson);

	// Demoparts
	std::int32_t jsonOrder = 0;
	processDemoPartsRecursive(json, currentPathJson, jsonOrder, 0.0);

	updateSubsequentPriorities();
}


JSONValue* DemoPlayerSettings::exportJson()
{
	JSONObject obj;

	obj[L"format"] = new JSONValue(supportedJsonFormat.toUtf8Wstring());

	// - general
	obj[L"general"] = general.exportJson();

	// - paths
	obj[L"paths"] = demoPaths.exportJson();

	// - Setup dialog
	obj[L"setupDialog"] = setupDialog.exportJson();

	// - debug
	obj[L"debug"] = debug.exportJson();

	// - demoparts
	{
		JSONArray demoPartsArray;

		std::multimap<std::int32_t, DemoPart*> demoPartPriorities;
		for (auto& i : demoParts) {
			i->getSettings().duplicateLevel = 0;
			demoPartPriorities.insert(std::pair<std::int32_t, DemoPart*>(i->getSettings().priority, i));
		}

		for (auto i = demoPartPriorities.begin(); i != demoPartPriorities.end(); ++i) {
			JSONValue* v = i->second->getSettings().exportJson();
			demoPartsArray.push_back(v);
		}

		obj[L"demoparts"] = new JSONValue(demoPartsArray);
	}

	return new JSONValue(obj);
}


std::int32_t DemoPlayerSettings::findDemoPart(DemoPart* demoPart)
{
	if (demoPart == nullptr) {
		return false;
	}

	for (size_t i=0; i<demoParts.size(); ++i) {
		if (demoParts[i] == demoPart) {
			return static_cast<std::int32_t>(i);
		}
	}

	return -1;
}


bool DemoPlayerSettings::moveDemoPart(DemoPart* demoPart, double newStart)
{
	if (findDemoPart(demoPart) < 0) {
		return false;
	}
	if (newStart < 0.0) {
		newStart = 0.0;
	}

	double oldStart = demoPart->getSettings().start;
	demoPart->getSettings().start = newStart;
	for (size_t i=0; i<demoParts.size(); ++i) {
		if (demoParts[i] != demoPart && demoParts[i]->collidesWithPriority(demoPart)) {
			demoPart->getSettings().start = oldStart;
			return false;
		}
	}

	return true;
}


bool DemoPlayerSettings::resizeDemoPart(DemoPart* demoPart, double newDuration)
{
	if (findDemoPart(demoPart) < 0) {
		return false;
	}
	if (newDuration < 0.01) {
		newDuration = 0.01;
	}

	double oldDuration = demoPart->getSettings().duration;

	demoPart->getSettings().duration = newDuration;
	for (size_t i=0; i<demoParts.size(); ++i) {
		if (demoParts[i] != demoPart && demoParts[i]->collidesWithPriority(demoPart)) {
			demoPart->getSettings().duration = oldDuration;
			return false;
		}
	}

	return true;
}


void DemoPlayerSettings::updateSubsequentPriorities()
{
	std::multimap<std::int32_t, DemoPart*> demoPartPriorities;

	// Insert demoparts into the multimap by priority as key
	for (auto& demoPart : demoParts) {
		demoPart->getSettings().duplicateLevel = 0;
		demoPartPriorities.insert(std::pair<std::int32_t, DemoPart*>(demoPart->getSettings().priority, demoPart));
	}

	// Set initial subsequentPriorities
	std::int32_t priorityCounter = 0;
	std::int32_t lastPriority = demoPartPriorities.begin()->second->getSettings().priority;
	for (auto it = demoPartPriorities.begin(); it != demoPartPriorities.end(); ++it) {
		auto& settings = it->second->getSettings();
		if (lastPriority != settings.priority) {
			lastPriority = settings.priority;
			priorityCounter++;
		}
		settings.subsequentPriority = priorityCounter;
	}

	// Calculate duplicate priority levels for colliding demoparts and update subsequentPriorities
	std::map<std::int32_t, std::int32_t> maxDuplicateLevelForPriority;
	for (auto itA = demoPartPriorities.begin(); itA != demoPartPriorities.end(); ++itA) {
		for (auto j = itA; j != demoPartPriorities.end(); ++j) {
			DemoPart* a = (*itA).second;
			DemoPart* b = (*j).second;

			if (a != b && a->collidesWithPriority(b) == true) {
				if (a->getSettings().start <= b->getSettings().start) {
					b->getSettings().duplicateLevel++;
					maxDuplicateLevelForPriority[b->getSettings().priority] = b->getSettings().duplicateLevel;
				}
				else {
					a->getSettings().duplicateLevel++;
					maxDuplicateLevelForPriority[a->getSettings().priority] = a->getSettings().duplicateLevel;
				}
			}
		}
	}

	// Update subsequentPriorities based in duplicate levels
	for (auto& demoPart : demoParts) {
		demoPart->getSettings().subsequentPriority += demoPart->getSettings().duplicateLevel;
	}

	// Apply extra "layers" for subsequentPriorities based on duplicate levels
	for (auto it = maxDuplicateLevelForPriority.begin(); it != maxDuplicateLevelForPriority.end(); ++it) {
		std::int32_t priority = it->first;
		std::int32_t maxPriority = it->second;
		for (auto& j : demoParts) {
			auto& settings = j->getSettings();
			if (settings.priority > priority) {
				settings.subsequentPriority += maxPriority;
			}
		}
	}
}


UString DemoPlayerSettings::toString(const UString& newLinePadding) const
{
	UString str;

	str += "{ \"sourceFileName\": \"" + sourceFileName + "\"";
	{
		UString newLinePadding2 = newLinePadding + "  ";
		str += ",\n" + newLinePadding2 + "\"format\": \"" + format + "\"";

		str += ",\n" + newLinePadding2 + "\"general\": ";
		str += general.toString(newLinePadding2 + "  ");

		str += ",\n" + newLinePadding2 + "\"paths\": ";
		str += demoPaths.toString(newLinePadding2 + "  ");

		str += ",\n" + newLinePadding2 + "\"setupDialog\": ";
		str += setupDialog.toString(newLinePadding2 + "  ");

		str += ",\n" + newLinePadding2 + "\"debug\": ";
		str += debug.toString(newLinePadding2 + "  ");

		str += ",\n" + newLinePadding2 + "\"demoparts\": [";
		{
			UString newLinePadding3 = newLinePadding2 + "  ";
			size_t i = 0;
			for (auto it = demoParts.begin(); it != demoParts.end(); ++it) {
				if (i != 0) {
					str += str += ",\n" + newLinePadding3;
				}
				else {
					str += str += "\n" + newLinePadding3;
				}
				str += (*it)->toString(newLinePadding3);
				i++;
			}
		}
		str += "\n" + newLinePadding2 + "]";
	}
	str += "\n" + newLinePadding + "}";

	return str;
}


UString DemoPlayerSettings::toStringDebug(const UString& newLinePadding) const
{
	UString str("Verso::DemoPlayerSettings(\n");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso

