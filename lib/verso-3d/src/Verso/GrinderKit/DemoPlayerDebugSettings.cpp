#include <Verso/GrinderKit/DemoPlayerDebugSettings.hpp>

namespace Verso {


const PlayMode defaultPlayMode = PlayMode::Production;
const DebugMode defaultDebugMode = DebugMode::Editor;
const float defaultFontSizeSmaller = 18.0f;
const float defaultFontSizeBigger = 28.0f;
const bool defaultPlaybackRangeEnabled = false;
const Ranged defaultPlaybackRange(0.0, 0.0);


DemoPlayerDebugSettings::DemoPlayerDebugSettings() :
	playMode(defaultPlayMode),
	debugMode(defaultDebugMode),
	fontFileName(),
	fontSizeSmaller(defaultFontSizeSmaller),
	fontSizeBigger(defaultFontSizeBigger),
	playbackRangeEnabled(defaultPlaybackRangeEnabled),
	playbackRange(defaultPlaybackRange)
{
}


DemoPlayerDebugSettings::DemoPlayerDebugSettings(const DemoPlayerDebugSettings& original) :
	playMode(original.playMode),
	debugMode(original.debugMode),
	fontFileName(original.fontFileName),
	playbackRangeEnabled(original.playbackRangeEnabled),
	playbackRange(original.playbackRange)
{
}


DemoPlayerDebugSettings::DemoPlayerDebugSettings(DemoPlayerDebugSettings&& original) :
	playMode(std::move(original.playMode)),
	debugMode(std::move(original.debugMode)),
	fontFileName(std::move(original.fontFileName)),
	playbackRangeEnabled(std::move(original.playbackRangeEnabled)),
	playbackRange(std::move(original.playbackRange))
{
}


DemoPlayerDebugSettings& DemoPlayerDebugSettings::operator =(const DemoPlayerDebugSettings& original)
{
	if (this != &original) {
		playMode = original.playMode;
		debugMode = original.debugMode;
		fontFileName = original.fontFileName;
		playbackRangeEnabled = original.playbackRangeEnabled;
		playbackRange = original.playbackRange;
	}
	return *this;
}


DemoPlayerDebugSettings& DemoPlayerDebugSettings::operator =(DemoPlayerDebugSettings&& original)
{
	if (this != &original) {
		playMode = std::move(original.playMode);
		debugMode = std::move(original.debugMode);
		fontFileName = std::move(original.fontFileName);
		playbackRangeEnabled = std::move(original.playbackRangeEnabled);
		playbackRange = std::move(original.playbackRange);

		// Clear the original
	}
	return *this;
}


DemoPlayerDebugSettings::~DemoPlayerDebugSettings()
{
}


void DemoPlayerDebugSettings::importJson(const JSONObject& debugSettingsObj, const UString& currentPathJson)
{
	playMode = JsonHelper::readPlayMode(debugSettingsObj, currentPathJson, "playmode", false, defaultPlayMode);
	debugMode = JsonHelper::readDebugMode(debugSettingsObj, currentPathJson, "debugMode", false, defaultDebugMode);
	fontFileName = JsonHelper::readString(debugSettingsObj, currentPathJson, "font", true);
	fontSizeSmaller = JsonHelper::readNumberf(debugSettingsObj, currentPathJson, "fontSizeSmaller", false, defaultFontSizeSmaller);
	fontSizeBigger = JsonHelper::readNumberf(debugSettingsObj, currentPathJson, "fontSizeBigger", false, defaultFontSizeBigger);
	playbackRange = JsonHelper::readRanged(debugSettingsObj, currentPathJson, "playbackRange", false, defaultPlaybackRange);
	playbackRangeEnabled = defaultPlaybackRangeEnabled;
	if ((playbackRange.minValue != 0.0 || playbackRange.maxValue != 0.0) && playbackRange.minValue < playbackRange.maxValue) {
		playbackRangeEnabled = true;
	}
}


JSONValue* DemoPlayerDebugSettings::exportJson()
{
	JSONObject obj;

	obj[L"playMode"] = JsonHelper::writeString(playModeToString(playMode));
	obj[L"debugMode"] = JsonHelper::writeString(debugModeToString(debugMode));
	obj[L"font"] = JsonHelper::writeString(fontFileName);
	obj[L"fontSizeSmaller"] = JsonHelper::writeNumberf(fontSizeSmaller);
	obj[L"fontSizeBigger"] = JsonHelper::writeNumberf(fontSizeBigger);
	obj[L"playbackRange"] = JsonHelper::writeRanged(playbackRange);

	return new JSONValue(obj);
}


UString DemoPlayerDebugSettings::toString(const UString& newLinePadding) const
{
	UString str("{ ");
	str += "\"playMode\": \"" + playModeToString(playMode) + "\", ";
	str += "\"debugMode\": \"" + debugModeToString(debugMode) + "\"";
	{
		UString newLinePadding2 = newLinePadding + "  ";
		str += ",\n" + newLinePadding2 + "\"fontFileName\": \"" + fontFileName + "\"";
		str += ",\n" + newLinePadding2 + "\"fontSizeSmaller\": ";
		str.append2(fontSizeSmaller);
		str += ",\n" + newLinePadding2 + "\"fontSizeBigger\": ";
		str.append2(fontSizeBigger);
		str += ",\n" + newLinePadding2 + "\"playbackRangeEnabled\": ";
		if (playbackRangeEnabled == true) {
			str += "true";
		}
		else {
			str += "false";
		}
		str += ",\n" + newLinePadding2 + "\"playbackRange\": \"" + playbackRange.toString() + "\"";
	}
	str += "\n" + newLinePadding + "}";

	return str;
}


UString DemoPlayerDebugSettings::toStringDebug(const UString& newLinePadding) const
{
	UString str("Verso::DemoPlayerDebugSettings(\n");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso
