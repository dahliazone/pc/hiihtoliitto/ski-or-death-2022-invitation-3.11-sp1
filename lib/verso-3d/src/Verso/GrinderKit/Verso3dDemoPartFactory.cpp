#include <Verso/GrinderKit/Verso3dDemoPartFactory.hpp>
#include <Verso/GrinderKit/DemoParts/ClearScreen.hpp>
#include <Verso/GrinderKit/DemoParts/EgaJs.hpp>
#include <Verso/GrinderKit/DemoParts/Heightmap.hpp>
#include <Verso/GrinderKit/DemoParts/ImageViewer.hpp>
#include <Verso/GrinderKit/DemoParts/ImGuiTest.hpp>
#include <Verso/GrinderKit/DemoParts/Particles.hpp>
#include <Verso/GrinderKit/DemoParts/RandomBackground.hpp>
#include <Verso/GrinderKit/DemoParts/Shadertoy.hpp>
#include <Verso/GrinderKit/DemoParts/SimpleScroller.hpp>
#include <Verso/GrinderKit/DemoParts/Sprites3d.hpp>
#include <Verso/GrinderKit/DemoParts/Text.hpp>

namespace Verso {


Verso3dDemoPartFactory::Verso3dDemoPartFactory()
{
}


Verso3dDemoPartFactory::~Verso3dDemoPartFactory()
{
}


DemoPart* Verso3dDemoPartFactory::instance(
		const DemoPaths* demoPaths, const JSONObject& demoPartJson,
		const UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator) const
{
	UString type = JsonHelper::readString(demoPartJson, currentPathJson, "type", true);

	if (type == "verso.clearscreen") {
		return new ClearScreen(demoPaths, demoPartJson, currentPathJson, jsonOrder, startAccumulator);
	}

	else if (type == "verso.egajs") {
		return new EgaJs(demoPaths, demoPartJson, currentPathJson, jsonOrder, startAccumulator);
	}

	else if (type == "verso.heightmap") {
		return new Heightmap(demoPaths, demoPartJson, currentPathJson, jsonOrder, startAccumulator);
	}

	else if (type == "verso.imageviewer") {
		return new ImageViewer(demoPaths, demoPartJson, currentPathJson, jsonOrder, startAccumulator);
	}

	else if (type == "verso.imguitest") {
		return new ImGuiTest(demoPaths, demoPartJson, currentPathJson, jsonOrder, startAccumulator);
	}

	else if (type == "verso.particles") {
		return new Particles(demoPaths, demoPartJson, currentPathJson, jsonOrder, startAccumulator);
	}

	else if (type == "verso.randombackground") {
		return new RandomBackground(demoPaths, demoPartJson, currentPathJson, jsonOrder, startAccumulator);
	}

	else if (type == "verso.shadertoy") {
		return new Shadertoy(demoPaths, demoPartJson, currentPathJson, jsonOrder, startAccumulator);
	}

	else if (type == "verso.simplescroller") {
		return new SimpleScroller(demoPaths, demoPartJson, currentPathJson, jsonOrder, startAccumulator);
	}

	else if (type == "verso.sprites3d") {
		return new Sprites3d(demoPaths, demoPartJson, currentPathJson, jsonOrder, startAccumulator);
	}

	else if (type == "verso.text") {
		return new Text(demoPaths, demoPartJson, currentPathJson, jsonOrder, startAccumulator);
	}

	else {
		UString error("Cannot find DemoPart with given type!");
		UString typeInfo("type = \""+type+"\"");
		VERSO_OBJECTNOTFOUND("verso-3d", error.c_str(), type.c_str());
	}
}


} // End namespace Verso

