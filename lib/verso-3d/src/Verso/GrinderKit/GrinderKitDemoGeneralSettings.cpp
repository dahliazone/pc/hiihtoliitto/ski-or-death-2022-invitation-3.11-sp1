#include <Verso/GrinderKit/GrinderKitDemoGeneralSettings.hpp>
#include <Verso/Math/ColorGenerator.hpp>

namespace Verso {


GrinderKitDemoGeneralSettings::GrinderKitDemoGeneralSettings() :
	name(),
	version(),
	authors(),

	musicFileName(),
	contentAspectRatio(),
	pixelAspectRatio(),
	borderColor(),
	targetFps(60.0),
	duration(0.0),
	loop(false)
{
}


GrinderKitDemoGeneralSettings::GrinderKitDemoGeneralSettings(const GrinderKitDemoGeneralSettings& original) :
	name(original.name),
	version(original.version),
	authors(original.authors),

	musicFileName(original.musicFileName),
	contentAspectRatio(original.contentAspectRatio),
	pixelAspectRatio(original.pixelAspectRatio),
	borderColor(original.borderColor),
	targetFps(original.targetFps),
	duration(original.duration),
	loop(original.loop)
{
}


GrinderKitDemoGeneralSettings::GrinderKitDemoGeneralSettings(GrinderKitDemoGeneralSettings&& original) :
	name(std::move(original.name)),
	version(std::move(original.version)),
	authors(std::move(original.authors)),

	musicFileName(std::move(original.musicFileName)),
	contentAspectRatio(std::move(original.contentAspectRatio)),
	pixelAspectRatio(std::move(original.pixelAspectRatio)),
	borderColor(std::move(original.borderColor)),
	targetFps(std::move(original.targetFps)),
	duration(std::move(original.duration)),
	loop(std::move(original.loop))
{
	// Clear original
}


GrinderKitDemoGeneralSettings& GrinderKitDemoGeneralSettings::operator =(const GrinderKitDemoGeneralSettings& original)
{
	if (this != &original) {
		name = original.name;
		version = original.version;
		authors = original.authors;

		musicFileName = original.musicFileName;
		contentAspectRatio = original.contentAspectRatio;
		pixelAspectRatio = original.pixelAspectRatio;
		borderColor = original.borderColor;
		targetFps = original.targetFps;
		duration = original.duration;
		loop = original.loop;
	}
	return *this;
}


GrinderKitDemoGeneralSettings& GrinderKitDemoGeneralSettings::operator =(GrinderKitDemoGeneralSettings&& original)
{
	if (this != &original) {
		name = std::move(original.name);
		version = std::move(original.version);
		authors = std::move(original.authors);

		musicFileName = std::move(original.musicFileName);
		contentAspectRatio = std::move(original.contentAspectRatio);
		pixelAspectRatio = std::move(original.pixelAspectRatio);
		borderColor = std::move(original.borderColor);
		targetFps = std::move(original.targetFps);
		duration = std::move(original.duration);
		loop = std::move(original.loop);

		// Clear original
	}
	return *this;
}


GrinderKitDemoGeneralSettings::~GrinderKitDemoGeneralSettings()
{
}


void GrinderKitDemoGeneralSettings::importJson(const JSONObject& demoGeneralSettingsObj, const UString& currentPathJson)
{
	name = JsonHelper::readString(demoGeneralSettingsObj, currentPathJson, "name", true);
	version = JsonHelper::readString(demoGeneralSettingsObj, currentPathJson, "version", true);
	authors = JsonHelper::readString(demoGeneralSettingsObj, currentPathJson, "authors", true);

	musicFileName = JsonHelper::readString(demoGeneralSettingsObj, currentPathJson, "music");
	contentAspectRatio = JsonHelper::readAspectRatio(demoGeneralSettingsObj, currentPathJson, "contentAspectRatio", false, AspectRatio());
	pixelAspectRatio = JsonHelper::readPixelAspectRatio(demoGeneralSettingsObj, currentPathJson, "pixelAspectRatio", false, PixelAspectRatio());
	borderColor = JsonHelper::readColor(demoGeneralSettingsObj, currentPathJson, "borderColor", false, ColorGenerator::getColor(ColorRgb::Black));
	targetFps = JsonHelper::readNumberd(demoGeneralSettingsObj, currentPathJson, "targetFps", false, 60.0);
	duration = JsonHelper::readNumberd(demoGeneralSettingsObj, currentPathJson, "duration", false, 180.0);
	loop = JsonHelper::readBool(demoGeneralSettingsObj, currentPathJson, "loop", false, false);
}


JSONValue* GrinderKitDemoGeneralSettings::exportJson()
{
	JSONObject obj;

	obj[L"name"] = new JSONValue(name.toUtf8Wstring());
	obj[L"version"] = new JSONValue(version.toUtf8Wstring());
	obj[L"authors"] = new JSONValue(authors.toUtf8Wstring());

	obj[L"music"] = new JSONValue(musicFileName.toUtf8Wstring());
	obj[L"contentAspectRatio"] = JsonHelper::writeString(contentAspectRatio.toString());
	obj[L"pixelAspectRatio"] = JsonHelper::writeString(pixelAspectRatio.toString());
	obj[L"borderColor"] = JsonHelper::writeColor(borderColor, false); // discard alpha
	obj[L"targetFps"] = new JSONValue(targetFps);
	obj[L"duration"] = new JSONValue(duration);
	obj[L"loop"] = new JSONValue(loop);

	return new JSONValue(obj);
}


UString GrinderKitDemoGeneralSettings::toString(const UString& newLinePadding) const
{
	UString str("{");
	{
		UString newLinePadding2 = newLinePadding + "  ";
		str += "\n" + newLinePadding2 + "\"name\": \"" + name + "\"";
		str += ",\n" + newLinePadding2 + "\"version\": \"" + version + "\"";
		str += ",\n" + newLinePadding2 + "\"authors\": \"" + authors + "\"";

		str += ",\n" + newLinePadding2 + "\"musicFileName\": \"" + musicFileName + "\"";
		str += ",\n" + newLinePadding2 + "\"contentAspectRatio\": \"" + contentAspectRatio.toString() + "\"";
		str += ",\n" + newLinePadding2 + "\"pixelAspectRatio\": \"" + pixelAspectRatio.toString() + "\"";
		str += ",\n" + newLinePadding2 + "\"borderColor\": \"" + borderColor.toString() + "\"";

		str += ",\n" + newLinePadding2 + "\"targetFps\": ";
		str.append2(targetFps);

		str += ",\n" + newLinePadding2 + "\"duration\": ";
		str.append2(duration);

		str += ",\n" + newLinePadding2 + "\"loop\": ";
		if (loop == true) {
			str += "true";
		}
		else {
			str += "false";
		}
	}
	str += "\n" + newLinePadding + "}";

	return str;
}


UString GrinderKitDemoGeneralSettings::toStringDebug(const UString& newLinePadding) const
{
	UString str("Verso::GrinderKitDemoGeneralSettings(\n");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso


