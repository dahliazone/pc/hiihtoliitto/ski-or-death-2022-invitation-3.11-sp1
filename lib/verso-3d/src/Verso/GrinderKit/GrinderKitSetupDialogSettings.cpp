#include <Verso/GrinderKit/GrinderKitSetupDialogSettings.hpp>
#include <Verso/Math/ColorGenerator.hpp>

namespace Verso {


GrinderKitSetupDialogSettings::GrinderKitSetupDialogSettings() :
	windowTitle(),
	logoFileName(),
	fontFileName(),
	backgroundColor(),
	startButtonText(),
	quitButtonText()
{
}


GrinderKitSetupDialogSettings::GrinderKitSetupDialogSettings(const GrinderKitSetupDialogSettings& original) :
	windowTitle(original.windowTitle),
	logoFileName(original.logoFileName),
	fontFileName(original.fontFileName),
	backgroundColor(original.backgroundColor),
	startButtonText(original.startButtonText),
	quitButtonText(original.quitButtonText)
{
}


GrinderKitSetupDialogSettings::GrinderKitSetupDialogSettings(GrinderKitSetupDialogSettings&& original) :
	windowTitle(std::move(original.windowTitle)),
	logoFileName(std::move(original.logoFileName)),
	fontFileName(std::move(original.fontFileName)),
	backgroundColor(std::move(original.backgroundColor)),
	startButtonText(std::move(original.startButtonText)),
	quitButtonText(std::move(original.quitButtonText))
{
	// Clear original
}


GrinderKitSetupDialogSettings& GrinderKitSetupDialogSettings::operator =(const GrinderKitSetupDialogSettings& original)
{
	if (this != &original) {
		windowTitle = original.windowTitle;
		logoFileName = original.logoFileName;
		fontFileName = original.fontFileName;
		backgroundColor = original.backgroundColor;
		startButtonText = original.startButtonText;
		quitButtonText = original.quitButtonText;
	}
	return *this;
}


GrinderKitSetupDialogSettings& GrinderKitSetupDialogSettings::operator =(GrinderKitSetupDialogSettings&& original)
{
	if (this != &original) {
		windowTitle = std::move(original.windowTitle);
		logoFileName = std::move(original.logoFileName);
		fontFileName = std::move(original.fontFileName);
		backgroundColor = std::move(original.backgroundColor);
		startButtonText = std::move(original.startButtonText);
		quitButtonText = std::move(original.quitButtonText);

		// Clear original
	}
	return *this;
}


GrinderKitSetupDialogSettings::~GrinderKitSetupDialogSettings()
{
}


void GrinderKitSetupDialogSettings::importJson(const JSONObject& setupDialogSettingsObj, const UString& currentPathJson)
{
	windowTitle = JsonHelper::readString(setupDialogSettingsObj, currentPathJson, "windowTitle", false, "Demo Setup");
	logoFileName = JsonHelper::readString(setupDialogSettingsObj, currentPathJson, "logo", false, "");
	fontFileName = JsonHelper::readString(setupDialogSettingsObj, currentPathJson, "font", false, "");
	backgroundColor = JsonHelper::readColor(setupDialogSettingsObj, currentPathJson, "backgroundColor", false, ColorGenerator::getColor(ColorRgb::Black));
	startButtonText = JsonHelper::readString(setupDialogSettingsObj, currentPathJson, "startButtonText", false, "Start");
	quitButtonText = JsonHelper::readString(setupDialogSettingsObj, currentPathJson, "quitButtonText", false, "Cancel");
}


JSONValue* GrinderKitSetupDialogSettings::exportJson()
{
	JSONObject obj;

	obj[L"windowTitle"] = JsonHelper::writeString(windowTitle);
	obj[L"logo"] = JsonHelper::writeString(logoFileName);
	obj[L"font"] = JsonHelper::writeString(fontFileName);
	obj[L"backgroundColor"] = JsonHelper::writeColor(backgroundColor, false); // discard alpha
	obj[L"startButtonText"] = JsonHelper::writeString(startButtonText);
	obj[L"quitButtonText"] = JsonHelper::writeString(quitButtonText);

	return new JSONValue(obj);
}


UString GrinderKitSetupDialogSettings::toString(const UString& newLinePadding) const
{
	UString str("{");
	{
		UString newLinePadding2 = newLinePadding + "  ";
		str += "\n" + newLinePadding2 + "\"windowTitle\": \"" + windowTitle + "\"";
		str += "\n" + newLinePadding2 + "\"logoFileName\": \"" + logoFileName + "\"";
		str += "\n" + newLinePadding2 + "\"fontFileName\": \"" + fontFileName + "\"";
		str += "\n" + newLinePadding2 + "\"backgroundColor\": \"" + backgroundColor.toString() + "\"";
		str += "\n" + newLinePadding2 + "\"startButtonText\": \"" + startButtonText + "\"";
		str += "\n" + newLinePadding2 + "\"quitButtonText\": \"" + quitButtonText + "\"";
	}
	str += "\n" + newLinePadding + "}";

	return str;
}


UString GrinderKitSetupDialogSettings::toStringDebug(const UString& newLinePadding) const
{
	UString str("Verso::GrinderKitSetupDialogSettings(\n");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso


