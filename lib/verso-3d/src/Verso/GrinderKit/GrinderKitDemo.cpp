#include <Verso/GrinderKit/GrinderKitDemo.hpp>
#include <Verso/Render/Render.hpp>
#include <Verso/Audio/Audio2dBass.hpp>
#include <Verso/Render/Camera/CameraFps.hpp>
#include <Verso/GrinderKit/GrinderKitSetupDialog.hpp>
#include <Verso/System/assert.hpp>

namespace Verso {


GrinderKitDemo::GrinderKitDemo(const IDemoPartFactory* demoPartFactory, const UString& sourceJsonFileName) :
	created(false),
	demoPartFactory(demoPartFactory),
	sourceJsonFileName(sourceJsonFileName),
	window(nullptr),
	demoPlayer(),
	grinderKitUi(nullptr),

	camera(nullptr),
//	testTexture(),

	shader(),
	imageRect(),

	fbo(nullptr),
//	iChannel0(nullptr),
//	iDepth0(nullptr),
//	iChannel1(nullptr),
//	iChannel2(nullptr),

	quitting(false)
{
}


GrinderKitDemo::GrinderKitDemo(GrinderKitDemo&& original) :
	created(std::move(original.created)),
	demoPartFactory(std::move(original.demoPartFactory)),
	sourceJsonFileName(std::move(original.sourceJsonFileName)),
	window(std::move(original.window)),
	demoPlayer(std::move(original.demoPlayer)),
	grinderKitUi(std::move(original.grinderKitUi)),

	camera(std::move(original.camera)),
//	testTexture(std::move(original.testTexture)),

	shader(std::move(original.shader)),
	imageRect(std::move(original.imageRect)),

	fbo(std::move(original.fbo)),
//	iChannel0(std::move(original.iChannel0)),
//	iDepth0(std::move(original.iDepth0)),
//	iChannel1(std::move(original.iChannel1)),
//	iChannel2(std::move(original.iChannel2)),

	quitting(std::move(original.quitting))
{
}


GrinderKitDemo& GrinderKitDemo::operator =(GrinderKitDemo&& original)
{
	if (this != &original) {
		created = std::move(original.created);
		demoPartFactory = std::move(original.demoPartFactory);
		sourceJsonFileName = std::move(original.sourceJsonFileName);
		window = std::move(original.window);
		demoPlayer = std::move(original.demoPlayer);
		grinderKitUi = std::move(original.grinderKitUi);

		camera = std::move(original.camera);
//		testTexture = std::move(original.testTexture);

		shader = std::move(original.shader);
		imageRect = std::move(original.imageRect);

		fbo = std::move(original.fbo);
//		iChannel0 = std::move(original.iChannel0);
//		iDepth0 = std::move(original.iDepth0);
//		iChannel1 = std::move(original.iChannel1);
//		iChannel2 = std::move(original.iChannel2);

		quitting = std::move(original.quitting);

		// clear original
		original.created = false;
		original.demoPartFactory = nullptr;
		original.window = nullptr;
		original.grinderKitUi = nullptr;
		original.camera = nullptr;
//		original.iChannel0 = nullptr;
//		original.iDepth0 = nullptr;
//		original.iChannel1 = nullptr;
//		original.iChannel2 = nullptr;
	}
	return *this;
}


GrinderKitDemo::~GrinderKitDemo()
{
	GrinderKitDemo::destroy();
}


///////////////////////////////////////////////////////////////////////////////////////////
// IProgramOpengl interface
///////////////////////////////////////////////////////////////////////////////////////////

void GrinderKitDemo::create(IWindowOpengl& window, Audio2d& audio2d)
{
	VERSO_ASSERT_MSG("verso-3d", isCreated() == false, "Already created!");
	VERSO_LOG_INFO("verso-3d", "Demo create ==================================================");
	this->window = &window;

	// Load GrinderKit JSON
	demoPlayer.createFromFile(demoPartFactory, sourceJsonFileName);
	VERSO_LOG_INFO("verso-3d", demoPlayer.toStringDebug());

	// Init Render
	// \TODO: render is not actually based on Windo Opengl intance as it should
	Render::create(window,
				   demoPlayer.settings.demoPaths.pathShaders(),
				   demoPlayer.settings.demoPaths.pathShadersVerso());

	window.getResourceManager().setupDefaultTexture(demoPlayer.settings.demoPaths.pathTextures() + "default.png");

	// Create DemoParts
	demoPlayer.createDemoParts(window, audio2d);

	grinderKitUi = new GrinderKitUi(*this, demoPlayer);
	grinderKitUi->create(window, audio2d);

	camera = new CameraTarget();
	camera->create(&window, "verso-3d/GrinderKitDemo/camera", true);

//	testTexture.createFromFile(demoPlayer.settings.demoPaths.pathTextures()+"backgrounds/test1.jpg",
//							   TextureParameters());

	// Shaders
	Render::quad2dVao.bind();
	shader.createFromFiles(
				demoPlayer.settings.demoPaths.pathShadersVerso()+"grinderkit/imageviewer.330.vert",
				demoPlayer.settings.demoPaths.pathShadersVerso()+"grinderkit/imageviewer.330.frag",
				false);
	shader.bindAttribLocation(ShaderAttribute::Position);
	shader.bindAttribLocation(ShaderAttribute::Uv);
	shader.linkProgram();
	Render::quad2dVao.unbind();

	VERSO_LOG_DEBUG_VARIABLE("verso-3d", "window->getRenderResolutioni()", window.getRenderResolutioni().toString());

	fbo = window.getResourceManager().createFbo(&window, "Verso-3d/GrinderKitDemo/fbo");
	fbo->createColorTexture(TextureParameters(
								"iChannel0",
								TexturePixelFormat::Rgb,
								MinFilter::Nearest, MagFilter::Nearest,
								WrapStyle::ClampToEdge, WrapStyle::ClampToEdge));
	fbo->createDepthTexture(TextureParameters(
								"iDepth",
								TexturePixelFormat::DepthComponent,
								MinFilter::Nearest, MagFilter::Nearest,
								WrapStyle::ClampToEdge, WrapStyle::ClampToEdge));
	if (fbo->isComplete() == false) {
		VERSO_ERROR("verso-3d", "FBO is incomplete", "");
	}

	if (demoPlayer.settings.debug.playMode == PlayMode::Development) {
		window.setCursorShown(true);
	}
	else {
		window.setCursorShown(false);
	}

	created = true;

	reset(window);

	VERSO_LOG_INFO("verso-3d", "Demo create DONE =============================================");
}


void GrinderKitDemo::reset(IWindowOpengl& window)
{
	VERSO_ASSERT_MSG("verso-3d", isCreated() == true, "IGrinderKitApp must be created before calling reset()");

	camera->reset(Vector3f(0.0f, 0.0f, 1.0f), Vector3f::up());
	camera->setTarget(Vector3f(0.0f, 0.0f, 0.0f));
	camera->setPosition(Vector3f(0.0f, 0.0f, -3.0f));
	camera->setProjectionOrthographic(Rangef(0.001f, 10.0f));

	demoPlayer.startDemo(window);
}


void GrinderKitDemo::destroy() VERSO_NOEXCEPT
{
	if (GrinderKitDemo::isCreated() == false) {
		return;
	}

	demoPlayer.stopDemo();

	VERSO_LOG_INFO("verso-3d", "Demo destroy =================================================");

	shader.destroy();

	//	testTexture.destroy();

	if (camera != nullptr) {
		camera->destroy();
		delete camera;
		camera = nullptr;
	}

	delete grinderKitUi;
	grinderKitUi = nullptr;

	Render::destroy();

	demoPlayer.destroy();

	this->window = nullptr;
	created = false;
	VERSO_LOG_INFO("verso-3d", "Demo destroy DONE ============================================");
}


bool GrinderKitDemo::isCreated() const
{
	return created;
}


void GrinderKitDemo::handleInput(IWindowOpengl& window, const FrameTimestamp& time)
{
	demoPlayer.handleInput(window, time);
	grinderKitUi->handleInput(window, time);

	if (demoPlayer.isDemoOver() == true) {
		quit();
	}
}


void GrinderKitDemo::handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event)
{
	if (event.type == EventType::Resized) {
		// \TODO: some old attempt of resizing
//		window.onResize(event.size);
//		destroyRenderFbo();
//		createRenderFbo();
	}

	demoPlayer.handleEvent(window, time, event);
	grinderKitUi->handleEvent(window, time, event);
}


void GrinderKitDemo::render(IWindowOpengl& window, const FrameTimestamp& time)
{
	Audio2dBass::instance().update();

	fbo->bind();
	demoPlayer.render(window, time);
	fbo->unbind();

	window.resetRelativeViewport();
	window.applyDrawableViewport();

	if (grinderKitUi->getEnableUi() == false) {
		Render::clearScreen(ClearFlag::SolidColor, demoPlayer.getDemoPlayerSettings().general.borderColor);
		Opengl::blendTranscluent();
		Render::draw2dDrawableResolution(
					window, *camera, *fbo->getColorDrawTexture(), Vector3f(0.5f, 0.5f),
					Align(HAlign::Center, VAlign::Center),
					Vector2f(1.0f, 1.0f),
					RefScale::ViewportSize_KeepAspectRatio_FitRect, 1.0f, 0.0f, true, true);
	}
	else {
		grinderKitUi->updateRenderTexture(fbo->getColorReadTexture());
		grinderKitUi->render(window, time);
	}
}


bool GrinderKitDemo::isQuitting() const
{
	return quitting;
}


void GrinderKitDemo::quit()
{
	quitting = true;
}


///////////////////////////////////////////////////////////////////////////////////////////
// IGrinderKitApp interface
///////////////////////////////////////////////////////////////////////////////////////////

UString GrinderKitDemo::getSourceJsonFileName() const
{
	return sourceJsonFileName;
}


const DemoPlayerSettings& GrinderKitDemo::getDemoPlayerSettings() const
{
	return demoPlayer.getDemoPlayerSettings();
}


bool GrinderKitDemo::saveRenderFboToFile(const UString& fileName, const ImageSaveFormat& imageSaveFormat) const
{
	Texture* tex = fbo->getColorReadTexture(0);
	if (tex != nullptr) {
		tex->bind();
		bool result = tex->saveToFile(fileName, imageSaveFormat);
		tex->unbind();
		return result;
	}
	else {
		return false;
	}
}


} // End namespace Verso

