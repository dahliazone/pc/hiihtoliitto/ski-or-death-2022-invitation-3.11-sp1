#include <Verso/GrinderKit/DemoParts/ClearScreen.hpp>
#include <Verso/System/JsonHelper3d.hpp>
#include <Verso/Render/Render.hpp>

namespace Verso {


ClearFlag clearFlagDefault = ClearFlag::SolidColor;
RgbaColorfKeyframes colorKeyframesDefault = RgbaColorfKeyframes(ColorGenerator::getColor(ColorRgb::Black));


ClearScreen::ClearScreen(const DemoPaths* demoPaths, const JSONObject& demoPartJson,
						 const UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator) :
	DemoPart(demoPaths, demoPartJson, currentPathJson, jsonOrder, startAccumulator),
	clearParam(clearFlagDefault, colorKeyframesDefault),

	created(false),
	quitting(false)
{
	const JSONObject& paramsObj = JsonHelper::readObject(demoPartJson, currentPathJson, "params");

	clearParam.parseAttributeAsObject(paramsObj, currentPathJson+"params.", "clear", false, ClearParam(clearFlagDefault, colorKeyframesDefault));
}


ClearScreen::ClearScreen(ClearScreen&& original) noexcept :
	DemoPart(std::move(original)),
	clearParam(std::move(original.clearParam)),

	created(std::move(original.created)),
	quitting(std::move(original.quitting))
{
}

ClearScreen& ClearScreen::operator =(ClearScreen&& original) noexcept
{
	if (this != &original) {
		clearParam = std::move(original.clearParam);

		created = std::move(original.created);
		quitting = std::move(original.quitting);
	}
	return *this;
}


ClearScreen::~ClearScreen()
{
	ClearScreen::destroy();
}


///////////////////////////////////////////////////////////////////////////////////////////
// IProgramOpengl interface
///////////////////////////////////////////////////////////////////////////////////////////
void ClearScreen::create(IWindowOpengl& window, Audio2d& audio2d)
{
	(void)audio2d;
	VERSO_ASSERT_MSG("verso-3d", isCreated() == false, "Already created!");

	created = true;

	reset(window);
}


void ClearScreen::reset(IWindowOpengl& window)
{
	(void)window;
	VERSO_ASSERT_MSG("verso-3d", isCreated() == true, "DemoPart must be created before calling reset()");
}


void ClearScreen::destroy() VERSO_NOEXCEPT
{
	if (ClearScreen::isCreated() == false) {
		return;
	}

	created = false;
}


bool ClearScreen::isCreated() const
{
	return created;
}


void ClearScreen::handleInput(IWindowOpengl& window, const FrameTimestamp& time)
{
	(void)window; (void)time;
}


void ClearScreen::handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event)
{
	(void)window; (void)time; (void)event;
}


void ClearScreen::render(IWindowOpengl& window, const FrameTimestamp& time)
{
	(void)window;

	float seconds = static_cast<float>(time.getElapsed().asSeconds());
	Render::clearScreen(clearParam.clearFlag, clearParam.colorKeyframes.getValueInterpolated(seconds));
}


bool ClearScreen::isQuitting() const
{
	return quitting;
}


void ClearScreen::quit()
{
	quitting = true;
}


} // End namespace Verso

