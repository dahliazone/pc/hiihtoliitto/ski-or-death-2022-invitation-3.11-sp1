#include <Verso/GrinderKit/DemoParts/Text.hpp>
#include <Verso/System/JsonHelper3d.hpp>
#include <Verso/Render/Render.hpp>
#include <cmath>

namespace Verso {


const ClearParam defaultClearParam;
const DepthParam defaultDepthParam(false, false);
const BlendMode defaultBlendMode = BlendMode::Transcluent;
const UString defaultText("Placeholder text...\nLorem ipsum dolor sit amet,\nconsectetur adipiscing elit.\nPraesent aliquet quam quam,\nut suscipit ante dignissim a.");
const Vector2f defaultRelativePosition(0.5f, 0.5f);
const Vector2f defaultRelativeCharacterSize(0.08f, 0.08f);
const float defaultAlpha = 1.0f;
const Vector2f defaultRelativeSpacing = Vector2f(0.01f, 0.01f);
const RefScale defaultRefScale = RefScale::ViewportSize_KeepAspectRatio_FromX;
const Degree defaultAngleZ = 0.0f;
const float defaultRevealCharactersPerSecond = 0.0f;


Text::Text(
		const DemoPaths* demoPaths, const JSONObject& demoPartJson,
		const UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator) :
	DemoPart(demoPaths, demoPartJson, currentPathJson, jsonOrder, startAccumulator),
	clearParam(defaultClearParam),
	depthParam(defaultDepthParam),
	blendMode(defaultBlendMode),
	fontParam(),
	text(defaultText),
	relativePositionKeyframes(defaultRelativePosition),
	alphaKeyframes(defaultAlpha),
	relativeCharacterSize(defaultRelativeCharacterSize),
	relativeSpacing(defaultRelativeSpacing),
	refScale(defaultRefScale),
	angleZKeyframes(defaultAngleZ),
	revealCharactersPerSecond(),

	created(false),
	quitting(false),
	window(nullptr),
	camera(),
	font(nullptr),
	textToBeRendered("")
{
	const JSONObject& paramsObj = JsonHelper::readObject(demoPartJson, currentPathJson, "params");

	UString paramsCurrentPathJson(currentPathJson+"params.");

	clearParam.parseAttributeAsObject(paramsObj, paramsCurrentPathJson, "clear", false, defaultClearParam);

	depthParam.parseAttributeAsObject(paramsObj, paramsCurrentPathJson, "depth", false, defaultDepthParam);

	blendMode = JsonHelper::readBlendMode(paramsObj, paramsCurrentPathJson, "blend", false, defaultBlendMode);

	fontParam.parseAttributeAsObject(paramsObj, paramsCurrentPathJson);

	if (JsonHelper::isArray(paramsObj, paramsCurrentPathJson, "text")) {
		JSONArray textParts = JsonHelper::readArray(paramsObj, paramsCurrentPathJson, "text", false);
		if (textParts.size() > 0) {
			text.clear();
			for (JSONArray::const_iterator channelIt = textParts.begin(); channelIt != textParts.end(); ++channelIt) {
				if ((*channelIt) && (*channelIt)->IsString()) {
					text.append((*channelIt)->AsString());
				}
			}
		}
	}
	else {
		text = JsonHelper::readString(paramsObj, paramsCurrentPathJson, "text", false, defaultText);
	}

	relativePositionKeyframes = JsonHelper::readVector2fKeyframes(paramsObj, paramsCurrentPathJson, "relativePosition", false, Vector2fKeyframes(defaultRelativePosition));

	alphaKeyframes = JsonHelper::readFloatKeyframes(paramsObj, paramsCurrentPathJson, "alpha", false, FloatKeyframes(defaultAlpha));

	relativeCharacterSize = JsonHelper::readVector2fArrayFormat(paramsObj, paramsCurrentPathJson, "relativeCharacterSize", false, defaultRelativeCharacterSize);

	relativeSpacing = JsonHelper::readVector2fArrayFormat(paramsObj, paramsCurrentPathJson, "relativeSpacing", false, defaultRelativeSpacing);

	refScale = JsonHelper::readRefScale(paramsObj, paramsCurrentPathJson, "refScale", false, defaultRefScale);

	angleZKeyframes = JsonHelper::readFloatKeyframes(paramsObj, paramsCurrentPathJson, "angleZ", false, FloatKeyframes(defaultAngleZ));

	revealCharactersPerSecond = JsonHelper::readNumberf(paramsObj, paramsCurrentPathJson, "revealCharactersPerSecond", false, defaultRevealCharactersPerSecond);
}


Text::Text(Text&& original) noexcept :
	DemoPart(std::move(original)),
	clearParam(std::move(original.clearParam)),
	depthParam(std::move(original.depthParam)),
	blendMode(std::move(original.blendMode)),
	fontParam(std::move(original.fontParam)),
	text(std::move(original.text)),
	relativePositionKeyframes(std::move(original.relativePositionKeyframes)),
	alphaKeyframes(std::move(original.alphaKeyframes)),
	relativeCharacterSize(std::move(original.relativeCharacterSize)),
	relativeSpacing(std::move(original.relativeSpacing)),
	refScale(std::move(original.refScale)),
	angleZKeyframes(std::move(original.angleZKeyframes)),
	revealCharactersPerSecond(std::move(original.revealCharactersPerSecond)),

	created(std::move(original.created)),
	quitting(std::move(original.quitting)),
	window(std::move(original.window)),
	camera(std::move(original.camera)),
	font(std::move(original.font)),
	textToBeRendered(std::move(original.textToBeRendered))
{
	original.window = nullptr;
	original.font = nullptr;
}


Text::~Text()
{
	Text::destroy();
}


Text& Text::operator =(Text&& original) noexcept
{
	if (this != &original) {
		clearParam = std::move(original.clearParam);
		depthParam = std::move(original.depthParam);
		blendMode = std::move(original.blendMode);
		fontParam = std::move(original.fontParam);
		text = std::move(original.text);
		relativePositionKeyframes = std::move(original.relativePositionKeyframes);
		alphaKeyframes = std::move(original.alphaKeyframes);
		relativeCharacterSize = std::move(original.relativeCharacterSize);
		relativeSpacing = std::move(original.relativeSpacing);
		refScale = std::move(original.refScale);
		angleZKeyframes = std::move(original.angleZKeyframes);
		revealCharactersPerSecond = std::move(original.revealCharactersPerSecond);

		created = std::move(original.created);
		quitting = std::move(original.quitting);
		window = std::move(original.window);
		camera = std::move(original.camera);
		font = std::move(original.font);
		textToBeRendered = std::move(original.textToBeRendered);

		original.window = nullptr;
		original.font = nullptr;
	}
	return *this;
}


///////////////////////////////////////////////////////////////////////////////////////////
// IProgramOpengl interface
///////////////////////////////////////////////////////////////////////////////////////////

void Text::create(IWindowOpengl& window, Audio2d& audio2d)
{
	(void)audio2d;
	VERSO_ASSERT_MSG("verso-3d", isCreated() == false, "Already created!");
	this->window = &window;

	// Setup camera
	camera.create(&window, "GrinderKit/Text/camera." + getInstanceStr(), false);

	font = new BitmapFontFixedWidth();
	font->createFromFile(
				window, demoPaths->pathFonts() + fontParam.texture.sourceFileName,
				fontParam.texture.minFilter,
				fontParam.texture.magFilter,
				Vector2f(1.0f / 16.0f, 1.0f / 16.0f),
				fontParam.firstFontCharacterIndex,
				fontParam.tabSize);

	created = true;

	reset(window);
}


void Text::reset(IWindowOpengl& window)
{
	(void)window;
	VERSO_ASSERT_MSG("verso-3d", isCreated() == true, "DemoPart must be created before calling reset()");

	camera.reset(Vector3f(0.0f, 0.0f, 1.0f), Vector3f::up());
	camera.setProjectionOrthographic(Rangef(0.001f, 10.0f));
	camera.setTarget(Vector3f(0.0f, 0.0f, 0.0f));
	camera.setPosition(Vector3f(0.0f, 0.0f, -9.9f));

	if (revealCharactersPerSecond == 0.0f) {
		textToBeRendered = text;
	}
	else {
		textToBeRendered = "";
	}
	font->updateText(textToBeRendered, relativeSpacing, true);
}


void Text::destroy() noexcept
{
	if (Text::isCreated() == false) {
		return;
	}

	camera.destroy();

	delete font;
	font = nullptr;

	created = false;
}


bool Text::isCreated() const
{
	return created;
}


void Text::handleInput(IWindowOpengl& window, const FrameTimestamp& time)
{
	(void)window;

	if (revealCharactersPerSecond == 0.0f) {
		textToBeRendered = text;
	}
	else {
		int maxSize = static_cast<int>(text.size());
		int revealCharAmount = static_cast<int>(floor(revealCharactersPerSecond * time.getElapsed().asSeconds()));
		if (revealCharAmount > maxSize) {
			revealCharAmount = maxSize;
		}
		if (revealCharAmount > 0) {
			textToBeRendered = text.substring(0, revealCharAmount);
		}
		else {
			textToBeRendered = "";
		}
	}

	font->updateText(textToBeRendered, relativeSpacing, true);
}


void Text::handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event)
{
	(void)window; (void)time; (void)event;
}


void Text::render(IWindowOpengl& window, const FrameTimestamp& time)
{
	float seconds = static_cast<float>(time.getElapsed().asSeconds());

	Render::clearScreen(
				clearParam.clearFlag,
				clearParam.colorKeyframes.getValueInterpolated(seconds));

	Opengl::depthTest(depthParam.depthTest);
	Opengl::depthWrite(depthParam.depthWrite);

	Opengl::blend(blendMode);
	Opengl::setPolygonRenderMode(PolygonRenderMode::Fill);

	const Vector3f point(relativePositionKeyframes.getValueInterpolated(seconds));

	font->renderText(window, camera,
					 point,
					 relativeCharacterSize,
					 refScale,
					 alphaKeyframes.getValueInterpolated(seconds),
					 angleZKeyframes.getValueInterpolated(seconds));

	Opengl::blend(BlendMode::None);
}


bool Text::isQuitting() const
{
	return quitting;
}


void Text::quit()
{
	quitting = true;
}


} // End namespace Verso

