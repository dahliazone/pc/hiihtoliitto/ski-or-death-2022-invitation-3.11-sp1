#include <Verso/GrinderKit/DemoParts/Particles.hpp>
#include <Verso/Render/Opengl.hpp>
#include <Verso/Render/VaoGenerator/VaoGenerator.hpp>
#include <Verso/Render/Camera/CameraFps.hpp>
#include <Verso/Render/Render.hpp>
#include <Verso/System/JsonHelper3d.hpp>

namespace Verso {


const ClearParam defaultClearParam;
const DepthParam defaultDepthParam(false, true);
const BlendMode defaultBlendMode = BlendMode::Additive;


Particles::Particles(const DemoPaths* demoPaths, const JSONObject& demoPartJson,
					 const UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator) :
	DemoPart(demoPaths, demoPartJson, currentPathJson, jsonOrder, startAccumulator),
	// Params
	clearParam(defaultClearParam),
	depthParam(defaultDepthParam),
	blendMode(defaultBlendMode),
	particlesParam(),
	behaviourParam(),
	emitterParam(),
	sequencerParam(),

	// State
	created(false),
	quitting(false),
	particleBehaviour(nullptr),
	particleSystem(nullptr),
	particleEmitter(nullptr),
	particleSequencer(nullptr)
{
	const JSONObject& paramsObj = JsonHelper::readObject(demoPartJson, currentPathJson, "params");

	UString paramsCurrentPathJson(currentPathJson+"params.");

	clearParam.parseAttributeAsObject(paramsObj, paramsCurrentPathJson, "clear", false, defaultClearParam);

	depthParam.parseAttributeAsObject(paramsObj, paramsCurrentPathJson, "depth", false, defaultDepthParam);

	blendMode = JsonHelper::readBlendMode(paramsObj, paramsCurrentPathJson, "blend", false, defaultBlendMode);

	particlesParam.parseAttributeAsObject(paramsObj, paramsCurrentPathJson);

	behaviourParam.parseAttributeAsObject(paramsObj, paramsCurrentPathJson);

	emitterParam.parseAttributeAsObject(paramsObj, paramsCurrentPathJson);

	sequencerParam.parseAttributeAsObject(paramsObj, paramsCurrentPathJson);
}


Particles::Particles(Particles&& original) noexcept :
	DemoPart(std::move(original)),
	clearParam(std::move(original.clearParam)),
	depthParam(std::move(original.depthParam)),
	blendMode(std::move(original.blendMode)),
	particlesParam(std::move(original.particlesParam)),
	behaviourParam(std::move(original.behaviourParam)),
	emitterParam(std::move(original.emitterParam)),
	sequencerParam(std::move(original.sequencerParam)),

	created(std::move(original.created)),
	quitting(std::move(original.quitting)),
	particleBehaviour(std::move(original.particleBehaviour)),
	particleSystem(std::move(original.particleSystem)),
	particleEmitter(std::move(original.particleEmitter)),
	particleSequencer(std::move(original.particleSequencer))
{
	original.particleBehaviour = nullptr;
	original.particleSystem = nullptr;
	original.particleEmitter = nullptr;
	original.particleSequencer = nullptr;
}


Particles::~Particles()
{
	Particles::destroy();
}


Particles& Particles::operator =(Particles&& original) noexcept
{
	if (this != &original) {
		clearParam = std::move(original.clearParam);
		depthParam = std::move(original.depthParam);
		blendMode = std::move(original.blendMode);
		particlesParam = std::move(original.particlesParam);
		behaviourParam = std::move(original.behaviourParam);
		emitterParam = std::move(original.emitterParam);
		sequencerParam = std::move(original.sequencerParam);

		created = std::move(original.created);
		quitting = std::move(original.quitting);
		particleBehaviour = std::move(original.particleBehaviour);
		particleSystem = std::move(original.particleSystem);
		particleEmitter = std::move(original.particleEmitter);
		particleSequencer = std::move(original.particleSequencer);

		original.particleBehaviour = nullptr;
		original.particleSystem = nullptr;
		original.particleEmitter = nullptr;
		original.particleSequencer = nullptr;
	}
	return *this;
}


///////////////////////////////////////////////////////////////////////////////////////////
// IProgramOpengl interface
///////////////////////////////////////////////////////////////////////////////////////////
void Particles::create(IWindowOpengl& window, Audio2d& audio2d)
{
	(void)audio2d;
	VERSO_ASSERT_MSG("verso-3d", isCreated() == false, "Already created!");

	particleBehaviour = behaviourParam.createBehaviour();

	particleSystem = particlesParam.createParticleSystem(window, "TODO");
	particleSystem->setParticleBehaviour(particleBehaviour);

	particleEmitter = emitterParam.createEmitter();
	particleEmitter->setParticleSystem(particleSystem);

	particleSequencer = sequencerParam.createParticleSequencer();
	particleSequencer->setParticleEmitter(particleEmitter);

	created = true;

	reset(window);
}


void Particles::reset(IWindowOpengl& window)
{
	(void)window;
	VERSO_ASSERT_MSG("verso-3d", isCreated() == true, "DemoPart must be created before calling reset()");

	particleBehaviour->reset();
	particleSystem->reset();
	particleEmitter->reset();
	particleSequencer->reset();

}


void Particles::destroy() VERSO_NOEXCEPT
{
	if (Particles::isCreated() == false) {
		return;
	}

	delete particleSequencer;
	particleSequencer = nullptr;

	delete particleEmitter;
	particleEmitter = nullptr;

	delete particleSystem;
	particleSystem = nullptr;

	delete particleBehaviour;
	particleBehaviour = nullptr;

	created = false;
}


bool Particles::isCreated() const
{
	return created;
}


void Particles::handleInput(IWindowOpengl& window, const FrameTimestamp& time)
{
	(void)window; (void)time;

	particleSequencer->sequence(time);
	particleSystem->calculate(time);
}


void Particles::handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event)
{
	(void)window; (void)time; (void)event;
}


void Particles::render(IWindowOpengl& window, const FrameTimestamp& time)
{
	(void)window;
	float seconds = static_cast<float>(time.getElapsed().asSeconds());

	Render::clearScreen(
				clearParam.clearFlag,
				clearParam.colorKeyframes.getValueInterpolated(seconds));

	Opengl::depthTest(depthParam.depthTest);
	Opengl::depthWrite(depthParam.depthWrite);

	Opengl::blend(blendMode);

	// \TODO: implement Particles::DemoPart
//	particleBehaviour->reset();
//	particleSystem->reset();
//	particleEmitter->reset();
//	particleSequencer->reset();

	//particleSystem->render(window, camera);

	Opengl::blend(BlendMode::None);
}


bool Particles::isQuitting() const
{
	return quitting;
}


void Particles::quit()
{
	quitting = true;
}


} // End namespace Verso

