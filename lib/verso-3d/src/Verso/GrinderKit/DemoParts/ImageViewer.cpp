#include <Verso/GrinderKit/DemoParts/ImageViewer.hpp>
#include <Verso/Render/Opengl.hpp>
#include <Verso/Render/VaoGenerator/VaoGenerator.hpp>
#include <Verso/Render/Camera/CameraFps.hpp>
#include <Verso/Render/Render.hpp>
#include <Verso/System/JsonHelper3d.hpp>

namespace Verso {


const ClearParam defaultClearParam;
const DepthParam defaultDepthParam(false, false);

const Vector3f defaultCameraPosition(0.0f, 0.0f, -9.9f);
const Vector3f defaultCameraTarget(0.0f, 0.0f, 0.0f);
const Vector3f defaultCameraDesiredUp(Vector3f::up());
const float defaultCameraFov(90.0f);
const Rangef defaultCameraNearFarPlane(0.001f, 10.0f);
const CameraParam defaultCameraParam(
		CameraType::Target, ProjectionType::Orthographic,
		Vector3fKeyframes(defaultCameraPosition), Vector3fKeyframes(defaultCameraTarget),
		Vector3fKeyframes(defaultCameraDesiredUp),
		FloatKeyframes(defaultCameraFov), defaultCameraNearFarPlane);

const BlendMode defaultBlendMode = BlendMode::Transcluent;
const Vector2f defaultPosition(0.5f, 0.5f);
const Align defaultAlign(HAlign::Center, VAlign::Center);
const Vector2f defaultRelSize(1.0f, 1.0f);
const RefScale defaultRefScale(RefScale::ViewportSize_KeepAspectRatio_FitRect);
const float defaultAngle(0.0f);
const float defaultAlpha(1.0f);

const Verso::TextureParam defaultTextureParam(
		"", "", Verso::TexturePixelFormat::Unset,
		Verso::MinFilter::Linear, Verso::MagFilter::Linear,
		Verso::WrapStyle::ClampToEdge, Verso::WrapStyle::ClampToEdge);


ImageViewer::ImageViewer(const DemoPaths* demoPaths, const JSONObject& demoPartJson,
						 const UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator) :
	DemoPart(demoPaths, demoPartJson, currentPathJson, jsonOrder, startAccumulator),
	clearParam(defaultClearParam),
	depthParam(defaultDepthParam),
	cameraParam(defaultCameraParam),
	blendMode(defaultBlendMode),
	textureParam(defaultTextureParam),
	positionKeyframes(),
	alignFromPosition(),
	relSizeKeyframes(defaultRelSize),
	refScale(defaultRefScale),
	rotationAngleKeyframes(defaultAngle),
	alphaKeyframes(defaultAlpha),

	created(false),
	quitting(false),
	texture(),
	camera()
{
	const JSONObject& paramsObj = JsonHelper::readObject(demoPartJson, currentPathJson, "params");

	UString paramsCurrentPathJson(currentPathJson+"params.");

	clearParam.parseAttributeAsObject(paramsObj, paramsCurrentPathJson, "clear", false, defaultClearParam);

	depthParam.parseAttributeAsObject(paramsObj, paramsCurrentPathJson, "depth", false, defaultDepthParam);

	const JSONObject& cameraObj = JsonHelper::readObject(paramsObj, paramsCurrentPathJson, "camera");
	if (cameraObj != JsonHelper::emptyJsonObject) {
		UString subCurrentPathJson(paramsCurrentPathJson + "camera.");
		if (cameraParam.parseObject(cameraObj, subCurrentPathJson, defaultCameraParam) == false) {
			UString error("Cannot read camera params from field \""+subCurrentPathJson+"\"!.");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}
	}

	blendMode = JsonHelper::readBlendMode(paramsObj, paramsCurrentPathJson, "blend", false, defaultBlendMode);

	textureParam.parseAttributeAsObject(paramsObj, paramsCurrentPathJson, "texture", true, defaultTextureParam);

	positionKeyframes = JsonHelper::readVector2fKeyframes(paramsObj, paramsCurrentPathJson, "position", false, Vector2fKeyframes(defaultPosition));

	alignFromPosition = JsonHelper::readAlign(paramsObj, paramsCurrentPathJson, "alignFromPosition", false, defaultAlign);

	relSizeKeyframes = JsonHelper::readVector2fKeyframes(paramsObj, paramsCurrentPathJson, "relSize", false, Vector2fKeyframes(defaultRelSize));

	refScale = JsonHelper::readRefScale(paramsObj, paramsCurrentPathJson, "refScale", false, defaultRefScale);

	rotationAngleKeyframes = JsonHelper::readFloatKeyframes(paramsObj, paramsCurrentPathJson, "angle", false, FloatKeyframes(defaultAngle));

	alphaKeyframes = JsonHelper::readFloatKeyframes(paramsObj, paramsCurrentPathJson, "alpha", false, FloatKeyframes(defaultAlpha));
}


ImageViewer::ImageViewer(ImageViewer&& original) noexcept :
	DemoPart(std::move(original)),
	clearParam(std::move(original.clearParam)),
	depthParam(std::move(original.depthParam)),
	cameraParam(std::move(original.cameraParam)),
	blendMode(std::move(original.blendMode)),
	textureParam(std::move(original.textureParam)),
	positionKeyframes(std::move(original.positionKeyframes)),
	alignFromPosition(std::move(original.alignFromPosition)),
	relSizeKeyframes(std::move(original.relSizeKeyframes)),
	refScale(std::move(original.refScale)),
	rotationAngleKeyframes(std::move(original.rotationAngleKeyframes)),
	alphaKeyframes(std::move(original.alphaKeyframes)),

	created(std::move(original.created)),
	quitting(std::move(original.quitting)),
	texture(std::move(original.texture)),
	camera(std::move(original.camera))
{
	// reset original
}


ImageViewer& ImageViewer::operator =(ImageViewer&& original) noexcept
{
	if (this != &original) {
		clearParam = std::move(original.clearParam);
		depthParam = std::move(original.depthParam);
		cameraParam = std::move(original.cameraParam);
		blendMode = std::move(original.blendMode);
		textureParam = std::move(original.textureParam);
		positionKeyframes = std::move(original.positionKeyframes);
		alignFromPosition = std::move(original.alignFromPosition);
		relSizeKeyframes = std::move(original.relSizeKeyframes);
		refScale = std::move(original.refScale);
		rotationAngleKeyframes = std::move(original.rotationAngleKeyframes);
		alphaKeyframes = std::move(original.alphaKeyframes);

		created = std::move(original.created);
		quitting = std::move(original.quitting);
		texture = std::move(original.texture);
		camera = std::move(original.camera);

		// reset original
	}
	return *this;
}


ImageViewer::~ImageViewer()
{
	ImageViewer::destroy();
}


///////////////////////////////////////////////////////////////////////////////////////////
// IProgramOpengl interface
///////////////////////////////////////////////////////////////////////////////////////////
void ImageViewer::create(IWindowOpengl& window, Audio2d& audio2d)
{
	(void)audio2d;
	VERSO_ASSERT_MSG("verso-3d", isCreated() == false, "Already created!");

	// Texture
	texture.createFromFile(
				window,
				demoPaths->pathTextures()+textureParam.sourceFileName, TextureParameters("iChannel0"));
	texture.setWrapStyle(textureParam.wrapStyleS, textureParam.wrapStyleT);
	texture.setMinFilter(textureParam.minFilter);
	texture.setMagFilter(textureParam.magFilter);

	// Setup camera
	camera.create(&window, "GrinderKit/ImageViewer/camera." + getInstanceStr(), false);

	created = true;

	reset(window);
}


void ImageViewer::reset(IWindowOpengl& window)
{
	(void)window;
	VERSO_ASSERT_MSG("verso-3d", isCreated() == true, "DemoPart must be created before calling reset()");

	camera.set(cameraParam);
}


void ImageViewer::destroy() VERSO_NOEXCEPT
{
	if (ImageViewer::isCreated() == false) {
		return;
	}

	texture.destroy();

	created = false;
}


bool ImageViewer::isCreated() const
{
	return created;
}


void ImageViewer::handleInput(IWindowOpengl& window, const FrameTimestamp& time)
{
	(void)window;

	float seconds = static_cast<float>(time.getElapsed().asSeconds());

	camera.setPosition(cameraParam.positionKeyframes.getValueInterpolated(seconds));
	camera.setTarget(cameraParam.targetKeyframes.getValueInterpolated(seconds));
	camera.setDesiredUp(cameraParam.desiredUpKeyframes.getValueInterpolated(seconds));
	if (cameraParam.projectionType == ProjectionType::Perspective) {
		camera.setFovY(cameraParam.fovYKeyframes.getValueInterpolated(seconds));
	}
	else if (cameraParam.projectionType == ProjectionType::Orthographic) {
		camera.setOrthographicZoomLevel(cameraParam.orthographicZoomLevelKeyframes.getValueInterpolated(seconds));
		camera.setOrthographicRotation(cameraParam.orthographicRotationKeyframes.getValueInterpolated(seconds));
	}
}


void ImageViewer::handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event)
{
	(void)window; (void)time; (void)event;
}


void ImageViewer::render(IWindowOpengl& window, const FrameTimestamp& time)
{
	float seconds = static_cast<float>(time.getElapsed().asSeconds());

	Render::clearScreen(
				clearParam.clearFlag,
				clearParam.colorKeyframes.getValueInterpolated(seconds));

	Opengl::depthTest(depthParam.depthTest);
	Opengl::depthWrite(depthParam.depthWrite);

	Opengl::blend(blendMode);
	Opengl::setPolygonRenderMode(PolygonRenderMode::Fill);

	Render::draw2d(
				window, camera, texture,
				positionKeyframes.getValueInterpolated(seconds),
				alignFromPosition,
				relSizeKeyframes.getValueInterpolated(seconds),
				refScale,
				alphaKeyframes.getValueInterpolated(seconds),
				rotationAngleKeyframes.getValueInterpolated(seconds));

	Opengl::blend(BlendMode::None);
}


bool ImageViewer::isQuitting() const
{
	return quitting;
}


void ImageViewer::quit()
{
	quitting = true;
}


///////////////////////////////////////////////////////////////////////////////////////////
// toString
///////////////////////////////////////////////////////////////////////////////////////////

UString ImageViewer::toString(const UString& newLinePadding) const
{
	UString str("{ ");
	UString newLinePadding2 = newLinePadding + "  ";
	str += "\n" + newLinePadding2 + "params: {";
	{
		UString newLinePadding3 = newLinePadding2 + "  ";
		str += "\n" + newLinePadding3 + "textureParam=" + textureParam.toString(newLinePadding3);
		str += ",\n" + newLinePadding3 + "clearParam=" + clearParam.toString(newLinePadding3);
		str += ",\n" + newLinePadding3 + "depthParam=" + depthParam.toString(newLinePadding3);
		str += ",\n" + newLinePadding3 + "cameraParam=" + cameraParam.toString(newLinePadding3);
		str += ",\n" + newLinePadding3 + "blendMode=" + blendModeToString(blendMode);
		str += ",\n" + newLinePadding3 + "positionKeyframes=" + positionKeyframes.toString(newLinePadding3);
		str += ",\n" + newLinePadding3 + "alignFromPosition=" + alignFromPosition.toString();
		str += ",\n" + newLinePadding3 + "relSizeKeyframes=" + relSizeKeyframes.toString(newLinePadding3);
		str += ",\n" + newLinePadding3 + "refScale=" + refScaleToString(refScale);
		str += ",\n" + newLinePadding3 + "rotationAngleKeyframes=" + rotationAngleKeyframes.toString(newLinePadding3);
		str += ",\n" + newLinePadding3 + "alphaKeyframes=" + alphaKeyframes.toString(newLinePadding3);
	}
	str += ",\n" + newLinePadding2 + "}";

	str += ",\n" + newLinePadding2 + "state: {";
	{
		UString newLinePadding3 = newLinePadding2 + "  ";
		str += "\n" + newLinePadding3 + "created=";
		if (created == true) {
			str += "true";
		}
		else {
			str += "false";
		}
		str += ",\n" + newLinePadding3 + "quitting=";
		if (quitting == true) {
			str += "true";
		}
		else {
			str += "false";
		}
		str += ",\n" + newLinePadding3 + "texture=" + texture.toString(newLinePadding3);
		str += ",\n" + newLinePadding3 + "camera=" + camera.toString(newLinePadding3);
	}
	str += ",\n" + newLinePadding + "}";

	return str;
}


UString ImageViewer::toStringDebug(const UString& newLinePadding) const
{
	UString str("ImageViewer(\n");
	str += toString(newLinePadding);
	str += "\n)";
	return str;
}


} // End namespace Verso

