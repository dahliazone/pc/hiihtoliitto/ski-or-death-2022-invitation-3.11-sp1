#include <Verso/GrinderKit/DemoParts/ImGuiTest.hpp>
#include <Verso/Gui/ImGuiVerso3dImpl.hpp>
#include <Verso/Render/Render.hpp>

namespace Verso {


const ClearParam defaultClearParam;
const DepthParam defaultDepthParam(false, true);


ImGuiTest::ImGuiTest(
		const DemoPaths* demoPaths, const JSONObject& demoPartJson,
		const UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator) :
	DemoPart(demoPaths, demoPartJson, currentPathJson, jsonOrder, startAccumulator),
	clearParam(defaultClearParam),
	depthParam(defaultDepthParam),

	created(false),
	quitting(false),
	showTestWindow(false),
	showAnotherWindow(false),
	bgColor()
{
	const JSONObject& paramsObj = JsonHelper::readObject(demoPartJson, currentPathJson, "params");

	UString paramsCurrentPathJson(currentPathJson+"params.");

	clearParam.parseAttributeAsObject(paramsObj, paramsCurrentPathJson, "clear", false, defaultClearParam);

	depthParam.parseAttributeAsObject(paramsObj, paramsCurrentPathJson, "depth", false, defaultDepthParam);
}


ImGuiTest::ImGuiTest(ImGuiTest&& original) :
	DemoPart(std::move(original)),
	clearParam(std::move(original.clearParam)),
	depthParam(std::move(original.depthParam)),

	created(std::move(original.created)),
	quitting(std::move(original.quitting)),
	showTestWindow(std::move(original.showTestWindow)),
	showAnotherWindow(std::move(original.showAnotherWindow)),
	bgColor(std::move(original.bgColor))
{
	//original.ptr = 0;
}


ImGuiTest::~ImGuiTest()
{
	ImGuiTest::destroy();
}


ImGuiTest& ImGuiTest::operator =(ImGuiTest&& original)
{
	if (this != &original) {
		clearParam = std::move(original.clearParam);
		depthParam = std::move(original.depthParam);

		created = std::move(original.created);
		quitting = std::move(original.quitting);
		showTestWindow = std::move(original.showTestWindow);
		showAnotherWindow = std::move(original.showAnotherWindow);
		bgColor = std::move(original.bgColor);

		//original.ptr = 0;
	}
	return *this;
}


///////////////////////////////////////////////////////////////////////////////////////////
// IProgramOpengl interface
///////////////////////////////////////////////////////////////////////////////////////////
void ImGuiTest::create(IWindowOpengl& window, Audio2d& audio2d)
{
	(void)audio2d;
	VERSO_ASSERT_MSG("verso-3d", isCreated() == false, "Already created!");

	created = true;

	reset(window);
}


void ImGuiTest::reset(IWindowOpengl& window)
{
	(void)window;
	VERSO_ASSERT_MSG("verso-3d", isCreated() == true, "DemoPart must be created before calling reset()");

	showTestWindow = true;
	showAnotherWindow = false;
	bgColor = clearParam.colorKeyframes.getValueInterpolated(0);
}


void ImGuiTest::destroy() VERSO_NOEXCEPT
{
	if (ImGuiTest::isCreated() == false) {
		return;
	}

	created = false;
}


bool ImGuiTest::isCreated() const
{
	return created;
}


void ImGuiTest::handleInput(IWindowOpengl& window, const FrameTimestamp& time)
{
	(void)window; (void)time;
}


void ImGuiTest::handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event)
{
	ImGuiVerso3dImpl::handleEvent(window, time, event);
}


void ImGuiTest::render(IWindowOpengl& window, const FrameTimestamp& time)
{
	(void)window;

	ImGuiVerso3dImpl::newFrame(window, time, true);

	// 1. Show a simple window
	// Tip: if we don't call ImGui::Begin()/ImGui::End() the widgets appears in a window automatically called "Debug"
	{
		static float f = 0.0f;
		ImGui::Text("Hello, world!");
		ImGui::SliderFloat("float", &f, 0.0f, 1.0f);
		ImGui::ColorEdit3("clear color", reinterpret_cast<float*>(&bgColor));
		if (ImGui::Button("Test Window"))
			showTestWindow ^= 1;
		if (ImGui::Button("Another Window"))
			showAnotherWindow ^= 1;
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
	}

	// 2. Show another simple window, this time using an explicit Begin/End pair
	if (showAnotherWindow) {
		ImGui::SetNextWindowSize(ImVec2(200, 100), ImGuiCond_FirstUseEver);
		ImGui::Begin("Another Window", &showAnotherWindow);
		ImGui::Text("Hello");
		ImGui::End();
	}

	// 3. Show the ImGui test window. Most of the sample code is in ImGui::ShowTestWindow()
	if (showTestWindow) {
		ImGui::SetNextWindowPos(ImVec2(650, 20), ImGuiCond_FirstUseEver);
		ImGui::ShowDemoWindow();
	}

	// Rendering
	Render::clearScreen(clearParam.clearFlag, bgColor);

	Opengl::depthTest(depthParam.depthTest);
	Opengl::depthWrite(depthParam.depthWrite);

	ImGuiVerso3dImpl::render(window, window.getRenderViewporti());
}


bool ImGuiTest::isQuitting() const
{
	return quitting;
}


void ImGuiTest::quit()
{
	quitting = true;
}


} // End namespace Verso

