#include <Verso/GrinderKit/DemoParts/EgaJs.hpp>
#include <Verso/System/JsonHelper3d.hpp>
#include <Verso/Render/Render.hpp>
#include <Verso/Image/Image.hpp>
#include <cmath>

namespace Verso {


const ClearParam defaultClearParam;
const DepthParam defaultDepthParam(false, false);

const Vector3f defaultCameraPosition(0.0f, 0.0f, -9.9f);
const Vector3f defaultCameraTarget(0.0f, 0.0f, 0.0f);
const Vector3f defaultCameraDesiredUp(Vector3f::up());
const float defaultCameraFov(90.0f);
const Rangef defaultCameraNearFarPlane(0.001f, 10.0f);
const CameraParam defaultCameraParam(
		CameraType::Target, ProjectionType::Orthographic,
		Vector3fKeyframes(defaultCameraPosition), Vector3fKeyframes(defaultCameraTarget),
		Vector3fKeyframes(defaultCameraDesiredUp),
		FloatKeyframes(defaultCameraFov), defaultCameraNearFarPlane);

const BlendMode defaultBlendMode = BlendMode::Transcluent;
const Vector2f defaultPosition(0.5f, 0.5f);
const Align defaultAlign(HAlign::Center, VAlign::Center);
const Vector2f defaultRelSize(1.0f, 1.0f);
const RefScale defaultRefScale(RefScale::ViewportSize);
const float defaultAngle(0.0f);
const float defaultAlpha(1.0f);

const Vector2i defaultResolution(320, 200);
const int defaultTransparentColor = 256;


EgaJs::EgaJs(
		const DemoPaths* demoPaths, const JSONObject& demoPartJson,
		const UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator) :
	DemoPart(demoPaths, demoPartJson, currentPathJson, jsonOrder, startAccumulator),
	clearParam(defaultClearParam),
	depthParam(defaultDepthParam),
	cameraParam(defaultCameraParam),
	blendMode(defaultBlendMode),
	positionKeyframes(),
	alignFromPosition(),
	relSize(defaultRelSize),
	refScale(defaultRefScale),
	rotationAngleKeyframes(defaultAngle),
	alphaKeyframes(defaultAlpha),
	sourceFileName(),
	initialFramebufferImageFileName(),
	resolution(defaultResolution),
	transparentColor(defaultTransparentColor),

	created(false),
	quitting(false),
	window(nullptr),
	camera(),
	sourceCode(),
	jsContext(nullptr),
	texture(nullptr),
	palette(),
	initialFrameBufferEga(nullptr),
	frameBufferEga(nullptr),
	firstTimeJsRun(true)
{
	resetPalette();

	const JSONObject& paramsObj = JsonHelper::readObject(demoPartJson, currentPathJson, "params");

	UString paramsCurrentPathJson(currentPathJson+"params.");

	clearParam.parseAttributeAsObject(paramsObj, paramsCurrentPathJson, "clear", false, defaultClearParam);

	depthParam.parseAttributeAsObject(paramsObj, paramsCurrentPathJson, "depth", false, defaultDepthParam);

	blendMode = JsonHelper::readBlendMode(paramsObj, paramsCurrentPathJson, "blend", false, defaultBlendMode);

	positionKeyframes = JsonHelper::readVector2fKeyframes(paramsObj, paramsCurrentPathJson, "position", false, Vector2fKeyframes(defaultPosition));

	alignFromPosition = JsonHelper::readAlign(paramsObj, paramsCurrentPathJson, "alignFromPosition", false, defaultAlign);

	relSize = JsonHelper::readVector2fArrayFormat(paramsObj, paramsCurrentPathJson, "relSize", false, defaultRelSize);

	refScale = JsonHelper::readRefScale(paramsObj, paramsCurrentPathJson, "refScale", false, defaultRefScale);

	rotationAngleKeyframes = JsonHelper::readFloatKeyframes(paramsObj, paramsCurrentPathJson, "angle", false, FloatKeyframes(defaultAngle));

	alphaKeyframes = JsonHelper::readFloatKeyframes(paramsObj, paramsCurrentPathJson, "alpha", false, FloatKeyframes(defaultAlpha));

	sourceFileName = JsonHelper::readString(paramsObj, paramsCurrentPathJson, "source", true);

	initialFramebufferImageFileName = JsonHelper::readString(paramsObj, paramsCurrentPathJson, "initialFramebufferSource", false, "");

	resolution = JsonHelper::readVector2iArrayFormat(paramsObj, paramsCurrentPathJson, "resolution", false, defaultResolution);

	transparentColor = JsonHelper::readNumberi(paramsObj, paramsCurrentPathJson, "transparentColor", false, defaultTransparentColor);
}


EgaJs::EgaJs(EgaJs&& original) noexcept :
	DemoPart(std::move(original)),
	clearParam(std::move(original.clearParam)),
	depthParam(std::move(original.depthParam)),
	cameraParam(std::move(original.cameraParam)),
	blendMode(std::move(original.blendMode)),
	positionKeyframes(std::move(original.positionKeyframes)),
	alignFromPosition(std::move(original.alignFromPosition)),
	relSize(std::move(original.relSize)),
	refScale(std::move(original.refScale)),
	rotationAngleKeyframes(std::move(original.rotationAngleKeyframes)),
	alphaKeyframes(std::move(original.alphaKeyframes)),
	sourceFileName(std::move(original.sourceFileName)),
	resolution(std::move(original.resolution)),
	transparentColor(std::move(original.transparentColor)),

	created(std::move(original.created)),
	quitting(std::move(original.quitting)),
	window(std::move(original.window)),
	camera(std::move(original.camera)),
	sourceCode(std::move(original.sourceCode)),
	jsContext(std::move(original.jsContext)),
	texture(std::move(original.texture)),
	initialFrameBufferEga(std::move(original.initialFrameBufferEga)),
	frameBufferEga(std::move(original.frameBufferEga)),
	firstTimeJsRun(std::move(original.firstTimeJsRun))
{
	original.window = nullptr;
	original.texture = nullptr;
	initialFrameBufferEga = nullptr;
	frameBufferEga = nullptr;
}


EgaJs::~EgaJs()
{
	EgaJs::destroy();
}


EgaJs& EgaJs::operator =(EgaJs&& original) noexcept
{
	if (this != &original) {
		clearParam = std::move(original.clearParam);
		depthParam = std::move(original.depthParam);
		cameraParam = std::move(original.cameraParam);
		blendMode = std::move(original.blendMode);
		positionKeyframes = std::move(original.positionKeyframes);
		alignFromPosition = std::move(original.alignFromPosition);
		relSize = std::move(original.relSize);
		refScale = std::move(original.refScale);
		rotationAngleKeyframes = std::move(original.rotationAngleKeyframes);
		alphaKeyframes = std::move(original.alphaKeyframes);
		sourceFileName = std::move(original.sourceFileName);
		resolution = std::move(original.resolution);
		transparentColor = std::move(original.transparentColor);

		created = std::move(original.created);
		quitting = std::move(original.quitting);
		window = std::move(original.window);
		camera = std::move(original.camera);
		sourceCode = std::move(original.sourceCode);
		jsContext = std::move(original.jsContext);
		texture = std::move(original.texture);
		initialFrameBufferEga = std::move(original.initialFrameBufferEga);
		frameBufferEga = std::move(original.frameBufferEga);
		firstTimeJsRun = std::move(original.firstTimeJsRun);

		original.window = nullptr;
		original.texture = nullptr;
		original.initialFrameBufferEga = nullptr;
		original.frameBufferEga = nullptr;
	}
	return *this;
}


void myErrorHandler(void* udata, const char* msg)
{
	(void)udata;
	VERSO_ERROR("verso-3d", "Duktape JavaScript error", msg);
}


///////////////////////////////////////////////////////////////////////////////////////////
// IProgramOpengl interface
///////////////////////////////////////////////////////////////////////////////////////////

void EgaJs::create(IWindowOpengl& window, Audio2d& audio2d)
{
	(void)audio2d;
	VERSO_ASSERT_MSG("verso-3d", isCreated() == false, "Already created!");
	this->window = &window;

	// Setup camera
	camera.create(&window, "GrinderKit/EgaJs/camera." + getInstanceStr(), false);

	// Initialize JavaScript
	jsContext = duk_create_heap(nullptr, nullptr, nullptr, nullptr, myErrorHandler);
	if (!jsContext) {
		VERSO_ERROR("verso-3d", "Cannot create duktape context!", "");
	}

	File sourceFile(demoPaths->pathJavascript() + sourceFileName);
	if (sourceFile.exists() == false) {
		UString message("Cannot load JavaScript. File not found!");
		VERSO_FILENOTFOUND("verso-3d", message.c_str(), this->sourceFileName.c_str());
	}
	if (sourceFile.loadToString(this->sourceCode) == false) { // NULL terminated string
		UString message("Cannot load JavaScript. File found, but could not be loaded!");
		VERSO_IOERROR("verso-3d", message.c_str(), this->sourceFileName.c_str());
	}
	sourceFile.close();

	duk_push_string(jsContext, sourceCode.c_str());
	duk_push_string(jsContext, "function");
	duk_compile(jsContext, DUK_COMPILE_FUNCTION);

	// Initialize framebuffer texture
	AspectRatio aspectRatio(resolution, "4:3");

	TextureParameters textureParameters(
		"EgaJs",
		TexturePixelFormat::Rgba,
		MinFilter::Nearest, MagFilter::Nearest,
		WrapStyle::ClampToEdge, WrapStyle::ClampToEdge);
	texture = new Texture();

	texture->createEmpty(window, resolution, textureParameters, TexturePixelFormat::Rgba, true, aspectRatio);

	// Initialize initial framebuffer ega bitmap
	initialFrameBufferEga = new std::uint8_t[resolution.x * resolution.y];
	memset(initialFrameBufferEga, 0, resolution.x * resolution.y);

	// Load potential image for initial framebuffer ega bitmap
	if (!initialFramebufferImageFileName.isEmpty()) {
		Image image;
		if (!image.createFromFile(demoPaths->pathTextures() + initialFramebufferImageFileName, false, aspectRatio)) {
			UString message("Cannot load initial framebuffer image. File not found!");
			VERSO_FILENOTFOUND("verso-3d", message.c_str(), this->initialFramebufferImageFileName.c_str());
		}

		if (image.getResolutioni() != resolution) {
			UString message("Image resolution (=");
			message += image.getResolutioni().toString();
			message += ") does not match demopart resolution (=";
			message += resolution.toString();
			message += ")!";
			VERSO_ILLEGALFORMAT("verso-3d", message.c_str(), this->initialFramebufferImageFileName.c_str());
		}

		std::uint8_t* src = image.getPixelData();
		int bpp = image.getBytesPerPixel();
		for (int i = 0; i < resolution.x * resolution.y; ++i)  {
			std::uint8_t r = src[i * bpp];
			std::uint8_t g = src[i * bpp + 1];
			std::uint8_t b = src[i * bpp + 2];

			bool found = false;
			for (int j = 0; j < 16; ++j) {
				if (palette[j * 3]     == r &&
					palette[j * 3 + 1] == g &&
					palette[j * 3 + 2] == b)
				{
					initialFrameBufferEga[i] = j;
					found = true;
					break;
				}
			}

			if (found == false) {
				UString message("Image has unknown color (");
				message.append2(static_cast<std::uint32_t>(r));
				message += ",";
				message.append2(static_cast<std::uint32_t>(g));
				message += ",";
				message.append2(static_cast<std::uint32_t>(b));
				message += ") not in EGA palette at coordinates (";
				message.append2(i % resolution.x);
				message += ",";
				message.append2(floor(i / resolution.x));
				message += ")!";
				VERSO_ILLEGALFORMAT("verso-3d", message.c_str(), this->initialFramebufferImageFileName.c_str());
			}
		}
	}

	// Initialize ega framebuffer
	frameBufferEga = new std::uint8_t[resolution.x * resolution.y];
	memset(frameBufferEga, 0, resolution.x * resolution.y);

	created = true;

	reset(window);
}


void EgaJs::reset(IWindowOpengl& window)
{
	(void)window;
	VERSO_ASSERT_MSG("verso-3d", isCreated() == true, "DemoPart must be created before calling reset()");

	camera.set(cameraParam);

	memcpy(frameBufferEga, initialFrameBufferEga, resolution.x * resolution.y);

	firstTimeJsRun = true;
}


void EgaJs::destroy() noexcept
{
	if (EgaJs::isCreated() == false) {
		return;
	}

	if (frameBufferEga) {
		delete[] frameBufferEga;
		frameBufferEga = nullptr;
	}

	if (initialFrameBufferEga) {
		delete[] initialFrameBufferEga;
		initialFrameBufferEga = nullptr;
	}

	if (texture) {
		delete texture;
		texture = nullptr;
	}

	if (jsContext) {
		duk_destroy_heap(jsContext);
		jsContext = nullptr;
	}

	camera.destroy();

	created = false;
}


bool EgaJs::isCreated() const
{
	return created;
}


void EgaJs::handleInput(IWindowOpengl& window, const FrameTimestamp& time)
{
	(void)window;

	float seconds = static_cast<float>(time.getElapsed().asSeconds());

	camera.setPosition(cameraParam.positionKeyframes.getValueInterpolated(seconds));
	camera.setTarget(cameraParam.targetKeyframes.getValueInterpolated(seconds));
	camera.setDesiredUp(cameraParam.desiredUpKeyframes.getValueInterpolated(seconds));
	if (cameraParam.projectionType == ProjectionType::Perspective) {
		camera.setFovY(cameraParam.fovYKeyframes.getValueInterpolated(seconds));
	}
	else if (cameraParam.projectionType == ProjectionType::Orthographic) {
		camera.setOrthographicZoomLevel(cameraParam.orthographicZoomLevelKeyframes.getValueInterpolated(seconds));
		camera.setOrthographicRotation(cameraParam.orthographicRotationKeyframes.getValueInterpolated(seconds));

	}
}


void EgaJs::handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event)
{
	(void)window; (void)time; (void)event;
}


void EgaJs::render(IWindowOpengl& window, const FrameTimestamp& time)
{
	float seconds = static_cast<float>(time.getElapsed().asSeconds());

	// Evaluate JavaScript
	duk_dup(jsContext, -1);
	duk_push_number(jsContext, seconds);
	duk_push_external_buffer(jsContext);
	duk_config_buffer(jsContext, -1, frameBufferEga, resolution.x * resolution.y);
	duk_push_boolean(jsContext, firstTimeJsRun);
	duk_call(jsContext, 3);
	duk_pop(jsContext);

	firstTimeJsRun = false;

	// Update texture
	std::uint8_t* rgbaPixels = texture->getPixelData();
	for (int i = 0; i < resolution.x * resolution.y; ++i) {
		std::uint8_t ega = frameBufferEga[i];
		size_t offset = i * 4;
		rgbaPixels[offset]     = palette[ega * 3];
		rgbaPixels[offset + 1] = palette[ega * 3 + 1];
		rgbaPixels[offset + 2] = palette[ega * 3 + 2];

		if (ega != transparentColor) {
			rgbaPixels[offset + 3] = 255;
		}
		else {
			rgbaPixels[offset + 3] = 0;
		}
	}
	texture->updateFromInternal();

	// Render result
	Render::clearScreen(
				clearParam.clearFlag,
				clearParam.colorKeyframes.getValueInterpolated(seconds));

	Opengl::depthTest(depthParam.depthTest);
	Opengl::depthWrite(depthParam.depthWrite);

	Opengl::blend(blendMode);
	Opengl::setPolygonRenderMode(PolygonRenderMode::Fill);

	Render::draw2d(
				window, camera, *texture,
				positionKeyframes.getValueInterpolated(seconds),
				alignFromPosition,
				relSize,
				refScale,
				alphaKeyframes.getValueInterpolated(seconds),
				rotationAngleKeyframes.getValueInterpolated(seconds));

	Opengl::blend(BlendMode::None);
}


bool EgaJs::isQuitting() const
{
	return quitting;
}


void EgaJs::quit()
{
	quitting = true;
}


void EgaJs::resetPalette()
{
	size_t c = 0;
	palette[c * 3] = 0;
	palette[c * 3 + 1] = 0;
	palette[c * 3 + 2] = 0;

	c = 1;
	palette[c * 3] = 0;
	palette[c * 3 + 1] = 0;
	palette[c * 3 + 2] = 170;

	c = 2;
	palette[c * 3] = 0;
	palette[c * 3 + 1] = 170;
	palette[c * 3 + 2] = 0;

	c = 3;
	palette[c * 3] = 0;
	palette[c * 3 + 1] = 170;
	palette[c * 3 + 2] = 170;

	c = 4;
	palette[c * 3] = 170;
	palette[c * 3 + 1] = 0;
	palette[c * 3 + 2] = 0;

	c = 5;
	palette[c * 3] = 170;
	palette[c * 3 + 1] = 0;
	palette[c * 3 + 2] = 170;

	c = 6;
	palette[c * 3] = 170;
	palette[c * 3 + 1] = 85;
	palette[c * 3 + 2] = 0;

	c = 7;
	palette[c * 3] = 170;
	palette[c * 3 + 1] = 170;
	palette[c * 3 + 2] = 170;

	c = 8;
	palette[c * 3] = 85;
	palette[c * 3 + 1] = 85;
	palette[c * 3 + 2] = 85;

	c = 9;
	palette[c * 3] = 85;
	palette[c * 3 + 1] = 85;
	palette[c * 3 + 2] = 255;

	c = 10;
	palette[c * 3] = 85;
	palette[c * 3 + 1] = 255;
	palette[c * 3 + 2] = 85;

	c = 11;
	palette[c * 3] = 85;
	palette[c * 3 + 1] = 255;
	palette[c * 3 + 2] = 255;

	c = 12;
	palette[c * 3] = 255;
	palette[c * 3 + 1] = 85;
	palette[c * 3 + 2] = 85;

	c = 13;
	palette[c * 3] = 255;
	palette[c * 3 + 1] = 85;
	palette[c * 3 + 2] = 255;

	c = 14;
	palette[c * 3] = 255;
	palette[c * 3 + 1] = 255;
	palette[c * 3 + 2] = 85;

	c = 15;
	palette[c * 3] = 255;
	palette[c * 3 + 1] = 255;
	palette[c * 3 + 2] = 255;
}


} // End namespace Verso

