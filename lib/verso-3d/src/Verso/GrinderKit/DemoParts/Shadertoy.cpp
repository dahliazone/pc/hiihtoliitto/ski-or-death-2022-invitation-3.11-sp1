#include <Verso/GrinderKit/DemoParts/Shadertoy.hpp>
#include <Verso/Render/VaoGenerator.hpp>
#include <Verso/Render/Render.hpp>
#include <Verso/System/JsonHelper3d.hpp>

namespace Verso {


const ClearParam defaultClearParam;
const DepthParam defaultDepthParam(false, false);
const BlendMode defaultBlendMode = BlendMode::None;


Shadertoy::Shadertoy(
		const DemoPaths* demoPaths, const JSONObject& demoPartJson,
		const UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator) :
	DemoPart(demoPaths, demoPartJson, currentPathJson, jsonOrder, startAccumulator),
	clearParam(defaultClearParam),
	depthParam(defaultDepthParam),
	blendMode(defaultBlendMode),
	shaderParam(),
	channelParam(),
	alphaKeyframes(1.0f),

	created(false),
	quitting(false),
	shader(),
	vao("GrinderKit/Shadertoy"),
	channels(),
	camera()
{
	const JSONObject& paramsObj = JsonHelper::readObject(demoPartJson, currentPathJson, "params");

	UString paramsCurrentPathJson(currentPathJson+"params.");

	clearParam.parseAttributeAsObject(paramsObj, paramsCurrentPathJson, "clear", false, defaultClearParam);

	depthParam.parseAttributeAsObject(paramsObj, paramsCurrentPathJson, "depth", false, defaultDepthParam);

	blendMode = JsonHelper::readBlendMode(paramsObj, paramsCurrentPathJson, "blend", false, defaultBlendMode);

	shaderParam.parseAttributeAsObject(paramsObj, paramsCurrentPathJson);
	vao.setId("GrinderKit/Shadertoy/" + demoPaths->pathShaders() + shaderParam.frag);

	channelParam.parseAttributeAsArray(paramsObj, paramsCurrentPathJson);

	alphaKeyframes = JsonHelper::readFloatKeyframes(paramsObj, paramsCurrentPathJson, "alpha", false, FloatKeyframes(1.0f));
}


Shadertoy::Shadertoy(Shadertoy&& original) noexcept :
	DemoPart(std::move(original)),
	clearParam(std::move(original.clearParam)),
	depthParam(std::move(original.depthParam)),
	blendMode(std::move(original.blendMode)),
	shaderParam(std::move(original.shaderParam)),
	channelParam(std::move(original.channelParam)),
	alphaKeyframes(std::move(original.alphaKeyframes)),

	created(std::move(original.created)),
	quitting(std::move(original.quitting)),
	shader(std::move(original.shader)),
	vao(std::move(original.vao)),
	channels(std::move(original.channels)),
	camera(std::move(original.camera))
{
	original.channels.clear();
}

Shadertoy::~Shadertoy()
{
	Shadertoy::destroy();
}


Shadertoy& Shadertoy::operator =(Shadertoy&& original) noexcept
{
	if (this != &original) {
		clearParam = std::move(original.clearParam);
		depthParam = std::move(original.depthParam);
		blendMode = std::move(original.blendMode);
		shaderParam = std::move(original.shaderParam);
		channelParam = std::move(original.channelParam);
		alphaKeyframes = std::move(original.alphaKeyframes);

		created = std::move(original.created);
		quitting = std::move(original.quitting);
		shader = std::move(original.shader);
		vao = std::move(original.vao);
		channels = std::move(original.channels);
		camera = std::move(original.camera);

		original.channels.clear();
	}
	return *this;
}


///////////////////////////////////////////////////////////////////////////////////////////
// IProgramOpengl interface
///////////////////////////////////////////////////////////////////////////////////////////
void Shadertoy::create(IWindowOpengl& window, Audio2d& audio2d)
{
	(void)audio2d;
	VERSO_ASSERT_MSG("verso-3d", isCreated() == false, "Already created!");

	// Vao
	Vector2f renderResolution(window.getRenderResolutionf());
	VaoGenerator::quad2d(
				vao, renderResolution,
				renderResolution / 2.0f,
				static_cast<BufferTypes>(BufferType::Position));
	vao.bind();

	// Default shaders if nothing is defined
	UString shaderVert = demoPaths->pathShadersVerso() + "grinderkit/layers/shadertoy.default.330.vert";
	if (shaderParam.vert != "") {
		shaderVert = demoPaths->pathShaders()+shaderParam.vert;
	}

	UString shaderFrag = demoPaths->pathShadersVerso() + "grinderkit/layers/shadertoy.default.330.frag";
	if (shaderParam.frag != "") {
		shaderFrag = demoPaths->pathShaders()+shaderParam.frag;
	}

	// Shaders
	shader.createFromFiles(
				shaderVert,
				shaderFrag,
				false);
	shader.bindAttribLocation(ShaderAttribute::Position);
	//shader.bindAttribLocation(ShaderAttribute::Uv);
	//shader.bindAttribLocation(ShaderAttribute(4, "foobar"));
	shader.linkProgram();

	// Load channels
	for (size_t i = 0; i < channelParam.sources.size(); ++i) {
		Texture* tex = new Texture();
		UString typeForShader("iChannel");
		typeForShader.append2(i);
		tex->createFromFile(window, demoPaths->pathTextures()+channelParam.sources[i], TextureParameters(typeForShader));
		tex->setWrapStyle(WrapStyle::Repeat, WrapStyle::Repeat);
		tex->setMinFilter(MinFilter::Linear);
		tex->setMagFilter(MagFilter::Linear);
		channels.push_back(tex);
	}

	camera.create(&window, "verso-3d/Shadertoy/camera." + getInstanceStr(), false);

	created = true;

	reset(window);
}


void Shadertoy::reset(IWindowOpengl& window)
{
	(void)window;
	VERSO_ASSERT_MSG("verso-3d", isCreated() == true, "DemoPart must be created before calling reset()");

	camera.reset(Vector3f(0.0f, 0.0f, -3.0f), Vector3f(0.0f, 0.0f, 0.0f), Vector3f::up());
	camera.setProjectionOrthographic(Rangef(0.001f, 10.0f));
}


void Shadertoy::destroy() VERSO_NOEXCEPT
{
	if (Shadertoy::isCreated() == false) {
		return;
	}

	camera.destroy();

	for (size_t i = 0; i < channels.size(); ++i) {
		if (channels[i] != nullptr) {
			delete channels[i];
			channels[i] = nullptr;
		}
	}

	created = false;
}


bool Shadertoy::isCreated() const
{
	return created;
}


void Shadertoy::handleInput(IWindowOpengl& window, const FrameTimestamp& time)
{
	(void)window; (void)time;
}


void Shadertoy::handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event)
{
	(void)window; (void)time; (void)event;
}


void Shadertoy::render(IWindowOpengl& window, const FrameTimestamp& time)
{
	float seconds = static_cast<float>(time.getElapsed().asSeconds());

	Render::clearScreen(
				clearParam.clearFlag,
				clearParam.colorKeyframes.getValueInterpolated(seconds));

	Opengl::depthTest(depthParam.depthTest);
	Opengl::depthWrite(depthParam.depthWrite);

	Opengl::blend(blendMode);

	shader.useProgram();

	shader.setUniform("iResolution", Vector3f(window.getRenderResolutionf()), false);
	shader.setUniformf("iTime", static_cast<float>(time.getElapsed().asSeconds()), false);
	shader.setUniformf("iTimeDelta", static_cast<float>(time.getDt().asSeconds()), false);
	shader.setUniformi("iFrame", static_cast<GLint>(time.getRenderFrame()), false);
	shader.setUniform("model", Matrix4x4f::Identity(), false);
	shader.setUniform("view", camera.getViewMatrix(), false);
	shader.setUniformf("alpha", static_cast<float>(alphaKeyframes.getValueInterpolated(seconds)), false);

	// projection matrix
	Vector2f renderResolution(window.getRenderResolutionf());
	Matrix4x4f projection = Matrix4x4f::orthographicProjection(
								0.0f, renderResolution.x,
								renderResolution.y, 0.0f,
								0.1f, 100.0f);
	shader.setUniform("projection", projection, false);

	// iChannelTime: channel playback time (in seconds)
	// \TODO: Fix to correspond to 0 or animation phase
	float iChannelTime[4] = { 1.1f, 2.2f, 3.3f, 4.4f };
	shader.setUniform1fv("iChannelTime", 4, iChannelTime, false);

	// iChannelResolution: channel resolution (in pixels)
	float iChannelResolution[4*3] = { 0,0,0, 0,0,0, 0,0,0, 0,0,0 };
	for (size_t i = 0; i < channels.size(); ++i) {
		Vector2f res = channels[i]->getResolutionf();
		iChannelResolution[i * 3] = res.x;
		iChannelResolution[i * 3 + 1] = res.y;
	}
	shader.setUniform3fv("iChannelResolution", 4, iChannelResolution, false);

	//uniform vec4      iMouse;                // mouse pixel coords. xy: current (if MLB down), zw: click

	// Setup channels
	// \TODO: support for cubemaps, animations, render-to-texture
	for (int i = 0; i < static_cast<int>(channels.size()); ++i) {
		//UString samplerVar("iChannel");
		//samplerVar.append2(i);
		shader.setUniformi(channels[i]->getParameters().typeForShader, i);
		window.setActiveTextureUnit(i);
		channels[i]->bind();
	}
	window.setActiveTextureUnit(0);

	//uniform vec4      iDate;                 // (year, month, day, time in seconds)

	shader.setUniformf("iSampleRate", 44100, false);

	// Render VAO
	vao.render();

	Opengl::blend(BlendMode::None);
}


bool Shadertoy::isQuitting() const
{
	return quitting;
}


void Shadertoy::quit()
{
	quitting = true;
}


} // End namespace Verso

