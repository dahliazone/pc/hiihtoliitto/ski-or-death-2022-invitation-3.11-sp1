#include <Verso/GrinderKit/DemoPaths.hpp>

namespace Verso {


DemoPaths::DemoPaths() :
	jsons(),
	models(),
	shaders(),
	shadersVerso(),
	music(),
	textures(),
	fonts(),
	particles(),
	gui(),
	guiIcons(),
	guiStyles(),
	javascript()
{
}


DemoPaths::DemoPaths(DemoPaths&& original) :
	jsons(std::move(original.jsons)),
	models(std::move(original.models)),
	shaders(std::move(original.shaders)),
	shadersVerso(std::move(original.shadersVerso)),
	music(std::move(original.music)),
	textures(std::move(original.textures)),
	fonts(std::move(original.fonts)),
	particles(std::move(original.particles)),
	gui(std::move(original.gui)),
	guiIcons(std::move(original.guiIcons)),
	guiStyles(std::move(original.guiStyles)),
	javascript(std::move(original.javascript))
{
	// Clear the original
	//original.asd = nullptr;
}


DemoPaths& DemoPaths::operator =(DemoPaths&& original)
{
	if (this != &original) {
		jsons = std::move(original.jsons);
		models = std::move(original.models);
		shaders = std::move(original.shaders);
		shadersVerso = std::move(original.shadersVerso);
		music = std::move(original.music);
		textures = std::move(original.textures);
		fonts = std::move(original.fonts);
		particles = std::move(original.particles);
		gui = std::move(original.gui);
		guiIcons = std::move(original.guiIcons);
		guiStyles = std::move(original.guiStyles);
		javascript = std::move(original.javascript);

		// Clear the original
		//original.asd = nullptr;
	}
	return *this;
}


DemoPaths::~DemoPaths()
{
}


void DemoPaths::importJson(const JSONObject& paths, const UString& currentPathJson, const UString& jsonFileNamePath)
{
	jsons = jsonFileNamePath;
	models = jsonFileNamePath + addSlashToEndIfNeeded(JsonHelper::readString(paths, currentPathJson, "models", false, "models"));
	shaders = jsonFileNamePath + addSlashToEndIfNeeded(JsonHelper::readString(paths, currentPathJson, "shaders", false, "shaders"));
	shadersVerso = jsonFileNamePath + addSlashToEndIfNeeded(JsonHelper::readString(paths, currentPathJson, "shadersVerso", false, "shaders/verso"));
	music = jsonFileNamePath + addSlashToEndIfNeeded(JsonHelper::readString(paths, currentPathJson, "music", false, "music"));
	textures = jsonFileNamePath + addSlashToEndIfNeeded(JsonHelper::readString(paths, currentPathJson, "textures", false, "textures"));
	fonts = jsonFileNamePath + addSlashToEndIfNeeded(JsonHelper::readString(paths, currentPathJson, "fonts", false, "fonts"));
	particles = jsonFileNamePath + addSlashToEndIfNeeded(JsonHelper::readString(paths, currentPathJson, "particles", false, "particles"));
	gui = jsonFileNamePath + addSlashToEndIfNeeded(JsonHelper::readString(paths, currentPathJson, "gui", false, "gui"));
	guiIcons = jsonFileNamePath + addSlashToEndIfNeeded(JsonHelper::readString(paths, currentPathJson, "guiIcons", false, "gui/icons"));
	guiStyles = jsonFileNamePath + addSlashToEndIfNeeded(JsonHelper::readString(paths, currentPathJson, "guiStyles", false, "gui/styles"));
	javascript = jsonFileNamePath + addSlashToEndIfNeeded(JsonHelper::readString(paths, currentPathJson, "javascript", false, "js"));
}


JSONValue* DemoPaths::exportJson()
{
	JSONObject obj;
	obj[L"models"] = new JSONValue(models.toUtf8Wstring());
	obj[L"shaders"] = new JSONValue(shaders.toUtf8Wstring());
	obj[L"shadersVerso"] = new JSONValue(shadersVerso.toUtf8Wstring());
	obj[L"music"] = new JSONValue(music.toUtf8Wstring());
	obj[L"textures"] = new JSONValue(textures.toUtf8Wstring());
	obj[L"fonts"] = new JSONValue(fonts.toUtf8Wstring());
	obj[L"particles"] = new JSONValue(particles.toUtf8Wstring());
	obj[L"gui"] = new JSONValue(gui.toUtf8Wstring());
	obj[L"guiIcons"] = new JSONValue(guiIcons.toUtf8Wstring());
	obj[L"guiStyles"] = new JSONValue(guiStyles.toUtf8Wstring());
	obj[L"javascript"] = new JSONValue(javascript.toUtf8Wstring());

	return new JSONValue(obj);
}


UString DemoPaths::addSlashToEndIfNeeded(const UString& path)
{
	if (path.charAt(static_cast<int>(path.size())-1) != '/') {
		UString newPath(path);
		newPath += "/";
		return newPath;
	}
	else {
		return path;
	}
}


UString DemoPaths::pathJsons() const
{
	return File::getBasePath()+jsons;
}


UString DemoPaths::pathModels() const
{
	return File::getBasePath()+models;
}


UString DemoPaths::pathShaders() const
{
	return File::getBasePath()+shaders;
}


UString DemoPaths::pathShadersVerso() const
{
	return File::getBasePath()+shadersVerso;
}


UString DemoPaths::pathMusic() const
{
	return File::getBasePath()+music;
}


UString DemoPaths::pathTextures() const
{
	return File::getBasePath()+textures;
}


UString DemoPaths::pathFonts() const
{
	return File::getBasePath()+fonts;
}


UString DemoPaths::pathParticles() const
{
	return File::getBasePath()+particles;
}


UString DemoPaths::pathGui() const
{
	return File::getBasePath()+gui;
}


UString DemoPaths::pathGuiIcons() const
{
	return File::getBasePath()+guiIcons;
}


UString DemoPaths::pathGuiStyles() const
{
	return File::getBasePath()+guiStyles;
}


UString DemoPaths::pathJavascript() const
{
	return File::getBasePath()+javascript;
}


UString DemoPaths::toString(const UString& newLinePadding) const
{
	UString str;

	str += "{ \"jsons\": \"" + jsons + "\"";
	{
		UString newLinePadding2 = newLinePadding + "  ";
		str += ",\n" + newLinePadding2 + "\"models\": \"" + models + "\"";
		str += ",\n" + newLinePadding2 + "\"shaders\": \"" + shaders + "\"";
		str += ",\n" + newLinePadding2 + "\"shadersVerso\": \"" + shadersVerso + "\"";
		str += ",\n" + newLinePadding2 + "\"music\": \"" + music + "\"";
		str += ",\n" + newLinePadding2 + "\"textures\": \"" + textures + "\"";
		str += ",\n" + newLinePadding2 + "\"fonts\": \"" + fonts + "\"";
		str += ",\n" + newLinePadding2 + "\"particles\": \"" + particles + "\"";
		str += ",\n" + newLinePadding2 + "\"gui\": \"" + gui + "\"";
		str += ",\n" + newLinePadding2 + "\"guiIcons\": \"" + guiIcons + "\"";
		str += ",\n" + newLinePadding2 + "\"guiStyles\": \"" + guiStyles + "\"";
		str += ",\n" + newLinePadding2 + "\"javascript\": \"" + javascript + "\"";
	}
	str += "\n" + newLinePadding + "}";

	return str;
}


UString DemoPaths::toStringDebug(const UString& newLinePadding) const
{
	UString str("Verso::DemoPaths(\n");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso

