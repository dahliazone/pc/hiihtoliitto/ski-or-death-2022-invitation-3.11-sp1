#include <Verso/GrinderKit/Params/ShaderParam.hpp>

namespace Verso {


ShaderParam::ShaderParam() :
	vert(""),
	frag(""),
	geom("")
{
}


ShaderParam::ShaderParam(const ShaderParam& original) :
	vert(original.vert),
	frag(original.frag),
	geom(original.geom)
{
}


ShaderParam::ShaderParam(const ShaderParam&& original) :
	vert(std::move(original.vert)),
	frag(std::move(original.frag)),
	geom(std::move(original.geom))
{
}


ShaderParam& ShaderParam::operator =(const ShaderParam& original)
{
	if (this != &original) {
		vert = original.vert;
		frag = original.frag;
		geom = original.geom;
	}
	return *this;
}


ShaderParam& ShaderParam::operator =(ShaderParam&& original) noexcept
{
	if (this != &original) {
		vert = std::move(original.vert);
		frag = std::move(original.frag);
		geom = std::move(original.geom);
	}
	return *this;
}


ShaderParam::~ShaderParam()
{
}


bool ShaderParam::parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required)
{
	JSONValue* valueTester = JsonHelper::readValue(object, currentPathJson, name);
	UString fieldCurrentPathJson(currentPathJson + name + ".");
	if (valueTester && valueTester->IsObject()) {
		const JSONObject& attributeObject = JsonHelper::readObject(object, currentPathJson, name);
		if (parseObject(attributeObject, fieldCurrentPathJson) == false) {
			UString error("Cannot read Shader param from field \""+fieldCurrentPathJson+"\"!.");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}

		return parseObject(attributeObject, fieldCurrentPathJson);
	}
	else if (required == true) {
		UString error("Cannot read Shader param from field \"" + fieldCurrentPathJson + "\"!.");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return true;
}


bool ShaderParam::parseObject(const JSONObject& object, const UString& currentPathJson)
{
	frag = JsonHelper::readString(object, currentPathJson, "frag");

	vert = JsonHelper::readString(object, currentPathJson, "vert");

	geom = JsonHelper::readString(object, currentPathJson, "geom");

	return true;
}


UString ShaderParam::toString(const UString& newLinePadding) const
{
	(void)newLinePadding;

	UString str;
	str += "{ frag=\"";
	str += frag;
	str += "\", vert=\"";
	str += vert;
	str += "\", geom=\"";
	str += geom;
	str += "\" }";
	return str;
}


UString ShaderParam::toStringDebug(const UString& newLinePadding) const
{
	UString str("ShaderParam(");
	str += toString(newLinePadding);
	str += "\")";
	return str;
}


} // End namespace Verso

