#include <Verso/GrinderKit/Params/Material3dParam.hpp>
#include <Verso/Render/Material/MaterialColor3d.hpp>
#include <Verso/Render/Material/MaterialColorPhong3d.hpp>
#include <Verso/Render/Material/MaterialLightmapsPhong3d.hpp>
#include <Verso/Render/Material/MaterialLightmapsPhong3d_GridHeightmapDisplacement.hpp>
#include <Verso/Render/Material/MaterialLightmapsPhong3d_GridHeightmapDisplacement_SinWave.hpp>
#include <Verso/Render/Material/MaterialNormalVisualizer3d.hpp>
#include <Verso/Render/Material/MaterialTexture3d.hpp>
#include <Verso/Render/Material/MaterialTexturePhong3d.hpp>
#include <Verso/Render/Material/MaterialTextureRgba3d.hpp>
#include <Verso/Math/ColorGenerator.hpp>

namespace Verso {


Material3dParam::Material3dParam() :
	type(),

	color(),
	phongMaterial(),
	diffuseTexture(),
	specularTexture(),
	heightmapTexture(),

	normalColor(),
	normalLength(),
	maxHeight(),
	waveSpeedXZ(),
	waveSpeedY(0.0f),

	shaderOverrideParam()
{
}


Material3dParam::Material3dParam(const Material3dParam& original) :
	type(original.type),

	color(original.color),
	phongMaterial(original.phongMaterial),
	diffuseTexture(original.diffuseTexture),
	specularTexture(original.specularTexture),
	heightmapTexture(original.heightmapTexture),

	normalColor(original.normalColor),
	normalLength(original.normalLength),
	maxHeight(original.maxHeight),
	waveSpeedXZ(original.waveSpeedXZ),
	waveSpeedY(original.waveSpeedY),

	shaderOverrideParam(original.shaderOverrideParam)
{
}


Material3dParam::Material3dParam(const Material3dParam&& original) :
	type(std::move(original.type)),

	color(std::move(original.color)),
	phongMaterial(std::move(original.phongMaterial)),
	diffuseTexture(std::move(original.diffuseTexture)),
	specularTexture(std::move(original.specularTexture)),
	heightmapTexture(std::move(original.heightmapTexture)),

	normalColor(std::move(original.normalColor)),
	normalLength(std::move(original.normalLength)),
	maxHeight(std::move(original.maxHeight)),
	waveSpeedXZ(std::move(original.waveSpeedXZ)),
	waveSpeedY(std::move(original.waveSpeedY)),

	shaderOverrideParam(std::move(original.shaderOverrideParam))
{
}


Material3dParam& Material3dParam::operator =(const Material3dParam& original)
{
	if (this != &original) {
		type = original.type;

		color = original.color;
		phongMaterial = original.phongMaterial;
		diffuseTexture = original.diffuseTexture;
		specularTexture = original.specularTexture;
		heightmapTexture = original.heightmapTexture;

		normalColor = original.normalColor;
		normalLength = original.normalLength;
		maxHeight = original.maxHeight;
		waveSpeedXZ = original.waveSpeedXZ;
		waveSpeedY = original.waveSpeedY;

		shaderOverrideParam = original.shaderOverrideParam;
	}
	return *this;
}


Material3dParam& Material3dParam::operator =(Material3dParam&& original) noexcept
{
	if (this != &original) {
		type = std::move(original.type);

		color = std::move(original.color);
		phongMaterial = std::move(original.phongMaterial);
		diffuseTexture = std::move(original.diffuseTexture);
		specularTexture = std::move(original.specularTexture);
		heightmapTexture = std::move(original.heightmapTexture);

		normalColor = std::move(original.normalColor);
		normalLength = std::move(original.normalLength);
		maxHeight = std::move(original.maxHeight);
		waveSpeedXZ = std::move(original.waveSpeedXZ);
		waveSpeedY = std::move(original.waveSpeedY);

		shaderOverrideParam = std::move(original.shaderOverrideParam);
	}
	return *this;
}


Material3dParam::~Material3dParam()
{
}


bool Material3dParam::parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required)
{
	JSONValue* valueTester = JsonHelper::readValue(object, currentPathJson, name);
	UString fieldCurrentPathJson(currentPathJson + name + ".");
	if (valueTester && valueTester->IsObject()) {
		const JSONObject& attributeObject = JsonHelper::readObject(object, currentPathJson, name);
		if (parseObject(attributeObject, fieldCurrentPathJson) == false) {
			UString error("Cannot read Material3d param from field \""+fieldCurrentPathJson+"\"!.");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}

		return parseObject(attributeObject, fieldCurrentPathJson);
	}
	else if (required == true) {
		UString error("Cannot read Material3d param from field \"" + fieldCurrentPathJson + "\"!.");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return true;
}


bool Material3dParam::parseObject(const JSONObject& object, const UString& currentPathJson)
{
	type = JsonHelper::readString(object, currentPathJson, "type");

	if (type.equals(MaterialColor3d::getTypeStatic())) {
		color = JsonHelper::readColor(object, currentPathJson, "color", false, ColorGenerator::getColor(ColorRgb::Blue));
	}

	else if (type.equals(MaterialColorPhong3d::getTypeStatic())) {
		phongMaterial.parseAttributeAsObject(object, currentPathJson);
	}

	else if (type.equals(MaterialLightmapsPhong3d::getTypeStatic())) {
		diffuseTexture = JsonHelper::readString(object, currentPathJson, "diffuseTexture", true);
		specularTexture = JsonHelper::readString(object, currentPathJson, "specularTexture", true);
		phongMaterial.parseAttributeAsObject(object, currentPathJson);
	}

	else if (type.equals(MaterialLightmapsPhong3d_GridHeightmapDisplacement::getTypeStatic())) {
		diffuseTexture = JsonHelper::readString(object, currentPathJson, "diffuseTexture", true);
		specularTexture = JsonHelper::readString(object, currentPathJson, "specularTexture", true);
		heightmapTexture = JsonHelper::readString(object, currentPathJson, "heightmapTexture", true);
		maxHeight = JsonHelper::readNumberf(object, currentPathJson, "maxHeight", false, 10.0f);
		phongMaterial.parseAttributeAsObject(object, currentPathJson);
	}

	else if (type.equals(MaterialLightmapsPhong3d_GridHeightmapDisplacement_SinWave::getTypeStatic())) {
		diffuseTexture = JsonHelper::readString(object, currentPathJson, "diffuseTexture", true);
		specularTexture = JsonHelper::readString(object, currentPathJson, "specularTexture", true);
		heightmapTexture = JsonHelper::readString(object, currentPathJson, "heightmapTexture", true);
		maxHeight = JsonHelper::readNumberf(object, currentPathJson, "maxHeight", false, 10.0f);
		waveSpeedXZ = JsonHelper::readVector2f(object, currentPathJson, "waveSpeedY", false, Vector2f(0.02f, 0.05f));
		waveSpeedY = JsonHelper::readNumberf(object, currentPathJson, "waveSpeedY", false, 2.0f);
		phongMaterial.parseAttributeAsObject(object, currentPathJson);
	}

	else if (type.equals(MaterialNormalVisualizer3d::getTypeStatic())) {
		normalLength = JsonHelper::readNumberf(object, currentPathJson, "normalLength", false, 1.0f);
		normalColor = JsonHelper::readColor(object, currentPathJson, "normalColor", false, ColorGenerator::getColor(ColorRgb::Yellow));
	}

	else if (type.equals(MaterialTexture3d::getTypeStatic())) {
		diffuseTexture = JsonHelper::readString(object, currentPathJson, "diffuseTexture", true);
	}

	else if (type.equals(MaterialTexturePhong3d::getTypeStatic())) {
		diffuseTexture = JsonHelper::readString(object, currentPathJson, "diffuseTexture", true);
		phongMaterial.parseAttributeAsObject(object, currentPathJson);
	}

	else if (type.equals(MaterialTextureRgba3d::getTypeStatic())) {
		diffuseTexture = JsonHelper::readString(object, currentPathJson, "diffuseTexture", true);
		phongMaterial.diffuse = JsonHelper::readColor(object, currentPathJson, "diffuseColor", false, ColorGenerator::getColor(ColorRgb::White));
	}

	else {
		UString error("Unknown material type \""+type+"\" in field \""+currentPathJson+"type\".");
		VERSO_OBJECTNOTFOUND("verso-3d", error.c_str(), "");
	}

	shaderOverrideParam.parseAttributeAsObject(object, currentPathJson);

	return true;
}


IMaterial* Material3dParam::createMaterial(IWindowOpengl& window) const
{
	if (type.equals(MaterialColor3d::getTypeStatic())) {
		MaterialColor3d* material = new MaterialColor3d();
		material->create(window, color);
		return material;
	}

	else if (type.equals(MaterialColorPhong3d::getTypeStatic())) {
		MaterialColorPhong3d* material = new MaterialColorPhong3d();
		material->create(window, phongMaterial);
		return material;
	}

	else if (type.equals(MaterialLightmapsPhong3d::getTypeStatic())) {
		MaterialLightmapsPhong3d* material = new MaterialLightmapsPhong3d();
		material->create(window, diffuseTexture, specularTexture, phongMaterial);
		return material;
	}

	else if (type.equals(MaterialLightmapsPhong3d_GridHeightmapDisplacement::getTypeStatic())) {
		MaterialLightmapsPhong3d_GridHeightmapDisplacement* material = new MaterialLightmapsPhong3d_GridHeightmapDisplacement();
		material->create(window, diffuseTexture, specularTexture, heightmapTexture, maxHeight, phongMaterial);
		return material;
	}

	else if (type.equals(MaterialLightmapsPhong3d_GridHeightmapDisplacement_SinWave::getTypeStatic())) {
		MaterialLightmapsPhong3d_GridHeightmapDisplacement_SinWave* material = new MaterialLightmapsPhong3d_GridHeightmapDisplacement_SinWave();
		material->create(window, diffuseTexture, specularTexture, heightmapTexture, maxHeight, waveSpeedXZ, waveSpeedY, phongMaterial);
		return material;
	}

	else if (type.equals(MaterialNormalVisualizer3d::getTypeStatic())) {
		MaterialNormalVisualizer3d* material = new MaterialNormalVisualizer3d();
		material->create(window, normalLength, normalColor);
		return material;
	}

	else if (type.equals(MaterialTexture3d::getTypeStatic())) {
		MaterialTexture3d* material = new MaterialTexture3d();
		material->create(window, diffuseTexture);
		return material;
	}

	else if (type.equals(MaterialTexturePhong3d::getTypeStatic())) {
		MaterialTexturePhong3d* material = new MaterialTexturePhong3d();
		material->create(window, diffuseTexture, phongMaterial);
		return material;
	}

	else if (type.equals(MaterialTextureRgba3d::getTypeStatic())) {
		MaterialTextureRgba3d* material = new MaterialTextureRgba3d();
		material->create(window, diffuseTexture);
		material->updateDiffuseColor(phongMaterial.diffuse);
		return material;
	}

	else {
		UString error("Cannot create material with given type: ");
		error.append2(type);
		VERSO_OBJECTNOTFOUND("verso-3d", error.c_str(), "");
	}
}


UString Material3dParam::toString(const UString& newLinePadding) const
{
	UString str("{ ");
	UString newLinePadding2 = newLinePadding + "  ";
	str += "type=\"";
	str += type;
	str += "\"";

	if (type.equals(MaterialColor3d::getTypeStatic())) {
		str += ", color=";
		str += color.toString();
	}

	else if (type.equals(MaterialColorPhong3d::getTypeStatic())) {
		str += ", phongMaterial=";
		str += phongMaterial.toString(newLinePadding2);
	}

	else if (type.equals(MaterialLightmapsPhong3d::getTypeStatic())) {
		str += ", diffuseTexture=\"";
		str += diffuseTexture;
		str += "\", specularTexture=\"";
		str += specularTexture;
		str += "\", phongMaterial=";
		str += phongMaterial.toString(newLinePadding2);
	}

	else if (type.equals(MaterialLightmapsPhong3d_GridHeightmapDisplacement::getTypeStatic())) {
		str += ", diffuseTexture=\"";
		str += diffuseTexture;
		str += "\", specularTexture=\"";
		str += specularTexture;
		str += "\", heightmapTexture=\"";
		str += heightmapTexture;
		str += "\", maxHeight=";
		str.append2(maxHeight);
		str += ", phongMaterial=";
		str += phongMaterial.toString(newLinePadding2);
	}

	else if (type.equals(MaterialLightmapsPhong3d_GridHeightmapDisplacement_SinWave::getTypeStatic())) {
		str += ", diffuseTexture=\"";
		str += diffuseTexture;
		str += "\", specularTexture=\"";
		str += specularTexture;
		str += "\", heightmapTexture=\"";
		str += heightmapTexture;
		str += "\", maxHeight=";
		str.append2(maxHeight);
		str += ", phongMaterial=";
		str += phongMaterial.toString(newLinePadding2);
	}

	else if (type.equals(MaterialNormalVisualizer3d::getTypeStatic())) {
		str += ", normalLength=";
		str.append2(normalLength);
		str += ", normalColor=";
		str += normalColor.toString();
	}

	else if (type.equals(MaterialTexture3d::getTypeStatic())) {
		str += ", diffuseTexture=\"";
		str += diffuseTexture;
		str += "\"";
	}

	else if (type.equals(MaterialTexturePhong3d::getTypeStatic())) {
		str += ", diffuseTexture=\"";
		str += diffuseTexture;
		str += "\", phongMaterial=";
		str += phongMaterial.toString(newLinePadding2);
	}

	else if (type.equals(MaterialTextureRgba3d::getTypeStatic())) {
		str += ", diffuseTexture=\"";
		str += diffuseTexture;
		str += "\", diffuseColor=";
		str += phongMaterial.diffuse.toString();
	}

	else {
		str += ", error=\"Unknown type\"";
	}

	str += ", shaderOverrideParam=";
	str += shaderOverrideParam.toString(newLinePadding2);
	str += " }";
	return str;
}


UString Material3dParam::toStringDebug(const UString& newLinePadding) const
{
	UString str("Material3dParam(");
	str += toString(newLinePadding);
	str += "\")";
	return str;
}


} // End namespace Verso

