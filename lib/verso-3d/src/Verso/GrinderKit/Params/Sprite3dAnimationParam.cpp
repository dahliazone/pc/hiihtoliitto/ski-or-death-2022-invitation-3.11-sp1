#include <Verso/GrinderKit/Params/Sprite3dAnimationParam.hpp>

namespace Verso {


Sprite3dAnimationParam::Sprite3dAnimationParam() :
	type(AnimationType::Loop),
	frameCount(0),
	useManualFrameOrder(false),
	autoTiming(),
	manualFrameOrder()
{
}


Sprite3dAnimationParam::Sprite3dAnimationParam(const Sprite3dAnimationParam& original) :
	type(original.type),
	frameCount(original.frameCount),
	useManualFrameOrder(original.useManualFrameOrder),
	autoTiming(original.autoTiming),
	manualFrameOrder(original.manualFrameOrder)
{
}


Sprite3dAnimationParam::Sprite3dAnimationParam(const Sprite3dAnimationParam&& original) :
	type(std::move(original.type)),
	frameCount(std::move(original.frameCount)),
	useManualFrameOrder(std::move(original.useManualFrameOrder)),
	autoTiming(std::move(original.autoTiming)),
	manualFrameOrder(std::move(original.manualFrameOrder))
{
}


Sprite3dAnimationParam& Sprite3dAnimationParam::operator =(const Sprite3dAnimationParam& original)
{
	if (this != &original) {
		type = original.type;
		frameCount = original.frameCount;
		useManualFrameOrder = original.useManualFrameOrder;
		autoTiming = original.autoTiming;
		manualFrameOrder = original.manualFrameOrder;
	}
	return *this;
}


Sprite3dAnimationParam& Sprite3dAnimationParam::operator =(Sprite3dAnimationParam&& original) noexcept
{
	if (this != &original) {
		type = std::move(original.type);
		frameCount = std::move(original.frameCount);
		useManualFrameOrder = std::move(original.useManualFrameOrder);
		autoTiming = std::move(original.autoTiming);
		manualFrameOrder = std::move(original.manualFrameOrder);
	}
	return *this;
}


Sprite3dAnimationParam::~Sprite3dAnimationParam()
{
}


bool Sprite3dAnimationParam::parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required)
{
	JSONValue* valueTester = JsonHelper::readValue(object, currentPathJson, name);
	UString fieldCurrentPathJson(currentPathJson + name + ".");
	if (valueTester && valueTester->IsObject()) {
		const JSONObject& attributeObject = JsonHelper::readObject(object, currentPathJson, name);
		if (parseObject(attributeObject, fieldCurrentPathJson) == false) {
			UString error("Cannot read Sprite3dAnimation param from field \""+fieldCurrentPathJson+"\"!");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}

		return parseObject(attributeObject, fieldCurrentPathJson);
	}
	else if (required == true) {
		UString error("Cannot read Sprite3dAnimation param from field \"" + fieldCurrentPathJson + "\"!");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return true;
}


bool Sprite3dAnimationParam::parseObject(const JSONObject& object, const UString& currentPathJson)
{
	type = JsonHelper::readAnimationType(object, currentPathJson, "type", false, AnimationType::Loop);

	frameCount = JsonHelper::readNumberi(object, currentPathJson, "frameCount", true);

	if (JsonHelper::isDefined(object, currentPathJson, "manualFrameOrder")) {
		if (JsonHelper::isDefined(object, currentPathJson, "autoTiming")) {
			UString error("Sprite3dAnimation at \"" + currentPathJson + "\" must contain either \"autoTiming\" or \"manualFrameOrder\" field but not both!");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}

		manualFrameOrder = JsonHelper::readIntKeyframes(object, currentPathJson, "manualFrameOrder", true);
		useManualFrameOrder = true;
	}
	else if (JsonHelper::isNumber(object, currentPathJson, "autoTiming")) {
		float value = JsonHelper::readNumberf(object, currentPathJson, "autoTiming", true);
		autoTiming.insert(autoTiming.end(), frameCount, value);
		useManualFrameOrder = false;
	}
	else if (JsonHelper::isArray(object, currentPathJson, "autoTiming")) {
		autoTiming = JsonHelper::readNumberfArray(object, currentPathJson, "autoTiming", true);
		useManualFrameOrder = false;
	}
	else {
		UString error("Sprite3dAnimation at \"" + currentPathJson + "\" must contain either \"autoTiming\" or \"manualFrameOrder\" field!");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return true;
}


UString Sprite3dAnimationParam::toString(const UString& newLinePadding) const
{
	(void)newLinePadding;

	UString str;
	str += "{ type=\"";
	str += animationTypeToString(type);
	str += "\", frameCount=";
	str.append2(frameCount);
	if (useManualFrameOrder == false) {
		str += ", autoTiming=[ ";
		for (auto& time : autoTiming) {
			str.append2(time);
			str += ", ";
		}
		str += "]";
	}
	else {
		str += ", manualFrameOrder=";
		str += manualFrameOrder.toString(newLinePadding);
	}
	str += " }";
	return str;
}


UString Sprite3dAnimationParam::toStringDebug(const UString& newLinePadding) const
{
	UString str("Sprite3dAnimationParam(");
	str += toString(newLinePadding);
	str += "\")";
	return str;
}


} // End namespace Verso
