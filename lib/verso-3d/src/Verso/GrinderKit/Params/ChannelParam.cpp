#include <Verso/GrinderKit/Params/ChannelParam.hpp>

namespace Verso {


ChannelParam::ChannelParam() :
	sources()
{
}


ChannelParam::ChannelParam(const ChannelParam& original) :
	sources(original.sources)
{
}


ChannelParam::ChannelParam(const ChannelParam&& original) :
	sources(std::move(original.sources))
{
}


ChannelParam& ChannelParam::operator =(const ChannelParam& original)
{
	if (this != &original) {
		sources = original.sources;
	}
	return *this;
}


ChannelParam& ChannelParam::operator =(ChannelParam&& original) noexcept
{
	if (this != &original) {
		sources = std::move(original.sources);
	}
	return *this;
}


ChannelParam::~ChannelParam()
{
}


bool ChannelParam::parseAttributeAsArray(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required)
{
	const JSONArray& channels = JsonHelper::readArray(object, currentPathJson, name, required);
	return parseArray(channels, currentPathJson + name + ".");
}


bool ChannelParam::parseArray(const JSONArray& array, const UString& currentPathJson)
{
	(void)currentPathJson;

	if (array.size() > 0) {
		for (JSONArray::const_iterator channelIt = array.begin(); channelIt != array.end(); ++channelIt) {
			if ((*channelIt) && (*channelIt)->IsString()) {
				sources.push_back((*channelIt)->AsString());
			}
		}
		return true;
	}
	return false;
}


UString ChannelParam::toString(const UString& newLinePadding) const
{
	(void)newLinePadding;

	UString str;
	str += "[ ";
	for (std::vector<UString>::const_iterator it = sources.begin(); it != sources.end(); ++it) {
		str += "\"";
		str += (*it);
		if (it != --sources.end())
			str += "\", ";
		else
			str += "\"";
	}
	str += " ]";
	return str;
}


UString ChannelParam::toStringDebug(const UString& newLinePadding) const
{
	UString str("ChannelParam(");
	str += toString(newLinePadding);
	str += "\")";
	return str;
}


} // End namespace Verso

