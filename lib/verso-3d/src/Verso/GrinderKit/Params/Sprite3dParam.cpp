#include <Verso/GrinderKit/Params/Sprite3dParam.hpp>

namespace Verso {

const BlendMode defaultBlendMode = BlendMode::Transcluent;
const RefScale defaultRefScale = RefScale::ViewportSize_KeepAspectRatio_FromX;

Verso::TextureParam textureParamDefault(
		"", "", Verso::TexturePixelFormat::Unset,
		Verso::MinFilter::Linear, Verso::MagFilter::Linear,
		Verso::WrapStyle::ClampToEdge, Verso::WrapStyle::ClampToEdge);


Sprite3dParam::Sprite3dParam() :
	texture(),
	blendMode(defaultBlendMode),
	alphaKeyframes(1.0f),
	positionKeyframes(Vector3f(0.0f, 0.0f, 0.0f)),
	relSizeKeyframes(Vector2f(1.0f, 1.0f)),
	refScale(defaultRefScale),
	yawAngleKeyframes(0.0f),
	pitchAngleKeyframes(0.0f),
	rollAngleKeyframes(0.0f),
	relOffset(0.0f, 0.0f),
	currentAnimationIndex(-1),
	animationParams()
{
}


Sprite3dParam::Sprite3dParam(const Sprite3dParam& original) :
	texture(original.texture),
	blendMode(original.blendMode),
	alphaKeyframes(original.alphaKeyframes),
	positionKeyframes(original.positionKeyframes),
	relSizeKeyframes(original.relSizeKeyframes),
	refScale(original.refScale),
	yawAngleKeyframes(original.yawAngleKeyframes),
	pitchAngleKeyframes(original.pitchAngleKeyframes),
	rollAngleKeyframes(original.rollAngleKeyframes),
	relOffset(original.relOffset),
	currentAnimationIndex(original.currentAnimationIndex),
	animationParams(original.animationParams)
{
}


Sprite3dParam::Sprite3dParam(const Sprite3dParam&& original) :
	texture(std::move(original.texture)),
	blendMode(std::move(original.blendMode)),
	alphaKeyframes(std::move(original.alphaKeyframes)),
	positionKeyframes(std::move(original.positionKeyframes)),
	relSizeKeyframes(std::move(original.relSizeKeyframes)),
	refScale(std::move(original.refScale)),
	yawAngleKeyframes(std::move(original.yawAngleKeyframes)),
	pitchAngleKeyframes(std::move(original.pitchAngleKeyframes)),
	rollAngleKeyframes(std::move(original.rollAngleKeyframes)),
	relOffset(std::move(original.relOffset)),
	currentAnimationIndex(std::move(original.currentAnimationIndex)),
	animationParams(std::move(original.animationParams))
{
}


Sprite3dParam& Sprite3dParam::operator =(const Sprite3dParam& original)
{
	if (this != &original) {
		texture = original.texture;
		blendMode = original.blendMode;
		alphaKeyframes = original.alphaKeyframes;
		positionKeyframes = original.positionKeyframes;
		relSizeKeyframes = original.relSizeKeyframes;
		refScale = original.refScale;
		yawAngleKeyframes = original.yawAngleKeyframes;
		pitchAngleKeyframes = original.pitchAngleKeyframes;
		rollAngleKeyframes = original.rollAngleKeyframes;
		relOffset = original.relOffset;
		currentAnimationIndex = original.currentAnimationIndex;
		animationParams = original.animationParams;
	}
	return *this;
}


Sprite3dParam& Sprite3dParam::operator =(Sprite3dParam&& original) noexcept
{
	if (this != &original) {
		texture = std::move(original.texture);
		blendMode = std::move(original.blendMode);
		alphaKeyframes = std::move(original.alphaKeyframes);
		positionKeyframes = std::move(original.positionKeyframes);
		relSizeKeyframes = std::move(original.relSizeKeyframes);
		refScale = std::move(original.refScale);
		yawAngleKeyframes = std::move(original.yawAngleKeyframes);
		pitchAngleKeyframes = std::move(original.pitchAngleKeyframes);
		rollAngleKeyframes = std::move(original.rollAngleKeyframes);
		relOffset = std::move(original.relOffset);
		currentAnimationIndex = std::move(original.currentAnimationIndex);
		animationParams = std::move(original.animationParams);
	}
	return *this;
}


Sprite3dParam::~Sprite3dParam()
{
}


std::vector<Sprite3dParam> Sprite3dParam::parseAttributeAsArray(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required)
{
	std::vector<Sprite3dParam> sprite3dParams;
	const JSONArray& sprites3dArr = JsonHelper::readArray(object, currentPathJson, name, true);
	if (sprites3dArr.size() > 0) {
		size_t i = 0;
		for (auto sprite3dIt = sprites3dArr.begin(); sprite3dIt != sprites3dArr.end(); ++sprite3dIt) {
			if ((*sprite3dIt) && (*sprite3dIt)->IsObject()) {
				Sprite3dParam sprite3dParam;
				auto& sprite3dObj = (*sprite3dIt)->AsObject();
				UString sprites3dCurrentPathJson = currentPathJson + name + "[";
				sprites3dCurrentPathJson.append2(i);
				sprites3dCurrentPathJson += "].";
				sprite3dParam.parseObject(sprite3dObj, sprites3dCurrentPathJson);
				sprite3dParams.push_back(sprite3dParam);
			}
			else {
				UString error("Array attribute \"" + name + "\" must contain only objects!");
				VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
			}
			i++;
		}
		return sprite3dParams;
	}
	else if (required == true) {
		UString error("Array attribute \"" + name + "\" must exist with at least one item!");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return sprite3dParams;
}


bool Sprite3dParam::parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required)
{
	JSONValue* valueTester = JsonHelper::readValue(object, currentPathJson, name);
	UString fieldCurrentPathJson(currentPathJson + name + ".");
	if (valueTester && valueTester->IsObject()) {
		const JSONObject& attributeObject = JsonHelper::readObject(object, currentPathJson, name);
		if (parseObject(attributeObject, fieldCurrentPathJson) == false) {
			UString error("Cannot read Sprite3d param from field \""+fieldCurrentPathJson+"\"!.");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}

		return parseObject(attributeObject, fieldCurrentPathJson);
	}
	else if (required == true) {
		UString error("Cannot read Sprite3d param from field \"" + fieldCurrentPathJson + "\"!.");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return true;
}


bool Sprite3dParam::parseObject(const JSONObject& object, const UString& currentPathJson)
{
	texture.parseAttributeAsObject(object, currentPathJson, "texture", true, textureParamDefault);

	blendMode = JsonHelper::readBlendMode(object, currentPathJson, "blend", false, defaultBlendMode);

	alphaKeyframes = JsonHelper::readFloatKeyframes(object, currentPathJson, "alpha", false, FloatKeyframes(1.0f));

	positionKeyframes = JsonHelper::readVector3fKeyframes(object, currentPathJson, "position", false, Vector3fKeyframes(Vector3f(0.0f, 0.0f, 0.0f)));

	refScale = JsonHelper::readRefScale(object, currentPathJson, "refScale", false, defaultRefScale);

	relSizeKeyframes = JsonHelper::readVector2fKeyframes(object, currentPathJson, "relSize", false, Vector2fKeyframes(Vector2f(0.0f, 0.0f)));

	yawAngleKeyframes = JsonHelper::readFloatKeyframes(object, currentPathJson, "yaw", false, FloatKeyframes(0.0f));

	pitchAngleKeyframes = JsonHelper::readFloatKeyframes(object, currentPathJson, "pitch", false, FloatKeyframes(0.0f));

	rollAngleKeyframes = JsonHelper::readFloatKeyframes(object, currentPathJson, "roll", false, FloatKeyframes(0.0f));

	relOffset = JsonHelper::readVector2fArrayFormat(object, currentPathJson, "relOffset", false, Vector2f(0.0f, 0.0f));

	currentAnimationIndex = JsonHelper::readIntKeyframes(object, currentPathJson, "currentAnimationIndex", false, IntKeyframes(-1));

	const JSONArray& animationsArr = JsonHelper::readArray(object, currentPathJson, "animations", false);
	if (animationsArr.size() > 0) {
		UString animationsCurrentPathJson = currentPathJson + ".animations[]";
		for (auto sprite3dIt = animationsArr.begin(); sprite3dIt != animationsArr.end(); ++sprite3dIt) {
			if ((*sprite3dIt) && (*sprite3dIt)->IsObject()) {
				Sprite3dAnimationParam animationParam;
				auto& animationObj = (*sprite3dIt)->AsObject();
				animationParam.parseObject(animationObj, animationsCurrentPathJson);
				animationParams.push_back(animationParam);
			}
			else {
				VERSO_ILLEGALFORMAT("verso-3d", "Array attribute \"animations\" must contain only objects!", "");
			}
		}
	}

	return true;
}


UString Sprite3dParam::toString(const UString& newLinePadding) const
{
	UString str;
	UString newLinePadding2 = newLinePadding + "  ";

	str += "{ texture=";
	str += texture.toString(newLinePadding2);
	str += ",\n" + newLinePadding2 + "blendMode=\"";
	str += blendModeToString(blendMode);
	str += "\",\n" + newLinePadding2 + "alpha=";
	str += alphaKeyframes.toString(newLinePadding2);
	str += ",\n" + newLinePadding2 + "position=";
	str += positionKeyframes.toString(newLinePadding2);
	str += ",\n" + newLinePadding2 + "relSize=";
	str += relSizeKeyframes.toString(newLinePadding2);
	str += ",\n" + newLinePadding2 + "refScale=";
	str += refScaleToString(refScale);
	str += ",\n" + newLinePadding2 + "yaw=";
	str += yawAngleKeyframes.toString(newLinePadding2);
	str += ",\n" + newLinePadding2 + "pitch=";
	str += pitchAngleKeyframes.toString(newLinePadding2);
	str += ",\n" + newLinePadding2 + "roll=";
	str += rollAngleKeyframes.toString(newLinePadding2);
	str += ",\n" + newLinePadding2 + "relOffset=";
	str += relOffset.toString();
	str += ",\n" + newLinePadding2 + "currentAnimationIndex=";
	str += currentAnimationIndex.toString(newLinePadding2);
	str += ",\n" + newLinePadding2 + "animations=[\n";
	UString newLinePadding3 = newLinePadding2 + "  ";
	for (auto& animationParam : animationParams) {
		str += newLinePadding3 + animationParam.toString(newLinePadding3) + "\n";
	}
	str += newLinePadding2 + "]\n";
	str += newLinePadding + "}";
	return str;
}


UString Sprite3dParam::toStringDebug(const UString& newLinePadding) const
{
	UString str("Sprite3dParam(");
	str += toString(newLinePadding);
	str += "\")";
	return str;
}


} // End namespace Verso
