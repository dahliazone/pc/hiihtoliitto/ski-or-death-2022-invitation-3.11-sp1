#include <Verso/GrinderKit/Params/ParticlesParam.hpp>

namespace Verso {


ParticlesParam::ParticlesParam() :
	particleLimit(0),
	position(),
	//material
	texture(),
	animatedTexture(false),
	trailSize(0),
	trailSaveInterval(0.0f),
	blend()
{
}


ParticlesParam::ParticlesParam(const ParticlesParam& original) :
	particleLimit(original.particleLimit),
	position(original.position),
	//material(original.material),
	texture(original.texture),
	animatedTexture(original.animatedTexture),
	trailSize(original.trailSize),
	trailSaveInterval(original.trailSaveInterval),
	blend(original.blend)
{
}


ParticlesParam::ParticlesParam(const ParticlesParam&& original) :
	particleLimit(std::move(original.particleLimit)),
	position(std::move(original.position)),
	//material(std::move(original.material)),
	texture(std::move(original.texture)),
	animatedTexture(std::move(original.animatedTexture)),
	trailSize(std::move(original.trailSize)),
	trailSaveInterval(std::move(original.trailSaveInterval)),
	blend(std::move(original.blend))
{
}


ParticlesParam& ParticlesParam::operator =(const ParticlesParam& original)
{
	if (this != &original) {
		particleLimit = original.particleLimit;
		position = original.position;
		//material = original.material;
		texture = original.texture;
		animatedTexture = original.animatedTexture;
		trailSize = original.trailSize;
		trailSaveInterval = original.trailSaveInterval;
		blend = original.blend;
	}
	return *this;
}


ParticlesParam& ParticlesParam::operator =(ParticlesParam&& original) noexcept
{
	if (this != &original) {
		particleLimit = std::move(original.particleLimit);
		position = std::move(original.position);
		//material = std::move(original.material);
		texture = std::move(original.texture);
		animatedTexture = std::move(original.animatedTexture);
		trailSize = std::move(original.trailSize);
		trailSaveInterval = std::move(original.trailSaveInterval);
		blend = std::move(original.blend);
	}
	return *this;
}


ParticlesParam::~ParticlesParam()
{
}


bool ParticlesParam::parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required)
{
	JSONValue* valueTester = JsonHelper::readValue(object, currentPathJson, name);
	UString fieldCurrentPathJson(currentPathJson + name + ".");
	if (valueTester && valueTester->IsObject()) {
		const JSONObject& attributeObject = JsonHelper::readObject(object, currentPathJson, name);
		if (parseObject(attributeObject, fieldCurrentPathJson) == false) {
			UString error("Cannot read Particles param from field \""+fieldCurrentPathJson+"\"!.");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}

		return parseObject(attributeObject, fieldCurrentPathJson);
	}
	else if (required == true) {
		UString error("Cannot read Particles param from field \"" + fieldCurrentPathJson + "\"!.");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return true;
}


bool ParticlesParam::parseObject(const JSONObject& object, const UString& currentPathJson)
{
	particleLimit = JsonHelper::readInt(object, currentPathJson, "particleLimit", false, 100); // \TODO: default is currently 5 in ParticleSystem

	position = JsonHelper::readVector3f(object, currentPathJson, "position", false, Vector3f(0.0f, 0.0f, 0.0f));

	//Material3dParam material; // \TODO: maybe also can change material and not just texture?

	texture = JsonHelper::readString(object, currentPathJson, "texture", true, "MISSING.png");

	animatedTexture = JsonHelper::readBool(object, currentPathJson, "animatedTexture", false, false);

	trailSize = JsonHelper::readInt(object, currentPathJson, "trailSize", false, 0);

	trailSaveInterval = JsonHelper::readNumberd(object, currentPathJson, "trailSaveInterval", false, 0.1);

	blend = JsonHelper::readBlendMode(object, currentPathJson, "blend", false, BlendMode::Additive);

	return true;
}


ParticleSystem* ParticlesParam::createParticleSystem(IWindowOpengl& window, const UString& runtimeId) const
{
	UString type = "CustomJson";
	ParticleSystem* particleSystem = new ParticleSystem("verso-3d/ParticleSystems/" + type + "::particleSystem: " + runtimeId);
	particleSystem->create(particleLimit);

	particleSystem->setPosition(position);
	//Material3dParam material; // \TODO: maybe also can change material and not just texture?
	particleSystem->setTexture(window, texture, animatedTexture);
	particleSystem->raiseTrailSize(trailSize);
	particleSystem->setTrailSaveInterval(trailSaveInterval);
	particleSystem->setBlendMode(blend);

	return particleSystem;
}


UString ParticlesParam::toString(const UString& newLinePadding) const
{
	(void)newLinePadding;

	UString str;
	str += "{ particleLimit=";
	str.append2(particleLimit);

	str += ", position=";
	str += position.toString();

	//Material3dParam material; // \TODO: maybe also can change material and not just texture?

	str += ", texture=\"";
	str += texture;

	str += "\", animatedTexture=";
	if (animatedTexture == true) {
		str += "true";
	}
	else {
		str += "false";
	}

	str += ", trailSize=";
	str.append2(trailSize);

	str += ", trailSaveInterval=";
	str.append2(trailSaveInterval);

	str += ", blend=";
	str += blendModeToString(blend);

	str += " }";

	return str;
}


UString ParticlesParam::toStringDebug(const UString& newLinePadding) const
{
	UString str("ParticlesParam(");
	str += toString(newLinePadding);
	str += "\")";
	return str;
}


} // End namespace Verso

