#include <Verso/GrinderKit/Params/PhongMaterialParam.hpp>

namespace Verso {


PhongMaterialParam::PhongMaterialParam() :
	ambient(),
	diffuse(),
	specular(),
	shininess()
{
}


PhongMaterialParam::PhongMaterialParam(const RgbaColorf& ambient,
										 const RgbaColorf& diffuse,
										 const RgbaColorf& specular,
										 float shininess) :
	ambient(ambient),
	diffuse(diffuse),
	specular(specular),
	shininess(shininess)
{
}


PhongMaterialParam::PhongMaterialParam(const PhongMaterialParam& original) :
	ambient(original.ambient),
	diffuse(original.diffuse),
	specular(original.specular),
	shininess(original.shininess)
{
}


PhongMaterialParam::PhongMaterialParam(PhongMaterialParam&& original) :
	ambient(std::move(original.ambient)),
	diffuse(std::move(original.diffuse)),
	specular(std::move(original.specular)),
	shininess(std::move(original.shininess))
{
}


PhongMaterialParam& PhongMaterialParam::operator =(const PhongMaterialParam& original)
{
	if (this != &original) {
		ambient = original.ambient;
		diffuse = original.diffuse;
		specular = original.specular;
		shininess = original.shininess;
	}
	return *this;
}


PhongMaterialParam& PhongMaterialParam::operator =(PhongMaterialParam&& original)
{
	if (this != &original) {
		ambient = std::move(original.ambient);
		diffuse = std::move(original.diffuse);
		specular = std::move(original.specular);
		shininess = std::move(original.shininess);
	}
	return *this;
}


PhongMaterialParam::~PhongMaterialParam()
{
}


bool PhongMaterialParam::parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required)
{
	JSONValue* valueTester = JsonHelper::readValue(object, currentPathJson, name);
	UString fieldCurrentPathJson(currentPathJson + name + ".");
	if (valueTester && valueTester->IsObject()) {
		const JSONObject& attributeObject = JsonHelper::readObject(object, currentPathJson, name);
		if (parseObject(attributeObject, fieldCurrentPathJson) == false) {
			UString error("Cannot read PhongMaterial param from field \""+fieldCurrentPathJson+"\"!.");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}

		return parseObject(attributeObject, fieldCurrentPathJson);
	}
	else if (required == true) {
		UString error("Cannot read PhongMaterial param from field \"" + fieldCurrentPathJson + "\"!.");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return true;
}


bool PhongMaterialParam::parseObject(const JSONObject& object, const UString& currentPathJson)
{
	ambient = JsonHelper::readColor(object, currentPathJson, "ambient", true);
	diffuse = JsonHelper::readColor(object, currentPathJson, "diffuse", true);
	specular = JsonHelper::readColor(object, currentPathJson, "specular", true);
	shininess = JsonHelper::readNumberf(object, currentPathJson, "shininess", true);

	return true;
}


///////////////////////////////////////////////////////////////////////////////////////////
// static default materials
///////////////////////////////////////////////////////////////////////////////////////////

PhongMaterialParam PhongMaterialParam::emerald()
{
	return PhongMaterialParam(RgbaColorf(0.0215f, 0.1745f, 0.0215f),
							   RgbaColorf(0.07568f, 0.61424f, 0.07568f),
							   RgbaColorf(0.633f, 0.727811f, 0.633f),
							   0.6f);
}

PhongMaterialParam PhongMaterialParam::jade()
{
	return PhongMaterialParam(RgbaColorf(0.135f, 0.2225f, 0.1575f),
							   RgbaColorf(0.54f, 0.89f, 0.63f),
							   RgbaColorf(0.316228f, 0.316228f, 0.316228f),
							   0.1f);
}

PhongMaterialParam PhongMaterialParam::obsidian()
{
	return PhongMaterialParam(RgbaColorf(0.05375f, 0.05f, 0.06625f),
							   RgbaColorf(0.18275f, 0.17f, 0.22525f),
							   RgbaColorf(0.332741f, 0.328634f, 0.346435f),
							   0.3f);
}

PhongMaterialParam PhongMaterialParam::pearl()
{
	return PhongMaterialParam(RgbaColorf(0.25f, 0.20725f, 0.20725f),
							   RgbaColorf(1.0f, 0.829f, 0.829f),
							   RgbaColorf(0.296648f, 0.296648f, 0.296648f),
							   0.088f);
}

PhongMaterialParam PhongMaterialParam::ruby()
{
	return PhongMaterialParam(RgbaColorf(0.1745f, 0.01175f, 0.01175f),
							   RgbaColorf(0.61424f, 0.04136f, 0.04136f),
							   RgbaColorf(0.727811f, 0.626959f, 0.626959f),
							   0.6f);
}

PhongMaterialParam PhongMaterialParam::turquoise()
{
	return PhongMaterialParam(RgbaColorf(0.1f, 0.18725f, 0.1745f),
							   RgbaColorf(0.396f, 0.74151f, 0.69102f),
							   RgbaColorf(0.297254f, 0.30829f, 0.306678f),
							   0.1f);
}

PhongMaterialParam PhongMaterialParam::brass()
{
	return PhongMaterialParam(RgbaColorf(0.329412f, 0.223529f, 0.027451f),
							   RgbaColorf(0.780392f, 0.568627f, 0.113725f),
							   RgbaColorf(0.992157f, 0.941176f, 0.807843f),
							   0.21794872f);
}

PhongMaterialParam PhongMaterialParam::bronze()
{
	return PhongMaterialParam(RgbaColorf(0.2125f, 0.1275f, 0.054f),
							   RgbaColorf(0.714f, 0.4284f, 0.18144f),
							   RgbaColorf(0.393548f, 0.271906f, 0.166721f),
							   0.2f);
}

PhongMaterialParam PhongMaterialParam::chrome()
{
	return PhongMaterialParam(RgbaColorf(0.25f, 0.25f, 0.25f),
							   RgbaColorf(0.4f, 0.4f, 0.4f),
							   RgbaColorf(0.774597f, 0.774597f, 0.774597f),
							   0.6f);
}

PhongMaterialParam PhongMaterialParam::copper()
{
	return PhongMaterialParam(RgbaColorf(0.19125f, 0.0735f, 0.0225f),
							   RgbaColorf(0.7038f, 0.27048f, 0.0828f),
							   RgbaColorf(0.256777f, 0.137622f, 0.086014f),
							   0.1f);
}

PhongMaterialParam PhongMaterialParam::gold()
{
	return PhongMaterialParam(RgbaColorf(0.24725f, 0.1995f, 0.0745f),
							   RgbaColorf(0.75164f, 0.60648f, 0.22648f),
							   RgbaColorf(0.628281f, 0.555802f, 0.366065f),
							   0.4f);
}

PhongMaterialParam PhongMaterialParam::silver()
{
	return PhongMaterialParam(RgbaColorf(0.19225f, 0.19225f, 0.19225f),
							   RgbaColorf(0.50754f, 0.50754f, 0.50754f),
							   RgbaColorf(0.508273f, 0.508273f, 0.508273f),
							   0.4f);
}

PhongMaterialParam PhongMaterialParam::blackPlastic()
{
	return PhongMaterialParam(RgbaColorf(0.0f, 0.0f, 0.0f),
							   RgbaColorf(0.01f, 0.01f, 0.01f),
							   RgbaColorf(0.50f, 0.50f, 0.50f),
							   0.25f);
}

PhongMaterialParam PhongMaterialParam::cyanPlastic()
{
	return PhongMaterialParam(RgbaColorf(0.0f, 0.1f, 0.06f),
							   RgbaColorf(0.0f, 0.50980392f, 0.50980392f),
							   RgbaColorf(0.50196078f, 0.50196078f, 0.50196078f),
							   0.25f);
}

PhongMaterialParam PhongMaterialParam::greenPlastic()
{
	return PhongMaterialParam(RgbaColorf(0.0f, 0.0f, 0.0f),
							   RgbaColorf(0.1f, 0.35f, 0.1f),
							   RgbaColorf(0.45f, 0.55f, 0.45f),
							   0.25f);
}

PhongMaterialParam PhongMaterialParam::redPlastic()
{
	return PhongMaterialParam(RgbaColorf(0.0f, 0.0f, 0.0f),
							   RgbaColorf(0.5f, 0.0f, 0.0f),
							   RgbaColorf(0.7f, 0.6f, 0.6f),
							   0.25f);
}

PhongMaterialParam PhongMaterialParam::whitePlastic()
{
	return PhongMaterialParam(RgbaColorf(0.0f, 0.0f, 0.0f),
							   RgbaColorf(0.55f, 0.55f, 0.55f),
							   RgbaColorf(0.70f, 0.70f, 0.70f),
							   0.25f);
}

PhongMaterialParam PhongMaterialParam::yellowPlastic()
{
	return PhongMaterialParam(RgbaColorf(0.0f, 0.0f, 0.0f),
							   RgbaColorf(0.5f, 0.5f, 0.0f),
							   RgbaColorf(0.60f, 0.60f, 0.50f),
							   0.25f);
}

PhongMaterialParam PhongMaterialParam::blackRubber()
{
	return PhongMaterialParam(RgbaColorf(0.02f, 0.02f, 0.02f),
							   RgbaColorf(0.01f, 0.01f, 0.01f),
							   RgbaColorf(0.4f, 0.4f, 0.4f),
							   0.078125f);
}

PhongMaterialParam PhongMaterialParam::cyanRubber()
{
	return PhongMaterialParam(RgbaColorf(0.0f, 0.05f, 0.05f),
							   RgbaColorf(0.4f, 0.5f, 0.5f),
							   RgbaColorf(0.04f, 0.7f, 0.7f),
							   0.078125f);
}

PhongMaterialParam PhongMaterialParam::greenRubber()
{
	return PhongMaterialParam(RgbaColorf(0.0f, 0.05f, 0.0f),
							   RgbaColorf(0.4f, 0.5f, 0.4f),
							   RgbaColorf(0.04f, 0.7f, 0.04f),
							   0.078125f);
}

PhongMaterialParam PhongMaterialParam::redRubber()
{
	return PhongMaterialParam(RgbaColorf(0.05f, 0.0f, 0.0f),
							   RgbaColorf(0.5f, 0.4f, 0.4f),
							   RgbaColorf(0.7f, 0.04f, 0.04f),
							   0.078125f);
}

PhongMaterialParam PhongMaterialParam::whiteRubber()
{
	return PhongMaterialParam(RgbaColorf(0.05f, 0.05f, 0.05f),
							   RgbaColorf(0.5f, 0.5f, 0.5f),
							   RgbaColorf(0.7f, 0.7f, 0.7f),
							   0.078125f);
}

PhongMaterialParam PhongMaterialParam::yellowRubber()
{
	return PhongMaterialParam(RgbaColorf(0.05f, 0.05f, 0.0f),
							   RgbaColorf(0.5f, 0.5f, 0.4f),
							   RgbaColorf(0.7f, 0.7f, 0.04f),
							   0.078125f);
}


///////////////////////////////////////////////////////////////////////////////////////////
// toString
///////////////////////////////////////////////////////////////////////////////////////////

UString PhongMaterialParam::toString(const UString& newLinePadding) const
{
	(void)newLinePadding;

	UString str;
	str += "{ ambient=(";
	str += ambient.toString();
	str += "), diffuse=(";
	str += diffuse.toString();
	str += "), specular=(";
	str += specular.toString();
	str += "), shininess=(";
	str.append2(shininess);
	str += ")";
	str += " }";
	return str;
}


UString PhongMaterialParam::toStringDebug(const UString& newLinePadding) const
{
	UString str("PhongMaterialParam(");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso

