#include <Verso/GrinderKit/Params/TextureParam.hpp>

namespace Verso {


const UString typeForShaderDefault = "";
const TexturePixelFormat pixelFormatDefault = TexturePixelFormat::Unset;
const MinFilter minFilterDefault = MinFilter::Default;
const MagFilter magFilterDefault = MagFilter::Default;
const WrapStyle wrapStyleSDefault = WrapStyle::Default;
const WrapStyle wrapStyleTDefault = WrapStyle::Default;


TextureParam::TextureParam() :
	sourceFileName(""),
	typeForShader(typeForShaderDefault),
	pixelFormat(pixelFormatDefault),
	minFilter(minFilterDefault),
	magFilter(magFilterDefault),
	wrapStyleS(wrapStyleSDefault),
	wrapStyleT(wrapStyleTDefault)
{
}



TextureParam::TextureParam(const TextureParam& original) :
	sourceFileName(original.sourceFileName),
	typeForShader(original.typeForShader),
	pixelFormat(original.pixelFormat),
	minFilter(original.minFilter),
	magFilter(original.magFilter),
	wrapStyleS(original.wrapStyleS),
	wrapStyleT(original.wrapStyleT)
{
}


TextureParam::TextureParam(const TextureParam&& original) :
	sourceFileName(std::move(original.sourceFileName)),
	typeForShader(std::move(original.typeForShader)),
	pixelFormat(std::move(original.pixelFormat)),
	minFilter(std::move(original.minFilter)),
	magFilter(std::move(original.magFilter)),
	wrapStyleS(std::move(original.wrapStyleS)),
	wrapStyleT(std::move(original.wrapStyleT))
{
}


TextureParam::TextureParam(const UString& sourceFileName, const UString& typeForShader,
						   const TexturePixelFormat& pixelFormat,
						   const MinFilter& minFilter, const MagFilter& magFilter,
						   const WrapStyle& wrapStyleS, const WrapStyle& wrapStyleT) :
	sourceFileName(sourceFileName),
	typeForShader(typeForShader),
	pixelFormat(pixelFormat),
	minFilter(minFilter),
	magFilter(magFilter),
	wrapStyleS(wrapStyleS),
	wrapStyleT(wrapStyleT)
{
}


TextureParam& TextureParam::operator =(const TextureParam& original)
{
	if (this != &original) {
		sourceFileName = original.sourceFileName;
		typeForShader = original.typeForShader;
		pixelFormat = original.pixelFormat;
		minFilter = original.minFilter;
		magFilter = original.magFilter;
		wrapStyleS = original.wrapStyleS;
		wrapStyleT = original.wrapStyleT;
	}
	return *this;
}


TextureParam& TextureParam::operator =(TextureParam&& original) noexcept
{
	if (this != &original) {
		sourceFileName = std::move(original.sourceFileName);
		typeForShader = std::move(original.typeForShader);
		pixelFormat = std::move(original.pixelFormat);
		minFilter = std::move(original.minFilter);
		magFilter = std::move(original.magFilter);
		wrapStyleS = std::move(original.wrapStyleS);
		wrapStyleT = std::move(original.wrapStyleT);
	}
	return *this;
}


TextureParam::~TextureParam()
{
}


bool TextureParam::parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const TextureParam& defaultValue)
{
	JSONValue* valueTester = JsonHelper::readValue(object, currentPathJson, name);
	UString fieldCurrentPathJson(currentPathJson + name + ".");
	if (valueTester && valueTester->IsObject()) {
		const JSONObject& attributeObject = JsonHelper::readObject(object, currentPathJson, name);
		if (parseObject(attributeObject, fieldCurrentPathJson, defaultValue) == false) {
			UString error("Cannot read Texture param from field \""+fieldCurrentPathJson+"\"!.");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}

		return true;
	}
	else if (required == true) {
		UString error("Cannot read Texture param from field \"" + fieldCurrentPathJson + "\"!.");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return true;
}


bool TextureParam::parseObject(const JSONObject& object, const UString& currentPathJson, const TextureParam& defaultValue)
{
	sourceFileName = JsonHelper::readString(object, currentPathJson, "source", true);

	typeForShader = JsonHelper::readString(object, currentPathJson, "typeForShader", false, defaultValue.typeForShader);

	pixelFormat = JsonHelper::readTexturePixelFormat(object, currentPathJson, "pixelFormat", false, defaultValue.pixelFormat);

	minFilter = JsonHelper::readMinFilter(object, currentPathJson, "minFilter", false, defaultValue.minFilter);

	magFilter = JsonHelper::readMagFilter(object, currentPathJson, "magFilter", false, defaultValue.magFilter);

	wrapStyleS = JsonHelper::readWrapStyle(object, currentPathJson, "wrapStyleS", false, defaultValue.wrapStyleS);

	wrapStyleT = JsonHelper::readWrapStyle(object, currentPathJson, "wrapStyleT", false, defaultValue.wrapStyleT);

	return true;
}


UString TextureParam::toString(const UString& newLinePadding) const
{
	UString newLinePadding2 = newLinePadding + "  ";
	UString str;
	str += "{ source=\"";
	str += sourceFileName;
	str += "\",\n" + newLinePadding2 + "typeForShader=\"";
	str += typeForShader;
	str += "\",\n" + newLinePadding2 + "pixelFormat=\"";
	str += texturePixelFormatToString(pixelFormat);
	str += ",\n" + newLinePadding2 + "minFilter=\"";
	str += minFilterToString(minFilter),
	str += "\",\n" + newLinePadding2 + "magFilter=\"";
	str += magFilterToString(magFilter);
	str += "\",\n" + newLinePadding2 + "wrapStyleS=\"";
	str += wrapStyleToString(wrapStyleS);
	str += "\",\n" + newLinePadding2 + "wrapStyleT=\"";
	str += wrapStyleToString(wrapStyleT);
	str += "\"\n" + newLinePadding + "}";
	return str;
}


UString TextureParam::toStringDebug(const UString& newLinePadding) const
{
	UString str("TextureParam(");
	str += toString(newLinePadding);
	str += "\")";
	return str;
}


} // End namespace Verso

