#include <Verso/GrinderKit/Params/BehaviourParam.hpp>
#include <Verso/Render/Particle/Behaviours/SimpleGravity.hpp>

namespace Verso {


BehaviourParam::BehaviourParam() :
	type(),
	gravity(0.0f)
{
}


BehaviourParam::BehaviourParam(const BehaviourParam& original) :
	type(original.type),
	gravity(original.gravity)
{
}


BehaviourParam::BehaviourParam(const BehaviourParam&& original) :
	type(std::move(original.type)),
	gravity(std::move(original.gravity))
{
}


BehaviourParam& BehaviourParam::operator =(const BehaviourParam& original)
{
	if (this != &original) {
		type = original.type;
		gravity = original.gravity;
	}
	return *this;
}


BehaviourParam& BehaviourParam::operator =(BehaviourParam&& original) noexcept
{
	if (this != &original) {
		type = std::move(original.type);
		gravity = std::move(original.gravity);
	}
	return *this;
}


BehaviourParam::~BehaviourParam()
{
}


bool BehaviourParam::parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required)
{
	JSONValue* valueTester = JsonHelper::readValue(object, currentPathJson, name);
	UString fieldCurrentPathJson(currentPathJson + name + ".");
	if (valueTester && valueTester->IsObject()) {
		const JSONObject& attributeObject = JsonHelper::readObject(object, currentPathJson, name);
		if (parseObject(attributeObject, fieldCurrentPathJson) == false) {
			UString error("Cannot read Behaviour param from field \""+fieldCurrentPathJson+"\"!.");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}

		return parseObject(attributeObject, fieldCurrentPathJson);
	}
	else if (required == true) {
		UString error("Cannot read Behaviour param from field \"" + fieldCurrentPathJson + "\"!.");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return true;
}


bool BehaviourParam::parseObject(const JSONObject& object, const UString& currentPathJson)
{
	type = JsonHelper::readString(object, currentPathJson, "type", true);
	if (type == SimpleGravity::getTypeStatic()) {
		gravity = JsonHelper::readNumberf(object, currentPathJson, "gravity", false, 9.81f);
	}

	else {
		UString error("Cannot find ParticleBehaviour with given type!");
		UString typeInfo("type = \""+type+"\"");
		VERSO_OBJECTNOTFOUND("verso-3d", error.c_str(), type.c_str());
	}

	return true;
}


ParticleBehaviour* BehaviourParam::createBehaviour() const
{
	if (type.equals(SimpleGravity::getTypeStatic())) {
		SimpleGravity* behaviour = new SimpleGravity();
		behaviour->setGravity(gravity);
		return behaviour;
	}

	else {
		UString error("Cannot create ParticleBehaviour with given type: ");
		error.append2(type);
		VERSO_OBJECTNOTFOUND("verso-3d", error.c_str(), "");
		return nullptr;
	}
}


UString BehaviourParam::toString(const UString& newLinePadding) const
{
	(void)newLinePadding;

	UString str;
	str += "{ type=\"";
	str += type;
	str += "\"";

	if (type == SimpleGravity::getTypeStatic()) {
		str += ", gravity=";
		str.append2(gravity);
	}

	else {
		str += ", error=\"Unknown type\"";
	}

	str += " }";

	return str;
}


UString BehaviourParam::toStringDebug(const UString& newLinePadding) const
{
	UString str("BehaviourParam(");
	str += toString(newLinePadding);
	str += "\")";
	return str;
}


} // End namespace Verso

