#include <Verso/GrinderKit/Params/ClearParam.hpp>

namespace Verso {


ClearParam::ClearParam(const ClearFlag& clearFlag, const RgbaColorfKeyframes& colorKeyframes) :
	clearFlag(clearFlag),
	colorKeyframes(colorKeyframes)
{
}


ClearParam::ClearParam(const ClearParam& original) :
	clearFlag(original.clearFlag),
	colorKeyframes(original.colorKeyframes)
{
}


ClearParam::ClearParam(const ClearParam&& original) :
	clearFlag(std::move(original.clearFlag)),
	colorKeyframes(std::move(original.colorKeyframes))
{
}


ClearParam& ClearParam::operator =(const ClearParam& original)
{
	if (this != &original) {
		clearFlag = original.clearFlag;
		colorKeyframes = original.colorKeyframes;
	}
	return *this;
}


ClearParam& ClearParam::operator =(ClearParam&& original) noexcept
{
	if (this != &original) {
		clearFlag = std::move(original.clearFlag);
		colorKeyframes = std::move(original.colorKeyframes);
	}
	return *this;
}


ClearParam::~ClearParam()
{
}


bool ClearParam::parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const ClearParam& defaults)
{
	JSONValue* valueTester = JsonHelper::readValue(object, currentPathJson, name);
	UString fieldCurrentPathJson(currentPathJson + name + ".");
	if (valueTester && valueTester->IsObject()) {
		const JSONObject& attributeObject = JsonHelper::readObject(object, currentPathJson, name);
		if (parseObject(attributeObject, fieldCurrentPathJson, defaults) == false) {
			UString error("Cannot read Clear param from field \""+fieldCurrentPathJson+"\"!.");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}

		return parseObject(attributeObject, fieldCurrentPathJson, defaults);
	}
	else if (required == true) {
		UString error("Cannot read Clear param from field \"" + fieldCurrentPathJson + "\"!.");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return true;
}


bool ClearParam::parseObject(const JSONObject& object, const UString& currentPathJson, const ClearParam& defaults)
{
	clearFlag = JsonHelper::readClearFlag(object, currentPathJson, "clearFlag", false, defaults.clearFlag);

	JSONValue* colorValue = JsonHelper::readValue(object, currentPathJson, "color");
	if (colorValue) {
		colorKeyframes = JsonHelper::readRgbaColorfKeyframes(object, currentPathJson, "color", false, defaults.colorKeyframes);
	}

	return true;
}


UString ClearParam::toString(const UString& newLinePadding) const
{
	UString str;
	str += "{ clearFlag=\"";
	str += clearFlagToString(clearFlag);
	str += "\", color=\"";
	str += colorKeyframes.toString(newLinePadding);
	str += "\" }";
	return str;
}


UString ClearParam::toStringDebug(const UString& newLinePadding) const
{
	UString str("ClearParam(");
	str += toString(newLinePadding);
	str += "\")";
	return str;
}


} // End namespace Verso

