#include <Verso/GrinderKit/Params/EmitterParam.hpp>
#include <Verso/Render/Particle/Emitters/SimpleRandom.hpp>
#include <Verso/Render/Particle/Emitters/SphericalRandom.hpp>

namespace Verso {


EmitterParam::EmitterParam() :
	type(),

	xRange(),
	yRange(),
	zRange(),
	angleRange(),
	angleVelocityRange(),
	startSizeRange(),
	endSizeRange(),
	totalLifeTimeRange(),
	redRange(),
	greenRange(),
	blueRange(),
	startAlphaRange(),
	endAlphaRange(),

	xVelocityRange(),
	yVelocityRange(),
	zVelocityRange(),

	radiusRange()
{
}


EmitterParam::EmitterParam(const EmitterParam& original) :
	type(original.type),

	xRange(original.xRange),
	yRange(original.yRange),
	zRange(original.zRange),
	angleRange(original.angleRange),
	angleVelocityRange(original.angleVelocityRange),
	startSizeRange(original.startSizeRange),
	endSizeRange(original.endSizeRange),
	totalLifeTimeRange(original.totalLifeTimeRange),
	redRange(original.redRange),
	greenRange(original.greenRange),
	blueRange(original.blueRange),
	startAlphaRange(original.startAlphaRange),
	endAlphaRange(original.endAlphaRange),

	xVelocityRange(original.xVelocityRange),
	yVelocityRange(original.yVelocityRange),
	zVelocityRange(original.zVelocityRange),

	radiusRange(original.radiusRange)
{
}


EmitterParam::EmitterParam(const EmitterParam&& original) :
	type(std::move(original.type)),

	xRange(std::move(original.xRange)),
	yRange(std::move(original.yRange)),
	zRange(std::move(original.zRange)),
	angleRange(std::move(original.angleRange)),
	angleVelocityRange(std::move(original.angleVelocityRange)),
	startSizeRange(std::move(original.startSizeRange)),
	endSizeRange(std::move(original.endSizeRange)),
	totalLifeTimeRange(std::move(original.totalLifeTimeRange)),
	redRange(std::move(original.redRange)),
	greenRange(std::move(original.greenRange)),
	blueRange(std::move(original.blueRange)),
	startAlphaRange(std::move(original.startAlphaRange)),
	endAlphaRange(std::move(original.endAlphaRange)),

	xVelocityRange(std::move(original.xVelocityRange)),
	yVelocityRange(std::move(original.yVelocityRange)),
	zVelocityRange(std::move(original.zVelocityRange)),

	radiusRange(std::move(original.radiusRange))
{
}


EmitterParam& EmitterParam::operator =(const EmitterParam& original)
{
	if (this != &original) {
		type = original.type;

		xRange = original.xRange;
		yRange = original.yRange;
		zRange = original.zRange;
		angleRange = original.angleRange;
		angleVelocityRange = original.angleVelocityRange;
		startSizeRange = original.startSizeRange;
		endSizeRange = original.endSizeRange;
		totalLifeTimeRange = original.totalLifeTimeRange;
		redRange = original.redRange;
		greenRange = original.greenRange;
		blueRange = original.blueRange;
		startAlphaRange = original.startAlphaRange;
		endAlphaRange = original.endAlphaRange;

		xVelocityRange = original.xVelocityRange;
		yVelocityRange = original.yVelocityRange;
		zVelocityRange = original.zVelocityRange;

		radiusRange = original.radiusRange;
	}
	return *this;
}


EmitterParam& EmitterParam::operator =(EmitterParam&& original) noexcept
{
	if (this != &original) {
		type = std::move(original.type);

		xRange = std::move(original.xRange);
		yRange = std::move(original.yRange);
		zRange = std::move(original.zRange);
		angleRange = std::move(original.angleRange);
		angleVelocityRange = std::move(original.angleVelocityRange);
		startSizeRange = std::move(original.startSizeRange);
		endSizeRange = std::move(original.endSizeRange);
		totalLifeTimeRange = std::move(original.totalLifeTimeRange);
		redRange = std::move(original.redRange);
		greenRange = std::move(original.greenRange);
		blueRange = std::move(original.blueRange);
		startAlphaRange = std::move(original.startAlphaRange);
		endAlphaRange = std::move(original.endAlphaRange);

		xVelocityRange = std::move(original.xVelocityRange);
		yVelocityRange = std::move(original.yVelocityRange);
		zVelocityRange = std::move(original.zVelocityRange);

		radiusRange = std::move(original.radiusRange);
	}
	return *this;
}


EmitterParam::~EmitterParam()
{
}


bool EmitterParam::parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required)
{
	JSONValue* valueTester = JsonHelper::readValue(object, currentPathJson, name);
	UString fieldCurrentPathJson(currentPathJson + name + ".");
	if (valueTester && valueTester->IsObject()) {
		const JSONObject& attributeObject = JsonHelper::readObject(object, currentPathJson, name);
		if (parseObject(attributeObject, fieldCurrentPathJson) == false) {
			UString error("Cannot read Emitter param from field \""+fieldCurrentPathJson+"\"!.");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}

		return parseObject(attributeObject, fieldCurrentPathJson);
	}
	else if (required == true) {
		UString error("Cannot read Emitter param from field \"" + fieldCurrentPathJson + "\"!.");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return true;
}


bool EmitterParam::parseObject(const JSONObject& object, const UString& currentPathJson)
{
	type = JsonHelper::readString(object, currentPathJson, "type", true);

	SimpleRandom defaultsSimpleRandom;
	xRange = JsonHelper::readRangef(object, currentPathJson, "x", false, defaultsSimpleRandom.getXRange());
	yRange = JsonHelper::readRangef(object, currentPathJson, "y", false, defaultsSimpleRandom.getYRange());
	zRange = JsonHelper::readRangef(object, currentPathJson, "z", false, defaultsSimpleRandom.getZRange());
	angleRange = JsonHelper::readRangef(object, currentPathJson, "angle", false, defaultsSimpleRandom.getAngleRange());
	angleVelocityRange = JsonHelper::readRangef(object, currentPathJson, "angleVelocity", false, defaultsSimpleRandom.getAngleVelocityRange());
	startSizeRange = JsonHelper::readRangef(object, currentPathJson, "startSize", false, defaultsSimpleRandom.getStartSizeRange());
	endSizeRange = JsonHelper::readRangef(object, currentPathJson, "endSize", false, defaultsSimpleRandom.getEndSizeRange());
	totalLifeTimeRange = JsonHelper::readRangef(object, currentPathJson, "totalLifeTime", false, defaultsSimpleRandom.getTotalLifeTimeRange());
	redRange = JsonHelper::readRangef(object, currentPathJson, "red", false, defaultsSimpleRandom.getRedRange());
	greenRange = JsonHelper::readRangef(object, currentPathJson, "green", false, defaultsSimpleRandom.getGreenRange());
	blueRange = JsonHelper::readRangef(object, currentPathJson, "blue", false, defaultsSimpleRandom.getBlueRange());
	startAlphaRange = JsonHelper::readRangef(object, currentPathJson, "startAlpha", false, defaultsSimpleRandom.getStartAlphaRange());
	endAlphaRange = JsonHelper::readRangef(object, currentPathJson, "endAlpha", false, defaultsSimpleRandom.getEndAlphaRange());

	if (type == SimpleRandom::getTypeStatic()) {
		xVelocityRange = JsonHelper::readRangef(object, currentPathJson, "xVelocity", false, defaultsSimpleRandom.getXVelocityRange());
		yVelocityRange = JsonHelper::readRangef(object, currentPathJson, "yVelocity", false, defaultsSimpleRandom.getYVelocityRange());
		zVelocityRange = JsonHelper::readRangef(object, currentPathJson, "zVelocity", false, defaultsSimpleRandom.getZVelocityRange());
	}

	else if (type == SphericalRandom::getTypeStatic()) {
		SphericalRandom defaultsSphericalRandom;
		radiusRange = JsonHelper::readRangef(object, currentPathJson, "radius", false, defaultsSphericalRandom.getRadiusRange());
	}

	else {
		UString error("Cannot find ParticleEmitter with given type!");
		UString typeInfo("type = \""+type+"\"");
		VERSO_OBJECTNOTFOUND("verso-3d", error.c_str(), type.c_str());
	}

	return true;
}


ParticleEmitter* EmitterParam::createEmitter() const
{
	if (type.equals(SimpleRandom::getTypeStatic())) {
		SimpleRandom* emitter = new SimpleRandom();
		emitter->setXRange(xRange);
		emitter->setYRange(yRange);
		emitter->setZRange(zRange);
		emitter->setAngleRange(angleRange);
		emitter->setAngleVelocityRange(angleVelocityRange);
		emitter->setStartSizeRange(startSizeRange);
		emitter->setEndSizeRange(endSizeRange);
		emitter->setTotalLifeTimeRange(totalLifeTimeRange);
		emitter->setRedRange(redRange);
		emitter->setGreenRange(greenRange);
		emitter->setBlueRange(blueRange);
		emitter->setStartAlphaRange(startAlphaRange);
		emitter->setEndAlphaRange(endAlphaRange);

		emitter->setXVelocityRange(xVelocityRange);
		emitter->setYVelocityRange(yVelocityRange);
		emitter->setZVelocityRange(zVelocityRange);
		return emitter;
	}

	else if (type.equals(SphericalRandom::getTypeStatic())) {
		SphericalRandom* emitter = new SphericalRandom();
		emitter->setXRange(xRange);
		emitter->setYRange(yRange);
		emitter->setZRange(zRange);
		emitter->setAngleRange(angleRange);
		emitter->setAngleVelocityRange(angleVelocityRange);
		emitter->setStartSizeRange(startSizeRange);
		emitter->setEndSizeRange(endSizeRange);
		emitter->setTotalLifeTimeRange(totalLifeTimeRange);
		emitter->setRedRange(redRange);
		emitter->setGreenRange(greenRange);
		emitter->setBlueRange(blueRange);
		emitter->setStartAlphaRange(startAlphaRange);
		emitter->setEndAlphaRange(endAlphaRange);

		emitter->setRadiusRange(radiusRange);
		return emitter;
	}

	else {
		UString error("Cannot create ParticleEmitter with given type: ");
		error.append2(type);
		VERSO_OBJECTNOTFOUND("verso-3d", error.c_str(), "");
		return nullptr;
	}
}


UString EmitterParam::toString(const UString& newLinePadding) const
{
	(void)newLinePadding;

	UString str;
	str += "{ type=\"";
	str += type;

	str += "\", xRange=";
	str += xRange.toString();
	str += ", yRange=";
	str += yRange.toString();
	str += ", zRange=";
	str += zRange.toString();

	str += ", angleRange=";
	str += angleRange.toString();
	str += ", angleVelocityRange=";
	str += angleVelocityRange.toString();

	str += ", startSizeRange=";
	str += startSizeRange.toString();
	str += ", endSizeRange=";
	str += endSizeRange.toString();

	str += ", totalLifeTimeRange=";
	str += totalLifeTimeRange.toString();

	str += ", redRange=";
	str += redRange.toString();
	str += ", greenRange=";
	str += greenRange.toString();
	str += ", blueRange=";
	str += blueRange.toString();

	str += ", startAlphaRange=";
	str += startAlphaRange.toString();
	str += ", endAlphaRange=";
	str += endAlphaRange.toString();

	if (type == SimpleRandom::getTypeStatic()) {
		str += ", xVelocityRange=";
		str += xVelocityRange.toString();
		str += ", yVelocityRange=";
		str += yVelocityRange.toString();
		str += ", zVelocityRange=";
		str += zVelocityRange.toString();
	}

	else if (type == SphericalRandom::getTypeStatic()) {
		str += ", radiusRange=";
		str += radiusRange.toString();
	}

	else {
		str += ", error=\"Unknown type\"";
	}

	str += " }";

	return str;
}


UString EmitterParam::toStringDebug(const UString& newLinePadding) const
{
	UString str("EmitterParam(");
	str += toString(newLinePadding);
	str += "\")";
	return str;
}


} // End namespace Verso

