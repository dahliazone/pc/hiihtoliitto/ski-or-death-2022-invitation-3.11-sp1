#include <Verso/GrinderKit/Params/FontFixedWidthParam.hpp>

namespace Verso {


FontFixedWidthParam::FontFixedWidthParam(
		const TextureParam& texture,
		size_t firstFontCharacterIndex, size_t tabSize) :
	texture(texture),
	firstFontCharacterIndex(firstFontCharacterIndex),
	tabSize(tabSize)
{
}


FontFixedWidthParam::FontFixedWidthParam(const FontFixedWidthParam& original) :
	texture(original.texture),
	firstFontCharacterIndex(original.firstFontCharacterIndex),
	tabSize(original.tabSize)
{
}


FontFixedWidthParam::FontFixedWidthParam(const FontFixedWidthParam&& original) :
	texture(std::move(original.texture)),
	firstFontCharacterIndex(std::move(original.firstFontCharacterIndex)),
	tabSize(std::move(original.tabSize))
{
}


FontFixedWidthParam& FontFixedWidthParam::operator =(const FontFixedWidthParam& original)
{
	if (this != &original) {
		texture = original.texture;
		firstFontCharacterIndex = original.firstFontCharacterIndex;
		tabSize = original.tabSize;
	}
	return *this;
}


FontFixedWidthParam& FontFixedWidthParam::operator =(FontFixedWidthParam&& original) noexcept
{
	if (this != &original) {
		texture = std::move(original.texture);
		firstFontCharacterIndex = std::move(original.firstFontCharacterIndex);
		tabSize = std::move(original.tabSize);
	}
	return *this;
}


FontFixedWidthParam::~FontFixedWidthParam()
{
}


bool FontFixedWidthParam::parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required)
{
	JSONValue* valueTester = JsonHelper::readValue(object, currentPathJson, name);
	UString fieldCurrentPathJson(currentPathJson + name + ".");
	if (valueTester && valueTester->IsObject()) {
		const JSONObject& attributeObject = JsonHelper::readObject(object, currentPathJson, name);
		if (parseObject(attributeObject, fieldCurrentPathJson) == false) {
			UString error("Cannot read FontFixedWidth param from field \""+fieldCurrentPathJson+"\"!.");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}

		return parseObject(attributeObject, fieldCurrentPathJson);
	}
	else if (required == true) {
		UString error("Cannot read FontFixedWidth param from field \"" + fieldCurrentPathJson + "\"!.");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return true;
}


bool FontFixedWidthParam::parseObject(const JSONObject& object, const UString& currentPathJson)
{
	texture.parseAttributeAsObject(object, currentPathJson, "texture", true);

	firstFontCharacterIndex = JsonHelper::readNumberu(object, currentPathJson, "firstFontCharacterIndex", false, 0);

	tabSize = JsonHelper::readNumberu(object, currentPathJson, "tabSize", false, 4);

	return true;
}


UString FontFixedWidthParam::toString(const UString& newLinePadding) const
{
	UString newLinePadding2 = newLinePadding + "  ";
	UString str;
	str += "{ texture=";
	str += texture.toString(newLinePadding2);
	str += ",\n" + newLinePadding2 + "firstFontCharacterIndex=";
	str.append2(firstFontCharacterIndex);
	str += ",\n" + newLinePadding2 + "tabSize=";
	str.append2(tabSize);
	str += "\n" + newLinePadding2 + "}";
	return str;
}


UString FontFixedWidthParam::toStringDebug(const UString& newLinePadding) const
{
	UString str("FontFixedWidthParam(");
	str += toString(newLinePadding);
	str += "\")";
	return str;
}


} // End namespace Verso

