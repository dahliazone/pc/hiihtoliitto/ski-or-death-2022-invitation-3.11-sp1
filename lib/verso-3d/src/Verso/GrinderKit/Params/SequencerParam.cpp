#include <Verso/GrinderKit/Params/SequencerParam.hpp>

namespace Verso {


SequencerParam::SequencerParam() :
	type(""),

	// type: BurstInterval
	sequenceType(SequenceType::None),
	startTime(0.0),
	endTime(0.0),
	neverEnding(false),
	interval(4.0),
	duration(2.0),
	burstSize(0),
	particlesPerSecond(9)
{
}


SequencerParam::SequencerParam(const SequencerParam& original) :
	type(original.type),

	sequenceType(original.sequenceType),
	startTime(original.startTime),
	endTime(original.endTime),
	neverEnding(original.neverEnding),
	interval(original.interval),
	duration(original.duration),
	burstSize(original.burstSize),
	particlesPerSecond(original.particlesPerSecond)
{
}


SequencerParam::SequencerParam(const SequencerParam&& original) :
	type(std::move(original.type)),

	sequenceType(std::move(original.sequenceType)),
	startTime(std::move(original.startTime)),
	endTime(std::move(original.endTime)),
	neverEnding(std::move(original.neverEnding)),
	interval(std::move(original.interval)),
	duration(std::move(original.duration)),
	burstSize(std::move(original.burstSize)),
	particlesPerSecond(std::move(original.particlesPerSecond))
{
}


SequencerParam& SequencerParam::operator =(const SequencerParam& original)
{
	if (this != &original) {
		type = original.type;

		sequenceType = original.sequenceType;
		startTime = original.startTime;
		endTime = original.endTime;
		neverEnding = original.neverEnding;
		interval = original.interval;
		duration = original.duration;
		burstSize = original.burstSize;
		particlesPerSecond = original.particlesPerSecond;
	}
	return *this;
}


SequencerParam& SequencerParam::operator =(SequencerParam&& original) noexcept
{
	if (this != &original) {
		type = std::move(original.type);

		sequenceType = std::move(original.sequenceType);
		startTime = std::move(original.startTime);
		endTime = std::move(original.endTime);
		neverEnding = std::move(original.neverEnding);
		interval = std::move(original.interval);
		duration = std::move(original.duration);
		burstSize = std::move(original.burstSize);
		particlesPerSecond = std::move(original.particlesPerSecond);
	}
	return *this;
}


SequencerParam::~SequencerParam()
{
}


bool SequencerParam::parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required)
{
	JSONValue* valueTester = JsonHelper::readValue(object, currentPathJson, name);
	UString fieldCurrentPathJson(currentPathJson + name + ".");
	if (valueTester && valueTester->IsObject()) {
		const JSONObject& attributeObject = JsonHelper::readObject(object, currentPathJson, name);
		if (parseObject(attributeObject, fieldCurrentPathJson) == false) {
			UString error("Cannot read Sequencer param from field \""+fieldCurrentPathJson+"\"!.");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}

		return parseObject(attributeObject, fieldCurrentPathJson);
	}
	else if (required == true) {
		UString error("Cannot read Sequencer param from field \"" + fieldCurrentPathJson + "\"!.");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return true;
}


bool SequencerParam::parseObject(const JSONObject& object, const UString& currentPathJson)
{
	type = JsonHelper::readString(object, currentPathJson, "type", true);

	if (type == BurstInterval::getTypeStatic()) {
		sequenceType = JsonHelper::readSequenceType(object, currentPathJson, "sequenceType", false, SequenceType::None);

		startTime = JsonHelper::readNumberd(object, currentPathJson, "startTime", false, 0.0);

		endTime = JsonHelper::readNumberd(object, currentPathJson, "endTime", false, 0.0);

		neverEnding = JsonHelper::readBool(object, currentPathJson, "neverEnding", false, false);

		interval = JsonHelper::readNumberd(object, currentPathJson, "interval", false, 4.0);

		duration = JsonHelper::readNumberd(object, currentPathJson, "duration", false, 2.0);

		burstSize = JsonHelper::readNumberu(object, currentPathJson, "burstSize", false, 0);

		particlesPerSecond = JsonHelper::readNumberu(object, currentPathJson, "particlesPerSecond", false, 0);
	}

	else {
		UString error("Cannot find ParticleSequencer with given type!");
		UString typeInfo("type = \""+type+"\"");
		VERSO_OBJECTNOTFOUND("verso-3d", error.c_str(), type.c_str());
	}

	return true;
}


ParticleSequencer* SequencerParam::createParticleSequencer() const
{
	if (type == BurstInterval::getTypeStatic()) {
		BurstInterval* tmpBurstInterval = new BurstInterval();

		tmpBurstInterval->setSequenceType(sequenceType);

		tmpBurstInterval->setStartTime(startTime);

		tmpBurstInterval->setEndTime(endTime);

		tmpBurstInterval->setNeverEnding(neverEnding);

		tmpBurstInterval->setInterval(interval);

		tmpBurstInterval->setDuration(duration);

		tmpBurstInterval->setBurstSize(burstSize);

		tmpBurstInterval->setParticlesPerSecond(particlesPerSecond);

		return tmpBurstInterval;
	}

	else {
		UString error("Cannot find ParticleSequencer with current type!");
		UString typeInfo("type = \""+type+"\"");
		VERSO_OBJECTNOTFOUND("verso-3d", error.c_str(), type.c_str());
		return nullptr;
	}
}


UString SequencerParam::toString(const UString& newLinePadding) const
{
	(void)newLinePadding;

	UString str;
	str += "{ type=\"";
	str += type;
	str += "\"";

	if (type == BurstInterval::getTypeStatic()) {
		str += ", sequenceType=";
		str += sequenceTypeToString(sequenceType);

		str += ", startTime=";
		str.append2(startTime);

		str += ", endTime=";
		str.append2(endTime);

		str += "\", neverEnding=";
		if (neverEnding == true) {
			str += "true";
		}
		else {
			str += "false";
		}

		str += ", interval=";
		str.append2(interval);

		str += ", duration=";
		str.append2(duration);

		str += ", burstSize=";
		str.append2(burstSize);

		str += ", particlesPerSecond=";
		str.append2(particlesPerSecond);
	}

	else {
		str += ", error=\"Unknown type\"";
	}

	str += "\" }";

	return str;
}


UString SequencerParam::toStringDebug(const UString& newLinePadding) const
{
	UString str("SequencerParam(");
	str += toString(newLinePadding);
	str += "\")";
	return str;
}


} // End namespace Verso

