#include <Verso/GrinderKit/Params/CameraParam.hpp>
#include <Verso/System/JsonHelper3d.hpp>

namespace Verso {


const CameraType defaultCameraType(CameraType::Target);
const ProjectionType defaultProjectionType(ProjectionType::Orthographic);
const Vector3f defaultPosition(0.0f, 0.0f, 0.0f);
const Vector3f defaultTarget(0.0f, 0.0f, 10.0f);
const Vector3f defaultDesiredUp(Vector3f::up());
const float defaultFovY(90.0f);
const Rangef defaultNearFarPlane(0.1f, 1000.0f);
const bool defaultOrthographicIsRelative(false);
const float defaultOrthographicZoomLevel(1.0f);
const float defaultOrthographicRotation(0.0f);
const float defaultOrthographicLeft(-1.0f);
const float defaultOrthographicRight(1.0f);
const float defaultOrthographicTop(-1.0f);
const float defaultOrthographicBottom(1.0f);


CameraParam::CameraParam() :
	cameraType(defaultCameraType),
	projectionType(defaultProjectionType),
	positionKeyframes(defaultPosition),
	targetKeyframes(defaultTarget),
	desiredUpKeyframes(defaultDesiredUp),
	fovYKeyframes(defaultFovY),
	nearFarPlane(defaultNearFarPlane),
	orthographicIsRelative(defaultOrthographicIsRelative),
	orthographicZoomLevelKeyframes(defaultOrthographicZoomLevel),
	orthographicRotationKeyframes(defaultOrthographicRotation),
	orthographicLeft(defaultOrthographicLeft),
	orthographicRight(defaultOrthographicRight),
	orthographicTop(defaultOrthographicTop),
	orthographicBottom(defaultOrthographicBottom)
{
}


CameraParam::CameraParam(const CameraParam& original) :
	cameraType(original.cameraType),
	projectionType(original.projectionType),
	positionKeyframes(original.positionKeyframes),
	targetKeyframes(original.targetKeyframes),
	desiredUpKeyframes(original.desiredUpKeyframes),
	fovYKeyframes(original.fovYKeyframes),
	nearFarPlane(original.nearFarPlane),
	orthographicIsRelative(original.orthographicIsRelative),
	orthographicZoomLevelKeyframes(original.orthographicZoomLevelKeyframes),
	orthographicRotationKeyframes(original.orthographicRotationKeyframes),
	orthographicLeft(original.orthographicLeft),
	orthographicRight(original.orthographicRight),
	orthographicTop(original.orthographicTop),
	orthographicBottom(original.orthographicBottom)
{
}


CameraParam::CameraParam(const CameraParam&& original) :
	cameraType(std::move(original.cameraType)),
	projectionType(std::move(original.projectionType)),
	positionKeyframes(std::move(original.positionKeyframes)),
	targetKeyframes(std::move(original.targetKeyframes)),
	desiredUpKeyframes(std::move(original.desiredUpKeyframes)),
	fovYKeyframes(std::move(original.fovYKeyframes)),
	nearFarPlane(std::move(original.nearFarPlane)),
	orthographicIsRelative(std::move(original.orthographicIsRelative)),
	orthographicZoomLevelKeyframes(std::move(original.orthographicZoomLevelKeyframes)),
	orthographicRotationKeyframes(std::move(original.orthographicRotationKeyframes)),
	orthographicLeft(std::move(original.orthographicLeft)),
	orthographicRight(std::move(original.orthographicRight)),
	orthographicTop(std::move(original.orthographicTop)),
	orthographicBottom(std::move(original.orthographicBottom))
{
}


CameraParam::CameraParam(
		CameraType cameraType, ProjectionType projectionType,
		const Vector3fKeyframes& positionKeyframes, const Vector3fKeyframes& targetKeyframes,
		const Vector3fKeyframes& desiredUpKeyframes,
		const FloatKeyframes& fovYKeyframes, const Rangef& nearFarPlane, bool orthographicIsRelative,
		const FloatKeyframes& orthographicZoomLevelKeyframes,
		const FloatKeyframes& orthographicRotationKeyframes,
		float orthographicLeft, float orthographicRight, float orthographicTop, float orthographicBottom) :
	cameraType(cameraType),
	projectionType(projectionType),
	positionKeyframes(positionKeyframes),
	targetKeyframes(targetKeyframes),
	desiredUpKeyframes(desiredUpKeyframes),
	fovYKeyframes(fovYKeyframes),
	nearFarPlane(nearFarPlane),
	orthographicIsRelative(orthographicIsRelative),
	orthographicZoomLevelKeyframes(orthographicZoomLevelKeyframes),
	orthographicRotationKeyframes(orthographicRotationKeyframes),
	orthographicLeft(orthographicLeft),
	orthographicRight(orthographicRight),
	orthographicTop(orthographicTop),
	orthographicBottom(orthographicBottom)
{
}


CameraParam& CameraParam::operator =(const CameraParam& original)
{
	if (this != &original) {
		cameraType = original.cameraType;
		projectionType = original.projectionType;
		positionKeyframes = original.positionKeyframes;
		targetKeyframes = original.targetKeyframes;
		desiredUpKeyframes = original.desiredUpKeyframes;
		fovYKeyframes = original.fovYKeyframes;
		nearFarPlane = original.nearFarPlane;
		orthographicIsRelative = original.orthographicIsRelative;
		orthographicZoomLevelKeyframes = original.orthographicZoomLevelKeyframes;
		orthographicRotationKeyframes = original.orthographicRotationKeyframes;
		orthographicLeft = original.orthographicLeft;
		orthographicRight = original.orthographicRight;
		orthographicTop = original.orthographicTop;
		orthographicBottom = original.orthographicBottom;
	}
	return *this;
}


CameraParam& CameraParam::operator =(CameraParam&& original) noexcept
{
	if (this != &original) {
		cameraType = std::move(original.cameraType);
		projectionType = std::move(original.projectionType);
		positionKeyframes = std::move(original.positionKeyframes);
		targetKeyframes = std::move(original.targetKeyframes);
		desiredUpKeyframes = std::move(original.desiredUpKeyframes);
		fovYKeyframes = std::move(original.fovYKeyframes);
		nearFarPlane = std::move(original.nearFarPlane);
		orthographicIsRelative = std::move(original.orthographicIsRelative);
		orthographicZoomLevelKeyframes = std::move(original.orthographicZoomLevelKeyframes);
		orthographicRotationKeyframes = std::move(original.orthographicRotationKeyframes);
		orthographicLeft = std::move(original.orthographicLeft);
		orthographicRight = std::move(original.orthographicRight);
		orthographicTop = std::move(original.orthographicTop);
		orthographicBottom = std::move(original.orthographicBottom);
	}
	return *this;
}


CameraParam::~CameraParam()
{
}


bool CameraParam::parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const CameraParam& defaultValue)
{
	JSONValue* valueTester = JsonHelper::readValue(object, currentPathJson, name);
	UString fieldCurrentPathJson(currentPathJson + name + ".");
	if (valueTester && valueTester->IsObject()) {
		const JSONObject& attributeObject = JsonHelper::readObject(object, currentPathJson, name);
		if (parseObject(attributeObject, fieldCurrentPathJson, defaultValue) == false) {
			UString error("Cannot read Camera param from field \""+fieldCurrentPathJson+"\"!.");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}

		return true;
	}
	else if (required == true) {
		UString error("Cannot read Camera param from field \"" + fieldCurrentPathJson + "\"!.");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return true;
}

bool CameraParam::parseObject(const JSONObject& object, const UString& currentPathJson, const CameraParam& defaultValue)
{
	cameraType = JsonHelper::readCameraType(object, currentPathJson, "type", true);

	projectionType = JsonHelper::readProjectionType(object, currentPathJson, "projectionType", true);

	positionKeyframes = JsonHelper::readVector3fKeyframes(object, currentPathJson, "position", false, defaultValue.positionKeyframes);

	targetKeyframes = JsonHelper::readVector3fKeyframes(object, currentPathJson, "target", false, defaultValue.targetKeyframes);

	desiredUpKeyframes = JsonHelper::readVector3fKeyframes(object, currentPathJson, "desiredUp", false, defaultValue.desiredUpKeyframes);

	fovYKeyframes = JsonHelper::readFloatKeyframes(object, currentPathJson, "fovY", false, defaultValue.fovYKeyframes);

	nearFarPlane.minValue = JsonHelper::readNumberf(object, currentPathJson, "nearPlane", false, defaultValue.nearFarPlane.minValue);

	nearFarPlane.maxValue = JsonHelper::readNumberf(object, currentPathJson, "farPlane", false, defaultValue.nearFarPlane.maxValue);

	JSONValue* orthographicTester = JsonHelper::readValue(object, currentPathJson, "orthographic");
	if (orthographicTester && orthographicTester->IsObject()) {
		const JSONObject& orthoObj = JsonHelper::readObject(object, currentPathJson, "orthographic");

		UString orthoCurrentPathJson = currentPathJson + ".orthographic";

		orthographicIsRelative = JsonHelper::readBool(orthoObj, orthoCurrentPathJson, "isRelative", false, defaultValue.orthographicIsRelative);

		orthographicZoomLevelKeyframes = JsonHelper::readFloatKeyframes(orthoObj, orthoCurrentPathJson, "zoomLevel", false, defaultValue.orthographicZoomLevelKeyframes);

		orthographicRotationKeyframes = JsonHelper::readFloatKeyframes(orthoObj, orthoCurrentPathJson, "rotation", false, defaultValue.orthographicRotationKeyframes);

		orthographicLeft = JsonHelper::readNumberf(orthoObj, orthoCurrentPathJson, "left", false, defaultValue.orthographicLeft);

		orthographicRight = JsonHelper::readNumberf(orthoObj, orthoCurrentPathJson, "right", false, defaultValue.orthographicRight);

		orthographicTop = JsonHelper::readNumberf(orthoObj, orthoCurrentPathJson, "top", false, defaultValue.orthographicTop);

		orthographicBottom = JsonHelper::readNumberf(orthoObj, orthoCurrentPathJson, "bottom", false, defaultValue.orthographicBottom);
	}
	else {
		orthographicIsRelative = defaultValue.orthographicIsRelative;

		orthographicZoomLevelKeyframes = defaultValue.orthographicZoomLevelKeyframes;

		orthographicRotationKeyframes = defaultValue.orthographicRotationKeyframes;

		orthographicLeft = defaultValue.orthographicLeft;

		orthographicRight = defaultValue.orthographicRight;

		orthographicTop = defaultValue.orthographicTop;

		orthographicBottom = defaultValue.orthographicBottom;
	}

	return true;
}


UString CameraParam::toString(const UString& newLinePadding) const
{
	UString str;
	UString newLinePadding2 = newLinePadding + "  ";
	str += "{ type=\"";
	str += cameraTypeToString(cameraType);
	str += "\",\n" + newLinePadding2 + "projectionType=\"";
	str += projectionTypeToString(projectionType);
	str += "\",\n" + newLinePadding2 + "position=";
	str += positionKeyframes.toString(newLinePadding2);
	str += ",\n" + newLinePadding2 + "target=";
	str += targetKeyframes.toString(newLinePadding2);
	str += ",\n" + newLinePadding2 + "desiredUp=";
	str += desiredUpKeyframes.toString(newLinePadding2);
	str += ",\n" + newLinePadding2 + "fovY=";
	str += fovYKeyframes.toString(newLinePadding2);
	str += ",\n" + newLinePadding2 + "nearFarPlane=";
	str += nearFarPlane.toString();
	str += ",\n" + newLinePadding2 + "orthographic = {";
	{
		UString newLinePadding3 = newLinePadding2 + "  ";
		str += "\n" + newLinePadding3 + "isRelative=";
		if (orthographicIsRelative) {
			str += "true,";
		}
		else {
			str += "false,";
		}
		str += "\n" + newLinePadding3 + "zoomLevel=";
		str += orthographicZoomLevelKeyframes.toString(newLinePadding3);
		str += ",\n" + newLinePadding3 + "rotation=";
		str += orthographicRotationKeyframes.toString(newLinePadding3);
		str += ",\n" + newLinePadding3 + "left=";
		str.append2(orthographicLeft);
		str += ",\n" + newLinePadding3 + "right=";
		str.append2(orthographicRight);
		str += ",\n" + newLinePadding3 + "top=";
		str.append2(orthographicTop);
		str += ",\n" + newLinePadding3 + "bottom=";
		str.append2(orthographicBottom);
		str += ",\n" + newLinePadding2 + "}";
	}
	str += "\n}";
	return str;
}


UString CameraParam::toStringDebug(const UString& newLinePadding) const
{
	UString str("CameraParam(");
	str += toString(newLinePadding);
	str += "\")";
	return str;
}


} // End namespace Verso
