#include <Verso/GrinderKit/Params/DepthParam.hpp>

namespace Verso {


DepthParam::DepthParam(bool depthTest, bool depthWrite) :
	depthTest(depthTest),
	depthWrite(depthWrite)
{
}


DepthParam::DepthParam(const DepthParam& original) :
	depthTest(original.depthTest),
	depthWrite(original.depthWrite)
{
}


DepthParam::DepthParam(const DepthParam&& original) :
	depthTest(std::move(original.depthTest)),
	depthWrite(std::move(original.depthWrite))
{
}


DepthParam& DepthParam::operator =(const DepthParam& original)
{
	if (this != &original) {
		depthTest = original.depthTest;
		depthWrite = original.depthWrite;
	}
	return *this;
}


DepthParam& DepthParam::operator =(DepthParam&& original) noexcept
{
	if (this != &original) {
		depthTest = std::move(original.depthTest);
		depthWrite = std::move(original.depthWrite);
	}
	return *this;
}


DepthParam::~DepthParam()
{
}


bool DepthParam::parseAttributeAsObject(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const DepthParam& defaults)
{
	JSONValue* valueTester = JsonHelper::readValue(object, currentPathJson, name);
	UString fieldCurrentPathJson(currentPathJson + name + ".");
	if (valueTester && valueTester->IsObject()) {
		const JSONObject& attributeObject = JsonHelper::readObject(object, currentPathJson, name);
		if (parseObject(attributeObject, fieldCurrentPathJson, defaults) == false) {
			UString error("Cannot read Depth param from field \""+fieldCurrentPathJson+"\"!.");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}

		return parseObject(attributeObject, fieldCurrentPathJson, defaults);
	}
	else if (required == true) {
		UString error("Cannot read Depth param from field \"" + fieldCurrentPathJson + "\"!.");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return true;
}


bool DepthParam::parseObject(const JSONObject& object, const UString& currentPathJson, const DepthParam& defaults)
{
	depthTest = JsonHelper::readBool(object, currentPathJson, "test", false, defaults.depthTest);

	depthWrite = JsonHelper::readBool(object, currentPathJson, "write", false, defaults.depthWrite);

	return true;
}


UString DepthParam::toString(const UString& newLinePadding) const
{
	(void)newLinePadding;

	UString str;
	str += "{ test=";
	if (depthTest == true) {
		str += "true";
	}
	else {
		str += "false";
	}
	str += ", write=";
	if (depthWrite == true) {
		str += "true";
	}
	else {
		str += "false";
	}
	str += " }";
	return str;
}


UString DepthParam::toStringDebug(const UString& newLinePadding) const
{
	UString str("DepthParam(");
	str += toString(newLinePadding);
	str += "\")";
	return str;
}


} // End namespace Verso

