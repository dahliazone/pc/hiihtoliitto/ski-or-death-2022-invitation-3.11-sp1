#ifndef VERSO_GFX_TODO_INSPECT_HPP
#define VERSO_GFX_TODO_INSPECT_HPP

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include "string_convert.hpp"

namespace Verso {


template <typename Iter>
Iter next(Iter iter)
{
	return ++iter;
}

template <typename Iter, typename Container>
bool isLast(Iter iter, const Container& cont)
{
	return (iter != cont.end()) && (next(iter) == cont.end());
}


class Inspect
{
public:
	template<typename T>
	inline static void printVariable(const string& variableName, const T& variable)
	{
		cout << variableName<<" = "<<variable << endl;
	}


	template<typename T>
	inline static void printVector(const string& variableName, const std::vector<T>& vector)
	{
		cout << variableName<<" = [ ";
		typename std::vector<T>::const_iterator it;
		for (it=vector.begin(); it!=vector.end(); ++it) {
			cout << (*it);
			if (!isLast(it, vector))
				cout <<", ";
		}
		cout << " ]" << endl;
	}


	template<typename S, typename T>
	inline static void printMap(const string& variableName, const map<S, T>& map)
	{
		cout << variableName<<" = [ ";
		typedef map<S, T> Map;
		typedef typename Map::const_iterator MapIter;
		MapIter it;
		for (it=map.begin(); it!=map.end(); ++it) {
			cout << (*it).first<<" => "<<(*it).second;
			if (!isLast<MapIter, Map>(it, map))
				cout <<", ";
		}
		cout << " ]" << endl;
	}
};


} // End namespace Verso

#endif // End header guard

