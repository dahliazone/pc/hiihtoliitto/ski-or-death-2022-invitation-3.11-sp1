#include <Trieng/Font2dSdl2Opengl.hpp>
#include <cstddef>

namespace Trieng {


static const UString BASENAME = "Font2dSdl2Opengl::vao";


Font2dSdl2Opengl::Font2dSdl2Opengl(AppWindowSdl2Opengl* renderer) :
	renderer(renderer),
	sourceFileName(""),
	font(nullptr),
	tex(nullptr),
	shader(),
	vao(true, BASENAME)
{
}


Font2dSdl2Opengl::~Font2dSdl2Opengl()
{
	Font2dSdl2Opengl::freeTexture();

	if (font != nullptr)
		TTF_CloseFont(font);

	// TODO: !!! We should call TTF_Quit(); at quit
}


bool Font2dSdl2Opengl::setShader(const UString& vertexShaderFileName, const UString& fragmentShaderFileName)
{
	// Init color shader
//	UString vertexShaderFileName = "gui-color.330.vert";
//	UString fragmentShaderFileName = "gui-color.330.frag";
//	colorShader.init();
//	colorShader.compileAndAddShaderFromFile(
//				getApp()->getDataShadersPath()+vertexShaderFileName, Shader::VERTEX_SHADER);
//	colorShader.compileAndAddShaderFromFile(
//				getApp()->getDataShadersPath()+fragmentShaderFileName, Shader::FRAGMENT_SHADER);
//	colorShader.link();
//	colorShader.use();
//	colorShader.setUniform4f("BgColor", 1, 1, 1, 1.0f);

	// Init texture shader
	shader.init();
	if (shader.compileAndAddShaderFromFile(
				vertexShaderFileName, Shader::VERTEX_SHADER) == false)
		return false;

	if (shader.compileAndAddShaderFromFile(
				fragmentShaderFileName, Shader::FRAGMENT_SHADER) == false)
		return false;
	shader.link();
	shader.use();
	shader.setUniform1i("Tex1", 0);

	return true;
}


bool Font2dSdl2Opengl::loadFont(const UString& fileName, int pointSize)
{
	if (!TTF_WasInit() && TTF_Init() == -1) {
		cerr << "Error in Font2dSdl2Opengl::loadFont(): "<<TTF_GetError() << endl;
		return false;
	}

	font = TTF_OpenFont(fileName.c_str(), pointSize);
	if (!font) {
		cerr << "Error in Font2dSdl2Opengl::loadFont(): "<<TTF_GetError() << endl;
		return false;
	}
	TTF_SetFontStyle(font, TTF_STYLE_NORMAL);
	//renderstyle |= TTF_STYLE_BOLD;
	//renderstyle |= TTF_STYLE_ITALIC;
	//renderstyle |= TTF_STYLE_UNDERLINE;

	sourceFileName = fileName;
	UString name = BASENAME;
	name += " for \"";
	name += sourceFileName;
	name += "\"";
	vao.setName(name);

	return true;
}


void Font2dSdl2Opengl::freeTexture()
{
	if (tex != nullptr)
		delete tex;
}


Texture* Font2dSdl2Opengl::generateTexture(const UString& text, const RgbaColorf& color)
{
	if (font == nullptr) {
		cerr << "Error in void Font2dSdl2Opengl::generateTexture(): Font is not loaded!" << endl;
		return nullptr;
	}

	RgbaColorf tc = color.getScaledHue(255);
	SDL_Color sdlColor = { (unsigned char)tc.r, (unsigned char)tc.g, (unsigned char)tc.b, (unsigned char)tc.a };
	SDL_Surface* tempSurface = TTF_RenderUTF8_Blended(font, text.toUtf8(), sdlColor);

	size_t w = Math::toNearestPowerOfTwo(tempSurface->w);
	size_t h = Math::toNearestPowerOfTwo(tempSurface->w);

	//GLfloat texMinU = 0.0f;
	//GLfloat texMinV = 0.0f;
	//GLfloat texMaxU = (GLfloat)surface->w / w;
	//GLfloat texMaxV = (GLfloat)surface->h / h;

	SDL_Surface* sdlSurface = SDL_CreateRGBSurface(
				SDL_SWSURFACE, w, h, 32,
			#if SDL_BYTEORDER == SDL_LIL_ENDIAN /* OpenGL RGBA masks */
				0x000000FF,	0x0000FF00, 0x00FF0000, 0xFF000000
			#else
				0xFF000000, 0x00FF0000, 0x0000FF00, 0x000000FF
			#endif
				);
	if (sdlSurface == nullptr) {
		cerr << "Error in Texture* Font2dSdl2Opengl::generateTexture(): Could not create SDL_Surface!" << endl;
		return nullptr;
	}

	// Save the alpha blending attributes
	unsigned char savedAlphaMod;
	SDL_BlendMode savedBlendMode;
	SDL_GetSurfaceAlphaMod(tempSurface, &savedAlphaMod);
	SDL_SetSurfaceAlphaMod(tempSurface, 0xFF);
	SDL_GetSurfaceBlendMode(tempSurface, &savedBlendMode);
	SDL_SetSurfaceBlendMode(tempSurface, SDL_BLENDMODE_NONE);

	// Copy the surface into the GL texture image
	SDL_Rect area;
	area.x = 0;
	area.y = 0;
	area.w = tempSurface->w;
	area.h = tempSurface->h;
	SDL_BlitSurface(tempSurface, &area, sdlSurface, &area);

	// Restore the alpha blending attributes
	SDL_SetSurfaceAlphaMod(tempSurface, savedAlphaMod);
	SDL_SetSurfaceBlendMode(tempSurface, savedBlendMode);

	// Free old texture
	freeTexture();

	// Create new texture
	tex = new Texture();
	tex->createAndCopy((unsigned char*)sdlSurface->pixels, GL_RGBA, sdlSurface->w, sdlSurface->h, true, false, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);
	tex->setTexCoord(Rect<GLfloat>(0.0f, 0.0f, (GLfloat)tempSurface->w / (GLfloat)w, (GLfloat)tempSurface->h / (GLfloat)h));

	// SET mag/min filters??!?!?!?!
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	SDL_FreeSurface(tempSurface);
	SDL_FreeSurface(sdlSurface);

	return tex;
}


void Font2dSdl2Opengl::renderString(Camera& camera, const Vector3f& pos, const UString& text, const RgbaColorf& color, const Align& align)
{
	(void)align; // TODO: implement align

	Texture* tex = generateTexture(text, color);
	if (tex == nullptr) {
		cerr << "Error in void Font2dSdl2Opengl::renderString(): Generated texture is null!" << endl;
		return;
	}

	glDisable(GL_DEPTH_TEST);
	GL_CHECK_FOR_ERRORS("", sourceFileName);

	Opengl::blend(Opengl::BLEND_TRANSCLUENT);
	//Opengl::blend(Opengl::BLEND_NONE);
	tex->bind();

	// Translate & rotate the model matrix
	glm::mat4 model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
	//model = glm::rotate(model, rotationAngle, glm::vec3(0.0f, 0.0f, 1.0f)); // rotationAngle must be in Radian

	// Calculate and set the ModelViewProjection matrix
	glm::mat4 mvp = camera.computeMvpMatrix(renderer->getSizef(), model);

	// Use shader
	shader.use();
	shader.setUniformMatrix4fv("MVP", 1, GL_FALSE, &mvp[0][0]);

	// Render VAO
	const Rect<size_t>& pixelTexCoords = tex->getPixelTexCoords();
	Shapes::generateRectangle(
				vao,
				Rect<float>(pos.x, pos.y,
							pixelTexCoords.getWidth(), pixelTexCoords.getHeight()),
				Vector3f(0.0f, 0.0f, 1.0f), &tex->getTexCoords());
	vao.render();
}


float Font2dSdl2Opengl::calculateMaxWidth(const UString& text) const
{
	if (font == nullptr) return 0;
	int w = 0, h = 0;
	TTF_SizeUTF8(font, text.c_str(), &w, &h);
	return w;
	//return getCharacterWidth() * static_cast<float>(text.size());
}


float Font2dSdl2Opengl::getGenericCharacterWidth() const
{
	if (font == nullptr) return 0;
	return TTF_FontHeight(font);
}


float Font2dSdl2Opengl::getCharacterHeight() const
{
	if (font == nullptr) return 0;
	return TTF_FontHeight(font);
}


float Font2dSdl2Opengl::getRowHeight() const
{
	if (font == nullptr) return 0;
	return TTF_FontLineSkip(font); // TODO: this is the default, but should it be adjusted?
}


} // End namespace Trieng

