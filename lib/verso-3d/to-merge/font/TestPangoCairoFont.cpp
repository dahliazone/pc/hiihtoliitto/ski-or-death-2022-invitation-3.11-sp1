
/*
Text mText;

mText.SetMargin(0);
mText.SetBorder(3);
mText.SetPadding(20);
mText.SetColor(RgbaColorf(0.9f, 0.9f, 0.9f, 1));
mText.SetBackgroundColor(RgbaColorf(0, 0, 0, 0.7f));
mText.SetBorderColor(RgbaColorf(0, 0, 1.0f, 0.5f));
mText.Init(350, 512);
mText.SetText("Init(): Hello World!");

*/

void TestSceneFont::Update(double deltaTime, double elapsedTime)
{
	std::cout << "TestSceneFont::Update()" << std::endl;
	(void)deltaTime; (void)elapsedTime;

	UString tStr = "";
	tStr += "<span foreground=\"red\">Colored</span> <b>bold</b> <span strikethrough=\"true\" ";
	tStr += "strikethrough_color=\"red\">strikethrough</span> ";
	tStr += "<i>italic</i> <span underline=\"single\">underline</span> <span underline=\"error\">error</span>, ";
	tStr += "<span size=\"large\">larger</span> <tt>monospace</tt> <span bgcolor=\"green\" fgcolor=\"black\">bgcolor</span>\n";
	tStr += "<sub>subscript</sub><sup>superscript</sup> ";
	tStr += "<span font_family=\"Times New Roman\" size=\"medium\">Times New Roman</span>\n";
	tStr += "弊社より販売された「みんなでスペランカ.\n\n";
	tStr += "Nam tincidunt velit ac mi aliquam sollicitudin. Sed sodales felis a turpis ultrices a eleifend mauris fringilla.\n\n";
	tStr += "Nunc aliquam, sapien luctus rutrum mollis, elit justo dignissim est, at eleifend nibh ante sed eros.\n\n";
	tStr += "Phasellus sit amet velit felis, sed faucibus eros. Cras euismod rutrum ipsum nec pharetra. Nulla facilisi.";

	//mText.SetText(tStr, true);
}


void TestSceneFont::Render(double deltaTime, double elapsedTime) const
{
	std::cout << "TestSceneFont::Render()" << std::endl;
	(void)deltaTime; (void)elapsedTime;

	//Gl::BlendTranscluent();
	//glEnable(GL_TEXTURE_2D);
	//glDisable(GL_DEPTH_TEST);
	//mText.Render(Vector2f(GetWindowConst()->GetWidth() - mText.GetWidth() - 20, GetWindowConst()->GetHeight() - mText.GetHeight() - 20));
}

