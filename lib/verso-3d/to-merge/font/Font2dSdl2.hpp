#ifndef VERSO_GFX_TODO_FONT2DSDL2_HPP
#define VERSO_GFX_TODO_FONT2DSDL2_HPP

#include "Font2d.hpp"
#include "Image2d.hpp"
#include <SDL2/SDL_ttf.h>

namespace Verso {


class Font2dSdl2 : public Font2d
{
public:
	Font2dSdl2(SDL_Renderer* renderer); // SDL2-specific constructor
	virtual ~Font2dSdl2() override;

	// Interface: Font2d /////////////////////////////////////////////////////////////////////////////////
	virtual bool loadFont(const UString& fileName, size_t size);
	virtual Image2d* generateText(const UString& text, const RgbaColorf& color) const;
	virtual Image2d* generateWrappedText(const UString& text, const RgbaColorf& color, int wrapLength) const;

private:
	SDL_Renderer* renderer;
	UString sourceFileName;
	size_t size;
	TTF_Font* font;
};


} // End namespace Verso

#endif // End header guard

