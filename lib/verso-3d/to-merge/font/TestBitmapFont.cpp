

	// Initialize all renderers
	mSystemFont.LoadTexture
		(mFontsPath + "systemfont.png", new Osime::TextureLoaders::PngLibsdl_image(), mResourceManager);
	mSystemFont.SetDrawMode(Osime::BitmapFont::MODE2D);
	mSystemFont.SetBlendMode(Osime::BlendMode(Osime::BlendMode::NONE));//TRANSCLUENT
	mSystemFont.SetHorizontalAlign(Osime::BitmapFont::LEFT);
	mSystemFont.SetVerticalAlign(Osime::BitmapFont::TOP);
	mSystemFont.SetCharacterWidth(10.0f * (mViewport2d->GetWidth() / resolution2dWidth));
	mSystemFont.SetCharacterHeight(15.0f * (mViewport2d->GetHeight() / resolution2dHeight));
	mSystemFont.SetColor(Dahlia::RgbColor(1.0f, 1.0f, 1.0f, 0.75f));


