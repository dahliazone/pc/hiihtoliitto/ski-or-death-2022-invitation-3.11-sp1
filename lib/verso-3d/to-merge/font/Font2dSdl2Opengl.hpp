#ifndef VERSO_GFX_TODO_FONT2DSDL2OPENGL_HPP
#define VERSO_GFX_TODO_FONT2DSDL2OPENGL_HPP

#include <Verso/Camera.hpp>
#include <Verso/ShaderProgram.hpp>
#include <Verso/Shapes.hpp>
#include <Verso/Opengl.hpp>
#include <Verso/Vao.hpp>
#include <Verso/Texture.hpp>
#include <Verso/Align.hpp>
#include <Verso/Vector3.hpp>
#include <Verso/RgbaColorf.hpp>
#include <Verso/AppWindowOpengl.hpp>

#if defined (__APPLE_CC__)
#include <SDL_ttf.h>
#else
#include <SDL2/SDL_ttf.h>
#endif

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <string>

namespace Verso {


class Font2dSdl2Opengl
{
public:
	Font2dSdl2Opengl(AppWindowOpengl* renderer);
	~Font2dSdl2Opengl();

	bool setShader(const UString& vertexShaderFileName, const UString& fragmentShaderFileName);
	bool loadFont(const UString& fileName, int pointSize);
	void freeTexture();
	Texture* generateTexture(const UString& text, const RgbaColorf& color);
	void renderString(ICamera& camera, const Vector3f& pos, const UString& text, const RgbaColorf& color, const Align& align=Align(Align::LEFT, Align::TOP));

	float calculateMaxWidth(const UString& text) const;
	float getGenericCharacterWidth() const;
	float getCharacterHeight() const;
	float getRowHeight() const;

private:
	AppWindowOpengl* renderer;
	UString sourceFileName;
	TTF_Font* font;
	Texture* tex;
	ShaderProgram shader;
	Vao vao;
};


} // End namespace Verso

#endif // End header guard

