
#include <Trieng/Font2dSdl2.hpp>
#include <Trieng/Image2dSdl2.hpp>
#include <Trieng/InitWrapperSdl2.hpp>

namespace Trieng {


Font2dSdl2::Font2dSdl2(SDL_Renderer* renderer) :
	renderer(renderer),
	sourceFileName(""),
	size(0),
	font(nullptr)
{
	InitWrapperSdl2::getSingletonPtr()->initTtf();
}


Font2dSdl2::~Font2dSdl2()
{
	if (font != nullptr) {
		sourceFileName = "";
		size = 0;
		TTF_CloseFont(font);
		font = nullptr;
	}
}


bool Font2dSdl2::loadFont(const UString& fileName, size_t size)
{
	if (font != nullptr) {
		sourceFileName = "";
		size = 0;
		TTF_CloseFont(font);
		font = nullptr;
	}

	font = TTF_OpenFont(fileName.c_str(), size);
	if (font == nullptr){
		UString error = "ERROR: Font2dSdl2::loadFont(const UString& fileName, size_t size): Could not load font from: "+fileName;
		error += " with size=";
		error += size;
		error += "\nSDL2_ttf reported: ";
		error += TTF_GetError();
		cerr << error << endl;
		throw runtime_error(error.c_str());
		return false;
	}
	this->sourceFileName = fileName;
	this->size = size;
	return true;
}


Image2d* Font2dSdl2::generateText(const UString& text, const RgbaColorf& color) const
{
	SDL_Color sdlColor;
	sdlColor.r = color.r * 255.0f;
	sdlColor.g = color.g * 255.0f;
	sdlColor.b = color.b * 255.0f;
	sdlColor.a = color.a * 255.0f;

	SDL_Surface* surface = TTF_RenderText_Blended(font, text.c_str(), sdlColor);
	if (surface == nullptr){
		UString error = "ERROR: Font2dSdl2::generateText(const UString& text, const RgbaColorf& color) const: Could not generate text from loaded font: "+sourceFileName;
		error += " with size=";
		error += size;
		error += "\nSDL2_ttf reported: ";
		error += TTF_GetError();
		cerr << error << endl;
		throw runtime_error(error.c_str());
		return nullptr;
	}

	Image2dSdl2* image = new Image2dSdl2(renderer);
	image->createFromSurface(surface);

	return image;
}


Image2d* Font2dSdl2::generateWrappedText(const UString& text, const RgbaColorf& color, int wrapLength) const
{
	SDL_Color sdlColor;
	sdlColor.r = color.r * 255.0f;
	sdlColor.g = color.g * 255.0f;
	sdlColor.b = color.b * 255.0f;
	sdlColor.a = color.a * 255.0f;

	SDL_Surface* surface = TTF_RenderText_Blended_Wrapped(font, text.c_str(), sdlColor, wrapLength);
	if (surface == nullptr){
		UString error = "ERROR: Font2dSdl2::generateText(const UString& text, const RgbaColorf& color) const: Could not generate text from loaded font: "+sourceFileName;
		error += " with size=";
		error += size;
		error += "\nSDL2_ttf reported: ";
		error += TTF_GetError();
		cerr << error << endl;
		throw runtime_error(error.c_str());
		return nullptr;
	}

	Image2dSdl2* image = new Image2dSdl2(renderer);
	image->createFromSurface(surface);

	return image;
}


} // End namespace Trieng

