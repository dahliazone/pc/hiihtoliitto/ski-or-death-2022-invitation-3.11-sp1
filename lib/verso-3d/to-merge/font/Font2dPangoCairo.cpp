
/*
#include "Text.hpp"
#include "Shapes.hpp"
#include "RgbaColorf.hpp"
#include "Gl.hpp"

#include <iostream>
#include <stdexcept>


const float Text::MIN_CONTENT_WIDTH = 5;
const float Text::MIN_CONTENT_HEIGHT = 5;


Text::Text() :
	mRichText(false),
	mText(""),
	mCairoContext(0),
	mCairoSurface(0),
	mSurfaceData(0),
	mPangoLayout(0),
	mPangoFontDescriptionStr("Sans Bold 12"),
	mPangoFontDescription(0),
	mTexture()
{
}


Text::~Text()
{
	if (mCairoContext != 0) {
		cairo_destroy(mCairoContext);
		mCairoContext = 0;
		cairo_surface_destroy(mCairoSurface);
		mCairoSurface = 0;
		delete[] mSurfaceData;
		mSurfaceData = 0;
	}

	if (mPangoFontDescription != 0)
		pango_font_description_free(mPangoFontDescription);

	if (mPangoLayout != 0)
		g_object_unref(mPangoLayout);
}


bool Text::Init(int width, int height)
{
	SetSize(width, height);

	Vector2f contentSize = GetContentSize();
	int width2 = static_cast<int>(contentSize.x);
	int height2 = static_cast<int>(contentSize.y);
	if (width2 < MIN_CONTENT_WIDTH) {
		width2 = MIN_CONTENT_WIDTH;
		SetSize(width2 + GetBorderLeft() + GetBorderRight() + GetPaddingLeft() + GetPaddingRight(), height2);
	}
	if (height2 < MIN_CONTENT_HEIGHT) {
		height2 = MIN_CONTENT_HEIGHT;
		SetSize(width2, height2 + GetBorderTop() + GetBorderBottom() + GetPaddingTop() + GetPaddingBottom());
	}

	// Create cairo-surface/context to act as OpenGL-texture source
	mCairoContext = CreateCairoContext(width2, height2,
									   &mCairoSurface, &mSurfaceData);

	GL_CHECK_FOR_ERRORS("", mPangoFontDescriptionStr.toStdString());
	mTexture.Generate(width2, height2, true, 0, false, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);
	GL_CHECK_FOR_ERRORS("", mPangoFontDescriptionStr.toStdString());

	// Init pango layout
	mPangoLayout = pango_cairo_create_layout(mCairoContext);

	pango_layout_set_spacing(mPangoLayout, -2 * PANGO_SCALE);
	pango_layout_set_width(mPangoLayout, GetContentSize().x * PANGO_SCALE);
	pango_layout_set_height(mPangoLayout, GetContentSize().y * PANGO_SCALE);
	pango_layout_set_indent(mPangoLayout, 0 * PANGO_SCALE);
	pango_layout_set_wrap(mPangoLayout, PANGO_WRAP_WORD);
	// PANGO_WRAP_WORD, PANGO_WRAP_CHAR, PANGO_WRAP_WORD_CHAR
	//pango_layout_set_justify(mPangoLayout, true);
	pango_layout_set_ellipsize(mPangoLayout, PANGO_ELLIPSIZE_NONE);
	//PANGO_ELLIPSIZE_NONE, PANGO_ELLIPSIZE_START, PANGO_ELLIPSIZE_MIDDLE, PANGO_ELLIPSIZE_END
	//pango_layout_set_attributes(); // ?

	// Set the text to be associated with the layout
	QByteArray ba = mText.toLocal8Bit();
	const char* textCstr = ba.data();
	pango_layout_set_text(mPangoLayout, textCstr, -1);

	// Set font
	if (mPangoFontDescription != 0)
		pango_font_description_free(mPangoFontDescription);

	//mPangoFontDescription = pango_font_description_from_string("serif");
	mPangoFontDescription = pango_font_description_new();
	//SetFontDescription(mFontDescription);
	pango_font_description_set_family(mPangoFontDescription, "sans serif");
	pango_font_description_set_weight(mPangoFontDescription, PANGO_WEIGHT_NORMAL);
	// PANGO_WEIGHT_THIN, PANGO_WEIGHT_ULTRALIGHT, PANGO_WEIGHT_LIGHT, PANGO_WEIGHT_BOOK,
	// PANGO_WEIGHT_NORMAL, PANGO_WEIGHT_MEDIUM, PANGO_WEIGHT_SEMIBOLD, PANGO_WEIGHT_BOLD,
	// PANGO_WEIGHT_ULTRABOLD, PANGO_WEIGHT_HEAVY, PANGO_WEIGHT_ULTRAHEAVY
	pango_font_description_set_absolute_size(mPangoFontDescription, 24 * PANGO_SCALE);
	pango_font_description_set_style(mPangoFontDescription, PANGO_STYLE_NORMAL); // PANGO_STYLE_NORMAL, PANGO_STYLE_OBLIQUE, PANGO_STYLE_ITALIC
	pango_font_description_set_variant(mPangoFontDescription, PANGO_VARIANT_NORMAL); // PANGO_VARIANT_NORMAL, PANGO_VARIANT_SMALL_CAPS
	pango_font_description_set_stretch(mPangoFontDescription, PANGO_STRETCH_ULTRA_CONDENSED);
	//PANGO_STRETCH_ULTRA_CONDENSED, PANGO_STRETCH_EXTRA_CONDENSED, PANGO_STRETCH_CONDENSED, PANGO_STRETCH_SEMI_CONDENSED,
	//PANGO_STRETCH_NORMAL, PANGO_STRETCH_SEMI_EXPANDED, PANGO_STRETCH_EXPANDED, PANGO_STRETCH_EXTRA_EXPANDED, PANGO_STRETCH_ULTRA_EXPANDED

	pango_layout_set_font_description(mPangoLayout, mPangoFontDescription);

	// Bypass changed text check
	mText = " ";
	SetText("");

	return true;
}


void Text::SetText(const UString& text, bool isRichText)
{
	if (mCairoContext == 0) {
		std::string error = "Text::SetText(): You must call Init() first!";
		std::cerr << error << std::endl; throw std::runtime_error(error);
	}

	// Do not update texture unless text is different
	if (text.compare(mText) == 0)
		return;

	mText = text;

	cairo_save(mCairoContext);

	// Clear surface
	cairo_set_operator(mCairoContext, CAIRO_OPERATOR_CLEAR);
	cairo_paint(mCairoContext);
	cairo_set_operator(mCairoContext, CAIRO_OPERATOR_OVER);

	// Set text color
	RgbaColorf color = GetColor();
	cairo_set_source_rgba(mCairoContext, color.r, color.g, color.b, color.a);

	// Set the text to be associated with the layout
	mRichText = isRichText;
	QByteArray ba = mText.toLocal8Bit();
	const char* textCstr = ba.data();
	if (isRichText == false)
		pango_layout_set_text(mPangoLayout, textCstr, -1);
	else
		pango_layout_set_markup(mPangoLayout, textCstr, -1);

	// Update pango layout
	pango_cairo_update_layout(mCairoContext, mPangoLayout);

	// Draw the pango layout onto the cairo surface
	pango_cairo_show_layout(mCairoContext, mPangoLayout);
	//pango_cairo_show_layout_line(mCairoContext, pango_layout_get_line(mPangoLayout, 0));

	cairo_restore(mCairoContext);

	mTexture.Update(mSurfaceData, 0, 0, mTexture.GetWidth(), mTexture.GetHeight(), GL_BGRA);
}


/7void Text::SetFontDescription(const UString& fontDescription)
//{
// mFontDescription = fontDescription;

// if (mPangoFontDescription != 0)
//  pango_font_description_free(mPangoFontDescription);

// mPangoFontDescription = pango_font_description_from_string(mFontDescription.c_str());
// pango_layout_set_font_description(mPangoLayout, mPangoFontDescription);
//}


void Text::Render(const Vector2f& location)
{
	if (mCairoContext == 0) {
		std::string error = "Text::Render(): You must call Init() first!";
		std::cerr << error << std::endl; throw std::runtime_error(error);
	}

	// Render a box underneath
	BoxModel::Render(location);

	// Render the text
	RgbaColorf(1, 1, 1).glColor();
	glDisable(GL_DEPTH_TEST);
	GL_CHECK_FOR_ERRORS("", "");
	////mTexture.Bind();

	// TODO: put shader to use and location as uniform variable to shader
	//glPushMatrix();
	//(location + GetContentLocation()).glTranslate();
	// TODO: fix that vao generation is done on update and rendering on render
	//Shapes::Rectangle(mVao, GetContentSize());
	//glPopMatrix();
}


void Text::DumpToPng(const UString& filename)
{
	if (mCairoContext == 0) {
		std::string error = "Text::DumpToPng(): You must call Init() first!";
		std::cerr << error << std::endl; throw std::runtime_error(error);
	}

	QByteArray ba = filename.toLocal8Bit();
	const char* filenameCstr = ba.data();
	if (cairo_surface_write_to_png(mCairoSurface, filenameCstr) != CAIRO_STATUS_SUCCESS) {
		std::string error = "void Font::DumpToPng(): Could not dump content to PNG-file ("+filename.toStdString()+")";
		std::cerr << error << std::endl; throw std::runtime_error(error);
	}
}





cairo_t* Text::CreateCairoContext(int width, int height, cairo_surface_t** surface, unsigned char** buffer, cairo_format_t format)
{
	// Allocate buffer for pixel-data
	int stride = cairo_format_stride_for_width(format, width);
	*buffer = new unsigned char[stride * height];
	if (!*buffer) {
		std::string error = "cairo_t* Font::CreateCairoContext(): Couldn't allocate buffer!";
		std::cerr << error << std::endl; throw std::runtime_error(error);
		return NULL;
	}

	// Create cairo surface from pixel data
	*surface = cairo_image_surface_create_for_data(
				*buffer, format, width, height, stride);
	if (cairo_surface_status(*surface) != CAIRO_STATUS_SUCCESS) {
		delete[] *buffer;
		std::string error = "cairo_t* Font::CreateCairoContext(): Couldn't create surface!";
		std::cerr << error << std::endl; throw std::runtime_error(error);
		return NULL;
	}

	// Create cairo context from cairo surface
	cairo_t* cr = cairo_create(*surface);
	if (cairo_status(cr) != CAIRO_STATUS_SUCCESS) {
		cairo_surface_destroy(*surface);
		delete[] *buffer;
		std::string error = "cairo_t* Font::CreateCairoContext(): Couldn't create context!";
		std::cerr << error << std::endl; throw std::runtime_error(error);
		return NULL;
	}

	// Paint with transparency
	cairo_save(cr);
	cairo_set_operator(cr, CAIRO_OPERATOR_CLEAR);
	cairo_paint(cr);
	cairo_restore(cr);

	return cr;
}

*/


