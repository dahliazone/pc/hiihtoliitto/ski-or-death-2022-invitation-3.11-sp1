#ifndef VERSO_GFX_TODO_BOXMODEL_HPP
#define VERSO_GFX_TODO_BOXMODEL_HPP

#include "Vector2.hpp"
#include "RgbaColorf.hpp"
#include "Shapes.hpp"

namespace Verso {


class BoxModel
{
public:
	inline BoxModel(const Vector2f& size = Vector2f(0.0f, 0.0f));
	inline virtual ~BoxModel();

	inline virtual void update();
	inline virtual void render(const Vector2f& location);

	inline Vector2f getSize() const;
	inline Vector2f getSizeWithMargin() const;
	inline void setSize(float width, float height);
	inline virtual void setSize(const Vector2f& size);
	inline float getWidth() const;
	inline void setWidth(float value);
	inline float getHeight() const;
	inline void setHeight(float value);

	inline Vector2f getBorderLocation() const;
	inline Vector2f getPaddingLocation() const;
	inline Vector2f getContentLocation() const;
	inline Vector2f getContentSize() const;
	inline Vector2f getContentSizeWithPadding() const;

	inline void setMargin(float value);
	inline void setHorizontalMargin(float value);
	inline void setVerticalMargin(float value);
	inline float getMarginTop() const;
	inline void setMarginTop(float value);
	inline float getMarginBottom() const;
	inline void setMarginBottom(float value);
	inline float getMarginLeft() const;
	inline void setMarginLeft(float value);
	inline float getMarginRight() const;
	inline void setMarginRight(float value);

	inline void setBorder(float value);
	inline void setHorizontalBorder(float value);
	inline void setVerticalBorder(float value);
	inline float getBorderTop() const;
	inline void setBorderTop(float value);
	inline float getBorderBottom() const;
	inline void setBorderBottom(float value);
	inline float getBorderLeft() const;
	inline void setBorderLeft(float value);
	inline float getBorderRight() const;
	inline void setBorderRight(float value);

	inline void setPadding(float value);
	inline void setHorizontalPadding(float value);
	inline void setVerticalPadding(float value);
	inline float getPaddingTop() const;
	inline void setPaddingTop(float value);
	inline float getPaddingBottom() const;
	inline void setPaddingBottom(float value);
	inline float getPaddingLeft() const;
	inline void setPaddingLeft(float value);
	inline float getPaddingRight() const;
	inline void setPaddingRight(float value);

	inline RgbaColorf getColor() const;
	inline void setColor(const RgbaColorf& color);
	inline RgbaColorf getBorderColor() const;
	inline void setBorderColor(const RgbaColorf& color);
	inline RgbaColorf getBackgroundColor() const;
	inline void setBackgroundColor(const RgbaColorf& color);

	inline bool getRenderDebugBorders() const;
	inline void setRenderDebugBorders(bool enabled);

private:
	Vector2f size;

	float marginTop;
	float marginBottom;
	float marginLeft;
	float marginRight;

	float borderTop;
	float borderBottom;
	float borderLeft;
	float borderRight;

	float paddingTop;
	float paddingBottom;
	float paddingLeft;
	float paddingRight;

	RgbaColorf color;
	RgbaColorf borderColor;
	RgbaColorf backgroundColor;

	bool renderDebugBorders;

	Vao backgroundVao;
	Vao borderVao;
	Vao marginOutlineVao;
	Vao borderOutlineVao;
	Vao paddingOutlineVao;
	Vao contentOutlineVao;
};



inline BoxModel::BoxModel(const Vector2f& size) :
    size(size),
    marginTop(0),
    marginBottom(0),
    marginLeft(0),
    marginRight(0),
    borderTop(0),
    borderBottom(0),
    borderLeft(0),
    borderRight(0),
    paddingTop(0),
    paddingBottom(0),
    paddingLeft(0),
    paddingRight(0),
    color(1, 1, 1, 1),
    borderColor(0, 0, 0, 0),
    backgroundColor(0, 0, 0, 0),
    renderDebugBorders(false),
    backgroundVao(),
    borderVao(),
    marginOutlineVao(),
    borderOutlineVao(),
    paddingOutlineVao(),
    contentOutlineVao()
{
}


inline BoxModel::~BoxModel()
{
}


inline void BoxModel::update()
{
	// \TODO: optimization on what to generate, e.g. check for changes, color alphas and renderDebugBorders
	Shapes::generateRectangle(backgroundVao, getContentSizeWithPadding());
	Shapes::generateRectangleWithBorders(borderVao, getSize(), getBorderTop(), getBorderBottom(), getBorderLeft(), getBorderRight());

	Shapes::generateRectangle(marginOutlineVao, getSizeWithMargin());
	Shapes::generateRectangle(borderOutlineVao, getSize());
	Shapes::generateRectangle(paddingOutlineVao, getContentSizeWithPadding());
	Shapes::generateRectangle(contentOutlineVao, getContentSize());
}


inline void BoxModel::render(const Vector2f& location)
{
	glDisable(GL_DEPTH_TEST);
	GL_CHECK_FOR_ERRORS("", "");

	RgbaColorf bgColor = getBackgroundColor();
	if (bgColor.a != 0) {
		bgColor.glColor();
		// \TODO: put shader to use and location as uniform variable to shader
		//glPushMatrix();
		//(location + getPaddingLocation()).glTranslate();
		backgroundVao.render();
		//glPopMatrix();
	}

	RgbaColorf borderColor = getBorderColor();
	if (borderColor.a != 0) {
		borderColor.glColor();
		// \TODO: put shader to use and location as uniform variable to shader
		//glPushMatrix();
		//(location + getBorderLocation()).glTranslate();
		borderVao.render();
		//glPopMatrix();
	}

	// Draw only wireframe
	if (renderDebugBorders == true) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		GL_CHECK_FOR_ERRORS("", "");

		// Margin outline
		RgbaColorf(0, 0, 1).glColor();
		// \TODO: put shader to use and location as uniform variable to shader
		//glPushMatrix();
		//location.glTranslate();
		marginOutlineVao.render();
		//glPopMatrix();

		// Border outine
		RgbaColorf(1, 0, 0).glColor();
		// \TODO: put shader to use and location as uniform variable to shader
		//glPushMatrix();
		//(location + getBorderLocation()).glTranslate();
		borderOutlineVao.render();
		//glPopMatrix();

		// Padding outline
		RgbaColorf(0, 1, 0).glColor();
		// \TODO: put shader to use and location as uniform variable to shader
		//glPushMatrix();
		//(location + getPaddingLocation()).glTranslate();
		paddingOutlineVao.render();
		//glPopMatrix();

		// Content outline
		RgbaColorf(1, 1, 1).glColor();
		// \TODO: put shader to use and location as uniform variable to shader
		//glPushMatrix();
		//(location + getContentLocation()).glTranslate();
		contentOutlineVao.render();
		//glPopMatrix();

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		GL_CHECK_FOR_ERRORS("", "");
	}
}


inline Vector2f BoxModel::getSize() const
{
	return size;
}


inline Vector2f BoxModel::getSizeWithMargin() const
{
	return size + Vector2f(marginLeft + marginRight, marginTop + marginBottom);
}


inline void BoxModel::setSize(float width, float height)
{
	setSize(Vector2f(width, height));
}


inline void BoxModel::setSize(const Vector2f& size)
{
	this->size = size;
}


inline float BoxModel::getWidth() const
{
	return size.x;
}


inline void BoxModel::setWidth(float value)
{
	setSize(Vector2f(value, size.x));
}


inline float BoxModel::getHeight() const
{
	return size.y;
}


inline void BoxModel::setHeight(float value)
{
	setSize(Vector2f(size.x, value));
}


inline Vector2f BoxModel::getBorderLocation() const
{
	return Vector2f(marginLeft, marginTop);
}


inline Vector2f BoxModel::getPaddingLocation() const
{
	return Vector2f(marginLeft + borderLeft, marginTop + borderTop);
}


inline Vector2f BoxModel::getContentLocation() const
{
	return Vector2f(marginLeft + borderLeft + paddingLeft, marginTop + borderTop + paddingTop);
}


inline Vector2f BoxModel::getContentSize() const
{
	return Vector2f(
	            size.x - borderLeft - borderRight - paddingLeft - paddingRight,
	            size.y - borderTop - borderBottom - paddingTop - paddingBottom);
}


inline Vector2f BoxModel::getContentSizeWithPadding() const
{
	return Vector2f(
	            size.x - getBorderLeft() - getBorderRight(),
	            size.y - getBorderTop() - getBorderBottom());
}


inline void BoxModel::setMargin(float value)
{
	this->marginTop = value;
	this->marginBottom = value;
	this->marginLeft = value;
	this->marginRight = value;
}


inline void BoxModel::setHorizontalMargin(float value)
{
	this->marginLeft = value;
	this->marginRight = value;
}


inline void BoxModel::setVerticalMargin(float value)
{
	this->marginTop = value;
	this->marginBottom = value;
}


inline float BoxModel::getMarginTop() const
{
	return marginTop;
}


inline void BoxModel::setMarginTop(float value)
{
	this->marginTop = value;
}


inline float BoxModel::getMarginBottom() const
{
	return marginBottom;
}


inline void BoxModel::setMarginBottom(float value)
{
	this->marginBottom = value;
}


inline float BoxModel::getMarginLeft() const
{
	return marginLeft;
}


inline void BoxModel::setMarginLeft(float value)
{
	this->marginLeft = value;
}


inline float BoxModel::getMarginRight() const
{
	return marginRight;
}


inline void BoxModel::setMarginRight(float value)
{
	this->marginRight = value;
}


inline void BoxModel::setBorder(float value)
{
	this->borderTop = value;
	this->borderBottom = value;
	this->borderLeft = value;
	this->borderRight = value;
}

inline void BoxModel::setHorizontalBorder(float value)
{
	this->borderLeft = value;
	this->borderRight = value;
}


inline void BoxModel::setVerticalBorder(float value)
{
	this->borderTop = value;
	this->borderBottom = value;
}


inline float BoxModel::getBorderTop() const
{
	return borderTop;
}


inline void BoxModel::setBorderTop(float value)
{
	this->borderTop = value;
}


inline float BoxModel::getBorderBottom() const
{
	return borderBottom;
}


inline void BoxModel::setBorderBottom(float value)
{
	this->borderBottom = value;
}


inline float BoxModel::getBorderLeft() const
{
	return borderLeft;
}


inline void BoxModel::setBorderLeft(float value)
{
	this->borderLeft = value;
}


inline float BoxModel::getBorderRight() const
{
	return borderRight;
}


inline void BoxModel::setBorderRight(float value)
{
	this->borderRight = value;
}


inline void BoxModel::setPadding(float value)
{
	this->paddingTop = value;
	this->paddingBottom = value;
	this->paddingLeft = value;
	this->paddingRight = value;
}


inline void BoxModel::setHorizontalPadding(float value)
{
	this->paddingLeft = value;
	this->paddingRight = value;
}


inline void BoxModel::setVerticalPadding(float value)
{
	this->paddingTop = value;
	this->paddingBottom = value;
}


inline float BoxModel::getPaddingTop() const
{
	return paddingTop;
}


inline void BoxModel::setPaddingTop(float value)
{
	this->paddingTop = value;
}


inline float BoxModel::getPaddingBottom() const
{
	return paddingBottom;
}


inline void BoxModel::setPaddingBottom(float value)
{
	this->paddingBottom = value;
}


inline float BoxModel::getPaddingLeft() const
{
	return paddingLeft;
}


inline void BoxModel::setPaddingLeft(float value)
{
	this->paddingLeft = value;
}


inline float BoxModel::getPaddingRight() const
{
	return paddingRight;
}


inline void BoxModel::setPaddingRight(float value)
{
	this->paddingRight = value;
}


inline RgbaColorf BoxModel::getColor() const
{
	return color;
}


inline void BoxModel::setColor(const RgbaColorf& color)
{
	this->color = color;
}


inline RgbaColorf BoxModel::getBorderColor() const
{
	return borderColor;
}


inline void BoxModel::setBorderColor(const RgbaColorf& color)
{
	this->borderColor = color;
}


inline RgbaColorf BoxModel::getBackgroundColor() const
{
	return backgroundColor;
}


inline void BoxModel::setBackgroundColor(const RgbaColorf& color)
{
	this->backgroundColor = color;
}


inline bool BoxModel::getRenderDebugBorders() const
{
	return renderDebugBorders;
}


inline void BoxModel::setRenderDebugBorders(bool enabled)
{
	this->renderDebugBorders = enabled;
}


} // End namespace Verso

#endif // End header guard

