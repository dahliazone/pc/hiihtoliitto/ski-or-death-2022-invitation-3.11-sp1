
#include <Verso/DepthIo/DioFile.hpp>
#include <Verso/System/File.hpp>
#include <gtest/gtest.h>

namespace {

using namespace Verso;


/**
 * Test functionaly before StartToSave() is called.
 */
TEST(DioFile, BeforeStartToSaveAndBasicSave)
{
	DioFile df;
	FrameData frame(0, (DioDataTypes)DioDataType::Array_Xyz); // uint32_t index, DioDataTypes requiredDioDataTypes, uint32_t timestamp = 0
	EXPECT_STRCASEEQ("", df.getErrorMsg().c_str());
	df.clearErrorMsg();

	EXPECT_FALSE(df.saveFrame(frame));
	EXPECT_STRCASENE("", df.getErrorMsg().c_str());
	df.clearErrorMsg();

	EXPECT_FALSE(df.finishSave());
	EXPECT_STRCASENE("", df.getErrorMsg().c_str());
	df.clearErrorMsg();

	UString fileName = "tmp_unittest_DioFile.dio";
	EXPECT_TRUE(df.startToSave(fileName, DioPackFormat::Unpacked, 3, 2));
	EXPECT_STRCASEEQ("", df.getErrorMsg().c_str());
	df.clearErrorMsg();

	EXPECT_FALSE(df.saveFrame(frame));
	EXPECT_STRCASENE("", df.getErrorMsg().c_str());
	df.clearErrorMsg();

	frame.addDataXyz(1, 2, 3);
	EXPECT_TRUE(df.saveFrame(frame));
	EXPECT_STRCASEEQ("", df.getErrorMsg().c_str());
	df.clearErrorMsg();

	EXPECT_TRUE(df.finishSave());
	EXPECT_STRCASEEQ("", df.getErrorMsg().c_str());
	df.clearErrorMsg();

	uint64_t filesize = File::getSize(fileName);
	EXPECT_NE(0, filesize);

	//File::delete(fileName);
}

/*
TEST(DioFile, SaveLoadSaveEquals)
{
	UString fileName1 = "tmp_unittest_DioFile_1.dio";
	UString fileName2 = "tmp_unittest_DioFile_2.dio";

	FrameAnimation anim1(1, 2);

	FrameData frame0(0, (DioDataTypes)DioDataType::Array_Xyz);
	frame0.addDataXyz(1, 2, 3);
	frame0.addDataXyz(4, 5, 6);

	FrameData frame1(1, (DioDataTypes)DioDataType::Array_Xyz);
	frame1.addDataXyz(7, 8, 9);
	frame1.addDataXyz(10, 11, 12);

	anim1.addFrame(frame0);
	anim1.addFrame(frame1);

	EXPECT_TRUE(saveDioFile(anim1, fileName1, DioPackFormat::UNPACKED));
	FrameAnimation anim2(1, 2);
	EXPECT_TRUE(loadDioFile(anim2, fileName1));
	EXPECT_TRUE(saveDioFile(anim2, fileName2, DioPackFormat::UNPACKED));

	EXPECT_TRUE(anim1.equals(anim2));
	EXPECT_TRUE(File::equals(fileName1, fileName2));

	//UString fileName1 = "tmp_unittest_DioFile_1.dio";
	//uint64_t filesize1 = 0;
	//char* file1Contents = File::loadBytes(fileName1, filesize1);
	//VERSO_LOG_INFO_VARIABLE("depthio/test", "file size", filesize1;
	//VERSO_LOG_INFO_VARIABLE("depthio/test", "contains", file1Contents);
	//delete[] file1Contents;

	//EXPECT_TRUE(File::equals("test-osx.sh", "test-osx.sh"));

	//DioFile df2;
	//EXPECT_TRUE(df2.load(fileName2));
	//EXPECT_STRCASEEQ("", df2.getErrorMsg().c_str());
	//df2.clearErrorMsg();

	//EXPECT_TRUE(df1.equals(df2));

	//bool loadDioFile(FrameAnimation& anim, const UString& fileName);

}*/


/*
class DioFile
{
public:
	DioFile();
	virtual ~DioFile();

	bool startToSave(const UString& fileName, DioPackFormat pack, uint32_t width, uint32_t height);
	uint32_t getLastTimestamp() const;
	bool isNewFrame(uint32_t timestamp) const;
	// For real recording usage it's usually recommended to check the frame with IsNewFrame(timestamp) before saving it.
	bool saveFrame(FrameData& frame);
	bool finishSave();

private:
	static const uint8_t mVersion;
	UString mFileName;
	DioPackFormat mPackFormat;
	DioHeaderFlagBits mFlags;
	std::ofstream mFile;
	uint32_t mWidth;
	uint32_t mHeight;
	std::vector<DioFooter_ChunkMetadata> mChunkMetadata;
	uint32_t mLengthInFrames;
	uint32_t mLengthInMillisec;
	uint32_t mLastTimestamp;
};
*/


} // End namespace nameless namespace

