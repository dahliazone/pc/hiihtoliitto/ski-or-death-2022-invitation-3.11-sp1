! include( ../verso-depthio-common.pri ) {
	error("Couldn't find the verso-depthio-common.pri file!")
}

TEMPLATE = app
CONFIG -= qt core gui
CONFIG += console

# Includes
INCLUDEPATH += "$${PWD}"
INCLUDEPATH += "$${LIBPATH}/googletest/include/"


# Will build the final executable in the main project directory.
DESTDIR = "$${BUILD_DIR}"
TARGET = libverso-depthio_test


# 64 bit
linux-g++ {
	# BASS
	LIBS += -L$${LIBPATH}/BASS/linux/x64
}

linux-* {
	QMAKE_CXXFLAGS += -std=c++17

	QMAKE_POST_LINK += echo "Copying libraries..."

	# Verso-depthio
	LIBS += -L"$${BUILD_DIR}" -lverso-depthio

	# Verso-3d
	LIBS += -L"$${LIBPATH}/verso-3d/builds/$${BUILD_SUBDIR}" -lverso-3d
	QMAKE_POST_LINK += && cp -rf "$${LIBPATH}/verso-3d/builds/$${BUILD_SUBDIR}/*.so" "$$DESTDIR/"

	# Verso-gfx
	LIBS += -L"$${LIBPATH}/verso-gfx/builds/$${BUILD_SUBDIR}" -lverso-gfx
	QMAKE_POST_LINK += && cp -rf "$${LIBPATH}/verso-gfx/builds/$${BUILD_SUBDIR}/*.so" "$$DESTDIR/"

	# Verso-base
	LIBS += -L"$${LIBPATH}/verso-base/builds/$${BUILD_SUBDIR}" -lverso-base
	QMAKE_POST_LINK += && cp -rf "$${LIBPATH}/verso-base/builds/$${BUILD_SUBDIR}/*.so" "$$DESTDIR/"

	# BASS
	INCLUDEPATH += "$${LIBPATH}/BASS/linux"
	QMAKE_CXXFLAGS += -D__LINUX_ALSA__
	QMAKE_POST_LINK += && cp -rf "$${LIBPATH}/BASS/linux/x64/*.so" "$$DESTDIR/"
	LIBS += -lbass

	# googletest
	LIBS += -L"$${LIBPATH}/googletest/build" -lgtest -lpthread
	QMAKE_CXXFLAGS += "-isystem $$LIBPATH/googletest/include/"
	#LIBGTEST = libgtest.so*
	#QMAKE_POST_LINK += && cp -f "$$LIBPATH/googletest/build/$$LIBGTEST" "$$DESTDIR/"

	# Other libs
	LIBS += -lGL -lGLU -lGLEW -lSDL2 -lSDL2_image -lSDL2_ttf

	# cmath
	LIBS += -lm

	# Unit test scripts
	QMAKE_POST_LINK += && cp -rf "$${PWD}/libverso-depthio-test.sh" "$$DESTDIR/"

	# Data
	QMAKE_POST_LINK += && cp -rf "$${PWD}/../data" "$$DESTDIR/"
}

macx {
	#CONFIG -= x86_64
	#CONFIG += x86

	CONFIG -= app_bundle

	QMAKE_CXXFLAGS += -std=c++17

	# Verso-base
	LIBS += -L"$${LIBPATH}/verso-base/builds/$${BUILD_SUBDIR}" -lverso-base

	# Verso-depthio
	LIBS += -L"$${BUILD_DIR}" -lverso-depthio

	# googletest
	LIBS += -L"$${LIBPATH}/googletest/build" -lgtest -lpthread
	QMAKE_CXXFLAGS += "-isystem $$LIBPATH/googletest/include/"
	#LIBGTEST = libgtest.so*
	#QMAKE_POST_LINK += && cp -f "$$LIBPATH/googletest/build/$$LIBGTEST" "$$DESTDIR/"

	# BASS
	QMAKE_POST_LINK += cp -rf "$${LIBPATH}/BASS/osx/*.dylib" "$$DESTDIR/"

	# Unit test scripts
	QMAKE_POST_LINK += && cp -rf "$${PWD}/libverso-depthio-test-osx.sh" "$$DESTDIR/"

	# Data
	QMAKE_POST_LINK += && cp -rf "$${PWD}/../data" "$$DESTDIR/"
}

# Example of running tests as a build step
#linux-* {
#	QMAKE_POST_LINK  = cd .. && ./test-x64.sh
#}
#macx {
#	QMAKE_POST_LINK  = cd .. && ./test-osx.sh
#}
#QMAKE_CLEAN     += batch_output.obj


win* {
	# Verso-base
	LIBS += -L"$${LIBPATH}/verso-base/builds/$${BUILD_SUBDIR}" -lverso-base

	# Verso-depthio
	LIBS += -L"$${BUILD_DIR}" -lverso-depthio

	# SDL2
	LIBS += -lSDL2

	# googletest & main program
	LIBS += -lgtest -lgtest_main
}


DEPENDPATH += \
	$${PWD}


SOURCES += \
	DepthIo__DepthioBinary__test.cpp \
	DepthIo__DioDataType__test.cpp \
	DepthIo__DioFile__test.cpp \
	DepthIo__DioHeaderFlagBit__test.cpp \
	DepthIo__DioPackFormat__test.cpp \
	DepthIo__DioVariableType__test.cpp \
	DepthIo__FrameData__test.cpp \
	DepthIo__TiltStateRawKinect__test.cpp \
	test__main__DepthIo.cpp


OTHER_FILES += \
	libverso-depthio-test-osx.sh \
	libverso-depthio-test.sh

