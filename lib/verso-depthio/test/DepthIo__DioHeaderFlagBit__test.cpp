// Tested 100% 2016-10-03 -codise

#include <Verso/DepthIo/DioHeaderFlagBit.hpp>
#include <gtest/gtest.h>

namespace {

using namespace Verso;


/**
 * \brief Test constructor.
 */
TEST(DioHeaderFlagBit, constructor)
{
	DioHeaderFlagBit type1 = DioHeaderFlagBit::HasViewport;

	EXPECT_EQ(DioHeaderFlagBit::HasViewport, type1);
	EXPECT_EQ(type1, DioHeaderFlagBit::HasViewport);
}


/**
 * \brief Test UString dioHeaderFlagBitToString(DioHeaderFlagBit dioHeaderFlagBit).
 */
TEST(DioHeaderFlagBit, dioHeaderFlagBitToString)
{
	//
	DioHeaderFlagBit valid1 = (DioHeaderFlagBit)0;
	EXPECT_STRCASEEQ(dioHeaderFlagBitToString(valid1).c_str(), "None");

	DioHeaderFlagBit valid2 = DioHeaderFlagBit::HasViewport;
	EXPECT_STRCASEEQ(dioHeaderFlagBitToString(valid2).c_str(), "HasViewport");

	DioHeaderFlagBit invalid = (DioHeaderFlagBit)128;
	EXPECT_STRCASEEQ(dioHeaderFlagBitToString(invalid).c_str(), "UNKNOWN");
}


/**
 * \brief Test std::vector<DioHeaderFlagBit> getAllDioHeaderFlagBits().
 */
TEST(DioHeaderFlagBit, getAllDioHeaderFlagBits)
{
	std::vector<DioHeaderFlagBit> types = getAllDioHeaderFlagBits();

	EXPECT_EQ(types.size(), 1);
	EXPECT_TRUE(std::find(types.begin(), types.end(), DioHeaderFlagBit::HasViewport) != types.end());
}


/**
 * \brief Test UString dioHeaderFlagBitsToString(DioHeaderFlagBits flags).
 */
TEST(DioHeaderFlagBit, dioHeaderFlagBitsToString)
{
	EXPECT_STRCASEEQ(dioHeaderFlagBitsToString((DioHeaderFlagBits)0).c_str(), "[ NONE ]");
	EXPECT_STRCASEEQ(dioHeaderFlagBitsToString((DioHeaderFlagBits)DioHeaderFlagBit::HasViewport).c_str(), "[ HasViewport ]");
	EXPECT_STRCASEEQ(dioHeaderFlagBitsToString((DioHeaderFlagBits)128).c_str(), "[ NONE ]");
}


/**
 * \brief Test std::ostream& operator << (std::ostream& os, const DioHeaderFlagBits& dioHeaderFlagBits).
 */
TEST(DioHeaderFlagBit, operator_lessthanlessthan)
{
	std::ostringstream oss;
	oss << (DioHeaderFlagBit)0;
	EXPECT_STRCASEEQ(oss.str().c_str(), "None");
	oss.clear();
	oss.str("");

	oss << DioHeaderFlagBit::HasViewport;
	EXPECT_STRCASEEQ(oss.str().c_str(), "HasViewport");
	oss.clear();
	oss.str("");

	oss << (DioHeaderFlagBit)129;
	EXPECT_STRCASEEQ(oss.str().c_str(), "UNKNOWN");
}


} // End namespace nameless namespace

