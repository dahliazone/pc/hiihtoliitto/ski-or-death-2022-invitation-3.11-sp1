// Tested 100% 2016-10-03 -codise

#include <Verso/DepthIo/TiltStateRawKinect.hpp>
#include <gtest/gtest.h>

namespace {

using namespace Verso;


/**
 * \brief Test constructor.
 */
TEST(TiltStateRawKinect, constructor)
{
	TiltStateRawKinect tiltState1;
	EXPECT_EQ(0, tiltState1.accelerometerX);
	EXPECT_EQ(0, tiltState1.accelerometerY);
	EXPECT_EQ(0, tiltState1.accelerometerZ);

	TiltStateRawKinect tiltState2(1, 2, 3);
	EXPECT_EQ(1, tiltState2.accelerometerX);
	EXPECT_EQ(2, tiltState2.accelerometerY);
	EXPECT_EQ(3, tiltState2.accelerometerZ);
}


/**
 * \brief Test set().
 */
TEST(TiltStateRawKinect, set)
{
	TiltStateRawKinect tiltState1;
	tiltState1.set(0, 0, 0);
	EXPECT_EQ(0, tiltState1.accelerometerX);
	EXPECT_EQ(0, tiltState1.accelerometerY);
	EXPECT_EQ(0, tiltState1.accelerometerZ);

	TiltStateRawKinect tiltState2;
	tiltState2.set(-10, 5, -3);
	EXPECT_EQ(-10, tiltState2.accelerometerX);
	EXPECT_EQ(  5, tiltState2.accelerometerY);
	EXPECT_EQ( -3, tiltState2.accelerometerZ);
}


/**
 * \brief Test getGravityAdjusted().
 */
TEST(TiltStateRawKinect, getGravityAdjusted)
{
	TiltStateRawKinect tiltState1;
	double x1 = 666.0f, y1 = 666.0f, z1 = 666.0f;
	tiltState1.getGravityAdjusted(x1, y1, z1);
	EXPECT_EQ(0.0f, x1);
	EXPECT_EQ(0.0f, y1);
	EXPECT_EQ(0.0f, z1);

	TiltStateRawKinect tiltState2(100, 200, 300);
	double x2 = 666.0f, y2 = 666.0f, z2 = 666.0f;
	tiltState2.getGravityAdjusted(x2, y2, z2);
	EXPECT_EQ(100.0f / TiltStateRawKinect::COUNTS_PER_G * TiltStateRawKinect::GRAVITY_ACCELERATION, x2);
	EXPECT_EQ(200.0f / TiltStateRawKinect::COUNTS_PER_G * TiltStateRawKinect::GRAVITY_ACCELERATION, y2);
	EXPECT_EQ(300.0f / TiltStateRawKinect::COUNTS_PER_G * TiltStateRawKinect::GRAVITY_ACCELERATION, z2);
}


/**
 * \brief Test equals().
 */
TEST(TiltStateRawKinect, equals)
{
	TiltStateRawKinect a1;
	a1.accelerometerX = 1;
	a1.accelerometerY = 2;
	a1.accelerometerZ = 3;

	TiltStateRawKinect a2;
	a2.accelerometerX = 1;
	a2.accelerometerY = 2;
	a2.accelerometerZ = 3;

	TiltStateRawKinect b;
	b.accelerometerX = 4;
	b.accelerometerY = 5;
	b.accelerometerZ = 6;

	TiltStateRawKinect c;
	c.accelerometerX = 1;
	c.accelerometerY = -2;
	c.accelerometerZ = 3;

	EXPECT_TRUE(a1.equals(a1));
	EXPECT_TRUE(a2.equals(a2));
	EXPECT_TRUE(b.equals(b));
	EXPECT_TRUE(c.equals(c));

	EXPECT_TRUE(a1.equals(a2));
	EXPECT_TRUE(a2.equals(a1));

	EXPECT_TRUE(a1.equals(a2));
	EXPECT_TRUE(a2.equals(a1));

	EXPECT_FALSE(a1.equals(b));
	EXPECT_FALSE(b.equals(a1));

	EXPECT_FALSE(a2.equals(b));
	EXPECT_FALSE(b.equals(a2));

	EXPECT_FALSE(c.equals(a1));
	EXPECT_FALSE(a1.equals(c));

	EXPECT_FALSE(c.equals(a2));
	EXPECT_FALSE(a2.equals(c));

	EXPECT_FALSE(c.equals(b));
	EXPECT_FALSE(b.equals(c));
}


} // End namespace nameless namespace

