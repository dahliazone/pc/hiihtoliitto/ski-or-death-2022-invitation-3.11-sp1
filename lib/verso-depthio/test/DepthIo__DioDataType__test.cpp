// Tested 100% 2016-10-03 -codise

#include <Verso/DepthIo/DioDataType.hpp>
#include <gtest/gtest.h>
#include <algorithm>
#include <sstream>

namespace {

using namespace Verso;


/**
 * \brief Test constructor.
 */
TEST(DioDataType, constructor)
{
	DioDataType type1 = DioDataType::Array_Xyz;

	EXPECT_EQ(DioDataType::Array_Xyz, type1);
	EXPECT_EQ(type1, DioDataType::Array_Xyz);
}


/**
 * \brief Test UString dioDataTypeToString(DioDataType dioDataType).
 */
TEST(DioDataType, dioDataTypeToString)
{
	DioDataType valid1 = DioDataType::Array_NormalXyz_Curvature;
	EXPECT_STRCASEEQ(dioDataTypeToString(valid1).c_str(), "Array_NormalXyz_Curvature");

	DioDataType valid2 = DioDataType::Array_Rgb;
	EXPECT_STRCASEEQ(dioDataTypeToString(valid2).c_str(), "Array_Rgb");

	DioDataType invalid = (DioDataType)128;
	EXPECT_STRCASEEQ(dioDataTypeToString(invalid).c_str(), "UNKNOWN");
}


/**
 * \brief Test DioVariableType dioDataTypesDioVariableType(DioDataType dioDataType).
 */
TEST(DioDataType, dioDataTypesDioVariableType)
{
	EXPECT_EQ(dioDataTypesDioVariableType(DioDataType::Array_Xyz), DioVariableType::Float32);
	EXPECT_EQ(dioDataTypesDioVariableType(DioDataType::Array_NormalXyz_Curvature), DioVariableType::Float32);
	EXPECT_EQ(dioDataTypesDioVariableType(DioDataType::Array_Depth), DioVariableType::Uint16);
	EXPECT_EQ(dioDataTypesDioVariableType(DioDataType::Array_Rgb), DioVariableType::Uint8);
	EXPECT_EQ(dioDataTypesDioVariableType(DioDataType::Array_Intensity), DioVariableType::Uint8);
	EXPECT_EQ(dioDataTypesDioVariableType(DioDataType::Array_Segmentation), DioVariableType::Uint8);
	EXPECT_EQ(dioDataTypesDioVariableType((DioDataType)128), DioVariableType::Unset);
}


/**
 * \brief Test bool hasDioDataType(DioDataTypes dioDataTypes, DioDataType dioDataType).
 */
TEST(DioDataType, hasDioDataType)
{
	DioDataTypes test1 = (DioDataTypes)DioDataType::Array_Xyz |
	                     (DioDataTypes)DioDataType::Array_Intensity |
	                     (DioDataTypes)DioDataType::Array_Segmentation;

	EXPECT_TRUE(hasDioDataType(test1, DioDataType::Array_Xyz));
	EXPECT_FALSE(hasDioDataType(test1, DioDataType::Array_NormalXyz_Curvature));
	EXPECT_FALSE(hasDioDataType(test1, DioDataType::Array_Depth));
	EXPECT_FALSE(hasDioDataType(test1, DioDataType::Array_Rgb));
	EXPECT_TRUE(hasDioDataType(test1, DioDataType::Array_Intensity));
	EXPECT_TRUE(hasDioDataType(test1, DioDataType::Array_Segmentation));

	DioDataTypes test2 = (DioDataTypes)DioDataType::Array_NormalXyz_Curvature |
	                     (DioDataTypes)DioDataType::Array_Depth |
	                     (DioDataTypes)DioDataType::Array_Rgb;

	EXPECT_FALSE(hasDioDataType(test2, DioDataType::Array_Xyz));
	EXPECT_TRUE(hasDioDataType(test2, DioDataType::Array_NormalXyz_Curvature));
	EXPECT_TRUE(hasDioDataType(test2, DioDataType::Array_Depth));
	EXPECT_TRUE(hasDioDataType(test2, DioDataType::Array_Rgb));
	EXPECT_FALSE(hasDioDataType(test2, DioDataType::Array_Intensity));
	EXPECT_FALSE(hasDioDataType(test2, DioDataType::Array_Segmentation));
}


/**
 * \brief Test std::vector<DioDataType> getAllDioDataTypes().
 */
TEST(DioDataType, getAllDioDataTypes)
{
	std::vector<DioDataType> types = getAllDioDataTypes();

	EXPECT_EQ(types.size(), 6);
	EXPECT_TRUE(std::find(types.begin(), types.end(), DioDataType::Array_Xyz) != types.end());
	EXPECT_TRUE(std::find(types.begin(), types.end(), DioDataType::Array_NormalXyz_Curvature) != types.end());
	EXPECT_TRUE(std::find(types.begin(), types.end(), DioDataType::Array_Depth) != types.end());
	EXPECT_TRUE(std::find(types.begin(), types.end(), DioDataType::Array_Rgb) != types.end());
	EXPECT_TRUE(std::find(types.begin(), types.end(), DioDataType::Array_Intensity) != types.end());
	EXPECT_TRUE(std::find(types.begin(), types.end(), DioDataType::Array_Segmentation) != types.end());
}


/**
 * \brief Test uint8_t dioDataTypeCount(DioDataTypes dioDataTypes).
 */
TEST(DioDataType, dioDataTypeCount)
{
	EXPECT_EQ(dioDataTypeCount((DioDataTypes)0), 0);

	EXPECT_EQ(dioDataTypeCount((DioDataTypes)Array_Xyz), 1);
	EXPECT_EQ(dioDataTypeCount((DioDataTypes)Array_Rgb), 1);
	EXPECT_EQ(dioDataTypeCount((DioDataTypes)Array_Segmentation), 1);

	EXPECT_EQ(dioDataTypeCount((DioDataTypes)Array_Xyz | (DioDataTypes)Array_Segmentation), 2);
	EXPECT_EQ(dioDataTypeCount((DioDataTypes)Array_Depth | (DioDataTypes)Array_Rgb), 2);

	EXPECT_EQ(dioDataTypeCount((DioDataTypes)Array_Xyz |
	                           (DioDataTypes)Array_Depth |
	                           (DioDataTypes)Array_Intensity), 3);

	EXPECT_EQ(dioDataTypeCount((DioDataTypes)Array_NormalXyz_Curvature |
	                           (DioDataTypes)Array_Rgb |
	                           (DioDataTypes)Array_Segmentation), 3);

	EXPECT_EQ(dioDataTypeCount((DioDataTypes)Array_NormalXyz_Curvature |
	                           (DioDataTypes)Array_Rgb |
	                           (DioDataTypes)Array_Intensity |
	                           (DioDataTypes)Array_Segmentation), 4);

	EXPECT_EQ(dioDataTypeCount((DioDataTypes)Array_Xyz |
	                           (DioDataTypes)Array_Depth |
	                           (DioDataTypes)Array_Rgb |
	                           (DioDataTypes)Array_Intensity |
	                           (DioDataTypes)Array_Segmentation), 5);

	EXPECT_EQ(dioDataTypeCount((DioDataTypes)Array_Xyz |
	                           (DioDataTypes)Array_NormalXyz_Curvature |
	                           (DioDataTypes)Array_Depth |
	                           (DioDataTypes)Array_Rgb |
	                           (DioDataTypes)Array_Intensity |
	                           (DioDataTypes)Array_Segmentation), 6);
}


/**
 * \brief Test UString dioDataTypesToString(DioDataTypes dioDataTypes).
 */
TEST(DioDataType, dioDataTypesToString)
{
	EXPECT_STRCASEEQ(dioDataTypesToString((DioDataTypes)0).c_str(),
	                 "[ NONE ]");

	EXPECT_STRCASEEQ(dioDataTypesToString((DioDataTypes)Array_NormalXyz_Curvature |
	                                      (DioDataTypes)Array_Depth |
	                                      (DioDataTypes)Array_Intensity).c_str(),
	                 "[ Array_NormalXyz_Curvature, Array_Depth, Array_Intensity ]");

	EXPECT_STRCASEEQ(dioDataTypesToString((DioDataTypes)Array_Xyz |
	                                      (DioDataTypes)Array_NormalXyz_Curvature |
	                                      (DioDataTypes)Array_Depth |
	                                      (DioDataTypes)Array_Rgb |
	                                      (DioDataTypes)Array_Intensity |
	                                      (DioDataTypes)Array_Segmentation).c_str(),
	                 "[ Array_Xyz, Array_NormalXyz_Curvature, Array_Depth, Array_Rgb, Array_Intensity, Array_Segmentation ]");
}


/**
 * \brief Test std::ostream& operator << (std::ostream& os, const DioDataType& dioDataType).
 */
TEST(DioDataType, operator_lessthanlessthan)
{
	std::ostringstream oss;
	oss << DioDataType::Array_Xyz;
	EXPECT_STRCASEEQ(oss.str().c_str(), "Array_Xyz");
	oss.clear();
	oss.str("");

	oss << DioDataType::Array_Segmentation;
	EXPECT_STRCASEEQ(oss.str().c_str(), "Array_Segmentation");
	oss.clear();
	oss.str("");

	oss << (DioDataType)0;
	EXPECT_STRCASEEQ(oss.str().c_str(), "Unset");
}


} // End namespace nameless namespace

