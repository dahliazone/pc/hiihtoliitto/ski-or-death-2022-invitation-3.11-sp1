#include <Verso/System/UString.hpp>
#include <Verso/System/Logger.hpp>

using namespace std;
using namespace Verso;

int main(int argc, char* argv[])
{
	(void)argc; (void)argv;
	VERSO_LOG_INFO("depthio/examples", "Hello verso-depthio example!");

	return EXIT_SUCCESS;
}

