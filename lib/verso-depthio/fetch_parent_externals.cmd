PATH=%PATH%;"C:\Program Files\SlikSvn\bin";"C:\Program Files (x86)\SlikSvn\bin";

echo "verso-depthio: Fetching externals from repositories..."

IF NOT EXIST ..\snappy (
	git clone https://gitlab.com/dahliazone/pc/extlib/snappy.git ..\snappy
) else (
	pushd ..\snappy
	git pull --rebase
	popd
)

IF NOT EXIST ..\freenect (
	git clone https://gitlab.com/dahliazone/pc/extlib/freenect.git ..\freenect
) else (
	pushd ..\freenect
	git pull --rebase
	popd
)

IF NOT EXIST ..\verso-3d (
	git clone https://gitlab.com/dahliazone/pc/lib/verso-3d.git ..\verso-3d
) else (
	pushd ..\verso-3d
	git pull --rebase
	popd
)

echo "verso-3d: Resursing into..."
pushd ..\verso-3d
call fetch_parent_externals.cmd
popd

echo "verso-depthio: All done"

