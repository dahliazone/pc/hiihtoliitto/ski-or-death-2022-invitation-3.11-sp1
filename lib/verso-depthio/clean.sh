#!/bin/sh

make clean
rm -rf clean builds Makefile*
rm -rf src/Makefile*
rm -rf examples/Makefile*
rm -rf camcorder/Makefile*
rm -rf diodev/Makefile*
rm -rf test/Makefile*

