#!/bin/bash

echo "verso-depthio: Building parent externals..."

echo "snappy: Building..."
mkdir -p ../snappy/build
pushd ../snappy/build
cmake ..
make -j5
popd

echo "freenect: Building..."
mkdir -p ../freenect/build
pushd ../freenect/build
cmake -DBUILD_AUDIO=true ..
make -j5
popd

echo "verso-depthio: Building done"

