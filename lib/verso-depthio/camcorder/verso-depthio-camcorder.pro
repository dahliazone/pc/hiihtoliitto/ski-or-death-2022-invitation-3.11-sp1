# == Shared ===============================================================
! include( ../verso-depthio-common.pri ) {
	error("Couldn't find the verso-depthio-common.pri file!")
}

TEMPLATE = app

DESTDIR = "$${BUILD_DIR}/bin"
win* {
	DESTDIR = "$${BUILD_DIR}"
}
TARGET = libverso-depthio-camcorder


# Includes
INCLUDEPATH += . ../include
INCLUDEPATH += $${LIBPATH}/freenect/include
INCLUDEPATH += $${LIBPATH}/freenect/wrappers/c_sync

DEPENDPATH += \
	$${PWD}

HEADERS += \
	Playground.hpp

SOURCES += \
	camcorder-main.cpp #\
#    "$${LIBPATH}/freenect/wrappers/c_sync/libfreenect_sync.c" \
#	glpclview.c \
#	glview.c \
#	micview.c


# == Linux ================================================================
linux-g++-32 {
}

linux-g++ {
	# BASS
	LIBS += -L"$${LIBPATH}/BASS/linux/x64"
}

linux-* {
	QMAKE_POST_LINK += echo "Post-link process..."

	# Verso-depthio
	LIBS += -L"$${BUILD_DIR}/" -lverso-depthio

	# Verso-3d
	LIBS += -L"$${LIBPATH}/verso-3d/builds/$${BUILD_SUBDIR}" -lverso-3d
	QMAKE_POST_LINK += && cp -rf "$${LIBPATH}/verso-3d/builds/$${BUILD_SUBDIR}/*.so*" "$${DESTDIR}/"

	# Verso-gfx
	LIBS += -L"$${LIBPATH}/verso-gfx/builds/$${BUILD_SUBDIR}" -lverso-gfx
	QMAKE_POST_LINK += && cp -rf "$${LIBPATH}/verso-gfx/builds/$${BUILD_SUBDIR}/*.so*" "$${DESTDIR}/"

	# Verso-base
	LIBS += -L"$${LIBPATH}/verso-base/builds/$${BUILD_SUBDIR}" -lverso-base
	QMAKE_POST_LINK += && cp -rf "$${LIBPATH}/verso-base/builds/$${BUILD_SUBDIR}/*.so*" "$${DESTDIR}/"

	# BASS
	INCLUDEPATH += "$${LIBPATH}/BASS/linux"
	QMAKE_CXXFLAGS += -D__LINUX_ALSA__
	LIBS += -lbass
	QMAKE_POST_LINK += && cp -rf "$${LIBPATH}/BASS/linux/x64/*.so" "$${DESTDIR}/"

	# RtMidi
	QMAKE_CXXFLAGS += -D__LINUX_ALSA__
	LIBS += -lpthread -lasound

	# Assimp
	LIBS += -L"$${LIBPATH}/assimp/build/code" -lassimp
	QMAKE_POST_LINK += && cp -rf "$${LIBPATH}/assimp/build/code/*.so" "$${DESTDIR}/"
	QMAKE_POST_LINK += && cp -rf "$${LIBPATH}/assimp/build/code/*.so.*" "$${DESTDIR}/"

	# Snappy
	LIBS += -L$${LIBPATH}/snappy/.libs/ -lsnappy
	QMAKE_POST_LINK += && cp -f "$${LIBPATH}/snappy/.libs/libsnappy.so*" "$$DESTDIR/"

	# Freenect
	LIBS += -L$${LIBPATH}/freenect/build/lib -lfreenect
	QMAKE_POST_LINK += && cp -f "$${LIBPATH}/freenect/build/lib/libfreenect.so*" "$$DESTDIR/"

	# GLUT
	LIBS += -lglut

	# Other libs
	LIBS += -lGL -lGLU -lGLEW -lSDL2_image -lSDL2_ttf -lSDL2

	# cmath
	LIBS += -lm

	# Scripts
	QMAKE_POST_LINK += && cp -rf "$${PWD}/libverso-depthio-camcorder.sh" "$${DESTDIR}/.."

	# Data
	QMAKE_POST_LINK += && cp -rf "$${PWD}/../data" "$${DESTDIR}/.."
}


# == OS X =================================================================
macx {
	QMAKE_POST_LINK += echo "Post-link process..."

	CONFIG -= app_bundle

	# Verso-depthio
	LIBDEPTHIO = libdepthio.1.dylib
	QMAKE_POST_LINK += && cp -f "$${LIBPATH}/verso-depthio/builds/$${BUILD_SUBDIR}/$${LIBDEPTHIO}*" "$${DESTDIR}/$${TARGET}".app/Contents/MacOS/
	QMAKE_POST_LINK += && install_name_tool -change "$$LIBDEPTHIO" "@executable_path/$$LIBDEPTHIO" "$${DESTDIR}/$${TARGET}".app/Contents/MacOS/$${TARGET}
	LIBS += -L"$${BUILD_DIR}" -lverso-depthio

	# Libraries
	LIBVERSO3D = libverso-3d.1.dylib
	QMAKE_POST_LINK += && cp -rf "$${LIBPATH}/verso-3d/builds/$${BUILD_SUBDIR}/$${LIBVERSO3D}*" "$${DESTDIR}/$${TARGET}".app/Contents/MacOS/
	QMAKE_POST_LINK += && install_name_tool -change "$${LIBVERSO3D}" "@executable_path/$${LIBVERSO3D}" "$${DESTDIR}/$${TARGET}".app/Contents/MacOS/$${TARGET}
	LIBS += -L"$${BUILD_DIR}" -lverso-3d

	# Snappy
	LIBS += -L$${LIBPATH}/snappy/.libs/ -lsnappy
	LIBSNAPPY = libsnappy.1.dylib
	QMAKE_POST_LINK += && cp -f "$${LIBPATH}/snappy/.libs/$$LIBSNAPPY" "$$DESTDIR/"
	QMAKE_POST_LINK += && install_name_tool -change "/usr/local/lib/$$LIBSNAPPY" "@executable_path/$$LIBSNAPPY" "$$DESTDIR/$$TARGET"

	# Freenect
	LIBS += -L$${LIBPATH}/freenect/build/lib -lfreenect

	LIBBASS = libbass.dylib
	QMAKE_POST_LINK += && cp -rf "$${LIBPATH}/BASS/osx/$${LIBBASS}" "$${DESTDIR}/$${TARGET}".app/Contents/MacOS/
	QMAKE_POST_LINK += && install_name_tool -change "$${LIBBASS}" "@executable_path/$${LIBBASS}" "$${DESTDIR}/$${TARGET}".app/Contents/MacOS/$${TARGET}

	LIBS += -framework OpenGL -lGLUT

	# Scripts
	QMAKE_POST_LINK += && cp -rf "$${PWD}/Playground-osx.sh" "$${DESTDIR}/"

	# Data
	QMAKE_POST_LINK += && cp -rf "$${PWD}/../data" "$${DESTDIR}/$${TARGET}".app/Contents/Resources/
}


# == Windows ==============================================================
win* {
	QMAKE_POST_LINK += @ECHO "Post-link process..."

	# Verso-depthio
	LIBS += -L"$${BUILD_DIR}" -lverso-depthio

	# Verso-3d
	LIBS += -L"$${LIBPATH}/verso-3d/builds/$${BUILD_SUBDIR}" -lverso-3d

	# Verso-gfx
	LIBS += -L"$${LIBPATH}/verso-gfx/builds/$${BUILD_SUBDIR}" -lverso-gfx

	# Verso-base
	LIBS += -L"$${LIBPATH}/verso-base/builds/$${BUILD_SUBDIR}" -lverso-base

	# SDL2 extras
	LIBS += -lSDL2_image -lSDL2_ttf

	# SDL2 winmain
	LIBS += -lSDL2main -lSDL2

	# BASS
	LIBS += -L"$${LIBPATH}/BASS/windows" -L"$${LIBPATH}/BASS/windows/c/safeseh" -lbass
}

win*:CONFIG(debug, debug|release) {
	LIBS += -lglew32d -lassimp-vc120-mtd

	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\assimp-vc120-mtd.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${LIBPATH}\BASS\windows\bass.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\glew32d.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\libfreetype-6.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\libjpeg-9.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\libpng16-16.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\SDL2.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\SDL2_image.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\SDL2_ttf.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\zlib1.dll\" \"$${DESTDIR}\"

	# Debug output on console
	CONFIG += console
}

win*:CONFIG(release, debug|release) {
	LIBS += -lglew32 -lassimp-vc120-mt

	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\assimp-vc120-mt.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${LIBPATH}\BASS\windows\bass.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\glew32.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\libfreetype-6.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\libjpeg-9.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\libpng16-16.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\SDL2.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\SDL2_image.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\SDL2_ttf.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\zlib1.dll\" \"$${DESTDIR}\"
}

win* {
	LIBS += -lglu32 -lopengl32

	# Copy data directory
	QMAKE_POST_LINK += && XCOPY /s /q /y /i \"$${PWD}\..\data\" \"$${DESTDIR}\data\"

	# Remove temporaries
	#QMAKE_POST_LINK += && RMDIR /S /Q \"$${PWD}\..\debug\" \"$${PWD}\..\release\"
}

