! include( ../common.pri ) {
	error("Couldn't find the common.pri file!")
}

HEADERPATH = ../include
LIBPATH = ../lib
INCLUDEPATH += $$HEADERPATH
INCLUDEPATH += ../lib/snappy/
INCLUDEPATH += ../lib/Trieng/include
INCLUDEPATH += ../lib/Trieng/lib/utf8-cpp/source/

TEMPLATE = lib
CONFIG -= qt core gui
HEADERS += \
	$$LIBPATH/Trieng/include/File.hpp \
	$$HEADERPATH/DioFile.hpp \
	$$HEADERPATH/KinectPlaintextFile.hpp \
	$$HEADERPATH/DataType.hpp \
	$$HEADERPATH/VariableType.hpp \
	$$HEADERPATH/DioPackFormat.hpp \
	$$HEADERPATH/DioChunkType.hpp \
	$$HEADERPATH/DioHeaderFlagBit.hpp \
	$$HEADERPATH/FrameData.hpp \
	$$HEADERPATH/FrameAnimation.hpp \
	$$HEADERPATH/TiltStateRawKinect.hpp

SOURCES += \
	DioFile.cpp \
	KinectPlaintextFile.cpp \
	FrameData.cpp \
	TiltStateRawKinect.cpp

# Will build the final executable in the main project directory.
DESTDIR = "$$OUT_PWD/../$$BUILD_DIR"
TARGET = depthio


linux-* {
	QMAKE_CXXFLAGS += -std=c++17
	LIBS += -L../lib/snappy/.libs/
	LIBS += -lsnappy
}

macx {
	QMAKE_CXXFLAGS += -std=c++17
	LIBS += -L../lib/snappy/.libs/
	LIBS += -lsnappy

	INCLUDEPATH += /usr/local/include
	#CONFIG -= x86_64
	#CONFIG += x86
	#CONFIG -= app_bundle
}

