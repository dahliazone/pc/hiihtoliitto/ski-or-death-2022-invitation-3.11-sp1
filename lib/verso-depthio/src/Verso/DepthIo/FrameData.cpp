#include <Verso/DepthIo/FrameData.hpp>

namespace Verso {


FrameData::FrameData(uint32_t index, DioDataTypes requiredDioDataTypes, uint32_t timestamp) :
	mIndex(index),
	mRequiredDioDataTypes(requiredDioDataTypes),
	mActualDioDataTypes(0),
	mTimestamp(0),
	mTimestampSec(0),
	mRawKinectTiltState(),
	mDataXyz(),
	mDataDepth(),
	mDataRgb(),
	mExtPtrForDataXyzPointCount(0),
	mExtPtrDataXyz(nullptr),
	mExtPtrForDataDepthPointCount(0),
	mExtPtrDataDepth(nullptr),
	mExtPtrForDataRgbPointCount(0),
	mExtPtrDataRgb(nullptr),
	mErrorMsg()
{
	setTimestamp(timestamp);
}


FrameData::~FrameData()
{
	// no need to free extPtrDataDepth & extPtrDataRgb, it should be free'd by the caller
	mExtPtrForDataXyzPointCount = 0;
	mExtPtrDataXyz = nullptr;
	mExtPtrForDataDepthPointCount = 0;
	mExtPtrDataDepth = nullptr;
	mExtPtrForDataRgbPointCount = 0;
	mExtPtrDataRgb = nullptr;
}


void FrameData::addDataXyz(float x, float y, float z)
{
	mDataXyz.push_back(x);
	mDataXyz.push_back(y);
	mDataXyz.push_back(z);
	mActualDioDataTypes |= DioDataType::Array_Xyz;
}


void FrameData::addDataDepth(uint16_t depth)
{
	mDataDepth.push_back(depth);
	mActualDioDataTypes |= DioDataType::Array_Depth;
}


void FrameData::addDataRgb(uint8_t r, uint8_t g, uint8_t b)
{
	mDataRgb.push_back(r);
	mDataRgb.push_back(g);
	mDataRgb.push_back(b);
	mActualDioDataTypes |= DioDataType::Array_Rgb;
}


void FrameData::setExternalDataXyz(float* extPtrDataXyz, size_t pointCount)
{
	mExtPtrForDataXyzPointCount = pointCount;
	mExtPtrDataXyz = extPtrDataXyz;
	mActualDioDataTypes |= DioDataType::Array_Xyz;
}


void FrameData::clearDataXyz()
{
	mExtPtrForDataXyzPointCount = 0;
	mExtPtrDataXyz = nullptr;
	mDataXyz.clear();
	mActualDioDataTypes &= ~DioDataType::Array_Xyz;
}


void FrameData::setExternalDataDepth(uint16_t* extPtrDataDepth, size_t pointCount)
{
	mExtPtrForDataDepthPointCount = pointCount;
	mExtPtrDataDepth = extPtrDataDepth;
	mActualDioDataTypes |= DioDataType::Array_Depth;
}


void FrameData::clearDataDepth()
{
	mExtPtrForDataDepthPointCount = 0;
	mExtPtrDataDepth = nullptr;
	mDataDepth.clear();
	mActualDioDataTypes &= ~DioDataType::Array_Depth;
}


void FrameData::setExternalDataRgb(uint8_t* extPtrDataRgb, size_t pointCount)
{
	mExtPtrForDataRgbPointCount = pointCount;
	mExtPtrDataRgb = extPtrDataRgb;
	mActualDioDataTypes |= DioDataType::Array_Rgb;
}


void FrameData::clearDataRgb()
{
	mExtPtrForDataRgbPointCount = 0;
	mExtPtrDataRgb = nullptr;
	mDataRgb.clear();
	mActualDioDataTypes &= ~DioDataType::Array_Rgb;
}


void FrameData::setRawKinectTiltState(int16_t accelerometerX, int16_t accelerometerY, int16_t accelerometerZ)
{
	mRawKinectTiltState.set(accelerometerX, accelerometerY, accelerometerZ);
}




size_t FrameData::pointsXyzCount() const
{
	if (mExtPtrForDataXyzPointCount != 0)
		return mExtPtrForDataXyzPointCount;
	else if (mDataXyz.size() != 0)
		return mDataXyz.size() / 3;
	else
		return 0;
}


size_t FrameData::pointsDepthCount() const
{
	if (mExtPtrForDataDepthPointCount != 0)
		return mExtPtrForDataDepthPointCount;
	else if (mDataDepth.size() != 0)
		return mDataDepth.size();
	else
		return 0;
}


size_t FrameData::pointsRgbCount() const
{
	if (mExtPtrForDataRgbPointCount != 0)
		return mExtPtrForDataRgbPointCount;
	else if (mDataRgb.size() != 0)
		return mDataRgb.size() / 3;
	else
		return 0;
}


size_t FrameData::pointsCount() const
{
	if (mActualDioDataTypes & static_cast<uint8_t>(DioDataType::Array_Xyz))
		return pointsXyzCount();
	if (mActualDioDataTypes & static_cast<uint8_t>(DioDataType::Array_Depth))
		return pointsDepthCount();
	if (mActualDioDataTypes & static_cast<uint8_t>(DioDataType::Array_Rgb))
		return pointsRgbCount();
	else
		return 0;
}


const float* FrameData::getDataXyz() const
{
	if (mExtPtrForDataDepthPointCount != 0)
		return mExtPtrDataXyz;
	else if (mDataXyz.size() != 0)
		return mDataXyz.data();
	else
		return nullptr;
}


const uint16_t* FrameData::getDataDepth() const
{
	if (mExtPtrForDataDepthPointCount != 0)
		return mExtPtrDataDepth;
	else if (mDataDepth.size() != 0)
		return mDataDepth.data();
	else
		return nullptr;
}


const uint8_t* FrameData::getDataRgb() const
{
	if (mExtPtrForDataRgbPointCount != 0)
		return mExtPtrDataRgb;
	else if (mDataRgb.size() != 0)
		return mDataRgb.data();
	else
		return nullptr;
}


bool FrameData::checkIfDataIsValid()
{
	if (mRequiredDioDataTypes == 0) {
		mErrorMsg = "FrameData::CheckIfDataIsValid(): No required data types!";
		return false;
	}

	if (mRequiredDioDataTypes & static_cast<uint8_t>(DioDataType::Array_Xyz) && !(mActualDioDataTypes & static_cast<uint8_t>(DioDataType::Array_Xyz))) {
		mErrorMsg = "FrameData::CheckIfDataIsValid(): Required data type Array_Xyz has no data added!";
		return false;
	}

	if (mRequiredDioDataTypes & static_cast<uint8_t>(DioDataType::Array_Depth) && !(mActualDioDataTypes & static_cast<uint8_t>(DioDataType::Array_Depth))) {
		mErrorMsg = "FrameData::CheckIfDataIsValid(): Required data type Array_Depth has no data added!";
		return false;
	}

	if (mRequiredDioDataTypes & static_cast<uint8_t>(DioDataType::Array_Rgb) && !(mActualDioDataTypes & static_cast<uint8_t>(DioDataType::Array_Rgb))) {
		mErrorMsg = "FrameData::CheckIfDataIsValid(): Required data type Array_Rgb has no data added!";
		return false;
	}

	if ((mActualDioDataTypes & static_cast<uint8_t>(DioDataType::Array_Xyz)) && (mActualDioDataTypes & static_cast<uint8_t>(DioDataType::Array_Rgb))) {
		if (pointsXyzCount() != pointsRgbCount()) {
			mErrorMsg = "FrameData::CheckIfDataIsValid(): Different number of point for XYZ(";
			mErrorMsg.append2(pointsXyzCount());
			mErrorMsg.append2(") and RGB(");
			mErrorMsg.append2(pointsRgbCount());
			mErrorMsg.append2(")!");
			return false;
		}
	}

	if ((mActualDioDataTypes & static_cast<uint8_t>(DioDataType::Array_Depth)) && (mActualDioDataTypes & static_cast<uint8_t>(DioDataType::Array_Rgb))) {
		if (pointsDepthCount() != pointsRgbCount()) {
			mErrorMsg = "FrameData::CheckIfDataIsValid(): Different number of point for Depth(";
			mErrorMsg.append2(pointsDepthCount());
			mErrorMsg.append2(") and RGB(");
			mErrorMsg.append2(pointsRgbCount());
			mErrorMsg.append2(")!");
			return false;
		}
	}

	if ((mActualDioDataTypes & static_cast<uint8_t>(DioDataType::Array_Rgb)) && (mActualDioDataTypes & static_cast<uint8_t>(DioDataType::Array_Depth))) {
		if (pointsRgbCount() != pointsDepthCount()) {
			mErrorMsg = "FrameData::CheckIfDataIsValid(): Different number of point for RGB(";
			mErrorMsg.append2(pointsRgbCount());
			mErrorMsg.append2(") and Depth(");
			mErrorMsg.append2(pointsDepthCount());
			mErrorMsg.append2(")!");
			return false;
		}
	}

	return true;
}


bool FrameData::equals(const FrameData& frame) const
{
	if (mIndex != frame.mIndex)
		return false;

	if (mRequiredDioDataTypes != frame.mRequiredDioDataTypes)
		return false;

	if (mActualDioDataTypes != frame.mActualDioDataTypes)
		return false;

	if (mTimestamp != frame.mTimestamp)
		return false;

	if (mTimestampSec != frame.mTimestampSec)
		return false;

	if (mRawKinectTiltState.equals(frame.getRawKinectTiltState()) == false)
		return false;

	const float* aXyz = getDataXyz();
	const float* bXyz = frame.getDataXyz();
	for (size_t i=0; i<pointsXyzCount()*3; ++i)
		if (aXyz[i] != bXyz[i])
			return false;

	const uint16_t* aDepth = getDataDepth();
	const uint16_t* bDepth = frame.getDataDepth();
	for (size_t i=0; i<pointsDepthCount(); ++i)
		if (aDepth[i] != bDepth[i])
			return false;

	const uint8_t* aRgb = getDataRgb();
	const uint8_t* bRgb = frame.getDataRgb();
	for (size_t i=0; i<pointsRgbCount()*3; ++i)
		if (aRgb[i] != bRgb[i])
			return false;

	/*
	if (mDataXyz != frame.mDataXyz)
		return false;

	if (mDataDepth != frame.mDataDepth)
		return false;

	if (mDataRgb != frame.mDataRgb)
		return false;

	if (mExtPtrForDataXyzPointCount != frame.mExtPtrForDataXyzPointCount)
		return false;

	const float* frame.get
	for (size_t i=0; i<; ++i)
		if (mExtPtrDataXyz[i] != frame.ads)
			return false;
	float* ;

	if (mExtPtrForDataDepthPointCount != frame.mExtPtrForDataDepthPointCount)
		return false;

	if (ads != frame.ads)
		return false;
	uint16_t* mExtPtrDataDepth;

	if (mExtPtrForDataRgbPointCount != frame.mExtPtrForDataRgbPointCount)
		return false;

	if (ads != frame.ads)
		return false;
	uint8_t* mExtPtrDataRgb;
	*/

	if (mErrorMsg == frame.mErrorMsg)
		return false;

	return true;
}


UString FrameData::toString() const
{
	UString t = "Frame @ ";
	t.append2(mTimestamp);
	t.append(", ");
	t.append2(pointsXyzCount());
	t.append(" points, ");
	t.append2(pointsRgbCount());
	t.append(" color values");
	return t;
}


UString FrameData::toStringFull() const
{
	UString t = toString();
	t += "\n[ ";
	if (hasDioDataType(getActualDioDataTypes(), DioDataType::Array_Xyz) == true) {
		for (size_t i=0; i<mDataXyz.size(); i+=3) {
			t.append("{ ");
			t.append2(mDataXyz[i]);
			t.append(", ");
			t.append2(mDataXyz[i+1]);
			t.append(", ");
			t.append2(mDataXyz[i+2]);
			t.append(" }");
			if (i < mDataXyz.size() - 1)
				t.append(", ");
		}
	}

	else if (hasDioDataType(getActualDioDataTypes(), DioDataType::Array_Rgb) == true) {
		for (size_t i=0; i<mDataXyz.size(); i+=3) {
			t.append("{ ");
			t.append2(mDataXyz[i]);
			t.append(", ");
			t.append2(mDataXyz[i+1]);
			t.append(", ");
			t.append2(mDataXyz[i+2]);
			t.append(" }");
			if (i < mDataXyz.size() - 1)
				t.append(", ");
		}
		for (size_t i=0; i<mDataRgb.size(); ++i) {
			t.append("{ ");
			t.append2(mDataRgb[i]);
			t.append(", ");
			t.append2(mDataRgb[i+1]);
			t.append(", ");
			t.append2(mDataRgb[i+2]);
			t.append(" }");
			if (i < mDataRgb.size() - 1)
				t.append(", ");
		}
	}
	t.append(" ]");
	return t;
}


} // End of namespace

