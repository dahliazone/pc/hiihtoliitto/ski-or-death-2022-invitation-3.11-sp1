#include <Verso/DepthIo/DioFile.hpp>
#include <Verso/DepthIo/DioVariableType.hpp>
#include <Verso/System/File.hpp>
#include <Verso/System/Logger.hpp>
#include <snappy/snappy.h>

namespace Verso {


/////////////////////////////////////////////////////////////////////////
// Load .dio
/////////////////////////////////////////////////////////////////////////


bool loadDioFile_Header(std::ifstream& file, FrameAnimation& anim, DioHeader& header)
{
	const int headerLen = sizeof(uint8_t)*10 + sizeof(uint16_t) + sizeof(uint32_t)*3 + sizeof(std::uint64_t)*4 + sizeof(uint8_t);
	char headerBytes[headerLen];
	file.read(static_cast<char*>(&headerBytes), headerLen);
/*
	file.write(static_cast<char*>(&header.identification), sizeof(uint8_t) * 8);
	file.write(static_cast<char*>(&header.version), sizeof(uint8_t));
	file.write(static_cast<char*>(&header.fileLength), sizeof(std::uint64_t));
	file.write(static_cast<char*>(&header.width), sizeof(uint32_t));
	file.write(static_cast<char*>(&header.height), sizeof(uint32_t));
	file.write(static_cast<char*>(&header.flags), sizeof(uint32_t));
	file.write(static_cast<char*>(&header.packFormat), sizeof(uint8_t));
	file.write(static_cast<char*>(&header.timeCreated), sizeof(std::uint64_t));
	file.write(static_cast<char*>(&header.timeModified), sizeof(std::uint64_t));
	file.write(static_cast<char*>(&header.offsetToFirstFrameHeader), sizeof(uint16_t));
	file.write(static_cast<char*>(&header.footerPtr), sizeof(std::uint64_t));
*/

	size_t index = 0;
	memcpy(header.identification, headerBytes, sizeof(uint8_t)*sizeof(header.identification)); index += index += sizeof(uint8_t)*sizeof(header.identification);
	header.version = headerBytes[index]; index += sizeof(uint8_t);
	header.fileLength = File::readUint64(headerBytes, index); //*(static_cast<std::uint64_t*>(&headerBytes[index]));
		index += sizeof(std::uint64_t);
	header.width = File::readUint32(headerBytes, index); //*(static_cast<uint32_t*>(&(headerBytes[index])));
		index += sizeof(uint32_t);
	header.height = File::readUint32(headerBytes, index); //*(static_cast<uint32_t*>(&(headerBytes[index])));
		index += sizeof(uint32_t);
	header.flags = File::readUint32(headerBytes, index); //*(static_cast<uint32_t*>(&headerBytes[index]));
		index += sizeof(uint32_t);
	header.packFormat = File::readUint8(headerBytes, index); //headerBytes[index];
		index += sizeof(uint8_t);
	header.timeCreated = File::readUint64(headerBytes, index); //*(static_cast<std::uint64_t*>(&headerBytes[index]));
		index += sizeof(std::uint64_t);
	header.timeModified = File::readUint64(headerBytes, index); //*(static_cast<std::uint64_t*>(&headerBytes[index]));
		index += sizeof(std::uint64_t);
	header.offsetToFirstFrameHeader = File::readUint16(headerBytes, index); //*(static_cast<uint16_t*>(&headerBytes[index]));
		index += sizeof(uint16_t);
	header.footerPtr = File::readUint64(headerBytes, index); //*(static_cast<std::uint64_t*>(&headerBytes[index]));
		index += sizeof(std::uint64_t);
	size_t crc32 = File::readUint32(headerBytes, index); //*(static_cast<uint32_t*>(&headerBytes[index]));
		index += sizeof(uint32_t);

	VERSO_LOG_INFO("verso-depthio", "__Header_________________________________");
	VERSO_LOG_INFO_VARIABLE("verso-depthio", "version", static_cast<uint32_t>(header.version));
	VERSO_LOG_INFO_VARIABLE("verso-depthio", "fileLength", header.fileLength<<" bytes");
	VERSO_LOG_INFO_VARIABLE("verso-depthio", "width", header.width);
	VERSO_LOG_INFO_VARIABLE("verso-depthio", "height", header.height);
	VERSO_LOG_INFO_VARIABLE("verso-depthio", "flags", dioHeaderFlagBitsToString(static_cast<DioHeaderFlagBits>(header.flags)));
	VERSO_LOG_INFO_VARIABLE("verso-depthio", "packFormat", dioPackFormatToString(static_cast<DioPackFormat>(header.packFormat)));
	VERSO_LOG_INFO_VARIABLE("verso-depthio", "timeCreated", header.timeCreated);
	VERSO_LOG_INFO_VARIABLE("verso-depthio", "timeModified", header.timeModified);
	VERSO_LOG_INFO_VARIABLE("verso-depthio", "offsetToFirstFrameHeader", header.offsetToFirstFrameHeader<<" bytes");
	VERSO_LOG_INFO_VARIABLE("verso-depthio", "footerPtr", header.footerPtr<<" bytes");
	VERSO_LOG_INFO_VARIABLE("verso-depthio", "CRC32", crc32);

	std::ostringstream oss;
	for (size_t i=0; i<headerLen; ++i) {
		uint8_t v = headerBytes[i];
		oss << static_cast<uint32_t>(v)<<"("<<v<<")"<<" ";
	}
	VERSO_LOG_INFO_VARIABLE("verso-depthio", "headerBytes", oss.str());

	return true;
}


bool loadDioFile_Footer(std::ifstream& file, FrameAnimation& anim, DioFooter& footer)
{
	return true;
}


bool loadDioFile_Chunks(std::ifstream& file, FrameAnimation& anim)
{
	return true;
}


bool loadDioFile(FrameAnimation& anim, const UString& fileName)
{
	VERSO_LOG_INFO("verso-depthio", "Loading DepthIO from \""<<fileName<<"\""<<"...");

	std::ifstream file(fileName.c_str(), std::ios::in | std::ios::binary);
	if (!file)
		return false;

	// Load header
	DioHeader header;
	loadDioFile_Header(file, anim, header);

	// Loader footer
	DioFooter footer;
	loadDioFile_Footer(file, anim, footer);

	// Load chunks
	loadDioFile_Chunks(file, anim);

	return true;
}


/////////////////////////////////////////////////////////////////////////
// Save .dio
/////////////////////////////////////////////////////////////////////////


bool saveDioFile_Header(std::ofstream& file, uint8_t version, uint32_t width, uint32_t height, DioHeaderFlagBits flags, DioPackFormat packFormat, size_t startOfFooterPtr, size_t lengthOfFile)
{
	if (version != 0)
		return false;

	DioHeader header;
	header.identification[0] = '\211';
	header.identification[1] = 'D';
	header.identification[2] = 'i';
	header.identification[3] = 'o';
	header.identification[4] = '\r';
	header.identification[5] = '\n';
	header.identification[6] = '\032';
	header.identification[7] = '\n';
	header.version = version;
	header.fileLength = lengthOfFile;
	header.width = width;
	header.height = height;
	header.flags = flags;
	header.packFormat = static_cast<uint8_t>(packFormat);
	header.timeCreated = 0; // TODO: set this to system independent value
	header.timeModified = 0; // TODO: set this to system independent value
	header.offsetToFirstFrameHeader = sizeof(std::uint64_t);
	header.footerPtr = startOfFooterPtr;

	file.write(static_cast<char*>(&header.identification), sizeof(uint8_t) * 8);
	file.write(static_cast<char*>(&header.version), sizeof(uint8_t));
	file.write(static_cast<char*>(&header.fileLength), sizeof(std::uint64_t));
	file.write(static_cast<char*>(&header.width), sizeof(uint32_t));
	file.write(static_cast<char*>(&header.height), sizeof(uint32_t));
	file.write(static_cast<char*>(&header.flags), sizeof(uint32_t));
	file.write(static_cast<char*>(&header.packFormat), sizeof(uint8_t));
	file.write(static_cast<char*>(&header.timeCreated), sizeof(std::uint64_t));
	file.write(static_cast<char*>(&header.timeModified), sizeof(std::uint64_t));
	file.write(static_cast<char*>(&header.offsetToFirstFrameHeader), sizeof(uint16_t));
	file.write(static_cast<char*>(&header.footerPtr), sizeof(std::uint64_t));

	DioCrc32 headerCrc32 = 0; // TODO: calculate
	file.write(static_cast<char*>(&headerCrc32), sizeof(uint32_t));

	return true;
}


bool saveDioFile_ChunkHeader_Frame(std::ofstream& file, DioHeaderFlagBits flags, FrameData& frame)
{
	if (frame.checkIfDataIsValid() == false) {
		VERSO_LOG_ERR("verso-depthio", "Invalid data on frame "<<frame.getIndex()<<". Skipping...");
		VERSO_LOG_ERR_VARIABLE("verso-depthio", "error message", frame.getErrorMsg());
		return false;
	}

	DioChunkHeader_Frame frameHeader;
	frameHeader.chunkType = static_cast<uint8_t>(DioChunkType::FRAME);
	frameHeader.dataLength = 0; // Add later when data types are discovered
	frameHeader.offsetToData = 0; // Will be calculated later
	frameHeader.timestamp = frame.getTimestamp();
	frameHeader.pointCount = frame.pointsCount();
	frameHeader.dataTypeCount = dioDataTypeCount(frame.getActualDioDataTypes());

	// dioDataTypes
	size_t dioDataTypesAllocSize = frameHeader.dataTypeCount * 2 * sizeof(uint8_t);
	if (dioDataTypesAllocSize % 4 != 0)
		dioDataTypesAllocSize += 2 * sizeof(uint8_t);
	frameHeader.dioDataTypes = new uint8_t[dioDataTypesAllocSize];
	for (size_t i=0; i<dioDataTypesAllocSize; ++i)
		frameHeader.dioDataTypes[i] = 0;

	size_t baseIndex = 0;
	for (DioDataType type : getAllDioDataTypes()) {
		if (frame.getActualDioDataTypes() & (DioDataTypes)type) {
			frameHeader.dioDataTypes[baseIndex+0] = static_cast<uint8_t>(dioDataTypesDioVariableType(type));
			frameHeader.dioDataTypes[baseIndex+1] = type;
			baseIndex += 2;
		}
	}


	frameHeader.offsetToData = sizeof(uint32_t) * 2 + sizeof(uint8_t) * (1 + dioDataTypesAllocSize);

	if (flags | (DioHeaderFlagBits)DioHeaderFlagBit::HasViewport) { // TODO: this data should be read from anim!
		//frameHeader.viewpointTranslation[]
		//frameHeader.viewpointQuaternion[]
		//frameHeader.offsetToData += (3 + 4) * sizeof(float);
	}

	file.write(static_cast<char*>(&frameHeader.chunkType), sizeof(uint8_t));
	file.write(static_cast<char*>(&frameHeader.dataLength), sizeof(uint32_t));
	file.write(static_cast<char*>(&frameHeader.offsetToData), sizeof(uint16_t));
	file.write(static_cast<char*>(&frameHeader.timestamp), sizeof(uint32_t));
	file.write(static_cast<char*>(&frameHeader.pointCount), sizeof(uint32_t));
	file.write(static_cast<char*>(&frameHeader.dataTypeCount), sizeof(uint8_t));
	file.write(static_cast<char*>(frameHeader.dioDataTypes), dioDataTypesAllocSize * sizeof(uint8_t));
	if (flags | (DioHeaderFlagBits)DioHeaderFlagBit::HasViewport) { // TODO: this data should be read from anim!
		//file.write(static_cast<char*>(&frameHeader.viewpointTranslation), 3 * sizeof(float));
		//file.write(static_cast<char*>(&frameHeader.viewpointQuaternion), 4 * sizeof(float));
	}

	delete[] frameHeader.dioDataTypes;

	return true;
}


bool saveDioFile_ChunkData_Frame(std::ofstream& file, const FrameData& frame)
{
	DioDataTypes types = frame.getActualDioDataTypes();

	if (types & DioDataType::Array_Xyz) {
		VERSO_LOG_DEBUG_VARIABLE("verso-depthio", "DioDataType::Array_Xyz", (sizeof(float) * 3 * frame.pointsXyzCount()));
		file.write(static_cast<char*>(frame.getDataXyz()), sizeof(float) * 3 * frame.pointsXyzCount());
	}

	//if (types & DataType::Array_NormalXyz_Curvature)
	//	file.write(static_cast<char*>(frame.dataTodo.data()), sizeof(float) * 4 * frame.dataTodo.size());

	if (types & DioDataType::Array_Depth) {
		VERSO_LOG_DEBUG_VARIABLE("verso-depthio", "DioDataType::Array_Depth", (sizeof(uint16_t) * frame.pointsDepthCount()));
		file.write(static_cast<char*>(frame.getDataDepth()), sizeof(uint16_t) * frame.pointsDepthCount());
	}

	if (types & DioDataType::Array_Rgb) {
		VERSO_LOG_DEBUG_VARIABLE("verso-depthio", "DioDataType::Array_Rgb", (sizeof(uint8_t) * 3 * frame.pointsRgbCount()));
		file.write(static_cast<char*>(frame.getDataRgb()), sizeof(uint8_t) * 3 * frame.pointsRgbCount());
	}

	//if (types & DioDataType::Array_Intensity)
	//	file.write(static_cast<char*>(frame.dataTodo.data()), sizeof(uint8_t) * frame.dataTodo.size());

	//if (types & DioDataType::Array_Segmentation)
	//	file.write(static_cast<char*>(frame.dataTodo.data()), sizeof(uint8_t?) * frame.dataTodo.size());

	return true;
}


bool saveDioFile_Footer(std::ofstream& file, uint32_t frameCount, uint32_t lenghtMilliseconds, const std::vector<DioFooter_ChunkMetadata>& chunkMetadata)
{
	DioFooter footer;
	footer.frameCount = frameCount;
	footer.lenghtMilliseconds = lenghtMilliseconds;
	footer.chunkCount = chunkMetadata.size();

	file.write(static_cast<char*>(&footer.frameCount), sizeof(uint32_t));
	file.write(static_cast<char*>(&footer.lenghtMilliseconds), sizeof(uint32_t));
	file.write(static_cast<char*>(&footer.chunkCount), sizeof(uint32_t));

	for (size_t i=0; i<chunkMetadata.size(); ++i) {
		file.write(static_cast<char*>(&chunkMetadata[i].type), sizeof(uint8_t));
		file.write(static_cast<char*>(&chunkMetadata[i].offsetToNext), sizeof(uint16_t));
		file.write(static_cast<char*>(&chunkMetadata[i].chunkType), sizeof(uint8_t));
		file.write(static_cast<char*>(&chunkMetadata[i].chunkHeaderPtr), sizeof(std::uint64_t));
		file.write(static_cast<char*>(&chunkMetadata[i].dataLength), sizeof(uint32_t));
		file.write(static_cast<char*>(&chunkMetadata[i].timestamp), sizeof(uint32_t));
	}

	DioCrc32 footerCrc32 = 0; // TODO: calculate
	file.write(static_cast<char*>(&footerCrc32), sizeof(uint32_t));

	return true;
}


bool saveDioFile(FrameAnimation& anim, const UString& fileName, DioPackFormat pack)
{
	VERSO_LOG_INFO("verso-depthio", "Saving DepthIO-"<<dioPackFormatToString(pack)<<" to \""<<fileName<<"\""<<"...");

	if (anim.checkIfDataIsValid() == false) {
		VERSO_LOG_ERR("verso-depthio", "Given FrameAnimation in invalid state. Cannot save!");
	}

	std::ofstream file(fileName.c_str(), std::ios::out | std::ios::binary);
	if (!file)
		return false;

	uint8_t version = 0;

	anim.updateWidthIfNeeded();
	std::vector<DioFooter_ChunkMetadata> chunkMetadata;

	DioHeaderFlagBits flags = 0;
	//flags |= (FileHeaderFlagBits)FileHeaderFlagBit::HAS_VIEWPORT; // TODO: read from anim

	// Write header
	if (saveDioFile_Header(file, version, anim.width, anim.height, flags, pack, 0, 0) == false) {
		VERSO_LOG_ERR("verso-depthio", "Failed writing file header!");
		file.close();
		return false;
	}

	// Write frame chunks
	for (size_t i=0; i<anim.lengthInFrames(); ++i) {
		FrameData& frame = anim.getFrame(i);

		size_t chunkHeaderPtr = file.tellp();
		if (saveDioFile_ChunkHeader_Frame(file, flags, frame) == false) {
			VERSO_LOG_ERR("verso-depthio", "Failed writing frame "<<i<<" header!");
			file.close();
			return false;
		}
		size_t chunkDataPtr = file.tellp();
		if (saveDioFile_ChunkData_Frame(file, frame) == false) {
			VERSO_LOG_ERR("verso-depthio", "Failed writing frame "<<i<<" data!");
			file.close();
			return false;
		}
		size_t chunkEndPtr = file.tellp();

		DioCrc32 chunkCrc32 = 0; // TODO: calculate
		file.write(static_cast<char*>(&chunkCrc32), sizeof(uint32_t));

		DioFooter_ChunkMetadata cm;
		cm.type = 0;
		cm.offsetToNext = sizeof(uint8_t) + sizeof(std::uint64_t) + 2 * sizeof(uint32_t);
		cm.chunkType = static_cast<uint8_t>(DioChunkType::FRAME);
		cm.chunkHeaderPtr = chunkHeaderPtr;
		cm.dataLength = chunkEndPtr - chunkDataPtr;
		cm.timestamp = anim.getFrame(i).getTimestamp();
		chunkMetadata.push_back(cm);
	}

	size_t startOfFooterPtr = file.tellp();

	// Write footer
	if (saveDioFile_Footer(file, anim.lengthInFrames(), anim.lengthInMillisec(), chunkMetadata) == false) {
		VERSO_LOG_ERR("verso-depthio", "Failed writing file footer!");
		file.close();
		return false;
	}

	size_t lengthOfFile = file.tellp();
	// Write header again (with up-to-date startOfFooterPtr, lengthOfFile)
	file.seekp(0);
	if (saveDioFile_Header(file, version, anim.width, anim.height, flags, pack, startOfFooterPtr, lengthOfFile) == false) {
		VERSO_LOG_ERR("verso-depthio", "Failed writing file header!");
		file.close();
		return false;
	}

	file.close();
	return true;
}



/////////////////////////////////////////////////////////////////////////
// DioFile implementation
/////////////////////////////////////////////////////////////////////////

const uint8_t DioFile::mVersion = 0;


DioFile::DioFile() :
	mFileName(),
	mPackFormat(),
	mFlags(),
	mFile(),
	mWidth(0),
	mHeight(0),
	mChunkMetadata(),
	mLengthInFrames(0),
	mLengthInMillisec(0),
	mLastTimestamp(0),
	mErrorMsg()
{
}


DioFile::~DioFile()
{
}


bool DioFile::startToSave(const UString& fileName, DioPackFormat packFormat, uint32_t width, uint32_t height)
{
	VERSO_LOG_INFO("verso-depthio", "Stream saving DepthIO-"<<dioPackFormatToString(packFormat)<<" to \""<<fileName<<"\""<<"...");

	mFile.open(fileName.c_str(), std::ios::out | std::ios::binary);
	if (!mFile.is_open()) {
		mErrorMsg = "Could not open file \""+fileName+"\"";
		return false;
	}

	//anim.UpdateWidthIfNeeded();

	mFlags = 0;
	//mFlags |= (FileHeaderFlagBits)FileHeaderFlagBit::HAS_VIEWPORT; // TODO: read from anim

	// Write header
	if (saveDioFile_Header(mFile, mVersion, width, height, mFlags, packFormat, 0, 0) == false) {
		mErrorMsg = "Failed writing file header!";
		mFile.close();
		return false;
	}

	mFileName = fileName;
	mPackFormat = packFormat;
	mWidth = width;
	mHeight = height;

	return true;
}


uint32_t DioFile::getLastTimestamp() const
{
	return mLastTimestamp;
}


bool DioFile::isNewFrame(uint32_t timestamp) const
{
	if (timestamp != mLastTimestamp) // TODO: should be (timestamp > mLastTimestamp) when timestamps work well!
		return true;
	else
		return false;
}


bool DioFile::saveFrame(FrameData& frame)
{
	if (!mFile.is_open()) {
		mErrorMsg = "DioFile::SaveFrame(): File is not yet or anymore open. Call StartToSave()!";
		return false;
	}

	VERSO_LOG_DEBUG("verso-depthio", "Saving "<<frame.pointsXyzCount()<<" xyz, "<<frame.pointsDepthCount()<<" depth, "<<frame.pointsRgbCount()<<" rgb points...");

	// Write frame chunks
	size_t chunkHeaderPtr = mFile.tellp();
	if (saveDioFile_ChunkHeader_Frame(mFile, mFlags, frame) == false) {
		mErrorMsg = "Failed writing frame ";
		mErrorMsg.append2(frame.getIndex());
		mErrorMsg.append2(" header!");
		return false;
	}
	size_t chunkDataPtr = mFile.tellp();
	if (saveDioFile_ChunkData_Frame(mFile, frame) == false) {
		mErrorMsg = "Failed writing frame ";
		mErrorMsg.append2(frame.getIndex());
		mErrorMsg.append2(" data!");
		return false;
	}
	size_t chunkEndPtr = mFile.tellp();

	DioCrc32 chunkCrc32 = 0; // TODO: calculate
	mFile.write(static_cast<char*>(&chunkCrc32), sizeof(uint32_t));

	DioFooter_ChunkMetadata cm;
	cm.type = 0;
	cm.offsetToNext = sizeof(uint8_t) + sizeof(std::uint64_t) + 2 * sizeof(uint32_t);
	cm.chunkType = static_cast<uint8_t>(DioChunkType::FRAME);
	cm.chunkHeaderPtr = chunkHeaderPtr;
	cm.dataLength = chunkEndPtr - chunkDataPtr;
	cm.timestamp = frame.getTimestamp();
	mChunkMetadata.push_back(cm);

	mLastTimestamp = cm.timestamp;

	return true;
}


bool DioFile::finishSave()
{
	if (!mFile.is_open()) {
		mErrorMsg = "DioFile::FinishSave(): File is not yet or anymore open. Call StartToSave()!";
		return false;
	}

	size_t startOfFooterPtr = mFile.tellp();
	bool returnValue = true;

	// Write footer
	if (saveDioFile_Footer(mFile, mLengthInFrames, mLengthInMillisec, mChunkMetadata) == false) {
		mErrorMsg = "Failed writing file footer!";
		returnValue = false;
	}

	size_t lengthOfFile = mFile.tellp();
	// Write header again (with up-to-date startOfFooterPtr, lengthOfFile)
	mFile.seekp(0);
	if (saveDioFile_Header(mFile, mVersion, mWidth, mHeight, mFlags, mPackFormat, startOfFooterPtr, lengthOfFile) == false) {
		mErrorMsg = "Failed writing file header!";
		returnValue = false;
	}

	mFile.close();

	return returnValue;
}


} // End of namespace

