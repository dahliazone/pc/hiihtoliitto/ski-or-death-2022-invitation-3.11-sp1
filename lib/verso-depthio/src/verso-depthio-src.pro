# == Shared ===============================================================
! include( ../verso-depthio-common.pri ) {
	error("Couldn't find the verso-depthio-common.pri file!")
}

TEMPLATE = lib
win* {
	TEMPLATE = vclib
}

DESTDIR = "$${BUILD_DIR}"
TARGET = verso-depthio


DEPENDPATH += \
	$$HEADERPATH/Verso/DepthIo \
	$$HEADERPATH/Verso \
	Verso/DepthIo \
	Verso/


HEADERS += \
	$$HEADERPATH/Verso/DepthIo/DioChunkType.hpp \
	$$HEADERPATH/Verso/DepthIo/DioDataType.hpp \
	$$HEADERPATH/Verso/DepthIo/DioFile.hpp \
	$$HEADERPATH/Verso/DepthIo/DioHeaderFlagBit.hpp \
	$$HEADERPATH/Verso/DepthIo/DioPackFormat.hpp \
	$$HEADERPATH/Verso/DepthIo/DioVariableType.hpp \
	$$HEADERPATH/Verso/DepthIo/FrameAnimation.hpp \
	$$HEADERPATH/Verso/DepthIo/FrameData.hpp \
	$$HEADERPATH/Verso/DepthIo/KinectPlaintextFile.hpp \
	$$HEADERPATH/Verso/DepthIo/TiltStateRawKinect.hpp \
	$$HEADERPATH/Verso/DepthIo.hpp


SOURCES += \
	Verso/DepthIo/DioFile.cpp \
	Verso/DepthIo/FrameData.cpp \
	Verso/DepthIo/KinectPlaintextFile.cpp \
	Verso/DepthIo/TiltStateRawKinect.cpp


# == Linux ================================================================
linux-g++-32 {
}

linux-g++ {
}

linux-* {
}


macx {
	# Verso-base
	LIBS += -L"$${LIBPATH}/verso-base/builds/$${BUILD_SUBDIR}" -lverso-base
	#CONFIG -= x86_64
	#CONFIG += x86
	#CONFIG -= app_bundle
}


# == OS X =================================================================
macx {
}


# == Windows ==============================================================
win* {
	QMAKE_POST_LINK += @ECHO "Post-link process..."
	CONFIG += staticlib
}

win*:CONFIG(debug, debug|release) {
	# Copy DLLs
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\assimp-vc120-mtd.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${OUT_PWD}\..\..\BASS\windows\bass.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\glew32d.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\libfreetype-6.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\libjpeg-9.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\libpng16-16.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\SDL2.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\SDL2_image.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\SDL2_ttf.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\zlib1.dll\" \"$${DESTDIR}\"
}

win*:CONFIG(release, debug|release) {
	# Copy DLLs
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\assimp-vc120-mt.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${OUT_PWD}\..\..\BASS\windows\bass.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\glew32.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\libfreetype-6.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\libjpeg-9.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\libpng16-16.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\SDL2.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\SDL2_image.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\SDL2_ttf.dll\" \"$${DESTDIR}\"
	QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\zlib1.dll\" \"$${DESTDIR}\"
}

win* {
	# Copy data directory
	QMAKE_POST_LINK += && XCOPY /s /q /y /i \"$${PWD}\..\data\" \"$${DESTDIR}\data\"

	# Remove temporaries
	QMAKE_POST_LINK += && RMDIR /S /Q \"$${PWD}\..\debug\" \"$${PWD}\..\release\"
}

