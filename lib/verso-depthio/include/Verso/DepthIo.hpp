#ifndef VERSO_DEPTHIO_MAININCLUDES_DEPTHIO_HPP
#define VERSO_DEPTHIO_MAININCLUDES_DEPTHIO_HPP


#include <Verso/DepthIo/DioChunkType.hpp>
#include <Verso/DepthIo/DioDataType.hpp>
#include <Verso/DepthIo/DioFile.hpp>
#include <Verso/DepthIo/DioHeaderFlagBit.hpp>
#include <Verso/DepthIo/DioPackFormat.hpp>
#include <Verso/DepthIo/DioVariableType.hpp>
#include <Verso/DepthIo/FrameAnimation.hpp>
#include <Verso/DepthIo/FrameData.hpp>
#include <Verso/DepthIo/KinectPlaintextFile.hpp>
#include <Verso/DepthIo/TiltStateRawKinect.hpp>


#endif // End header guard

