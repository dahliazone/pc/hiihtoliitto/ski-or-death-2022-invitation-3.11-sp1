#ifndef VERSO_DEPTHIO_VERSO_DEPTHIO_COMMON_HPP
#define VERSO_DEPTHIO_VERSO_DEPTHIO_COMMON_HPP


#if defined(WIN32) || defined _WIN32 || defined __CYGWIN__
#	if defined(VERSO_DEPTHIO_BUILD_DYNAMIC)
#		ifdef __GNUC__
#			define VERSO_DEPTHIO_API __attribute__ ((dllexport))
#		else
#			define VERSO_DEPTHIO_API __declspec(dllexport) // Note: actually gcc seems to also supports this syntax.
#			define VERSO_DEPTHIO_EXPIMP
#		endif
#	else
#		ifdef __GNUC__
#			define VERSO_DEPTHIO_API __attribute__ ((dllimport))
#		else
#			define VERSO_DEPTHIO_API __declspec(dllimport) // Note: actually gcc seems to also supports this syntax.
#			define VERSO_DEPTHIO_EXPIMP extern
#		endif
#	endif
#	define VERSO_DEPTHIO_PRIVATE
#else
#	if __GNUC__ >= 4
#		define VERSO_DEPTHIO_API __attribute__ ((visibility ("default")))
#		define VERSO_DEPTHIO_PRIVATE  __attribute__ ((visibility ("hidden")))
#	else
#		define VERSO_DEPTHIO_API
#		define VERSO_DEPTHIO_PRIVATE
#	endif
#endif


#endif // End header guard

