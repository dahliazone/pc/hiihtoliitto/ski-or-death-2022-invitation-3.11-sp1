#ifndef VERSO_DEPTHIO_DEPTHIOPLAINTEXT_HPP
#define VERSO_DEPTHIO_DEPTHIOPLAINTEXT_HPP

#include <Verso/DepthIo/FrameAnimation.hpp>

namespace Verso {


VERSO_DEPTHIO_API bool loadKinectPlaintextFile(FrameAnimation& anim, const UString& fileName);
VERSO_DEPTHIO_API bool saveKinectPlaintextFile(const FrameAnimation& anim, const UString& fileName);


} // End of namespace

#endif // End header guard

