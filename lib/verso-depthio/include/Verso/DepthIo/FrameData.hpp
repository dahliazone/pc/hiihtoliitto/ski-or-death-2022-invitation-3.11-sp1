#ifndef VERSO_DEPTHIO_FRAMEDATA_HPP
#define VERSO_DEPTHIO_FRAMEDATA_HPP

#include <Verso/DepthIo/DioDataType.hpp>
#include <Verso/DepthIo/TiltStateRawKinect.hpp>
#include <Verso/System/UString.hpp>
#include <vector>

namespace Verso {


struct VERSO_DEPTHIO_API FrameData
{
public:
	FrameData(uint32_t index, DioDataTypes requiredDioDataTypes = 0, uint32_t timestamp = 0);
	~FrameData();

	void addDataXyz(float x, float y, float z);
	void addDataDepth(uint16_t depth);
	void addDataRgb(uint8_t r, uint8_t g, uint8_t b);

	// Uses the given pointer. Will not free or modify it. Call ClearExternalDataXyz() before free'ing the pointer.
	void setExternalDataXyz(float* extPtrDataXyz, size_t pointCount);
	void clearDataXyz();

	// Uses the given pointer. Will not free or modify it. Call ClearExternalDataDepth() before free'ing the pointer.
	void setExternalDataDepth(uint16_t* extPtrDataDepth, size_t pointCount);
	void clearDataDepth();

	// Uses the given pointer. Will not free or modify it. Call ClearExternalDataRgb() before free'ing the pointer.
	// When you set external data buffer then add
	void setExternalDataRgb(uint8_t* extPtrDataRgb, size_t pointCount);
	void clearDataRgb();

	void setRawKinectTiltState(int16_t accelerometerX, int16_t accelerometerY, int16_t accelerometerZ);

	size_t pointsXyzCount() const;
	size_t pointsDepthCount() const;
	size_t pointsRgbCount() const;
	size_t pointsCount() const;

	const float* getDataXyz() const;
	const uint16_t* getDataDepth() const;
	const uint8_t* getDataRgb() const;

	bool checkIfDataIsValid();

	bool equals(const FrameData& frame) const;

	UString toString() const;
	UString toStringFull() const;

	uint32_t getIndex() const;
	void setIndex(uint32_t index);
	DioDataTypes getRequiredDioDataTypes() const;
	void setRequiredDioDataTypes(DioDataTypes requiredDioDataTypes);
	DioDataTypes getActualDioDataTypes() const;
	uint32_t getTimestamp() const;
	void setTimestamp(uint32_t timestamp);
	double getTimestampSec() const;

	const TiltStateRawKinect& getRawKinectTiltState() const;
	void setRawKinectTiltState(const TiltStateRawKinect& tiltState);

	UString getErrorMsg() const;
	void clearErrorMsg();

private:
	uint32_t mIndex;
	DioDataTypes mRequiredDioDataTypes;
	DioDataTypes mActualDioDataTypes;
	uint32_t mTimestamp;
	double mTimestampSec;
	TiltStateRawKinect mRawKinectTiltState;

	std::vector<float> mDataXyz;
	std::vector<uint16_t> mDataDepth;
	std::vector<uint8_t> mDataRgb;

	size_t mExtPtrForDataXyzPointCount; // note that extPtrDataXyz size is 3 * extPtrForDataXyzPointCount.
	float* mExtPtrDataXyz;
	size_t mExtPtrForDataDepthPointCount;
	uint16_t* mExtPtrDataDepth;
	size_t mExtPtrForDataRgbPointCount; // note that extPtrDataRgb size is 3 * extPtrForDataRgbPointCount.
	uint8_t* mExtPtrDataRgb;

	UString mErrorMsg;
};



inline uint32_t FrameData::getIndex() const
{
	return mIndex;
}

inline void FrameData::setIndex(uint32_t index)
{
	mIndex = index;
}

inline DioDataTypes FrameData::getRequiredDioDataTypes() const
{
	return mRequiredDioDataTypes;
}

inline void FrameData::setRequiredDioDataTypes(DioDataTypes requiredDioDataTypes)
{
	mRequiredDioDataTypes = requiredDioDataTypes;
}


inline DioDataTypes FrameData::getActualDioDataTypes() const
{
	return mActualDioDataTypes;
}


inline uint32_t FrameData::getTimestamp() const
{
	return mTimestamp;
}

inline void FrameData::setTimestamp(uint32_t timestamp)
{
	mTimestamp = timestamp;
	mTimestampSec = static_cast<double>(timestamp) / 1000.0f;
}


inline double FrameData::getTimestampSec() const
{
	return mTimestampSec;
}


inline const TiltStateRawKinect& FrameData::getRawKinectTiltState() const
{
	return mRawKinectTiltState;
}

inline void FrameData::setRawKinectTiltState(const TiltStateRawKinect& tiltState)
{
	mRawKinectTiltState = tiltState;
}


inline UString FrameData::getErrorMsg() const
{
	return mErrorMsg;
}

inline void FrameData::clearErrorMsg()
{
	mErrorMsg = "";
}


} // End of namespace

#endif // End header guard

