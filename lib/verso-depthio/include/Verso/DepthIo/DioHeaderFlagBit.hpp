
#ifndef VERSO_DEPTHIO_DIOHEADERFLAGBIT_HPP
#define VERSO_DEPTHIO_DIOHEADERFLAGBIT_HPP

#include <Verso/System/UString.hpp>
#include <vector>
#include <cstdint>

namespace Verso {


enum class DioHeaderFlagBit : std::uint32_t
{
	HasViewport = 1
	// Whether there's viewport translation and orientation available every frame.
	// Otherwise default values are for translation vector (0 0 0) and rotation quaternion (1 0 0 0).
};

typedef std::uint32_t DioHeaderFlagBits;


inline UString dioHeaderFlagBitToString(DioHeaderFlagBit dioHeaderFlagBit)
{
	if (dioHeaderFlagBit == DioHeaderFlagBit::HasViewport) {
		return "HasViewport";
	}
	else if (dioHeaderFlagBit == (DioHeaderFlagBit)0) {
		return "None";
	}
	else {
		return "UNKNOWN";
	}
}


inline std::vector<DioHeaderFlagBit> getAllDioHeaderFlagBits()
{
	std::vector<DioHeaderFlagBit> dioHeaderFlagBits;
	dioHeaderFlagBits.push_back(DioHeaderFlagBit::HasViewport);
	return dioHeaderFlagBits;
}


inline UString dioHeaderFlagBitsToString(DioHeaderFlagBits flags)
{
	UString str = "[ ";
	size_t count = 0;

	std::vector<DioHeaderFlagBit> bits = getAllDioHeaderFlagBits();
	for (auto it = bits.begin(); it != bits.end(); ++it) {
		if ((flags & (DioHeaderFlagBits)(*it)) != 0) {
			if (count != 0) {
				str += ", ";
			}
			str += dioHeaderFlagBitToString(*it);
			++count;
		}
	}

	if (count == 0) {
		str += "NONE";
	}

	str += " ]";
	return str;
}


inline std::ostream& operator << (std::ostream& ost, const DioHeaderFlagBit& dioHeaderFlagBit)
{
	return ost << dioHeaderFlagBitToString(dioHeaderFlagBit);
}



} // End of namespace

#endif // End header guard

