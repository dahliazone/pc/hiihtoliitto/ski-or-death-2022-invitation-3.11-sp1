#ifndef VERSO_DEPTHIO_DIOVARIABLETYPE_HPP
#define VERSO_DEPTHIO_DIOVARIABLETYPE_HPP

#include <Verso/System/UString.hpp>
#include <cstdint>

namespace Verso {


enum class DioVariableType : std::uint8_t
{
	Uint8 = 0,
	Uint16 = 1,
	Uint32 = 2,
	Sint8 = 3,
	Sint16 = 4,
	Sint32 = 5,
	Float32 = 7,
	Doudble64 = 8,
	Unset = 255
};


inline UString dioVariableTypeToString(DioVariableType dioVariableType)
{
	if (dioVariableType == DioVariableType::Uint8) {
		return "Uint8";
	}
	else if (dioVariableType == DioVariableType::Uint16) {
		return "Uint16";
	}
	else if (dioVariableType == DioVariableType::Uint32) {
		return "Uint32";
	}
	else if (dioVariableType == DioVariableType::Sint8) {
		return "Sint8";
	}
	else if (dioVariableType == DioVariableType::Sint16) {
		return "Sint16";
	}
	else if (dioVariableType == DioVariableType::Sint32) {
		return "Sint32";
	}
	else if (dioVariableType == DioVariableType::Float32) {
		return "Float32";
	}
	else if (dioVariableType == DioVariableType::Doudble64) {
		return "Doudble64";
	}
	else if (dioVariableType == DioVariableType::Unset) {
		return "Unset";
	}
	else {
		return "UNKNOWN";
	}
}


inline std::vector<DioVariableType> getAllDioVariableTypes()
{
	std::vector<DioVariableType> dioVariableTypes;
	dioVariableTypes.push_back(DioVariableType::Uint8);
	dioVariableTypes.push_back(DioVariableType::Uint16);
	dioVariableTypes.push_back(DioVariableType::Uint32);
	dioVariableTypes.push_back(DioVariableType::Sint8);
	dioVariableTypes.push_back(DioVariableType::Sint16);
	dioVariableTypes.push_back(DioVariableType::Sint32);
	dioVariableTypes.push_back(DioVariableType::Float32);
	dioVariableTypes.push_back(DioVariableType::Doudble64);
	return dioVariableTypes;
}


inline std::ostream& operator << (std::ostream& ost, const DioVariableType& dioVariableType)
{
	return ost << dioVariableTypeToString(dioVariableType);
}


} // End of namespace

#endif // End header guard

