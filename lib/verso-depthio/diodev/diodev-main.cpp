
#include <Verso/DepthIo/KinectPlaintextFile.hpp>
#include <Verso/DepthIo/DioFile.hpp>
#include "snappy/snappy.h"

#include <ctime>
#include <fstream>
#include <iomanip>


bool testLoad(const std::string& fileName)
{
   char buffer[100];

   std::ifstream file(fileName.c_str(), std::ios::in | std::ios::binary);
   if (!file.read(buffer, 100)) {
	   // An error occurred!
	   // myFile.gcount() returns the number of bytes read.
	   // calling myFile.clear() will reset the stream state
	   // so it is usable again.
	   file.close();
	   return false;
   }
   VERSO_LOG_INFO("depthio/diodev", buffer);

   if (!file.read(buffer, 100)) {
	   // Same effect as above
	   file.close();
	   return false;
   }
   VERSO_LOG_INFO("depthio/diodev", buffer);

   file.close();
   return true;
}


void testSnappy()
{
	std::string input = "aaaaaaaqqqqqqqqqqqqqqqaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasd";
	std::string compressed;
	std::string output;

	VERSO_LOG_INFO("depthio/diodev", "input: "<<input);

	snappy::Compress(input.data(), input.size(), &compressed);

	VERSO_LOG_INFO("depthio/diodev", "compressed: " << compressed);

	snappy::Uncompress(compressed.data(), compressed.size(), &output);

	VERSO_LOG_INFO("depthio/diodev", "output: " << output);
}


int main()
{
	clock_t timeStart = clock();

	Verso::FrameAnimation anim(0, 0);

	Verso::UString dataPath = "data/";
	Verso::UString plaintextExt = ".txt";
	Verso::UString binaryExt = ".dio";

	//Verso::UString source = "test.280-5k.kinect";
	//Verso::UString source = "tworows";
	Verso::UString source = "onerowshort";

	if (Verso::loadKinectPlaintextFile(anim, dataPath+source+plaintextExt) == false)
		VERSO_LOG_ERR("verso-depthio", "File loading failed!");

	double elapsedLoadPlaintext = (double)(clock() - timeStart) / CLOCKS_PER_SEC;
	anim.print();
	VERSO_LOG_INFO("depthio/diodev", "Time taken to load: "<<std::fixed<<std::setw(11)<<std::setprecision(6)<<elapsedLoadPlaintext<<" sec");
	timeStart = clock();

	if (!Verso::saveDioFile(anim, dataPath+source+binaryExt, Verso::DioPackFormat::Unpacked))
		VERSO_LOG_ERR("verso-depthio", "File saving failed!");

	double elapsedDioSave = (double)(clock() - timeStart) / CLOCKS_PER_SEC;
	VERSO_LOG_INFO("depthio/diodev", "Time taken to save: "<<std::fixed<<std::setw(11)<<std::setprecision(6)<<elapsedDioSave<<" sec");


	timeStart = clock();
	if (!loadDioFile(anim, dataPath+source+binaryExt))
		VERSO_LOG_ERR("verso-depthio", ".dio file loading failed!");

	double elapsedDioLoad = (double)(clock() - timeStart) / CLOCKS_PER_SEC;
	VERSO_LOG_INFO("depthio/diodev", "Time taken to load .dio: "<<std::fixed<<std::setw(11)<<std::setprecision(6)<<elapsedDioLoad<<" sec");

	//testLoad("data/onerow.txt");
	//testSnappy();

	VERSO_LOG_INFO("depthio/diodev", "__________________________________________");
	VERSO_LOG_INFO("depthio/diodev", " SUMMARY");
	VERSO_LOG_INFO("depthio/diodev", "__________________________________________");
	VERSO_LOG_INFO_VARIABLE("depthio/diodev", "frames", std::fixed<<std::setw(28)<<anim.lengthInFrames());
	VERSO_LOG_INFO_VARIABLE("depthio/diodev", "avg. points per frame", std::fixed<<std::setw(13)<<std::setprecision(2)<<anim.avgPointsPerFrame());
	VERSO_LOG_INFO_VARIABLE("depthio/diodev", "points total", std::fixed<<std::setw(22)<<std::setprecision(6)<<anim.pointsInTotal());
	VERSO_LOG_INFO_VARIABLE("depthio/diodev", "anim length", std::fixed<<std::setw(23)<<std::setprecision(6)<<anim.lengthInSec()<<" sec");
	VERSO_LOG_INFO_VARIABLE("depthio/diodev", "load .txt", std::fixed<<std::setw(30)<<std::setprecision(6)<<elapsedLoadPlaintext<<" sec");
	VERSO_LOG_INFO_VARIABLE("depthio/diodev", "save .dio", std::fixed<<std::setw(30)<<std::setprecision(6)<<elapsedDioSave<<" sec");
	VERSO_LOG_INFO_VARIABLE("depthio/diodev", "load .dio", std::fixed<<std::setw(30)<<std::setprecision(6)<<elapsedDioLoad<<" sec");
	VERSO_LOG_INFO("depthio/diodev", "__________________________________________");

	return EXIT_SUCCESS;
}
