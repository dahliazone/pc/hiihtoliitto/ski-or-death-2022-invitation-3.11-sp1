@echo off
echo.
echo ================================ verso-depthio ================================
echo ======== Cleaning up all temporary for create by QMAKE for Visual C++ =========

echo.
echo === Directory .\ ===
del /q /f Makefile
del /q /f verso-depthio.pro.user
del /q /f /A:H *.opensdf
del /q /f *.sdf 
del /q /f *.sln
del /q /f /A:H *.suo
rmdir /s /q builds


echo.
echo === Directory .\examples ===
del /q /f examples\*.vcxproj
del /q /f examples\*.vcxproj.filters
del /q /f examples\*.vcxproj.user
rmdir /s /q examples\builds
rmdir /s /q examples\debug
rmdir /s /q examples\release
rmdir /s /q examples\Win32


echo.
echo === Directory .\src ===
del /q /f src\*.vcxproj
del /q /f src\*.vcxproj.filters
del /q /f src\*.vcxproj.user
rmdir /s /q src\builds
rmdir /s /q src\debug
rmdir /s /q src\release
rmdir /s /q src\Win32

echo.
echo === Directory .\test ===
del /q /f test\*.vcxproj
del /q /f test\*.vcxproj.filters
del /q /f test\*.vcxproj.user
rmdir /s /q test\builds
rmdir /s /q test\debug
rmdir /s /q test\release
rmdir /s /q test\Win32


call msvc-fix-working-dir.cmd

echo.
echo ============================  verso-depthio done ==============================

