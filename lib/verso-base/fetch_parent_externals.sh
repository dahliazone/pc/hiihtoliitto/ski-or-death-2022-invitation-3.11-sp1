#!/bin/bash

echo "verso-base: Fetching externals from repositories..."
if [ ! -d ../utf8-cpp ]; then
	git clone https://gitlab.com/dahliazone/pc/extlib/utf8-cpp.git ../utf8-cpp
else
	pushd ../utf8-cpp
	git pull --rebase
	popd
fi

if [ ! -d ../SimpleJSON ]; then
	git clone https://gitlab.com/dahliazone/pc/extlib/SimpleJSON.git ../SimpleJSON
else
	pushd ../SimpleJSON
	git pull --rebase
	popd
fi

if [ ! -d ../nlohmann-json ]; then
	git clone https://gitlab.com/dahliazone/pc/extlib/nlohmann-json.git ../nlohmann-json
else
	pushd ../nlohmann-json
	git pull --rebase
	popd
fi

if [ ! -d ../stb ]; then
	git clone https://gitlab.com/dahliazone/pc/extlib/stb.git ../stb
else
	pushd ../stb
	git pull --rebase
	popd
fi

if [ ! -d ../googletest ]; then
	git clone https://gitlab.com/dahliazone/pc/extlib/googletest.git ../googletest
else
	pushd ../googletest
	git pull --rebase
	popd
fi

echo "verso-base: All done"

