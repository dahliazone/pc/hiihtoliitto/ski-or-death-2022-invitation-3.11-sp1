/**
 * @file Math__Quaternion_test.cpp
 * @brief Test for the class Quaternion.
 * @author Max Vilkki <codise@codise.org>
 *
 * TODO:
 * - \todo
 *
 * What is not tested(!):
 * - What:
 *   Why:
 */

#include <Verso/Math/Quaternion.hpp>
#include <gtest/gtest.h>

namespace {

using Verso::Quaternion;


/**
 * Test
 */
TEST(MathQuaternionTest, TestName123)
{
	Quaternion q1;
	EXPECT_EQ(q1, q1);
}

/*
inline void testQuaternion()
{
	cout << "Quaternion" << endl;
	Quaternion q1();
	cout << "    Quaternion q1() << "<<q1 << endl;
	cout << "    q1.toString() => "<<q1.toString() << endl;
	cout << "    q1.toStringDebug() => "<<q1.toStringDebug() << endl;

	Quaternion q2(1, 2, 3, 4);
	cout << "    Quaternion q2(1,2,3,4) << "<<q2 << endl;
	cout << "    q2.toString() => "<<q2.toString() << endl;
	cout << "    q2.toStringDebug() => "<<q2.toStringDebug() << endl << endl;

	Quaternion q3(10, 20, 30);
	cout << "    Quaternion q3(10, 20, 30) << "<<q3 << endl << endl;

	Quaternion q4(Vector3f(1, 2, 3), 45);
	cout << "    Quaternion q4(Vector3f(1, 2, 3), 45) << "<<q4 << endl;

	cout << "    Quaternion::Identity << "<<Quaternion::Identity << endl << endl;

	Matrix4x4f matrix;
	Quaternion q5(matrix);
	cout << "    Quaternion q4(Vector3f(1, 2, 3), 45) << "<<q4 << endl;

	Quaternion q2Copy(qt);

	void set(float x, float y, float z, float w)
	void set(Degree pitch, Degree yaw, Degree roll)
	void set(const Vector3f& axis, float angle)
	void set(const Matrix4x4f& matrix)
*/
			/*
	void setFromToRotation(const Vector3f& fromDirection, const Vector3f& toDirection)
	void setLookRotation(const Vector3f& forward, const Vector3f& upward = Vector3f::up())
	float getLength()
	float getLengthSquared()
	void normalize()
	Quaternion exp()
	Quaternion log()
	Quaternion multiplyByScalar(float right)
	Quaternion multiplyByQuaternion(const Quaternion& right)
	Quaternion divideByScalar(float right)
	void toEulerAngles(float& pitch, float& yaw, float& roll, bool homogenous=true)
	void toAngleAxis(Degree& outAngle, Vector3f& outAxis)
	Matrix4x4f toMatrix()
	float & operator [](size_t index)
	Quaternion& operator =(const Quaternion& original)
	Quaternion operator +(const Quaternion& right)
	Quaternion operator -(const Quaternion& right)
	Quaternion operator *(float right)
	Quaternion operator /(float right)
	Vector3f operator *(const Vector3f& right)
	Quaternion operator *(const Quaternion& right)

	static float Quaternion::dotProduct(const Quaternion& left, const Quaternion& right)
	static Degree Quaternion::angleBetween(const Quaternion& left, const Quaternion& right)
	static Degree Quaternion::angleBetweenFast(const Quaternion& left, const Quaternion& right)
	static Quaternion Quaternion::angleAxis(Degree angle, const Vector3f& axis)
	//static Quaternion Quaternion::fromToRotation(const Vector3f& fromDirection, const Vector3f& toDirection)
	static Quaternion Quaternion::conjugate(const Quaternion& original)
	static Quaternion Quaternion::inverse(const Quaternion& original)
	static Quaternion Quaternion::inverseFast(const Quaternion& original)
	static Quaternion Quaternion::lookRotation(const Vector3f& forward, const Vector3f& upward = Vector3f::up())
	//static Quaternion Quaternion::rotateTowards(const Quaternion& from, const Quaternion& to, Degree maxDegreesDelta)
	static Quaternion lerp(const Quaternion& from, const Quaternion& to, float delta)
	static Quaternion slerp(const Quaternion& from, const Quaternion& to, float delta)
	static Quaternion slerpNoInvert(const Quaternion& from, const Quaternion& to, float delta)
	static Quaternion intermediate(const Quaternion& before, const Quaternion& current, const Quaternion& after)
	static Quaternion squad(const Quaternion& from, const Quaternion& to, const Quaternion& a,const Quaternion& b, float delta)
	*/
/*
}
*/


} // End namespace nameless namespace

