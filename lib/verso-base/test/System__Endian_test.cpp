/**
 * @file System__Endian_test.cpp
 * @brief Test for the class Endian.
 * @author Max Vilkki <codise@codise.org>
 *
 * TODO:
 * - \todo
 *
 * What is not tested(!):
 * - What:
 *   Why:
 */

#include <Verso/System/Endian.hpp>
#include <gtest/gtest.h>
#include <iostream>

namespace {

using Verso::Endian;


/**
 * Test Endian defines (compile-time).
 */
TEST(SystemEndianTest, DefinesCompileTime)
{
	bool littleEndian1, littleEndian2, littleEndian3,
			littleEndian4, littleEndian5, littleEndian6;


#if (VERSO_LITTLE_ENDIAN)
	littleEndian1 = true;
#else
	littleEndian1 = false;
#endif


#if (!VERSO_LITTLE_ENDIAN)
	littleEndian2 = false;
#else
	littleEndian2 = true;
#endif

	EXPECT_EQ(littleEndian1, littleEndian2);


#if (VERSO_LITTLE_ENDIAN)
	littleEndian3 = true;
#else
	littleEndian3 = false;
#endif

	EXPECT_EQ(littleEndian2, littleEndian3);


	if (VERSO_LITTLE_ENDIAN)
		littleEndian4 = true;
	else
		littleEndian4 = false;

	EXPECT_EQ(littleEndian3, littleEndian4);


	if (VERSO_LITTLE_ENDIAN == true)
		littleEndian5 = true;
	else
		littleEndian5 = false;

	EXPECT_EQ(littleEndian4, littleEndian5);


	littleEndian6 = Endian::isLittleEndian();

	EXPECT_EQ(littleEndian5, littleEndian6);
}


/**
 * Test Endian defines (compile-time) vs. runtime test.
 */
TEST(SystemEndianTest, DefinesCompileTimeVsRuntime)
{
	bool littleEndian5, littleEndian7;


	if (VERSO_LITTLE_ENDIAN == true)
		littleEndian5 = true;
	else
		littleEndian5 = false;


	// Runtime test
	unsigned int i = 1;
	char *c = reinterpret_cast<char*>(&i);
	if (*c)
		littleEndian7 = true;
	else
		littleEndian7 = false;

	EXPECT_EQ(littleEndian5, littleEndian7);
}


} // End namespace nameless namespace

