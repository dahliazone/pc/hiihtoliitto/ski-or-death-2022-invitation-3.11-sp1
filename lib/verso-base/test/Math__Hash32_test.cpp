/**
 * @file Math__Hash32_test.cpp
 * @brief Test for the class Hash.
 * @author Max Vilkki <codise@codise.org>
 *
 * TODO:
 * - \todo
 *
 * What is not tested(!):
 * - What:
 *   Why:
 */

#include <Verso/Math/Hash32.hpp>
#include <gtest/gtest.h>

namespace {

using Verso::Hash32;


/**
 * Test constructors.
 */
TEST(Math__Hash32_test, Test_constructors)
{
	Hash32 h1;
	EXPECT_EQ(h1.hashValue, 0);

	Hash32 h2(123);
	EXPECT_EQ(h2.hashValue, 123);
}


/**
 * Test toString() and toStringDebug().
 */
TEST(Math__Hash32_test, Test_toString_and_toStringDebug)
{
	Hash32 h1;
	EXPECT_STRCASEEQ(h1.toString().c_str(), "0");
	EXPECT_STRCASEEQ(h1.toStringDebug().c_str(), "Hash32(0)");
	std::cout << "testing << operator: h1 = "<<h1 << std::endl; // just to check for crashes etc.

	Hash32 h2(123);
	EXPECT_STRCASEEQ(h2.toString().c_str(), "123");
	EXPECT_STRCASEEQ(h2.toStringDebug().c_str(), "Hash32(123)");
	std::cout << "testing << operator: h2 = "<<h2 << std::endl; // just to check for crashes etc.
}


/**
 * Test FNV-1 hash algorithm.
 */
TEST(Math__Hash32_test, Test_fnv1)
{
	const char str1[] = "abcde";
	Hash32 h1(Hash32::fnv1(str1, sizeof(str1)));
	EXPECT_EQ(h1.hashValue, 0x9f2d477e);

	const char str2[] = "E2j\\5of023 jfqwjo3rt0 qg3qw3.g34 5fwj/FJaL3";
	Hash32 h2(Hash32::fnv1(str2, sizeof(str2)));
	EXPECT_EQ(h2.hashValue, 0x7b877baa);
}


/**
 * Test compare operators.
 */
TEST(Math__Hash32_test, Test_compare_operators)
{
	Hash32 h1_zero, h2_zero;
	EXPECT_EQ(h1_zero, h2_zero);
}


} // End namespace nameless namespace

