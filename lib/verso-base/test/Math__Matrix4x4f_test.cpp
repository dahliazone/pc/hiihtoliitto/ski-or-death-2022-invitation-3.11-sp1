/**
 * @file Math__Matrix4x4f_test.cpp
 * @brief Test for the class Matrix4x4f.
 * @author Max Vilkki <codise@codise.org>
 *
 * TODO:
 * - \todo
 *
 * What is not tested(!):
 * - What:
 *   Why:
 */

#include <Verso/Math/Matrix4x4f.hpp>
#include <gtest/gtest.h>

namespace {

using Verso::Matrix4x4f;
using Verso::Vector3f;
using Verso::Vector4f;


TEST(MathMatrix4x4fTest, ZeroAndCopy)
{
	Matrix4x4f z1 = Matrix4x4f::Zero();
	Matrix4x4f z2(Matrix4x4f::Zero());

	for (size_t row=0; row<4; row++) {
		for (size_t column=0; column<4; column++) {
			EXPECT_EQ(z1.data[row][column], 0.0f);
			EXPECT_EQ(z2.data[row][column], 0.0f);
		}
	}
}


TEST(MathMatrix4x4fTest, IdentityAndCopy)
{
	Matrix4x4f i1 = Matrix4x4f::Identity();
	Matrix4x4f i2(Matrix4x4f::Identity());

	for (size_t row=0; row<4; row++) {
		for (size_t column=0; column<4; column++) {
			if (row == column) {
				EXPECT_EQ(i1.data[row][column], 1.0f);
				EXPECT_EQ(i2.data[row][column], 1.0f);
			}
			else {
				EXPECT_EQ(i1.data[row][column], 0.0f);
				EXPECT_EQ(i2.data[row][column], 0.0f);
			}
		}
	}
}


TEST(MathMatrix4x4fTest, Constructor)
{
	Matrix4x4f i(Matrix4x4f::Identity());

	Matrix4x4f m;
	for (size_t row=0; row<4; row++) {
		for (size_t column=0; column<4; column++) {
			EXPECT_EQ(m.data[row][column], i.data[row][column]);
		}
	}
}


TEST(MathMatrix4x4fTest, ConstructorFromPointer)
{
	const float data[16] = { 1.0f, 2.0f, 3.0f, 4.0f,
							 5.0f, 6.0f, 7.0f, 8.0f,
							 9.0f, 10.0f, 11.0f, 12.0f,
							 13.0f, 14.0f, 15.0f, 16.0f };
	Matrix4x4f m(data);
	for (size_t row=0; row<4; row++) {
		for (size_t column=0; column<4; column++) {
			EXPECT_EQ(m.data[row][column], data[row * 4 + column]);
		}
	}
}


TEST(MathMatrix4x4fTest, ConstructorFromParams3x3)
{
	const float data[16] = { 1.0f, 2.0f, 3.0f, 0.0f,
							 5.0f, 6.0f, 7.0f, 0.0f,
							 9.0f, 10.0f, 11.0f, 0.0f,
							 0.0f, 0.0f, 0.0f, 1.0f };
	Matrix4x4f m(data[0], data[1], data[2],
			data[4], data[5], data[6],
			data[8], data[9], data[10]);
	for (size_t row=0; row<4; row++) {
		for (size_t column=0; column<4; column++) {
			EXPECT_EQ(m.data[row][column], data[row * 4 + column]);
		}
	}
}


TEST(MathMatrix4x4fTest, ConstructorFromParams4x4)
{
	const float data[16] = { 1.0f, 2.0f, 3.0f, 4.0f,
							 5.0f, 6.0f, 7.0f, 8.0f,
							 9.0f, 10.0f, 11.0f, 12.0f,
							 13.0f, 14.0f, 15.0f, 16.0f };
	Matrix4x4f m(data[0], data[1], data[2], data[3],
			data[4], data[5], data[6], data[7],
			data[8], data[9], data[10], data[11],
			data[12], data[13], data[14], data[15]);
	for (size_t row=0; row<4; row++) {
		for (size_t column=0; column<4; column++) {
			EXPECT_EQ(m.data[row][column], data[row * 4 + column]);
		}
	}
}


TEST(MathMatrix4x4fTest, CopyConstructor)
{
	const float data[16] = { 1.0f, 2.0f, 3.0f, 4.0f,
							 5.0f, 6.0f, 7.0f, 8.0f,
							 9.0f, 10.0f, 11.0f, 12.0f,
							 13.0f, 14.0f, 15.0f, 16.0f };
	Matrix4x4f src(data);
	Matrix4x4f dest(src);

	for (size_t row=0; row<4; row++) {
		for (size_t column=0; column<4; column++) {
			EXPECT_EQ(dest.data[row][column], src.data[row][column]);
		}
	}
}


TEST(MathMatrix4x4fTest, GetElement)
{
	const float data[16] = { 1.0f, 2.0f, 3.0f, 4.0f,
							 5.0f, 6.0f, 7.0f, 8.0f,
							 9.0f, 10.0f, 11.0f, 12.0f,
							 13.0f, 14.0f, 15.0f, 16.0f };
	Matrix4x4f m(data);

	for (size_t row=0; row<4; row++) {
		for (size_t column=0; column<4; column++) {
			EXPECT_EQ(m.getElement(row, column), data[row * 4 + column]);
		}
	}

	EXPECT_THROW(m.getElement(-1,  0), Verso::AssertException);
	EXPECT_THROW(m.getElement( 0, -1), Verso::AssertException);
	EXPECT_THROW(m.getElement(-1, -1), Verso::AssertException);
	EXPECT_THROW(m.getElement(-1,  1), Verso::AssertException);
	EXPECT_THROW(m.getElement( 1, -1), Verso::AssertException);

	EXPECT_THROW(m.getElement( 4,  0), Verso::AssertException);
	EXPECT_THROW(m.getElement( 0,  4), Verso::AssertException);
	EXPECT_THROW(m.getElement( 4,  4), Verso::AssertException);
	EXPECT_THROW(m.getElement( 4,  1), Verso::AssertException);
	EXPECT_THROW(m.getElement( 1,  4), Verso::AssertException);
}


TEST(MathMatrix4x4fTest, SetElement)
{
	const float data[16] = { 1.0f, 2.0f, 3.0f, 4.0f,
							 5.0f, 6.0f, 7.0f, 8.0f,
							 9.0f, 10.0f, 11.0f, 12.0f,
							 13.0f, 14.0f, 15.0f, 16.0f };
	Matrix4x4f m;

	for (size_t row=0; row<4; row++) {
		for (size_t column=0; column<4; column++) {
			m.setElement(row, column, data[row * 4 + column]);
		}
	}

	for (size_t row=0; row<4; row++) {
		for (size_t column=0; column<4; column++) {
			EXPECT_EQ(m.getElement(row, column), data[row * 4 + column]);
		}
	}

	EXPECT_THROW(m.setElement(-1,  0, 1.0f), Verso::AssertException);
	EXPECT_THROW(m.setElement( 0, -1, 1.0f), Verso::AssertException);
	EXPECT_THROW(m.setElement(-1, -1, 1.0f), Verso::AssertException);
	EXPECT_THROW(m.setElement(-1,  1, 1.0f), Verso::AssertException);
	EXPECT_THROW(m.setElement( 1, -1, 1.0f), Verso::AssertException);

	EXPECT_THROW(m.setElement( 4,  0, 1.0f), Verso::AssertException);
	EXPECT_THROW(m.setElement( 0,  4, 1.0f), Verso::AssertException);
	EXPECT_THROW(m.setElement( 4,  4, 1.0f), Verso::AssertException);
	EXPECT_THROW(m.setElement( 4,  1, 1.0f), Verso::AssertException);
	EXPECT_THROW(m.setElement( 1,  4, 1.0f), Verso::AssertException);
}


TEST(MathMatrix4x4fTest, GetRow)
{
	const float data[16] = { 1.0f, 2.0f, 3.0f, 4.0f,
							 5.0f, 6.0f, 7.0f, 8.0f,
							 9.0f, 10.0f, 11.0f, 12.0f,
							 13.0f, 14.0f, 15.0f, 16.0f };
	Matrix4x4f m(data);

	for (size_t row=0; row<4; row++) {
		Vector4f val(m.getRow(row));
		EXPECT_EQ(val.x, data[row * 4 + 0]);
		EXPECT_EQ(val.y, data[row * 4 + 1]);
		EXPECT_EQ(val.z, data[row * 4 + 2]);
		EXPECT_EQ(val.w, data[row * 4 + 3]);
	}

	EXPECT_THROW(m.getRow(-2), Verso::AssertException);
	EXPECT_THROW(m.getRow(-1), Verso::AssertException);
	EXPECT_THROW(m.getRow(4), Verso::AssertException);
	EXPECT_THROW(m.getRow(5), Verso::AssertException);
}


TEST(MathMatrix4x4fTest, SetRow)
{
	const float data[16] = { 1.0f, 2.0f, 3.0f, 4.0f,
							 5.0f, 6.0f, 7.0f, 8.0f,
							 9.0f, 10.0f, 11.0f, 12.0f,
							 13.0f, 14.0f, 15.0f, 16.0f };
	Matrix4x4f m;

	for (size_t row=0; row<4; row++) {
		Vector4f val(data[row * 4 + 0], data[row * 4 + 1], data[row * 4 + 2], data[row * 4 + 3]);
		m.setRow(row, val);
	}

	for (size_t row=0; row<4; row++) {
		Vector4f val(m.getRow(row));
		EXPECT_EQ(val.x, data[row * 4 + 0]);
		EXPECT_EQ(val.y, data[row * 4 + 1]);
		EXPECT_EQ(val.z, data[row * 4 + 2]);
		EXPECT_EQ(val.w, data[row * 4 + 3]);
	}

	EXPECT_THROW(m.getRow(-2), Verso::AssertException);
	EXPECT_THROW(m.getRow(-1), Verso::AssertException);
	EXPECT_THROW(m.getRow(4), Verso::AssertException);
	EXPECT_THROW(m.getRow(5), Verso::AssertException);
}


TEST(MathMatrix4x4fTest, GetColumn)
{
	const float data[16] = { 1.0f, 2.0f, 3.0f, 4.0f,
							 5.0f, 6.0f, 7.0f, 8.0f,
							 9.0f, 10.0f, 11.0f, 12.0f,
							 13.0f, 14.0f, 15.0f, 16.0f };
	Matrix4x4f m(data);

	for (size_t column=0; column<4; column++) {
		Vector4f val(m.getColumn(column));
		EXPECT_EQ(val.x, data[0 * 4 + column]);
		EXPECT_EQ(val.y, data[1 * 4 + column]);
		EXPECT_EQ(val.z, data[2 * 4 + column]);
		EXPECT_EQ(val.w, data[3 * 4 + column]);
	}

	EXPECT_THROW(m.getColumn(-2), Verso::AssertException);
	EXPECT_THROW(m.getColumn(-1), Verso::AssertException);
	EXPECT_THROW(m.getColumn(4), Verso::AssertException);
	EXPECT_THROW(m.getColumn(5), Verso::AssertException);
}


TEST(MathMatrix4x4fTest, SetColumn)
{
	const float data[16] = { 1.0f, 2.0f, 3.0f, 4.0f,
							 5.0f, 6.0f, 7.0f, 8.0f,
							 9.0f, 10.0f, 11.0f, 12.0f,
							 13.0f, 14.0f, 15.0f, 16.0f };
	Matrix4x4f m;

	for (size_t column=0; column<4; column++) {
		Vector4f val(data[0 * 4 + column], data[1 * 4 + column], data[2 * 4 + column], data[3 * 4 + column]);
		m.setColumn(column, val);
	}

	for (size_t column=0; column<4; column++) {
		Vector4f val(m.getColumn(column));
		EXPECT_EQ(val.x, data[0 * 4 + column]);
		EXPECT_EQ(val.y, data[1 * 4 + column]);
		EXPECT_EQ(val.z, data[2 * 4 + column]);
		EXPECT_EQ(val.w, data[3 * 4 + column]);
	}

	EXPECT_THROW(m.setColumn(-2, Vector4f()), Verso::AssertException);
	EXPECT_THROW(m.setColumn(-1, Vector4f()), Verso::AssertException);
	EXPECT_THROW(m.setColumn(4, Vector4f()), Verso::AssertException);
	EXPECT_THROW(m.setColumn(5, Vector4f()), Verso::AssertException);
}


TEST(MathMatrix4x4fTest, SetFromPointer)
{
	const float data[16] = { 1.0f, 2.0f, 3.0f, 4.0f,
							 5.0f, 6.0f, 7.0f, 8.0f,
							 9.0f, 10.0f, 11.0f, 12.0f,
							 13.0f, 14.0f, 15.0f, 16.0f };
	Matrix4x4f m;
	m.set(data);

	for (size_t row=0; row<4; row++) {
		for (size_t column=0; column<4; column++) {
			EXPECT_EQ(m.data[row][column], data[row * 4 + column]);
		}
	}
}


TEST(MathMatrix4x4fTest, SetFromParams3x3)
{
	const float data[16] = { 1.0f, 2.0f, 3.0f, 0.0f,
							 5.0f, 6.0f, 7.0f, 0.0f,
							 9.0f, 10.0f, 11.0f, 0.0f,
							 0.0f, 0.0f, 0.0f, 1.0f };
	Matrix4x4f m;
	m.set(data[0], data[1], data[2],
			data[4], data[5], data[6],
			data[8], data[9], data[10]);

	for (size_t row=0; row<4; row++) {
		for (size_t column=0; column<4; column++) {
			EXPECT_EQ(m.data[row][column], data[row * 4 + column]);
		}
	}
}


TEST(MathMatrix4x4fTest, SetFromParams4x4)
{
	const float data[16] = { 1.0f, 2.0f, 3.0f, 4.0f,
							 5.0f, 6.0f, 7.0f, 8.0f,
							 9.0f, 10.0f, 11.0f, 12.0f,
							 13.0f, 14.0f, 15.0f, 16.0f };
	Matrix4x4f m;
	m.set(data[0], data[1], data[2], data[3],
			data[4], data[5], data[6], data[7],
			data[8], data[9], data[10], data[11],
			data[12], data[13], data[14], data[15]);

	for (size_t row=0; row<4; row++) {
		for (size_t column=0; column<4; column++) {
			EXPECT_EQ(m.data[row][column], data[row * 4 + column]);
		}
	}
}


TEST(MathMatrix4x4fTest, SetFromMatrix4x4f)
{
	const float data[16] = { 1.0f, 2.0f, 3.0f, 4.0f,
							 5.0f, 6.0f, 7.0f, 8.0f,
							 9.0f, 10.0f, 11.0f, 12.0f,
							 13.0f, 14.0f, 15.0f, 16.0f };
	Matrix4x4f src(data);
	Matrix4x4f dest;
	dest.set(src);

	for (size_t row=0; row<4; row++) {
		for (size_t column=0; column<4; column++) {
			EXPECT_EQ(dest.data[row][column], src.data[row][column]);
		}
	}
}


TEST(MathMatrix4x4fTest, MultiplyByVector3f)
{
	const float data[16] = { 1.0f, 2.0f, 3.0f, 4.0f,
							 5.0f, 6.0f, 7.0f, 8.0f,
							 9.0f, 10.0f, 11.0f, 12.0f,
							 13.0f, 14.0f, 15.0f, 16.0f };
	Matrix4x4f m(data);

	Vector3f v1(10.0f, 20.0f, 30.0f);
	Vector3f result1Expected(140.0f, 380.0f, 620.0f);
	Vector3f result1_1(m.multiplyByVector(v1));
	EXPECT_EQ(result1_1, result1Expected);
	Vector3f result1_2(m * v1);
	EXPECT_EQ(result1_2, result1Expected);

	Vector3f v2(-2.0f, -1.0f, 2.0f);
	Vector3f result2Expected(2.0f, -2.0f, -6.0f);
	Vector3f result2_1(m.multiplyByVector(v2));
	EXPECT_EQ(result2_1, result2Expected);
	Vector3f result2_2(m * v2);
	EXPECT_EQ(result2_2, result2Expected);
}


TEST(MathMatrix4x4fTest, MultiplyByVector4f)
{
	const float data[16] = { 1.0f, 2.0f, 3.0f, 4.0f,
							 5.0f, 6.0f, 7.0f, 8.0f,
							 9.0f, 10.0f, 11.0f, 12.0f,
							 13.0f, 14.0f, 15.0f, 16.0f };
	Matrix4x4f m(data);

	Vector4f v1(10.0f, 20.0f, 30.0f, 40.0f);
	Vector4f result1Expected(300.0f, 700.0f, 1100.0f, 1500.0f);
	Vector4f result1_1(m.multiplyByVector(v1));
	EXPECT_EQ(result1_1, result1Expected);
	Vector4f result1_2(m * v1);
	EXPECT_EQ(result1_2, result1Expected);

	Vector4f v2(-2.0f, -1.0f, 2.0f, 3.0f);
	Vector4f result2Expected(14.0f, 22.0f, 30.0f, 38.0f);
	Vector4f result2_1(m.multiplyByVector(v2));
	EXPECT_EQ(result2_1, result2Expected);
	Vector4f result2_2(m * v2);
	EXPECT_EQ(result2_2, result2Expected);
}


TEST(MathMatrix4x4fTest, MultiplyByMatrix4x4)
{
	const float dataA[16] = { 1.0f, 2.0f, 3.0f, 4.0f,
							  5.0f, 6.0f, 7.0f, 8.0f,
							  9.0f, 10.0f, 11.0f, 12.0f,
							  13.0f, 14.0f, 15.0f, 16.0f };
	Matrix4x4f a(dataA);

	const float dataB[16] = { 16.0f, 15.0f, 14.0f, 13.0f,
							  12.0f, 11.0f, 10.0f, 9.0f,
							  8.0f, 7.0f, 6.0f, 5.0f,
							  4.0f, 3.0f, 2.0f, 1.0f };
	Matrix4x4f b(dataB);

	const float result1Data[16] = { 80.0f, 70.0f, 60.0f, 50.0f,
									240.0f, 214.0f, 188.0f, 162.0f,
									400.0f, 358.0f, 316.0f, 274.0f,
									560.0f, 502.0f, 444.0f, 386 };
	Matrix4x4f mResult1_1(a.multiplyByMatrix(b));
	for (size_t row=0; row<4; row++) {
		for (size_t column=0; column<4; column++) {
			EXPECT_EQ(mResult1_1.data[row][column], result1Data[row * 4 + column]);
		}
	}
	Matrix4x4f mResult1_2(a * b);
	for (size_t row=0; row<4; row++) {
		for (size_t column=0; column<4; column++) {
			EXPECT_EQ(mResult1_2.data[row][column], result1Data[row * 4 + column]);
		}
	}


	const float result2Data[16] = { 386.0f, 444.0f, 502.0f, 560.0f,
									274.0f, 316.0f, 358.0f, 400.0f,
									162.0f, 188.0f, 214.0f, 240.0f,
									50.0f, 60.0f, 70.0f, 80.0f };
	Matrix4x4f mResult2_1(b.multiplyByMatrix(a));
	for (size_t row=0; row<4; row++) {
		for (size_t column=0; column<4; column++) {
			EXPECT_EQ(mResult2_1.data[row][column], result2Data[row * 4 + column]);
		}
	}
	Matrix4x4f mResult2_2(b * a);
	for (size_t row=0; row<4; row++) {
		for (size_t column=0; column<4; column++) {
			EXPECT_EQ(mResult2_2.data[row][column], result2Data[row * 4 + column]);
		}
	}


	const float dataC[16] = {
		-2.0f, 4.0f, 10.0f, -5.0f,
		44.0f, 3.0f, -3.0f, -44.0f,
		-9.0f, 2.0f, 42.0f, 9.0f,
		-2.0f, 4.0f, 77.0f, -21.0f
	};
	Matrix4x4f c(dataC);

	const float dataD[16] = {
		2.0f, -4.0f, 6.0f, -8.0f,
		16.0f, -32.0f, 64.0f, -128.0f,
		256.0f, -512.0f, 1024.0f, -2048.0f,
		4096.0f, -8192.0f, 16384.0f, -32768.0f
	};
	Matrix4x4f d(dataD);

	const float result3Data[16] = { -17860.0f, 35720.0f, -71436.0f, 142864.0f,
									-180856.0f, 361712.0f, -723512.0f, 1447200.0f,
									47630.0f, -95260.0f, 190538.0f, -381112.0f,
									-66244.0f, 132488.0f, -264972.0f, 529936.0f };
	Matrix4x4f mResult3_1(c.multiplyByMatrix(d));
	for (size_t row=0; row<4; row++) {
		for (size_t column=0; column<4; column++) {
			EXPECT_EQ(mResult3_1.data[row][column], result3Data[row * 4 + column]);
		}
	}
	Matrix4x4f mResult3_2(c * d);
	for (size_t row=0; row<4; row++) {
		for (size_t column=0; column<4; column++) {
			EXPECT_EQ(mResult3_2.data[row][column], result3Data[row * 4 + column]);
		}
	}

	const float result4Data[16] = { -218.0f, -24.0f, -332.0f, 388.0f,
									-1760.0f, -416.0f, -6912.0f, 4592.0f,
									-28160.0f, -6656.0f, -110592.0f, 73472.0f,
									-450560.0f, -106496.0f, -1769472.0f, 1175552.0f
	};
	Matrix4x4f mResult4_1(d.multiplyByMatrix(c));
	for (size_t row=0; row<4; row++) {
		for (size_t column=0; column<4; column++) {
			EXPECT_EQ(mResult4_1.data[row][column], result4Data[row * 4 + column]);
		}
	}
	Matrix4x4f mResult4_2(d * c);
	for (size_t row=0; row<4; row++) {
		for (size_t column=0; column<4; column++) {
			EXPECT_EQ(mResult4_2.data[row][column], result4Data[row * 4 + column]);
		}
	}
}


// Zero() & Identity() already tested above


TEST(MathMatrix4x4fTest, Transpose)
{
	const float data[16] = { 1.0f, 2.0f, 3.0f, 4.0f,
							 5.0f, 6.0f, 7.0f, 8.0f,
							 9.0f, 10.0f, 11.0f, 12.0f,
							 13.0f, 14.0f, 15.0f, 16.0f };
	Matrix4x4f m(data);
	Matrix4x4f result(Matrix4x4f::transpose(m));

	const float expectedResult[16] =
	{
		1.0f, 5.0f, 9.0f, 13.0f,
		2.0f, 6.0f, 10.0f, 14.0f,
		3.0f, 7.0f, 11.0f, 15.0f,
		4.0f, 8.0f, 12.0f, 16.0f
	};
	for (size_t row=0; row<4; row++) {
		for (size_t column=0; column<4; column++) {
			EXPECT_EQ(result.data[row][column], expectedResult[row * 4 + column]);
		}
	}
}


TEST(MathMatrix4x4fTest, InverseUnimplemented)
{
	//static Matrix4x4f inverse(const Matrix4x4f& right)
}



/*
inline void testMatrix4x4f()
{
	cout << "Matrix4x4f" << endl;
	Matrix4x4f m1;
	cout << "  Matrix4x4f m1()<< "<<m1 << endl;
	cout << "  m1.toString() = "<<m1.toString() << endl;
	cout << "  m1.toStringDebug() = "<<m1.toStringDebug() << endl;

	cout << "  Matrix4x4f::Zero<< "<<Matrix4x4f::Zero << endl;
	cout << "  Matrix4x4f::Identity<< "<<Matrix4x4f::Identity << endl;

	Matrix4x4f identity(Matrix4x4f::Identity);
	cout << "  Matrix4x4f identity(Matrix4x4f::Identity) << "<<identity << endl;
	cout << "  identity.multiplyByMatrix(m2); identity << "<<identity << endl;

	const float data1[16] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
	const float data2[16] = { 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
	cout << "  Matrix4x4f(const float* data1) << "<<Matrix4x4f(data1) << endl;
	cout << "  Matrix4x4f({16, 15, ..., 1}) << "<<Matrix4x4f({16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1}) << endl;
	cout << "  Matrix4x4f(float m00, float m01, ...) << "<<Matrix4x4f(11, 12, 13, 21, 22, 23, 31, 32, 33) << endl;

	Matrix4x4f m2(data1);
	cout << "  Matrix4x4f m2(data1) << "<<m2 << endl;
	cout << "  Matrix4x4f(const Matrix& m2) << "<<Matrix4x4f(m2) << endl;

	cout << "    m2.getElement(0, 0) = "<<m2.getElement(0, 0) << endl;
	cout << "    m2.getElement(0, 3) = "<<m2.getElement(0, 3) << endl;
	cout << "    m2.getElement(1, 0) = "<<m2.getElement(1, 0) << endl;
	cout << "    m2.getElement(2, 2) = "<<m2.getElement(2, 2) << endl;
	cout << "    m2.getElement(3, 3) = "<<m2.getElement(3, 3) << endl;

	m2.setElement(0, 0, 333);
	m2.setElement(1, 3, 666);
	m2.setElement(3, 2, 777);
	cout << "    m2.setElement(0, 0, 333);" << endl;
	cout << "    m2.setElement(1, 3, 666);" << endl;
	cout << "    m2.setElement(3, 2, 777);" << endl;
	cout << "    m2 << "<<m2 << endl;

	cout << "  m2 * Matrix4x4f::Identity << "<<(m2 * Matrix4x4f::Identity) << endl;
	cout << "  Matrix4x4f::Identity * m2 << "<<(Matrix4x4f::Identity * m2) << endl;
	cout << "  m2.multiplyByMatrix(Matrix4x4f::Identity) => "<<m2.multiplyByMatrix(Matrix4x4f::Identity) << endl;
	cout << "  identity.multiplyByMatrix(m2) => "<<identity.multiplyByMatrix(m2) << endl;

	Matrix4x4f m3(data1);
	Matrix4x4f m4(data2);
	cout << "    m3 << "<<m3 << endl;
	cout << "    m4 << "<<m4 << endl;
	cout << "    m3 * m4 => "<<(m3 * m4) << endl;
	cout << "    m4 * m3 => "<<(m4 * m3) << endl << endl;

	cout << "    m3 == m4 => "<<(m3 == m4) << endl;
	cout << "    m4 == m3 => "<<(m4 == m3) << endl;
	cout << "    m3 == m3 => "<<(m3 == m3) << endl;
	cout << "    m4 == m4 => "<<(m4 == m4) << endl;
	cout << "    m3 != m4 => "<<(m3 != m4) << endl;
	cout << "    m4 != m3 => "<<(m4 != m3) << endl;
	cout << "    m3 != m3 => "<<(m3 != m3) << endl;
	cout << "    m4 != m4 => "<<(m4 != m4) << endl << endl;

	Matrix4x4f m3Copy, m4Copy;
	cout << "    m3 << "<<m3 << endl;
	cout << "    m4 << "<<m4 << endl;
	m3Copy = m3;
	m4Copy = m4;
	cout << "    m3Copy = m3 => "<<m3Copy << endl;
	cout << "    m4Copy = m4 => "<<m4Copy << endl;

	cout << "    Matrix4x4f::transpose(m3) => "<<Matrix4x4f::transpose(m3) << endl;

	//Matrix4x4f inverse(const Matrix4x4f& right)

	// TODO: kusee
	Matrix4x4f orthographicProjection1 = Matrix4x4f::orthographicProjection();
	cout << "    orthographicProjection1 << "<<orthographicProjection1 << endl;
	Matrix4x4f orthographicProjection2 = Matrix4x4f::orthographicProjection(-1.0f, 2.0f, 2.0f, 2.0f, -2.0f, 2.0f);
	cout << "    orthographicProjection2 << "<<orthographicProjection2 << endl << endl;

	// TODO: kusee
	float fov = 90.0f, aspect4_3 = 4.0f/3.0f, aspect16_9 = 16.0f/9.0f;
	Matrix4x4f perspectiveProjection1 = Matrix4x4f::perspectiveProjection(fov, aspect4_3, 0.0f, 10.0f);
	cout << "    perspectiveProjection1 << "<<perspectiveProjection1 << endl;
	Matrix4x4f perspectiveProjection2 = Matrix4x4f::perspectiveProjection(fov, aspect16_9, 0.0f, 10.0f);
	cout << "    perspectiveProjection2 << "<<perspectiveProjection2 << endl << endl;


	Matrix4x4f translation1 = Matrix4x4f::translation(Vector3f(1, 2, 3));
	cout << "    translation1 << "<<translation1 << endl;

	Matrix4x4f translation2 = Matrix4x4f::translation(1, 2, 3);
	cout << "    translation2 << "<<translation2 << endl;

	Matrix4x4f rotationX1 = Matrix4x4f::rotationX(100);
	cout << "    rotationX1 << "<<rotationX1 << endl;

	Matrix4x4f rotationY1 = Matrix4x4f::rotationY(50);
	cout << "    rotationY1 << "<<rotationY1 << endl;

	Matrix4x4f rotationZ1 = Matrix4x4f::rotationZ(25);
	cout << "    rotationZ1 << "<<rotationZ1 << endl;

	Matrix4x4f scaling1 = Matrix4x4f::scaling(2);
	cout << "    scaling1 << "<<scaling1 << endl;

	Matrix4x4f scaling2 = Matrix4x4f::scaling(Vector3f(1, 2, 3));
	cout << "    scaling2 << "<<scaling2 << endl;

	Matrix4x4f scaling3 = Matrix4x4f::scaling(3, 2, 1);
	cout << "    scaling3 << "<<scaling3 << endl;
}
*/


} // End namespace nameless namespace

