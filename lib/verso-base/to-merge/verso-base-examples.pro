# == Shared ===============================================================
! include( ../verso-base-common.pri ) {
    error("Couldn't find the verso-base-common.pri file!")
}

TEMPLATE = app
CONFIG += console

DESTDIR = "$${BUILD_DIR}"
TARGET = libverso-base-examples


DEPENDPATH += \
    $${PWD}

HEADERS += \
    examples.hpp

SOURCES += \
    examples.cpp \
    main.cpp


# == Linux ================================================================
linux-* {
    QMAKE_POST_LINK += echo "Post-link process..."

    # Verso-base
    LIBS += -L"$${BUILD_DIR}" -lverso-base

    # cmath
    LIBS += -lm

    # Scripts
    QMAKE_POST_LINK += && cp -rf "$${PWD}/libverso-base-examples-x64.sh" "$${DESTDIR}/"

    # Data
    QMAKE_POST_LINK += && cp -rf "$${PWD}/../data" "$${DESTDIR}/"
}


# == OS X =================================================================
macx {
    CONFIG -= app_bundle
    QMAKE_POST_LINK += echo "Post-link process..."

    # Verso-base
    LIBS += -L"$${BUILD_DIR}" -lverso-base
    LIBVERSOBASE = libverso-base.1.dylib
    QMAKE_POST_LINK += && install_name_tool -change "$${LIBVERSOBASE}" "@executable_path/$${LIBVERSOBASE}" "$${DESTDIR}/$${TARGET}"

    # Scripts
    QMAKE_POST_LINK += && cp -rf "$${PWD}/libverso-base-examples-osx.sh" "$${DESTDIR}/"

    # Data
    QMAKE_POST_LINK += && cp -rf "$${PWD}/../data" "$${DESTDIR}/"
}


# == Windows ==============================================================
win* {
    QMAKE_POST_LINK += @ECHO "Post-link process..."

    # Verso-base
    LIBS += -L"$${BUILD_DIR}" -lverso-base

    # Copy data directory
    QMAKE_POST_LINK += && XCOPY /s /q /y /i \"$${PWD}\..\data\" \"$${DESTDIR}\data\"

    # Remove temporaries
    #QMAKE_POST_LINK += && RMDIR /S /Q \"$${PWD}\..\debug\" \"$${PWD}\..\release\"
}
