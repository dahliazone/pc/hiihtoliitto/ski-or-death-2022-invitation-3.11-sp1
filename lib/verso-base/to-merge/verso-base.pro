TEMPLATE = subdirs
win* {
		TEMPLATE = subdirs
}
CONFIG += ordered


libverso-base.file = src/verso-base-src.pro
libverso-base.target = libverso-base-dep

tests_libverso-base.file = test/verso-base-test.pro
tests_libverso-base.target = tests_libverso-base-dep
tests_libverso-base.depends = libverso-base-dep

examples_libverso-base.file = examples/verso-base-examples.pro
examples_libverso-base.depends = libverso-base-dep tests_libverso-base-dep

SUBDIRS = libverso-base tests_libverso-base examples_libverso-base
