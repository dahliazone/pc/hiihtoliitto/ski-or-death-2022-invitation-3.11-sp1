#include <Verso/Time/RecordFrameTimer.hpp>
#include <Verso/Time/Sleep.hpp>

namespace Verso {


RecordFrameTimer::RecordFrameTimer(double recordFps) :
	recordFps(recordFps),
	recordTimer(),

	dt(Timestamp::seconds(1.0f / recordFps)),
	updateFrame(1),
	renderFrame(1)
{
}


void RecordFrameTimer::reset(double recordFps)
{
	this->recordFps = recordFps;
	recordTimer.restart();

	dt = Timestamp::seconds(1.0f / recordFps);
	updateFrame = 1;
	renderFrame = 1;
}


void RecordFrameTimer::update()
{
	recordTimer.advance(dt);
	updateFrame++;
}


void RecordFrameTimer::addRenderFrame()
{
	renderFrame++;
}


///////////////////////////////////////////////////////////////////////////////////////////
// getters & setters
///////////////////////////////////////////////////////////////////////////////////////////

double RecordFrameTimer::getRecordFps() const
{
	return recordFps;
}


Timestamp RecordFrameTimer::getTargetDt() const
{
	return Timestamp::seconds(1.0 / recordFps);
}


double RecordFrameTimer::getFps() const
{
	return 1.0 / dt.asSeconds();
}


Timestamp RecordFrameTimer::getDt() const
{
	return dt;
}


Timestamp RecordFrameTimer::getElapsed() const
{
	return recordTimer.getElapsed();
}


size_t RecordFrameTimer::getUpdateFrame() const
{
	return updateFrame;
}


size_t RecordFrameTimer::getRenderFrame() const
{
	return renderFrame;
}


///////////////////////////////////////////////////////////////////////////////////////////
// toString
///////////////////////////////////////////////////////////////////////////////////////////

UString RecordFrameTimer::toString() const
{
	UString str("updateFrame=");
	str.append2(updateFrame);
	str += ", renderFrame=";
	str.append2(renderFrame);
	str += ", dt=";
	str += dt.toStringDebug();
	str += ", elapsed=";
	str += recordTimer.getElapsed().toStringDebug();
	str += ", fps=";
	str.append2(getFps());
	return str;
}


UString RecordFrameTimer::toStringDebug() const
{
	UString str("RecordFrameTimer(");
	str += toString();
	str += ")";
	return str;
}


} // End namespace Verso

