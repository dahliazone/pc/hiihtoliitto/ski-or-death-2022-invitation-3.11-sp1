#include <Verso/Time/Datetime.hpp>
#include <ctime>

namespace Verso {


UString Datetime::getNow()
{
	std::time_t now = std::time(nullptr);
	std::tm * ptm = std::localtime(&now);
	char buffer[20];
	std::strftime(buffer, 20, "%Y-%m-%d %H:%M:%S", ptm);
	return UString(&buffer[0]);
}


UString Datetime::getNowFileName()
{
	std::time_t now = std::time(nullptr);
	std::tm * ptm = std::localtime(&now);
	char buffer[20];
	std::strftime(buffer, 20, "%Y-%m-%d_%H-%M-%S", ptm);
	return UString(&buffer[0]);
}


UString Datetime::getNowDate()
{
	std::time_t now = std::time(nullptr);
	std::tm * ptm = std::localtime(&now);
	char buffer[11];
	std::strftime(buffer, 11, "%Y-%m-%d", ptm);
	return UString(&buffer[0]);
}


UString Datetime::getNowTime()
{
	std::time_t now = std::time(nullptr);
	std::tm * ptm = std::localtime(&now);
	char buffer[9];
	std::strftime(buffer, 9, "%H:%M:%S", ptm);
	return UString(&buffer[0]);
}


} // End namespace Verso

