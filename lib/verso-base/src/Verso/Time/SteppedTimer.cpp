#include <Verso/Time/SteppedTimer.hpp>
#include <Verso/Time/Clock.hpp>

namespace Verso {


SteppedTimer::SteppedTimer(double targetFps) :
	targetFps(targetFps),
	targetDt(Timestamp::seconds(1.0 / targetFps)),
	started(Clock::instance().getElapsed()),
	reserve()
{
}


SteppedTimer::~SteppedTimer()
{
}


///////////////////////////////////////////////////////////////////////////////////////////
// interface ITimer
///////////////////////////////////////////////////////////////////////////////////////////

Timestamp SteppedTimer::getElapsed() const
{
	return Clock::instance().getElapsed() - started;
}


Timestamp SteppedTimer::restart()
{
	Timestamp now = Clock::instance().getElapsed();
	Timestamp elapsed = now - started;

	if (elapsed < targetDt) {
		return Timestamp::Zero();
	}
	else {
		Timestamp oldReserve = reserve;
		reserve = elapsed - targetDt;
		started = now - reserve;
		return elapsed - oldReserve;
	}
}


UString SteppedTimer::toString() const
{
	UString str("elapsed=");
	str.append2(getElapsed().asSeconds());
	str += ", targetDt=";
	str.append2(targetDt);
	str += ", targetFps=";
	str.append2(targetFps);
	return str;
}


UString SteppedTimer::toStringDebug() const
{
	UString str("SteppedTimer(");
	str += toString();
	str += "reserve=";
	str.append2(reserve.asSeconds());
	str += "started=";
	str.append2(started.asSeconds());
	str += ")";
	return str;
}


///////////////////////////////////////////////////////////////////////////////////////////
// SteppedTimer specific getters & setters
///////////////////////////////////////////////////////////////////////////////////////////

double SteppedTimer::getTargetFps() const
{
	return targetFps;
}


Timestamp SteppedTimer::getTargetDt() const
{
	return targetDt;
}


Timestamp SteppedTimer::getStarted() const
{
	return started;
}


Timestamp SteppedTimer::getReserve() const
{
	return reserve;
}


} // End namespace Verso

