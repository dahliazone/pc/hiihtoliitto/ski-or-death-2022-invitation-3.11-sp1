#include <Verso/Time/Sleep.hpp>
#include <chrono>
#include <thread>

namespace Verso {


void Sleep::seconds(double amount)
{
	std::this_thread::sleep_for(std::chrono::microseconds(static_cast<uint64_t>(amount * 1000000.0)));
}


void Sleep::milliseconds(int64_t amount)
{
	std::this_thread::sleep_for(std::chrono::milliseconds(amount));
}


void Sleep::microseconds(int64_t amount)
{
	std::this_thread::sleep_for(std::chrono::microseconds(amount));
}


} // End namespace Verso

