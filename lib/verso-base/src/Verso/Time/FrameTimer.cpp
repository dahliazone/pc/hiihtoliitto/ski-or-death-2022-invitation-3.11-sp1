#include <Verso/Time/FrameTimer.hpp>
#include <Verso/Time/Sleep.hpp>

namespace Verso {


FrameTimer::FrameTimer(double targetFps, Timestamp measureFrequency) :
	targetFps(targetFps),
	frameTimer(targetFps),

	dt(),
	elapsed(),
	updateFrame(1),
	renderFrame(1),

	measureFrequency(measureFrequency),
	measureSumTime(),
	measureSumUpdateFrames(0),
	measureSumRenderFrames(0),
	lastAverageDt(0),
	lastUpdateFrames(0),
	lastRenderFrames(0)
{
}


void FrameTimer::reset(double targetFps, Timestamp measureFrequency)
{
	this->targetFps = targetFps;
	frameTimer.restart();

	dt = Timestamp::Zero();
	elapsed = Timestamp::Zero();
	updateFrame = 1;
	renderFrame = 1;

	this->measureFrequency = measureFrequency;
	measureSumTime = Timestamp::Zero();
	measureSumUpdateFrames = 0;
	measureSumRenderFrames = 0;
	lastAverageDt = 0.0;
	lastUpdateFrames = 0;
	lastRenderFrames = 0;
}


void FrameTimer::update()
{
	dt = frameTimer.restart();
	while (dt == Timestamp::Zero()) {
		dt = frameTimer.restart();
		Sleep::microseconds(5);
	}

	elapsed += dt;
	measureSumTime += dt;

	updateFrame++;
	measureSumUpdateFrames++;
}


void FrameTimer::addRenderFrame()
{
	renderFrame++;
	measureSumRenderFrames++;

	if (measureSumTime >= measureFrequency) {
		lastAverageDt = measureSumTime.asSeconds() / static_cast<double>(measureSumRenderFrames);
		lastUpdateFrames = measureSumUpdateFrames;
		lastRenderFrames = measureSumRenderFrames;

		measureSumTime = Timestamp::Zero();
		measureSumUpdateFrames = 0;
		measureSumRenderFrames = 0;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////
// getters & setters
///////////////////////////////////////////////////////////////////////////////////////////

double FrameTimer::getTargetFps() const
{
	return targetFps;
}


Timestamp FrameTimer::getTargetDt() const
{
	return Timestamp::seconds(1.0 / targetFps);
}


double FrameTimer::getFps() const
{
	return 1.0 / dt.asSeconds();
}


Timestamp FrameTimer::getDt() const
{
	return dt;
}


Timestamp FrameTimer::getElapsed() const
{
	return elapsed;
}


size_t FrameTimer::getUpdateFrame() const
{
	return updateFrame;
}


size_t FrameTimer::getRenderFrame() const
{
	return renderFrame;
}


Timestamp FrameTimer::getMeasureFrequency() const
{
	return measureFrequency;
}


double FrameTimer::getLastAverageDt() const
{
	return lastAverageDt;
}


double FrameTimer::getLastAverageFps() const
{
	return 1.0 / lastAverageDt;
}


size_t FrameTimer::getLastUpdateFrames() const
{
	return lastUpdateFrames;
}


size_t FrameTimer::getLastRenderFrames() const
{
	return lastRenderFrames;
}


size_t FrameTimer::getLastSkippedFrames() const
{
	return lastUpdateFrames - lastRenderFrames;
}


///////////////////////////////////////////////////////////////////////////////////////////
// toString
///////////////////////////////////////////////////////////////////////////////////////////

UString FrameTimer::toString() const
{
	UString str("updateFrame=");
	str.append2(updateFrame);
	str += ", renderFrame=";
	str.append2(renderFrame);
	str += ", dt=";
	str += dt.toStringDebug();
	str += ", lastAverageDt=";
	str.append2(getLastAverageDt());
	str += ", elapsed=";
	str += elapsed.toStringDebug();
	str += ", fps=";
	str.append2(getFps());
	str += ", lastAverageFps=";
	str.append2(getLastAverageFps());
	str += ", lastSkippedFrames=";
	str.append2(getLastSkippedFrames());
	return str;
}


UString FrameTimer::toStringDebug() const
{
	UString str("FrameTimer(");
	str += toString();
	str += ", measureFrequency=";
	str += measureFrequency.toString();
	str += ", measureSumTime=";
	str += measureSumTime.toString();
	str += ", measureSumUpdateFrames=";
	str.append2(measureSumUpdateFrames);
	str += ", measureSumRenderFrames=";
	str.append2(measureSumRenderFrames);
	str += ", lastUpdateFrames=";
	str.append2(getLastUpdateFrames());
	str += ", lastRenderFrames=";
	str.append2(getLastRenderFrames());
	str += ")";
	return str;
}


} // End namespace Verso

