#include <Verso/Time/TimeSource/StdChrono.hpp>
#include <chrono>

namespace Verso {


// interface ITimeSource
UString StdChrono::getName() const
{
	return "StdChrono";
}


int64_t StdChrono::getCurrentMicrosecs()
{
	auto now = std::chrono::high_resolution_clock::now();
	std::chrono::microseconds microsecs =
	        std::chrono::duration_cast<std::chrono::microseconds>(now.time_since_epoch());
	return microsecs.count();
}


} // End namespace Verso

