#include <Verso/System/CommandLineParser/Option.hpp>

namespace Verso {


Option::Option() :
	opt(),
	longOpt(),
	description(),
	required(false),
	hasArgs(false),
	argNames(),
	argDefaultValues(),
	argValues()
{
}


Option::Option(const UString& opt, const UString& longOpt, const UString& description) :
	opt(opt),
	longOpt(longOpt),
	description(description),
	required(false),
	hasArgs(false),
	argNames(),
	argDefaultValues(),
	argValues()
{
	if (opt.size() < 1) {
		VERSO_ILLEGALPARAMETERS("verso-base", "Parameter 'opt' is less than one character long", opt.c_str());
	}

	if (opt.size() > 1) {
		VERSO_ILLEGALPARAMETERS("verso-base", "Parameter 'opt' is more than one character long", opt.c_str());
	}

	if (longOpt.size() != 0 && longOpt.size() < 2) {
		VERSO_ILLEGALPARAMETERS("verso-base", "Parameter 'longOpt' is set and less than two character long", longOpt.c_str());
	}
}


void Option::setRequired(bool required)
{
	this->required = required;
}


void Option::setArgNames(const UString& argName1)
{
	setArgAmount(1);
	this->argNames[0] = argName1;
}


void Option::setArgNames(const UString& argName1, const UString& argName2)
{
	setArgAmount(2);
	this->argNames[0] = argName1;
	this->argNames[1] = argName2;
}


void Option::setArgNames(const UString& argName1, const UString& argName2, const UString& argName3)
{
	setArgAmount(3);
	this->argNames[0] = argName1;
	this->argNames[1] = argName2;
	this->argNames[2] = argName3;
}


void Option::setArgNames(const std::vector<UString>& argNames)
{
	setArgAmount(static_cast<int>(argNames.size()));
	this->argNames = argNames;
}


void Option::setArgDefaultValues(const UString& defaultValue1)
{
	setArgAmount(1);
	this->argDefaultValues[0] = defaultValue1;
}


void Option::setArgDefaultValues(const UString& defaultValue1, const UString& defaultValue2)
{
	setArgAmount(2);
	this->argDefaultValues[0] = defaultValue1;
	this->argDefaultValues[1] = defaultValue2;
}


void Option::setArgDefaultValues(const UString& defaultValue1, const UString& defaultValue2, const UString& defaultValue3)
{
	setArgAmount(3);
	this->argDefaultValues[0] = defaultValue1;
	this->argDefaultValues[1] = defaultValue2;
	this->argDefaultValues[2] = defaultValue3;
}


void Option::setArgDefaultValues(const std::vector<UString>& defaultValues)
{
	setArgAmount(static_cast<int>(defaultValues.size()));
	this->argDefaultValues = defaultValues;
}


void Option::setArgValues(const UString& argValue1)
{
	setArgAmount(1);
	this->argValues[0] = argValue1;
}


void Option::setArgValues(const UString& argValue1, const UString& argValue2)
{
	setArgAmount(2);
	this->argValues[0] = argValue1;
	this->argValues[1] = argValue2;
}


void Option::setArgValues(const UString& argValue1, const UString& argValue2, const UString& argValue3)
{
	setArgAmount(3);
	this->argValues[0] = argValue1;
	this->argValues[1] = argValue2;
	this->argValues[2] = argValue3;
}


void Option::setArgValues(const std::vector<UString>& argValues)
{
	setArgAmount(static_cast<int>(argValues.size()));
	this->argValues = argValues;
}


bool Option::match(const UString& opt) const
{
	if (opt.size() == 1 && this->opt.equals(opt)) {
		return true;
	}
	else if (opt.size() > 2 && this->longOpt.equals(opt)) {
		return true;
	}

	return false;
}


void Option::setArgAmount(int amount)
{
	if (amount == 0) {
		this->hasArgs = false;
	}
	else {
		this->hasArgs = true;
		this->argNames.resize(amount, "");
		this->argDefaultValues.resize(amount, "");
		this->argValues.resize(amount, "");
	}
}


} // End namespace Verso

