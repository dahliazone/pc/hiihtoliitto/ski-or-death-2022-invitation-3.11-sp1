#include <Verso/System/JsonHelper.hpp>
#include <locale>
#include <codecvt>

namespace Verso {


namespace JsonHelper {


JSONObject emptyJsonObject;
JSONArray emptyJsonArray;


JSONValue* loadAndParse(const UString& fileName)
{
	if (File::exists(fileName) == false) {
		UString error("Cannot find JSON file!");
		VERSO_FILENOTFOUND("verso-base", error.c_str(), fileName.c_str());
	}

	// Load file
	std::ifstream fileStream(fileName.c_str(), std::ifstream::in);
	std::stringstream ss;
	ss << fileStream.rdbuf();
	UString buffer(ss.str());

	// \TODO: Strip comments
	/*std::vector<UString> lines = buffer.split("\n");
	for (auto& line : lines) {
		for (int i=0; i<line.size(); ++i) {
			if (line[i] == "\"") {
				break;
			}
			else if (line[i] == "/") {
				if (i+1 < line.size() && line[i+1] == "/") {
					// remove line
				}
			}
		}
	}*/

	// Parse JSON
	JSONValue* rootValue = JSON::Parse(buffer.c_str());
	if (rootValue == nullptr) {
		UString error("Failed to parse JSON structure! Check against JSON lint.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	if (rootValue->IsObject() == false) {
		UString error("JSON root value is not an object!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	return rootValue;
}


void saveToFile(const UString& fileName, JSONValue* root, bool prettyPrint)
{
	std::ofstream ofs(fileName.c_str(), std::ofstream::out);
	std::wbuffer_convert<std::codecvt_utf8<wchar_t>> converter(ofs.rdbuf());
	std::wostream out(&converter);

	out << root->Stringify(prettyPrint);

	ofs.close();
}


void free(JSONValue* root)
{
	if (root != nullptr) {
		delete root;
	}
}


JSONValue* readValue(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required)
{
	JSONObject::const_iterator valueItr = object.find(name.toUtf8Wstring());
	if (valueItr == object.end()) {
		if (required == true) {
			UString error("Cannot read required field \""+currentPathJson+name+"\"!");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
		}
		else {
			return nullptr;
		}
	}
	return valueItr->second;
}


bool isDefined(const JSONObject& object, const UString& currentPathJson, const UString& name)
{
	JSONValue* value = JsonHelper::readValue(object, currentPathJson, name);

	if (value) {
		return true;
	}
	else {
		return false;
	}
}


bool isNull(const JSONObject& object, const UString& currentPathJson, const UString& name)
{
	JSONValue* value = JsonHelper::readValue(object, currentPathJson, name);

	if (value && value->IsNull()) {
		return true;
	}
	else {
		return false;
	}
}


bool isObject(const JSONObject& object, const UString& currentPathJson, const UString& name)
{
	JSONValue* value = JsonHelper::readValue(object, currentPathJson, name);

	if (value && value->IsObject()) {
		return true;
	}
	else {
		return false;
	}
}


const JSONObject& readObject(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required)
{
	JSONValue* value = JsonHelper::readValue(object, currentPathJson, name);

	if (value && value->IsObject()) {
		return value->AsObject();
	}
	if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}
	else {
		return emptyJsonObject;
	}
}


JSONValue* writeObject(JSONObject& jsonObject)
{
	return new JSONValue(jsonObject);
}


bool isArray(const JSONObject& object, const UString& currentPathJson, const UString& name)
{
	JSONValue* value = JsonHelper::readValue(object, currentPathJson, name);

	if (value && value->IsArray()) {
		return true;
	}
	else {
		return false;
	}
}


const JSONArray& readArray(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required)
{
	JSONValue* value = JsonHelper::readValue(object, currentPathJson, name);

	if (value && value->IsArray()) {
		return value->AsArray();
	}
	if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}
	else {
		return emptyJsonArray;
	}
}


JSONValue* writeArray(JSONArray& jsonArray)
{
	return new JSONValue(jsonArray);
}


bool isBool(const JSONObject& object, const UString& currentPathJson, const UString& name)
{
	JSONValue* value = JsonHelper::readValue(object, currentPathJson, name);

	if (value && value->IsBool()) {
		return true;
	}
	else {
		return false;
	}
}


bool readBool(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, bool defaultValue)
{
	JSONValue* value = JsonHelper::readValue(object, currentPathJson, name);
	if (value && value->IsBool()) {
		return value->AsBool();
	}
	if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}
	else {
		return defaultValue;
	}
}


JSONValue* writeBool(bool value)
{
	return new JSONValue(value);
}


bool isInt(const JSONObject& object, const UString& currentPathJson, const UString& name, bool checkForDecimalZero)
{
	JSONValue* value = JsonHelper::readValue(object, currentPathJson, name);

	if (value && value->IsNumber()) {
		double valueNumber = value->AsNumber();
		if (checkForDecimalZero == false) {
			return true;
		}
		else if (valueNumber - floor(valueNumber) == 0.0) {
			return true;
		}
		else {
			return false;
		}
	}
	else {
		return false;
	}
}


int readInt(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, int defaultValue)
{
	JSONValue* value = JsonHelper::readValue(object, currentPathJson, name);
	if (value && value->IsNumber()) {
		double valueNumber = value->AsNumber();
		if (valueNumber - floor(valueNumber) != 0.0) {
			UString warn("Field \""+currentPathJson+name+"\" should be integer but instead is ");
			warn.append2(valueNumber);
			warn += " floored to ";
			warn.append2(floor(valueNumber));
			warn += ".";
			VERSO_LOG_WARN("verso-base", warn.c_str());
			valueNumber = floor(valueNumber);
		}

		return static_cast<int>(valueNumber);
	}
	if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}
	else {
		return defaultValue;
	}
}


JSONValue* writeInt(int value)
{
	return new JSONValue(value);
}


bool isNumber(const JSONObject& object, const UString& currentPathJson, const UString& name)
{
	JSONValue* value = JsonHelper::readValue(object, currentPathJson, name);

	if (value && value->IsNumber()) {
		return true;
	}
	else {
		return false;
	}
}


double readNumberd(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, double defaultValue)
{
	JSONValue* value = JsonHelper::readValue(object, currentPathJson, name);
	if (value && value->IsNumber()) {
		return value->AsNumber();
	}
	if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}
	else {
		return defaultValue;
	}
}


JSONValue* writeNumberd(double value)
{
	return new JSONValue(value);
}


float readNumberf(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, float defaultValue)
{
	JSONValue* value = JsonHelper::readValue(object, currentPathJson, name);
	if (value && value->IsNumber()) {
		return static_cast<float>(value->AsNumber());
	}
	if (required == true) {
		UString error("Cannot read required field \"" + currentPathJson + name + "\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}
	else {
		return defaultValue;
	}
}


JSONValue* writeNumberf(float value)
{
	return new JSONValue(static_cast<double>(value));
}


std::vector<float> readNumberfArray(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const std::vector<float>& defaultValue)
{
	const JSONArray& numberArr = JsonHelper::readArray(object, currentPathJson, name, required);
	if (numberArr.size() <= 0) {
		if (required == false) {
			return defaultValue;
		}
		else {
			UString error("Cannot read required field \""+currentPathJson+name+"\"!");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
		}
	}

	std::vector<float> numbers;
	for (auto& n : numberArr) {
		if (n->IsNumber()) {
			numbers.push_back(static_cast<float>(n->AsNumber()));
		}
		else {
			UString error("Invalid type of values in field \""+currentPathJson+name+"\"! Array must contain only number values.");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
		}
	}

	return numbers;
}


int readNumberi(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, int defaultValue)
{
	JSONValue* value = JsonHelper::readValue(object, currentPathJson, name);
	if (value && value->IsNumber()) {
		return static_cast<int>(value->AsNumber());
	}
	if (required == true) {
		UString error("Cannot read required field \"" + currentPathJson + name + "\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}
	else {
		return defaultValue;
	}
}


JSONValue* writeNumberi(int value)
{
	return new JSONValue(value);
}


unsigned int readNumberu(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, unsigned int defaultValue)
{
	JSONValue* value = JsonHelper::readValue(object, currentPathJson, name);
	if (value && value->IsNumber()) {
		return static_cast<unsigned int>(value->AsNumber());
	}
	if (required == true) {
		UString error("Cannot read required field \"" + currentPathJson + name + "\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}
	else {
		return defaultValue;
	}
}


JSONValue* writeNumberu(unsigned int value)
{
	return new JSONValue(static_cast<int>(value));
}


size_t readNumbersz(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, size_t defaultValue)
{
	JSONValue* value = JsonHelper::readValue(object, currentPathJson, name);
	if (value && value->IsNumber()) {
		return static_cast<size_t>(value->AsNumber());
	}
	if (required == true) {
		UString error("Cannot read required field \"" + currentPathJson + name + "\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}
	else {
		return defaultValue;
	}
}


JSONValue* writeNumbersz(size_t value)
{
	return new JSONValue(static_cast<int>(value));
}


bool isString(const JSONObject& object, const UString& currentPathJson, const UString& name)
{
	JSONValue* value = JsonHelper::readValue(object, currentPathJson, name);

	if (value && value->IsString()) {
		return true;
	}
	else {
		return false;
	}
}


UString readString(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const UString& defaultValue)
{
	JSONValue* value = JsonHelper::readValue(object, currentPathJson, name, required);
	if (value && value->IsString()) {
		return value->AsString();
	}
	if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}
	else {
		return defaultValue;
	}
}


JSONValue* writeString(const UString& str)
{
	return new JSONValue(str.toUtf8Wstring());
}


Align readAlign(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const Align& defaultValue)
{
	JSONValue* value = JsonHelper::readValue(object, currentPathJson, name, required);
	if (value && value->IsObject()) {
		const JSONObject& alignObj = value->AsObject();

		HAlign hAlign = defaultValue.hAlign;
		UString horizontal = JsonHelper::readString(alignObj, currentPathJson+name+".", "horizontal");
		if (!horizontal.isEmpty()) {
			hAlign = stringToHAlign(horizontal);
			if (hAlign == HAlign::Undefined) {
				UString error("Unknown value ("+horizontal+") in field \""+currentPathJson+name+".horizontal\"! Must be one of: \"Left\", \"Center\", \"Right\".");
				VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
			}
		}

		VAlign vAlign = defaultValue.vAlign;
		UString vertical = JsonHelper::readString(alignObj, currentPathJson+name+".", "vertical");
		if (!vertical.isEmpty()) {
			vAlign = stringToVAlign(vertical);
			if (vAlign == VAlign::Undefined) {
				UString error("Unknown value ("+vertical+") in field \""+currentPathJson+name+".vertical\"! Must be one of: \"Top\", \"Center\", \"Bottom\".");
				VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
			}
		}

		return Align(hAlign, vAlign);
	}
	if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}
	else {
		return defaultValue;
	}
}


RefScale readRefScale(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required,  const RefScale& defaultValue)
{
	UString refScaleStr = JsonHelper::readString(object, currentPathJson, name, required, "");
	if (!refScaleStr.isEmpty()) {
		RefScale refScale = stringToRefScale(refScaleStr);
		if (refScale == RefScale::Unset) {
			UString error("Unknown value ("+refScaleStr+") in field \""+currentPathJson+name);
			error.append("\"! Must be one of: \"ImageSize\", \"ImageSize_KeepAspectRatio_FitRect\", \"ImageSize_KeepAspectRatio_FromX\", \"ImageSize_KeepAspectRatio_FromY\", ");
			error.append("\"ViewportSize\", \"ViewportSize_KeepAspectRatio_FitRect\", \"ViewportSize_KeepAspectRatio_FromX\", \"ViewportSize_KeepAspectRatio_FromY\".");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}
		return refScale;
	}
	else if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return defaultValue;

}


bool isColor(const JSONObject& object, const UString& currentPathJson, const UString& name)
{
	if (JsonHelper::isArray(object, currentPathJson, name)) {
		const JSONArray& vectorArr = JsonHelper::readArray(object, currentPathJson, name, false);
		if (vectorArr.size() == 3 &&
			vectorArr[0]->IsNumber() &&
			vectorArr[1]->IsNumber() &&
			vectorArr[2]->IsNumber())
		{
			return true;
		}
		else if (vectorArr.size() == 4 &&
			vectorArr[0]->IsNumber() &&
			vectorArr[1]->IsNumber() &&
			vectorArr[2]->IsNumber() &&
			vectorArr[3]->IsNumber())
		{
			return true;
		}
	}

	return false;
}


RgbaColorf readColor(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const RgbaColorf& defaultValue)
{
	const JSONArray& colorArr = JsonHelper::readArray(object, currentPathJson, name, required);
	if (colorArr.size() <= 0) {
		if (required == false) {
			return defaultValue;
		}
		else {
			UString error("Cannot read required field \""+currentPathJson+name+"\"!");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
		}
	}
	if (colorArr.size() != 3 && colorArr.size() != 4) {
		UString error("Invalid type of values in field \""+currentPathJson+name+"\"! RgbaColorf array must contain 3-4 float values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}

	bool typeError = false;
	float r = 0.0f, g = 0.0f, b = 0.0f, a = 1.0f;
	if (colorArr[0]->IsNumber()) {
		r = static_cast<float>(colorArr[0]->AsNumber());
	}
	else {
		typeError = true;
	}

	if (colorArr[1]->IsNumber()) {
		g = static_cast<float>(colorArr[1]->AsNumber());
	}
	else {
		typeError = true;
	}

	if (colorArr[2]->IsNumber()) {
		b = static_cast<float>(colorArr[2]->AsNumber());
	}
	else {
		typeError = true;
	}

	if (colorArr.size() == 4){
		if (colorArr[3]->IsNumber()) {
			a = static_cast<float>(colorArr[3]->AsNumber());
		}
		else {
			typeError = true;
		}
	}

	if (typeError == true) {
		UString error("Invalid type of values in field \""+currentPathJson+name+"\"! RgbaColorf array can only contain number values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}

	return RgbaColorf(r, g, b, a);
}


JSONValue* writeColor(const RgbaColorf& color, bool useAlpha)
{
	JSONArray array;
	array.push_back(writeNumberf(color.r));
	array.push_back(writeNumberf(color.g));
	array.push_back(writeNumberf(color.b));
	if (useAlpha) {
		array.push_back(writeNumberf(color.a));
	}
	return new JSONValue(array);
}


Rangei readRangei(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const Rangei& defaultValue)
{
	const JSONArray& rangeArr = JsonHelper::readArray(object, currentPathJson, name, required);
	if (rangeArr.size() <= 0) {
		if (required == false) {
			return defaultValue;
		}
		else {
			UString error("Cannot read required field \""+currentPathJson+name+"\"!");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
		}
	}
	if (rangeArr.size() != 2) {
		UString error("Invalid type of values in field \""+currentPathJson+name+"\"! Rangei array must contain 2 int values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}

	bool typeError = false;
	int a = 0, b = 0;
	if (rangeArr[0]->IsNumber()) {
		a = static_cast<int>(rangeArr[0]->AsNumber());
	}
	else {
		typeError = true;
	}

	if (rangeArr[1]->IsNumber()) {
		b = static_cast<int>(rangeArr[1]->AsNumber());
	}
	else {
		typeError = true;
	}

	if (typeError == true) {
		UString error("Invalid type of values in field \""+currentPathJson+name+"\"! Rangei array can only contain number values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}

	return Rangei(a, b);
}


JSONValue* writeRangei(const Rangei& range)
{
	JSONArray array;
	array.push_back(writeNumberi(range.minValue));
	array.push_back(writeNumberi(range.maxValue));
	return new JSONValue(array);
}


Rangeu readRangeu(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const Rangeu& defaultValue)
{
	const JSONArray& rangeArr = JsonHelper::readArray(object, currentPathJson, name, required);
	if (rangeArr.size() <= 0) {
		if (required == false) {
			return defaultValue;
		}
		else {
			UString error("Cannot read required field \""+currentPathJson+name+"\"!");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
		}
	}
	if (rangeArr.size() != 2) {
		UString error("Invalid type of values in field \""+currentPathJson+name+"\"! Rangeu array must contain 2 unsigned int values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}

	bool typeError = false;
	bool signedError = false;
	unsigned int a = 0, b = 0;
	if (rangeArr[0]->IsNumber()) {
		if (rangeArr[0]->AsNumber() < 0.0) {
			signedError = true;
		}
		else {
			a = static_cast<unsigned int>(rangeArr[0]->AsNumber());
		}
	}
	else {
		typeError = true;
	}

	if (rangeArr[1]->IsNumber()) {
		if (rangeArr[1]->AsNumber() < 0.0) {
			signedError = true;
		}
		else {
			b = static_cast<unsigned int>(rangeArr[1]->AsNumber());
		}

	}
	else {
		typeError = true;
	}

	if (typeError == true) {
		UString error("Invalid type of values in field \""+currentPathJson+name+"\"! Rangeu array can only contain number values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}

	if (signedError == true) {
		UString error("Invalid type of values in field \""+currentPathJson+name+"\"! Rangeu array can only contain unsigned (>=0) values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}

	return Rangeu(a, b);
}


JSONValue* writeRangeu(const Rangeu& range)
{
	JSONArray array;
	array.push_back(writeNumberu(range.minValue));
	array.push_back(writeNumberu(range.maxValue));
	return new JSONValue(array);
}


Rangef readRangef(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const Rangef& defaultValue)
{
	const JSONArray& rangeArr = JsonHelper::readArray(object, currentPathJson, name, required);
	if (rangeArr.size() <= 0) {
		if (required == false) {
			return defaultValue;
		}
		else {
			UString error("Cannot read required field \""+currentPathJson+name+"\"!");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
		}
	}
	if (rangeArr.size() != 2) {
		UString error("Invalid type of values in field \""+currentPathJson+name+"\"! Rangef array must contain 2 float values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}

	bool typeError = false;
	float a = 0.0f, b = 0.0f;
	if (rangeArr[0]->IsNumber()) {
		a = static_cast<float>(rangeArr[0]->AsNumber());
	}
	else {
		typeError = true;
	}

	if (rangeArr[1]->IsNumber()) {
		b = static_cast<float>(rangeArr[1]->AsNumber());
	}
	else {
		typeError = true;
	}

	if (typeError == true) {
		UString error("Invalid type of values in field \""+currentPathJson+name+"\"! Rangef array can only contain number values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}

	return Rangef(a, b);
}


JSONValue* writeRangef(const Rangef& range)
{
	JSONArray array;
	array.push_back(writeNumberf(range.minValue));
	array.push_back(writeNumberf(range.maxValue));
	return new JSONValue(array);
}


Ranged readRanged(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const Ranged& defaultValue)
{
	const JSONArray& rangeArr = JsonHelper::readArray(object, currentPathJson, name, required);
	if (rangeArr.size() <= 0) {
		if (required == false) {
			return defaultValue;
		}
		else {
			UString error("Cannot read required field \""+currentPathJson+name+"\"!");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
		}
	}
	if (rangeArr.size() != 2) {
		UString error("Invalid type of values in field \""+currentPathJson+name+"\"! Ranged array must contain 2 double values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}

	bool typeError = false;
	double a = 0.0, b = 0.0;
	if (rangeArr[0]->IsNumber()) {
		a = rangeArr[0]->AsNumber();
	}
	else {
		typeError = true;
	}

	if (rangeArr[1]->IsNumber()) {
		b = rangeArr[1]->AsNumber();
	}
	else {
		typeError = true;
	}

	if (typeError == true) {
		UString error("Invalid type of values in field \""+currentPathJson+name+"\"! Ranged array can only contain number values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}

	return Ranged(a, b);
}


JSONValue* writeRanged(const Ranged& range)
{
	JSONArray array;
	array.push_back(writeNumberd(range.minValue));
	array.push_back(writeNumberd(range.maxValue));
	return new JSONValue(array);
}


Rectf readRectf(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const Rectf& defaultValue)
{
	JSONValue* value = JsonHelper::readValue(object, currentPathJson, name, required);
	if (value && value->IsObject()) {
		const JSONObject& rectObj = value->AsObject();

		float x = readNumberf(rectObj, currentPathJson+name+".", "x", false);
		float y = readNumberf(rectObj, currentPathJson+name+".", "y", false);
		float width = readNumberf(rectObj, currentPathJson+name+".", "width", false);
		float height = readNumberf(rectObj, currentPathJson+name+".", "height", false);

		return Rectf(x, y, width, height);
	}
	if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}
	else {
		return defaultValue;
	}
}


Vector2f readVector2f(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const Vector2f& defaultValue)
{
	JSONValue* value = JsonHelper::readValue(object, currentPathJson, name, required);
	if (value && value->IsObject()) {
		const JSONObject& rectObj = value->AsObject();

		float x = readNumberf(rectObj, currentPathJson+name+".", "x", false);
		float y = readNumberf(rectObj, currentPathJson+name+".", "y", false);

		return Vector2f(x, y);
	}
	if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}
	else {
		return defaultValue;
	}
}


bool isVector2fArrayFormat(const JSONObject& object, const UString& currentPathJson, const UString& name)
{
	if (JsonHelper::isArray(object, currentPathJson, name)) {
		const JSONArray& vectorArr = JsonHelper::readArray(object, currentPathJson, name, false);
		if (vectorArr.size() == 2 &&
			vectorArr[0]->IsNumber() &&
			vectorArr[1]->IsNumber())
		{
			return true;
		}
	}

	return false;
}


Vector2f readVector2fArrayFormat(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const Vector2f& defaultValue)
{
	if (JsonHelper::readValue(object, currentPathJson, name) == nullptr) {
		return defaultValue;
	}

	const JSONArray& vectorArr = JsonHelper::readArray(object, currentPathJson, name, required);
	if (vectorArr.size() <= 0) {
		if (required == false) {
			UString error("Cannot read optional field \""+currentPathJson+name+"\": Must be of type [x, y]");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
		}
		else {
			UString error("Cannot read required field \""+currentPathJson+name+"\": Must be of type [x, y]");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
		}
	}
	if (vectorArr.size() != 2) {
		UString error("Invalid type of values in field \""+currentPathJson+name+"\": Must be of type [x, y]");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}

	bool typeError = false;
	float x = 0.0f, y = 0.0f;
	if (vectorArr[0]->IsNumber()) {
		x = static_cast<float>(vectorArr[0]->AsNumber());
	}
	else {
		typeError = true;
	}

	if (vectorArr[1]->IsNumber()) {
		y = static_cast<float>(vectorArr[1]->AsNumber());
	}
	else {
		typeError = true;
	}

	if (typeError == true) {
		UString error("Invalid type of values in field \""+currentPathJson+name+"\"! Vector2f array must contain only number values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}

	return Vector2f(x, y);
}


Vector2i readVector2iArrayFormat(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const Vector2i& defaultValue)
{
	if (JsonHelper::readValue(object, currentPathJson, name) == nullptr) {
		return defaultValue;
	}

	const JSONArray& vectorArr = JsonHelper::readArray(object, currentPathJson, name, required);
	if (vectorArr.size() <= 0) {
		if (required == false) {
			UString error("Cannot read optional field \""+currentPathJson+name+"\": Must be of type [x, y]");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
		}
		else {
			UString error("Cannot read required field \""+currentPathJson+name+"\": Must be of type [x, y]");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
		}
	}
	if (vectorArr.size() != 2) {
		UString error("Invalid type of values in field \""+currentPathJson+name+"\": Must be of type [x, y]");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}

	bool typeError = false;
	float x = 0, y = 0;
	if (vectorArr[0]->IsNumber()) {
		x = static_cast<float>(vectorArr[0]->AsNumber());
		if (x - floorf(x) != 0.0f) {
			UString warn("Field \""+currentPathJson+name+"[0]\" should be integer but instead is ");
			warn.append2(x);
			warn += " floored to ";
			warn.append2(floorf(x));
			warn += ".";
			VERSO_LOG_WARN("verso-base", warn.c_str());
			x = floorf(x);
		}
	}
	else {
		typeError = true;
	}

	if (vectorArr[1]->IsNumber()) {
		y = static_cast<float>(vectorArr[1]->AsNumber());
		if (y - floorf(y) != 0.0f) {
			UString warn("Field \""+currentPathJson+name+"[1]\" should be integer but instead is ");
			warn.append2(y);
			warn += " floored to ";
			warn.append2(floorf(y));
			warn += ".";
			VERSO_LOG_WARN("verso-base", warn.c_str());
			y = floorf(y);
		}
	}
	else {
		typeError = true;
	}

	if (typeError == true) {
		UString error("Invalid type of values in field \""+currentPathJson+name+"\"! Vector2f array must contain only number values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}

	return Vector2i(static_cast<int>(x), static_cast<int>(y));
}


Vector2i readVector2i(const JSONObject& object, const UString& currentPathJson,const UString& name, bool required, const Vector2i& defaultValue)
{
	JSONValue* value = JsonHelper::readValue(object, currentPathJson, name, required);
	if (value && value->IsObject()) {
		const JSONObject& rectObj = value->AsObject();

		float x = readNumberf(rectObj, currentPathJson+name+".", "x", false);
		float y = readNumberf(rectObj, currentPathJson+name+".", "y", false);

		if (x - floorf(x) != 0.0f) {
			UString warn("Field \""+currentPathJson+name+".x\" should be integer but instead is ");
			warn.append2(x);
			warn += " floored to ";
			warn.append2(floorf(x));
			warn += ".";
			VERSO_LOG_WARN("verso-base", warn.c_str());
			x = floorf(x);
		}

		if (y - floorf(y) != 0.0f) {
			UString warn("Field \""+currentPathJson+name+".y\" should be integer but instead is ");
			warn.append2(y);
			warn += " floored to ";
			warn.append2(floorf(y));
			warn += ".";
			VERSO_LOG_WARN("verso-base", warn.c_str());
			y = floorf(y);
		}

		return Vector2i(static_cast<int>(x), static_cast<int>(y));
	}
	if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}
	else {
		return defaultValue;
	}
}


Vector3f readVector3f(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const Vector3f& defaultValue)
{
	JSONValue* value = JsonHelper::readValue(object, currentPathJson, name, required);
	if (value && value->IsObject()) {
		const JSONObject& rectObj = value->AsObject();

		float x = readNumberf(rectObj, currentPathJson+name+".", "x", false);
		float y = readNumberf(rectObj, currentPathJson+name+".", "y", false);
		float z = readNumberf(rectObj, currentPathJson+name+".", "z", false);

		return Vector3f(x, y, z);
	}
	if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}
	else {
		return defaultValue;
	}
}


bool isVector3fArrayFormat(const JSONObject& object, const UString& currentPathJson, const UString& name)
{
	if (JsonHelper::isArray(object, currentPathJson, name)) {
		const JSONArray& vectorArr = JsonHelper::readArray(object, currentPathJson, name, false);
		if (vectorArr.size() == 3 &&
			vectorArr[0]->IsNumber() &&
			vectorArr[1]->IsNumber() &&
			vectorArr[2]->IsNumber())
		{
			return true;
		}
	}

	return false;
}


Vector3f readVector3fArrayFormat(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const Vector3f& defaultValue)
{
	if (JsonHelper::readValue(object, currentPathJson, name) == nullptr) {
		return defaultValue;
	}

	const JSONArray& vectorArr = JsonHelper::readArray(object, currentPathJson, name, required);
	if (vectorArr.size() <= 0) {
		if (required == false) {
			UString error("Cannot read value for optional field \""+currentPathJson+name+"\": must be of type [x, y, z]");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
		}
		else {
			UString error("Cannot read required field \""+currentPathJson+name+"\":  must be of type [x, y, z]");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
		}
	}
	if (vectorArr.size() != 3) {
		UString error("Invalid type of values in field \""+currentPathJson+name+"\": must be of type [x, y, z]");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}

	bool typeError = false;
	float x = 0.0f, y = 0.0f, z = 0.0f;
	if (vectorArr[0]->IsNumber()) {
		x = static_cast<float>(vectorArr[0]->AsNumber());
	}
	else {
		typeError = true;
	}

	if (vectorArr[1]->IsNumber()) {
		y = static_cast<float>(vectorArr[1]->AsNumber());
	}
	else {
		typeError = true;
	}

	if (vectorArr[2]->IsNumber()) {
		z = static_cast<float>(vectorArr[2]->AsNumber());
	}
	else {
		typeError = true;
	}

	if (typeError == true) {
		UString error("Invalid type of values in field \""+currentPathJson+name+"\"! Vector3f array must contain only number values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}

	return Vector3f(x, y, z);
}


AspectRatio readAspectRatio(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const AspectRatio& defaultValue)
{
	UString aspectRatio = readString(object, currentPathJson, name, required, "");
	if (aspectRatio.isEmpty()) {
		return defaultValue;
	}
	else {
		return AspectRatio(aspectRatio);
	}
}


PixelAspectRatio readPixelAspectRatio(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const PixelAspectRatio& defaultValue)
{
	UString pixelAspectRatio = readString(object, currentPathJson, name, required, "");
	if (pixelAspectRatio.isEmpty()) {
		return defaultValue;
	}
	else {
		return PixelAspectRatio(pixelAspectRatio);
	}
}


EasingType readEasingType(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const EasingType& defaultValue)
{
	UString easingTypeStr = JsonHelper::readString(object, currentPathJson, name, required, "");
	if (!easingTypeStr.isEmpty()) {
		EasingType easingType = stringToEasingType(easingTypeStr);
		if (easingType == EasingType::Undefined) {
			UString error("Unknown value ("+easingTypeStr+") in field \""+currentPathJson+name+"\"! Must be one of: \"Linear\", \"EaseIn\", \"EaseOut\", \"EaseInOut\", \"QuadraticIn\", \"QuadraticOut\", \"QuadraticInOut\".");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}
		return easingType;
	}
	else if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return defaultValue;
}


InterpolationType readInterpolationType(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const InterpolationType& defaultValue)
{
	UString interpolationTypeStr = JsonHelper::readString(object, currentPathJson, name, required, "");
	if (!interpolationTypeStr.isEmpty()) {
		InterpolationType interpolationType = stringToInterpolationType(interpolationTypeStr);
		if (interpolationType == InterpolationType::Undefined) {
			UString error("Unknown value ("+interpolationTypeStr+") in field \""+currentPathJson+name+"\"! Must be one of: \"Constant\", \"Linear\", \"QuadraticBezier\", \"CubicBezier\", \"QuinticBezier\", \"CubicHermiteSpline\".");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}
		return interpolationType;
	}
	else if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return defaultValue;
}


IntKeyframes readIntKeyframes(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const IntKeyframes& defaultValue)
{
	if (JsonHelper::isNumber(object, currentPathJson, name)) {
		return IntKeyframes(JsonHelper::readNumberf(object, currentPathJson, name, required));
	}

	else if (JsonHelper::isObject(object, currentPathJson, name)) {
		const JSONObject& keyframeObj = JsonHelper::readObject(object, currentPathJson, name, required);
		UString currentPathJsonKeyframeObj = currentPathJson+name+".";

		IntKeyframes keyframes;
		keyframes.easingType = JsonHelper::readEasingType(keyframeObj, currentPathJsonKeyframeObj, "easingType");
		keyframes.interpolationType = JsonHelper::readInterpolationType(keyframeObj, currentPathJsonKeyframeObj, "interpolationType");
		keyframes.looping = JsonHelper::readBool(keyframeObj, currentPathJsonKeyframeObj, "looping", false, false);

		if (JsonHelper::isArray(keyframeObj, currentPathJsonKeyframeObj, "keyframes")) {
			const JSONArray& keyframesArr = JsonHelper::readArray(keyframeObj, currentPathJsonKeyframeObj, "keyframes", required);
			UString currentPathJsonKeyframesArr = currentPathJsonKeyframeObj + "keyframes.";

			if (!keyframesArr.empty()) {
				// Check that there's at least 2 values
				if (keyframesArr.size() < 2) {
					UString error("Invalid data in field \""+currentPathJsonKeyframesArr+"\"! IntKeyframes array must contain at least two entries.");
					VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
				}

				int i = 0;
				for (JSONArray::const_iterator elementIt = keyframesArr.begin(); elementIt != keyframesArr.end(); ++elementIt) {
					if ((*elementIt)->IsObject()) {
						JSONObject elementObj = (*elementIt)->AsObject();
						UString currentPathJsonKeyframesArrEl(currentPathJsonKeyframesArr);
						currentPathJsonKeyframesArrEl += "[" ;
						currentPathJsonKeyframesArrEl.append2(i);
						currentPathJsonKeyframesArrEl += "].";

						float time = JsonHelper::readNumberf(elementObj, currentPathJsonKeyframesArrEl, "time", true);
						int value = JsonHelper::readNumberi(elementObj, currentPathJsonKeyframesArrEl, "value", true);
						int control1 = JsonHelper::readNumberi(elementObj, currentPathJsonKeyframesArrEl, "control1", false);
						int control2 = JsonHelper::readNumberi(elementObj, currentPathJsonKeyframesArrEl, "control2", false);
						keyframes.add(IntKeyframeElement(time, value, control1, control2));
					}
					else {
						UString error("Invalid data in field \""+currentPathJsonKeyframesArr+"\"! IntKeyframes array must contain objects like { \"delta\": <number>, \"value\": [<number>, <number>] }!");
						VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
					}
					i++;
				}
				return keyframes;
			}
			else if (required == true) {
				UString error("Invalid data in field \""+currentPathJsonKeyframeObj+".keyframes\". Cannot be empty!");
				VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
			}
		}
	}

	else if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return defaultValue;
}


FloatKeyframes readFloatKeyframes(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const FloatKeyframes& defaultValue)
{
	if (JsonHelper::isNumber(object, currentPathJson, name)) {
		return FloatKeyframes(JsonHelper::readNumberf(object, currentPathJson, name, required));
	}

	else if (JsonHelper::isObject(object, currentPathJson, name)) {
		const JSONObject& keyframeObj = JsonHelper::readObject(object, currentPathJson, name, required);
		UString currentPathJsonKeyframeObj = currentPathJson+name+".";

		FloatKeyframes keyframes;
		keyframes.easingType = JsonHelper::readEasingType(keyframeObj, currentPathJsonKeyframeObj, "easingType");
		keyframes.interpolationType = JsonHelper::readInterpolationType(keyframeObj, currentPathJsonKeyframeObj, "interpolationType");
		keyframes.looping = JsonHelper::readBool(keyframeObj, currentPathJsonKeyframeObj, "looping", false, false);

		if (JsonHelper::isArray(keyframeObj, currentPathJsonKeyframeObj, "keyframes")) {
			const JSONArray& keyframesArr = JsonHelper::readArray(keyframeObj, currentPathJsonKeyframeObj, "keyframes", required);
			UString currentPathJsonKeyframesArr = currentPathJsonKeyframeObj + "keyframes.";

			if (!keyframesArr.empty()) {
				// Check that there's at least 2 values
				if (keyframesArr.size() < 2) {
					UString error("Invalid data in field \""+currentPathJsonKeyframesArr+"\"! FloatKeyframes array must contain at least two entries.");
					VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
				}

				int i = 0;
				for (JSONArray::const_iterator elementIt = keyframesArr.begin(); elementIt != keyframesArr.end(); ++elementIt) {
					if ((*elementIt)->IsObject()) {
						JSONObject elementObj = (*elementIt)->AsObject();
						UString currentPathJsonKeyframesArrEl(currentPathJsonKeyframesArr);
						currentPathJsonKeyframesArrEl += "[" ;
						currentPathJsonKeyframesArrEl.append2(i);
						currentPathJsonKeyframesArrEl += "].";

						float time = JsonHelper::readNumberf(elementObj, currentPathJsonKeyframesArrEl, "time", true);
						float value = JsonHelper::readNumberf(elementObj, currentPathJsonKeyframesArrEl, "value", true);
						float control1 = JsonHelper::readNumberf(elementObj, currentPathJsonKeyframesArrEl, "control1", false);
						float control2 = JsonHelper::readNumberf(elementObj, currentPathJsonKeyframesArrEl, "control2", false);
						keyframes.add(FloatKeyframeElement(time, value, control1, control2));
					}
					else {
						UString error("Invalid data in field \""+currentPathJsonKeyframesArr+"\"! FloatKeyframes array must contain objects like { \"delta\": <number>, \"value\": [<number>, <number>] }!");
						VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
					}
					i++;
				}
				return keyframes;
			}
			else if (required == true) {
				UString error("Invalid data in field \""+currentPathJsonKeyframeObj+".keyframes\". Cannot be empty!");
				VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
			}
		}
	}

	else if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return defaultValue;
}


Vector2fKeyframes readVector2fKeyframes(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const Vector2fKeyframes& defaultValue)
{
	if (JsonHelper::isVector2fArrayFormat(object, currentPathJson, name)) {
		return Vector2fKeyframes(JsonHelper::readVector2fArrayFormat(object, currentPathJson, name, required));
	}

	else if (JsonHelper::isObject(object, currentPathJson, name)) {
		const JSONObject& keyframeObj = JsonHelper::readObject(object, currentPathJson, name, required);
		UString currentPathJsonKeyframeObj = currentPathJson+name+".";

		Vector2fKeyframes keyframes;
		keyframes.easingType = JsonHelper::readEasingType(keyframeObj, currentPathJsonKeyframeObj, "easingType");
		keyframes.interpolationType = JsonHelper::readInterpolationType(keyframeObj, currentPathJsonKeyframeObj, "interpolationType");
		keyframes.looping = JsonHelper::readBool(keyframeObj, currentPathJsonKeyframeObj, "looping", false, false);

		if (JsonHelper::isArray(keyframeObj, currentPathJsonKeyframeObj, "keyframes")) {
			const JSONArray& keyframesArr = JsonHelper::readArray(keyframeObj, currentPathJsonKeyframeObj, "keyframes", required);
			UString currentPathJsonKeyframesArr = currentPathJsonKeyframeObj + "keyframes.";

			if (!keyframesArr.empty()) {
				// Check that there's at least 2 values
				if (keyframesArr.size() < 2) {
					UString error("Invalid data in field \""+currentPathJsonKeyframesArr+"\"! Vector2fKeyframes array must contain at least two entries.");
					VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
				}

				int i = 0;
				for (JSONArray::const_iterator elementIt = keyframesArr.begin(); elementIt != keyframesArr.end(); ++elementIt) {
					if ((*elementIt)->IsObject()) {
						JSONObject elementObj = (*elementIt)->AsObject();
						UString currentPathJsonKeyframesArrEl(currentPathJsonKeyframesArr);
						currentPathJsonKeyframesArrEl += "[" ;
						currentPathJsonKeyframesArrEl.append2(i);
						currentPathJsonKeyframesArrEl += "].";

						float time = JsonHelper::readNumberf(elementObj, currentPathJsonKeyframesArrEl, "time", true);
						Vector2f value = JsonHelper::readVector2fArrayFormat(elementObj, currentPathJsonKeyframesArrEl, "value", true);
						Vector2f control1 = JsonHelper::readVector2fArrayFormat(elementObj, currentPathJsonKeyframesArrEl, "control1", false);
						Vector2f control2 = JsonHelper::readVector2fArrayFormat(elementObj, currentPathJsonKeyframesArrEl, "control2", false);
						keyframes.add(Vector2fKeyframeElement(time, value, control1, control2));
					}
					else {
						UString error("Invalid data in field \""+currentPathJsonKeyframesArr+"\"! Vector2fKeyframes array must contain objects like { \"delta\": <number>, \"value\": [<number>, <number>] }!");
						VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
					}
					i++;
				}
				return keyframes;
			}
			else if (required == true) {
				UString error("Invalid data in field \""+currentPathJsonKeyframeObj+".keyframes\". Cannot be empty!");
				VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
			}
		}
	}

	else if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return defaultValue;
}


Vector3fKeyframes readVector3fKeyframes(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const Vector3fKeyframes& defaultValue)
{
	if (JsonHelper::isVector3fArrayFormat(object, currentPathJson, name)) {
		return Vector3fKeyframes(JsonHelper::readVector3fArrayFormat(object, currentPathJson, name, required));
	}

	else if (JsonHelper::isObject(object, currentPathJson, name)) {
		const JSONObject& keyframeObj = JsonHelper::readObject(object, currentPathJson, name, required);
		UString currentPathJsonKeyframeObj = currentPathJson+name+".";

		Vector3fKeyframes keyframes;
		keyframes.easingType = JsonHelper::readEasingType(keyframeObj, currentPathJsonKeyframeObj, "easingType");
		keyframes.interpolationType = JsonHelper::readInterpolationType(keyframeObj, currentPathJsonKeyframeObj, "interpolationType");
		keyframes.looping = JsonHelper::readBool(keyframeObj, currentPathJsonKeyframeObj, "looping", false, false);

		if (JsonHelper::isArray(keyframeObj, currentPathJsonKeyframeObj, "keyframes")) {
			const JSONArray& keyframesArr = JsonHelper::readArray(keyframeObj, currentPathJsonKeyframeObj, "keyframes", required);
			UString currentPathJsonKeyframesArr = currentPathJsonKeyframeObj + "keyframes.";

			if (!keyframesArr.empty()) {
				// Check that there's at least 2 values
				if (keyframesArr.size() < 2) {
					UString error("Invalid data in field \""+currentPathJsonKeyframesArr+"\"! Vector3fKeyframes array must contain at least two entries.");
					VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
				}

				int i = 0;
				for (JSONArray::const_iterator elementIt = keyframesArr.begin(); elementIt != keyframesArr.end(); ++elementIt) {
					if ((*elementIt)->IsObject()) {
						JSONObject elementObj = (*elementIt)->AsObject();
						UString currentPathJsonKeyframesArrEl(currentPathJsonKeyframesArr);
						currentPathJsonKeyframesArrEl += "[" ;
						currentPathJsonKeyframesArrEl.append2(i);
						currentPathJsonKeyframesArrEl += "].";

						float time = JsonHelper::readNumberf(elementObj, currentPathJsonKeyframesArrEl, "time", true);
						Vector3f value = JsonHelper::readVector3fArrayFormat(elementObj, currentPathJsonKeyframesArrEl, "value", true);
						Vector3f control1 = JsonHelper::readVector3fArrayFormat(elementObj, currentPathJsonKeyframesArrEl, "control1", false);
						Vector3f control2 = JsonHelper::readVector3fArrayFormat(elementObj, currentPathJsonKeyframesArrEl, "control2", false);
						keyframes.add(Vector3fKeyframeElement(time, value, control1, control2));
					}
					else {
						UString error("Invalid data in field \""+currentPathJsonKeyframesArr+"\"! Vector3fKeyframes array must contain objects like { \"delta\": <number>, \"value\": [<number>, <number>, <number>] }!");
						VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
					}
					i++;
				}
				return keyframes;
			}
			else if (required == true) {
				UString error("Invalid data in field \""+currentPathJsonKeyframeObj+".keyframes\". Cannot be empty!");
				VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
			}
		}
	}

	else if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return defaultValue;
}


RgbaColorfKeyframes readRgbaColorfKeyframes(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const RgbaColorfKeyframes& defaultValue)
{
	if (JsonHelper::isColor(object, currentPathJson, name)) {
		return RgbaColorfKeyframes(JsonHelper::readColor(object, currentPathJson, name, required));
	}

	else if (JsonHelper::isObject(object, currentPathJson, name)) {
		const JSONObject& keyframeObj = JsonHelper::readObject(object, currentPathJson, name, required);
		UString currentPathJsonKeyframeObj = currentPathJson+name+".";

		RgbaColorfKeyframes keyframes;
		keyframes.easingType = JsonHelper::readEasingType(keyframeObj, currentPathJsonKeyframeObj, "easingType");
		keyframes.interpolationType = JsonHelper::readInterpolationType(keyframeObj, currentPathJsonKeyframeObj, "interpolationType");
		keyframes.looping = JsonHelper::readBool(keyframeObj, currentPathJsonKeyframeObj, "looping", false, false);

		if (JsonHelper::isArray(keyframeObj, currentPathJsonKeyframeObj, "keyframes")) {
			const JSONArray& keyframesArr = JsonHelper::readArray(keyframeObj, currentPathJsonKeyframeObj, "keyframes", required);
			UString currentPathJsonKeyframesArr = currentPathJsonKeyframeObj + "keyframes.";

			if (!keyframesArr.empty()) {
				// Check that there's at least 2 values
				if (keyframesArr.size() < 2) {
					UString error("Invalid data in field \""+currentPathJsonKeyframesArr+"\"! RgbaColorfKeyframes array must contain at least two entries.");
					VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
				}

				int i = 0;
				for (JSONArray::const_iterator elementIt = keyframesArr.begin(); elementIt != keyframesArr.end(); ++elementIt) {
					if ((*elementIt)->IsObject()) {
						JSONObject elementObj = (*elementIt)->AsObject();
						UString currentPathJsonKeyframesArrEl(currentPathJsonKeyframesArr);
						currentPathJsonKeyframesArrEl += "[" ;
						currentPathJsonKeyframesArrEl.append2(i);
						currentPathJsonKeyframesArrEl += "].";

						float time = JsonHelper::readNumberf(elementObj, currentPathJsonKeyframesArrEl, "time", true);
						RgbaColorf value = JsonHelper::readColor(elementObj, currentPathJsonKeyframesArrEl, "value", true);
						RgbaColorf control1 = JsonHelper::readColor(elementObj, currentPathJsonKeyframesArrEl, "control1", false);
						RgbaColorf control2 = JsonHelper::readColor(elementObj, currentPathJsonKeyframesArrEl, "control2", false);
						keyframes.add(RgbaColorfKeyframeElement(time, value, control1, control2));
					}
					else {
						UString error("Invalid data in field \""+currentPathJsonKeyframesArr+"\"! RgbaColorfKeyframes array must contain objects like { \"delta\": <number>, \"value\": [<number>, <number>, <number>] }!");
						VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
					}
					i++;
				}
				return keyframes;
			}
			else if (required == true) {
				UString error("Invalid data in field \""+currentPathJsonKeyframeObj+".keyframes\". Cannot be empty!");
				VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
			}
		}
	}

	else if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return defaultValue;
}


CameraKeyframes readCameraKeyframes(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required, const CameraKeyframes& defaultValue)
{
	const JSONArray& cameraKeyframesArr = JsonHelper::readArray(object, currentPathJson, name, required);
	if (!cameraKeyframesArr.empty()) {
		CameraKeyframes cameraKeyframes;

		// Check that there's at least 2 values
		if (cameraKeyframesArr.size() < 2) {
			UString error("Invalid data in field \""+currentPathJson+name+"\"! CameraKeyframes array must contain at least two entries.");
			VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
		}

		for (JSONArray::const_iterator elementIt = cameraKeyframesArr.begin(); elementIt != cameraKeyframesArr.end(); ++elementIt) {
			if ((*elementIt) && (*elementIt)->IsObject()) {
				JSONObject elementObj = (*elementIt)->AsObject();
				Vector3f position = JsonHelper::readVector3fArrayFormat(elementObj, currentPathJson+name+".", "position", true);
				Vector3f direction = JsonHelper::readVector3fArrayFormat(elementObj, currentPathJson+name+".", "direction", true);
				Vector3f up = JsonHelper::readVector3fArrayFormat(elementObj, currentPathJson+name+".", "up", true);
				float time = JsonHelper::readNumberf(elementObj, currentPathJson+name+".", "time", true);
				cameraKeyframes.add(CameraKeyframeElement(time, position, direction, up));
			}
		}
		return cameraKeyframes;
	}
	else if (required == true) {
		UString error("Cannot read required field \""+currentPathJson+name+"\"!");
		VERSO_ILLEGALFORMAT("verso-3d", error.c_str(), "");
	}

	return defaultValue;
}


} // End namespace JsonHelper


} // End namespace Verso

