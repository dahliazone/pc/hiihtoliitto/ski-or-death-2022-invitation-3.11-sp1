char FileLinux_symbol = 0;

#if __linux__

#include <Verso/System/File.hpp>
#include <Verso/System/linux/FileLinux.hpp>
#include <Verso/System/Exception.hpp>
#include <linux/limits.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <dirent.h>

namespace Verso {
namespace priv {


UString FileLinux::readSymLink(const UString& symLink)
{
	char buffer[PATH_MAX];
	ssize_t len = ::readlink(symLink.c_str(), buffer, sizeof(buffer)-1);
	if (len != -1) {
		buffer[len] = '\0';
		return UString(buffer);
	}
	else {
		VERSO_IOERROR("verso-base", "Cannot read given symbolic link", symLink.c_str());
	}
}


UString FileLinux::getBasePath()
{
	UString result;

	// Check for /proc filesystem
	if (access("/proc", F_OK) == 0) {
		UString procPath("/proc/");
		procPath.append2(static_cast<unsigned long long>(getpid()));
		procPath += "/exe";
		result = readSymLink(procPath);
	}
	else {
		VERSO_ERROR("verso-base", "Cannot access /proc", "");
	}

	if (result.isEmpty() == false) {
		// Remove file name from the result
		std::size_t pos = result.findLastOf("/");
		if (pos != std::string::npos) {
			result = result.substring(0, pos+1);
		}
		else {
			VERSO_ERROR("verso-base", "Cannot find any '/' in absolute path. This should not happen.", "");
		}
	}

	return result;
}


UString FileLinux::getPreferencesPath()
{
	return getBasePath();
}


bool FileLinux::mkdir(const UString& fileName)
{
	struct stat st;
	if (::stat(fileName.c_str(), &st) == -1) {
		return ::mkdir(fileName.c_str(), 0700);
	}
	return true;
}


void FileLinux::ls(const UString& directoryName, bool showFiles, bool showDirectories, bool showDotFiles, std::vector<UString>& out)
{
	DIR* dir = opendir(directoryName.c_str());
	if (dir == nullptr) {
		VERSO_FILENOTFOUND("verso-base", "Cannot open directory", directoryName.c_str());
	}

	struct dirent* ent;
	struct stat st;
	while ((ent = readdir(dir)) != nullptr) {
		UString fileName = ent->d_name;

		// Hide files beginning with '.'
		if (showDotFiles == false && fileName[0] == '.') {
			continue;
		}

		UString fullFileName = directoryName;
		fullFileName += "/";
		fullFileName += fileName;
		if (stat(fullFileName.c_str(), &st) == -1) {
			continue;
		}

		bool isDirectory = (st.st_mode & S_IFDIR) != 0;

		// Hide files if requested not to be shown
		if (showFiles == false && isDirectory == false) {
			continue;
		}

		// Hide directories if requested not to be shown
		if (showDirectories == false && isDirectory == true) {
			continue;
		}

		out.push_back(fileName);
	}

	closedir(dir);
}


int64_t FileLinux::getSize(const char* fileName)
{
	// \TODO: stat based 64-bit file size
	//struct __stat64 stat_buf;
	//int rc = _stat64(fileName, &stat_buf);
	//return rc == 0 ? stat_buf.st_size : -1;

	std::ifstream in(fileName, std::ifstream::ate | std::ifstream::binary);
	std::ifstream::pos_type streamPos = in.tellg();
	if (streamPos == std::ifstream::pos_type(-1)) {
		UString message("Cannot get size of non-existing file: \"");
		message.append(fileName);
		message += "\"";
		VERSO_FILENOTFOUND("verso-base", message.c_str(), fileName);
	}
	in.close();
	return static_cast<int64_t>(streamPos);
}


} // End namespace priv
} // End namespace Verso


#endif // End #ifdef __linux__


