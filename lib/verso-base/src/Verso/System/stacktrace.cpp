#include <Verso/System/stacktrace.hpp>
#include <Verso/System/Logger.hpp>
#include <iomanip>
#if __linux__|| (__APPLE__ && __MACH__)
#include <execinfo.h>
#include <cxxabi.h>
#endif

namespace Verso {


void logErrStacktrace(bool printTitle)
{
	if (printTitle == true) {
		VERSO_LOG_ERR("verso-base", "Stack trace:");
	}
#if __linux__ || (__APPLE__ && __MACH__)
	VERSO_LOG_ERR_DIRECT("verso-base", getStacktrace(false));
	VERSO_LOG_ERR_EMPTY_ROW();
	VERSO_LOG_FLUSH();
#endif
}


UString getStacktrace(bool printTitle)
{
#if (__APPLE__ && __MACH__)
	const size_t bufferSize = 1024;
	void* buffer[bufferSize];
	int count = backtrace(buffer, bufferSize);
	char** symbols = backtrace_symbols(buffer, count);

	int lineNumberColumnSize = 2;
	int moduleColumnSize = 2;
	for (int i = 0; i < count; ++i) {
		char moduleName[1024] = {};
		sscanf(symbols[i], "%*d %s", moduleName);

		int moduleLen = static_cast<int>(strlen(moduleName));
		if (moduleLen + 1 > moduleColumnSize) {
			moduleColumnSize = moduleLen + 1;
		}
	}

	UString str;
	if (printTitle == true) {
		str += "Stack trace:\n";
	}
	for (int i = 0; i < count; ++i) {
		int lineNumber = 0;
		char moduleName[1024] = {};
		char functionSymbol[1024] = {};
		sscanf(symbols[i], "%d %s %*s %s %*s", &lineNumber, moduleName, functionSymbol);

		int validCppName = 0;
		char* functionName = abi::__cxa_demangle(functionSymbol, nullptr, nullptr, &validCppName);

		std::stringstream ss;
		ss << " " << std::setw(lineNumberColumnSize) << std::right << lineNumber;
		ss << ": " << std::setw(moduleColumnSize) << std::left << moduleName;
		if (validCppName == 0) {
			ss << " " << functionName;
		}
		else {
			ss << " " << functionSymbol;
		}
		ss << std::endl;

		free(functionName);

		str.append(ss.str());
	}
	free(symbols);

	return str;
#elif __linux__
	const size_t bufferSize = 1024;
	void* buffer[bufferSize];
	int count = backtrace(buffer, bufferSize);
	char** symbols = backtrace_symbols(buffer, count);

	int moduleColumnSize = 2;
	int moduleColumnMaxSize = 25;
	for (int i = 0; i < count; ++i) {
		UString symbol(symbols[i]);
		size_t pos = symbol.findFirstOf('(');

		int moduleLen = 1;
		if (pos != std::string::npos) {
			moduleLen = pos;
		}
		if (moduleLen + 1 > moduleColumnSize) {
			moduleColumnSize = moduleLen + 1;
			if (moduleColumnSize > moduleColumnMaxSize) {
				moduleColumnSize = moduleColumnMaxSize;
				break;
			}
		}
	}

	UString str;
	if (printTitle == true) {
		str += "Stack trace:\n";
	}
	for (int i = 0; i < count; ++i) {
		UString symbol(symbols[i]);
		size_t moduleEndPos = symbol.findFirstOf('(');
		size_t functionSymbolEndPos = symbol.findFirstOf('+', moduleEndPos);
		size_t functionSymbolOffsetEndPos = symbol.findFirstOf(')', functionSymbolEndPos);
		size_t memLocationStartPos = symbol.findFirstOf('[', functionSymbolOffsetEndPos);
		size_t memLocationEndPos = symbol.findFirstOf(']', memLocationStartPos);

		UString moduleName(symbol.substring(0, moduleEndPos));
		if (static_cast<int>(moduleName.size()) > moduleColumnSize - 1) {
			moduleName = moduleName.substring(moduleName.size() - moduleColumnSize);
		}
		UString functionSymbol(symbol.substring(moduleEndPos + 1, functionSymbolEndPos - moduleEndPos - 1));
		char functionSymbolC[1024] = {};
		strcpy(functionSymbolC, functionSymbol.c_str());
		UString functionSymbolOffset(symbol.substring(functionSymbolEndPos + 1, functionSymbolOffsetEndPos - functionSymbolEndPos - 1));
		UString memLocation(symbol.substring(memLocationStartPos + 1, memLocationEndPos - memLocationStartPos - 1));

		int validCppName = 0;
		char* functionName = abi::__cxa_demangle(functionSymbolC, nullptr, nullptr, &validCppName);

		std::stringstream ss;
		//ss << "-----------------------------------------------------------------" << std::endl;
		//ss << symbol << std::endl;
		//ss << "=================================================================" << std::endl;
		ss << std::setw(moduleColumnSize) << std::left << moduleName.c_str() << " ";
		if (validCppName == 0) {
			ss << functionName << " ";
		}
		else if (functionSymbol.size() > 0) {
			ss << functionSymbol.c_str() << " ";
		}

		ss << "(+" << functionSymbolOffset.c_str() << ") ";
		ss << "[" << memLocation.c_str() << "]";
		ss << std::endl;
		//ss << "-----------------------------------------------------------------" << std::endl;

		free(functionName);

		str.append(ss.str());
	}
	free(symbols);

	return str;
#else
	(void)printTitle;
	return "[Stacktrace not implemented for Windows]"; // \TODO: Stacktrace not implemented for Windows
#endif
}


} // End namespace Verso


