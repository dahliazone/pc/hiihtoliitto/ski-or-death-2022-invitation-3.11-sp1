#ifndef VERSO_BASE_TIME_RECORDFRAMETIMER_HPP
#define VERSO_BASE_TIME_RECORDFRAMETIMER_HPP

#include <Verso/Time/ManualTimer.hpp>

namespace Verso {


class RecordFrameTimer
{
private:
	double recordFps;
	ManualTimer recordTimer;

	Timestamp dt;
	size_t updateFrame;
	size_t renderFrame;

public:
	VERSO_BASE_API RecordFrameTimer(
			double recordFps = 0.0);

public:
	VERSO_BASE_API void reset(
			double recordFps = 0.0);

	VERSO_BASE_API void update();
	VERSO_BASE_API void addRenderFrame();

public: // getters & setters
	VERSO_BASE_API double getRecordFps() const;
	VERSO_BASE_API Timestamp getTargetDt() const;
	VERSO_BASE_API double getFps() const;
	VERSO_BASE_API Timestamp getDt() const;
	VERSO_BASE_API Timestamp getElapsed() const;
	VERSO_BASE_API size_t getUpdateFrame() const;
	VERSO_BASE_API size_t getRenderFrame() const;

public: // toString
	VERSO_BASE_API UString toString() const;
	VERSO_BASE_API UString toStringDebug() const;

	friend std::ostream& operator <<(std::ostream& ost, const RecordFrameTimer& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso

#endif // End header guard

