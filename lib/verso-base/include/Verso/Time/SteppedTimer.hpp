#ifndef VERSO_BASE_TIME_STEPPEDTIMER_HPP
#define VERSO_BASE_TIME_STEPPEDTIMER_HPP

#include <Verso/Time/ITimer.hpp>

namespace Verso {


class SteppedTimer : public ITimer
{
private:
	double targetFps;
	Timestamp targetDt;
	Timestamp started;
	Timestamp reserve;

public:
	VERSO_BASE_API SteppedTimer(double targetFps = 60.0);

	VERSO_BASE_API virtual ~SteppedTimer() override;

public: // interface ITimer
	VERSO_BASE_API virtual Timestamp getElapsed() const override;

	VERSO_BASE_API virtual Timestamp restart() override;

public: // toString (interface ITimer)
	VERSO_BASE_API virtual UString toString() const override;

	VERSO_BASE_API virtual UString toStringDebug() const override;

public: // SteppedTimer specific getters & setters
	VERSO_BASE_API double getTargetFps() const;

	VERSO_BASE_API Timestamp getTargetDt() const;

	VERSO_BASE_API Timestamp getStarted() const;

	VERSO_BASE_API Timestamp getReserve() const;
};


} // End namespace Verso

#endif // End header guard

