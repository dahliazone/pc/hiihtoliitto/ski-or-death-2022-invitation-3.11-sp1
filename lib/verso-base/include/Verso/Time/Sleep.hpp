#ifndef VERSO_BASE_TIME_SLEEP_HPP
#define VERSO_BASE_TIME_SLEEP_HPP

#include <Verso/verso-base-common.hpp>
#include <cstdint>

namespace Verso {


class Sleep
{
public:
	VERSO_BASE_API static void seconds(double amount);
	VERSO_BASE_API static void milliseconds(int64_t amount);
	VERSO_BASE_API static void microseconds(int64_t amount);
};


} // End namespace Verso

#endif // End header guard

