#ifndef VERSO_BASE_TIME_TIMESTAMP_HPP
#define VERSO_BASE_TIME_TIMESTAMP_HPP

#include <Verso/System/UString.hpp>
#include <cstdint>
#include <cmath>

namespace Verso {


class Timestamp
{
public: // static
	VERSO_BASE_API static Timestamp seconds(double amount);

	VERSO_BASE_API static Timestamp milliseconds(int32_t amount);

	VERSO_BASE_API static Timestamp microseconds(int64_t amount);

private:
	int64_t timeMicroseconds;

public:
	Timestamp() :
		timeMicroseconds(0)
	{
	}


	// Leap years not calculated
	double asYears() const
	{
		return static_cast<double>(timeMicroseconds) / (1000000.0 * 60.0 * 60.0 * 24.0 * 365.0);
	}


	// Leap years not calculated
	double asMonths() const
	{
		return static_cast<double>(timeMicroseconds) / (1000000.0 * 60.0 * 60.0 * 24.0 * (365.0/12.0));
	}


	double asDays() const
	{
		return static_cast<double>(timeMicroseconds) / (1000000.0 * 60.0 * 60.0 * 24.0);
	}


	double asHours() const
	{
		return static_cast<double>(timeMicroseconds) / (1000000.0 * 60.0 * 60.0);
	}


	double asMinutes() const
	{
		return static_cast<double>(timeMicroseconds) / (1000000.0 * 60.0);
	}


	double asSeconds() const
	{
		return static_cast<double>(timeMicroseconds) / 1000000.0;
	}


	int32_t asMilliseconds() const
	{
		return static_cast<int32_t>(timeMicroseconds / 1000);
	}


	int64_t asMicroseconds() const
	{
		return timeMicroseconds;
	}


	VERSO_BASE_API UString asDigitalClockString(bool fractions = false, bool forceHours = false) const;

public: // static
	VERSO_BASE_API static Timestamp Zero();

public: // toString
	UString toString() const
	{
		UString str;
		str.append2(asSeconds());
		return str;
	}


	UString toStringDebug() const
	{
		UString str("Timestamp(");
		str += toString();
		str += "timeMicroseconds=";
		str.append2(timeMicroseconds);
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const Timestamp& right)
	{
		return ost << right.toString();
	}
};


inline bool operator ==(Timestamp left, Timestamp right)
{
	if (left.asMicroseconds() == right.asMicroseconds())
		return true;
	else
		return false;
}


inline bool operator !=(Timestamp left, Timestamp right)
{
	if (!(left == right))
		return true;
	else
		return false;
}


inline bool operator <(Timestamp left, Timestamp right)
{
	if (left.asMicroseconds() < right.asMicroseconds())
		return true;
	else
		return false;
}


inline bool operator <=(Timestamp left, Timestamp right)
{
	if (left.asMicroseconds() <= right.asMicroseconds())
		return true;
	else
		return false;
}


inline bool operator >(Timestamp left, Timestamp right)
{
	if (left.asMicroseconds() > right.asMicroseconds())
		return true;
	else
		return false;
}


inline bool operator >=(Timestamp left, Timestamp right)
{
	if (left.asMicroseconds() >= right.asMicroseconds())
		return true;
	else
		return false;
}


inline Timestamp operator -(Timestamp right)
{
	return Timestamp::microseconds(-right.asMicroseconds());
}


inline Timestamp operator +(Timestamp left, Timestamp right)
{
	return Timestamp::microseconds(left.asMicroseconds() + right.asMicroseconds());
}


inline Timestamp& operator +=(Timestamp &left, Timestamp right)
{
	return left = left + right;
}


inline Timestamp operator -(Timestamp left, Timestamp right)
{
	return Timestamp::microseconds(left.asMicroseconds() - right.asMicroseconds());
}


inline Timestamp& operator -=(Timestamp &left, Timestamp right)
{
	return left = left - right;
}


inline Timestamp operator *(Timestamp left, double right)
{
	return Timestamp::seconds(left.asSeconds() * right);
}


inline Timestamp operator *(Timestamp left, int64_t right)
{
	return Timestamp::microseconds(left.asMicroseconds() * right);
}


inline Timestamp operator *(double left, Timestamp right)
{
	return right * left;
}


inline Timestamp operator *(std::int64_t left, Timestamp right)
{
	return Timestamp::microseconds(left * right.asMicroseconds());
}


inline Timestamp& operator *=(Timestamp &left, double right)
{
	return left = left * right;
}


inline Timestamp& operator *=(Timestamp &left, int64_t right)
{
	return left = left * right;
}


inline Timestamp operator /(Timestamp left, double right)
{
	return Timestamp::seconds(left.asSeconds() / right);
}


inline Timestamp& operator /=(Timestamp &left, double right)
{
	return left = left / right;
}


inline Timestamp operator /(Timestamp left, int64_t right)
{
	return Timestamp::microseconds(left.asMicroseconds() / right);
}


inline Timestamp& operator /=(Timestamp &left, int64_t right)
{
	return left = left / right;
}


inline double operator /(Timestamp left, Timestamp right)
{
	return static_cast<double>(left.asMicroseconds()) / static_cast<double>(right.asMicroseconds());
}


inline Timestamp operator %(Timestamp left, Timestamp right)
{
	return Timestamp::microseconds(left.asMicroseconds() % right.asMicroseconds());
}


inline Timestamp& operator %=(Timestamp& left, Timestamp right)
{
	return left = Timestamp::microseconds(left.asMicroseconds() % right.asMicroseconds());
}


} // End namespace Verso

#endif // End header guard

