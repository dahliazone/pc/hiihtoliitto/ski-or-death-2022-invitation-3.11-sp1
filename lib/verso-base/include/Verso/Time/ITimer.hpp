#ifndef VERSO_BASE_TIME_ITIMER_HPP
#define VERSO_BASE_TIME_ITIMER_HPP

#include <Verso/Time/Timestamp.hpp>

namespace Verso {


class ITimer
{
public:
	virtual ~ITimer() = default;

public: // interface ITimer
	virtual Timestamp getElapsed() const = 0;

	virtual Timestamp restart() = 0;

	virtual UString toString() const = 0;

	virtual UString toStringDebug() const = 0;

public: // toString
	friend std::ostream& operator <<(std::ostream& ost, const ITimer& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso

#endif // End header guard

