#ifndef VERSO_BASE_TIME_DATETIME_HPP
#define VERSO_BASE_TIME_DATETIME_HPP

#include <Verso/System/UString.hpp>

namespace Verso {


class Datetime
{
public:
	// Format: 2016-02-16 18:40:00
	VERSO_BASE_API static UString getNow();

	// Format: 2016-02-16_18-40-00
	VERSO_BASE_API static UString getNowFileName();

	// Format: 2016-02-16
	VERSO_BASE_API static UString getNowDate();

	// Format: 18:40:00
	VERSO_BASE_API static UString getNowTime();
};


} // End namespace Verso

#endif // End header guard

