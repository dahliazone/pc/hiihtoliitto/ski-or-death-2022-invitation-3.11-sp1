#ifndef VERSO_BASE_SYSTEM_FILELINUX_HPP
#define VERSO_BASE_SYSTEM_FILELINUX_HPP
#if __linux__

#include <Verso/System/UString.hpp>
#include <vector>

namespace Verso {
namespace priv {


class FileLinux
{
public:
	static UString readSymLink(const UString& symLink);
	static UString getBasePath();
	static UString getPreferencesPath();
	static bool mkdir(const UString& fileName);
	static void ls(const UString& directoryName, bool showFiles, bool showDirectories, bool showDotFiles, std::vector<UString>& out);
	static int64_t getSize(const char* fileName);

	inline static UString getDirectorySeparator() {
		return "/";
	}
};


} // End namespace priv
} // End namespace Verso

#endif // End #ifdef __linux__
#endif // End header guard


