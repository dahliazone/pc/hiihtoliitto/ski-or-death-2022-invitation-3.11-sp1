#ifndef VERSO_BASE_SYSTEM_COMMANDLINEPARSER_OPTIONS_HPP
#define VERSO_BASE_SYSTEM_COMMANDLINEPARSER_OPTIONS_HPP

#include <Verso/System/CommandLineParser/Option.hpp>
#include <Verso/System/Exception/IllegalFormatException.hpp>
#include <Verso/System/Exception/ObjectNotFoundException.hpp>

namespace Verso {


class Options
{
private:
	std::vector<Option> options;

public:
	VERSO_BASE_API Options();
	VERSO_BASE_API virtual ~Options();
	VERSO_BASE_API void addOption(const Option& option);
	VERSO_BASE_API Option getOption(const UString& optionName) const;
	VERSO_BASE_API bool hasOption(const UString& optionName) const;
	VERSO_BASE_API bool hasOption(const Option& option) const;
	VERSO_BASE_API std::vector<UString> getOptionValues(const UString& optionName) const;
//	VERSO_BASE_API std::vector<UString> getOptionValues(const Option& option) const;
	VERSO_BASE_API const std::vector<Option>& getOptions() const;
	VERSO_BASE_API std::vector<Option> getRequiredOptions() const;
};


} // End namespace Verso

#endif // End header guard

