#ifndef VERSO_BASE_SYSTEM_COMMANDLINEPARSER_HELPFORMATTER_HPP
#define VERSO_BASE_SYSTEM_COMMANDLINEPARSER_HELPFORMATTER_HPP

#include <Verso/System/CommandLineParser/Options.hpp>

namespace Verso {


class HelpFormatter
{
public:
	VERSO_BASE_API void printHelp(const UString& binaryName, const Options& options) const;
	VERSO_BASE_API UString generateString(const UString& binaryName, const Options& options) const;

private:
	UString generateOptionString(const Option& option) const;
};


} // End namespace Verso

#endif // End header guard

