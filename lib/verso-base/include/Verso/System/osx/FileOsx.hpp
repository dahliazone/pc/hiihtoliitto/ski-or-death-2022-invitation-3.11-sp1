#ifndef VERSO_BASE_SYSTEM_FILEOSX_HPP
#define VERSO_BASE_SYSTEM_FILEOSX_HPP
#if __APPLE__ && __MACH__

#include <Verso/System/UString.hpp>
#include <vector>

namespace Verso {
namespace priv {


class FileOsx
{
public:
	static UString readSymLink(const UString& symLink);
	static UString getBasePath();
	static UString getPreferencesPath();
	static bool mkdir(const UString& fileName);
	static void ls(const UString& directoryName, bool showFiles, bool showDirectories, bool showDotFiles, std::vector<UString>& out);
	static int64_t getSize(const char* fileName);

	inline static UString getDirectorySeparator() {
		return "/";
	}
};


} // End namespace priv
} // End namespace Verso

#endif // End #ifdef __APPLE__ && __MACH__
#endif // End header guard


