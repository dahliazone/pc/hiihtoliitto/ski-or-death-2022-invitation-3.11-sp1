#ifndef VERSO_BASE_SYSTEM_COMMANDLINEPARSER_HPP
#define VERSO_BASE_SYSTEM_COMMANDLINEPARSER_HPP

#include <Verso/System/Args.hpp>
#include <Verso/System/CommandLineParser/Options.hpp>

namespace Verso {


class CommandLineParser
{
private:
	Options resultOptions;

public:
	VERSO_BASE_API void parse(const Options& options, int argc, char* argv[]);
	VERSO_BASE_API void parse(const Options& searchOptions, const std::vector<UString>& args);
	VERSO_BASE_API const std::vector<Option>& getOptions() const;
	VERSO_BASE_API bool hasOption(const Option& option) const;
	VERSO_BASE_API bool hasOption(const UString& optionName) const;
	VERSO_BASE_API std::vector<UString> getOptionValues(const Option& option) const;
	VERSO_BASE_API std::vector<UString> getOptionValues(const UString& optionName) const;
};


} // End namespace Verso

#endif // End header guard

