#ifndef VERSO_BASE_SYSTEM_NONCOPYABLE_HPP
#define VERSO_BASE_SYSTEM_NONCOPYABLE_HPP

#include <Verso/verso-base-common.hpp>

namespace Verso {


struct NonCopyable
{
	NonCopyable() = default;
	NonCopyable(const NonCopyable&) = delete;
	NonCopyable& operator=(const NonCopyable&) = delete;
};


} // End namespace Verso

#endif // End header guard

