#ifndef VERSO_BASE_SYSTEM_NONMOVABLE_HPP
#define VERSO_BASE_SYSTEM_NONMOVABLE_HPP

#include <Verso/verso-base-common.hpp>

namespace Verso {


class NonMovable
{
public:
	NonMovable(NonMovable&&) = delete;

	NonMovable& operator=(NonMovable&&) = delete;

protected:
	NonMovable() = default;
};


} // End namespace Verso

#endif // End header guard

