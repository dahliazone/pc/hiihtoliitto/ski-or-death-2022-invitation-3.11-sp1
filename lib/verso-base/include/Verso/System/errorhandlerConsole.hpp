#ifndef VERSO_GFX_SYSTEM_ERRORHANDLERCONSOLE_HPP
#define VERSO_GFX_SYSTEM_ERRORHANDLERCONSOLE_HPP

#include <Verso/System/UString.hpp>

namespace Verso {


VERSO_BASE_API void errorHandlerConsoleEnable();


} // End namespace Verso

#endif // End header guard

