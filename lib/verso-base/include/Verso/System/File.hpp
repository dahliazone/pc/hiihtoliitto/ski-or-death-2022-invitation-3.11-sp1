#ifndef VERSO_BASE_SYSTEM_FILE_HPP
#define VERSO_BASE_SYSTEM_FILE_HPP

#include <Verso/System/UString.hpp>
#include <Verso/System/Logger.hpp>
#include <cstdio>
#include <fstream>
#include <cstdint>

namespace Verso {


class File
{
private:
	UString fileName;
	FILE* filePointer;

public: // static
	VERSO_BASE_API static UString getDirectorySeparator();
	VERSO_BASE_API static UString readSymLink(const UString& symLink);
	VERSO_BASE_API static UString getBasePath();
	VERSO_BASE_API static UString getPreferencesPath();
	VERSO_BASE_API static UString getFileNamePath(const UString& fileName);

	VERSO_BASE_API static bool mkdir(const UString& fileName);
	VERSO_BASE_API static std::vector<UString> ls(const UString& directoryName,
								   bool showFiles = true, bool showDirectories = true, bool showDotFiles = false);

	VERSO_BASE_API static bool exists(const UString& fileName);
	VERSO_BASE_API static bool exists(const std::string& fileName);
	VERSO_BASE_API static bool exists(const char* fileName);

	VERSO_BASE_API static int64_t getSize(const UString& fileName);
	VERSO_BASE_API static int64_t getSize(const std::string& fileName);
	VERSO_BASE_API static int64_t getSize(const char* fileName);

	VERSO_BASE_API static bool remove(const UString& fileName);
	VERSO_BASE_API static bool remove(const std::string& fileName);
	VERSO_BASE_API static bool remove(const char* fileName);

	VERSO_BASE_API static uint8_t readUint8(const char* headerBytes, size_t index);
	VERSO_BASE_API static uint16_t readUint16(const char* headerBytes, size_t index);
	VERSO_BASE_API static uint32_t readUint32(const char* headerBytes, size_t index);
	VERSO_BASE_API static uint64_t readUint64(const char* headerBytes, size_t index);

	// Please free the given buffer yourself with delete[].
	VERSO_BASE_API static char* loadBytes(const UString& fileName, size_t& filesize);
	VERSO_BASE_API static char* loadBytes(const std::string& fileName, size_t& filesize);
	VERSO_BASE_API static char* loadBytes(const char* fileName, size_t& filesize);

	VERSO_BASE_API static bool equals(const UString& fileName1, const UString& fileName2);
	VERSO_BASE_API static bool equals(const std::string& fileName1, const std::string& fileName2);
	VERSO_BASE_API static bool equals(const char* fileName1, const char* fileName2);

public:
	VERSO_BASE_API File();
	VERSO_BASE_API explicit File(const UString& fileName);
	File(const File& original) = delete;
	File(File&& original) = delete;
	VERSO_BASE_API virtual ~File();
	File& operator =(const File& original) = delete;
	File& operator =(File&& original) = delete;

public:
	VERSO_BASE_API bool isOpen() const;
	VERSO_BASE_API UString getFileName() const;

	// note: Will always close the currently open file
	VERSO_BASE_API void changeFileName(const UString& fileName);

	VERSO_BASE_API bool exists();

	// note: mode is same as for fopen()
	// note: calling Open will close the previously opened file only if it succeeds to open the next
	VERSO_BASE_API bool open(const UString& mode, const UString& fileName = "");

	VERSO_BASE_API void close();

	VERSO_BASE_API bool loadToString(UString& target);
};


} // End namespace Verso

#endif // End header guard


