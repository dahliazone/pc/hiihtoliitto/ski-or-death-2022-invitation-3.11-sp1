#ifndef VERSO_BASE_SYSTEM_EXCEPTION_EXCEPTIONINTERFACE_HPP
#define VERSO_BASE_SYSTEM_EXCEPTION_EXCEPTIONINTERFACE_HPP

#include <Verso/verso-base-common.hpp>
#include <stdexcept>
#include <string>

#if !defined(VERSO_HAS_NOEXCEPT)
#if defined(__clang__)
#if __has_feature(cxx_noexcept)
#define VERSO_HAS_NOEXCEPT
#endif
#else
#if defined(__GXX_EXPERIMENTAL_CXX0X__) && __GNUC__ * 10 + __GNUC_MINOR__ >= 46 || \
	defined(_MSC_FULL_VER) && _MSC_FULL_VER >= 190023026
#define VERSO_HAS_NOEXCEPT
#endif
#endif
#endif

#ifdef VERSO_HAS_NOEXCEPT
#define VERSO_NOEXCEPT noexcept
#else
#define VERSO_NOEXCEPT
#endif

namespace Verso {


class ExceptionInterface : public std::runtime_error
{
public:
	ExceptionInterface() VERSO_NOEXCEPT :
		std::runtime_error("")
	{
	}

	virtual ~ExceptionInterface() VERSO_NOEXCEPT override = default;

public: // Interface Verso::ExceptionInterface
	virtual const std::string& getType() const = 0;
	virtual const std::string& getComponent() const = 0;
	virtual const std::string& getMessage() const = 0;
	virtual const std::string& getFunction() const = 0;
	virtual const std::string& getFileName() const = 0;
	virtual long getLine() const = 0;
	virtual const std::string& getResources() const = 0;
	virtual const std::string& toString() const = 0;

public: // Interface std::runtime_error
	const char* what() const VERSO_NOEXCEPT override = 0;
};


} // End namespace Verso


#endif // End header guard

