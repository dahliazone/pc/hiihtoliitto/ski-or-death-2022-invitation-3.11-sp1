#ifndef VERSO_BASE_SYSTEM_EXCEPTION_IOERROREXCEPTION_HPP
#define VERSO_BASE_SYSTEM_EXCEPTION_IOERROREXCEPTION_HPP

#include <Verso/System/Exception/ExceptionInterface.hpp>
#include <Verso/System/current_function.hpp>
#include <sstream>

namespace Verso {


class IoErrorException : public ExceptionInterface
{
private:
	std::string component;
	std::string message;
	std::string function;
	std::string fileName;
	long line;
	std::string resources;
	std::string output;

public:
	IoErrorException(
			const std::string& component,
			const std::string& message,
			const std::string& function, const std::string& fileName, long line,
			const std::string& resources = "") VERSO_NOEXCEPT;
	virtual ~IoErrorException() VERSO_NOEXCEPT override = default;

public: // Interface Verso::ExceptionInterface
	virtual const std::string& getType() const override;
	virtual const std::string& getComponent() const override;
	virtual const std::string& getMessage() const override;
	virtual const std::string& getFunction() const override;
	virtual const std::string& getFileName() const override;
	virtual long getLine() const override;
	virtual const std::string& getResources() const override;
	virtual const std::string& toString() const override;

public: // Interface std::runtime_error
	virtual const char* what() const VERSO_NOEXCEPT override;
};


#define VERSO_IOERROR(component, message, resources) (throw IoErrorException(component, message, VERSO_FUNCTION, VERSO_FILE, VERSO_LINE, resources))


inline IoErrorException::IoErrorException(
		const std::string& component,
		const std::string& message,
		const std::string& function, const std::string& fileName, long line,
		const std::string& resources) VERSO_NOEXCEPT :
	ExceptionInterface(),
	component(component),
	message(message),
	function(function),
	fileName(fileName),
	line(line),
	resources(resources),
	output()
{
	std::ostringstream oss;
	oss << "  Type: " << getType() << std::endl
		<< "  Component: " << component << std::endl
		<< "  Origin: " << function << std::endl
		<< "          at \"" << fileName << "\":" << line;

	if (message.size() != 0) {
		oss << std::endl << "  Message: " << message;
	}

	if (resources.size() != 0) {
		oss << std::endl << "  Resources: " << resources;
	}

	output = oss.str();
}


///////////////////////////////////////////////////////////////////////////////////////////
// Interface Verso::ExceptionInterface
///////////////////////////////////////////////////////////////////////////////////////////

inline const std::string& IoErrorException::getType() const
{
	static std::string type("IoErrorException");
	return type;
}


inline const std::string& IoErrorException::getComponent() const
{
	return component;
}


inline const std::string& IoErrorException::getMessage() const
{
	return message;
}


inline const std::string& IoErrorException::getFunction() const
{
	return function;
}


inline const std::string& IoErrorException::getFileName() const
{
	return fileName;
}


inline long IoErrorException::getLine() const
{
	return line;
}


inline const std::string& IoErrorException::getResources() const
{
	return resources;
}


inline const std::string& IoErrorException::toString() const
{
	return output;
}


///////////////////////////////////////////////////////////////////////////////////////////
// Interface std::runtime_error
///////////////////////////////////////////////////////////////////////////////////////////

inline const char* IoErrorException::what() const VERSO_NOEXCEPT
{
	return toString().c_str();
}


} // End namespace Verso

#endif // End header guard

