#ifndef VERSO_BASE_SYSTEM_STRING_CONVERT_HPP
#define VERSO_BASE_SYSTEM_STRING_CONVERT_HPP

#include <Verso/verso-base-common.hpp>
#include <sstream>
#include <string>

namespace Verso {


/**
 * Template function for converting an arbitrary type to C++ string
 */
template <class T>
inline void stringAdd(std::string &val, const T& t)
{
	std::ostringstream oss; // create a stream
	oss << t;          // insert value to stream
	val += oss.str();  // extract string and copy
}


/**
 * Template function for converting from one arbitrary type to another
 */
template <class out_type, class in_value>
inline out_type castStream(const in_value& t)
{
	std::stringstream ss;
	ss << t;         // first insert value to stream
	out_type result; // value will be converted to out_type
	ss >> result;    // write value to result
	return result;
}


} // End namespace Verso

#endif // End header guard

