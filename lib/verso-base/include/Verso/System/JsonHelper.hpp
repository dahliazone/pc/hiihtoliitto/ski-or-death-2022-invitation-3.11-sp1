#ifndef VERSO_BASE_SYSTEM_JSONHELPER_HPP
#define VERSO_BASE_SYSTEM_JSONHELPER_HPP

#include <Verso/System/File.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>
#include <Verso/Math/Align.hpp>
#include <Verso/Math/AspectRatio.hpp>
#include <Verso/Math/CameraKeyframes.hpp>
#include <Verso/Math/EasingType.hpp>
#include <Verso/Math/FloatKeyframes.hpp>
#include <Verso/Math/InterpolationType.hpp>
#include <Verso/Math/IntKeyframes.hpp>
#include <Verso/Math/PixelAspectRatio.hpp>
#include <Verso/Math/Range.hpp>
#include <Verso/Math/RefScale.hpp>
#include <Verso/Math/RgbaColorf.hpp>
#include <Verso/Math/RgbaColorfKeyframes.hpp>
#include <Verso/Math/Vector2Keyframes.hpp>
#include <Verso/Math/Vector3.hpp>
#include <Verso/Math/Vector3Keyframes.hpp>

#include <JSON.h>

namespace Verso {


namespace JsonHelper {


VERSO_BASE_API extern JSONObject emptyJsonObject;
VERSO_BASE_API extern JSONArray emptyJsonArray;


VERSO_BASE_API JSONValue* loadAndParse(const UString& fileName);
VERSO_BASE_API void saveToFile(const UString& fileName, JSONValue* root, bool prettyPrint = true);
VERSO_BASE_API void free(JSONValue* root);
VERSO_BASE_API JSONValue* readValue(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false);

VERSO_BASE_API bool isDefined(const JSONObject& object, const UString& currentPathJson, const UString& name);

VERSO_BASE_API bool isNull(const JSONObject& object, const UString& currentPathJson, const UString& name);

VERSO_BASE_API bool isObject(const JSONObject& object, const UString& currentPathJson, const UString& name);
VERSO_BASE_API const JSONObject& readObject(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false);
VERSO_BASE_API JSONValue* writeObject(JSONObject& jsonObject);

VERSO_BASE_API bool isArray(const JSONObject& object, const UString& currentPathJson, const UString& name);
VERSO_BASE_API const JSONArray& readArray(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false);
VERSO_BASE_API JSONValue* writeArray(JSONArray& jsonArray);

VERSO_BASE_API bool isBool(const JSONObject& object, const UString& currentPathJson, const UString& name);
VERSO_BASE_API bool readBool(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, bool defaultValue = false);
VERSO_BASE_API JSONValue* writeBool(bool value);

VERSO_BASE_API bool isInt(const JSONObject& object, const UString& currentPathJson, const UString& name, bool checkForDecimalZero);
VERSO_BASE_API int readInt(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, int defaultValue = 0);
VERSO_BASE_API JSONValue* writeInt(int value);

VERSO_BASE_API bool isNumber(const JSONObject& object, const UString& currentPathJson, const UString& name);

VERSO_BASE_API double readNumberd(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, double defaultValue = 0.0);
VERSO_BASE_API JSONValue* writeNumberd(double value);

VERSO_BASE_API float readNumberf(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, float defaultValue = 0.0f);
VERSO_BASE_API JSONValue* writeNumberf(float value);

VERSO_BASE_API std::vector<float> readNumberfArray(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const std::vector<float>& defaultValue = std::vector<float>());

VERSO_BASE_API int readNumberi(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, int defaultValue = 0);
VERSO_BASE_API JSONValue* writeNumberi(int value);

VERSO_BASE_API unsigned int readNumberu(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, unsigned int defaultValue = 0);
VERSO_BASE_API JSONValue* writeNumberu(unsigned int value);

VERSO_BASE_API size_t readNumbersz(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, size_t defaultValue = 0);
VERSO_BASE_API JSONValue* writeNumbersz(size_t value);

VERSO_BASE_API bool isString(const JSONObject& object, const UString& currentPathJson, const UString& name);
VERSO_BASE_API UString readString(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const UString& defaultValue = "");
VERSO_BASE_API JSONValue* writeString(const UString& str);

VERSO_BASE_API Align readAlign(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const Align& defaultValue = Align());

VERSO_BASE_API RefScale readRefScale(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const RefScale& defaultValue = RefScale::ViewportSize_KeepAspectRatio_FitRect);

VERSO_BASE_API bool isColor(const JSONObject& object, const UString& currentPathJson, const UString& name);
VERSO_BASE_API RgbaColorf readColor(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const RgbaColorf& defaultValue = RgbaColorf(0.0f, 0.0f, 0.0f, 1.0f));
VERSO_BASE_API JSONValue* writeColor(const RgbaColorf& color, bool useAlpha);

VERSO_BASE_API Rangei readRangei(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const Rangei& defaultValue = Rangei());
VERSO_BASE_API JSONValue* writeRangei(const Rangei& range);

VERSO_BASE_API Rangeu readRangeu(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const Rangeu& defaultValue = Rangeu());
VERSO_BASE_API JSONValue* writeRangeu(const Rangeu& range);

VERSO_BASE_API Rangef readRangef(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const Rangef& defaultValue = Rangef());
VERSO_BASE_API JSONValue* writeRangef(const Rangef& range);

VERSO_BASE_API Ranged readRanged(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const Ranged& defaultValue = Ranged());
VERSO_BASE_API JSONValue* writeRanged(const Ranged& range);

VERSO_BASE_API Rectf readRectf(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const Rectf& defaultValue = Rectf());

VERSO_BASE_API Vector2f readVector2f(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const Vector2f& defaultValue = Vector2f());

VERSO_BASE_API bool isVector2fArrayFormat(const JSONObject& object, const UString& currentPathJson, const UString& name);
VERSO_BASE_API Vector2f readVector2fArrayFormat(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const Vector2f& defaultValue = Vector2f());

VERSO_BASE_API Vector2i readVector2iArrayFormat(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const Vector2i& defaultValue = Vector2i());

VERSO_BASE_API Vector2i readVector2i(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const Vector2i& defaultValue = Vector2i());

VERSO_BASE_API Vector3f readVector3f(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const Vector3f& defaultValue = Vector3f());

VERSO_BASE_API bool isVector3fArrayFormat(const JSONObject& object, const UString& currentPathJson, const UString& name);
VERSO_BASE_API Vector3f readVector3fArrayFormat(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const Vector3f& defaultValue = Vector3f());

VERSO_BASE_API AspectRatio readAspectRatio(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const AspectRatio& defaultValue = AspectRatio());

VERSO_BASE_API PixelAspectRatio readPixelAspectRatio(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const PixelAspectRatio& = PixelAspectRatio());

VERSO_BASE_API EasingType readEasingType(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const EasingType& defaultValue = EasingType::Linear);

VERSO_BASE_API InterpolationType readInterpolationType(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const InterpolationType& defaultValue = InterpolationType::Linear);

VERSO_BASE_API IntKeyframes readIntKeyframes(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const IntKeyframes& defaultValue = IntKeyframes());

VERSO_BASE_API FloatKeyframes readFloatKeyframes(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const FloatKeyframes& defaultValue = FloatKeyframes());

VERSO_BASE_API Vector2fKeyframes readVector2fKeyframes(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const Vector2fKeyframes& defaultValue = Vector2fKeyframes());

VERSO_BASE_API Vector3fKeyframes readVector3fKeyframes(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const Vector3fKeyframes& defaultValue = Vector3fKeyframes());

VERSO_BASE_API RgbaColorfKeyframes readRgbaColorfKeyframes(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const RgbaColorfKeyframes& defaultValue = RgbaColorfKeyframes());

VERSO_BASE_API CameraKeyframes readCameraKeyframes(const JSONObject& object, const UString& currentPathJson, const UString& name, bool required = false, const CameraKeyframes& defaultValue = CameraKeyframes());

} // End namespace JsonHelper


} // End namespace Verso

#endif // End header guard

