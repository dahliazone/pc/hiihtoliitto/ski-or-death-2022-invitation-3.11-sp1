#ifndef VERSO_BASE_SYSTEM_ASSERT_HPP
#define VERSO_BASE_SYSTEM_ASSERT_HPP

#include <Verso/System/Exception/AssertException.hpp>
#include <Verso/System/current_function.hpp>

namespace Verso {


/**
 * @brief Creates and throws an AssertException.
 */
[[noreturn]] inline void assertionFailed(const char* component, const char* expression, const char* function, const char* file, long line, const char* message = "", const char* resources = "")
{
	throw Verso::AssertException(component, message, function, file, line, resources, expression);
}


#define VERSO_HELPER_ASSERT(component, expr, message, resources) \
	((expr) ? ((void)0) : ::Verso::assertionFailed(component, #expr, VERSO_FUNCTION, VERSO_FILE, VERSO_LINE, message, resources))


/**
 * @brief Works like assert().
 */
#define VERSO_ASSERT(component, expr) (VERSO_HELPER_ASSERT(component, expr, "", ""))

/**
 * @brief Works like assert() but with custom string message.
 */
#define VERSO_ASSERT_MSG(component, expr, message) (VERSO_HELPER_ASSERT(component, expr, message, ""))

/**
 * @brief Works like assert() but with custom string message and string resource.
 */
#define VERSO_ASSERT_MSG_AND_RESOURCES(component, expr, message, resources) (VERSO_HELPER_ASSERT(component, expr, message, resources))

/**
 * @brief Use inside non-implemented function/methods.
 * Works like always failing assert() but with the string "Not implemented / executed invalid code block".
 */
#define VERSO_FAIL(component) (::Verso::assertionFailed(component, "", VERSO_FUNCTION, VERSO_FILE, VERSO_LINE, "Not implemented / executed invalid code block"))


} // End namespace Verso


#endif // End header guard

