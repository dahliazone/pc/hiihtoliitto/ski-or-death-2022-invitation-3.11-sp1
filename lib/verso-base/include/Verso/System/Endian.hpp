#ifndef VERSO_BASE_SYSTEM_ENDIAN_HPP
#define VERSO_BASE_SYSTEM_ENDIAN_HPP

#include <Verso/verso-base-common.hpp>
#include <cstdint>
#ifdef __linux__
#include <endian.h>
#include <byteswap.h>
#endif

// big-endian target architecture
#if defined(__BYTE_ORDER) && __BYTE_ORDER == __BIG_ENDIAN || \
	defined(__BIG_ENDIAN__) || \
	defined(__ARMEB__) || \
	defined(__THUMBEB__) || \
	defined(__AARCH64EB__) || \
	defined(_MIBSEB) || defined(__MIBSEB) || defined(__MIBSEB__) || \
	defined(_M_PPC)
	#define VERSO_LITTLE_ENDIAN (0)

// little-endian target architecture
#elif defined(__BYTE_ORDER) && __BYTE_ORDER == __LITTLE_ENDIAN || \
	defined(__LITTLE_ENDIAN__) || \
	defined(__ARMEL__) || \
	defined(__THUMBEL__) || \
	defined(__AARCH64EL__) || \
	defined(_MIPSEL) || defined(__MIPSEL) || defined(__MIPSEL__) || \
	defined(_M_IX86) || defined(_M_X64) || defined(_M_IA64) || defined(_M_ARM)
	#define VERSO_LITTLE_ENDIAN (1)
#else
#error "I don't know what architecture this is!"
#endif

namespace Verso {


class Endian
{
public:
	inline static bool isLittleEndian()
	{
		return VERSO_LITTLE_ENDIAN;
	}


	inline static std::uint16_t bswap(std::uint16_t value)
	{
#ifdef bswap_16
		return bswap_16(value);
#else
		return ((value & 0xff00) >> 8) |
				((value & 0x00ff) << 8);
#endif
	}


	inline static std::uint32_t bswap(std::uint32_t value)
	{
#ifdef bswap_32
		return bswap_32(value);
#else
		return ((value & 0xff000000) >> 24) |
				((value & 0x00ff0000) >>  8) |
				((value & 0x0000ff00) <<  8) |
				((value & 0x000000ff) << 24);
#endif
	}


	inline static std::uint64_t bswap(std::uint64_t value)
	{
#ifdef bswap_64
		return bswap_64(value);
#else
		return ((value & 0xff00000000000000ULL) >> 56) |
				((value & 0x00ff000000000000ULL) >> 40) |
				((value & 0x0000ff0000000000ULL) >> 24) |
				((value & 0x000000ff00000000ULL) >>  8) |
				((value & 0x00000000ff000000ULL) <<  8) |
				((value & 0x0000000000ff0000ULL) << 24) |
				((value & 0x000000000000ff00ULL) << 40) |
				((value & 0x00000000000000ffULL) << 56);
#endif
	}


	float bswap(float value)
	{
		float retVal = 0;
		char *floatToConvert = reinterpret_cast<char*>(&value);
		char *returnFloat = reinterpret_cast<char*>(&retVal);

		// swap the bytes into a temporary buffer
		returnFloat[0] = floatToConvert[3];
		returnFloat[1] = floatToConvert[2];
		returnFloat[2] = floatToConvert[1];
		returnFloat[3] = floatToConvert[0];

		return retVal;
	}
};


} // End namespace Verso

#endif // End header guard

