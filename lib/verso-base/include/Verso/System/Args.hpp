#ifndef VERSO_BASE_SYSTEM_ARGS_HPP
#define VERSO_BASE_SYSTEM_ARGS_HPP

#include <Verso/System/UString.hpp>

namespace Verso {


class Args
{
public:
	VERSO_BASE_API static std::vector<UString> convert(int argc, char* argv[])
	{
		std::vector<UString> args;
		for (int i=0; i<argc; ++i) {
			args.push_back(UString(argv[i]));
		}
		return args;
	}
};


} // End namespace Verso

#endif // End header guard

