#ifndef VERSO_BASE_MAININCLUDES_MATH_HPP
#define VERSO_BASE_MAININCLUDES_MATH_HPP


#include <Verso/Math/Align.hpp>
#include <Verso/Math/Array2.hpp>
#include <Verso/Math/AspectRatio.hpp>
#include <Verso/Math/CameraKeyframeElement.hpp>
#include <Verso/Math/CameraKeyframes.hpp>
#include <Verso/Math/Degree.hpp>
#include <Verso/Math/Easing.hpp>
#include <Verso/Math/EasingType.hpp>
#include <Verso/Math/FloatKeyframeElement.hpp>
#include <Verso/Math/FloatKeyframes.hpp>
#include <Verso/Math/HAlign.hpp>
#include <Verso/Math/Hash32.hpp>
#include <Verso/Math/Interpolation.hpp>
#include <Verso/Math/InterpolationType.hpp>
#include <Verso/Math/IntKeyframeElement.hpp>
#include <Verso/Math/IntKeyframes.hpp>
#include <Verso/Math/Math.hpp>
#include <Verso/Math/math_defines.hpp>
#include <Verso/Math/Matrix4x4f.hpp>
#include <Verso/Math/PixelAspectRatio.hpp>
#include <Verso/Math/Quaternion.hpp>
#include <Verso/Math/Radian.hpp>
#include <Verso/Math/Random.hpp>
#include <Verso/Math/Range.hpp>
#include <Verso/Math/Rect.hpp>
#include <Verso/Math/RefScale.hpp>
#include <Verso/Math/RgbaColorf.hpp>
#include <Verso/Math/RgbaColorfKeyframeElement.hpp>
#include <Verso/Math/RgbaColorfKeyframes.hpp>
#include <Verso/Math/Transform.hpp>
#include <Verso/Math/VAlign.hpp>
#include <Verso/Math/Vector2.hpp>
#include <Verso/Math/Vector2KeyframeElement.hpp>
#include <Verso/Math/Vector2Keyframes.hpp>
#include <Verso/Math/Vector3.hpp>
#include <Verso/Math/Vector3KeyframeElement.hpp>
#include <Verso/Math/Vector3Keyframes.hpp>
#include <Verso/Math/Vector4.hpp>
#include <Verso/Math/Vertex.hpp>


#endif // End header guard

