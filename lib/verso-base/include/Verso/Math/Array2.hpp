#ifndef VERSO_BASE_MATH_ARRAY2_HPP
#define VERSO_BASE_MATH_ARRAY2_HPP

#include <Verso/System/UString.hpp>
#include <vector>
#include <iomanip>
#include <queue>
#include <utility>
#include <sstream>

namespace Verso {


/**
 * @brief Class representing a n*m sized 2-dimensional array with elements of
 * templated type T.
 *
 * It simply holds information about what the each element holds and offers
 * some more advanced manipulation operations on them.
 *
 * Array dimensions are defined as the constructor parameters and the array
 * cannot be resized. If resize is needed then create new Array2 of the new
 * size and blit the old one to the new one. This allows you choose how to
 * expand or crop the array. Also internal resize method would not have been
 * any faster because of the internal representation of the data.
 *
 * Type T can be a basic type (char, int, bool, etc) or a class. If T is a class
 * then it must have working public constructor (that can be called with no
 * parameters), destructor, copy constructor and operator=. invert() also
 * requires that operator! is defined. toString*() and << operator with ostreams
 * also requires that operator<< is defined.
 *
 * Ideas for future development:
 * - Blitting only a part of source-array.
 * - BoundaryFill()
 * - Optimized version of the class for fundamental data types like int, char,
 *   bool, ...
 * - STL-container compatibility
 */
template<class T>
class Array2
{
private:
	int width; //!< Width of the array.
	int height; //!< Height of the array.

	/**
	 * @brief values 1-dimensional vector representing the 2-dimensional array
	 * of elements with type T.
	 */
	std::vector<T> values;

	/**
	 * @brief valuePadding Amount of padding per printed out value value when
	 * using toString*() or << with ostreams.
	 */
	size_t valuePadding;

public:
	typedef typename std::vector<T>::reference reference;
	typedef typename std::vector<T>::const_reference const_reference;

	Array2(int width, int height);

	~Array2();

	int getWidth() const;

	int getHeight() const;

	size_t getValuePadding() const;

	void setValuePadding(size_t valuePadding);

	void resizeAndReset(int width, int height, const T& value);

	bool isInside(int x, int y) const;

	bool isInside(size_t index) const;

	T getElementCopy(int x, int y) const;

	const_reference getElementRef(int x, int y) const;

	reference getElementRef(int x, int y);

	void setElement(int x, int y, const T& value);

	T getElementCopy(size_t index) const;

	const_reference getElementRef(size_t index) const;

	reference getElementRef(size_t index);

	void setElement(size_t index, const T& value);

	bool isEqualSize(const Array2<T>& another) const;

	template<class U> bool isEqualSize(const Array2<U>& another) const;

	bool isEqual(const Array2<T>& another) const;

	void clear(const T& value=T());

	void invert();

	void turnClockwise90();

	void turnCounterclockwise90();

	void flipHorizontally();

	void flipVertically();

	void blit(int x, int y, const Array2& source);

	void blit(int x1, int y1, int x2, int y2, const Array2& source);

	void blitMasked(int x, int y, const Array2& source,
		const Array2<bool>* const sourceMask,
		const Array2<bool>* const targetMask = nullptr);

	void blitMasked(int x1, int y1, int x2, int y2, const Array2& source,
		const Array2<bool>* const sourceMask,
		const Array2<bool>* const targetMask = nullptr);

	void blitSourceMasked(int x1, int y1, int x2, int y2,
		const Array2& source, const Array2<bool>& sourceMask);

	void blitTargetMasked(int x1, int y1, int x2, int y2,
		const Array2& source, const Array2<bool>& targetMask);

	void blitSourceTargetMasked(int x1, int y1, int x2, int y2,
		const Array2& source, const Array2<bool>& sourceMask,
		const Array2<bool>& targetMask);

	size_t floodFill4Directions(int x, int y, const T& value);

	size_t floodFill8Directions(int x, int y, const T& value);

	void printout(size_t valuePadding=3) const;

	bool refitInsideValidArea(int& x1, int& y1, int& x2, int& y2,
		const Array2& source, int& sourceX1, int& sourceY1);

public: // toString
	/**
	 * @brief toString Prints out the current Array2 to UString using given
	 *        padding for values. Printout of the value is padded
	 *        valuePadding characters with space if shorter).
	 *
	 * Type T must implement operator<< and not use endl for clean
	 * output.
	 *
	 * Note: uses internal variable valuePadding (default 3).
	 *
	 * @return Textual representation of the Array2.
	 */
	UString toString() const
	{
		std::ostringstream oss;
		for (int y=0; y<getHeight(); ++y)
		{
			oss << "[ ";
			for (int x=0; x<getWidth(); ++x)
			{
				oss << std::showbase << std::setw(valuePadding)
					<< values[getWidth() * y + x] << " ";
			}
			oss << "]" << std::endl;
		}

		return oss.str();
	}


	/**
	 * @brief toStringDebug Same as toString() but contains more debug
	 *	      oriented information.
	 * @return Textual representation of the Array2.
	 */
	UString toStringDebug() const
	{
		UString str("Array2<T>(\n");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const Array2<T>& right)
	{
		return ost << right.toString();
	}
};


/**
 * @brief Constructor.
 *
 * @param width [in] Width of the array [1..largest integer].
 * @param height [in] Height of the array [1..largest integer].
 */
template<class T>
inline Array2<T>::Array2(int width, int height) :
	width(width),
	height(height),
	values(width*height),
	valuePadding(3)
{
	VERSO_ASSERT("verso-base", width>0 && height>0);
}


/**
 * @brief Destructor.
 */
template<class T>
inline Array2<T>::~Array2()
{
}

/**
 * @return width of the array.
 */
template<class T>
inline int Array2<T>::getWidth() const
{
	return width;
}


/**
 * @return height of the array.
 */
template<class T>
inline int Array2<T>::getHeight() const
{
	return height;
}


/**
 * @brief Array2<T>::getValuePadding Gets the current amount of padding per
 *			printed out value value when using toString*() or << with ostreams.
 * @return amount of padding characters per printed out value.
 */
template<class T>
size_t Array2<T>::getValuePadding() const
{
	return valuePadding;
}


/**
 * @brief Array2<T>::setValuePadding Sets the current amount of padding per
 *			printed out value value when using toString*() or << with ostreams.
 * @param valuePadding amount of padding characters per printed out value.
 */
template<class T>
void Array2<T>::setValuePadding(size_t valuePadding)
{
	this->valuePadding = valuePadding;
}


/**
 * Resize the array to given size and resets with given value.
 *
 * @param width New width for the array.
 * @param height New height for the array.
 * @param value Value to reset the array with.
 */
template<class T>
void Array2<T>::resizeAndReset(int width, int height, const T& value)
{
	VERSO_ASSERT("verso-base", width>0 && height>0);

	this->width = width;
	this->height = height;
	values.resize(this->width * this->height);

	for (int i=0; i<this->width*this->height; i++)
		setElement(i, value);
}


/**
 * @return True is given coordinates (x,y) are inside the array,
 *         otherwise false. Values between [0..width-1], [0..height-1]
 *         are inside.
 * @param x [in] Requested X-coordinate.
 * @param y [in] Requested Y-coordinate.
 */
template<class T>
inline bool Array2<T>::isInside(int x, int y) const
{
	if (x >= 0 &&
		x < static_cast<int>(getWidth()) &&
		y >= 0 &&
		y < static_cast<int>(getHeight()))
	{
		return true;
	}
	else
		return false;
}


/**
 * @return True is given index is inside the array, otherwise false.
 *         Values between [0..width*height-1] are inside.
 * @param index [in] Requested index.
 */
template<class T>
inline bool Array2<T>::isInside(size_t index) const
{
	if (static_cast<int>(index) < getWidth() * getHeight())
		return true;
	else
		return false;
}


/**
 * @return copy the element in coordinates (x,y).
 * @param x [in] X-coordinate of the requested element [0..width-1].
 * @param y [in] Y-coordinate of the requested element [0..height-1].
 */
template<class T>
inline T Array2<T>::getElementCopy(int x, int y) const
{
	VERSO_ASSERT("verso-base", x >= 0);
	VERSO_ASSERT("verso-base", y >= 0);
	VERSO_ASSERT("verso-base", x < getWidth());
	VERSO_ASSERT("verso-base", y < getHeight());
	return values[getWidth() * y + x];
}


/**
 * @return const reference to the element in coordinates (x,y).
 *
 * Note that const_reference is the same as const T& except that is also
 * works with bool-references to the array used by Array2.
 *
 * @param x [in] X-coordinate of the requested element [0..width-1].
 * @param y [in] Y-coordinate of the requested element [0..height-1].
 */
template<class T>
inline typename Array2<T>::const_reference
	Array2<T>::getElementRef(int x, int y) const
{
	VERSO_ASSERT("verso-base", x >= 0);
	VERSO_ASSERT("verso-base", y >= 0);
	VERSO_ASSERT("verso-base", x < getWidth());
	VERSO_ASSERT("verso-base", y < getHeight());
	return values[getWidth() * y + x];
}


/**
 * @return mutable reference to the element in coordinates (x,y).
 *
 * Note that reference is the same as T& except that is also works with
 * bool-references to the array used by Array2.
 *
 * @param x [in] X-coordinate of the requested element [0..width-1].
 * @param y [in] Y-coordinate of the requested element [0..height-1].
 */
template<class T>
inline typename Array2<T>::reference Array2<T>::getElementRef(int x, int y)
{
	VERSO_ASSERT("verso-base", x >= 0);
	VERSO_ASSERT("verso-base", y >= 0);
	VERSO_ASSERT("verso-base", x < getWidth());
	VERSO_ASSERT("verso-base", y < getHeight());
	return values[getWidth() * y + x];
}


/**
 * @brief set new value for the element in given coordinates (x,y).
 *
 * @param x [in] X-coordinate of the element to set [0..width-1].
 * @param y [in] Y-coordinate of the element to set [0..height-1].
 * @param value [in] New value for the element.
 */
template<class T>
inline void Array2<T>::setElement(int x, int y, const T& value)
{
	VERSO_ASSERT("verso-base", x >= 0);
	VERSO_ASSERT("verso-base", y >= 0);
	VERSO_ASSERT("verso-base", x < getWidth());
	VERSO_ASSERT("verso-base", y < getHeight());
	this->values[getWidth() * y + x] = value;
}


/**
 * @return copy the element in the given index.
 * @param index [in] Index of the requested elemente in the array
 *        [0..width*height-1]. (x,y)-coordinate can be mapped using
 *        the formula y*width+x.
 */
template<class T>
inline T Array2<T>::getElementCopy(size_t index) const
{
	VERSO_ASSERT("verso-base", static_cast<int>(index) < getWidth() * getHeight());
	return values[index];
}

/**
 * @return const reference to the element in the given index.
 *
 * Note that const_reference is the same as const T& except that is also
 * works with bool-references to the array used by Array2.
 *
 * @param index [in] Index of the requested elemente in the array
 *        [0..width*height-1]. (x,y)-coordinate can be mapped using
 *        the formula y*width+x.
 */
template<class T>
inline typename Array2<T>::const_reference
	Array2<T>::getElementRef(size_t index) const
{
	VERSO_ASSERT("verso-base", static_cast<int>(index) < getWidth() * getHeight());
	return values[index];
}


/**
 * @return mutable reference to the element in the given index.
 *
 * Note that reference is the same as T& except that is also works with
 * bool-references to the array used by Array2.
 *
 * @param index [in] Index of the requested elemente in the array
 *        [0..width*height-1]. (x,y)-coordinate can be mapped using
 *        the formula y*width+x.
 */
template<class T>
inline typename Array2<T>::reference Array2<T>::getElementRef(size_t index)
{
	VERSO_ASSERT("verso-base", static_cast<int>(index) < getWidth() * getHeight());
	return values[index];
}


/**
 * @brief set new value for the element in the given index.
 *
 * @param index [in] Index to set in the array [0..width*height-1].
 *        (x,y)-coordinate can be mapped using formula y*width+x.
 * @param value [in] New value for the element.
 */
template<class T>
inline void Array2<T>::setElement(size_t index, const T& value)
{
	VERSO_ASSERT("verso-base", static_cast<int>(index) < getWidth() * getHeight());
	this->values[index] = value;
}


/**
 * @brief Check whether current and given Array2<T> are the same size.
 *        Type T must be the same for both!
 * @return true if current and given arrays are the same size
 *         (widths and heights match).
 * @param another [in] Another Array2<T> to checked against.
 */
template<class T>
inline bool Array2<T>::isEqualSize(const Array2& another) const
{
	if (getWidth() == another.getWidth() &&
		height == another.getHeight())
	{
		return true;
	}
	else
		return false;
}


/**
 * @brief Check whether current Array2<T> are the same size. Type T
 *        can differ from type U.
 *
 * Usage example:
 *   Verso::Array2<int> a1(3,3);
 *   Verso::Array2<bool> a2(3,3);
 *   a1.isEqualSize<bool>(a2); // returns true
 *
 * @return true if current and given arrays are the same size
 *         (widths and heights match).
 * @param another [in] Another Array2<T> to checked against.
 */
template<class T>
template<class U>
inline bool Array2<T>::isEqualSize(const Array2<U>& another) const
{
	if (getWidth() == another.getWidth() &&
		getHeight() == another.getHeight())
	{
		return true;
	}
	else
		return false;
}


/**
 * @return True is another array is the same size and has the same
 *         elements values when compared with operator=, otherwise
 *         false.
 * @param another [in] Another array to check equality against with.
 */
template<class T>
bool Array2<T>::isEqual(const Array2<T>& another) const
{
	if (isEqualSize(another) == true)
	{
		if (values.size() < another.values.size())
			return equal (values.begin(),
				values.end(),
				another.values.begin());
		else
			return equal (another.values.begin(),
				another.values.end(),
				values.begin());
	}

	return false;
}


/**
 * @brief Clears the array to given value.
 * @param value [in] Value to clear the elements with (default: T()).
 */
template<class T>
inline void Array2<T>::clear(const T& value)
{
	values.assign(values.size(), value);
}


/**
 * @brief Inverts the values of every element in this array.
 * Requires that operator! is defined for type T!
 */
template<class T>
void Array2<T>::invert()
{
	for (int i=0; i<getWidth() * getHeight(); ++i) {
		values[i] = !values[i];
	}
}


/**
 * @brief Turns the array 90 degrees clockwise.
 */
template<class T>
void Array2<T>::turnClockwise90()
{
	std::vector<T> temp = values;

	// Swap width and height
	if (getWidth() != getHeight())
	{
		int temp = getWidth();
		width = getHeight();
		height = temp;
	}

	// \todo optimize index calculation
	for (int y=0; y<getHeight(); ++y)
		for (int x=0; x<getWidth(); ++x)
		{
			int x_ = y;
			int y_ = (getWidth() - x - 1);

			values[getWidth() * y + x] =
				temp[getHeight() * y_ + x_];
		}
}


/**
 * @brief Turns the array 90 degrees counterclockwise.
 */
template<class T>
void Array2<T>::turnCounterclockwise90()
{
	std::vector<T> temp = values;

	// Swap width and height
	if (getWidth() != getHeight())
	{
		int temp = getWidth();
		width = getHeight();
		height = temp;
	}

	// \todo optimize index calculation
	for (int y=0; y<getHeight(); ++y)
		for (int x=0; x<getWidth(); ++x)
		{
			int x_ = (getHeight() - y - 1);
			int y_ = x;

			values[getWidth() * y + x] =
				temp[getHeight() * y_ + x_];
		}
}


/**
 * @brief Flips the array horizontally.
 */
template<class T>
void Array2<T>::flipHorizontally()
{
	// \todo optimize index calculation
	for (int y=0; y<getHeight(); ++y)
		for (int x=0; x<getWidth()/2; ++x)
		{
			int index1 = getWidth() * y + x;
			int index2 = getWidth() * y + getWidth() - x - 1;

			T temp = values[index1];
			values[index1] = values[index2];
			values[index2] = temp;
		}
}


/**
 * @brief Flips the array vertically.
 */
template<class T>
void Array2<T>::flipVertically()
{
	// \todo optimize index calculation
	for (int y=0; y<getHeight()/2; ++y)
		for (int x=0; x<getWidth(); ++x)
		{
			int index1 = getWidth() * y + x;
			int index2 = getWidth() * (getHeight() - y - 1) + x;

			T temp = values[index1];
			values[index1] = values[index2];
			values[index2] = temp;
		}
}


/**
 * @brief Blits the given Array2 (=source) to current instance
 *        (=target) at coordinates (x,y).
 *
 * If blit does not fit the area where it was requested to be blitted
 * it will be cropped to fit.
 *
 * @param x [in] Top-left X-coordinate where to blit the given Array2.
 * @param y [in] Top-left Y-coordinate where to blit the given Array2.
 * @param source [in] Array2 to blit.
 */
template<class T>
inline void Array2<T>::blit(int x, int y, const Array2& source)
{
	blit(x, y,
		x + source.getWidth() - 1,
		y + source.getHeight() - 1,
		source);
}


/**
 * @brief Blits the given Array2 (=source) to current instance
 *        (=target) at coordinates (x1,y1) to (x2,y2).
 *
 * If blit does not fit the area where it was requested to be blitted
 * it will be cropped to fit.
 *
 * If (x1,y1)-(x2,y2) is bigger than the source Array2 then only the
 * source Array2 size is blitted.
 *
 * @param x1 [in] Top-left X-coordinate where to blit the given Array2.
 * @param y1 [in] Top-left Y-coordinate where to blit the given Array2.
 * @param x2 [in] Bottom-right X-coordinate where to blit the given
 *        Array2.
 * @param y2 [in] Bottom-right Y-coordinate where to blit the given
 *        Array2.
 * @param source [in] Array2 to blit.
 */
template<class T>
void Array2<T>::blit(int targetX1, int targetY1, int targetX2, int targetY2,
	const Array2& source)
{
	int sourceX1 = 0, sourceY1 = 0;

	//cout <<
	//	"blit (" << showbase << setw(4) << targetX1 << "," << showbase << setw(4) << targetY1 << ")-" << "(" <<
	//	showbase << setw(4) << targetX2 << "," << showbase << setw(4) << targetY2 << ") source (" << showbase << setw(4) << sourceX1 << "," << showbase << setw(4) << sourceY1 << ")" <<
	//	"source size " << source.getWidth() << "x" << source.getHeight() << endl;

	if (refitInsideValidArea(targetX1, targetY1, targetX2, targetY2,
		source, sourceX1, sourceY1) == false)
	{
		return;
	}

	//cout << showbase << setw(4) <<
	//	" Aft.(" << showbase << setw(4) << targetX1 << "," << showbase << setw(4) << targetY1 << ")-" << "(" <<
	//	showbase << setw(4) << targetX2 << "," << showbase << setw(4) << targetY2 << ") source (" << showbase << setw(4) << sourceX1 << "," << showbase << setw(4) << sourceY1 << ")" <<
	//	"source size " << source.getWidth() << "x" << source.getHeight() << endl;

	// \todo optimize index calculation
	for (int targetY=targetY1, fromY=sourceY1;
		targetY<=targetY2; ++targetY, ++fromY)
	{
		for (int targetX=targetX1, fromX=sourceX1;
			targetX<=targetX2; ++targetX, ++fromX)
		{
			VERSO_ASSERT("verso-base", isInside(targetX, targetY) == true);
			values[getWidth() * targetY + targetX] =
				source.values[source.getWidth() * fromY + fromX];
		}
	}
}


/**
 * @brief Blits the given Array2 (=source) to current instance
 *        (=target) at coordinates (x,y) using no mask or the
 *        sourceMask and/or targetMask.
 *
 * If blit does not fit the area where it was requested to be blitted
 * it will be cropped to fit.
 *
 * True value at the same coordinates in the source/targetMask as
 * source/target means that element is blitted. False means that
 * element is not blitted.
 *
 * @param x [in] Top-left X-coordinate where to blit the given Array2.
 * @param y [in] Top-left Y-coordinate where to blit the given Array2.
 * @param source [in] Array2 to blit.
 * @param sourceMask [in] Pointer to a mask for source array or value 0
 *        if no mask is needed (default 0). Source-mask size must match
 *        source-array size.
 * @param targetMask [in] Pointer to a mask for target array or value 0
 *        if no mask is needed. Target-mask size must match targetarray
 *        size.
 */
template<class T>
inline void Array2<T>::blitMasked(int x, int y, const Array2& source,
	const Array2<bool>* const sourceMask,
	const Array2<bool>* const targetMask)
{
	blitMasked(x, y,
		x + source.getWidth() - 1,
		y + source.getHeight() - 1,
		source, sourceMask, targetMask);
}


/**
 * @brief Blits the given Array2 (=source) to current instance
 *        (=target) at coordinates (x1,y1) to (x2,y2) using no mask or
 *        the sourceMask and/or targetMask.
 *
 * If blit does not fit the area where it was requested to be blitted
 * it will be cropped to fit.
 *
 * If (x1,y1)-(x2,y2) is bigger than the source Array2 then only the
 * source Array2 size is blitted.
 *
 * True value at the same coordinates in the source/targetMask as
 * source/target means that element is blitted. False means that
 * element is not blitted.
 *
 * @param x1 [in] Top-left X-coordinate where to blit the given Array2.
 * @param y1 [in] Top-left Y-coordinate where to blit the given Array2.
 * @param x2 [in] Bottom-right X-coordinate where to blit the given
 *        Array2.
 * @param y2 [in] Bottom-right Y-coordinate where to blit the given
 *        Array2.
 * @param source [in] Array2 to blit.
 * @param sourceMask [in] Pointer to a mask for source array or value 0
 *        if no mask is needed. Source-mask size must match sourcearray
 *        size.
 * @param targetMask [in] Pointer to a mask for target array or value 0
 *        if no mask is needed (default 0). Target-mask size must match
 *        target-array size.
 */
template<class T>
void Array2<T>::blitMasked(int targetX1, int targetY1, int targetX2,
	int targetY2, const Array2& source, const Array2<bool>* const sourceMask,
	const Array2<bool>* const targetMask)
{
	if (targetMask == nullptr)
	{
		if (sourceMask == nullptr)
		{
			blit(targetX1, targetY1, targetX2, targetY2, source);
		}
		else { // sourceMask != 0
			VERSO_ASSERT("verso-base", source.isEqualSize<bool>(*sourceMask) == true);

			blitSourceMasked(targetX1, targetY1, targetX2, targetY2,
				source, *sourceMask);
		}
	}
	else { // targetMask != nullptr
		if (sourceMask == nullptr)
		{
			VERSO_ASSERT("verso-base", isEqualSize<bool>(*targetMask) == true);

			blitTargetMasked(targetX1, targetY1, targetX2, targetY2,
				source, *targetMask);
		}
		else { // sourceMask != 0
			VERSO_ASSERT("verso-base", source.isEqualSize<bool>(*sourceMask) == true);
			VERSO_ASSERT("verso-base", isEqualSize<bool>(*targetMask) == true);

			blitSourceTargetMasked(targetX1, targetY1, targetX2, targetY2,
				source, *sourceMask, *targetMask);
		}
	}
}


/**
 * @brief Blits the given Array2 (=source) to current instance
 *        (=target) at coordinates (x1,y1) to (x2,y2) using the
 *        sourceMask.
 *
 * If blit does not fit the area where it was requested to be blitted
 * it will be cropped to fit.
 *
 * If (x1,y1)-(x2,y2) is bigger than the source Array2 then only the
 * source Array2 size is blitted.
 *
 * True value at the same coordinates in the sourceMask as source means
 * that element is blitted. False means that element is not blitted.
 *
 * @param x1 [in] Top-left X-coordinate where to blit the given Array2.
 * @param y1 [in] Top-left Y-coordinate where to blit the given Array2.
 * @param x2 [in] Bottom-right X-coordinate where to blit the given
 *        Array2.
 * @param y2 [in] Bottom-right Y-coordinate where to blit the given
 *        Array2.
 * @param source [in] Array2 to blit.
 * @param sourceMask [in] Mask for source array. Source-mask size must
 *        match source-array size.
 */
template<class T>
void Array2<T>::blitSourceMasked(int targetX1, int targetY1, int targetX2,
	int targetY2, const Array2& source, const Array2<bool>& sourceMask)
{
	int sourceX1 = 0, sourceY1 = 0;

	if (refitInsideValidArea(targetX1, targetY1, targetX2, targetY2,
		source, sourceX1, sourceY1) == false)
	{
		return;
	}

	// \todo optimize index calculation
	for (int targetY=targetY1, fromY=sourceY1;
		targetY<=targetY2; ++targetY, ++fromY)
	{
		for (int targetX=targetX1, fromX=sourceX1;
			targetX<=targetX2; ++targetX, ++fromX)
		{
			size_t sourceIndex = source.getWidth() * fromY + fromX;
			if (sourceMask.getElementRef(sourceIndex) != 0)
			{
				setElement(targetX, targetY,
					source.getElementRef(sourceIndex));
			}
		}
	}
}


/**
 * @brief Blits the given Array2 (=source) to current instance
 *        (=target) at coordinates (x1,y1) to (x2,y2) using the
 *        targetMask.
 *
 * If blit does not fit the area where it was requested to be blitted
 * it will be cropped to fit.
 *
 * If (x1,y1)-(x2,y2) is bigger than the source Array2 then only the
 * source Array2 size is blitted.
 *
 * True value at the same coordinates in the targetMask as target means
 * that element is blitted. False means that element is not blitted.
 *
 * @param x1 [in] Top-left X-coordinate where to blit the given Array2.
 * @param y1 [in] Top-left Y-coordinate where to blit the given Array2.
 * @param x2 [in] Bottom-right X-coordinate where to blit the given
 *        Array2.
 * @param y2 [in] Bottom-right Y-coordinate where to blit the given
 *        Array2.
 * @param source [in] Array2 to blit.
 * @param targetMask [in] Mask for target array. Target-mask size must
 *        match target-array size.
 */
template<class T>
void Array2<T>::blitTargetMasked(int targetX1, int targetY1, int targetX2,
	int targetY2, const Array2& source, const Array2<bool>& targetMask)
{
	int sourceX1 = 0, sourceY1 = 0;

	if (refitInsideValidArea(targetX1, targetY1, targetX2, targetY2,
		source, sourceX1, sourceY1) == false)
	{
		return;
	}

	// \todo optimize index calculation
	for (int targetY=targetY1, fromY=sourceY1;
		targetY<=targetY2; ++targetY, ++fromY)
	{
		for (int targetX=targetX1, fromX=sourceX1;
			targetX<=targetX2; ++targetX, ++fromX)
		{
			size_t targetIndex = getWidth() * targetY + targetX;
			if (targetMask.getElementRef(targetIndex) != 0)
			{
				setElement(targetIndex,
					source.getElementRef(fromX, fromY));
			}
		}
	}
}


/**
 * @brief Blits the given Array2 (=source) to current instance
 *        (=target) at coordinates (x1,y1) to (x2,y2) using the
 *        sourceMask and the targetMask.
 *
 * If blit does not fit the area where it was requested to be blitted
 * it will be cropped to fit.
 *
 * If (x1,y1)-(x2,y2) is bigger than the source Array2 then only the
 * source Array2 size is blitted.
 *
 * True value at the same coordinates in the source/targetMask as
 * source/target means that element is blitted. False means that
 * element is not blitted.
 *
 * @param x1 [in] Top-left X-coordinate where to blit the given Array2.
 * @param y1 [in] Top-left Y-coordinate where to blit the given Array2.
 * @param x2 [in] Bottom-right X-coordinate where to blit the given
 *        Array2.
 * @param y2 [in] Bottom-right Y-coordinate where to blit the given
 *        Array2.
 * @param source [in] Array2 to blit.
 * @param sourceMask [in] Mask for source array. Source-mask size must
 *        match source-array size.
 * @param targetMask [in] Mask for target array. Target-mask size must
 *        match target-array size.
 */
template<class T>
void Array2<T>::blitSourceTargetMasked(int targetX1, int targetY1,
	int targetX2, int targetY2, const Array2& source,
	const Array2<bool>& sourceMask, const Array2<bool>& targetMask)
{
	int sourceX1 = 0, sourceY1 = 0;

	if (refitInsideValidArea(targetX1, targetY1, targetX2, targetY2,
		source, sourceX1, sourceY1) == false)
	{
		return;
	}

	// \todo optimize index calculation
	for (int targetY=targetY1, fromY=sourceY1;
		targetY<=targetY2; ++targetY, ++fromY)
	{
		for (int targetX=targetX1, fromX=sourceX1;
			targetX<=targetX2; ++targetX, ++fromX)
		{
			size_t sourceIndex = source.getWidth() * fromY + fromX;
			if (sourceMask.getElementRef(sourceIndex) != 0)
			{
				size_t targetIndex = getWidth() * targetY + targetX;
				if (targetMask.getElementRef(targetIndex) != 0)
				{
					setElement(targetIndex,
						source.getElementRef(sourceIndex));
				}
			}
		}
	}
}


/**
 * @brief 4-way expanding floodfill algorithm searching the value in
 *        defined at (x,y) and replacing them with given value.
 *
 * @return Number of replaced values (=size of the area). If given
 *         coordinates are out of bounds then method will return 0.
 * @param x [in] Starting point for the flood fill (X-coordinate).
 * @param y [in] Starting point for the flood fill (Y-coordinate).
 * @param value [in] Value to replace the area with.
 */
template<class T>
size_t Array2<T>::floodFill4Directions(int x, int y, const T& newValue)
{
	if (isInside(x, y) == false)
		return 0;

	// \todo better (=faster) implementation for queue?
	std::queue< std::pair<int, int> > elementsToProcess;
	elementsToProcess.push(std::pair<int, int>(x, y));

	// \todo would it be possible to use: const_reference targetValue = getElementRef(x, y); and get == operator working on the value?
	T targetValue = getElementCopy(x, y);

	size_t replacedValues = 0;
	while (!elementsToProcess.empty())
	{
		int nX = elementsToProcess.front().first;
		int nY = elementsToProcess.front().second;
		elementsToProcess.pop();

		if (getElementRef(nX, nY) == targetValue)
		{
			int west = nX;
			while ((west-1 >= 0) &&
				getElementRef(west-1, nY) == targetValue)
			{
				west--;
			}

			int east = nX;
			while ((east+1 < static_cast<int>(getWidth())) &&
				getElementRef(east+1, nY) == targetValue)
			{
				east++;
			}

			for (int i=west; i<=east; ++i)
			{
				setElement(i, nY, newValue);
				replacedValues++;

				if ((nY-1 >= 0) &&
					getElementRef(i, nY-1) == targetValue)
				{
					elementsToProcess.push(std::pair<int, int>(i, nY-1));
				}

				if ((nY+1 < static_cast<int>(getHeight())) &&
					getElementRef(i, nY+1) == targetValue)
				{
					elementsToProcess.push(std::pair<int, int>(i, nY+1));
				}
			}
		}
	}

	return replacedValues;
}


/**
 * @brief 8-way expanding floodfill algorithm searching the value in
 *        defined at (x,y) and replacing them with given value.
 *
 * @return Number of replaced values (=size of the area). If given
 *         coordinates are out of bounds then method will return 0.
 * @param x [in] Starting point for the flood fill (X-coordinate).
 * @param y [in] Starting point for the flood fill (Y-coordinate).
 * @param value [in] Value to replace the area with.
 */
template<class T>
size_t Array2<T>::floodFill8Directions(int x, int y, const T& newValue)
{
	if (isInside(x, y) == false)
		return 0;

	// \todo better (=faster) implementation for queue?
	// \todo more intelligent pixel chooser.
	std::queue< std::pair<int, int> > elementsToProcess;
	elementsToProcess.push(std::pair<int, int>(x, y));

	// \todo would it be possible to use: const_reference targetValue = getElementRef(x, y); and get == operator working on the value?
	T targetValue = getElementCopy(x, y);

	size_t replacedValues = 0;
	while (!elementsToProcess.empty())
	{
		int nX = elementsToProcess.front().first;
		int nY = elementsToProcess.front().second;
		elementsToProcess.pop();

		if (getElementRef(nX, nY) == targetValue)
		{
			int west = nX;
			while ((west-1 >= 0) &&
				getElementRef(west-1, nY) == targetValue)
			{
				west--;
			}

			int east = nX;
			while ((east+1 < static_cast<int>(getWidth())) &&
				getElementRef(east+1, nY) == targetValue)
			{
				east++;
			}

			for (int i=west; i<=east; ++i)
			{
				setElement(i, nY, newValue);
				replacedValues++;

				if ((i-1 >= 0) &&
					(nY-1 >= 0) &&
					getElementRef(i-1, nY-1) == targetValue)
				{
					elementsToProcess.push(std::pair<int, int>(i-1, nY-1));
				}

				if ((nY-1 >= 0) &&
					getElementRef(i, nY-1) == targetValue)
				{
					elementsToProcess.push(std::pair<int, int>(i, nY-1));
				}

				if ((i+1 < static_cast<int>(getWidth())) &&
					(nY-1 >= 0) &&
					getElementRef(i+1, nY-1) == targetValue)
				{
					elementsToProcess.push(std::pair<int, int>(i+1, nY-1));
				}

				if ((i-1 >= 0) &&
					(nY+1 < static_cast<int>(getHeight())) &&
					getElementRef(i-1, nY+1) == targetValue)
				{
					elementsToProcess.push(std::pair<int, int>(i-1, nY+1));
				}

				if ((nY+1 < static_cast<int>(getHeight())) &&
					getElementRef(i, nY+1) == targetValue)
				{
					elementsToProcess.push(std::pair<int, int>(i, nY+1));
				}

				if ((i+1 < static_cast<int>(getWidth())) &&
					(nY+1 < static_cast<int>(getHeight())) &&
					getElementRef(i+1, nY+1) == targetValue)
				{
					elementsToProcess.push(std::pair<int, int>(i+1, nY+1));
				}
			}
		}
	}

	return replacedValues;
}


/**
 * @brief Refits the given area to fit both the current array and the
 *        given one.
 *
 * This methods is useful when you wan't to calculate the area a
 * rectangle (x1, y1)-(x2,y2) can occupy in current array when it's
 * top-left corner is placed at (x1,y1).
 *
 * This method doesn't support situations where only part of source
 * array is requested to blit, so you cannot use (sourceX1, sourceY1)
 * as input, they'll be overwritten.
 *
 * Usage: Give requested rectangle of (x1,y1)-(x2,y2) and after function
 * returns given values top-left coordinates for source (sourceX,
 * sourceY) are adjusted to fit. Possibly changed width and height can
 * be calculated using the formula:
 *   width = x2 - x1 + 1;
 *   height = y2 - y1 + 1;
 *
 * @return False is width or height of the given area are less than one
 *         or given rectangle was fully out of bounds, otherwise true.
 * @param x1 [in,out] Top-left X-coordinate to adjust for current array.
 * @param y1 [in,out] Top-left Y-coordinate to adjust for current array.
 * @param x2 [in,out] Bottom-right X-coordinate to adjust for current
 *        array.
 * @param y2 [in,out] Bottom-right Y-coordinate to adjust for current
 *        array.
 * @param source [in] Array2 to check against.
 * @param sourceX1 [out] Top-left X-coordinate to adjust (default 0,
 *        might change if source array does not fit fully).
 * @param sourceY1 [out] Top-left Y-coordinate to adjust (default 0,
 *        might change if source array does not fit fully).
 */
template<class T>
bool Array2<T>::refitInsideValidArea(int& targetX1, int& targetY1,
	int& targetX2, int& targetY2, const Array2& source,
	int& sourceX1, int& sourceY1)
{
	int w = targetX2 - targetX1 + 1;
	int h = targetY2 - targetY1 + 1;

	if (w < 1 || h < 1)
		return false;

	int tempTargetX1 = targetX1;
	int tempTargetY1 = targetY1;
	int tempTargetX2 = targetX2;
	int tempTargetY2 = targetY2;
	int tempSourceX1 = sourceX1;
	int tempSourceY1 = sourceY1;

	if (w > static_cast<int>(source.getWidth()))
		tempTargetX2 -= w - source.getWidth();

	if (h > static_cast<int>(source.getHeight()))
		tempTargetY2 -= h - source.getHeight();

	tempSourceX1 = 0;
	tempSourceY1 = 0;

	// Make sure top-left corner is inside (X-coordinate)
	if (tempTargetX1 < 0)
	{
		tempSourceX1 += -tempTargetX1;
		tempTargetX1 = 0;
	}

	// Make sure top-left corner is inside (Y-coordinate)
	if (tempTargetY1 < 0)
	{
		tempSourceY1 += -tempTargetY1;
		tempTargetY1 = 0;
	}

	// Make sure bottom-right corner is inside (X-coordinate)
	if (tempTargetX2 >= static_cast<int>(getWidth()))
	{
		tempTargetX2 = getWidth() - 1;
	}

	// Make sure bottom-right corner is inside (Y-coordinate)
	if (tempTargetY2 >= static_cast<int>(getHeight()))
	{
		tempTargetY2 = getHeight() - 1;
	}

	if (tempTargetX1 > tempTargetX2 || tempTargetY1 > tempTargetY2)
		return false;

	if (source.isInside(tempSourceX1, tempSourceY1) == false)
		return false;

	targetX1 = tempTargetX1;
	targetY1 = tempTargetY1;
	targetX2 = tempTargetX2;
	targetY2 = tempTargetY2;
	sourceX1 = tempSourceX1;
	sourceY1 = tempSourceY1;

	return true;
}


} // End namespace Verso

#endif // End header guard

