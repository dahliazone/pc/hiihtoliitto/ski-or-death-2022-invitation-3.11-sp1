#ifndef VERSO_BASE_MATH_FLOATKEYFRAMES_HPP
#define VERSO_BASE_MATH_FLOATKEYFRAMES_HPP

#include <Verso/Math/Easing.hpp>
#include <Verso/Math/InterpolationType.hpp>
#include <Verso/Math/Interpolation.hpp>
#include <Verso/Math/FloatKeyframeElement.hpp>

namespace Verso {


class FloatKeyframes
{
public:
	EasingType easingType;
	InterpolationType interpolationType;
	bool looping;
	std::vector<FloatKeyframeElement> data;

public:
	FloatKeyframes() :
		easingType(EasingType::Linear),
		interpolationType(InterpolationType::Undefined),
		looping(false),
		data()
	{
	}


	explicit FloatKeyframes(
			const EasingType& easingType,
			const InterpolationType& interpolationType,
			bool looping,
			const std::vector<FloatKeyframeElement>& data) :
		easingType(easingType),
		interpolationType(interpolationType),
		looping(looping),
		data(data)
	{
	}


	explicit FloatKeyframes(float singleValue) :
		easingType(EasingType::Linear),
		interpolationType(InterpolationType::Constant),
		looping(false),
		data()
	{
		data.push_back(FloatKeyframeElement(0.0f, singleValue));
	}


	FloatKeyframes(const FloatKeyframes& original) :
		easingType(original.easingType),
		interpolationType(original.interpolationType),
		looping(original.looping),
		data(original.data)
	{
	}


	FloatKeyframes(FloatKeyframes&& original) noexcept :
		easingType(std::move(original.easingType)),
		interpolationType(std::move(original.interpolationType)),
		looping(std::move(original.looping)),
		data(std::move(original.data))
	{
	}


	FloatKeyframes& operator =(const FloatKeyframes& original)
	{
		if (this != &original) {
			easingType = original.easingType;
			interpolationType = original.interpolationType;
			looping = original.looping;
			data = original.data;
		}
		return *this;
	}


	FloatKeyframes& operator =(FloatKeyframes&& original) noexcept
	{
		if (this != &original) {
			easingType = std::move(original.easingType);
			interpolationType = std::move(original.interpolationType);
			looping = std::move(original.looping);
			data = std::move(original.data);
		}
		return *this;
	}


	~FloatKeyframes()
	{
	}


public:
	void add(const FloatKeyframeElement& element)
	{
		if (data.size() > 0 && element.seconds <= data[data.size() - 1].seconds) {
			UString error("FloatKeyframe seconds at index ");
			error.append2(data.size() - 1);
			error += " was not bigger than previous value!";
			VERSO_ILLEGALPARAMETERS("verso-3d", error.c_str(), "");
		}
		data.push_back(element);
	}


	void clear()
	{
		data.clear();
	}


	FloatKeyframeElement& operator[](size_t index)
	{
		return data[index];
	}


	const FloatKeyframeElement& operator[](size_t index) const
	{
		return data[index];
	}


	float getValueInterpolated(double seconds) const
	{
		const float lengthSeconds = data[data.size() - 1].seconds;
		if (looping == true && seconds > lengthSeconds) {
			seconds = seconds - floor(seconds / lengthSeconds) * lengthSeconds;
		}
		float delta = static_cast<float>(seconds) / lengthSeconds;
		if (delta > 1.0f) {
			delta = 1.0f;
		}
		const float deltaEased = Easing::byType(easingType, delta);
		const float secondsEased = deltaEased * lengthSeconds;

		switch (interpolationType)
		{
		case InterpolationType::Constant:
			return getValueInterpolatedLinear(secondsEased); // handles single value case
		case InterpolationType::Linear:
			return getValueInterpolatedLinear(secondsEased);
		case InterpolationType::QuadraticBezier:
			return getValueInterpolatedQuadraticBezier(secondsEased);
		default:
			std::cout << "Unsupported InterpolationType=" << interpolationTypeToString(interpolationType) << std::endl;
			VERSO_FAIL("verso-3d");
		}
	}

private:
	float getValueInterpolatedLinear(double seconds) const
	{
		size_t length = data.size();

		if (length == 0) {
			return 0.0f;
		}
		else if (length == 1) {
			return data[0].value;
		}

		for (size_t i = 0; i < length - 1; ++i) {
			if (seconds >= data[i].seconds && seconds <= data[i + 1].seconds) {
				float prevValue(data[i].value);
				float nextValue(data[i + 1].value);

				float innerElapsedSeconds = static_cast<float>(seconds) - data[i].seconds;
				float innerLengthSeconds = data[i+1].seconds - data[i].seconds;
				float innerSeconds = innerElapsedSeconds / innerLengthSeconds;

				return Interpolation::lerp<float>(prevValue, nextValue, innerSeconds);
			}
		}

		return data[length - 1].value;
	}


	float getValueInterpolatedQuadraticBezier(double seconds) const
	{
		size_t length = data.size();

		if (length == 0) {
			return 0.0f;
		}
		else if (length == 1) {
			return data[0].value;
		}

		for (size_t i = 0; i < length - 1; ++i) {
			if (seconds >= data[i].seconds && seconds <= data[i + 1].seconds) {
				float prevValue(data[i].value);
				float nextValue(data[i + 1].value);
				float control1Value(data[i].control1);

				float innerElapsedSeconds = static_cast<float>(seconds) - data[i].seconds;
				float innerLengthSeconds = data[i+1].seconds - data[i].seconds;
				float innerSeconds = innerElapsedSeconds / innerLengthSeconds;

				return Interpolation::quadraticBezier<float>(prevValue, control1Value, nextValue, innerSeconds);
			}
		}

		return data[length - 1].value;
	}

public:
	EasingType getEasingType() const
	{
		return easingType;
	}


	void setEasingType(const EasingType& easingType)
	{
		this->easingType = easingType;
	}


	InterpolationType getInterpolationType() const
	{
		return interpolationType;
	}


	void setInterpolationType(const InterpolationType& interpolationType)
	{
		this->interpolationType = interpolationType;
	}


	bool isLooping() const
	{
		return looping;
	}


	void setLooping(bool looping)
	{
		this->looping = looping;
	}


	size_t size() const
	{
		return data.size();
	}


public: // toString
	UString toString(const UString& newLinePadding) const
	{
		if (data.size() == 1) {
			UString str;
			str.append2(data[0].value);
			return str;
		}

		UString str;
		str += "{\n" + newLinePadding + "  easingType=\"";
		str += easingTypeToString(easingType);
		str += "\", interpolationType=";
		str += interpolationTypeToString(interpolationType);
		str += "\", looping=";
		if (looping == true) {
			str += "true";
		}
		else {
			str += "false";
		}
		str += ",\n" + newLinePadding + "  data=[\n";
		for (const auto& value : data) {
			str += newLinePadding + "    ";
			str += value.toString();
			str += ",\n";
		}
		str += newLinePadding + "  ].length=";
		str.append2(data.size());
		str += "\n" + newLinePadding + "}";
		return str;
	}


	UString toStringDebug(const UString& newLinePadding) const
	{
		UString str("FloatKeyframes(");
		str += toString(newLinePadding);
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const FloatKeyframes& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

