#ifndef VERSO_BASE_MATH_CAMERAKEYFRAMEELEMENT_HPP
#define VERSO_BASE_MATH_CAMERAKEYFRAMEELEMENT_HPP

#include <Verso/Math/Vector3.hpp>

namespace Verso {


class CameraKeyframeElement
{
public:
	float seconds;
	Vector3f position;
	Vector3f direction;
	Vector3f up;

public:
	CameraKeyframeElement(float seconds, const Vector3f& position, const Vector3f& direction, const Vector3f& up) :
		seconds(seconds),
		position(position),
		direction(direction),
		up(up)
	{
		this->direction.normalize();
		this->up.normalize();
	}


public: // toString
	UString toString() const
	{
		UString str("seconds=");
		str.append2(seconds);
		str += ", position=";
		str += position.toString();
		str += ", direction=";
		str += direction.toString();
		str += ", up=";
		str += up.toString();
		return str;
	}


	UString toStringDebug() const
	{
		UString str("CameraKeyframeElement(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const CameraKeyframeElement& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso

#endif // End header guard

