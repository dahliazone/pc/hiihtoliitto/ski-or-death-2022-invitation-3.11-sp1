#ifndef VERSO_BASE_MATH_EASINGTYPE_HPP
#define VERSO_BASE_MATH_EASINGTYPE_HPP

#include <Verso/System/UString.hpp>

namespace Verso {


enum class EasingType
{
	Undefined,
	Linear,
	EaseIn,
	EaseOut,
	EaseInOut,
	QuadraticIn,
	QuadraticOut,
	QuadraticInOut
};


inline UString easingTypeToString(const EasingType& type)
{
	if (type == EasingType::Undefined)
		return "Undefined";
	else if (type == EasingType::Linear)
		return "Linear";
	else if (type == EasingType::EaseIn)
		return "EaseIn";
	else if (type == EasingType::EaseOut)
		return "EaseOut";
	else if (type == EasingType::EaseInOut)
		return "EaseInOut";
	else if (type == EasingType::QuadraticIn)
		return "QuadraticIn";
	else if (type == EasingType::QuadraticOut)
		return "QuadraticOut";
	else if (type == EasingType::QuadraticInOut)
		return "QuadraticInOut";
	else
		return "Unknown value";
}


// returns EasingType::Undefined for any erroneuos string
inline EasingType stringToEasingType(const UString& str)
{
	if (str.equals("Linear"))
		return EasingType::Linear;
	else if (str.equals("EaseIn"))
		return EasingType::EaseIn;
	else if (str.equals("EaseOut"))
		return EasingType::EaseOut;
	else if (str.equals("EaseInOut"))
		return EasingType::EaseInOut;
	else if (str.equals("QuadraticIn"))
		return EasingType::QuadraticIn;
	else if (str.equals("QuadraticOut"))
		return EasingType::QuadraticOut;
	else if (str.equals("QuadraticInOut"))
		return EasingType::QuadraticInOut;
	else
		return EasingType::Undefined;
}


} // End namespace Verso

#endif // End header guard

