#ifndef VERSO_BASE_MATH_RECT_HPP
#define VERSO_BASE_MATH_RECT_HPP

#include <Verso/Math/Vector2.hpp>

namespace Verso {


template<typename T>
class Rect
{
public:
	Vector2<T> pos;
	Vector2<T> size;

public:
	Rect<T>() :
		pos(),
		size()
	{
	}


	Rect<T>(const Vector2<T>& pos, const Vector2<T>& size) :
		pos(pos),
		size(size)
	{
	}


	Rect<T>(const T& x, const T& y, const Vector2<T>& size) :
		pos(x, y),
		size(size)
	{
	}


	Rect<T>(const T& x, const T& y, const T& width, const T& height) :
		pos(x, y),
		size(width, height)
	{
	}


	Rect<T>(const Rect<T>& original) :
		pos(original.pos),
		size(original.size)
	{
	}


	Rect<T>(Rect<T>&& original) noexcept :
		pos(std::move(original.pos)),
		size(std::move(original.size))
	{
	}


	Rect<T>& operator =(const Rect<T>& original)
	{
		if (this != &original) {
			pos = original.pos;
			size = original.size;
		}
		return *this;
	}


	Rect<T>& operator =(Rect<T>&& original) noexcept
	{
		if (this != &original) {
			pos = std::move(original.pos);
			size = std::move(original.size);
		}
		return *this;
	}


	~Rect<T>()
	{
	}


public:
	void set(const Vector2<T>& pos, const Vector2<T>& size)
	{
		this->pos = pos;
		this->size = size;
	}


	void set(const T& x, const T& y, const Vector2<T>& size)
	{
		this->x = x;
		this->y = y;
		this->size = size;
	}


	void set(const T& x, const T& y, const T& width, const T& height)
	{
		this->x = x;
		this->y = y;
		this->size = Vector2<T>(width, height);
	}


	void setPos(const Vector2<T>& pos)
	{
		this->pos = pos;
	}


	void setPos(const T& x, const T& y)
	{
		this->pos.x = x;
		this->pos.y = y;
	}


	void setSize(const Vector2<T>& size)
	{
		this->size = size;
	}


	void setSize(const T& width, const T& height)
	{
		this->size.x = width;
		this->size.y = height;
	}


	T getX1() const
	{
		return pos.x;
	}


	T getY1() const
	{
		return pos.y;
	}


	T getX2() const
	{
		return pos.x + size.x - 1;
	}


	T getY2() const
	{
		return pos.y + size.y - 1;
	}


	T calculateArea() const
	{
		return size.calculateArea();
	}


	bool isInside(const Vector2<T> point) const
	{
		if (point.x >= getX1() && point.y >= getY1() &&
				point.x <= getX2() && point.y <= getY2()) {
			return true;
		}
		else {
			return false;
		}
	}


public: // toString
	UString toString() const
	{
		UString str;
		str.append2(pos.toString());
		str += ", [";
		str.append2(size.x);
		str += " x ";
		str.append2(size.y);
		str += "]";
		return str;
	}


	UString toStringDebug() const
	{
		UString str("Rect<T>(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const Rect<T>& right)
	{
		return ost << right.toString("");
	}
};


typedef Rect<int> Recti;
typedef Rect<unsigned int> Rectu;
typedef Rect<float> Rectf;


template<typename T>
inline bool operator ==(const Rect<T>& left, const Rect<T>& right)
{
	return (left.size == right.size) &&
			(left.pos == right.pos);
}


template<typename T>
inline bool operator !=(const Rect<T>& left, const Rect<T>& right)
{
	return !(left == right);
}


} // End namespace Verso

#endif // End header guard

