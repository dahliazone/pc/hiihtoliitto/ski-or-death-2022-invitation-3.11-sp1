#ifndef VERSO_BASE_MATH_VECTOR3_HPP
#define VERSO_BASE_MATH_VECTOR3_HPP

#include <Verso/Math/Math.hpp>
#include <Verso/Math/Degree.hpp>
#include <Verso/Math/Radian.hpp>
#include <Verso/Math/Vector2.hpp>
#include <Verso/System/UString.hpp>
#include <ostream>

namespace Verso {


template<typename T>
class Vector3
{
public:
	T x;
	T y;
	T z;

public:
	Vector3() :
		x(0), y(0), z(0)
	{
	}


	Vector3(T value) :
		x(value), y(value), z(value)
	{
	}


	Vector3(const Vector2<T>& value) :
		x(value.x), y(value.y), z(0)
	{
	}


	Vector3(const Vector3<T>& value) :
		x(value.x), y(value.y), z(value.z)
	{
	}


	Vector3(const T& x, const T& y, const T& z = 0.0f) :
		x(x), y(y), z(z)
	{
	}


	Vector3& operator =(const Vector3& original)
	{
		if (this != &original) {
			x = original.x;
			y = original.y;
			z = original.z;
		}
		return *this;
	}


	Vector3& operator =(Vector3&& original) noexcept
	{
		if (this != &original) {
			x = std::move(original.x);
			y = std::move(original.y);
			z = std::move(original.z);
		}
		return *this;
	}


	~Vector3()
	{
	}

public:
	void set(const T& x, const T& y, const T& z = 0.0f)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}


	void set(const Vector3<T>& value)
	{
		this->x = value.x;
		this->y = value.y;
		this->z = value.z;
	}


	float getLength() const
	{
		return std::sqrt((x * x) + (y * y) + (z * z));
	}


	float calculateVolume() const
	{
		return x * y * z;
	}


	// Note: faster for relative comparing
	float getLengthSquared() const
	{
		return (x * x) + (y * y) + (z * z);
	}


	void normalize()
	{
		float invLength = 1.0f / getLength();
		x *= invLength;
		y *= invLength;
		z *= invLength;
	}


	Vector3<T> getNormalized() const
	{
		Vector3<T> out;
		float invLength = 1.0f / getLength();
		out.x = x * invLength;
		out.y = y * invLength;
		out.z = z * invLength;

		return out;
	}


	void scalarMultiplication(float value)
	{
		x *= value;
		y *= value;
		z *= value;
	}


	void rotateAroundX(Degree angle)
	{
		const Radian radianAngle = degreesToRadians(angle);
		//x = static_cast<T>(x);
		y = static_cast<T>(y * std::cos(radianAngle) - z * std::sin(radianAngle));
		z = static_cast<T>(y * std::sin(radianAngle) + z * std::cos(radianAngle));
	}


	void rotateAroundY(Degree angle)
	{
		const Radian radianAngle = degreesToRadians(angle);
		x = static_cast<T>(z * std::sin(radianAngle) + x * std::cos(radianAngle));
		//y = static_cast<T>(y);
		z = static_cast<T>(z * std::cos(radianAngle) - x * std::sin(radianAngle));
	}


	void rotateAroundZ(Degree angle)
	{
		const Radian radianAngle = degreesToRadians(angle);
		x = static_cast<T>(x * std::cos(radianAngle) - y * std::sin(radianAngle));
		y = static_cast<T>(x * std::sin(radianAngle) + y * std::cos(radianAngle));
		//z = static_cast<T>(z);
	}


	float dotProduct(const Vector3<T> right) const
	{
		return x*right.x + y*right.y + z*right.z;
	}


	Vector3<T> crossProduct(const Vector3<T> right) const
	{
		return Vector3<T>(y*right.z - right.y*z, z*right.x - right.z*x, x*right.y - right.x*y);
	}


public: // operators
	Vector3<T> operator +(const Vector3<T>& right) const
	{
		return Vector3<T>(x+right.x, y+right.y, z+right.z);
	}


	Vector3<T>& operator +=(const Vector3<T>& right)
	{
		x += right.x;
		y += right.y;
		z += right.z;
		return *this;
	}


	Vector3<T> operator -(const Vector3<T>& right) const
	{
		return Vector3<T>(x-right.x, y-right.y, z-right.z);
	}


	Vector3<T>& operator -=(const Vector3<T>& right)
	{
		x -= right.x;
		y -= right.y;
		z -= right.z;
		return *this;
	}


	Vector3<T> operator -() const
	{
		return Vector3<T>(-x, -y, -z);
	}


	Vector3<T> operator *(float right) const
	{
		return Vector3<T>(x*right, y*right, z*right);
	}


	Vector3<T>& operator *=(float right)
	{
		x *= right;
		y *= right;
		z *= right;
		return *this;
	}


	Vector3<T> operator /(float right) const
	{
		float invRight = 1.0f / right;
		return Vector3<T>(x*invRight, y*invRight, z*invRight);
	}


	Vector3<T>& operator /=(float right)
	{
		float invRight = 1.0f / right;
		x *= invRight;
		y *= invRight;
		z *= invRight;
		return *this;
	}


	float operator *(const Vector3<T> right) const
	{
		return dotProduct(right);
	}


public: // static
	static Vector3<T> zero()
	{
		return Vector3<T>(0, 0, 0);
	}


	static Vector3<T> one()
	{
		return Vector3<T>(1, 1, 1);
	}


	static Vector3<T> up()
	{
		return Vector3<T>(0, 1, 0);
	}


	static Vector3<T> down()
	{
		return Vector3<T>(0, -1, 0);
	}


	static Vector3<T> forward()
	{
		return Vector3<T>(0, 0, 1);
	}


	static Vector3<T> backward()
	{
		return Vector3<T>(0, 0, -1);
	}


	static Vector3<T> left()
	{
		return Vector3<T>(-1, 0, 0);
	}


	static Vector3<T> right()
	{
		return Vector3<T>(1, 0, 0);
	}


public: // toString
	UString toString() const
	{
		UString str("(");
		str.append2(x);
		str += ", ";
		str.append2(y);
		str += ", ";
		str.append2(z);
		str += ")";
		return str;
	}


	UString toStringDebug() const
	{
		UString str("Vector3<T>(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const Vector3<T>& right)
	{
		return ost << right.toString();
	}
};


typedef Vector3<float> Vector3f;
typedef Vector3<unsigned int> Vector3u;
typedef Vector3<int> Vector3i;


template<typename T>
inline bool operator ==(const Vector3<T>& left, const Vector3<T>& right)
{
	return (left.x == right.x) && (left.y == right.y) && (left.z == right.z);
}


template<typename T>
inline bool operator !=(const Vector3<T>& left, const Vector3<T>& right)
{
	return !(left == right);
}


template<typename T>
inline bool operator <(const Vector3<T>& left, const Vector3<T>& right)
{
	return left.calculateVolume() < right.calculateVolume();
}


template<typename T>
inline bool operator >(const Vector3<T>& left, const Vector3<T>& right)
{
	return right < left;
}


template<typename T>
inline bool operator <=(const Vector3<T>& left, const Vector3<T>& right)
{
	return !(right < left);
}


template<typename T>
inline bool operator >=(const Vector3<T>& left, const Vector3<T>& right)
{
	return !(left < right);
}


} // End namespace Verso

#endif // End header guard

