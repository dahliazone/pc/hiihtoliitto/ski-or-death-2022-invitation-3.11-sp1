#ifndef VERSO_BASE_MATH_HALIGN_HPP
#define VERSO_BASE_MATH_HALIGN_HPP

#include <Verso/System/UString.hpp>

namespace Verso {


enum class HAlign
{
	Undefined,
	Left,
	Center,
	Right
};


inline UString hAlignToString(const HAlign& hAlign)
{
	if (hAlign ==  HAlign::Undefined)
		return "Undefined";
	else if (hAlign == HAlign::Left)
		return "Left";
	else if (hAlign == HAlign::Center)
		return "Center";
	else if (hAlign == HAlign::Right)
		return "Right";
	else
		return "Unknown value";
}


// returns HAlign::Undefined for any erroneuos string
inline HAlign stringToHAlign(const UString& str)
{
	if (str.equals("Left"))
		return HAlign::Left;
	else if (str.equals("Center"))
		return HAlign::Center;
	else if (str.equals("Right"))
		return HAlign::Right;
	else
		return HAlign::Undefined;
}


} // End namespace Verso

#endif // End header guard

