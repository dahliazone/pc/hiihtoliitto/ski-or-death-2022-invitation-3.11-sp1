#ifndef VERSO_BASE_MATH_VECTOR4_HPP
#define VERSO_BASE_MATH_VECTOR4_HPP

#include <Verso/Math/Math.hpp>
#include <Verso/Math/Degree.hpp>
#include <Verso/Math/Radian.hpp>
#include <Verso/Math/Vector2.hpp>
#include <Verso/Math/Vector3.hpp>
#include <Verso/System/UString.hpp>
#include <ostream>

namespace Verso {


template<typename T>
class Vector4
{
public:
	T x;
	T y;
	T z;
	T w;

public:
	Vector4() :
		x(0), y(0), z(0), w(0)
	{
	}


	Vector4(T value) :
		x(value), y(value), z(value), w(value)
	{
	}


	Vector4(const Vector2<T>& value) :
		x(value.x), y(value.y), z(0), w(0)
	{
	}


	Vector4(const Vector3<T>& value) :
		x(value.x), y(value.y), z(value.z), w(0)
	{
	}


	Vector4(const Vector4<T>& value) :
		x(value.x), y(value.y), z(value.z), w(value.w)
	{
	}


	Vector4(const T& x, const T& y, const T& z = 0.0f, const T& w = 0.0f) :
		x(x), y(y), z(z), w(w)
	{
	}


	Vector4& operator =(const Vector4& original)
	{
		if (this != &original) {
			x = original.x;
			y = original.y;
			z = original.z;
			w = original.w;
		}
		return *this;
	}


	Vector4& operator =(Vector4&& original) noexcept
	{
		if (this != &original) {
			x = std::move(original.x);
			y = std::move(original.y);
			z = std::move(original.z);
			w = std::move(original.w);
		}
		return *this;
	}


	~Vector4()
	{
	}

public:
	void set(const T& x, const T& y, const T& z = 0.0f, const T& w = 0.0f)
	{
		this->x = x;
		this->y = y;
		this->z = z;
		this->w = w;
	}


	void set(const Vector4<T>& value)
	{
		this->x = value.x;
		this->y = value.y;
		this->z = value.z;
		this->w = value.w;
	}


	float getLength() const
	{
		return std::sqrt((x * x) + (y * y) + (z * z) + (w * w));
	}


	// Note: faster for relative comparing
	float getLengthSquared() const
	{
		return (x * x) + (y * y) + (z * z) + (w * w);
	}


	void normalize()
	{
		float invLength = 1.0f / getLength();
		x *= invLength;
		y *= invLength;
		z *= invLength;
		w *= invLength;
	}


	Vector4<T> getNormalized() const
	{
		Vector4<T> out;
		float invLength = 1.0f / getLength();
		out.x = x * invLength;
		out.y = y * invLength;
		out.z = z * invLength;
		out.w = w * invLength;

		return out;
	}


	void scalarMultiplication(float value)
	{
		x *= value;
		y *= value;
		z *= value;
		w *= value;
	}


	void rotateAroundX(Degree angle)
	{
		const Radian radianAngle = degreesToRadians(angle);
		//x = static_cast<T>(x);
		y = static_cast<T>(y * std::cos(radianAngle) - z * std::sin(radianAngle));
		z = static_cast<T>(y * std::sin(radianAngle) + z * std::cos(radianAngle));
		//w = static_cast<T>(w);
	}


	void rotateAroundY(Degree angle)
	{
		const Radian radianAngle = degreesToRadians(angle);
		x = static_cast<T>(z * std::sin(radianAngle) + x * std::cos(radianAngle));
		//y = static_cast<T>(y);
		z = static_cast<T>(z * std::cos(radianAngle) - x * std::sin(radianAngle));
		//w = static_cast<T>w;
	}


	void rotateAroundZ(Degree angle)
	{
		const Radian radianAngle = degreesToRadians(angle);
		x = static_cast<T>(x * std::cos(radianAngle) - y * std::sin(radianAngle));
		y = static_cast<T>(x * std::sin(radianAngle) + y * std::cos(radianAngle));
		//z = static_cast<T>(z);
		//w = static_cast<T>(w);
	}


	float dotProduct(const Vector4<T> right) const
	{
		return x*right.x + y*right.y + z*right.z + w*right.w;
	}


public: // operators
	Vector4<T> operator +(const Vector4<T>& right) const
	{
		return Vector4<T>(x+right.x, y+right.y, z+right.z, w+right.w);
	}


	Vector4<T>& operator +=(const Vector4<T>& right)
	{
		x += right.x;
		y += right.y;
		z += right.z;
		w += right.w;
		return *this;
	}


	Vector4<T> operator -(const Vector4<T>& right) const
	{
		return Vector4<T>(x-right.x, y-right.y, z-right.z, w-right.w);
	}


	Vector4<T>& operator -=(const Vector4<T>& right)
	{
		x -= right.x;
		y -= right.y;
		z -= right.z;
		w -= right.w;
		return *this;
	}


	Vector4<T> operator -() const
	{
		return Vector4<T>(-x, -y, -z, -w);
	}


	Vector4<T> operator *(float right) const
	{
		return Vector4<T>(x*right, y*right, z*right, w*right);
	}


	Vector4<T>& operator *=(float right)
	{
		x *= right;
		y *= right;
		z *= right;
		w *= right;
		return *this;
	}


	Vector4<T> operator /(float right) const
	{
		float invRight = 1.0f / right;
		return Vector4<T>(x*invRight, y*invRight, z*invRight, w*invRight);
	}


	Vector4<T>& operator /=(float right)
	{
		float invRight = 1.0f / right;
		x *= invRight;
		y *= invRight;
		z *= invRight;
		w *= invRight;
		return *this;
	}


public: // static
	static Vector4<T> zero()
	{
		return Vector4<T>(0, 0, 0, 0);
	}


	static Vector4<T> one()
	{
		return Vector4<T>(1, 1, 1, 1);
	}


	static Vector4<T> up()
	{
		return Vector4<T>(0, 1, 0, 0);
	}


	static Vector4<T> down()
	{
		return Vector4<T>(0, -1, 0, 0);
	}


	static Vector4<T> forward()
	{
		return Vector4<T>(0, 0, 1, 0);
	}


	static Vector4<T> backward()
	{
		return Vector4<T>(0, 0, -1, 0);
	}


	static Vector4<T> left()
	{
		return Vector4<T>(-1, 0, 0, 0);
	}


	static Vector4<T> right()
	{
		return Vector4<T>(1, 0, 0, 0);
	}


public: // toString
	UString toString() const
	{
		UString str("(");
		str.append2(x);
		str += ", ";
		str.append2(y);
		str += ", ";
		str.append2(z);
		str += ", ";
		str.append2(w);
		str += ")";
		return str;
	}


	UString toStringDebug() const
	{
		UString str("Vector4<T>(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const Vector4<T>& right)
	{
		return ost << right.toString();
	}
};


typedef Vector4<float> Vector4f;
typedef Vector4<unsigned int> Vector4u;
typedef Vector4<int> Vector4i;


template<typename T>
inline bool operator ==(const Vector4<T>& left, const Vector4<T>& right)
{
	return (left.x == right.x) && (left.y == right.y) && (left.z == right.z) && (left.w == right.w);
}


template<typename T>
inline bool operator !=(const Vector4<T>& left, const Vector4<T>& right)
{
	return !(left == right);
}


template<typename T>
inline bool operator <(const Vector4<T>& left, const Vector4<T>& right)
{
	if (left.x == right.x) {
		if (left.y == right.y) {
			if (left.z == right.z) {
				return left.w < right.w;
			}
			else {
				return left.z < right.z;
			}
		}
		else {
			return left.y < right.y;
		}
	}
	else {
		return left.x < right.x;
	}
}


template<typename T>
inline bool operator >(const Vector4<T>& left, const Vector4<T>& right)
{
	return right < left;
}


template<typename T>
inline bool operator <=(const Vector4<T>& left, const Vector4<T>& right)
{
	return !(right < left);
}


template<typename T>
inline bool operator >=(const Vector4<T>& left, const Vector4<T>& right)
{
	return !(left < right);
}


} // End namespace Verso

#endif // End header guard

