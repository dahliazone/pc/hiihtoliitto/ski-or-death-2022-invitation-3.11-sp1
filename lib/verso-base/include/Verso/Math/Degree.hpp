#ifndef VERSO_BASE_MATH_DEGREE_HPP
#define VERSO_BASE_MATH_DEGREE_HPP

#include <Verso/System/UString.hpp>
#include <Verso/Math/math_defines.hpp>

namespace Verso {


#ifndef VERSO_BASE_MATH_DEGREE_RADIAN
#define VERSO_BASE_MATH_DEGREE_RADIAN
typedef float Radian;
typedef float Degree;
#endif


inline Radian degreesToRadians(Degree angleDegrees)
{
	return angleDegrees * static_cast<float>(M_PI / 180);
}


inline UString degreesToString(Degree angleDegrees)
{
	return UString::from(angleDegrees);
}


} // End namespace Verso

#endif // End header guard

