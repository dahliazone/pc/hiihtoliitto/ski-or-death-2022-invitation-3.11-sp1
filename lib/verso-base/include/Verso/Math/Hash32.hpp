#ifndef VERSO_BASE_MATH_HASH32_HPP
#define VERSO_BASE_MATH_HASH32_HPP

#include <cstdint>
#include <cstdlib>
#include <Verso/System/UString.hpp>

namespace Verso {


class Hash32
{
public: // member variables
	std::uint32_t hashValue;

public:
	Hash32() :
		hashValue(0)
	{
	}


	Hash32(std::uint32_t hashValue) :
		hashValue(hashValue)
	{
	}


public: // static

	static Hash32 fnv1(const UString& key)
	{
		return fnv1(key.c_str(), key.size());
	}


	// Generate 32-bit hash from data with FNV-1 (Fowler/Noll/Vo) algorithm.
	// see, https://en.wikipedia.org/wiki/Fowler%E2%80%93Noll%E2%80%93Vo_hash_function
	static Hash32 fnv1(const void* key, size_t lenght)
	{
		const unsigned char* p = reinterpret_cast<const unsigned char*>(key);
		std::uint32_t hash = 2166136261;

		for (size_t i = 0; i < lenght; ++i) {
			hash = (hash * 16777619) ^ p[i];
		}

		return Hash32(hash);
	}


public: // toString
	UString toString() const
	{
		UString str;
		str.append2(hashValue);
		return str;
	}


	UString toStringDebug() const
	{
		UString str("Hash32(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const Hash32& right)
	{
		return ost << right.toString();
	}
};


inline bool operator ==(const Hash32& left, const Hash32& right)
{
	return (left.hashValue == right.hashValue);
}


inline bool operator !=(const Hash32& left, const Hash32& right)
{
	return !(left == right);
}


inline bool operator <(const Hash32& left, const Hash32& right)
{
	return left.hashValue < right.hashValue;
}


inline bool operator >(const Hash32& left, const Hash32& right)
{
	return right < left;
}


inline bool operator <=(const Hash32& left, const Hash32& right)
{
	return !(right < left);
}


inline bool operator >=(const Hash32& left, const Hash32& right)
{
	return !(left < right);
}


} // End namespace Verso

#endif // End header guard

