#ifndef VERSO_BASE_MATH_VERTEX_HPP
#define VERSO_BASE_MATH_VERTEX_HPP

#include <Verso/Math/Vector2.hpp>
#include <Verso/Math/Vector3.hpp>
#include <Verso/System/UString.hpp>
#include <ostream>

namespace Verso {


class Vertex
{
public:
	Vector3f position;
	Vector3f normal;
	Vector2f texCoords;

public:
	Vertex() :
		position(),
		normal(),
		texCoords()
	{
	}


	Vertex(const Vertex& original) :
		position(original.position),
		normal(original.normal),
		texCoords(original.texCoords)
	{
	}


public: // toString
	UString toString() const
	{
		UString str("position=");
		str.append2(position);
		str += ", normal=";
		str.append2(normal);
		str += ", texCoords=";
		str.append2(texCoords);
		return str;
	}


	UString toStringDebug() const
	{
		UString str("Vertex(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const Vertex& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso

#endif // End header guard

