#ifndef VERSO_BASE_MATH_INTERPOLATION_HPP
#define VERSO_BASE_MATH_INTERPOLATION_HPP

#include <Verso/System/Exception.hpp>
#include <Verso/Math/Easing.hpp>
#include <vector>
#include <cmath>

namespace Verso {


class Interpolation
{
public: // static
	template <typename T>
	static T lerp(const T& a, const T& b, float t)
	{
		return a + (b - a) * t;
	}


	template <typename T>
	static T mix(const T& a, const T& b, float t)
	{
		return a + (b - a) * t;
	}


	template <typename T>
	static T easeIn(const T& a, const T& b, float t)
	{
		return lerp(a, b, Easing::easeIn(t));
	}


	// Problem with not using A and B!
//	template <typename T>
//	static T easeOut(const T& a, const T& b, float t)
//	{
//		return 1.0f - (1.0f - t) * (1.0f - t);
//	}


	// Problem with not using A and B!
//	template <typename T>
//	static T easeInOut(const T& a, const T& b, float t)
//	{
//		return lerp(Easing::easeIn(t), Easing::easeOut(t), t);
//	}


	template <typename T>
	static T quadraticBezier(const T& a, const T& b, const T& c, float t)
	{
		const T d = mix(a, b, t);
		const T e = mix(b, c, t);
		return mix(d, e, t);
	}


	template <typename T>
	static T cubicBezier(const T& a, const T& b, const T& c, const T& d, float t)
	{
		const T e = mix(a, b, t);
		const T f = mix(b, c, t);
		const T g = mix(c, d, t);
		return quadraticBezier(e, f, g, t);
	}


	template <typename T>
	static T quinticBezier(const T& a, const T& b, const T& c, const T& d, const T& e, float t)
	{
		const T f = mix(a, b, t);
		const T g = mix(b, c, t);
		const T h = mix(c, d, t);
		const T i = mix(d, e, t);
		return cubicBezier(f, g, h, i, t);
	}


	// given array must contain 2n values point + velocity for each point
	// broken & untested as of yet.
	template <typename T>
	static T cubicHermiteSpline(const std::vector<T>& cp, float t)
	{
		if (cp.size() < 2) {
			VERSO_ILLEGALFORMAT("verso-base", "You must give a vector with at least two entries!", "");
			return T();
		}

		float f = t * 2.0f;
		int i = static_cast<int>(floor(static_cast<double>(f)));
		float s = f - static_cast<float>(i);

		if (t <= 0.0f) {
			return cp[0];
		}

		if (t >= static_cast<float>(cp.size()) / 2.0f) {
			return cp[cp.size() - 2];
		}

		const T a =  cp[i * 2];
		const T b =  cp[i * 2 + 1];
		const T c = -cp[(i + 1) * 2];
		const T d =  cp[(i + 1) * 2];

		return cubicBezier(a, b, c, d, s);
	}
};


} // End namespace Verso

#endif // End header guard

