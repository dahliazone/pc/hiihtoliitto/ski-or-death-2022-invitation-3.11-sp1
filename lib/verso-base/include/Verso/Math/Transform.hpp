#ifndef VERSO_BASE_MATH_TRANSFORM_HPP
#define VERSO_BASE_MATH_TRANSFORM_HPP

#include <Verso/Math/Vector2.hpp>
#include <Verso/Math/Vector3.hpp>
#include <Verso/Math/Matrix4x4f.hpp>
#include <Verso/Math/Rect.hpp>
#include <Verso/System/UString.hpp>
#include <ostream>

namespace Verso {


/*
class Transform
{
private:
	Matrix4x4f matrix;

public:
	// Default constructor.
	Transform()
	{
	}


	Transform(float m00, float m01, float m02, float m10, float m11, float m12, float m20, float m21, float m22) :
		matrix(m00, m01, m02, m10, m11, m12, m20, m21, m22)
	{
	}


	const Matrix4x4f& getMatrix4x4() const
	{
		return matrix;
	}


	// Return the inverse of the transform.
	Transform getInverse() const
	{
		//return Transform(matrix.get);
		return Transform();
	}


	// Transform a 2D point.
	Vector2f transformPoint(float x, float y) const
	{
	}


	// Transform a 2D point.
	Vector2f transformPoint(const Vector2f& point) const
	{
	}


	// Transform a rectangle.
	Rectf transformRect(const Rectf& rectangle) const
	{
	}


	// Combine the current transform with another one.
	Transform& combine(const Transform &transform)
	{
	}


	// Combine the current transform with a translation.
	Transform& translate(float x, float y)
	{
	}


	// Combine the current transform with a translation.
	Transform& translate(const Vector2f& offset)
	{
	}


	// Combine the current transform with a rotation.
	Transform& rotate(float angle)
	{
	}


	// Combine the current transform with a rotation.
	Transform& rotate(float angle, float centerX, float centerY)
	{
	}


	// Combine the current transform with a rotation.
	Transform& rotate(float angle, const Vector2f& center)
	{
	}


	// Combine the current transform with a scaling.
	Transform& scale(float scaleX, float scaleY)
	{
	}


	// Combine the current transform with a scaling.
	Transform& scale(float scaleX, float scaleY, float centerX, float centerY)
	{
	}


	// Combine the current transform with a scaling.
	Transform& scale(const Vector2f& factors)
	{
	}


	// Combine the current transform with a scaling.
	Transform& scale(const Vector2f& factors, const Vector2f& center)
	{
	}
};


// Overload of binary operator * to combine two transforms.
Transform operator *(const Transform& left, const Transform& right)
{
}


// Overload of binary operator *= to combine two transforms.
Transform& operator *=(Transform& left, const Transform& right)
{
}


// Overload of binary operator * to transform a point.
Vector2f operator *(const Transform& left, const Vector2f& right)
{
}
*/

/*
protected:
	// Gets the full transformation matrix for this node. More...
	const Matrix4& _getFullTransform() const
	{
	}


	// Gets the position of the node relative to it's parent. More...
	const Vector3& getPosition() const
	{
	}

	// Sets the position of the node relative to it's parent. More...
	void setPosition(const Vector3& pos)
	{
	}

	// Sets the position of the node relative to it's parent. More...
	void setPosition(Real x, Real y, Real z)
	{
	}

	// Moves the node along the Cartesian axes. More...
	void translate(const Vector3& d, TransformSpace relativeTo=TS_PARENT)
	{
	}

	// Moves the node along the Cartesian axes. More...
	void translate(Real x, Real y, Real z, TransformSpace relativeTo=TS_PARENT)
	{
	}

	// Moves the node along arbitrary axes. More...
	void translate(const Matrix3& axes, const Vector3& move, TransformSpace relativeTo=TS_PARENT)
	{
	}

	// Moves the node along arbitrary axes. More...
	void translate(const Matrix3& axes, Real x, Real y, Real z, TransformSpace relativeTo=TS_PARENT)
	{
	}



	// Returns a quaternion representing the nodes orientation. More...
	const Quaternion& getOrientation() const
	{
	}

	// Sets the orientation of this node via a quaternion. More...
	void setOrientation(const Quaternion& q)
	{
	}

	// Sets the orientation of this node via quaternion parameters. More...
	void setOrientation(Real w, Real x, Real y, Real z)
	{
	}

	// Rotate the node around the X-axis. More...
	void pitch(const Radian &angle, TransformSpace relativeTo=TS_LOCAL)
	{
	}

	// Rotate the node around the Y-axis. More...
	void yaw(const Radian& angle, TransformSpace relativeTo=TS_LOCAL)
	{
	}

	// Rotate the node around the Z-axis. More...
	void roll(const Radian& angle, TransformSpace relativeTo=TS_LOCAL)
	{
	}

	// Rotate the node around an arbitrary axis. More...
	void rotate(const Vector3& axis, const Radian& angle, TransformSpace relativeTo=TS_LOCAL)
	{
	}

	// Rotate the node around an aritrary axis using a Quarternion. More...
	void rotate(const Quaternion& q, TransformSpace relativeTo=TS_LOCAL)
	{
	}



	//Gets the scaling factor of this node. More...
	const Vector3& getScale() const
	{
	}

	// Sets the scaling factor applied to this node. More...
	void setScale(const Vector3& scale)
	{
	}

	// Sets the scaling factor applied to this node. More...
	void setScale(Real x, Real y, Real z)
	{
	}

	// Scales the node, combining it's current scale with the passed in scaling factor. More...
	void scale(const Vector3& scale)
	{
	}

	// Scales the node, combining it's current scale with the passed in scaling factor. More...
	void scale(Real x, Real y, Real z)
	{
	}




public: // Node
	// Returns the name of the node. More...
	const String& getName() const
	{
	}

	// Gets this node's parent(nullptr if this is the root). More...
	Node* getParent() const
	{
	}

	// Adds a(precreated) child scene node to this node. More...
	void addChild(Node* child)
	{
	}

	// Creates an unnamed new Node as a child of this node. More...
	Node* createChild(const Vector3& translate=Vector3::ZERO, const Quaternion& rotate=Quaternion::IDENTITY)
	{
	}

	// Creates a new named Node as a child of this node. More...
	Node* createChild(const String& name, const Vector3& translate=Vector3::ZERO, const Quaternion& rotate=Quaternion::IDENTITY)
	{
	}

	// Gets a pointer to a child node. More...
	Node* getChild(unsigned short index) const
	{
	}

	// Gets a pointer to a named child node. More...
	Node* getChild(const String& name) const
	{
	}

	// Retrieves an iterator for efficiently looping through all children of this node. More...
	ChildNodeIterator getChildIterator()
	{
	}

	// Retrieves an iterator for efficiently looping through all children of this node. More...
	ConstChildNodeIterator getChildIterator() const
	{
	}

	// Reports the number of child nodes under this one. More...
	unsigned short numChildren() const
	{
	}

	// Removes all child Nodes attached to this node. More...
	void removeAllChildren()
	{
	}


	// Drops the specified child from this node. More...
	Node* removeChild(unsigned short index)
	{
	}


	// Drops the specified child from this node. More...
	Node* removeChild(Node* child)
	{
	}


	// Drops the named child from this node. More...
	Node* removeChild(const String& name)
	{
	}


public: // Node: inherit
	// Returns true if this node is affected by orientation applied to the parent node. More...
	bool getInheritOrientation() const
	{
	}


	// Returns true if this node is affected by scaling factors applied to the parent node. More...
	bool getInheritScale() const
	{
	}


public: // Coordinate conversions
	// Gets the world orientation of an orientation in the node local space useful for simple transforms that don't require a child node. More...
	Quaternion convertLocalToWorldOrientation(const Quaternion& localOrientation)
	{
	}

	// Gets the world position of a point in the node local space useful for simple transforms that don't require a child node. More...
	Vector3 convertLocalToWorldPosition(const Vector3& localPos)
	{
	}

	// Gets the local orientation, relative to this node, of the given world-space orientation. More...
	Quaternion convertWorldToLocalOrientation(const Quaternion& worldOrientation)
	{
	}

	// Gets the local position, relative to this node, of the given world-space position. More...
	Vector3 convertWorldToLocalPosition(const Vector3& worldPos)
	{
	}



public: // ????
	// Get a debug renderable for rendering the Node. More...
	DebugRenderable* getDebugRenderable(Real scaling)
	{
	}

	// Gets the current listener for this Node. More...
	Listener* getListener() const
	{
	}

	// Gets a matrix whose columns are the local axes based on the nodes orientation relative to it's parent. More...
	Matrix3 getLocalAxes() const
	{
	}

	// Helper function, get the squared view depth. More...
	Real getSquaredViewDepth(const Camera* cam) const
	{
	}

	// Return an instance of user objects binding associated with this class. More...
	UserObjectBindings& getUserObjectBindings()
	{
	}


	// Return an instance of user objects binding associated with this class. More...
	const UserObjectBindings& getUserObjectBindings() const
	{
	}


public: // new / delete operators
	void operator delete(void* ptr)
	{
	}


	void operator delete(void* ptr, void* )
	{
	}


	void operator delete(void* ptr, const char *, int, const char *)
	{
	}


	void operator delete[](void* ptr)
	{
	}


	void operator delete[](void* ptr, const char *, int, const char *)
	{
	}


	// operator new, with debug line info More...
	void* operator new(size_t sz, const char *file, int line, const char *func)
	{
	}


	void* operator new(size_t sz)
	{
	}


	// placement operator new More...
	void* operator new(size_t sz, void* ptr)
	{
	}


	// array operator new, with debug line info More...
	void* operator new[](size_t sz, const char *file, int line, const char *func)
	{
	}


	void* operator new[](size_t sz)
	{
	}


public: // \TODO check through






	// Called by children to notify their parent that they need an update. More...
	void requestUpdate(Node* child, bool forceParentUpdate=false)
	{
	}


	// Resets the nodes orientation(local axes as world axes, no rotation). More...
	void resetOrientation()
	{
	}
*/


} // End namespace Verso

#endif // End header guard

