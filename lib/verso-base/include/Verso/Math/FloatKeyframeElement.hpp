#ifndef VERSO_BASE_MATH_FLOATKEYFRAMEELEMENT_HPP
#define VERSO_BASE_MATH_FLOATKEYFRAMEELEMENT_HPP

#include <Verso/System/UString.hpp>

namespace Verso {


class FloatKeyframeElement
{
public:
	float seconds;
	float value;
	float control1;
	float control2;

public:
	FloatKeyframeElement(float seconds, float value,
						 float control1 = 0.0f,
						 float control2 = 0.0f) :
		seconds(seconds),
		value(value),
		control1(control1),
		control2(control2)
	{
	}


public: // toString
	UString toString() const
	{
		UString str("{ seconds=");
		str.append2(seconds);
		str += ", value=";
		str.append2(value);
		str += ", control1=";
		str.append2(control1);
		str += ", control2=";
		str.append2(control2);
		str += " }";
		return str;
	}


	UString toStringDebug() const
	{
		UString str("FloatKeyframeElement(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const FloatKeyframeElement& right)
	{
		return ost << right.toString();
	}
};


inline bool operator ==(const FloatKeyframeElement& left, const FloatKeyframeElement& right)
{
	return (left.seconds == right.seconds) &&
			(left.value == right.value) &&
			(left.control1 == right.control1) &&
			(left.control2 == right.control2);
}


inline bool operator !=(const FloatKeyframeElement& left, const FloatKeyframeElement& right)
{
	return !(left == right);
}


} // End namespace Verso

#endif // End header guard

