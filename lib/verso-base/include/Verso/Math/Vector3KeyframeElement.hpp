#ifndef VERSO_BASE_MATH_VECTOR3KEYFRAMEELEMENT_HPP
#define VERSO_BASE_MATH_VECTOR3KEYFRAMEELEMENT_HPP

#include <Verso/Math/Vector3.hpp>

namespace Verso {


template<typename T>
class Vector3KeyframeElement
{
public:
	float seconds;
	Vector3<T> value;
	Vector3<T> control1;
	Vector3<T> control2;

public:
	Vector3KeyframeElement<T>(float seconds, const Vector3<T>& value,
							  const Vector3<T>& control1 = Vector3<T>(),
							  const Vector3<T>& control2 = Vector3<T>()) :
		seconds(seconds),
		value(value),
		control1(control1),
		control2(control2)
	{
	}


public: // toString
	UString toString() const
	{
		UString str("{ seconds=");
		str.append2(seconds);
		str += ", value=";
		str += value.toString();
		str += ", control1=";
		str += control1.toString();
		str += ", control2=";
		str += control2.toString();
		str += " }";
		return str;
	}


	UString toStringDebug() const
	{
		UString str("Vector3KeyframeElement<T>(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const Vector3KeyframeElement<T>& right)
	{
		return ost << right.toString();
	}
};

typedef Vector3KeyframeElement<int> Vector3iKeyframeElement;
typedef Vector3KeyframeElement<unsigned int> Vector3uKeyframeElement;
typedef Vector3KeyframeElement<float> Vector3fKeyframeElement;


template<typename T>
inline bool operator ==(const Vector3KeyframeElement<T>& left, const Vector3KeyframeElement<T>& right)
{
	return (left.seconds == right.seconds) &&
			(left.value == right.value) &&
			(left.control1 == right.control1) &&
			(left.control2 == right.control2);
}


template<typename T>
inline bool operator !=(const Vector3KeyframeElement<T>& left, const Vector3KeyframeElement<T>& right)
{
	return !(left == right);
}


} // End namespace Verso

#endif // End header guard

