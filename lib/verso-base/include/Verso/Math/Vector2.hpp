#ifndef VERSO_BASE_MATH_VECTOR2_HPP
#define VERSO_BASE_MATH_VECTOR2_HPP

#include <Verso/Math/Math.hpp>
#include <Verso/Math/Degree.hpp>
#include <Verso/Math/Radian.hpp>
#include <Verso/System/UString.hpp>
#include <ostream>

namespace Verso {


template<typename T>
class Vector2
{
public:
	T x;
	T y;

public:
	Vector2() :
		x(0), y(0)
	{
	}


	Vector2(T value) :
		x(value), y(value)
	{
	}


	Vector2(const Vector2<T>& value) :
		x(value.x), y(value.y)
	{
	}


	Vector2(const Vector2<T>&& value) noexcept :
		x(value.x), y(value.y)
	{
	}


	Vector2(const T& x, const T& y) :
		x(x), y(y)
	{
	}


	Vector2& operator =(const Vector2& original)
	{
		if (this != &original) {
			x = original.x;
			y = original.y;
		}
		return *this;
	}


	Vector2& operator =(Vector2&& original) noexcept
	{
		if (this != &original) {
			x = std::move(original.x);
			y = std::move(original.y);
		}
		return *this;
	}


	~Vector2()
	{
	}

public:
	void set(const T& x, const T& y)
	{
		this->x = x;
		this->y = y;
	}


	void set(const Vector2<T>& value)
	{
		this->x = value.x;
		this->y = value.y;
	}


	float getLength() const
	{
		return std::sqrt((x * x) + (y * y));
	}


	// Note: faster for relative comparing
	float getLengthSquared() const
	{
		return (x * x) + (y * y);
	}


	T calculateArea() const
	{
		return x * y;
	}


	void normalize()
	{
		float invLength = 1.0f / getLength();
		x *= invLength;
		y *= invLength;
	}


	Vector2<T> getNormalized() const
	{
		Vector2<T> out;
		float invLength = 1.0f / getLength();
		out.x = x * invLength;
		out.y = y * invLength;

		return out;
	}


	void scalarMultiplication(float value)
	{
		x *= value;
		y *= value;
	}


	//void rotateAroundX(Degree angleDegrees);
	//void rotateAroundY(Degree angleDegrees);


	void rotateAroundZ(Degree angleDegrees)
	{
		const Radian angleRadians = degreesToRadians(angleDegrees);
		x = static_cast<T>(x * std::cos(angleRadians) - y * std::sin(angleRadians));
		y = static_cast<T>(x * std::sin(angleRadians) + y * std::cos(angleRadians));
	}


	float dotProduct(const Vector2<T> v) const
	{
		return x*v.x + y*v.y;
	}


public: // operators
	Vector2<T> operator +(const Vector2<T>& right) const
	{
		return Vector2<T>(x+right.x, y+right.y);
	}


	Vector2<T>& operator +=(const Vector2<T>& right)
	{
		x += right.x;
		y += right.y;
		return *this;
	}


	Vector2<T> operator -(const Vector2<T>& right) const
	{
		return Vector2<T>(x-right.x, y-right.y);
	}


	Vector2<T>& operator -=(const Vector2<T>& right)
	{
		x -= right.x;
		y -= right.y;
		return *this;
	}


	Vector2<T> operator -() const
	{
		return Vector2<T>(-x, -y);
	}


	Vector2<T> operator *(float right) const
	{
		return Vector2<T>(x*right, y*right);
	}


	Vector2<T>& operator *=(float right)
	{
		x *= right;
		y *= right;
		return *this;
	}


	Vector2<T> operator /(float right) const
	{
		float invRight = 1.0f / right;
		return Vector2<T>(x*invRight, y*invRight);
	}


	Vector2<T>& operator /=(float right)
	{
		float invRight = 1.0f / right;
		x = static_cast<T>(x * invRight);
		y = static_cast<T>(y * invRight);
		return *this;
	}


	float operator *(const Vector2<T> right) const
	{
		return dotProduct(right);
	}


public: // static
	static Vector2<T> zero()
	{
		return Vector2<T>(0, 0);
	}


	static Vector2<T> one()
	{
		return Vector2<T>(1, 1);
	}


	static Vector2<T> up()
	{
		return Vector2<T>(0, -1);
	}


	static Vector2<T> down()
	{
		return Vector2<T>(0, 1);
	}


	static Vector2<T> left()
	{
		return Vector2<T>(-1, 0);
	}


	static Vector2<T> right()
	{
		return Vector2<T>(1, 0);
	}


public: // toString
	UString toString() const
	{
		UString str("(");
		str.append2(x);
		str += ", ";
		str.append2(y);
		str += ")";
		return str;
	}


	UString toStringDebug() const
	{
		UString str("Vector2<T>(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const Vector2<T>& right)
	{
		return ost << right.toString();
	}
};


typedef Vector2<float> Vector2f;
typedef Vector2<unsigned int> Vector2u;
typedef Vector2<int> Vector2i;


template<typename T>
inline bool operator ==(const Vector2<T>& left, const Vector2<T>& right)
{
	return (left.x == right.x) && (left.y == right.y);
}


template<typename T>
inline bool operator !=(const Vector2<T>& left, const Vector2<T>& right)
{
	return !(left == right);
}


template<typename T>
inline bool operator <(const Vector2<T>& left, const Vector2<T>& right)
{
	return left.calculateArea() < right.calculateArea();
}


template<typename T>
inline bool operator >(const Vector2<T>& left, const Vector2<T>& right)
{
	return right < left;
}


template<typename T>
inline bool operator <=(const Vector2<T>& left, const Vector2<T>& right)
{
	return !(right < left);
}


template<typename T>
inline bool operator >=(const Vector2<T>& left, const Vector2<T>& right)
{
	return !(left < right);
}


} // End namespace Verso

#endif // End header guard

