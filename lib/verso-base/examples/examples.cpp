#include <Verso/Image/Image.hpp>

#include <Verso/Math/Align.hpp>
#include <Verso/Math/Array2.hpp>
#include <Verso/Math/AspectRatio.hpp>
#include <Verso/Math/Degree.hpp>
#include <Verso/Math/HAlign.hpp>
#include <Verso/Math/Hash32.hpp>
#include <Verso/Math/Math.hpp>
#include <Verso/Math/Matrix4x4f.hpp>
#include <Verso/Math/Quaternion.hpp>
#include <Verso/Math/Radian.hpp>
#include <Verso/Math/Random.hpp>
#include <Verso/Math/Range.hpp>
#include <Verso/Math/Rect.hpp>
#include <Verso/Math/RgbaColorf.hpp>
#include <Verso/Math/Transform.hpp>
#include <Verso/Math/VAlign.hpp>
#include <Verso/Math/Vector2.hpp>
#include <Verso/Math/Vector3.hpp>
#include <Verso/Math/Vector4.hpp>
#include <Verso/Math/Vertex.hpp>

#include <Verso/System/assert.hpp>
#include <Verso/System/Endian.hpp>
#include <Verso/System/File.hpp>
#include <Verso/System/JsonHelper.hpp>
#include <Verso/System/string_convert.hpp>
#include <Verso/System/UString.hpp>

#include <Verso/Image.hpp>
#include <Verso/Math.hpp>
#include <Verso/System.hpp>

#include <Verso/Base.hpp>

#include <iostream>


using namespace std;
using namespace Verso;


void versoBaseSimpleTestsImage()
{
	cout << "//// verso-base/Image //////////////////////////////////////////" << endl;
	Image image;
	image.create(10, 10);
	cout << "Image = "<<image << endl << endl;
}


void versoBaseSimpleTestsMath()
{
	cout << "//// verso-base/Math ///////////////////////////////////////////" << endl;
	Align align;
	align.set(HAlign::Left, VAlign::Top);
	cout << "Align = "<<align << endl << endl;


	Array2<int> array2i(3, 2);
	array2i.isInside(3, 2);
	cout << "Array2 = " << endl;
	cout << array2i << endl;


	AspectRatio aspectRatio;
	aspectRatio.setByResolution(1280, 720);
	cout << "AspectRatio = "<<aspectRatio << endl << endl;


	cout << "degreesToRadians(90) => "<<degreesToRadians(90) << endl;
	cout << "degreesToString(45.5f) = "<<degreesToString(45.5f) << endl << endl;


	cout << "hAlignToString(HAlign::Right) => "<<hAlignToString(HAlign::Right) << endl << endl;


	Hash32 hash32;
	const char key[] = "tWJPbS53zCVnvvuiHY0ZZV34Cwrsn4coHzMEBuLlFhqPciOVgfiy17hF141jz1cylR3a4Rpca9d3zCsraGdKGN2oakzgYBdEvckwNNE8AFc2Y170zaWJeFTNVs99r5cHs9AhCdKyXuxrUHf8jQbCUwoOygr1FaurKgjEqvgeZjfzyskDs4rB892vzs9BBz1lbqvMQY9cfMVtcucWl9k6xMjv3RAfP5VLmPiH5oYAeteo0OjehlN3JILa01n5LExj\0";
	cout << "Hash32 = "<<hash32 << endl;
	cout << "  key = "<<key << endl;
	cout << "  keyLength = "<<strlen(key) << endl;
	cout << "  hash.fnv1(key, keyLength) = "<<hash32.fnv1(key, strlen(key)) << endl << endl;


	cout << "Math::invSqrt(16) => "<<Math::invSqrt(16) << endl;
	cout << "Math::invSqrt(16.5f) => "<<Math::invSqrt(16.5f) << endl << endl;


	cout << "Matrix4x4f::Indentity = " << endl;
	cout << Matrix4x4f::Identity() << endl;
	Matrix4x4f matrix4x4f;
	matrix4x4f = Matrix4x4f::perspectiveProjection(89, AspectRatio(1920, 1080, "16:9"), 0.1f, 10.0f);
	cout << "Matrix4x4f::perspectiveProjection(89, AspectRatio(1920, 1080, \"16:9\"), 0.1f, 10.0f) => " << endl;
	cout << matrix4x4f << endl << endl;


	Quaternion quaternion;
	quaternion.getLengthSquared();
	cout << "Quaternion = "<<quaternion << endl;
	cout << "Quaternion::identity = "<<Quaternion::Identity() << endl << endl;


	cout << "radiansToDegrees(0.25f) => "<<radiansToDegrees(0.25f) << endl;
	cout << "radiansToString(0.25f) => "<<radiansToString(0.25f) << endl << endl;


	cout << "Random::intRange(1, 25) => "<<Random::intRange(1, 25) << endl;
	cout << "Random::floatRange(0.25f, 0.5f) => "<<Random::floatRange(0.25f, 0.5f) << endl << endl;


	Range<float> range(1.0f, 3.0f);
	cout << "Range(1.0f, 3.0f) = "<<range << endl;
	cout << "Range.scaleToRange(0.0f) = "<<range.scaleToRange(0.0f) << endl;
	cout << "Range.scaleToRange(0.57f) = "<<range.scaleToRange(0.57f) << endl;
	cout << "Range.scaleToRange(1.0f) = "<<range.scaleToRange(1.0f) << endl;


	Rect<int> rect(1, 2, 3, 6);
	cout << "Rect(1, 2, 3, 6) = "<<rect << endl;
	cout << "  calculateArea() => "<<rect.calculateArea() << endl << endl;


	RgbaColorf rgbaColorf(0.1f, 0.2f, 0.3f, 0.4f);
	cout << "RgbaColorf(0.1f, 0.2f, 0.3f, 0.4f) = "<<rgbaColorf << endl;
	cout << "  getArgbValue() => "<<rgbaColorf.getArgbValue() << endl << endl;


	cout << "//Transform transform; // commented code in the header" << endl << endl;


	cout << "vAlignToString(VAlign::Bottom) => "<<vAlignToString(VAlign::Bottom) << endl << endl;


	Vector2f vec2f(1.45f, 2.5f);
	cout << "Vector2f(1.45f, 2.5f) = "<<vec2f << endl;
	cout << "  getLength() => "<<vec2f.getLength() << endl << endl;
	cout << "  calculateArea() => "<<vec2f.calculateArea() << endl << endl;


	Vector3f vec3f(2.8f, 3.1f, 2.1f);
	cout << "Vector3f(2.8f, 3.1f, 2.1f) = "<<vec3f << endl;
	cout << "  getLength() => "<<vec3f.getLength() << endl << endl;
	cout << "  calculateVolume() => "<<vec3f.calculateVolume() << endl << endl;


	Vector4f vec4f(2.5f, 0.45f, 0.245f, 3.9f);
	cout << "Vector4f(2.5f, 0.45f, 0.245f, 3.9f) = "<<vec4f << endl;
	cout << "  getLength() => "<<vec4f.getLength() << endl << endl;


	Vertex vertex;
	vertex.position = Vector3f(1, 2, 3);
	vertex.normal = Vector3f(0.1f, 0.9f, 0.0f);
	vertex.texCoords = Vector2f(0.5f, 0.2f);
	cout << "Vertex = "<<vertex << endl << endl;
}


void versoBaseSimpleTestsSystem()
{
	cout << "//// verso-base/System /////////////////////////////////////////" << endl;
	AssertException assertException("mycomponent", "my message", "myFunction()", "myFile.hpp", 100, "resource.xml", "myTestExpression");


	cout <<	"Endian::isLittleEndian() => "<<Endian::isLittleEndian() << endl;
	cout <<	"Endian::bswap(static_cast<std::uint16_t>(123)) => "<<Endian::bswap(static_cast<std::uint16_t>(123)) << endl << endl;


	cout << "File::getBasePath() => "<<File::getBasePath() << endl;
	cout << "File::exists(\"nonexisting.foo\") => "<<File::exists("nonexisting.foo") << endl << endl;


	cout << "// ForceOneInstance - skipped because hard to test" << endl << endl;
	cout << "// JsonHelper - skipped because hard to test without test files" << endl << endl;
	cout << "// Singleton - skipped because hard to test" << endl << endl;
	cout << "// SingletonTestManager - skipped because hard to test" << endl << endl;


	string str("abc");
	stringAdd(str, 321);
	cout << "stringAdd(string(\"abc\", 321) => "<<str << endl;
	cout << "castStream<int, string>(\"654\") => "<<castStream<int, string>("654") << endl << endl;


	UString ustring("foobar 123");
	cout << "UString(\"foobar 123\") = "<<ustring << endl;
	cout << "  getLength() => "<<ustring.getLength() << endl;
	cout << "  beginsWith(\"abc\") => "<<ustring.beginsWith("abc") << endl;
	cout << "  beginsWith(\"foo\") => "<<ustring.beginsWith("foo") << endl << endl;
}


void versoBaseSimpleTests()
{
	cout << "//// RUNNING TEST FOR VERSO-BASE ///////////////////////////////" << endl << endl;

	versoBaseSimpleTestsImage();
	versoBaseSimpleTestsMath();
	versoBaseSimpleTestsSystem();

	cout << "//// ALL TESTS DONE FOR VERSO-BASE /////////////////////////////" << endl << endl;
}
