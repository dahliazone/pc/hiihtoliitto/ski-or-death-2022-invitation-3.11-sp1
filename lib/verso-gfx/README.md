# Verso-Gfx

Graphics library for supporting basic windowed graphical applications. Useful classes for
window handling, audio, input devices (keyboard, mice, joysticks) and timing.

`4.11.2016 04:10 - Tested debug & release builds in Win32 and Linux64 (but not OS X).`
