#!/bin/bash

wc -l ../include/Verso/*.hpp \
../include/Verso/Audio/*.hpp \
../include/Verso/Display/*.hpp \
../include/Verso/Display/linux/*.hpp \
../include/Verso/Display/osx/*.hpp \
../include/Verso/Display/win32/*.hpp \
../include/Verso/Input/*.hpp \
../include/Verso/Input/GameController/*.hpp \
../include/Verso/Input/Joystick/*.hpp \
../include/Verso/Input/Keyboard/*.hpp \
../include/Verso/Input/Mouse/*.hpp \
../include/Verso/System/*.hpp \
../src/Verso/Audio/*.cpp \
../src/Verso/Display/*.cpp \
../src/Verso/Display/linux/*.cpp \
../src/Verso/Display/osx/*.cpp \
../src/Verso/Display/win32/*.cpp \
../src/Verso/Input/*.cpp \
../src/Verso/Input/Keyboard/*.cpp \
../src/Verso/System/*.cpp

