#include "../../verso-base/examples/examples.cpp"
#include "examples.hpp"
#include <cstdlib>


int main(int argc, char* argv[])
{
	(void)argc; (void)argv;

	versoBaseSimpleTests();
	versoGfxSimpleTests();

	return EXIT_SUCCESS;
}

