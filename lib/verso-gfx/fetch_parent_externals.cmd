PATH=%PATH%;"C:\Program Files\SlikSvn\bin";"C:\Program Files (x86)\SlikSvn\bin";

echo "verso-gfx: Fetching externals from repositories..."

IF NOT EXIST ..\SDL2 (
	git clone https://gitlab.com/dahliazone/pc/extlib/SDL2.git ..\SDL2
) else (
	pushd ..\SDL2
	git pull --rebase
	popd
)

IF NOT EXIST ..\BASS (
	git clone https://gitlab.com/dahliazone/pc/extlib/BASS.git ..\BASS
) else (
	pushd ..\BASS
	git pull --rebase
	popd
)

IF NOT EXIST ..\rtmidi (
	git clone https://gitlab.com/dahliazone/pc/extlib/rtmidi.git ..\rtmidi
) else (
	pushd ..\rtmidi
	git pull --rebase
	popd
)

IF NOT EXIST ..\verso-base (
	git clone https://gitlab.com/dahliazone/pc/lib/verso-base.git ..\verso-base
) else (
	pushd ..\verso-base
	git pull --rebase
	popd
)

echo "verso-base: Resursing into..."
pushd ..\verso-base
#call fetch_parent_externals.cmd
popd

echo "verso-gfx: All done"

