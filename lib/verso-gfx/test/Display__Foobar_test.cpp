/**
 * @file System__Endian_test.cpp
 * @brief Test for the class Endian.
 * @author Max Vilkki <codise@codise.org>
 *
 * TODO:
 * - \todo
 *
 * What is not tested(!):
 * - What:
 *   Why:
 */

#include "../../verso-base/examples/examples.cpp"
#include "../examples/examples.cpp"
#include <gtest/gtest.h>
#include <iostream>

namespace {

using Verso::Endian;


/**
 * Dummy test.
 */
TEST(DisplayFoobarTest, DummyTest)
{
	versoBaseSimpleTests();
	versoGfxSimpleTests();
}


} // End namespace nameless namespace

