# == Shared ===============================================================
! include( ../verso-gfx-common.pri ) {
    error("Couldn't find the verso-gfx-common.pri file!")
}

TEMPLATE = app
CONFIG += console

DESTDIR = "$${BUILD_DIR}"
TARGET = libverso-gfx_test


INCLUDEPATH += "$${PWD}"
INCLUDEPATH += "$${LIBPATH}/googletest/googletest/include/"


DEPENDPATH += \
    $${PWD}


SOURCES += \
    Display__Foobar_test.cpp \
    test.cpp


# == Linux ================================================================
linux-g++ {
    # BASS
    LIBS += -L$${LIBPATH}/BASS/linux/x64
}

linux-* {
    QMAKE_POST_LINK += echo "Post-link process..."

    # Verso-gfx
    LIBS += -L"$${BUILD_DIR}" -lverso-gfx

    # Verso-base
    LIBS += -L"$${LIBPATH}/verso-base/builds/$${BUILD_SUBDIR}" -lverso-base
    #QMAKE_POST_LINK += && cp -rf "$${LIBPATH}/verso-base/builds/$${BUILD_SUBDIR}/*.so*" "$${DESTDIR}/"

    # googletest
    LIBS += -L"$${LIBPATH}/googletest/build/googlemock/gtest" -lgtest -lpthread
    QMAKE_CXXFLAGS += "-isystem $${LIBPATH}/googletest/googletest/include/"

    # BASS
    INCLUDEPATH += "$${LIBPATH}/BASS/linux"
    QMAKE_CXXFLAGS += -D__LINUX_ALSA__
    QMAKE_POST_LINK += && cp -rf "$${LIBPATH}/BASS/linux/x64/*.so*" "$${DESTDIR}/"
    LIBS += -lbass

    # RtMidi
    QMAKE_CXXFLAGS += -D__LINUX_ALSA__
    LIBS += -lpthread -lasound

    # Other libs
    LIBS += -lGL -lGLU -lGLEW -lSDL2

    # cmath
    LIBS += -lm

    # Startup scripts
    QMAKE_POST_LINK += && cp -rf "$${PWD}/libverso-gfx-test-x64.sh" "$${DESTDIR}/"

    # Data
    QMAKE_POST_LINK += && cp -rf "$${PWD}/../data" "$${DESTDIR}/"
}


# == OS X =================================================================
macx {
    CONFIG -= app_bundle
    QMAKE_POST_LINK += echo "Post link-process..."

    # Verso-gfx
    LIBS += -L"$${BUILD_DIR}" -lverso-gfx
    LIBVERSOGFX = libverso-gfx.1.dylib
    QMAKE_POST_LINK += && install_name_tool -change "$${LIBVERSOGFX}" "@executable_path/$${LIBVERSOGFX}" "$${DESTDIR}/$${TARGET}"

    # Verso-base
    LIBS += -L"$${LIBPATH}/verso-base/builds/$${BUILD_SUBDIR}" -lverso-base
    LIBVERSOBASE = libverso-base.1.dylib
    QMAKE_POST_LINK += && cp -rf "$${LIBPATH}/verso-base/builds/$${BUILD_SUBDIR}/*.dylib*" "$${DESTDIR}/"
    QMAKE_POST_LINK += && install_name_tool -change "$${LIBVERSOBASE}" "@executable_path/$${LIBVERSOBASE}" "$${DESTDIR}/$${TARGET}"

    # googletest
    LIBS += -L"$${LIBPATH}/googletest/build/googlemock/gtest" -lgtest -lpthread
    QMAKE_CXXFLAGS += "-isystem $${LIBPATH}/googletest/googletest/include/"

    # BASS
    QMAKE_POST_LINK += && cp -rf "$${LIBPATH}/BASS/osx/*.dylib*" "$${DESTDIR}/"

    # Startup scripts
    QMAKE_POST_LINK += && cp -rf "$${PWD}/libverso-gfx-test-osx.sh" "$${DESTDIR}/"

    # Data
    QMAKE_POST_LINK += && cp -rf "$${PWD}/../data" "$${DESTDIR}/"
}


# == Windows ==============================================================
win* {
    QMAKE_POST_LINK += @ECHO "Post-link process..."

    # Verso-gfx
    LIBS += -L"$${BUILD_DIR}" -lverso-gfx

    # Verso-base
    LIBS += -L"$${LIBPATH}/verso-base/builds/$${BUILD_SUBDIR}" -lverso-base

    # SDL2
    LIBS += -lSDL2

    # Copy data directory
    QMAKE_POST_LINK += && XCOPY /s /q /y /i \"$${PWD}\..\data\" \"$${DESTDIR}\data\"

    # googletest & main program
    LIBS += -lgtest -lgtest_main

    # Remove temporaries
    #QMAKE_POST_LINK += && RMDIR /S /Q \"$${PWD}\..\debug\" \"$${PWD}\..\release\"
}

