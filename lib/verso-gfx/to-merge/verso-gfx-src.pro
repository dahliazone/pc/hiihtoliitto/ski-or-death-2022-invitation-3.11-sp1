# == Shared ===============================================================
! include( ../verso-gfx-common.pri ) {
    error("Couldn't find the verso-gfx-common.pri file!")
}

TEMPLATE = lib
win* {
    TEMPLATE = vclib
}

DESTDIR = "$${BUILD_DIR}"
TARGET = verso-gfx

DEPENDPATH += \
    $${HEADERPATH}/Verso/Audio \
    $${HEADERPATH}/Verso/Display/linux \
    $${HEADERPATH}/Verso/Display/osx \
    $${HEADERPATH}/Verso/Display/win32 \
    $${HEADERPATH}/Verso/Display \
    $${HEADERPATH}/Verso/Input/GameController \
    $${HEADERPATH}/Verso/Input/Joystick \
    $${HEADERPATH}/Verso/Input/Keyboard \
    $${HEADERPATH}/Verso/Input/Midi \
    $${HEADERPATH}/Verso/Input/Mouse \
    $${HEADERPATH}/Verso/Input \
    $${HEADERPATH}/Verso/System \
    $${HEADERPATH}/Verso \
    Verso/Audio \
    Verso/Display/linux \
    Verso/Display/osx \
    Verso/Display/win32 \
    Verso/Display \
    Verso/Input/Keyboard \
    Verso/Input/Midi \
    Verso/Input \
    Verso/System \
    Verso/


HEADERS += \
    $${LIBPATH}/rtmidi/RtMidi.h \
    $${HEADERPATH}/Verso/Audio/Audio2d.hpp \
    $${HEADERPATH}/Verso/Audio/Audio2dBass.hpp \
    $${HEADERPATH}/Verso/Display/linux/MessageDialogLinux.hpp \
    $${HEADERPATH}/Verso/Display/osx/MessageDialogOsx.hpp \
    $${HEADERPATH}/Verso/Display/win32/MessageDialogWin32.hpp \
    $${HEADERPATH}/Verso/Display/Display.hpp \
    $${HEADERPATH}/Verso/Display/DisplayMode.hpp \
    $${HEADERPATH}/Verso/Display/IWindow.hpp \
    $${HEADERPATH}/Verso/Display/MessageDialog.hpp \
    $${HEADERPATH}/Verso/Display/Native.hpp \
    $${HEADERPATH}/Verso/Display/PixelFormat.hpp \
    $${HEADERPATH}/Verso/Display/SwapInterval.hpp \
    $${HEADERPATH}/Verso/Display/WindowSdl2.hpp \
    $${HEADERPATH}/Verso/Display/WindowStyle.hpp \
    $${HEADERPATH}/Verso/Input/GameController/GameControllerAxis.hpp \
    $${HEADERPATH}/Verso/Input/GameController/GameControllerButton.hpp \
    $${HEADERPATH}/Verso/Input/GameController/GameControllerEvent.hpp \
    $${HEADERPATH}/Verso/Input/GameController/GameControllerEventType.hpp \
    $${HEADERPATH}/Verso/Input/Joystick/JoystickAxis.hpp \
    $${HEADERPATH}/Verso/Input/Joystick/JoystickButton.hpp \
    $${HEADERPATH}/Verso/Input/Joystick/JoystickEvent.hpp \
    $${HEADERPATH}/Verso/Input/Joystick/JoystickEventType.hpp \
    $${HEADERPATH}/Verso/Input/Joystick/JoystickHat.hpp \
    $${HEADERPATH}/Verso/Input/Joystick/JoystickHatDirection.hpp \
    $${HEADERPATH}/Verso/Input/Joystick/JoystickTrackball.hpp \
    $${HEADERPATH}/Verso/Input/Keyboard/KeyboardEvent.hpp \
    $${HEADERPATH}/Verso/Input/Keyboard/KeyboardEventType.hpp \
    $${HEADERPATH}/Verso/Input/Keyboard/KeyCodeConverterSdl2.hpp \
    $${HEADERPATH}/Verso/Input/Keyboard/KeyCode.hpp \
    $${HEADERPATH}/Verso/Input/Keyboard/Keymodifier.hpp \
    $${HEADERPATH}/Verso/Input/Keyboard/KeyScancode.hpp \
    $${HEADERPATH}/Verso/Input/Keyboard/KeySymbol.hpp \
    $${HEADERPATH}/Verso/Input/Midi/Midi.hpp \
    $${HEADERPATH}/Verso/Input/Mouse/MouseAxis.hpp \
    $${HEADERPATH}/Verso/Input/Mouse/MouseButton.hpp \
    $${HEADERPATH}/Verso/Input/Mouse/MouseEvent.hpp \
    $${HEADERPATH}/Verso/Input/Mouse/MouseEventType.hpp \
    $${HEADERPATH}/Verso/Input/Mouse/MouseState.hpp \
    $${HEADERPATH}/Verso/Input/AxisState.hpp \
    $${HEADERPATH}/Verso/Input/ButtonState.hpp \
    $${HEADERPATH}/Verso/Input/Event.hpp \
    $${HEADERPATH}/Verso/Input/InputController.hpp \
    $${HEADERPATH}/Verso/Input/EventType.hpp \
    $${HEADERPATH}/Verso/Input/InputManagerSdl2.hpp \
    $${HEADERPATH}/Verso/Input/InputNormalizer.hpp \
    $${HEADERPATH}/Verso/Input/Midi.hpp \
    $${HEADERPATH}/Verso/Input/WeightedButtonState.hpp \
    $${HEADERPATH}/Verso/System/InitWrapperSdl2.hpp \
    $${HEADERPATH}/Verso/System/IProgram.hpp \
    $${HEADERPATH}/Verso/System/main.hpp \
    $${HEADERPATH}/Verso/System/SubsystemSdl2.hpp \
    $${HEADERPATH}/Verso/Audio.hpp \
    $${HEADERPATH}/Verso/Display.hpp \
    $${HEADERPATH}/Verso/Input.hpp \
    $${HEADERPATH}/Verso/System.hpp \
    $${HEADERPATH}/Verso/Gfx.hpp


SOURCES += \
    $${LIBPATH}/rtmidi/RtMidi.cpp \
    Verso/Audio/Audio2dBass.cpp \
    Verso/Display/linux/MessageDialogLinux.cpp \
    Verso/Display/osx/MessageDialogOsx.cpp \
    Verso/Display/win32/debugConsoleWin32.cpp \
    Verso/Display/win32/MessageDialogWin32.cpp \
    Verso/Display/Display.cpp \
    Verso/Display/DisplayMode.cpp \
    Verso/Display/MessageDialog.cpp \
    Verso/Display/Native.cpp \
    Verso/Input/Keyboard/Key.cpp \
    Verso/Input/Keyboard/KeyCodeConverterSdl2.cpp \
    Verso/Input/Midi/Midi.cpp \
    Verso/Input/InputController.cpp \
    Verso/Input/InputManagerSdl2.cpp \
    Verso/System/InitWrapperSdl2.cpp \
    Verso/System/SubsystemSdl2.cpp


# == Linux ================================================================
linux-g++-32 {
}

linux-g++ {
}

linux-* {
}


# == OS X =================================================================
macx {
    QMAKE_POST_LINK += echo "Post-link process..."

    # Verso-base
    LIBS += -L"$${LIBPATH}/verso-base/builds/$${BUILD_SUBDIR}" -lverso-base
    QMAKE_POST_LINK += && cp -rf "$${LIBPATH}/verso-base/builds/$${BUILD_SUBDIR}/*.dylib*" "$${DESTDIR}/"

    # BASS
    LIBS += -L"$${LIBPATH}/BASS/osx" -lbass
    QMAKE_POST_LINK += && cp -rf "$${LIBPATH}/BASS/osx/*.dylib*" "$$DESTDIR/"

    # RtMidi
    QMAKE_CXXFLAGS += -D__MACOSX_CORE__
    LIBS += -lpthread -framework CoreMIDI -framework CoreAudio -framework CoreFoundation

    # Other libs
    LIBS += -framework Cocoa
    QT_CONFIG -= no-pkg-config
    PKG_CONFIG = /usr/local/bin/pkg-config
    CONFIG += link_pkgconfig
    PKGCONFIG += sdl2

    # cmath
    #LIBS += -lm
}


# == Windows ==============================================================
win* {
    QMAKE_POST_LINK += @ECHO "Post-link process..."
    CONFIG += staticlib

    # Copy DLLs
    QMAKE_POST_LINK += && COPY /y \"$${OUT_PWD}\..\..\BASS\windows\bass.dll\" \"$${DESTDIR}\"
    QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\glew32.dll\" \"$${DESTDIR}\"
    QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\libfreetype-6.dll\" \"$${DESTDIR}\"
    QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\libjpeg-9.dll\" \"$${DESTDIR}\"
    QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\libpng16-16.dll\" \"$${DESTDIR}\"
    QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\SDL2.dll\" \"$${DESTDIR}\"
    QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\zlib1.dll\" \"$${DESTDIR}\"

    # Copy data directory
    QMAKE_POST_LINK += && XCOPY /s /q /y /i \"$${PWD}\..\data\" \"$${DESTDIR}\data\"

    # Remove temporaries
    QMAKE_POST_LINK += && RMDIR /S /Q \"$${PWD}\..\debug\" \"$${PWD}\..\release\"
}

