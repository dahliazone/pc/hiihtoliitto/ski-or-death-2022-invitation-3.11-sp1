# Common configuration for all subdirectory .pro files.

# == Shared ===============================================================
HEADERPATH = "$${PWD}/include"
LIBPATH = "$${PWD}/.."
DATAPATH = "$${PWD}/data"

INCLUDEPATH += "$${HEADERPATH}"
INCLUDEPATH += "$${LIBPATH}"
INCLUDEPATH += "$${LIBPATH}/verso-base/include"
INCLUDEPATH += "$${LIBPATH}/utf8-cpp/source"
INCLUDEPATH += "$${LIBPATH}/xmlParser"
INCLUDEPATH += "$${LIBPATH}/SimpleJSON/src"
INCLUDEPATH += "$${LIBPATH}/rtmidi"

WARNINGS += -Wall
CONFIG -= qt core gui

CONFIG(debug, debug|release) {
    DEFINES += DEBUG
}


# == Linux ================================================================
linux-g++-32:CONFIG(debug, debug|release) {
    BUILD_SUBDIR = "linux-gcc32-debug"
    LIBS += "-rdynamic"
}

linux-g++-32:CONFIG(release, debug|release) {
    BUILD_SUBDIR = "linux-gcc32-release"
}

linux-g++:CONFIG(debug, debug|release) {
    BUILD_SUBDIR = "linux-gcc64-debug"
    LIBS += "-rdynamic"
}

linux-g++:CONFIG(release, debug|release) {
    BUILD_SUBDIR = "linux-gcc64-release"
}

linux-* {
    BUILD_DIR = "$${PWD}/builds/$${BUILD_SUBDIR}"
    DESTDIR = "$${BUILD_DIR}"
    UI_DIR = "$${DESTDIR}/uics"
    MOC_DIR = "$${DESTDIR}/mocs"
    OBJECTS_DIR = "$${DESTDIR}/objs"

    QMAKE_CXXFLAGS += "-std=c++17"
    QMAKE_CXXFLAGS += "-isystem $${HEADERPATH}"
    QMAKE_CXXFLAGS += "-isystem $${LIBPATH}"
    QMAKE_CXXFLAGS += "-isystem $${LIBPATH}/verso-base/include"
    QMAKE_CXXFLAGS += "-isystem $${LIBPATH}/utf8-cpp/source"
    QMAKE_CXXFLAGS += "-isystem $${LIBPATH}/xmlParser"
    QMAKE_CXXFLAGS += "-isystem $${LIBPATH}/rtmidi"
    QMAKE_CXXFLAGS += "-isystem $${LIBPATH}/BASS/linux"
}


# == OS X =================================================================
macx:CONFIG(debug, debug|release) {
    BUILD_SUBDIR = "macosx-debug"
}

macx:CONFIG(release, debug|release) {
    BUILD_SUBDIR = "macosx-release"
}

macx {
    BUILD_DIR = "$${PWD}/builds/$${BUILD_SUBDIR}"
    DESTDIR = "$${BUILD_DIR}"
    UI_DIR = "$${DESTDIR}/uics"
    MOC_DIR = "$${DESTDIR}/mocs"
    OBJECTS_DIR = "$${DESTDIR}/objs"

    QMAKE_CXXFLAGS += "-mmacosx-version-min=10.10"
    QMAKE_CXXFLAGS += "-std=c++17"

    # System includes
    INCLUDEPATH += "/usr/local/include"
    QMAKE_CXXFLAGS += "-isystem /usr/local/include"
    #INCLUDEPATH += /Library/Frameworks/SDL2.framework/Headers
    #QMAKE_CXXFLAGS += "-isystem /Library/Frameworks/SDL2.framework/Headers"
    #LIBS += -F/Library/Frameworks

    QMAKE_CXXFLAGS += "-isystem $${HEADERPATH}"
    QMAKE_CXXFLAGS += "-isystem $${LIBPATH}"
    QMAKE_CXXFLAGS += "-isystem $${LIBPATH}/verso-base/include"
    QMAKE_CXXFLAGS += "-isystem $${LIBPATH}/utf8-cpp/source"
    QMAKE_CXXFLAGS += "-isystem $${LIBPATH}/xmlParser"
    QMAKE_CXXFLAGS += "-isystem $${LIBPATH}/rtmidi"
    QMAKE_CXXFLAGS += "-isystem $${LIBPATH}/BASS/osx"
}


# == Windows ==============================================================
win* {
    # You may need to edit VERSO_VC to match your installation.
    VERSO_VC = "\code\libraries\vc2013"

    # Fix M_PI define
    #QMAKE_CXXFLAGS += /D_USE_MATH_DEFINES

    # googletest tuple fix
    DEFINES += "_VARIADIC_MAX=10"

    # Disable secure CRT nagging
    DEFINES += _CRT_SECURE_NO_WARNINGS

    # Include path for system installed libraries
    INCLUDEPATH += "$${VERSO_VC}/include"

    # SDL
    INCLUDEPATH += "$${VERSO_VC}/include/SDL2"

    # BASS
    INCLUDEPATH += "$${LIBPATH}/BASS/windows/c"

    # Linker debug info
    QMAKE_LIBFLAGS += /VERBOSE:LIB
    QMAKE_LFLAGS_WINDOWS += /VERBOSE:LIB
}

win32:CONFIG(debug, debug|release) {
    BUILD_SUBDIR = "win32-debug"
    VC_BUILDDIR = "Win32/Debug"
}

win32:CONFIG(release, debug|release) {
    BUILD_SUBDIR = "win32-release"
    VC_BUILDDIR = "Win32/Release"
}

win64:CONFIG(debug, debug|release) {
    BUILD_SUBDIR = "win64-debug"
    VC_BUILDDIR = "x64/Debug"
}

win64:CONFIG(release, debug|release) {
    BUILD_SUBDIR = "win64-release"
    VC_BUILDDIR = "x64/Release"
}

win* {
    LIBS += -L"$${VERSO_VC}\\lib\\$${VC_BUILDDIR}"
    DLL_DIR = "$${VERSO_VC}/dll/$${VC_BUILDDIR}"
    BUILD_DIR = "$${PWD}/builds/$${BUILD_SUBDIR}"
    DESTDIR = "$${OUT_PWD}/builds/$${VC_BUILDDIR}"
    MOC_DIR = "$${OUT_PWD}/$${VC_BUILDDIR}"
    OBJECTS_DIR = "$${MOC_DIR}"
    RCC_DIR = "$${MOC_DIR}"
    UI_DIR = "$${MOC_DIR}"
}

