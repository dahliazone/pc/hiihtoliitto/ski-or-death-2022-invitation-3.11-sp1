#ifndef VERSO_GFX_DISPLAY_MESSAGEDIALOGOSX_HPP
#define VERSO_GFX_DISPLAY_MESSAGEDIALOGOSX_HPP

#include <Verso/System/UString.hpp>

namespace Verso {
namespace priv {


class MessageDialogOsx
{
public:
	static void run(const UString& title, const UString& message);
};


} // End namespace priv
} // End namespace Verso

#endif // End header guard

