#ifndef VERSO_GFX_DISPLAY_MESSAGEDIALOG_HPP
#define VERSO_GFX_DISPLAY_MESSAGEDIALOG_HPP

#include <Verso/System/UString.hpp>

namespace Verso {


class MessageDialog
{
public:
	static void run(const UString& title, const UString& contents);
};


} // End namespace Verso

#endif // End header guard

