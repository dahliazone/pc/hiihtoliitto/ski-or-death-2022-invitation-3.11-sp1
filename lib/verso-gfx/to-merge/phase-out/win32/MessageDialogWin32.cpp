char MessageDialogWin32_symbol = 0;

#ifdef _MSC_VER

#include <Verso/Display/MessageDialog.hpp>
#include <Verso/Display/win32/MessageDialogWin32.hpp>
#include <Verso/System/Logger.hpp>

namespace Verso {
namespace priv {


void MessageDialogWin32::run(const UString& title, const UString& message)
{
	VERSO_LOG_INFO("verso-gfx", title.c_str());
	VERSO_LOG_INFO_VARIABLE("verso-gfx", message.c_str(), "");
	VERSO_LOG_FLUSH();
}


} // End namespace priv
} // End namespace Verso


#endif // End #ifdef _MSC_VER

