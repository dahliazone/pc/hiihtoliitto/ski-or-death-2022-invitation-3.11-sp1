#ifndef VERSO_GFX_DISPLAY_MESSAGEDIALOGLINUX_HPP
#define VERSO_GFX_DISPLAY_MESSAGEDIALOGLINUX_HPP

#include <Verso/System/UString.hpp>

namespace Verso {
namespace priv {


class MessageDialogLinux
{
public:
	static void run(const UString& title, const UString& message);
};


} // End namespace priv
} // End namespace Verso

#endif // End header guard

