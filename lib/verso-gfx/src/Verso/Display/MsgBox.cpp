#include <Verso/Display/MsgBox.hpp>
#include <Verso/System/Exception.hpp>
#include <Verso/System/Logger.hpp>
#include <SDL2/SDL.h>

namespace Verso {


UString wrapLongLines(const UString& text, int maxLineLength, const UString& newLinePadding = "")
{
	UString out(text);
	int i = 0;
	int lineCharCounter = 0;
	while (i < static_cast<int>(out.size())) {
		if (out[i] == '\n') {
			lineCharCounter = 0;
		}
		else {
			lineCharCounter++;
			if (lineCharCounter > maxLineLength) {
				UString tmp = out.substring(0, i + 1);
				tmp += "\n" + newLinePadding;
				out = tmp + out.substring(i + 1);
				lineCharCounter = 0;
				i += 1 + static_cast<int>(newLinePadding.size());
			}
		}
		i++;
	}

	return out;
}


bool MsgBox::run(MsgBoxType type, const UString& title, const UString& message)
{
	Uint32 flags = 0;

	switch (type) {
	case MsgBoxType::Error:
	{
		flags = SDL_MESSAGEBOX_ERROR;
		VERSO_LOG_ERR_EMPTY_ROW();
		VERSO_LOG_ERR("verso-gfx", title.c_str());
		VERSO_LOG_ERR_DIRECT("verso-gfx", "  " << message.c_str());
		break;
	}
	case MsgBoxType::Warning:
	{
		flags = SDL_MESSAGEBOX_WARNING;
		VERSO_LOG_ERR_EMPTY_ROW();
		VERSO_LOG_ERR("verso-gfx", title.c_str());
		VERSO_LOG_ERR_DIRECT("verso-gfx", "  " << message.c_str());
		break;
	}
	case MsgBoxType::Information:
	{
		flags = SDL_MESSAGEBOX_INFORMATION;
		VERSO_LOG_INFO_EMPTY_ROW();
		VERSO_LOG_INFO("verso-gfx", title.c_str());
		VERSO_LOG_INFO_DIRECT("verso-gfx", "  " << message.c_str());
		break;
	}
	default:
	{
		UString error("Unknown/unsupported MsgBoxType(");
		error.append2(static_cast<std::int32_t>(type));
		error += ")";
		VERSO_ERROR("verso-gfx", error.c_str(), "");
	}
	}

	int maxSizeLength = 120;
	UString newLinePadding("    ");
	UString messageWrapped = wrapLongLines(message, maxSizeLength, newLinePadding);

	if (SDL_ShowSimpleMessageBox(flags, title.c_str(), messageWrapped.c_str(), nullptr) == 0) {
		return true;
	}
	else {
		return false;
	}
}


} // End namespace Verso

