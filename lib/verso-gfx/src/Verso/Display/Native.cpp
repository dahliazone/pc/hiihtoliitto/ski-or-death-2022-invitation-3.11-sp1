#include <Verso/Display/Native.hpp>
#include <SDL_syswm.h>

namespace Verso {


namespace Native {


void* getWindowHandle(SDL_Window* window)
{
	SDL_SysWMinfo wmInfo;
	SDL_VERSION(&wmInfo.version); // create info structure with SDL version info
	if (SDL_GetWindowWMInfo(window, &wmInfo)) {
#ifdef SDL_SYSWM_WINDOWS
		return wmInfo.info.win.window;
#else
		return nullptr;
#endif
	}
	else {
		return nullptr;
	}
}


} // End namespace Native


} // End namespace Verso

