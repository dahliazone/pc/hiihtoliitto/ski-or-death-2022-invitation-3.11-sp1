#include <Verso/Display/Window2d.hpp>
#include <Verso/System/InitWrapperSdl2.hpp>
#include <Verso/Input/InputManager.hpp>
#include <Verso/Display/Native.hpp>
#include <SDL2/SDL.h>
#include <algorithm>

namespace Verso {


std::vector<Window2d*> Window2d::windowsStillOpen = std::vector<Window2d*>();


SDL_SystemCursor systemCursorTypeToSdlSystemCursorType(SystemCursorType systemCursorType)
{
	switch (systemCursorType)
	{
		case SystemCursorType::Arrow:
			return SDL_SYSTEM_CURSOR_ARROW;
		case SystemCursorType::IBeam:
			return SDL_SYSTEM_CURSOR_IBEAM;
		case SystemCursorType::Wait:
			return SDL_SYSTEM_CURSOR_WAIT;
		case SystemCursorType::Crosshair:
			return SDL_SYSTEM_CURSOR_CROSSHAIR;
		case SystemCursorType::WaitArrow:
			return SDL_SYSTEM_CURSOR_WAITARROW;
		case SystemCursorType::SizeNwSe:
			return SDL_SYSTEM_CURSOR_SIZENWSE;
		case SystemCursorType::SizeNeSw:
			return SDL_SYSTEM_CURSOR_SIZENESW;
		case SystemCursorType::SizeWe:
			return SDL_SYSTEM_CURSOR_SIZEWE;
		case SystemCursorType::SizeNs:
			return SDL_SYSTEM_CURSOR_SIZENS;
		case SystemCursorType::SizeAll:
			return SDL_SYSTEM_CURSOR_SIZEALL;
		case SystemCursorType::No:
			return SDL_SYSTEM_CURSOR_NO;
		case SystemCursorType::Hand:
			return SDL_SYSTEM_CURSOR_HAND;
		case SystemCursorType::Unset:
			break;
		case SystemCursorType::EnumSize:
			break;
	}
	return SDL_NUM_SYSTEM_CURSORS;
}


Window2d::Window2d(const UString& id) :
	created(false),
	id(id),
	displayMode(),
	display(),
	title(""),
	sdlWindow(nullptr),
	sdlRenderer(nullptr),
	sdlSystemCursors(),
	renderDownscaleRatio(),
	renderResolution(),
	renderDisplayAspectRatio(),
	forcedRenderDisplayAspectRatio(false),
	renderPixelAspectRatio(),
	//iconFileName(),
	relativeViewport(0.0f, 0.0f, 1.0f, 1.0f),
	cameraInputControllerId(),
	cameraInputController(nullptr),
	cameraForward(0),
	cameraBackward(0),
	cameraLeft(0),
	cameraRight(0),
	cameraUp(0),
	cameraDown(0),
	cameraForwardFast(0),
	cameraBackwardFast(0),
	cameraLeftFast(0),
	cameraRightFast(0),
	cameraUpFast(0),
	cameraDownFast(0),
	cameraForwardReallyFast(0),
	cameraBackwardReallyFast(0),
	cameraLeftReallyFast(0),
	cameraRightReallyFast(0),
	cameraUpReallyFast(0),
	cameraDownReallyFast(0),
	cameraActionA(0),
	cameraActionB(0),
	latestClipboardText()
{
	sdlSystemCursors.fill(nullptr);
}


Window2d::Window2d(
		const UString& id, const DisplayMode& displayMode, const Display& display,
		const UString& title, WindowStyle style,
		const RenderDownscaleRatio& renderDownscaleRatio,
		const AspectRatio& renderDisplayAspectRatio,
		const PixelAspectRatio& renderPixelAspectRatio,
		const SwapInterval& swapInterval,
		bool forceSoftware, bool forceAccelerated, bool forceRenderToTexture) :
	created(false),
	id(id),
	displayMode(),
	display(),
	title(""),
	sdlWindow(nullptr),
	sdlRenderer(nullptr),
	sdlSystemCursors(),
	renderDownscaleRatio(),
	renderResolution(),
	renderDisplayAspectRatio(),
	forcedRenderDisplayAspectRatio(),
	renderPixelAspectRatio(renderPixelAspectRatio),
	//iconFileName(),
	relativeViewport(0.0f, 0.0f, 1.0f, 1.0f),
	cameraInputControllerId(),
	cameraInputController(nullptr),
	cameraForward(0),
	cameraBackward(0),
	cameraLeft(0),
	cameraRight(0),
	cameraUp(0),
	cameraDown(0),
	cameraForwardFast(0),
	cameraBackwardFast(0),
	cameraLeftFast(0),
	cameraRightFast(0),
	cameraUpFast(0),
	cameraDownFast(0),
	cameraForwardReallyFast(0),
	cameraBackwardReallyFast(0),
	cameraLeftReallyFast(0),
	cameraRightReallyFast(0),
	cameraUpReallyFast(0),
	cameraDownReallyFast(0),
	cameraActionA(0),
	cameraActionB(0),
	latestClipboardText()
{
	sdlSystemCursors.fill(nullptr);
	create(displayMode, display, title, static_cast<WindowStyles>(style), renderDownscaleRatio,
		   renderDisplayAspectRatio, renderPixelAspectRatio, swapInterval,
		   forceSoftware, forceAccelerated, forceRenderToTexture);
}


Window2d::Window2d(
		const UString& id, const DisplayMode& displayMode, const Display& display,
		const UString& title, WindowStyles styles,
		const RenderDownscaleRatio& renderDownscaleRatio,
		const AspectRatio& renderDisplayAspectRatio,
		const PixelAspectRatio& renderPixelAspectRatio,
		const SwapInterval& swapInterval,
		bool forceSoftware, bool forceAccelerated, bool forceRenderToTexture) :
	created(false),
	id(id),
	displayMode(),
	display(),
	title(""),
	sdlWindow(nullptr),
	sdlRenderer(nullptr),
	sdlSystemCursors(),
	renderDownscaleRatio(),
	renderResolution(),
	renderDisplayAspectRatio(),
	forcedRenderDisplayAspectRatio(false),
	renderPixelAspectRatio(renderPixelAspectRatio),
	//iconFileName(),
	relativeViewport(0.0f, 0.0f, 1.0f, 1.0f),
	cameraInputControllerId(),
	cameraInputController(nullptr),
	cameraForward(0),
	cameraBackward(0),
	cameraLeft(0),
	cameraRight(0),
	cameraUp(0),
	cameraDown(0),
	cameraForwardFast(0),
	cameraBackwardFast(0),
	cameraLeftFast(0),
	cameraRightFast(0),
	cameraUpFast(0),
	cameraDownFast(0),
	cameraForwardReallyFast(0),
	cameraBackwardReallyFast(0),
	cameraLeftReallyFast(0),
	cameraRightReallyFast(0),
	cameraUpReallyFast(0),
	cameraDownReallyFast(0),
	cameraActionA(0),
	cameraActionB(0),
	latestClipboardText()
{
	sdlSystemCursors.fill(nullptr);
	create(displayMode, display, title, styles, renderDownscaleRatio,
		   renderDisplayAspectRatio, renderPixelAspectRatio, swapInterval,
		   forceSoftware, forceAccelerated, forceRenderToTexture);
}


void Window2d::create(
		const DisplayMode& displayMode, const Display& display,
		const UString& title, WindowStyle style,
		const RenderDownscaleRatio& renderDownscaleRatio,
		const AspectRatio& renderDisplayAspectRatio,
		const PixelAspectRatio& renderPixelAspectRatio,
		const SwapInterval& swapInterval,
		bool forceSoftware, bool forceAccelerated, bool forceRenderToTexture)
{
	create(displayMode, display, title, static_cast<WindowStyles>(style), renderDownscaleRatio,
		   renderDisplayAspectRatio, renderPixelAspectRatio, swapInterval,
		   forceSoftware, forceAccelerated, forceRenderToTexture);
}


void Window2d::create(
		const DisplayMode& displayMode, const Display& display,
		const UString& title, WindowStyles styles,
		const RenderDownscaleRatio& renderDownscaleRatio,
		const AspectRatio& renderDisplayAspectRatio,
		const PixelAspectRatio& renderPixelAspectRatio,
		const SwapInterval& swapInterval,
		bool forceSoftware, bool forceAccelerated, bool forceRenderToTexture)
{
	VERSO_ASSERT_MSG("verso-gfx", isCreated() == false, "Already created!");

	InitWrapperSdl2::instance().createSubsystem(SubsystemSdl2::Video);

	this->displayMode = displayMode;
	this->display = display;
	this->title = title;
	this->sdlWindow = nullptr;
	this->sdlRenderer = nullptr;
	//this->windowId = -1;

	Uint32 flags = SDL_WINDOW_OPENGL;

	if (styles & static_cast<WindowStyles>(WindowStyle::Fullscreen)) {
		flags |= SDL_WINDOW_FULLSCREEN;
	}

	if (styles & static_cast<WindowStyles>(WindowStyle::FullscreenDesktop)) {
		flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
	}

	if (styles & static_cast<WindowStyles>(WindowStyle::Hidden)) {
		flags |=  SDL_WINDOW_HIDDEN;
	}

	if (styles & static_cast<WindowStyles>(WindowStyle::Borderless)) {
		flags |= SDL_WINDOW_BORDERLESS;
	}

	if (styles & static_cast<WindowStyles>(WindowStyle::Resizable)) {
		flags |=  SDL_WINDOW_RESIZABLE;
	}

	if (styles & static_cast<WindowStyles>(WindowStyle::Minimized)) {
		flags |=  SDL_WINDOW_MINIMIZED;
	}

	if (styles & static_cast<WindowStyles>(WindowStyle::Maximized)) {
		flags |=  SDL_WINDOW_MAXIMIZED;
	}

	if (styles & static_cast<WindowStyles>(WindowStyle::InputGrabbed)) {
		flags |=  SDL_WINDOW_INPUT_GRABBED;
	}

	if (styles & static_cast<WindowStyles>(WindowStyle::AllowHighDpi)) {
		flags |=  SDL_WINDOW_ALLOW_HIGHDPI;
	}

	int windowX = SDL_WINDOWPOS_UNDEFINED;
	int windowY = SDL_WINDOWPOS_UNDEFINED;
	if (display.index != -1) {
		windowX = display.rect.pos.x;
		windowY = display.rect.pos.y;
	}
	if (!(styles & static_cast<WindowStyles>(WindowStyle::ForceOnTop))) {
		SDL_SetHint(SDL_HINT_ALLOW_TOPMOST, "0");
	}
	sdlWindow = SDL_CreateWindow(
				this->title.c_str(), windowX, windowY,
				displayMode.resolution.x, displayMode.resolution.y,
				flags);

	// Check that the window was successfully made
	if (sdlWindow == nullptr){
		UString error("Could not create window. Reason: ");
		error += SDL_GetError();
		VERSO_ERROR("verso-gfx", error.c_str(), "");
	}

	Uint32 rendererFlags = 0;
	if (swapInterval == SwapInterval::Vsync || swapInterval == SwapInterval::AdaptiveVsync) {
		rendererFlags |= SDL_RENDERER_PRESENTVSYNC;
	}
	if (forceSoftware == true) {
		rendererFlags |= SDL_RENDERER_SOFTWARE;
	}
	if (forceAccelerated == true) {
		rendererFlags |= SDL_RENDERER_ACCELERATED;
	}
	if (forceRenderToTexture == true) {
		rendererFlags |= SDL_RENDERER_TARGETTEXTURE;
	}

	sdlRenderer = SDL_CreateRenderer(reinterpret_cast<SDL_Window*>(sdlWindow), -1, rendererFlags); // \TODO: support driver index

	// setup vertical sync
	if (swapInterval ==  SwapInterval::Immediate) {
		SDL_SetHint(SDL_HINT_RENDER_VSYNC, "0");
	}
	else if (swapInterval == SwapInterval::Vsync || swapInterval == SwapInterval::AdaptiveVsync) {
		SDL_SetHint(SDL_HINT_RENDER_VSYNC, "1");
	}
	// in any case => pass SDL_RENDERER_PRESENTVSYNC to SDL_CreateRenderer()

	this->renderDownscaleRatio = renderDownscaleRatio;

	// No custom renderDisplayAspectRatio given => use displayMode aspect ratio
	if (renderDisplayAspectRatio.value == 0) {
		this->renderDisplayAspectRatio = AspectRatio(getDrawableResolutioni(), "");  // \TODO: fix aspect ratio if known
		this->forcedRenderDisplayAspectRatio = false;
	}
	else {
		this->renderDisplayAspectRatio = renderDisplayAspectRatio;
		this->forcedRenderDisplayAspectRatio = true;
	}

	this->renderPixelAspectRatio = renderPixelAspectRatio;

	onResize();

	// Create camera input controller
	cameraInputControllerId = "versogfx-window2d-camera";
	cameraInputControllerId.append2(Window2d::getUniqueInstanceId());
	cameraInputController = InputManager::instance().createInputController(cameraInputControllerId);
	cameraForward = cameraInputController->mapKeyboardToButton("forward", KeyCode::Up);
	cameraInputController->mapKeyboardToButton("forward", KeyCode::W);
	cameraBackward = cameraInputController->mapKeyboardToButton("backward", KeyCode::Down);
	cameraInputController->mapKeyboardToButton("backward", KeyCode::S);
	cameraLeft = cameraInputController->mapKeyboardToButton("left", KeyCode::Left);
	cameraInputController->mapKeyboardToButton("left", KeyCode::A);
	cameraRight = cameraInputController->mapKeyboardToButton("right", KeyCode::Right);
	cameraInputController->mapKeyboardToButton("right", KeyCode::D);
	cameraUp = cameraInputController->mapKeyboardToButton("up", KeyCode::PageUp);
	cameraInputController->mapKeyboardToButton("up", KeyCode::R);
	cameraDown = cameraInputController->mapKeyboardToButton("down", KeyCode::PageDown);
	cameraInputController->mapKeyboardToButton("down", KeyCode::F);

	cameraForwardFast = cameraInputController->mapKeyboardToButton("forwardFast", KeyCode::Up, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraInputController->mapKeyboardToButton("forwardFast", KeyCode::W, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraBackwardFast = cameraInputController->mapKeyboardToButton("backwardFast", KeyCode::Down, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraInputController->mapKeyboardToButton("backwardFast", KeyCode::S, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraLeftFast = cameraInputController->mapKeyboardToButton("leftFast", KeyCode::Left, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraInputController->mapKeyboardToButton("leftFast", KeyCode::A, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraRightFast = cameraInputController->mapKeyboardToButton("rightFast", KeyCode::Right, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraInputController->mapKeyboardToButton("rightFast", KeyCode::D, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraUpFast = cameraInputController->mapKeyboardToButton("upFast", KeyCode::PageUp, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraInputController->mapKeyboardToButton("upFast", KeyCode::R, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraDownFast = cameraInputController->mapKeyboardToButton("downFast", KeyCode::PageDown, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraInputController->mapKeyboardToButton("downFast", KeyCode::F, static_cast<Keymodifiers>(Keymodifier::AnyShift));

	cameraForwardReallyFast = cameraInputController->mapKeyboardToButton("forwardReallyFast", KeyCode::Up, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraInputController->mapKeyboardToButton("forwardReallyFast", KeyCode::W, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraBackwardReallyFast = cameraInputController->mapKeyboardToButton("backwardReallyFast", KeyCode::Down, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraInputController->mapKeyboardToButton("backwardReallyFast", KeyCode::S, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraLeftReallyFast = cameraInputController->mapKeyboardToButton("leftReallyFast", KeyCode::Left, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraInputController->mapKeyboardToButton("leftReallyFast", KeyCode::A, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraRightReallyFast = cameraInputController->mapKeyboardToButton("rightReallyFast", KeyCode::Right, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraInputController->mapKeyboardToButton("rightReallyFast", KeyCode::D, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraUpReallyFast = cameraInputController->mapKeyboardToButton("upReallyFast", KeyCode::PageUp, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraInputController->mapKeyboardToButton("upReallyFast", KeyCode::R, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraDownReallyFast = cameraInputController->mapKeyboardToButton("downReallyFast", KeyCode::PageDown, static_cast<Keymodifiers>(Keymodifier::AnyShift));
	cameraInputController->mapKeyboardToButton("downReallyFast", KeyCode::F, static_cast<Keymodifiers>(Keymodifier::AnyShift));

	cameraActionA = cameraInputController->mapKeyboardToButton("action-a", KeyCode::N1);
	cameraActionB = cameraInputController->mapKeyboardToButton("action-b", KeyCode::N2);

	latestClipboardText = "";

	Window2d::windowsStillOpen.push_back(this);
	Window2d::getUniqueInstanceId()++;

	created = true;
}


Window2d::~Window2d()
{
	destroy();
}


///////////////////////////////////////////////////////////////////////////////////////////
// interface IWindow
///////////////////////////////////////////////////////////////////////////////////////////

void Window2d::destroy() VERSO_NOEXCEPT
{
	if (isCreated() == false) {
		return;
	}

	if (sdlWindow != nullptr) {
		SDL_DestroyWindow(reinterpret_cast<SDL_Window*>(sdlWindow));
		sdlWindow = nullptr;
		renderResolution.x = 0;
		renderResolution.y = 0;

		auto it = std::find(windowsStillOpen.begin(), windowsStillOpen.end(), this);
		if (it != windowsStillOpen.end()) {
			*it = nullptr;
			windowsStillOpen.erase(it);
		}
	}

	for (auto sdlSystemCursor : sdlSystemCursors) {
		if (sdlSystemCursor != nullptr) {
			SDL_FreeCursor(reinterpret_cast<SDL_Cursor*>(sdlSystemCursor));
		}
	}

	created = false;
}


bool Window2d::isCreated() const
{
	return created;
}


void Window2d::close() VERSO_NOEXCEPT
{
	destroy();
}


bool Window2d::isOpen() const
{
	if (sdlWindow != nullptr) {
		return true;
	}
	else {
		return false;
	}
}


UString Window2d::getId() const
{
	return id;
}


Vector2i Window2d::getWindowResolutioni() const
{
	Vector2i windowResolution;
	SDL_GetWindowSize(reinterpret_cast<SDL_Window*>(this->sdlWindow),
					  &windowResolution.x, &windowResolution.y);
	return windowResolution;
}


Vector2f Window2d::getWindowResolutionf() const
{
	Vector2i windowResolution;
	SDL_GetWindowSize(reinterpret_cast<SDL_Window*>(this->sdlWindow),
					  &windowResolution.x, &windowResolution.y);
	return Vector2f(
				static_cast<float>(windowResolution.x),
				static_cast<float>(windowResolution.y));
}


void Window2d::setWindowResolution(const Vector2i& windowResolution)
{
	SDL_SetWindowSize(reinterpret_cast<SDL_Window*>(this->sdlWindow),
					  windowResolution.x, windowResolution.y);
}


RenderDownscaleRatio Window2d::getRenderDownscaleRatio() const
{
	return renderDownscaleRatio;
}


Vector2i Window2d::getRenderResolutioni() const
{
	return renderResolution;
}


Vector2f Window2d::getRenderResolutionf() const
{
	return Vector2f(
				static_cast<float>(renderResolution.x),
				static_cast<float>(renderResolution.y));
}


Vector2i Window2d::getDrawableResolutioni() const
{
	Vector2i s;
	SDL_GL_GetDrawableSize(reinterpret_cast<SDL_Window*>(sdlWindow), &s.x, &s.y);
	return s;
}


Vector2f Window2d::getDrawableResolutionf() const
{
	Vector2i ds(getDrawableResolutioni());
	return Vector2f(
				static_cast<float>(ds.x),
				static_cast<float>(ds.y));
}


AspectRatio Window2d::getRenderDisplayAspectRatio() const
{
	return renderDisplayAspectRatio;
}


void Window2d::setRenderDisplayAspectRatio(const AspectRatio& renderDisplayAspectRatio)
{
	// No custom renderDisplayAspectRatio given => use displayMode aspect ratio
	if (renderDisplayAspectRatio.value == 0) {
		this->renderDisplayAspectRatio = AspectRatio(getDrawableResolutioni(), ""); // \TODO: fix aspect ratio if known
		this->forcedRenderDisplayAspectRatio = false;
	}
	else {
		this->renderDisplayAspectRatio = renderDisplayAspectRatio;
		this->forcedRenderDisplayAspectRatio = true;
	}
	// \TODO: need to resize render buffer?
	//onResize();
}


PixelAspectRatio Window2d::getRenderPixelAspectRatio() const
{
	return renderPixelAspectRatio;
}


void Window2d::setRenderPixelAspectRatio(const PixelAspectRatio& renderPixelAspectRatio)
{
	this->renderPixelAspectRatio = renderPixelAspectRatio;
	// \TODO: need to resize render buffer?
	//onResize();
}


void Window2d::makeActive()
{
	SDL_Rect rect = {
		static_cast<int>(relativeViewport.pos.x * displayMode.resolution.x),
		static_cast<int>(relativeViewport.pos.y * displayMode.resolution.y),
		static_cast<int>(relativeViewport.size.x * displayMode.resolution.x),
		static_cast<int>(relativeViewport.size.y * displayMode.resolution.y)
	};

	SDL_RenderSetViewport(reinterpret_cast<SDL_Renderer*>(sdlRenderer), &rect);
}


void Window2d::swapScreenBuffer() const
{
	SDL_RenderPresent(reinterpret_cast<SDL_Renderer*>(sdlRenderer));
}


const UString& Window2d::getClipboardText()
{
	char* tempStr = SDL_GetClipboardText();
	latestClipboardText.set(tempStr);
	free(tempStr);
	return latestClipboardText;
}


void Window2d::setClipboardText(const UString& text)
{
	SDL_SetClipboardText(text.c_str());
}


bool Window2d::isCursorShown() const
{
	return SDL_ShowCursor(SDL_QUERY) != 0;
}


void Window2d::setCursorShown(bool shown) const
{
	if (shown == true) {
		showCursor();
	}
	else {
		hideCursor();
	}
}


void Window2d::showCursor() const
{
	SDL_ShowCursor(SDL_ENABLE);
}


void Window2d::hideCursor() const
{
	SDL_ShowCursor(SDL_DISABLE);
}


bool Window2d::hasInputGrabbed() const
{
	return (SDL_GetWindowFlags(reinterpret_cast<SDL_Window*>(sdlWindow)) & SDL_WINDOW_INPUT_GRABBED) != 0;
}


bool Window2d::hasInputFocus() const
{
	return (SDL_GetWindowFlags(reinterpret_cast<SDL_Window*>(sdlWindow)) & SDL_WINDOW_INPUT_FOCUS) != 0;
}


bool Window2d::hasMouseFocus() const
{
	return (SDL_GetWindowFlags(reinterpret_cast<SDL_Window*>(sdlWindow)) & SDL_WINDOW_MOUSE_FOCUS) != 0;
}


MouseState Window2d::getMouseState() const
{
	MouseState state = InputManager::instance().getMouseState();
	state.windowId = SDL_GetWindowID(reinterpret_cast<SDL_Window*>(sdlWindow));

	// Set mouse location relative to window to -1,-1 if no mouse / on another screen, etc.
	if (!hasMouseFocus()) {
		state.x = -1;
		state.y = -1;
	}

	return state;
}


bool Window2d::captureMouse(bool enabled) const
{
	return (SDL_CaptureMouse((enabled == true ? SDL_TRUE : SDL_FALSE)) == 0 ? true : false);
}


bool Window2d::warpMouseGlobal(int x, int y) const
{
	return SDL_WarpMouseGlobal(x, y) == 0 ? true : false;
}


void Window2d::warpMouseInWindow(int x, int y) const
{
	SDL_WarpMouseInWindow(reinterpret_cast<SDL_Window*>(sdlWindow), x, y);
}


bool Window2d::createSystemCursor(SystemCursorType systemCursorType)
{
	SDL_SystemCursor sdlSystemCursorType = systemCursorTypeToSdlSystemCursorType(systemCursorType);
	if (sdlSystemCursorType == SDL_NUM_SYSTEM_CURSORS) {
		return false;
	}

	destroySystemCursor(systemCursorType);

	sdlSystemCursors[static_cast<size_t>(sdlSystemCursorType)] =
			static_cast<void*>(
				SDL_CreateSystemCursor(sdlSystemCursorType));

	return true;
}


void Window2d::destroySystemCursor(SystemCursorType systemCursorType)
{
	void* sdlSystemCursor = sdlSystemCursors[static_cast<size_t>(systemCursorType)];
	if (sdlSystemCursor != nullptr) {
		SDL_FreeCursor(reinterpret_cast<SDL_Cursor*>(sdlSystemCursor));
	}
}


bool Window2d::setSystemCursor(SystemCursorType systemCursorType) const
{
	void* sdlSystemCursor = sdlSystemCursors[static_cast<size_t>(systemCursorType)];
	if (sdlSystemCursor == nullptr) {
		return false;
	}

	SDL_SetCursor(reinterpret_cast<SDL_Cursor*>(sdlSystemCursor));
	return true;
}

void* Window2d::getNativeWindowHandle() const
{
	return Native::getWindowHandle(reinterpret_cast<SDL_Window*>(sdlWindow));
}


void* Window2d::getRenderer() const
{
	return sdlRenderer;
}


void Window2d::show()
{
	SDL_ShowWindow(reinterpret_cast<SDL_Window*>(sdlWindow));
	setWindowResolution(displayMode.resolution); // \TODO: is this resize really necessary?
	// \TODO: need to resize render buffer?
	//onResize();
}


void Window2d::hide()
{
	SDL_HideWindow(reinterpret_cast<SDL_Window*>(sdlWindow));
}


DisplayMode Window2d::getDisplayMode() const
{
	return displayMode;
}


void Window2d::setDisplayMode(const DisplayMode& displayMode)
{
	this->displayMode = displayMode;
}


Display Window2d::getDisplay() const
{
	return display;
}


void Window2d::setDisplay(const Display& display)
{
	this->display = display;
}


UString Window2d::getTitle() const
{
	return this->title;
}


void Window2d::setTitle(const UString& windowTitle)
{
	this->title = windowTitle;
	SDL_SetWindowTitle(reinterpret_cast<SDL_Window*>(sdlWindow), this->title.c_str());
}


Vector2i Window2d::getPositioni() const
{
	Vector2i point;
	SDL_GetWindowPosition(reinterpret_cast<SDL_Window*>(sdlWindow), &point.x, &point.y);
	return point;
}


Vector2f Window2d::getPositionf() const
{
	Vector2i pos(getPositioni());
	return Vector2f(
				static_cast<float>(pos.x),
				static_cast<float>(pos.y));
}


void Window2d::setPosition(const Vector2i& location)
{
	SDL_SetWindowPosition(reinterpret_cast<SDL_Window*>(sdlWindow), location.x, location.y);
}


void Window2d::setPositionCentered()
{
	setPosition(Align(HAlign::Center, VAlign::Center).objectInContainerOffseti(displayMode.resolution, display.rect));
}


void Window2d::setPositionAlignedOnDisplay(const Display& anotherDisplay, const Align& align)
{
	setPosition(align.objectInContainerOffseti(displayMode.resolution, anotherDisplay.rect));
}


Rectf Window2d::getRelativeViewport() const
{
	return relativeViewport;
}


void Window2d::setRelativeViewport(const Rectf& relativeViewport)
{
	this->relativeViewport = relativeViewport;
	VERSO_FAIL("verso-gfx"); // \TODO: implement this
}


void Window2d::resetRelativeViewport()
{
	setRelativeViewport(Rectf(0.0f, 0.0f, 1.0f, 1.0f));
	VERSO_FAIL("verso-gfx"); // \TODO: implement this
}


Recti Window2d::getRenderViewporti() const
{
	Rectf relative = getRelativeViewport();
	return Recti(static_cast<int>(relative.pos.x * renderResolution.x),
				 static_cast<int>(relative.pos.y * renderResolution.y),
				 static_cast<int>(relative.size.x * renderResolution.x),
				 static_cast<int>(relative.size.y * renderResolution.y));
}


Rectf Window2d::getRenderViewportf() const
{
	Recti vp = getRenderViewporti();
	return Rectf(static_cast<float>(vp.pos.x),
				 static_cast<float>(vp.pos.y),
				 static_cast<float>(vp.size.x),
				 static_cast<float>(vp.size.y));
}


void Window2d::applyRenderViewport() const
{
	VERSO_FAIL("verso-gfx"); // \TODO: implement this
}


Recti Window2d::getDrawableViewporti() const
{
	Rectf relative = getRelativeViewport();
	Vector2i drawableResolution(getDrawableResolutioni());
	return Recti(static_cast<int>(relative.pos.x * drawableResolution.x),
				 static_cast<int>(relative.pos.y * drawableResolution.y),
				 static_cast<int>(relative.size.x * drawableResolution.x),
				 static_cast<int>(relative.size.y * drawableResolution.y));
}


Rectf Window2d::getDrawableViewportf() const
{
	Recti vp = getRenderViewporti();
	return Rectf(static_cast<float>(vp.pos.x),
				 static_cast<float>(vp.pos.y),
				 static_cast<float>(vp.size.x),
				 static_cast<float>(vp.size.y));
}


void Window2d::applyDrawableViewport() const
{
	VERSO_FAIL("verso-gfx"); // \TODO: implement this
}


void Window2d::loadIcon(const UString& iconFileName)
{
	// Load icon
	/*if (iconFileName.compare("") != 0) {
		SDL_Surface* icon = SDL_LoadBMP(iconFileName.c_str());
		SDL_SetColorKey(icon, SDL_SRCCOLORKEY, SDL_MapRGB(icon->format, 0, 0, 0));
		SDL_WM_SetIcon(icon, nullptr);
	}
	this->iconFileName = iconFilename;*/
	(void)iconFileName; VERSO_FAIL("verso-gfx"); // \TODO: implement
}


bool Window2d::pollEvent(Event& event)
{
	/*if (m_impl && m_impl->popEvent(event, false)) { // \TODO: implement
		return filterEvent(event);
	}
	else {
		return false;
	}*/
	bool result = InputManager::instance().pollEvent(event);

	//if (event.type == EventType::GainedFocus)
	//else if (event.type == EventType::LostFocus)

	return result;
}


bool Window2d::waitEvent(Event& event)
{
	(void)event; VERSO_FAIL("verso-gfx"); // \TODO: Implement Window2d::waitEvent(Event& event)
	/*if (m_impl && m_impl->popEvent(event, true)) { // \TODO: implement
		return filterEvent(event);
	}
	else {
		return false;
	}*/
}


InputController* Window2d::getCameraInputController() const
{
	return cameraInputController;
}


ButtonHandle Window2d::getCameraForward() const
{
	return cameraForward;
}


ButtonHandle Window2d::getCameraBackward() const
{
	return cameraBackward;
}


ButtonHandle Window2d::getCameraLeft() const
{
	return cameraLeft;
}


ButtonHandle Window2d::getCameraRight() const
{
	return cameraRight;
}


ButtonHandle Window2d::getCameraUp() const
{
	return cameraUp;
}


ButtonHandle Window2d::getCameraDown() const
{
	return cameraDown;
}


ButtonHandle Window2d::getCameraForwardFast() const
{
	return cameraForwardFast;
}


ButtonHandle Window2d::getCameraBackwardFast() const
{
	return cameraBackwardFast;
}


ButtonHandle Window2d::getCameraLeftFast() const
{
	return cameraLeftFast;
}


ButtonHandle Window2d::getCameraRightFast() const
{
	return cameraRightFast;
}


ButtonHandle Window2d::getCameraUpFast() const
{
	return cameraUpFast;
}


ButtonHandle Window2d::getCameraDownFast() const
{
	return cameraDownFast;
}


ButtonHandle Window2d::getCameraForwardReallyFast() const
{
	return cameraForwardReallyFast;
}


ButtonHandle Window2d::getCameraBackwardReallyFast() const
{
	return cameraBackwardReallyFast;
}


ButtonHandle Window2d::getCameraLeftReallyFast() const
{
	return cameraLeftReallyFast;
}


ButtonHandle Window2d::getCameraRightReallyFast() const
{
	return cameraRightReallyFast;
}


ButtonHandle Window2d::getCameraUpReallyFast() const
{
	return cameraUpReallyFast;
}


ButtonHandle Window2d::getCameraDownReallyFast() const
{
	return cameraDownReallyFast;
}


ButtonHandle Window2d::getCameraActionA() const
{
	return cameraActionA;
}


ButtonHandle Window2d::getCameraActionB() const
{
	return cameraActionB;
}


void Window2d::onResize()
{
	displayMode.setResolution(getWindowResolutioni());
	updateRenderResolution();
}


UString Window2d::toString() const
{
	return "TODO";
}


UString Window2d::toStringDebug() const
{
	UString str("Window2d(");
	str += toString();
	str += ")";
	return str;
}


///////////////////////////////////////////////////////////////////////////////////////////
// static (public)
///////////////////////////////////////////////////////////////////////////////////////////

void Window2d::closeAll()
{
	while (windowsStillOpen.size() > 0) {
		if (windowsStillOpen[0] != nullptr) {
			windowsStillOpen[0]->close();
		}
	}
}


///////////////////////////////////////////////////////////////////////////////////////////
// static (protected)
///////////////////////////////////////////////////////////////////////////////////////////

int& Window2d::getUniqueInstanceId()
{
	static int uniqueInstanceId = 0;
	return uniqueInstanceId;
}


std::vector<Window2d*>& Window2d::getWindowsStillOpen()
{
	return windowsStillOpen;
}


///////////////////////////////////////////////////////////////////////////////////////////
// protected
///////////////////////////////////////////////////////////////////////////////////////////

void Window2d::onCreate()
{
}


///////////////////////////////////////////////////////////////////////////////////////////
// private
///////////////////////////////////////////////////////////////////////////////////////////

void Window2d::updateRenderResolution()
{
	Vector2i drawableResolution = getDrawableResolutioni();

	if (this->forcedRenderDisplayAspectRatio == true) {
		if (AspectRatio(drawableResolution, "").value > renderDisplayAspectRatio.value) {
			this->renderResolution.y = drawableResolution.y;
			this->renderResolution.x = static_cast<int>(static_cast<float>(drawableResolution.y) * renderDisplayAspectRatio.value);
		}
		else if (AspectRatio(drawableResolution, "").value < renderDisplayAspectRatio.value) {
			this->renderResolution.x = drawableResolution.x;
			this->renderResolution.y = static_cast<int>(static_cast<float>(drawableResolution.x) / renderDisplayAspectRatio.value);
		}
		else {
			this->renderResolution.x = drawableResolution.x;
			this->renderResolution.y = static_cast<int>(static_cast<float>(drawableResolution.x) / renderDisplayAspectRatio.value);
		}
	}
	else {
		this->renderResolution = drawableResolution;
		this->renderDisplayAspectRatio = AspectRatio(this->renderResolution, ""); // \TODO: fix aspect ratio if known
	}

	this->renderResolution = this->renderPixelAspectRatio.fitToResolution(this->renderResolution);

	// Downscale renderResolution if requested
	if (renderDownscaleRatio != RenderDownscaleRatio::Disabled()) {
		this->renderResolution /= renderDownscaleRatio.ratio;
	}
}


bool Window2d::filterEvent(const Event& event)
{
	(void)event;
	/*if (event.type == Event::Resized) { // \TODO: implement
		this->size = event.size;

		onResize();
	}*/

	return true;
}


} // End namespace Verso


