#include <Verso/Render2d/Render2d.hpp>
#include <SDL2/SDL.h>

namespace Verso {

inline SDL_Renderer* rendererHelper(const Window2d& window, const RgbaColorf& color)
{
	SDL_Renderer* renderer = reinterpret_cast<SDL_Renderer*>(window.getRenderer());
	SDL_SetRenderDrawColor(renderer,
	    static_cast<Uint8>(color.r * 255.0f),
	    static_cast<Uint8>(color.g * 255.0f),
	    static_cast<Uint8>(color.b * 255.0f),
	    static_cast<Uint8>(color.a * 255.0f));
	return renderer;
}


void Render2d::clear(const Window2d& window, const RgbaColorf& color)
{
	SDL_RenderClear(rendererHelper(window, color));
}


void Render2d::drawRectangle(const Window2d& window, const Recti& rect, const RgbaColorf& color)
{
	SDL_Rect sdlRect = { rect.pos.x, rect.pos.y, rect.size.x, rect.size.y };
	SDL_RenderDrawRect(rendererHelper(window, color), &sdlRect);
}


void Render2d::fillRectangle(const Window2d& window, const Recti& rect, const RgbaColorf& color)
{
	SDL_Rect sdlRect = { rect.pos.x, rect.pos.y, rect.size.x, rect.size.y };
	SDL_RenderFillRect(rendererHelper(window, color), &sdlRect);
}


} // End namespace Verso

