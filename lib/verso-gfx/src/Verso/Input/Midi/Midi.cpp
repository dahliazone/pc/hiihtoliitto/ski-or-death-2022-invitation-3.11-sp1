#include <Verso/Input/Midi/Midi.hpp>
#include <Verso/System/Logger.hpp>
#include <vector>

namespace Verso {


void mycallback(double deltatime, std::vector<unsigned char>* inMessage, void* userData)
{
	MidiDeviceType deviceType = *reinterpret_cast<MidiDeviceType*>(userData);
	std::uint8_t deviceTypeUint = static_cast<std::uint8_t>(deviceType);
	size_t nBytes = inMessage->size();
	std::ostringstream oss;
	oss << (Midi::getMidiDeviceTypesString()[deviceTypeUint]) << ": ";
	for (size_t i=0; i<nBytes; ++i) {
		oss << "Byte "<<std::hex<<i<<" = "<<static_cast<int>(inMessage->at(i))<<", ";
	}
	if (nBytes > 0) {
		oss << "stamp = "<<deltatime;
	}
	VERSO_LOG_DEBUG("verso-gfx", oss.str());

	// Control lights
	if (inMessage->at(0) == 0x80) {
		std::vector<unsigned char> outMessage(3);
		if (rand() % 2 == 0) {
			outMessage[0] = 0x90;
			outMessage[2] = 127;
		} else {
			outMessage[0] = 0x80;
			outMessage[2] = 0;
		}
		outMessage[1] = inMessage->at(1);
		Midi::getMidiOut()[deviceTypeUint]->sendMessage(&outMessage);
	}
	else if (inMessage->at(0) == 0xb0 && inMessage->at(2) == 127) {
		std::vector<unsigned char> outMessage(3);
		if (rand() % 2 == 0) {
			outMessage[0] = inMessage->at(0);
			outMessage[2] = 127;
		} else {
			outMessage[0] = inMessage->at(0);
			outMessage[2] = 0;
		}
		outMessage[1] = inMessage->at(1);
		Midi::getMidiOut()[deviceTypeUint]->sendMessage(&outMessage);
	}
}


Midi::Midi()
{
	getMidiIn().clear();
	getMidiIn().assign(static_cast<size_t>(MidiDeviceType::Size), nullptr);

	getMidiOut().clear();
	getMidiOut().assign(static_cast<size_t>(MidiDeviceType::Size), nullptr);

	getMidiDeviceTypesEnum().clear();
	getMidiDeviceTypesEnum().assign(static_cast<size_t>(MidiDeviceType::Size), static_cast<std::uint8_t>(MidiDeviceType::Unset));
	getMidiDeviceTypesEnum()[static_cast<std::uint8_t>(MidiDeviceType::Lpd8)] = static_cast<std::uint8_t>(MidiDeviceType::Lpd8);
	getMidiDeviceTypesEnum()[static_cast<std::uint8_t>(MidiDeviceType::NanoKontrol2)] = static_cast<std::uint8_t>(MidiDeviceType::NanoKontrol2);
	getMidiDeviceTypesEnum()[static_cast<std::uint8_t>(MidiDeviceType::DJ2Go)] = static_cast<std::uint8_t>(MidiDeviceType::DJ2Go);
	getMidiDeviceTypesEnum()[static_cast<std::uint8_t>(MidiDeviceType::QinHengElectronicsCh345MidiAdapter)] = static_cast<std::uint8_t>(MidiDeviceType::QinHengElectronicsCh345MidiAdapter);

	getMidiDeviceTypesString().clear();
	getMidiDeviceTypesString().assign(static_cast<size_t>(MidiDeviceType::Size), "");
	getMidiDeviceTypesString()[static_cast<std::uint8_t>(MidiDeviceType::Lpd8)] = "LPD8";
	getMidiDeviceTypesString()[static_cast<std::uint8_t>(MidiDeviceType::NanoKontrol2)] = "nanoKONTROL2";
	getMidiDeviceTypesString()[static_cast<std::uint8_t>(MidiDeviceType::DJ2Go)] = "Numark DJ2Go";
	getMidiDeviceTypesString()[static_cast<std::uint8_t>(MidiDeviceType::QinHengElectronicsCh345MidiAdapter)] = "QinHeng Electronics CH345 MIDI adapter";
}


Midi::Midi(Midi&& original) noexcept
{
	(void)original;
	// no state
}


Midi& Midi::operator =(Midi&& original) noexcept
{
	if (this != &original) {
		// no state
	}
	return *this;
}


Midi::~Midi()
{
	destroy();
}


void Midi::printMidiDevices() const
{
	RtMidiIn* rtMidiIn = nullptr;
	RtMidiOut* rtMidiOut = nullptr;
	try {
		rtMidiIn = new RtMidiIn();

		std::ostringstream oss;
		oss << "Midi input devices found: ";
		size_t portCount = rtMidiIn->getPortCount();
		for (size_t i=0; i<portCount; ++i) {
			oss << "\""<<rtMidiIn->getPortName(static_cast<unsigned int>(i))<<"\"";
			if (i != portCount-1)
				oss << ", ";
		}
		VERSO_LOG_INFO("verso-gfx", oss.str());

		rtMidiOut = new RtMidiOut();

		oss.str("");
		oss.clear();
		oss << "Midi output devices found: ";
		portCount = rtMidiOut->getPortCount();
		for (size_t i=0; i<portCount; ++i) {
			oss << "\""<<rtMidiOut->getPortName(static_cast<unsigned int>(i))<<"\"";
			if (i != portCount-1) {
				oss << ", ";
			}
		}
		VERSO_LOG_INFO("verso-gfx", oss.str());
	}
	catch (RtMidiError& error) {
		error.printMessage();
	}

	if (rtMidiIn != nullptr) {
		delete rtMidiIn;
	}
	if (rtMidiOut != nullptr) {
		delete rtMidiOut;
	}
}


bool Midi::createMidi(MidiDeviceType deviceType)
{
	try {
		std::uint8_t deviceTypeUint = static_cast<std::uint8_t>(deviceType);
		auto& midiIn = getMidiIn();

		if (midiIn[deviceTypeUint] != nullptr) {
			delete midiIn[deviceTypeUint];
		}
		midiIn[deviceTypeUint] = new RtMidiIn();

		bool foundInput = false;
		size_t portCount = midiIn[deviceTypeUint]->getPortCount();
		for (size_t i = 0; i < portCount; ++i) {
			UString portName = midiIn[deviceTypeUint]->getPortName(static_cast<unsigned int>(i));
			VERSO_LOG_DEBUG("verso-gfx", "Comparing \""<<portName<<"\" against \""<<midiDeviceTypeToString(deviceType)<<"\"...");
			if (portName.beginsWith(getMidiDeviceTypesString()[deviceTypeUint])) {
				VERSO_LOG_INFO("verso-gfx", "Opening input port #" << i << ": " << portName);
				midiIn[deviceTypeUint]->openPort(static_cast<unsigned int>(i));
				foundInput = true;
				break;
			}
		}
		if (foundInput == false) {
			if (midiIn[deviceTypeUint] != nullptr) {
				delete midiIn[deviceTypeUint];
				midiIn[deviceTypeUint] = nullptr;
			}
			return false;
		}

		// set our callback function.  This should be done immediately after
		// opening the port to avoid having incoming messages written to the
		// queue instead of sent to the callback function.
		//midiIn[deviceTypeUint]->setCallback(&mycallback, &getMidiDeviceTypesEnum()[deviceTypeUint]);

		// Don't ignore sysex, timing, or active sensing messages.
		midiIn[deviceTypeUint]->ignoreTypes(false, false, false);

		auto& midiOut = getMidiOut();
		if (midiOut[deviceTypeUint] != nullptr) {
			delete midiOut[deviceTypeUint];
		}
		midiOut[deviceTypeUint] = new RtMidiOut();
		bool foundOutput = false;
		portCount = midiOut[deviceTypeUint]->getPortCount();
		for (size_t i = 0; i < portCount; ++i) {
			UString portName = midiOut[deviceTypeUint]->getPortName(static_cast<unsigned int>(i));
			VERSO_LOG_DEBUG("verso-gfx", "Comparing \""<<portName<<"\" against \""<<midiDeviceTypeToString(deviceType)<<"\"...");
			if (portName.beginsWith(getMidiDeviceTypesString()[deviceTypeUint])) {
				VERSO_LOG_INFO("verso-gfx", "Opening input port #" << i << ": " << portName);
				bool TODO_midi_out_disabled_because_of_MSVC_bug;
				//midiOut[deviceTypeUint]->openPort(static_cast<unsigned int>(i));
				foundOutput = true;
				break;
			}
		 }

		 if (foundOutput) {
			 return true;
		 }
		 else {
			 return false;
		 }
	}
	catch (RtMidiError& error) {
		error.printMessage();
	}

	return false;
}


void Midi::createAll()
{
	for (std::uint8_t deviceTypeInt : getMidiDeviceTypesEnum()) {
		MidiDeviceType deviceType = static_cast<MidiDeviceType>(deviceTypeInt);
		createMidi(deviceType);
	}
}


void Midi::destroy()
{
	for (size_t i=0; i<getMidiIn().size(); ++i) {
		if (getMidiIn()[i] != nullptr) {
			delete getMidiIn()[i];
			getMidiIn()[i] = nullptr;
		}
	}

	for (size_t i=0; i<getMidiOut().size(); ++i) {
		if (getMidiOut()[i] != nullptr) {
			delete getMidiOut()[i];
			getMidiOut()[i] = nullptr;
		}
	}
}


std::vector<std::uint8_t> Midi::getInitializedInputDevicesEnum() const
{
	std::vector<std::uint8_t> enums;
	for (size_t i=0; i<getMidiIn().size(); ++i) {
		if (getMidiIn()[i] != nullptr) {
			enums.push_back(getMidiDeviceTypesEnum()[i]);
		}
	}
	return enums;
}


std::vector<std::uint8_t> Midi::getInitializedOutputDevicesEnum() const
{
	std::vector<std::uint8_t> enums;
	for (size_t i=0; i<getMidiOut().size(); ++i) {
		if (getMidiOut()[i] != nullptr) {
			enums.push_back(getMidiDeviceTypesEnum()[i]);
		}
	}
	return enums;
}


std::vector<UString> Midi::getInitializedInputDevicesString() const
{
	std::vector<std::uint8_t> devices = getInitializedInputDevicesEnum();
	std::vector<UString> strings;
	for (std::uint8_t deviceTypeInt : devices) {
		MidiDeviceType deviceType = static_cast<MidiDeviceType>(deviceTypeInt);
		strings.push_back(getMidiDeviceTypesString()[static_cast<std::uint8_t>(deviceType)]);
	}
	return strings;
}


std::vector<UString> Midi::getInitializedOutputDevicesString() const
{
	std::vector<std::uint8_t> devices = getInitializedOutputDevicesEnum();
	std::vector<UString> strings;
	for (std::uint8_t deviceTypeInt : devices) {
		MidiDeviceType deviceType = static_cast<MidiDeviceType>(deviceTypeInt);
		strings.push_back(getMidiDeviceTypesString()[static_cast<std::uint8_t>(deviceType)]);
	}
	return strings;
}


void Midi::printInitializedInputDevices() const
{
	std::vector<UString> devices = getInitializedInputDevicesString();
	std::ostringstream oss;
	oss << "Midi input devices created: ";
	if (devices.size() == 0) {
		oss << "none";
	}
	else {
		for (size_t i=0; i<devices.size(); ++i) {
			oss << devices[i];
			if (i != devices.size()-1) {
				oss << ", ";
			}
		}
	}
	VERSO_LOG_INFO("verso-gfx", oss.str());
}


void Midi::printInitializedOutputDevices() const
{
	std::vector<UString> devices = getInitializedOutputDevicesString();
	std::ostringstream oss;
	oss << "Midi output devices created: ";
	if (devices.size() == 0) {
		oss << "none";
	}
	else {
		for (size_t i=0; i<devices.size(); ++i) {
			oss << devices[i];
			if (i != devices.size()-1) {
				oss << ", ";
			}
		}
	}
	VERSO_LOG_INFO("verso-gfx", oss.str());
}


// static

std::vector<std::uint8_t>& Midi::getMidiDeviceTypesEnum()
{
	static std::vector<std::uint8_t> midiDeviceTypesEnum(static_cast<size_t>(MidiDeviceType::Size), static_cast<std::uint8_t>(MidiDeviceType::Unset));
	return midiDeviceTypesEnum;
}


std::vector<UString>& Midi::getMidiDeviceTypesString()
{
	static std::vector<UString> midiDeviceTypesString(static_cast<size_t>(MidiDeviceType::Size), "");
	return midiDeviceTypesString;
}


std::vector<RtMidiIn*>& Midi::getMidiIn()
{
	static std::vector<RtMidiIn*> midiIn(static_cast<size_t>(MidiDeviceType::Size), nullptr);
	return midiIn;
}


std::vector<RtMidiOut*>& Midi::getMidiOut()
{
	static std::vector<RtMidiOut*> midiOut(static_cast<size_t>(MidiDeviceType::Size), nullptr);
	return midiOut;
}


} // End namespace Verso

