#ifdef _WIN32
#include <Verso/System/win32/consoleRedirectWin32.hpp>

#include <iostream>
#include <cstdio>
#include <io.h>
#include <fcntl.h>
#include <windows.h>

namespace Verso {
namespace priv {


ULONG_PTR getParentProcessIdWin32()
{
	ULONG_PTR pbi[6];
	ULONG ulSize = 0;
	LONG (WINAPI *NtQueryInformationProcess)(HANDLE ProcessHandle, ULONG ProcessInformationClass,
			PVOID ProcessInformation, ULONG ProcessInformationLength, PULONG ReturnLength);
	auto lib = LoadLibraryA("NTDLL.DLL");
	if (lib == 0) {
		return ATTACH_PARENT_PROCESS;
	}
	*(FARPROC*)&NtQueryInformationProcess = GetProcAddress(lib, "NtQueryInformationProcess");

	if (NtQueryInformationProcess) {
		if (NtQueryInformationProcess(GetCurrentProcess(), 0,
			&pbi, sizeof(pbi), &ulSize) >= 0 && ulSize == sizeof(pbi))
		{
			return pbi[5];
		}
	}

	return ATTACH_PARENT_PROCESS;
}


void redirectToExistingConsoleWin32()
{
	DWORD ppid = static_cast<DWORD>(getParentProcessIdWin32());
	if (ppid != ATTACH_PARENT_PROCESS) {
		if (AttachConsole(ppid) != 0) {
			(void)freopen("CONIN$", "r", stdin);
			(void)freopen("CONOUT$", "w", stdout);
			(void)freopen("CONOUT$", "w", stderr);
			std::ios::sync_with_stdio(true);
		}
	}
}


} // End namespace priv
} // End namespace Verso


#endif // End _WIN32
