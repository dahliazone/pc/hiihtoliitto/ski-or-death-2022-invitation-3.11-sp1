#include <Verso/System/SubsystemSdl2.hpp>
#include <SDL2/SDL.h>

namespace Verso {


std::uint32_t subsystemSdl2ToSdl2Type(const SubsystemSdl2& subsystemSdl2)
{
	switch (subsystemSdl2)
	{
	case SubsystemSdl2::None:
		return 0;
	case SubsystemSdl2::Audio:
		return SDL_INIT_AUDIO;
	case SubsystemSdl2::Events:
		return SDL_INIT_EVENTS;
	case SubsystemSdl2::Everything:
		return SDL_INIT_EVERYTHING;
	case SubsystemSdl2::GameController:
		return SDL_INIT_GAMECONTROLLER;
	case SubsystemSdl2::Haptic:
		return SDL_INIT_HAPTIC;
	case SubsystemSdl2::Joystick:
		return SDL_INIT_JOYSTICK;
	case SubsystemSdl2::NoParachute:
		return SDL_INIT_NOPARACHUTE;
	case SubsystemSdl2::Timer:
		return SDL_INIT_TIMER;
	case SubsystemSdl2::Video:
		return SDL_INIT_VIDEO;
	default:
		VERSO_LOG_WARN("verso-gfx", "Unknown SubsystemSdl2 enum given '"<<static_cast<std::uint32_t>(subsystemSdl2)<<"'!");
		return 0;
	}
}


} // End namespace Verso

