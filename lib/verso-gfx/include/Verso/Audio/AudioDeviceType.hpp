#ifndef VERSO_GFX_AUDIO_AUDIODEVICETYPE_HPP
#define VERSO_GFX_AUDIO_AUDIODEVICETYPE_HPP

#include <Verso/System/UString.hpp>
#include <cstdint>

namespace Verso {


enum class AudioDeviceType : int8_t
{
	Digital = 0,     /*!< An audio endpoint device that connects to an audio adapter through a connector for a digital interface of unknown type. */
	DisplayPort = 1, /*!< An audio endpoint device that connects to an audio adapter through a DisplayPort connector. */
	Handset = 2,     /*!< The part of a telephone that is held in the hand and that contains a speaker and a microphone for two-way communication. */
	Hdmi = 3,        /*!< An audio endpoint device that connects to an audio adapter through a High-Definition Multimedia Interface (HDMI) connector. */
	Headphones = 4,  /*!< A set of headphones. */
	Headset = 5,     /*!< An earphone or a pair of earphones with an attached mouthpiece for two-way communication. */
	Line = 6,        /*!< An audio endpoint device that sends a line-level analog signal to a line-input jack on an audio adapter or that receives a line-level analog signal from a line-output jack on the adapter. */
	Microphone = 7,  /*!< A microphone. */
	Network = 8,     /*!< An audio endpoint device that the user accesses remotely through a network. */
	Spdif = 9,       /*!< An audio endpoint device that connects to an audio adapter through a Sony/Philips Digital Interface (S/PDIF) connector. */
	Speakers = 10,   /*!< A set of speakers. */
	Unset = -1
};


class AudioDeviceTypeIo
{
public:
	bool output;
	bool input;

public:
	AudioDeviceTypeIo(bool output, bool input) :
		output(output),
		input(input)
	{
	}

public: // toString
	UString toString() const
	{
		UString str("output=");
		if (output == true) {
			str += "true";
		}
		else {
			str += "false";
		}

		str += ", input=";
		if (input == true) {
			str += "true";
		}
		else {
			str += "false";
		}
		return str;
	}

	UString toStringDebug() const
	{
		UString str("AudioDeviceTypeIo(");
		str += toString();
		str += ")";
		return str;
	}
};


inline UString audioDeviceTypeToString(const AudioDeviceType& type)
{
	if (type ==  AudioDeviceType::Digital) {
		return "Digital";
	}
	else if (type == AudioDeviceType::DisplayPort) {
		return "DisplayPort";
	}
	else if (type == AudioDeviceType::Handset) {
		return "Handset";
	}
	else if (type == AudioDeviceType::Hdmi) {
		return "HDMI";
	}
	else if (type == AudioDeviceType::Headphones) {
		return "Headphones";
	}
	else if (type == AudioDeviceType::Headset) {
		return "Headset";
	}
	else if (type == AudioDeviceType::Line) {
		return "Line";
	}
	else if (type == AudioDeviceType::Microphone) {
		return "Microphone";
	}
	else if (type == AudioDeviceType::Network) {
		return "Network";
	}
	else if (type == AudioDeviceType::Spdif) {
		return "S/PDIF";
	}
	else if (type == AudioDeviceType::Speakers) {
		return "Speakers";
	}
	else if (type == AudioDeviceType::Unset) {
		return "Unset";
	}
	else {
		return "Unknown value";
	}
}


inline AudioDeviceTypeIo audioDeviceTypeGetIo(const AudioDeviceType& type)
{
	if (type ==  AudioDeviceType::Digital) {
		return AudioDeviceTypeIo(true, false);
	}
	else if (type == AudioDeviceType::DisplayPort) {
		return AudioDeviceTypeIo(true, false);
	}
	else if (type == AudioDeviceType::Handset) {
		return AudioDeviceTypeIo(true, true);
	}
	else if (type == AudioDeviceType::Hdmi) {
		return AudioDeviceTypeIo(true, false);
	}
	else if (type == AudioDeviceType::Headphones) {
		return AudioDeviceTypeIo(true, false);
	}
	else if (type == AudioDeviceType::Headset) {
		return AudioDeviceTypeIo(true, true);
	}
	else if (type == AudioDeviceType::Line) {
		return AudioDeviceTypeIo(true, false);
	}
	else if (type == AudioDeviceType::Microphone) {
		return AudioDeviceTypeIo(false, true);
	}
	else if (type == AudioDeviceType::Network) {
		return AudioDeviceTypeIo(true, false);
	}
	else if (type == AudioDeviceType::Spdif) {
		return AudioDeviceTypeIo(true, false);
	}
	else if (type == AudioDeviceType::Speakers) {
		return AudioDeviceTypeIo(true, false);
	}
	else if (type == AudioDeviceType::Unset) {
		return AudioDeviceTypeIo(false, false);
	}
	else {
		return AudioDeviceTypeIo(false, false);
	}
}


inline UString audioDeviceTypeDescriptionToString(const AudioDeviceType& type)
{
	if (type ==  AudioDeviceType::Digital) {
		return "An audio endpoint device that connects to an audio adapter through a connector for a digital interface of unknown type.";
	}
	else if (type == AudioDeviceType::DisplayPort) {
		return "An audio endpoint device that connects to an audio adapter through a DisplayPort connector.";
	}
	else if (type == AudioDeviceType::Handset) {
		return "The part of a telephone that is held in the hand and that contains a speaker and a microphone for two-way communication.";
	}
	else if (type == AudioDeviceType::Hdmi) {
		return "An audio endpoint device that connects to an audio adapter through a High-Definition Multimedia Interface (HDMI) connector.";
	}
	else if (type == AudioDeviceType::Headphones) {
		return "A set of headphones.";
	}
	else if (type == AudioDeviceType::Headset) {
		return "An earphone or a pair of earphones with an attached mouthpiece for two-way communication.";
	}
	else if (type == AudioDeviceType::Line) {
		return "An audio endpoint device that sends a line-level analog signal to a line-input jack on an audio adapter or that receives a line-level analog signal from a line-output jack on the adapter.";
	}
	else if (type == AudioDeviceType::Microphone) {
		return "A microphone.";
	}
	else if (type == AudioDeviceType::Network) {
		return "An audio endpoint device that the user accesses remotely through a network.";
	}
	else if (type == AudioDeviceType::Spdif) {
		return "An audio endpoint device that connects to an audio adapter through a Sony/Philips Digital Interface (S/PDIF) connector.";
	}
	else if (type == AudioDeviceType::Speakers) {
		return "A set of speakers.";
	}
	else if (type == AudioDeviceType::Unset) {
		return "None";
	}
	else {
		return "Unknown value";
	}
}


} // End namespace Verso

#endif // End header guard

