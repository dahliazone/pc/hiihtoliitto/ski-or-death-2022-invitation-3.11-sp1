#ifndef VERSO_GFX_AUDIO_AUDIODEVICE_HPP
#define VERSO_GFX_AUDIO_AUDIODEVICE_HPP

#include <Verso/Audio/AudioDeviceType.hpp>

namespace Verso {


class AudioDevice
{
public:
	int deviceId;
	UString name;
	AudioDeviceType type;
	bool initialized;
	bool defaultDevice;
	bool output;
	bool input;
	bool loopback;
	UString driverFileName;

public:
	AudioDevice(int deviceId, const AudioDeviceType& type, const UString& name,
				bool initialized = false, bool defaultDevice = false,
				bool output = true, bool input = false, bool loopback = false,
				const UString& driverFileName = "") :
		deviceId(deviceId),
		name(name),
		type(type),
		initialized(initialized),
		defaultDevice(defaultDevice),
		output(output),
		input(input),
		loopback(loopback),
		driverFileName(driverFileName)
	{
	}

public: // toString
	UString toString() const
	{
		return name;
	}

	UString toStringFull() const
	{
		UString str("deviceId=");
		str.append2(deviceId);

		str += ", name=\"";
		str.append2(name);
		str += "\"";

		str += ", type=";
		str.append2(audioDeviceTypeToString(type));

		if (!driverFileName.isEmpty()) {
			str += ", driverFileName=\"";
			str += driverFileName;
			str += "\"";
		}

		str += ", initialized=";
		if (initialized == true) {
			str += "true";
		}
		else {
			str += "false";
		}

		str += ", defaultDevice=";
		if (defaultDevice == true) {
			str += "true";
		}
		else {
			str += "false";
		}

		str += ", output=";
		if (output == true) {
			str += "true";
		}
		else {
			str += "false";
		}

		str += ", input=";
		if (input == true) {
			str += "true";
		}
		else {
			str += "false";
		}

		str += ", loopback=";
		if (loopback == true) {
			str += "true";
		}
		else {
			str += "false";
		}

		return str;
	}

	UString toStringDebug() const
	{
		UString str("AudioDevice(");
		str += toString();
		str += ")";
		return str;
	}
};


} // End namespace Verso

#endif // End header guard

