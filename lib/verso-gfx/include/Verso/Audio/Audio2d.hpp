#ifndef VERSO_GFX_AUDIO_AUDIO2D_HPP
#define VERSO_GFX_AUDIO_AUDIO2D_HPP

#include <Verso/verso-gfx-common.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/Audio/AudioDevice.hpp>

namespace Verso {


/**
 * Handles all the necessary stuff to play music and sounds (not implemented).
 */
class Audio2d
{
public:
	virtual ~Audio2d() = default;

	virtual std::vector<AudioDevice> getDevices() = 0;

	/**
	 * @param mixingChannels How many channels to mix (must be at least 8).
	 */
	virtual void create(
		bool noSound=false,
		int deviceId = -1, // -1 = default device, 1 = first real output device.
		unsigned int rate=44100,
		bool stereo=true) = 0;

	/**
	 * Call this only if you wan't clean fadeout of music. Destructor
	 * will handle deleting of all resources.
	 */
	virtual void destroy() VERSO_NOEXCEPT = 0;

	/**
	 * Handles the fading between two songs.
	 * Not needed if changeMusicFadeOutIn() is never used.
	 * If needed then must be called ideally every frame.
	 */
	virtual void update() = 0;

	virtual void loadMusic(const UString& name, const UString& fileName) = 0;

	virtual void deleteMusic(const UString& name) = 0;

	/**
	 * @param loopCount How many times the music is looped (-1 for infite).
	 */
	virtual void playMusic(const UString& name, bool loop = false) = 0;

	virtual double getPositionSeconds(const UString& name) = 0;
	virtual double getDurationSeconds(const UString& name) = 0;
	virtual void rewind(const UString& name, double seconds) = 0;

	/**
	 * @param loopCount How many times the music is looped (-1 for infite).
	 */
	virtual void fadeInMusic(const UString& name, int loopCount=0, float fadeTime=1.0f) = 0;

	/**
	 * @param loopCount How many times the music is looped (-1 for infite).
	 */
	virtual void changeMusicFadeOutIn(const UString& newMusic, int loopCount=0, float fadeTime=2.0f) = 0;

	virtual void startMusic() = 0;

	virtual void pauseMusic() = 0;

	virtual void stopMusic() = 0;

	virtual void fadeOutMusic(float fadeTime = 1.0f) = 0;

	virtual int setMusicVolume(int volume) = 0;

	virtual void loadSfx(const UString& name, const UString& fileName) = 0;
	virtual void deleteSfx(const UString& name) = 0;

	/**
	 * @return Channel number which is used to play the sfx in.
	 */
	virtual int playSfx(const UString& name, int channel=-1, int loops=0) = 0;
	virtual int setChannelVolume(int channel, int volume) = 0;
	virtual void haltChannel(int channel) = 0;

	//virtual void registerEffect(Mix_EffectFunc_t processor, Mix_EffectDone_t cleanUp, void* arg) = 0;

	virtual AudioDevice getDevice() const = 0;
};


} // End namespace Verso

#endif // End header guard

