#ifndef VERSO_GFX_VERSO_GFX_COMMON_HPP
#define VERSO_GFX_VERSO_GFX_COMMON_HPP


#if defined(WIN32) || defined _WIN32 || defined __CYGWIN__
#	if defined(VERSO_GFX_BUILD_DYNAMIC)
#		ifdef __GNUC__
#			define VERSO_GFX_API __attribute__ ((dllexport))
#		else
#			define VERSO_GFX_API __declspec(dllexport) // Note: actually gcc seems to also supports this syntax.
#			define VERSO_GFX_EXPIMP
#		endif
#	else
#		ifdef __GNUC__
#			define VERSO_GFX_API __attribute__ ((dllimport))
#		else
#			define VERSO_GFX_API __declspec(dllimport) // Note: actually gcc seems to also supports this syntax.
#			define VERSO_GFX_EXPIMP extern
#		endif
#	endif
#	define VERSO_GFX_PRIVATE
#else
#	if __GNUC__ >= 4
#		define VERSO_GFX_API __attribute__ ((visibility ("default")))
#		define VERSO_GFX_PRIVATE  __attribute__ ((visibility ("hidden")))
#	else
#		define VERSO_GFX_API
#		define VERSO_GFX_PRIVATE
#	endif
#endif


#endif // End header guard

