#ifndef VERSO_GFX_DISPLAY_PIXELFORMAT_HPP
#define VERSO_GFX_DISPLAY_PIXELFORMAT_HPP

#include <Verso/verso-gfx-common.hpp>
#include <Verso/System/UString.hpp>

namespace Verso {


class PixelFormat
{
public:
	enum PixelFormatEnum {
		Unknown = 0,
		Index1Lsb,
		Index1Msb,
		Index4Lsb,
		Index4Msb,
		Index8,
		Rgb332,
		Rgb444,
		Rgb555,
		Bgr555,
		Argb4444,
		Rgba4444,
		Abgr4444,
		Bgra4444,
		Argb1555,
		Rgba5551,
		Abrr1555,
		Bgra5551,
		Rgb565,
		Bgr565,
		Rgb24,
		Bgr24,
		Rgb888,
		Rgbx8888,
		Bgr888,
		Bgrx8888,
		Argb8888,
		Rgba8888,
		Abgr8888,
		Bgra8888,
		Argb2101010,
		Yv12, // planar mode: Y + V + U (3 planes)
		Iyuv, // planar mode: Y + U + V (3 planes)
		Yuy2, // packed mode: Y0+U0+Y1+V0 (1 plane)
		Uyvy, // packed mode: U0+Y0+V0+Y1 (1 plane)
		Yvyu, // packed mode: Y0+V0+Y1+U0 (1 plane)
	};

	PixelFormatEnum pixelFormatEnum;

public:
	VERSO_GFX_API PixelFormat();

	VERSO_GFX_API explicit PixelFormat(PixelFormatEnum pixelFormatEnum);

	VERSO_GFX_API static PixelFormat sdlPixelFormatToPixelFormat(std::uint32_t sdlPixelFormat);

public: // toString
	VERSO_GFX_API UString toString() const;

	VERSO_GFX_API UString toStringDebug() const;


	friend std::ostream& operator <<(std::ostream& ost, const PixelFormat& right)
	{
		return ost << right.toString();
	}
};


inline bool operator ==(const PixelFormat& left, const PixelFormat& right)
{
	return left.pixelFormatEnum == right.pixelFormatEnum;
}


inline bool operator !=(const PixelFormat& left, const PixelFormat& right)
{
	return !(left == right);
}


inline bool operator ==(const PixelFormat& left, PixelFormat::PixelFormatEnum right)
{
	return left.pixelFormatEnum == right;
}


inline bool operator !=(const PixelFormat& left, PixelFormat::PixelFormatEnum right)
{
	return !(left == right);
}


} // End namespace Verso

#endif // End header guard

