#ifndef VERSO_GFX_DISPLAY_SWAPINTERVAL_HPP
#define VERSO_GFX_DISPLAY_SWAPINTERVAL_HPP

#include <Verso/System/UString.hpp>
#include <cstdint>

namespace Verso {


enum class SwapInterval : int8_t {
	Immediate = 0,
	Vsync = 1,
	AdaptiveVsync = -1,
	Unset = -2
};


inline UString swapIntervalToString(const SwapInterval& swapInterval)
{
	if (swapInterval == SwapInterval::Immediate) {
		return "Immediate";
	}
	else if (swapInterval == SwapInterval::Vsync) {
		return "Vsync";
	}
	else if (swapInterval == SwapInterval::AdaptiveVsync) {
		return "AdaptiveVsync";
	}
	else if (swapInterval == SwapInterval::Unset) {
		return "Unset";
	}
	else {
		return "Unknown value";
	}
}



} // End namespace Verso

#endif // End header guard

