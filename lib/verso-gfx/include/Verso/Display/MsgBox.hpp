#ifndef VERSO_GFX_DISPLAY_MSGBOX_HPP
#define VERSO_GFX_DISPLAY_MSGBOX_HPP

#include <Verso/verso-gfx-common.hpp>
#include <Verso/Display/MsgBox/MsgBoxType.hpp>

namespace Verso {


class MsgBox
{
public:
	// Note that only first 32 rows of the message will be shown correctly in linux at least
	VERSO_GFX_API static bool run(MsgBoxType type, const UString& title, const UString& message);
};


} // End namespace Verso

#endif // End header guard

