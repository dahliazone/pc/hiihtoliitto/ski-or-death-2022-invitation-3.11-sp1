#ifndef VERSO_GFX_DISPLAY_REFRESHRATE_HPP
#define VERSO_GFX_DISPLAY_REFRESHRATE_HPP

#include <Verso/verso-gfx-common.hpp>
#include <Verso/System/UString.hpp>

namespace Verso {


class RefreshRate
{
public:
	int hz;

public:
	RefreshRate();
	explicit RefreshRate(int hz);
	~RefreshRate();

public: // static
	static RefreshRate* findIdenticalFromList(const RefreshRate& selected, std::vector<RefreshRate>& refreshRates);

public: // toString
	UString toString() const;

	UString toStringDebug() const;


	friend std::ostream& operator <<(std::ostream& ost, const RefreshRate& right)
	{
		return ost << right.toString();
	}
};


// External operators
inline bool operator ==(const RefreshRate& left, const RefreshRate& right)
{
	return (left.hz == right.hz);
}


inline bool operator !=(const RefreshRate& left, const RefreshRate& right)
{
	return !(left == right);
}


inline bool operator <(const RefreshRate& left, const RefreshRate& right)
{
	return left.hz < right.hz;
}


inline bool operator >(const RefreshRate& left, const RefreshRate& right)
{
	return right < left;
}


inline bool operator <=(const RefreshRate& left, const RefreshRate& right)
{
	return !(right < left);
}


inline bool operator >=(const RefreshRate& left, const RefreshRate& right)
{
	return !(left < right);
}


// Constructors

inline RefreshRate::RefreshRate() :
	hz(0)
{
}


inline RefreshRate::RefreshRate(int hz) :
	hz(hz)
{
}


inline RefreshRate::~RefreshRate()
{
}


// static
inline RefreshRate* RefreshRate::findIdenticalFromList(const RefreshRate& selected, std::vector<RefreshRate>& refreshRates)
{
	for (size_t i=0; i<refreshRates.size(); ++i) {
		if (refreshRates[i] == selected) {
			return &refreshRates[i];
		}
	}
	return nullptr;
}


// Non-static toString(s)

inline UString RefreshRate::toString() const
{
	UString str("");
	str.append2(hz);
	str += " hz";
	return str;
}


inline UString RefreshRate::toStringDebug() const
{
	UString str("RefreshRate(");
	str += toString();
	str += ")";
	return str;
}


} // End namespace Verso

#endif // End header guard

