#ifndef VERSO_GFX_DISPLAY_IWINDOW_HPP
#define VERSO_GFX_DISPLAY_IWINDOW_HPP

#include <Verso/Math/Align.hpp>
#include <Verso/Math/PixelAspectRatio.hpp>
#include <Verso/Display/Display.hpp>
#include <Verso/Display/DisplayMode.hpp>
#include <Verso/Display/RenderDownscaleRatio.hpp>
#include <Verso/Display/SystemCursorType.hpp>
#include <Verso/Input/Event.hpp>
#include <Verso/Input/Mouse/MouseState.hpp>
#include <Verso/Input/InputController.hpp>

namespace Verso {


class IWindow
{
public:
	VERSO_GFX_API virtual ~IWindow() = default;

	VERSO_GFX_API virtual void destroy() VERSO_NOEXCEPT = 0;

	VERSO_GFX_API virtual bool isCreated() const = 0;

	VERSO_GFX_API virtual void close() VERSO_NOEXCEPT = 0;

	VERSO_GFX_API virtual bool isOpen() const = 0;

	VERSO_GFX_API virtual UString getId() const = 0;

	VERSO_GFX_API virtual Vector2i getWindowResolutioni() const = 0;

	VERSO_GFX_API virtual Vector2f getWindowResolutionf() const = 0;

	VERSO_GFX_API virtual void setWindowResolution(const Vector2i& windowResolution) = 0;

	VERSO_GFX_API virtual RenderDownscaleRatio getRenderDownscaleRatio() const = 0;

	VERSO_GFX_API virtual Vector2i getRenderResolutioni() const = 0;

	VERSO_GFX_API virtual Vector2f getRenderResolutionf() const = 0;

	VERSO_GFX_API virtual Vector2i getDrawableResolutioni() const = 0;

	VERSO_GFX_API virtual Vector2f getDrawableResolutionf() const = 0;

	VERSO_GFX_API virtual AspectRatio getRenderDisplayAspectRatio() const = 0;

	VERSO_GFX_API virtual void setRenderDisplayAspectRatio(const AspectRatio& renderDisplayAspectRatio) = 0;

	VERSO_GFX_API virtual PixelAspectRatio getRenderPixelAspectRatio() const = 0;

	VERSO_GFX_API virtual void setRenderPixelAspectRatio(const PixelAspectRatio& renderPixelAspectRatio) = 0;

	VERSO_GFX_API virtual void makeActive() = 0;

	VERSO_GFX_API virtual void swapScreenBuffer() const = 0;

	VERSO_GFX_API virtual const UString& getClipboardText() = 0;

	VERSO_GFX_API virtual void setClipboardText(const UString& text) = 0;

	VERSO_GFX_API virtual bool isCursorShown() const = 0;

	VERSO_GFX_API virtual void setCursorShown(bool shown) const = 0;

	VERSO_GFX_API virtual void showCursor() const = 0;

	VERSO_GFX_API virtual void hideCursor() const = 0;

	VERSO_GFX_API virtual bool hasInputGrabbed() const = 0;

	VERSO_GFX_API virtual bool hasInputFocus() const = 0;

	VERSO_GFX_API virtual bool hasMouseFocus() const = 0;

	VERSO_GFX_API virtual MouseState getMouseState() const = 0;

	VERSO_GFX_API virtual bool captureMouse(bool enabled) const = 0;

	VERSO_GFX_API virtual bool warpMouseGlobal(int x, int y) const = 0;

	VERSO_GFX_API virtual void warpMouseInWindow(int x, int y) const = 0;

	VERSO_GFX_API virtual bool createSystemCursor(SystemCursorType systemCursorType) = 0;

	VERSO_GFX_API virtual void destroySystemCursor(SystemCursorType systemCursorType) = 0;

	VERSO_GFX_API virtual bool setSystemCursor(SystemCursorType systemCursorType) const = 0;

	VERSO_GFX_API virtual void* getNativeWindowHandle() const = 0;

	VERSO_GFX_API virtual void* getRenderer() const = 0;

	VERSO_GFX_API virtual void show() = 0;

	VERSO_GFX_API virtual void hide() = 0;

	VERSO_GFX_API virtual DisplayMode getDisplayMode() const = 0;

	VERSO_GFX_API virtual void setDisplayMode(const DisplayMode& displayMode) = 0;

	VERSO_GFX_API virtual Display getDisplay() const  = 0;

	VERSO_GFX_API virtual void setDisplay(const Display& display) = 0;

	VERSO_GFX_API virtual UString getTitle() const = 0;

	VERSO_GFX_API virtual void setTitle(const UString& windowTitle) = 0;

	VERSO_GFX_API virtual Vector2i getPositioni() const = 0;

	VERSO_GFX_API virtual Vector2f getPositionf() const = 0;

	VERSO_GFX_API virtual void setPosition(const Vector2i& location) = 0;

	// Note: centers on display chosen at create()
	VERSO_GFX_API virtual void setPositionCentered() = 0;

	VERSO_GFX_API virtual void setPositionAlignedOnDisplay(const Display& anotherDisplay, const Align& align) = 0;

	VERSO_GFX_API virtual Rectf getRelativeViewport() const = 0;

	VERSO_GFX_API virtual void setRelativeViewport(const Rectf& relativeViewport) = 0;

	VERSO_GFX_API virtual void resetRelativeViewport() = 0;

	VERSO_GFX_API virtual Recti getRenderViewporti() const = 0;

	VERSO_GFX_API virtual Rectf getRenderViewportf() const = 0;

	VERSO_GFX_API virtual void applyRenderViewport() const = 0;

	VERSO_GFX_API virtual Recti getDrawableViewporti() const = 0;

	VERSO_GFX_API virtual Rectf getDrawableViewportf() const = 0;

	VERSO_GFX_API virtual void applyDrawableViewport() const = 0;

	VERSO_GFX_API virtual void loadIcon(const UString& iconFileName) = 0;

	VERSO_GFX_API virtual bool pollEvent(Event& event) = 0;

	VERSO_GFX_API virtual bool waitEvent(Event& event) = 0;

	VERSO_GFX_API virtual InputController* getCameraInputController() const = 0;

	VERSO_GFX_API virtual ButtonHandle getCameraForward() const = 0;

	VERSO_GFX_API virtual ButtonHandle getCameraBackward() const = 0;

	VERSO_GFX_API virtual ButtonHandle getCameraLeft() const = 0;

	VERSO_GFX_API virtual ButtonHandle getCameraRight() const = 0;

	VERSO_GFX_API virtual ButtonHandle getCameraUp() const = 0;

	VERSO_GFX_API virtual ButtonHandle getCameraDown() const = 0;

	VERSO_GFX_API virtual ButtonHandle getCameraForwardFast() const = 0;

	VERSO_GFX_API virtual ButtonHandle getCameraBackwardFast() const = 0;

	VERSO_GFX_API virtual ButtonHandle getCameraLeftFast() const = 0;

	VERSO_GFX_API virtual ButtonHandle getCameraRightFast() const = 0;

	VERSO_GFX_API virtual ButtonHandle getCameraUpFast() const = 0;

	VERSO_GFX_API virtual ButtonHandle getCameraDownFast() const = 0;

	VERSO_GFX_API virtual ButtonHandle getCameraForwardReallyFast() const = 0;

	VERSO_GFX_API virtual ButtonHandle getCameraBackwardReallyFast() const = 0;

	VERSO_GFX_API virtual ButtonHandle getCameraLeftReallyFast() const = 0;

	VERSO_GFX_API virtual ButtonHandle getCameraRightReallyFast() const = 0;

	VERSO_GFX_API virtual ButtonHandle getCameraUpReallyFast() const = 0;

	VERSO_GFX_API virtual ButtonHandle getCameraDownReallyFast() const = 0;

	VERSO_GFX_API virtual ButtonHandle getCameraActionA() const = 0;

	VERSO_GFX_API virtual ButtonHandle getCameraActionB() const = 0;

	VERSO_GFX_API virtual void onResize() = 0;

public: // toString
	VERSO_GFX_API virtual UString toString() const = 0;

	VERSO_GFX_API virtual UString toStringDebug() const = 0;
};


} // End namespace Verso

#endif // End header guard

