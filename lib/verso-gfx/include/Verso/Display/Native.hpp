﻿#ifndef VERSO_GFX_DISPLAY_NATIVE_HPP
#define VERSO_GFX_DISPLAY_NATIVE_HPP

#include <Verso/verso-gfx-common.hpp>
#include <SDL2/SDL.h>

namespace Verso {


namespace Native {


VERSO_GFX_API extern void* getWindowHandle(SDL_Window* window);


} // End namespace Native


} // End namespace Verso

#endif // End header guard

