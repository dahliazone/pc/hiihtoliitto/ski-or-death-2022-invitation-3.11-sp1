#ifndef VERSO_GFX_DISPLAY_DISPLAYMODERATING_HPP
#define VERSO_GFX_DISPLAY_DISPLAYMODERATING_HPP

#include <Verso/System/UString.hpp>
#include <cstdint>
#include <ostream>
#include <Verso/System/Exception/IllegalParametersException.hpp>

namespace Verso {


enum class DisplayModeRating : int8_t {
	None = 0,
	Normal = 1 << 0,
	Extra = 1 << 1,
	Esoteric = 1 << 2,
	All = Normal | Extra | Esoteric,
	Default = Normal
};


typedef int8_t DisplayModeRatings;


inline DisplayModeRatings operator |(DisplayModeRating left, DisplayModeRating right)
{
	return static_cast<DisplayModeRatings>(static_cast<DisplayModeRatings>(left) | static_cast<DisplayModeRatings>(right));
}


inline DisplayModeRatings operator |(DisplayModeRating left, DisplayModeRatings right)
{
	return static_cast<DisplayModeRatings>(static_cast<DisplayModeRatings>(left) | right);
}


inline DisplayModeRatings operator |(DisplayModeRatings left, DisplayModeRating right)
{
	return static_cast<DisplayModeRatings>(left | static_cast<DisplayModeRatings>(right));
}


inline UString displayModeRatingsToString(const DisplayModeRatings& ratings)
{
	if (ratings == static_cast<DisplayModeRatings>(DisplayModeRating::None)) {
		return "None";
	}

	UString result;
	bool first = true;
	int8_t ratingsLeft = ratings;
	if (ratings & static_cast<DisplayModeRatings>(DisplayModeRating::Normal)) {
		result += "Normal";
		ratingsLeft |= static_cast<DisplayModeRatings>(DisplayModeRating::Normal);
		first = false;
	}

	if (ratings & static_cast<DisplayModeRatings>(DisplayModeRating::Extra)) {
		if (first == false) {
			result += " | ";
		}
		result += "Extra";
		ratingsLeft |= static_cast<DisplayModeRatings>(DisplayModeRating::Extra);
		first = false;
	}

	if (ratings & static_cast<DisplayModeRatings>(DisplayModeRating::Esoteric)) {
		if (first == false) {
			result += " | ";
		}
		result += "Esoteric";
		ratingsLeft |= static_cast<DisplayModeRatings>(DisplayModeRating::Esoteric);
	}

	if (ratingsLeft != 0) {
		UString error("Invalid DisplayModeRatings(");
		error.append2(static_cast<int8_t>(ratings));
		error += ") given. Flags (";
		error.append2(ratingsLeft);
		error += ") left.";
		VERSO_ILLEGALPARAMETERS("verso-gfx", error.c_str(), "");
	}

	return result;
}


inline UString displayModeRatingToString(DisplayModeRating rating)
{
	return displayModeRatingsToString(static_cast<DisplayModeRatings>(rating));
}


inline std::ostream& operator <<(std::ostream& ost, const DisplayModeRating& right)
{
	return ost << displayModeRatingToString(right);
}


} // End namespace Verso

#endif // End header guard

