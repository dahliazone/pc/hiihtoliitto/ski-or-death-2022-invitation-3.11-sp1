#ifndef VERSO_GFX_INPUT_MIDI_MIDIDEVICETYPE_HPP
#define VERSO_GFX_INPUT_MIDI_MIDIDEVICETYPE_HPP

#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>

namespace Verso {


enum class MidiDeviceType : std::uint8_t {
	Lpd8 = 0,
	NanoKontrol2,
	DJ2Go,
	QinHengElectronicsCh345MidiAdapter,
	Size,
	Unset
};


inline UString midiDeviceTypeToString(const MidiDeviceType& midiDeviceType)
{
	switch (midiDeviceType)
	{
	case MidiDeviceType::Unset:
		return "Unset";
	case MidiDeviceType::Lpd8:
		return "Lpd8";
	case MidiDeviceType::NanoKontrol2:
		return "NanoKontrol2";
	case MidiDeviceType::DJ2Go:
		return "DJ2Go";
	case MidiDeviceType::QinHengElectronicsCh345MidiAdapter:
		return "QinHengElectronicsCh345MidiAdapter";
	default:
		return "Unknown value";
	}
}


// returns MidiDeviceType::Unset for any erroneuos string
inline MidiDeviceType stringToMidiDeviceType(const UString& str)
{
	if (str.equals("Lpd8")) {
		return MidiDeviceType::Lpd8;
	}
	else if (str.equals("NanoKontrol2")) {
		return MidiDeviceType::NanoKontrol2;
	}
	else if (str.equals("DJ2Go")) {
		return MidiDeviceType::DJ2Go;
	}
	else if (str.equals("QinHengElectronicsCh345MidiAdapter")) {
		return MidiDeviceType::QinHengElectronicsCh345MidiAdapter;
	}
	else {
		return MidiDeviceType::Unset;
	}
}


} // End namespace Verso

#endif // End header guard

