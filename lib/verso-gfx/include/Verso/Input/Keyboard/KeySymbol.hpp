#ifndef VERSO_GFX_INPUT_KEYBOARD_KEYSYMBOL_HPP
#define VERSO_GFX_INPUT_KEYBOARD_KEYSYMBOL_HPP

#include <Verso/Input/Keyboard/KeyCode.hpp>
#include <Verso/Input/Keyboard/Keymodifier.hpp>
#include <Verso/Input/Keyboard/KeyScancode.hpp>

namespace Verso {


struct KeySymbol
{
public:
	inline KeySymbol() :
		keyCode(KeyCode::Unknown),
		scancode(KeyScancode::Unknown),
		keymodifiers(static_cast<Keymodifiers>(Keymodifier::NoneOrAny))
	{
	}


	inline explicit KeySymbol(KeyCode keyCode) :
		keyCode(keyCode),
		scancode(KeyScancode::Unknown),
		keymodifiers(static_cast<Keymodifiers>(Keymodifier::NoneOrAny))
	{
	}


	inline KeySymbol(KeyCode keyCode, Keymodifiers keymodifiers) :
		keyCode(keyCode),
		scancode(KeyScancode::Unknown),
		keymodifiers(keymodifiers)
	{
	}


	inline KeySymbol(KeyCode keyCode, KeyScancode scancode, Keymodifiers keymodifiers) :
		keyCode(keyCode),
		scancode(scancode),
		keymodifiers(keymodifiers)
	{
	}


	inline void reset()
	{
		keyCode = KeyCode::Unknown;
		scancode = KeyScancode::Unknown;
		keymodifiers = static_cast<Keymodifiers>(Keymodifier::NoneOrAny);
	}

public: // Operators
//	inline bool operator ==(const KeySymbol& right) const
//	{
//		std::cout << "  comparison: "<<(*this)<<"::operator ==(const KeySymbol& right="<<right<<") const" << std::endl;
//		if ((keyCode == right.keyCode) || (scancode == right.scancode)) {
//			if ((keymodifiers & right.keymodifiers) || (right.keymodifiers & keymodifiers)) {
//				std::cout << "    => result: true" << std::endl;
//				return true;
//			}
//			else {
//				std::cout << "    => result: false" << std::endl;
//				return false;
//			}
//			//return ( && (keymodifiers == right.keymodifiers);
//		}
//		else
//			return false;
//	}


//	inline bool operator !=(const KeySymbol& right) const
//	{
//		std::cout << "  comparison: "<<(*this)<<"::operator !=(const KeySymbol& right="<<right<<") const" << std::endl;
//		return !(*this == right);
//	}


	inline bool operator <(const KeySymbol& right) const
	{
		//std::cout << "  comparison: "<<(*this)<<"::operator <(const KeySymbol& right="<<right<<") const" << std::endl;
		if (keyCode != right.keyCode) {
			//std::cout << "    => result: "<<(keyCode < right.keyCode) << std::endl;
			return keyCode < right.keyCode;
		}
		else {
			return keymodifiers < right.keymodifiers;
		}
	}


//	inline bool operator <=(const KeySymbol& right) const
//	{
//		std::cout << "  comparison: "<<(*this)<<"::operator <=(const KeySymbol& right="<<right<<") const" << std::endl;
//		if (keyCode != right.keyCode)
//			return keyCode <= right.keyCode;
//		else
//			return keymodifiers <= right.keymodifiers;
//	}


//	inline bool operator >(const KeySymbol& right) const
//	{
//		std::cout << "  comparison: "<<(*this)<<"::operator >(const KeySymbol& right="<<right<<") const" << std::endl;
//		if (keyCode != right.keyCode)
//			return keyCode > right.keyCode;
//		else
//			return keymodifiers > right.keymodifiers;
//	}


//	inline bool operator >=(const KeySymbol& right) const
//	{
//		std::cout << "  comparison: "<<(*this)<<"::operator >=(const KeySymbol& right="<<right<<") const" << std::endl;
//		if (keyCode != right.keyCode)
//			return keyCode >= right.keyCode;
//		else
//			return keymodifiers >= right.keymodifiers;
//	}


	inline KeySymbol& operator =(const KeySymbol& right)
	{
		keyCode = right.keyCode;
		scancode = right.scancode;
		keymodifiers = right.keymodifiers;
		return *this;
	}

public:
	KeyCode keyCode;
	KeyScancode scancode;
	Keymodifiers keymodifiers;
};


VERSO_GFX_API UString keySymbolToString(const KeySymbol& keySymbol);
VERSO_GFX_API std::ostream& operator <<(std::ostream& ost, const KeySymbol& right);

VERSO_GFX_API UString keyCodeAndKeymodifiersToString(KeyCode keyCode, Keymodifiers keymodifiers, bool humanReadable = true);
VERSO_GFX_API bool keymodifiersContainKeyCode(Keymodifiers keymodifiers, KeyCode keyCode);


} // End namespace Verso

#endif // End header guard

