#ifndef VERSO_GFX_INPUT_KEYBOARD_KEYSCANCODE_HPP
#define VERSO_GFX_INPUT_KEYBOARD_KEYSCANCODE_HPP

#include <Verso/verso-gfx-common.hpp>
#include <Verso/System/UString.hpp>
#include <SDL2/SDL.h>
#include <ostream>
#include <cstdint>

namespace Verso {


enum class KeyScancode : uint32_t
{
	First = SDL_SCANCODE_UNKNOWN,
	None = SDL_SCANCODE_UNKNOWN,
	Unknown = SDL_SCANCODE_UNKNOWN,

	A = SDL_SCANCODE_A,
	B = SDL_SCANCODE_B,
	C = SDL_SCANCODE_C,
	D = SDL_SCANCODE_D,
	E = SDL_SCANCODE_E,
	F = SDL_SCANCODE_F,
	G = SDL_SCANCODE_G,
	H = SDL_SCANCODE_H,
	I = SDL_SCANCODE_I,
	J = SDL_SCANCODE_J,
	K = SDL_SCANCODE_K,
	L = SDL_SCANCODE_L,
	M = SDL_SCANCODE_M,
	N = SDL_SCANCODE_N,
	O = SDL_SCANCODE_O,
	P = SDL_SCANCODE_P,
	Q = SDL_SCANCODE_Q,
	R = SDL_SCANCODE_R,
	S = SDL_SCANCODE_S,
	T = SDL_SCANCODE_T,
	U = SDL_SCANCODE_U,
	V = SDL_SCANCODE_V,
	W = SDL_SCANCODE_W,
	X = SDL_SCANCODE_X,
	Y = SDL_SCANCODE_Y,
	Z = SDL_SCANCODE_Z,

	N1 = SDL_SCANCODE_1,
	N2 = SDL_SCANCODE_2,
	N3 = SDL_SCANCODE_3,
	N4 = SDL_SCANCODE_4,
	N5 = SDL_SCANCODE_5,
	N6 = SDL_SCANCODE_6,
	N7 = SDL_SCANCODE_7,
	N8 = SDL_SCANCODE_8,
	N9 = SDL_SCANCODE_9,
	N0 = SDL_SCANCODE_0,

	Return = SDL_SCANCODE_RETURN,
	Escape = SDL_SCANCODE_ESCAPE,
	Backspace = SDL_SCANCODE_BACKSPACE,
	Tab = SDL_SCANCODE_TAB,
	Space = SDL_SCANCODE_SPACE,
	Minus = SDL_SCANCODE_MINUS,
	Equals = SDL_SCANCODE_EQUALS,
	LeftBracket = SDL_SCANCODE_LEFTBRACKET,
	RightBracket = SDL_SCANCODE_RIGHTBRACKET,
	Backslash = SDL_SCANCODE_BACKSLASH,
	NonUsHash = SDL_SCANCODE_NONUSHASH,
	Semicolon = SDL_SCANCODE_SEMICOLON,
	Apostrophe = SDL_SCANCODE_APOSTROPHE,
	Grave = SDL_SCANCODE_GRAVE,
	Comma = SDL_SCANCODE_COMMA,
	Period = SDL_SCANCODE_PERIOD,
	Slash = SDL_SCANCODE_SLASH,
	CapsLock = SDL_SCANCODE_CAPSLOCK,
	F1 = SDL_SCANCODE_F1,
	F2 = SDL_SCANCODE_F2,
	F3 = SDL_SCANCODE_F3,
	F4 = SDL_SCANCODE_F4,
	F5 = SDL_SCANCODE_F5,
	F6 = SDL_SCANCODE_F6,
	F7 = SDL_SCANCODE_F7,
	F8 = SDL_SCANCODE_F8,
	F9 = SDL_SCANCODE_F9,
	F10 = SDL_SCANCODE_F10,
	F11 = SDL_SCANCODE_F11,
	F12 = SDL_SCANCODE_F12,
	PrintScreen = SDL_SCANCODE_PRINTSCREEN,
	ScrollLock = SDL_SCANCODE_SCROLLLOCK,
	Pause = SDL_SCANCODE_PAUSE,
	Insert = SDL_SCANCODE_INSERT,
	Home = SDL_SCANCODE_HOME,
	PageUp = SDL_SCANCODE_PAGEUP,
	Del = SDL_SCANCODE_DELETE,
	End = SDL_SCANCODE_END,
	PageDown = SDL_SCANCODE_PAGEDOWN,
	Right = SDL_SCANCODE_RIGHT,
	Left = SDL_SCANCODE_LEFT,
	Down = SDL_SCANCODE_DOWN,
	Up = SDL_SCANCODE_UP,
	NumLockClear = SDL_SCANCODE_NUMLOCKCLEAR,
	KpDivide = SDL_SCANCODE_KP_DIVIDE,
	KpMultiply = SDL_SCANCODE_KP_MULTIPLY,
	KpMinus = SDL_SCANCODE_KP_MINUS,
	KpPlus = SDL_SCANCODE_KP_PLUS,
	KpEnter = SDL_SCANCODE_KP_ENTER,
	Kp1 = SDL_SCANCODE_KP_1,
	Kp2 = SDL_SCANCODE_KP_2,
	Kp3 = SDL_SCANCODE_KP_3,
	Kp4 = SDL_SCANCODE_KP_4,
	Kp5 = SDL_SCANCODE_KP_5,
	Kp6 = SDL_SCANCODE_KP_6,
	Kp7 = SDL_SCANCODE_KP_7,
	Kp8 = SDL_SCANCODE_KP_8,
	Kp9 = SDL_SCANCODE_KP_9,
	Kp0 = SDL_SCANCODE_KP_0,
	KpPeriod = SDL_SCANCODE_KP_PERIOD,
	NonUsBackslash = SDL_SCANCODE_NONUSBACKSLASH,
	Application = SDL_SCANCODE_APPLICATION,
	Power = SDL_SCANCODE_POWER,
	KpEquals = SDL_SCANCODE_KP_EQUALS,
	F13 = SDL_SCANCODE_F13,
	F14 = SDL_SCANCODE_F14,
	F15 = SDL_SCANCODE_F15,
	F16 = SDL_SCANCODE_F16,
	F17 = SDL_SCANCODE_F17,
	F18 = SDL_SCANCODE_F18,
	F19 = SDL_SCANCODE_F19,
	F20 = SDL_SCANCODE_F20,
	F21 = SDL_SCANCODE_F21,
	F22 = SDL_SCANCODE_F22,
	F23 = SDL_SCANCODE_F23,
	F24 = SDL_SCANCODE_F24,
	Execute = SDL_SCANCODE_EXECUTE,
	Help = SDL_SCANCODE_HELP,
	Menu = SDL_SCANCODE_MENU,
	Select = SDL_SCANCODE_SELECT,
	Stop = SDL_SCANCODE_STOP,
	Again = SDL_SCANCODE_AGAIN, //!< Again or Redo
	Undo = SDL_SCANCODE_UNDO,
	Cut = SDL_SCANCODE_CUT,
	Copy = SDL_SCANCODE_COPY,
	Paste = SDL_SCANCODE_PASTE,
	Find = SDL_SCANCODE_FIND,
	Mute = SDL_SCANCODE_MUTE,
	VolumeUp = SDL_SCANCODE_VOLUMEUP,
	VolumeDown = SDL_SCANCODE_VOLUMEDOWN,
// SDL comment: not sure whether there's a reason to enable these
// 	LockingCapsLock = SDL_SCANCODE_LOCKINGCAPSLOCK,
// 	LockingNumLock = SDL_SCANCODE_LOCKINGNUMLOCK,
// 	LockingScrollLock = SDL_SCANCODE_LOCKINGSCROLLLOCK,
	KpComma = SDL_SCANCODE_KP_COMMA,
	KpEqualsAs400 = SDL_SCANCODE_KP_EQUALSAS400,

	International1 = SDL_SCANCODE_INTERNATIONAL1,
	International2 = SDL_SCANCODE_INTERNATIONAL2,
	International3 = SDL_SCANCODE_INTERNATIONAL3, //!< Yen
	International4 = SDL_SCANCODE_INTERNATIONAL4,
	International5 = SDL_SCANCODE_INTERNATIONAL5,
	International6 = SDL_SCANCODE_INTERNATIONAL6,
	International7 = SDL_SCANCODE_INTERNATIONAL7,
	International8 = SDL_SCANCODE_INTERNATIONAL8,
	International9 = SDL_SCANCODE_INTERNATIONAL9,
	Lang1 = SDL_SCANCODE_LANG1, //!< Hangul/English toggle
	Lang2 = SDL_SCANCODE_LANG2, //!< Hanja conversion
	Lang3 = SDL_SCANCODE_LANG3, //!< Katakana
	Lang4 = SDL_SCANCODE_LANG4, //!< Hiragana
	Lang5 = SDL_SCANCODE_LANG5, //!< Zenkaku/Hankaku
	Lang6 = SDL_SCANCODE_LANG6, //!< reserved
	Lang7 = SDL_SCANCODE_LANG7, //!< reserved
	Lang8 = SDL_SCANCODE_LANG8, //!< reserved
	Lang9 = SDL_SCANCODE_LANG9, //!< reserved

	Alterase = SDL_SCANCODE_ALTERASE, //!< Erase-Eaze
	SysReq = SDL_SCANCODE_SYSREQ,
	Cancel = SDL_SCANCODE_CANCEL,
	Clear = SDL_SCANCODE_CLEAR,
	Prior = SDL_SCANCODE_PRIOR,
	Return2 = SDL_SCANCODE_RETURN2,
	Separator = SDL_SCANCODE_SEPARATOR,
	Out = SDL_SCANCODE_OUT,
	Oper = SDL_SCANCODE_OPER,
	ClearAgain = SDL_SCANCODE_CLEARAGAIN,
	CrSel = SDL_SCANCODE_CRSEL,
	ExSel = SDL_SCANCODE_EXSEL,

	Kp00 = SDL_SCANCODE_KP_00,
	Kp000 = SDL_SCANCODE_KP_000,
	ThousandsSeparator = SDL_SCANCODE_THOUSANDSSEPARATOR,
	DecimalSeparator = SDL_SCANCODE_DECIMALSEPARATOR,
	CurrencyUnit = SDL_SCANCODE_CURRENCYUNIT,
	CurrencySubUnit = SDL_SCANCODE_CURRENCYSUBUNIT,
	KpLeftParen = SDL_SCANCODE_KP_LEFTPAREN,
	KpRightParen = SDL_SCANCODE_KP_RIGHTPAREN,
	KpLeftBrace = SDL_SCANCODE_KP_LEFTBRACE,
	KpRightBrace = SDL_SCANCODE_KP_RIGHTBRACE,
	KpTab = SDL_SCANCODE_KP_TAB,
	KpBackspace = SDL_SCANCODE_KP_BACKSPACE,
	KpA = SDL_SCANCODE_KP_A,
	KpB = SDL_SCANCODE_KP_B,
	KpC = SDL_SCANCODE_KP_C,
	KpD = SDL_SCANCODE_KP_D,
	KpE = SDL_SCANCODE_KP_E,
	KpF = SDL_SCANCODE_KP_F,
	KpXor = SDL_SCANCODE_KP_XOR,
	KpPower = SDL_SCANCODE_KP_POWER,
	KpPercent = SDL_SCANCODE_KP_PERCENT,
	KpLess = SDL_SCANCODE_KP_LESS,
	KpGreater = SDL_SCANCODE_KP_GREATER,
	KpAmpersand = SDL_SCANCODE_KP_AMPERSAND,
	KpDblAmpersand = SDL_SCANCODE_KP_DBLAMPERSAND,
	KpVerticalBar = SDL_SCANCODE_KP_VERTICALBAR,
	KpDblVerticalBar = SDL_SCANCODE_KP_DBLVERTICALBAR,
	KpColon = SDL_SCANCODE_KP_COLON,
	KpHash = SDL_SCANCODE_KP_HASH,
	KpSpace = SDL_SCANCODE_KP_SPACE,
	KpAt = SDL_SCANCODE_KP_AT,
	KpExclam = SDL_SCANCODE_KP_EXCLAM,
	KpMemStore = SDL_SCANCODE_KP_MEMSTORE,
	KpMemRecall = SDL_SCANCODE_KP_MEMRECALL,
	KpMemClear = SDL_SCANCODE_KP_MEMCLEAR,
	KpMemAdd = SDL_SCANCODE_KP_MEMADD,
	KpMemSubtract = SDL_SCANCODE_KP_MEMSUBTRACT,
	KpMemMultiply = SDL_SCANCODE_KP_MEMMULTIPLY,
	KpMemDivide = SDL_SCANCODE_KP_MEMDIVIDE,
	KpPlusMinus = SDL_SCANCODE_KP_PLUSMINUS,
	KpClear = SDL_SCANCODE_KP_CLEAR,
	KpClearEntry = SDL_SCANCODE_KP_CLEARENTRY,
	KpBinary = SDL_SCANCODE_KP_BINARY,
	KpOctal = SDL_SCANCODE_KP_OCTAL,
	KpDecimal = SDL_SCANCODE_KP_DECIMAL,
	KpHexadecimal = SDL_SCANCODE_KP_HEXADECIMAL,

	LCtrl = SDL_SCANCODE_LCTRL,
	LShift = SDL_SCANCODE_LSHIFT,
	LAlt = SDL_SCANCODE_LALT, //!< alt, option
	LGui = SDL_SCANCODE_LGUI, //!< windows, command (apple), meta
	RCtrl = SDL_SCANCODE_RCTRL,
	RShift = SDL_SCANCODE_RSHIFT,
	RAlt = SDL_SCANCODE_RALT, //!< alt gr, option
	RGui = SDL_SCANCODE_RGUI, //!< windows, command (apple), meta

	Mode = SDL_SCANCODE_MODE,

	AudioNext = SDL_SCANCODE_AUDIONEXT,
	AudioPrev = SDL_SCANCODE_AUDIOPREV,
	AudioStop = SDL_SCANCODE_AUDIOSTOP,
	AudioPlay = SDL_SCANCODE_AUDIOPLAY,
	AudioMute = SDL_SCANCODE_AUDIOMUTE,
	MediaSelect = SDL_SCANCODE_MEDIASELECT,
	Www = SDL_SCANCODE_WWW,
	Mail = SDL_SCANCODE_MAIL,
	Calculator = SDL_SCANCODE_CALCULATOR,
	Computer = SDL_SCANCODE_COMPUTER,
	AcSearch = SDL_SCANCODE_AC_SEARCH,
	AcHome = SDL_SCANCODE_AC_HOME,
	AcBack = SDL_SCANCODE_AC_BACK,
	AcForward = SDL_SCANCODE_AC_FORWARD,
	AcStop = SDL_SCANCODE_AC_STOP,
	AcRefresh = SDL_SCANCODE_AC_REFRESH,
	AcBookmarks = SDL_SCANCODE_AC_BOOKMARKS,

	BrightnessDown = SDL_SCANCODE_BRIGHTNESSDOWN,
	BrightnessUp = SDL_SCANCODE_BRIGHTNESSUP,
	DisplaySwitch = SDL_SCANCODE_DISPLAYSWITCH, //!< display mirroring/dual display switch, video mode switch
	KbdIllumToggle = SDL_SCANCODE_KBDILLUMTOGGLE,
	KbdIllumDown = SDL_SCANCODE_KBDILLUMDOWN,
	KbdIllumUp = SDL_SCANCODE_KBDILLUMUP,
	Eject = SDL_SCANCODE_EJECT,
	Sleep = SDL_SCANCODE_SLEEP,

	App1 = SDL_SCANCODE_APP1,
	App2 = SDL_SCANCODE_APP2,

	Size = SDL_NUM_SCANCODES,	//!< Size of scancodes array
	Unset						//!< Unset key
};


VERSO_GFX_API UString keyScancodeToString(KeyScancode keyScancode);
VERSO_GFX_API std::ostream& operator <<(std::ostream& ost, KeyScancode right);


} // End namespace Verso

#endif // End header guard

