#ifndef VERSO_GFX_INPUT_KEYBOARD_KEYMODIFIER_HPP
#define VERSO_GFX_INPUT_KEYBOARD_KEYMODIFIER_HPP

#include <Verso/verso-gfx-common.hpp>
#include <Verso/Input/ButtonState.hpp>
#include <SDL2/SDL.h>
#include <cstdint>

namespace Verso {


enum class Keymodifier : uint32_t
{
	None = KMOD_NONE,		//!< No key modifiers active
	LShift = KMOD_LSHIFT,	//!< Left shift
	RShift = KMOD_RSHIFT,	//!< Right shift
	LCtrl = KMOD_LCTRL,		//!< Left control
	RCtrl = KMOD_RCTRL,		//!< Right control
	LAlt = KMOD_LALT,		//!< Left alt
	RAlt = KMOD_RALT,		//!< Right alt
	LGui = KMOD_LGUI,		//!< Left meta/logo
	RGui = KMOD_RGUI,		//!< Right meta/logo
	Num = KMOD_NUM,			//!< Numlock
	Caps = KMOD_CAPS,		//!< Capslock
	Mode = KMOD_MODE,
	Reserved = KMOD_RESERVED,
	AnyShift = LShift | RShift,					//!< Any shift
	AnyCtrl = LCtrl | RCtrl,					//!< Any control
	AnyAlt = LAlt | RAlt,						//!< Any alt
	AnyGui = LGui | RGui,						//!< Any meta/logo
	NoneOrAnyNumCapsMode = Num | Caps | Mode,	//!< Any stateful (numlock, capslock, mode) button
	NoneOrAny = KMOD_RESERVED*2,				//!< None or any modifier active
	Unset = KMOD_RESERVED*2*2
};

typedef uint32_t Keymodifiers;


VERSO_GFX_API std::vector<Keymodifier> getAllKeymodifiers();
VERSO_GFX_API bool keyModifiersMatchRules(Keymodifiers input, Keymodifiers rules);
VERSO_GFX_API UString keymodifierToString(Keymodifier keymodifier);
VERSO_GFX_API UString keymodifiersToString(Keymodifiers keymodifiers, bool humanReadable = true);
VERSO_GFX_API std::ostream& operator <<(std::ostream& ost, Keymodifier right);


// any Control key
#define KEYMOD_CTRL		(KEYMOD_LCTRL | KEYMOD_RCTRL)
// any Shift key
#define KEYMOD_SHIFT	(KEYMOD_LSHIFT | KEYMOD_RSHIFT)
// any Alt key
#define KEYMOD_ALT		(KEYMOD_LALT | KEYMOD_RALT)
// any Meta key
#define KEYMOD_META		(KEYMOD_LMETA | KEYMOD_RMETA)
// any other modifier
#define KEYMOD_OTHER	(KMOD_NUM | KMOD_CAPS | KMOD_MODE)


} // End namespace Verso

#endif // End header guard

