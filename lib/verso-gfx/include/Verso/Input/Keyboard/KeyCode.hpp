#ifndef VERSO_GFX_INPUT_KEYBOARD_KEYCODE_HPP
#define VERSO_GFX_INPUT_KEYBOARD_KEYCODE_HPP

#include <Verso/verso-gfx-common.hpp>
#include <Verso/System/UString.hpp>
#include <SDL2/SDL.h>
#include <ostream>
#include <cstdint>

namespace Verso {


/**
 * Key code defines from almost all interesting keys. SDL2 compatible mapping.
 */
enum class KeyCode : uint32_t
{
	// The keyboard syms have been cleverly chosen to map to ASCII
	Unknown		= 0,
	None		= 0,
	First		= 0,

	Return = SDLK_RETURN, // '\r'
	Escape = SDLK_ESCAPE, // '\033'
	Backspace = SDLK_BACKSPACE, // '\b'
	Tab = SDLK_TAB, // '\t'
	Space = SDLK_SPACE, // ' '
	Exclaim = SDLK_EXCLAIM, // '!'
	QuoteDbl = SDLK_QUOTEDBL, // '"'
	Hash = SDLK_HASH, // '#'
	Percent = SDLK_PERCENT, // '%'
	Dollar = SDLK_DOLLAR, // '$'
	Ampersand = SDLK_AMPERSAND, // '&'
	Quote = SDLK_QUOTE, // '\'
	LeftParen = SDLK_LEFTPAREN, // '('
	RightParen = SDLK_RIGHTPAREN, // ')'
	Asterisk = SDLK_ASTERISK, // '*'
	Plus = SDLK_PLUS, // '+'
	Comma = SDLK_COMMA, // ','
	Minus = SDLK_MINUS, // '-'
	Period = SDLK_PERIOD, // '.'
	Slash = SDLK_SLASH, // '/'
	N0 = SDLK_0, // '0'
	N1 = SDLK_1, // '1'
	N2 = SDLK_2, // '2'
	N3 = SDLK_3, // '3'
	N4 = SDLK_4, // '4'
	N5 = SDLK_5, // '5'
	N6 = SDLK_6, // '6'
	N7 = SDLK_7, // '7'
	N8 = SDLK_8, // '8'
	N9 = SDLK_9, // '9'
	Colon = SDLK_COLON, // ':'
	Semicolon = SDLK_SEMICOLON, // ';'
	Less = SDLK_LESS, // '<'
	Equals = SDLK_EQUALS, // '='
	Greater = SDLK_GREATER, // '>'
	Question = SDLK_QUESTION, // '?'
	At = SDLK_AT, // '@'

	// Skip uppercase letters

	LeftBracket = SDLK_LEFTBRACKET, // '['
	Backslash = SDLK_BACKSLASH, // '\\'
	RightBracket = SDLK_RIGHTBRACKET, // ']'
	Caret = SDLK_CARET, // '^'
	Underscore = SDLK_UNDERSCORE, // '_'
	Backquote = SDLK_BACKQUOTE, // '`'
	A = SDLK_a, // 'a'
	B = SDLK_b, // 'b'
	C = SDLK_c, // 'c'
	D = SDLK_d, // 'd'
	E = SDLK_e, // 'e'
	F = SDLK_f, // 'f'
	G = SDLK_g, // 'g'
	H = SDLK_h, // 'h'
	I = SDLK_i, // 'i'
	J = SDLK_j, // 'j'
	K = SDLK_k, // 'k'
	L = SDLK_l, // 'l'
	M = SDLK_m, // 'm'
	N = SDLK_n, // 'n'
	O = SDLK_o, // 'o'
	P = SDLK_p, // 'p'
	Q = SDLK_q, // 'q'
	R = SDLK_r, // 'r'
	S = SDLK_s, // 's'
	T = SDLK_t, // 't'
	U = SDLK_u, // 'u'
	V = SDLK_v, // 'v'
	W = SDLK_w, // 'w'
	X = SDLK_x, // 'x'
	Y = SDLK_y, // 'y'
	Z = SDLK_z, // 'z'

	CapsLock = SDLK_CAPSLOCK,

	F1 = SDLK_F1,
	F2 = SDLK_F2,
	F3 = SDLK_F3,
	F4 = SDLK_F4,
	F5 = SDLK_F5,
	F6 = SDLK_F6,
	F7 = SDLK_F7,
	F8 = SDLK_F8,
	F9 = SDLK_F9,
	F10 = SDLK_F10,
	F11 = SDLK_F11,
	F12 = SDLK_F12,

	PrintScreen = SDLK_PRINTSCREEN,
	ScrollLock = SDLK_SCROLLLOCK,
	Pause = SDLK_PAUSE,
	Insert = SDLK_INSERT,
	Home = SDLK_HOME,
	PageUp = SDLK_PAGEUP,
	Del = SDLK_DELETE, // '\177'
	End = SDLK_END,
	PageDown = SDLK_PAGEDOWN,
	Right = SDLK_RIGHT,
	Left = SDLK_LEFT,
	Down = SDLK_DOWN,
	Up = SDLK_UP,

	NumLockClear = SDLK_NUMLOCKCLEAR,
	KpDivide = SDLK_KP_DIVIDE,
	KpMultiply = SDLK_KP_MULTIPLY,
	KpMinus = SDLK_KP_MINUS,
	KpPlus = SDLK_KP_PLUS,
	KpEnter = SDLK_KP_ENTER,
	Kp1 = SDLK_KP_1,
	Kp2 = SDLK_KP_2,
	Kp3 = SDLK_KP_3,
	Kp4 = SDLK_KP_4,
	Kp5 = SDLK_KP_5,
	Kp6 = SDLK_KP_6,
	Kp7 = SDLK_KP_7,
	Kp8 = SDLK_KP_8,
	Kp9 = SDLK_KP_9,
	Kp0 = SDLK_KP_0,
	KpPeriod = SDLK_KP_PERIOD,

	Application = SDLK_APPLICATION,
	Power = SDLK_POWER,
	KpEquals = SDLK_KP_EQUALS,
	F13 = SDLK_F13,
	F14 = SDLK_F14,
	F15 = SDLK_F15,
	F16 = SDLK_F16,
	F17 = SDLK_F17,
	F18 = SDLK_F18,
	F19 = SDLK_F19,
	F20 = SDLK_F20,
	F21 = SDLK_F21,
	F22 = SDLK_F22,
	F23 = SDLK_F23,
	F24 = SDLK_F24,
	Execute = SDLK_EXECUTE,
	Help = SDLK_HELP,
	Menu = SDLK_MENU,
	Select = SDLK_SELECT,
	Stop = SDLK_STOP,
	Again = SDLK_AGAIN,
	Undo = SDLK_UNDO,
	Cut = SDLK_CUT,
	Copy = SDLK_COPY,
	Paste = SDLK_PASTE,
	Find = SDLK_FIND,
	Mute = SDLK_MUTE,
	VolumeUp = SDLK_VOLUMEUP,
	VolumeDown = SDLK_VOLUMEDOWN,
	KpComma = SDLK_KP_COMMA,
	KpEqualsAs400 = SDLK_KP_EQUALSAS400,

	Alterase = SDLK_ALTERASE,
	SysReq = SDLK_SYSREQ,
	Cancel = SDLK_CANCEL,
	Clear = SDLK_CLEAR,
	Prior = SDLK_PRIOR,
	Return2 = SDLK_RETURN2,
	Separator = SDLK_SEPARATOR,
	Out = SDLK_OUT,
	Oper = SDLK_OPER,
	ClearAgain = SDLK_CLEARAGAIN,
	CrSel = SDLK_CRSEL,
	ExSel = SDLK_EXSEL,

	Kp00 = SDLK_KP_00,
	Kp000 = SDLK_KP_000,
	ThousandsSeparator = SDLK_THOUSANDSSEPARATOR,
	DecimalSeparator = SDLK_DECIMALSEPARATOR,
	CurrencyUnit = SDLK_CURRENCYUNIT,
	CurrencySubUnit = SDLK_CURRENCYSUBUNIT,

	KpLeftParen = SDLK_KP_LEFTPAREN,
	KpRightParen = SDLK_KP_RIGHTPAREN,
	KpLeftBrace = SDLK_KP_LEFTBRACE,
	KpRightBrace = SDLK_KP_RIGHTBRACE,
	KpTab = SDLK_KP_TAB,
	KpBackspace = SDLK_KP_BACKSPACE,
	KpA = SDLK_KP_A,
	KpB = SDLK_KP_B,
	KpC = SDLK_KP_C,
	KpD = SDLK_KP_D,
	KpE = SDLK_KP_E,
	KpF = SDLK_KP_F,
	KpXor = SDLK_KP_XOR,
	KpPower = SDLK_KP_POWER,
	KpPercent = SDLK_KP_PERCENT,
	KpLess = SDLK_KP_LESS,
	KpGreater = SDLK_KP_GREATER,
	KpAmpersand = SDLK_KP_AMPERSAND,
	KpDblAmpersand = SDLK_KP_DBLAMPERSAND,
	KpVerticalBar = SDLK_KP_VERTICALBAR,
	KpDblVerticalBar = SDLK_KP_DBLVERTICALBAR,
	KpColon = SDLK_KP_COLON,
	KpHash = SDLK_KP_HASH,
	KpSpace = SDLK_KP_SPACE,
	KpAt = SDLK_KP_AT,
	KpExclam = SDLK_KP_EXCLAM,
	KpMemStore = SDLK_KP_MEMSTORE,
	KpMemRecall = SDLK_KP_MEMRECALL,
	KpMemClear = SDLK_KP_MEMCLEAR,
	KpMemAdd = SDLK_KP_MEMADD,
	KpMemSubtract = SDLK_KP_MEMSUBTRACT,
	KpMemMultiply = SDLK_KP_MEMMULTIPLY,
	KpMemDivide = SDLK_KP_MEMDIVIDE,
	KpPlusMinus = SDLK_KP_PLUSMINUS,
	KpClear = SDLK_KP_CLEAR,
	KpClearEntry = SDLK_KP_CLEARENTRY,
	KpBinary = SDLK_KP_BINARY,
	KpOctal = SDLK_KP_OCTAL,
	KpDecimal = SDLK_KP_DECIMAL,
	KpHexadecimal = SDLK_KP_HEXADECIMAL,

	LCtrl = SDLK_LCTRL,
	LShift = SDLK_LSHIFT,
	LAlt = SDLK_LALT,
	LGui = SDLK_LGUI,
	RCtrl = SDLK_RCTRL,
	RShift = SDLK_RSHIFT,
	RAlt = SDLK_RALT,
	RGui = SDLK_RGUI,

	Mode = SDLK_MODE,

	AudioNext = SDLK_AUDIONEXT,
	AudioPrev = SDLK_AUDIOPREV,
	AudioStop = SDLK_AUDIOSTOP,
	AudioPlay = SDLK_AUDIOPLAY,
	AudioMute = SDLK_AUDIOMUTE,
	MediaSelect = SDLK_MEDIASELECT,
	Www = SDLK_WWW,
	Mail = SDLK_MAIL,
	Calculator = SDLK_CALCULATOR,
	Computer = SDLK_COMPUTER,
	AcSearch = SDLK_AC_SEARCH,
	AcHome = SDLK_AC_HOME,
	AcBack = SDLK_AC_BACK,
	AcForward = SDLK_AC_FORWARD,
	AcStop = SDLK_AC_STOP,
	AcRefresh = SDLK_AC_REFRESH,
	AcBookmarks = SDLK_AC_BOOKMARKS,

	BrightnessDown = SDLK_BRIGHTNESSDOWN,
	BrightnessUp = SDLK_BRIGHTNESSUP,
	DisplaySwitch = SDLK_DISPLAYSWITCH,
	KbdIllumToggle = SDLK_KBDILLUMTOGGLE,
	KbdIllumDown = SDLK_KBDILLUMDOWN,
	KbdIllumUp = SDLK_KBDILLUMUP,
	Eject = SDLK_EJECT,
	Sleep = SDLK_SLEEP,

	// Add any other keys here

	Size,					//!< Size of keys array
	Unset					//!< Unset key
};


VERSO_GFX_API UString keyCodeToString(KeyCode keyCode);
VERSO_GFX_API std::ostream& operator <<(std::ostream& ost, KeyCode right);


} // End namespace Verso

#endif // End header guard

