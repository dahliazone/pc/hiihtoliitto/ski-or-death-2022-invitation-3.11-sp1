#ifndef VERSO_GFX_INPUT_INPUTCONTROLLER_HPP
#define VERSO_GFX_INPUT_INPUTCONTROLLER_HPP

// \TODO: check through code to match Verso standards!!!

#include <Verso/Input/Event.hpp>
#include <Verso/Input/Mouse/MouseState.hpp>
#include <Verso/Input/WeightedButtonState.hpp>
#include <forward_list>

namespace Verso {


typedef uint32_t AxisHandle;
typedef uint32_t ButtonHandle;


// contains current state + list of events
class InputController
{
private:
	// \TODO: Mouse field (temporary?)
	std::forward_list<MouseEvent> mouseEvents;
	  // \TODO: check if here's all the state provided by SDL2!!!
	int32_t mouseX, mouseY;
	int32_t mouseMotionX, mouseMotionY; // mouse relative movement
	AxisState mouseMotionXRel, mouseMotionYRel; // mouse relative movement in relative coordinates [0..1]
	ButtonState mouseButtonState[static_cast<size_t>(MouseButton::Size)];
	int32_t mouseWheelMotionX, mouseWheelMotionY; // scrolled amount with mouse wheel (usually -1 or 1)
	AxisState mouseWheelMotionXRel, mouseWheelMotionYRel; // scrolled amount with mouse wheel in relative coordinates [0..1]

	UString name;

	//map<KeySymbol, map<ButtonHandle>> keyboardToButton;
	std::map<KeyCode, std::map<Keymodifiers, ButtonHandle>> keyboardToButton;
	//map<MouseAxis, AxisHandle> mouseAxisToAxis;
	std::map<MouseButton, ButtonHandle> mouseButtonToButton;
	std::map<GameControllerAxis, AxisHandle> gameControllerButtonToAxis;
	std::map<GameControllerButton, ButtonHandle> gameControllerButtonToButton;
	std::map<JoystickAxis, AxisHandle> joystickAxisToAxis;
	std::map<JoystickButton, ButtonHandle> joystickButtonToButton;

	AxisHandle nextAxisHandle;
	std::map<UString, AxisHandle> axisHandles;
	AxisHandle nextButtonHandle;
	std::map<UString, ButtonHandle> buttonHandles;

	std::vector<AxisState> axis;
	std::vector<WeightedButtonState> buttons;
	std::vector<bool> buttonsPressedDown;
	std::vector<bool> buttonsReleasedUp;

// \TODO: map source kb, mouse,gamepad,midi to names.
// \TODO: think about what different kinds of string->action there is and extra params for them!

public:
	VERSO_GFX_API explicit InputController(const UString& name);
	VERSO_GFX_API ~InputController();

	VERSO_GFX_API AxisHandle getAxisHandle(const UString& axisName);
	VERSO_GFX_API ButtonHandle getButtonHandle(const UString& buttonName);

	VERSO_GFX_API void processEvent(const Event& event);
	VERSO_GFX_API void resetBoth();
	VERSO_GFX_API void resetPressedDown();
	VERSO_GFX_API void resetReleasedUp();

	// Retrieve input status
	VERSO_GFX_API AxisState getAxisState(AxisHandle axisHandle) const;

	VERSO_GFX_API ButtonState getButtonState(ButtonHandle buttonHandle) const;
	VERSO_GFX_API WeightedButtonState getWeightedButtonState(ButtonHandle buttonHandle) const;
	VERSO_GFX_API bool getButtonPressedDown(ButtonHandle buttonHandle) const;
	VERSO_GFX_API bool getButtonReleasedUp(ButtonHandle buttonHandle) const;

	VERSO_GFX_API void setButtonState(ButtonHandle buttonHandle, WeightedButtonState state);
	VERSO_GFX_API void setButtonPressed(ButtonHandle buttonHandle, ButtonState pressed);


	// Events


	// Keyboard
	VERSO_GFX_API ButtonHandle mapKeyboardToButton(const UString& buttonName, KeyCode key, Keymodifiers modifiers = static_cast<Keymodifiers>(Keymodifier::NoneOrAny), bool alwaysNoneOrAnyNumCapsMode = true);

	// Mouse
	//VERSO_GFX_API AxisHandle mapMouseAxisToAxis(MouseAxis axis, const UString& axisName);
	VERSO_GFX_API ButtonHandle mapMouseButtonToButton(MouseButton mouseButton, const UString& buttonName);

	// Gamepad
	VERSO_GFX_API AxisHandle mapGameControllerButtonToAxis(GameControllerAxis gameControllerAxis, const UString& axisName);
	VERSO_GFX_API ButtonHandle mapGameControllerButtonToButton(GameControllerButton gameControllerButton, const UString& buttonName);

	// Joystick
	VERSO_GFX_API UString findAxisNameByAxisType(JoystickAxis joystickAxis);
	VERSO_GFX_API UString findButtonNameByButtonType(JoystickButton joystickButton);
	VERSO_GFX_API AxisHandle mapJoystickAxisToAxis(JoystickAxis joystickAxis, const UString& axisName);
	VERSO_GFX_API ButtonHandle mapJoystickButtonToButton(JoystickButton joystickButton, const UString& buttonName);
	VERSO_GFX_API ButtonHandle mapJoystickHatToButton(JoystickHat joystickHat, JoystickHatDirection joystickDirection, const UString& buttonName);
};


/*
class InputController
{
	public:
		inline InputController();
		inline virtual ~InputController();

		// Accessors (getters)
		// - Interface for keyboard like devices
		virtual inline bool getButtonState(const UString& buttonName) const;
		virtual inline bool getButtonPressed(const UString& buttonName) const;

		// - Interface for pointer like devices (mice etc.)
		virtual inline void getPointerCurrentPosition(double& x, double& y) const;
		virtual inline double getPointerCurrentPositionX() const;
		virtual inline double getPointerCurrentPositionY() const;

		virtual inline void getPointerLastPosition(double& x, double& y) const;
		virtual inline double getPointerLastPositionX() const;
		virtual inline double getPointerLastPositionY() const;

		virtual inline void getPointerRelativeMovement(
			double& x, double& y) const;
		virtual inline double getPointerRelativeMovementX() const;
		virtual inline double getPointerRelativeMovementY() const;

		virtual inline void getPointerRelativeMovementInsideWindow(
			double& x, double& y) const;
		virtual inline double getPointerRelativeMovementInsideWindowX() const;
		virtual inline double getPointerRelativeMovementInsideWindowY() const;

		virtual inline bool isPointerButtonPressed(
			const UString& buttonName, double& x, double& y) const;
		virtual inline bool isPointerButtonReleased(
			const UString& buttonName, double& x, double& y) const;
		virtual inline bool isPointerButtonClicked(
			const UString& buttonName, double& pressedX, double& pressedY,
			double& releasedX, double& releasedY) const;
		virtual inline bool isPointerButtonClickedInside(
			const UString& buttonName, double x1, double y1,
			double x2, double y2) const;


		// Mutators (setters)

		// - Interface for keyboard like devices
		virtual inline void setButtonState(const UString& buttonName, bool state);
		virtual inline void setButtonPressed(const UString& buttonName, bool pressed);

		// - Interface for pointer like devices (mice etc.)

		// - General methods
		virtual void clearPressedStates() {} // \TODO should this be abtract? = 0;
		virtual void clearAllStates() {} // \TODO should this be abtract? = 0;

	protected:

	private:
		InputController(const InputController&);
		InputController& operator = (const InputController&);
};*/


/*
InputController::InputController()
{
}


InputController::~InputController()
{
}


bool InputController::getButtonState(const UString&) const
{
	return false;
}


bool InputController::getButtonPressed(const UString&) const
{
	return false;
}


void InputController::getPointerCurrentPosition(double&, double&) const
{
}


double InputController::getPointerCurrentPositionX() const
{
	return 0.0f;
}


double InputController::getPointerCurrentPositionY() const
{
	return 0.0f;
}


void InputController::getPointerLastPosition(double&, double&) const
{
}


double InputController::getPointerLastPositionX() const
{
	return 0.0f;
}


double InputController::getPointerLastPositionY() const
{
	return 0.0f;
}


void InputController::getPointerRelativeMovementInsideWindow(double&, double&) const
{
}


double InputController::getPointerRelativeMovementInsideWindowX() const
{
	return 0.0f;
}


double InputController::getPointerRelativeMovementInsideWindowY() const
{
	return 0.0f;
}


void InputController::getPointerRelativeMovement(double&, double&) const
{
}


double InputController::getPointerRelativeMovementX() const
{
	return 0.0f;
}


double InputController::getPointerRelativeMovementY() const
{
	return 0.0f;
}


bool InputController::isPointerButtonPressed(
	const UString&, double&, double&) const
{
	return false;
}


bool InputController::isPointerButtonReleased(
	const UString&, double&, double&) const
{
	return false;
}


bool InputController::isPointerButtonClicked(
	const UString&, double&, double&,
	double&, double&) const
{
	return false;
}


bool InputController::isPointerButtonClickedInside(
	const UString&, double, double,
	double, double) const
{
	return false;
}


void InputController::setButtonState(const UString&, bool)
{
}


void InputController::setButtonPressed(const UString&, bool)
{
}*/


} // End namespace Verso

#endif // End header guard

