#ifndef VERSO_GFX_INPUT_WEIGHTEDBUTTONSTATE_HPP
#define VERSO_GFX_INPUT_WEIGHTEDBUTTONSTATE_HPP

#include <Verso/verso-gfx-common.hpp>
#include <Verso/Input/ButtonState.hpp>

namespace Verso {


struct WeightedButtonState
{
public:
	ButtonState state;
	float weight;

	WeightedButtonState() :
		state(ButtonState::Released),
		weight(0.0f)
	{
	}


	explicit WeightedButtonState(ButtonState buttonState) :
		state(buttonState),
		weight(0.0f)
	{
		if (buttonState == ButtonState::Pressed) {
			weight = 1.0f;
		}
	}


	WeightedButtonState(ButtonState buttonState, float weight) :
		state(buttonState),
		weight(weight)
	{
	}


public: // toString
	UString toString() const
	{
		UString s;
		s.append2(state);
		s += "(";
		s.append2(weight);
		s += ")";
		return s;
	}


	UString toStringDebug() const
	{
		UString str("WeightedButtonState(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const WeightedButtonState& right)
	{
		return ost << right.toStringDebug();
	}
};




inline ButtonState weightedButtonStateToButtonState(const WeightedButtonState& weightedButtonState)
{
	return weightedButtonState.state;
}


inline WeightedButtonState buttonStateToWeightedButtonState(const ButtonState& buttonState)
{
	WeightedButtonState result;
	result.state = buttonState;
	result.weight = 1.0f;
	return result;
}


} // End namespace Verso

#endif // End header guard

