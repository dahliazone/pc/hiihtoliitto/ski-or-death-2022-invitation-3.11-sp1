#ifndef VERSO_GFX_INPUT_MOUSE_MOUSEEVENTTYPE_HPP
#define VERSO_GFX_INPUT_MOUSE_MOUSEEVENTTYPE_HPP

#include <Verso/System/UString.hpp>
#include <ostream>

namespace Verso {


enum class MouseEventType : int8_t
{
	Unset = 0,
	Motion,
	Button,
	Wheel,
	Size
};


inline UString mouseEventTypeToString(const MouseEventType& eventType)
{
	if (eventType == MouseEventType::Unset) {
		return "Unset";
	}
	else if (eventType == MouseEventType::Motion) {
		return "Motion";
	}
	else if (eventType == MouseEventType::Button) {
		return "Button";
	}
	else if (eventType == MouseEventType::Wheel) {
		return "Wheel";
	}
	else if (eventType == MouseEventType::Size) {
		return "Size";
	}
	else {
		return "Unknown value";
	}
}


inline std::ostream& operator <<(std::ostream& ost, const MouseEventType& right)
{
	return ost << mouseEventTypeToString(right);
}


} // End namespace Verso

#endif // End header guard

