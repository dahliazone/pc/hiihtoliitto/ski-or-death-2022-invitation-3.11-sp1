#ifndef VERSO_GFX_INPUT_MOUSE_MOUSEAXIS_HPP
#define VERSO_GFX_INPUT_MOUSE_MOUSEAXIS_HPP

#include <Verso/System/UString.hpp>
#include <ostream>

namespace Verso {


enum class MouseAxis : int8_t
{
	None = 0,
	X,
	Y,
	WheelX,
	WheelY,
	Size,
	Unset
};


inline MouseAxis intToMouseAxis(int8_t axis)
{
	if (axis == static_cast<int8_t>(MouseAxis::None)) {
		return MouseAxis::None;
	}
	else if (axis == static_cast<int8_t>(MouseAxis::X)) {
		return MouseAxis::X;
	}
	else if (axis == static_cast<int8_t>(MouseAxis::Y)) {
		return MouseAxis::Y;
	}
	else if (axis == static_cast<int8_t>(MouseAxis::WheelX)) {
		return MouseAxis::WheelX;
	}
	else if (axis == static_cast<int8_t>(MouseAxis::WheelY)) {
		return MouseAxis::WheelY;
	}
	else {
		return MouseAxis::Unset;
	}
}


inline UString mouseAxisToString(const MouseAxis& axis)
{
	if (axis == MouseAxis::None) {
		return "None";
	}
	else if (axis == MouseAxis::X) {
		return "X";
	}
	else if (axis == MouseAxis::Y) {
		return "Y";
	}
	else if (axis == MouseAxis::WheelX) {
		return "WheelX";
	}
	else if (axis == MouseAxis::WheelY) {
		return "WheelY";
	}
	else if (axis == MouseAxis::Size) {
		return "Size";
	}
	else if (axis == MouseAxis::Unset) {
		return "Unset";
	}
	else {
		return "Unknown value";
	}
}


inline std::ostream& operator <<(std::ostream& ost, const MouseAxis& right)
{
	return ost << mouseAxisToString(right);
}


} // End namespace Verso

#endif // End header guard

