#ifndef VERSO_GFX_INPUT_MOUSE_MOUSEBUTTON_HPP
#define VERSO_GFX_INPUT_MOUSE_MOUSEBUTTON_HPP

#include <Verso/System/UString.hpp>
#include <ostream>

namespace Verso {


enum class MouseButton : int8_t
{
	None = 0,
	Left,
	Middle,
	Right,
	X1,
	X2,
	Size,
	Unset
};


inline MouseButton intToMouseButton(int8_t button)
{
	if (button == static_cast<int8_t>(MouseButton::None)) {
		return MouseButton::None;
	}
	else if (button == static_cast<int8_t>(MouseButton::Left)) {
		return MouseButton::Left;
	}
	else if (button == static_cast<int8_t>(MouseButton::Middle)) {
		return MouseButton::Middle;
	}
	else if (button == static_cast<int8_t>(MouseButton::Right)) {
		return MouseButton::Right;
	}
	else if (button == static_cast<int8_t>(MouseButton::X1)) {
		return MouseButton::X1;
	}
	else if (button == static_cast<int8_t>(MouseButton::X2)) {
		return MouseButton::X2;
	}
	else {
		return MouseButton::Unset;
	}
}


inline UString mouseButtonToString(const MouseButton& button)
{
	if (button == MouseButton::None) {
		return "None";
	}
	else if (button == MouseButton::Left) {
		return "Left";
	}
	else if (button == MouseButton::Middle) {
		return "Middle";
	}
	else if (button == MouseButton::Right) {
		return "Right";
	}
	else if (button == MouseButton::X1) {
		return "X1";
	}
	else if (button == MouseButton::X2) {
		return "X2";
	}
	else if (button == MouseButton::Size) {
		return "Size";
	}
	else if (button == MouseButton::Unset) {
		return "Unset";
	}
	else {
		return "Unknown value";
	}
}


inline std::ostream& operator <<(std::ostream& ost, const MouseButton& right)
{
	return ost << mouseButtonToString(right);
}


} // End namespace Verso

#endif // End header guard

