#ifndef VERSO_GFX_INPUT_BUTTONSTATE_HPP
#define VERSO_GFX_INPUT_BUTTONSTATE_HPP

#include <Verso/System/UString.hpp>
#include <cstdint>
#include <ostream>

namespace Verso {


enum class ButtonState : int8_t
{
	Released = 0,
	Pressed,
	Size,
	Unset
};


inline UString buttonStateToString(const ButtonState& state)
{
	if (state == ButtonState::Released) {
		return "Released";
	}
	else if (state == ButtonState::Pressed) {
		return "Pressed";
	}
	else if (state == ButtonState::Size) {
		return "Size";
	}
	else if (state == ButtonState::Unset) {
		return "Unset";
	}
	else {
		return "Unknown value";
	}
}


inline std::ostream& operator <<(std::ostream& ost, const ButtonState& state)
{
	return ost << buttonStateToString(state);
}


} // End namespace Verso

#endif // End header guard

