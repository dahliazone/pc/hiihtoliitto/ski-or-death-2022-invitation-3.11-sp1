#ifndef VERSO_GFX_INPUT_EVENTTYPE_HPP
#define VERSO_GFX_INPUT_EVENTTYPE_HPP

#include <Verso/System/UString.hpp>
#include <ostream>
#include <cstdint>

namespace Verso {


enum class EventType : int8_t
{
	Unset = 0,
	Quit,
	Closed,
	Resized,
	GainedFocus,
	LostFocus,
	Keyboard,
	Mouse,
	GameController,
	Joystick,
	Size,
};


inline UString eventTypeToString(const EventType& eventType)
{
	if (eventType == EventType::Quit) {
		return "Quit";
	}
	else if (eventType == EventType::Closed) {
		return "Closed";
	}
	else if (eventType == EventType::Resized) {
		return "Resized";
	}
	else if (eventType == EventType::GainedFocus) {
		return "GainedFocus";
	}
	else if (eventType == EventType::LostFocus) {
		return "LostFocus";
	}
	else if (eventType == EventType::Keyboard) {
		return "Keyboard";
	}
	else if (eventType == EventType::Mouse) {
		return "Mouse";
	}
	else if (eventType == EventType::GameController) {
		return "GameController";
	}
	else if (eventType == EventType::Joystick) {
		return "Joystick";
	}
	else if (eventType == EventType::Size) {
		return "Size";
	}
	else if (eventType == EventType::Unset) {
		return "Unset";
	}
	else {
		return "Unknown value";
	}
}


inline std::ostream& operator <<(std::ostream& ost, const EventType& right)
{
	return ost << eventTypeToString(right);
}


} // End namespace Verso

#endif // End header guard

