#ifndef VERSO_GFX_INPUT_EVENT_HPP
#define VERSO_GFX_INPUT_EVENT_HPP

#include <Verso/Input/Keyboard/KeyboardEvent.hpp>
#include <Verso/Input/Joystick/JoystickEvent.hpp>
#include <Verso/Input/GameController/GameControllerEvent.hpp>
#include <Verso/Input/Mouse/MouseEvent.hpp>
#include <Verso/Input/EventType.hpp>
#include <Verso/Math/Vector2.hpp>

namespace Verso {


struct Event
{
	EventType type;

	// Only defined for type == Closed

	// Only defined for type == Resized
	Vector2i resolution;

	// Only defined for type == LostFocus,

	// Only defined for type == GainedFocus

	// Only defined for type == Keyboard
	KeyboardEvent keyboard;

	// Only defined for type == Mouse
	MouseEvent mouse;

	// Only defined for type == GameController
	GameControllerEvent gameController;

	// Only defined for type == Joystick
	JoystickEvent joystick;

public:
	Event() :
		type(EventType::Unset),
		resolution(),
		keyboard(),
		mouse()
	{
	}

public: // toString
	UString toString() const
	{
		UString s;
		if (type == EventType::Closed) {
			s += "Closed";
		}
		else if (type == EventType::Resized) {
			s += "Resized => ";
			s += resolution.toString();
		}
		else if (type == EventType::Keyboard) {
			s += keyboard.toStringDebug();
		}
		else if (type == EventType::Mouse) {
			s += mouse.toStringDebug();
		}
		else if (type == EventType::GameController) {
			s += gameController.toStringDebug();
		}
		else if (type == EventType::Joystick) {
			s += joystick.toStringDebug();
		}
		return s;
	}


	UString toStringDebug() const
	{
		UString str("Event(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const Event& right)
	{
		return ost << right.toStringDebug();
	}
};


} // End namespace Verso

#endif // End header guard

