#ifndef VERSO_GFX_INPUT_JOYSTICK_JOYSTICKHAT_HPP
#define VERSO_GFX_INPUT_JOYSTICK_JOYSTICKHAT_HPP

#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>
#include <ostream>

namespace Verso {


enum class JoystickHat : int8_t
{
	Hat1 = 0,
	Hat2,
	Hat3,
	Hat4,
	Hat5,
	Hat6,
	Hat7,
	Hat8,
	Hat9,
	Hat10,
	Hat11,
	Hat12,
	Size,
	Unset
};


inline UString joystickHatToString(const JoystickHat& hat)
{
	if (hat >= JoystickHat::Hat1 && hat < JoystickHat::Size) {
		UString s = "Hat ";
		s.append2(static_cast<int>(hat));
		return s;
	}
	else if (hat == JoystickHat::Size) {
		return "Size";
	}
	else if (hat == JoystickHat::Unset) {
		return "Unset";
	}
	else {
		UString error("Invalid JoystickHat(");
		error.append2(static_cast<int>(hat));
		error += ") given.";
		VERSO_ILLEGALPARAMETERS("verso-gfx", error.c_str(), "");
	}
}


inline std::ostream& operator <<(std::ostream& ost, const JoystickHat& right)
{
	return ost << joystickHatToString(right);
}


} // End namespace Verso

#endif // End header guard

