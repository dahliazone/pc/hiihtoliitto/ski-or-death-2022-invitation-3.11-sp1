#ifndef VERSO_GFX_INPUT_JOYSTICK_JOYSTICKTRACKBALL_HPP
#define VERSO_GFX_INPUT_JOYSTICK_JOYSTICKTRACKBALL_HPP

#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>
#include <ostream>

namespace Verso {


enum class JoystickTrackball : int8_t
{
	Trackball1 = 0,
	Trackball2,
	Trackball3,
	Trackball4,
	Trackball5,
	Trackball6,
	Trackball7,
	Trackball8,
	Trackball9,
	Trackball10,
	Trackball11,
	Trackball12,
	Size,
	Unset
};


inline UString joystickTrackballToString(const JoystickTrackball& trackball)
{
	if (trackball >= JoystickTrackball::Trackball1 && trackball < JoystickTrackball::Size) {
		UString s = "Trackball ";
		s.append2(static_cast<int>(trackball));
		return s;
	}
	else if (trackball == JoystickTrackball::Size) {
		return "Size";
	}
	else if (trackball == JoystickTrackball::Unset) {
		return "Unset";
	}
	else {
		UString error("Invalid JoystickTrackball(");
		error.append2(static_cast<int>(trackball));
		error += ") given";
		VERSO_ILLEGALPARAMETERS("verso-gfx", error.c_str(), "");
	}
}


inline std::ostream& operator <<(std::ostream& ost, const JoystickTrackball& right)
{
	return ost << joystickTrackballToString(right);
}


} // End namespace Verso

#endif // End header guard

