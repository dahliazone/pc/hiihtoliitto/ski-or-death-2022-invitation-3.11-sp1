#ifndef VERSO_GFX_INPUT_JOYSTICK_JOYSTICKEVENTTYPE_HPP
#define VERSO_GFX_INPUT_JOYSTICK_JOYSTICKEVENTTYPE_HPP

#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>
#include <cstdint>
#include <ostream>

namespace Verso {


enum class JoystickEventType : int8_t
{
	Unset = 0,
	AxisMotion,
	TrackballMotion,
	HatMotion,
	Button,
	Size
};


inline UString joystickEventTypeToString(const JoystickEventType& eventType)
{
	if (eventType == JoystickEventType::Unset) {
		return "Unset";
	}
	else if (eventType == JoystickEventType::AxisMotion) {
		return "AxisMotion";
	}
	else if (eventType == JoystickEventType::TrackballMotion) {
		return "TrackballMotion";
	}
	else if (eventType == JoystickEventType::HatMotion) {
		return "HatMotion";
	}
	else if (eventType == JoystickEventType::Button) {
		return "Button";
	}
	else if (eventType == JoystickEventType::Size) {
		return "Size";
	}
	else {
		UString error("Invalid JoystickEventType(");
		error.append2(static_cast<int8_t>(eventType));
		error += ") given.";
		VERSO_ILLEGALPARAMETERS("verso-gfx", error.c_str(), "");
	}
}


inline std::ostream& operator <<(std::ostream& ost, const JoystickEventType& right)
{
	return ost << joystickEventTypeToString(right);
}


} // End namespace Verso

#endif // End header guard

