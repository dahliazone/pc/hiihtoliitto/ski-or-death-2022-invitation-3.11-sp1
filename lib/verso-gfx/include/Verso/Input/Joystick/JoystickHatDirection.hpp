#ifndef VERSO_GFX_INPUT_JOYSTICK_JOYSTICKHATDIRECTION_HPP
#define VERSO_GFX_INPUT_JOYSTICK_JOYSTICKHATDIRECTION_HPP

#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>
#include <ostream>

namespace Verso {


enum class JoystickHatDirection : uint8_t
{
	LeftUp = 1,
	Up,
	RightUp,
	Left,
	Centered,
	Right,
	LeftDown,
	Down,
	RightDown,
	Size,
	Unset
};


inline UString joystickHatDirectionToString(const JoystickHatDirection& hatDirection)
{
	switch (hatDirection) {
	case JoystickHatDirection::LeftUp:
		return "Left Up";
	case JoystickHatDirection::Up:
		return "Up";
	case JoystickHatDirection::RightUp:
		return "Right Up";
	case JoystickHatDirection::Left:
		return "Left";
	case JoystickHatDirection::Centered:
		return "Centered";
	case JoystickHatDirection::Right:
		return "Right";
	case JoystickHatDirection::LeftDown:
		return "Left Down";
	case JoystickHatDirection::Down:
		return "Down";
	case JoystickHatDirection::RightDown:
		return "Right Down";
	case JoystickHatDirection::Size:
		return "Size";
	case JoystickHatDirection::Unset:
		return "Unset";
	default:
	{
		UString error("Invalid JoystickHatDirection(");
		error.append2(static_cast<unsigned int>(hatDirection));
		error += ") given.";
		VERSO_ILLEGALPARAMETERS("verso-gfx", error.c_str(), "");
	}
	}
}


inline std::ostream& operator <<(std::ostream& ost, const JoystickHatDirection& right)
{
	return ost << joystickHatDirectionToString(right);
}


} // End namespace Verso

#endif // End header guard

