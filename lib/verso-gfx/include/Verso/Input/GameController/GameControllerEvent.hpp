#ifndef VERSO_GFX_INPUT_GAMECONTROLLER_GAMECONTROLLEREVENT_HPP
#define VERSO_GFX_INPUT_GAMECONTROLLER_GAMECONTROLLEREVENT_HPP

#include <Verso/verso-gfx-common.hpp>
#include <Verso/Input/GameController/GameControllerAxis.hpp>
#include <Verso/Input/GameController/GameControllerButton.hpp>
#include <Verso/Input/GameController/GameControllerEventType.hpp>
#include <Verso/Input/AxisState.hpp>
#include <Verso/Input/ButtonState.hpp>

namespace Verso {


struct GameControllerEvent
{
	GameControllerEventType type;
	uint32_t timestamp;
	int32_t joystickId;

	// Only defined for type == AxisMotion
	GameControllerAxis axis;
	AxisState value; // the axis value (range: -1 to 1) // \TODO: int16_t (range: -32768 to 32767)

	// Only defined for type == Button
	GameControllerButton button;
	ButtonState state;

public: // toString
	UString toString() const
	{
		UString s = gameControllerEventTypeToString(type)+", timestamp=";
		s.append2(timestamp);
		s += ", joystickId=";
		s.append2(joystickId);

		if (type == GameControllerEventType::AxisMotion) {
			s += ", "+gameControllerAxisToString(axis);
			s += "=";
			s.append2(value);
		}

		else if (type == GameControllerEventType::Button) {
			s += ", button="+gameControllerButtonToString(button)+", state="+buttonStateToString(state);
		}
		return s;
	}


	UString toStringDebug() const
	{
		UString str("GameControllerEvent(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const GameControllerEvent& event)
	{
		return ost << event.toStringDebug();
	}
};


} // End namespace Verso

#endif // End header guard

