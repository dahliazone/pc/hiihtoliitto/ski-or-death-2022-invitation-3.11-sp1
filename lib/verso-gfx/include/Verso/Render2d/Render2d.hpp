#ifndef VERSO_GFX_RENDER2D_RENDER2D_HPP
#define VERSO_GFX_RENDER2D_RENDER2D_HPP

#include <Verso/Display/Window2d.hpp>
#include <Verso/Math/RgbaColorf.hpp>

namespace Verso {


class Render2d
{
public: // static
	VERSO_GFX_API static void clear(const Window2d& window, const RgbaColorf& color);
	VERSO_GFX_API static void drawRectangle(const Window2d& window, const Recti& rect, const RgbaColorf& color);
	VERSO_GFX_API static void fillRectangle(const Window2d& window, const Recti& rect, const RgbaColorf& color);
};


} // End namespace Verso

#endif // End header guard

