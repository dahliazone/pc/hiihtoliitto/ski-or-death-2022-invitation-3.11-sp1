#ifndef VERSO_GFX_SYSTEM_CONSOLEREDIRECTWIN32_HPP
#define VERSO_GFX_SYSTEM_CONSOLEREDIRECTWIN32_HPP
#ifdef _WIN32

#include <Verso/verso-gfx-common.hpp>

namespace Verso {
namespace priv {


VERSO_GFX_API void redirectToExistingConsoleWin32();


} // End namespace priv
} // End namespace Verso

#endif // End _WIN32
#endif // End header guard
