#ifndef VERSO_GFX_SYSTEM_MAIN_HPP
#define VERSO_GFX_SYSTEM_MAIN_HPP

// Header required to include for generating Windows main function

#if _MSC_VER && !__INTEL_COMPILER
#include <SDL_main.h>
#endif


#endif // End header guard

