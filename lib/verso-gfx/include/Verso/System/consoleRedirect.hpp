#ifndef VERSO_GFX_SYSTEM_CONSOLEREDIRECT_HPP
#define VERSO_GFX_SYSTEM_CONSOLEREDIRECT_HPP

#include <Verso/System/win32/consoleRedirectWin32.hpp>

namespace Verso {


void redirectToExistingConsole()
{
#ifdef _WIN32
	priv::redirectToExistingConsoleWin32();
#endif
}


} // End namespace Verso

#endif // End header guard
