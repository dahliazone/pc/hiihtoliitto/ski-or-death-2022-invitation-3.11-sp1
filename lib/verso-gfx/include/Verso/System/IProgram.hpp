#ifndef VERSO_GFX_SYSTEM_IPROGRAM_HPP
#define VERSO_GFX_SYSTEM_IPROGRAM_HPP

#include <Verso/Time/FrameTimestamp.hpp>
#include <Verso/Display/IWindow.hpp>
#include <Verso/Input/Event.hpp>

namespace Verso {


class IProgram
{
public:
	virtual ~IProgram() = default;
	virtual void create(const IWindow& window) = 0;
	virtual void destroy() VERSO_NOEXCEPT = 0;
	virtual void handleInput(const FrameTimestamp& frameTimestamp, const IWindow& window) = 0;
	virtual void handleEvent(const Event& event, const FrameTimestamp& frameTimestamp, const IWindow& window) = 0;
	virtual void render(const FrameTimestamp& frameTimestamp, const IWindow& window) = 0;
	virtual bool isQuitting() const = 0;
	virtual void quit() = 0;
};


} // End namespace Verso

#endif // End header guard

