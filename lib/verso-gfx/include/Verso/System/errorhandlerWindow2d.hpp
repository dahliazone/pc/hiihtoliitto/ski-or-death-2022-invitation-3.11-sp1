#ifndef VERSO_GFX_SYSTEM_ERRORHANDLERWINDOW2D_HPP
#define VERSO_GFX_SYSTEM_ERRORHANDLERWINDOW2D_HPP

#include <Verso/verso-gfx-common.hpp>
#include <Verso/System/UString.hpp>

namespace Verso {


VERSO_GFX_API void errorHandlerWindow2dEnable(const UString& title);


} // End namespace Verso

#endif // End header guard

