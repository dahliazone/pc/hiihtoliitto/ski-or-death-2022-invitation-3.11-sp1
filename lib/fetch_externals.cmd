PATH=%PATH%;"C:\Program Files\SlikSvn\bin";"C:\Program Files (x86)\SlikSvn\bin";


echo "Partakone: Fetching externals from repositories..."

IF NOT EXIST ..\data\gui\iconsttf (
	git clone https://gitlab.com/dahliazone/pc/extlib/IconFontCppHeaders-ttf.git ..\data\gui\iconsttf
) else (
	pushd ..\data\gui\iconsttf
	git pull --rebase
	popd
)

IF NOT EXIST ..\data\gui\icons (
	git clone https://gitlab.com/dahliazone/pc/extlib/material-design-icons-json.git ..\data\gui\icons
) else (
	pushd ..\data\gui\icons
	git pull --rebase
	popd
)

IF NOT EXIST IconFontCppHeaders  (
	git clone https://gitlab.com/dahliazone/pc/extlib/IconFontCppHeaders.git IconFontCppHeaders
) else (
	pushd IconFontCppHeaders
	git pull --rebase
	popd
)

IF NOT EXIST verso-depthio (
	git clone https://gitlab.com/dahliazone/pc/lib/verso-depthio.git verso-depthio
) else (
	pushd verso-depthio
	git pull --rebase
	popd
)

echo "verso-depthio: Resursing into..."
pushd verso-depthio
call fetch_parent_externals.cmd
popd

echo "Partakone: All done"

