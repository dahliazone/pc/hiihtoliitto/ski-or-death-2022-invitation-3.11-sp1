#include <Verso/GrinderKit/GrinderKitSetupDialog.hpp>
#include <Verso/System/errorhandlerWindowOpengl.hpp>
#include <Verso/System/errorhandlerConsole.hpp>
#include <Verso/Display/WindowOpengl.hpp>
#include <Verso/Time/FrameTimer.hpp>
#include <Verso/Time/RecordFrameTimer.hpp>
#include <Verso/Render/Render.hpp>
#include <Verso/Render/RenderStats.hpp>
#include <Verso/GrinderKit/GrinderKitDemo.hpp>
#include <Verso/Audio/Audio2dBass.hpp>
#include <Verso/System/InitWrapperSdl2.hpp>
#include <Verso/System/consoleRedirect.hpp>
#include "PartakoneFactory.hpp"
#include "jsontest.hpp"

using namespace std;
using namespace Verso;


bool run(const GrinderKitSetupDialog& setupDialog, WindowOpengl& window, IGrinderKitApp& program)
{
	VERSO_LOG_INFO("demo/SkiOrDeath", "Demo - Verso-3d create (only output) ==================================================");

	// Apply settings from SetupDialog and create window
	ContextSettings contextSettings = ContextSettings(3, 3);
	VERSO_LOG_INFO_VARIABLE("demo/SkiOrDeath", "Requested", contextSettings.toStringDebug());

	WindowStyles windowStyles = 0; //static_cast<WindowStyles>(WindowStyle::Hidden);
	if (setupDialog.fullscreen == true) {
		if (setupDialog.customResolution == false) {
			windowStyles |= static_cast<WindowStyles>(WindowStyle::FullscreenDesktop);
		}
		else {
			windowStyles |= static_cast<WindowStyles>(WindowStyle::Fullscreen);
		}
	}
	else {
		windowStyles |= static_cast<WindowStyles>(WindowStyle::Resizable);
	}

	// \TODO: option for WindowStyle::AllowHighDpi

	SwapInterval swapInterval = SwapInterval::AdaptiveVsync;
	if (setupDialog.vsync == false) {
		swapInterval = SwapInterval::Immediate;
	}

	UString title("Ski or Death 2022 Invitation 3.11 SP1");
	window.create(*setupDialog.displayMode, *setupDialog.display, title, windowStyles,
				  *setupDialog.downscaleRatio, setupDialog.demoPlayerSettings.general.contentAspectRatio,
				  setupDialog.demoPlayerSettings.general.pixelAspectRatio,
				  contextSettings, swapInterval, setupDialog.recordResolution);
	GL_CHECK_FOR_ERRORS("", "");

	contextSettings = window.getSettings();
	GL_CHECK_FOR_ERRORS("", "");
	VERSO_LOG_INFO_VARIABLE("demo/SkiOrDeath", "Got", contextSettings.toStringDebug());

	Opengl::printInfo(false);
	GL_CHECK_FOR_ERRORS("", "");

	if (setupDialog.fullscreen == false) {
		window.setPositionAlignedOnDisplay(*setupDialog.display, Align(HAlign::Left, VAlign::Top));
	}
	GL_CHECK_FOR_ERRORS("", "");

	InputManager::instance().create();

	window.show();
	GL_CHECK_FOR_ERRORS("", "");

	window.makeActive();
	Render::clearScreen(ColorGenerator::getColor(ColorRgb::Black), true, true);
	window.swapScreenBuffer();

	// Setup audio
	Audio2dBass& audio2d = Audio2dBass::instance();
	int audioDeviceId = -1;
	if (setupDialog.audioDevice != nullptr) {
		audioDeviceId = setupDialog.audioDevice->deviceId;
	}

	audio2d.create(setupDialog.muteAudio, audioDeviceId);

	program.create(window, audio2d);
	GL_CHECK_FOR_ERRORS("", "");

	double refreshRate = 0;
	if (setupDialog.vsync == false && setupDialog.unrestrictedFps == false) {
		refreshRate = static_cast<double>(setupDialog.displayMode->refreshRate.hz); // \TODO: use displayMode.refreshRate or separate?
	}

	UString recordPath = "record/";
	if (setupDialog.record == true) {
		File::mkdir(recordPath);
	}

	FrameTimer realtimeFrameTimer(refreshRate);
	RecordFrameTimer recordFrameTimer(program.getDemoPlayerSettings().general.targetFps);

	while (!program.isQuitting()) {
		FrameTimestamp frameTime;
		if (setupDialog.record == false) {
			realtimeFrameTimer.update();
			frameTime = FrameTimestamp(realtimeFrameTimer);
		}
		else {
			recordFrameTimer.update();
			frameTime = FrameTimestamp(recordFrameTimer);
		}

		Event event;
		while (window.pollEvent(event)) {
			if (event.type == EventType::Closed) {
				program.quit();
				continue;
			}

			else if (event.type == EventType::Quit) {
				program.quit();
				continue;
			}

			else if (event.type == EventType::Keyboard) {
				KeyboardEvent& kb = event.keyboard;
				if (kb.type == KeyboardEventType::Key) {
					if (kb.key.keyCode == KeyCode::Escape) {
						program.quit();
						continue;
					}
				}
			}

			else if (event.type == EventType::Resized) {
				window.onResize();
				continue;
			}

			program.handleEvent(window, frameTime, event);
		}
		program.handleInput(window, frameTime);

		window.makeActive();
		program.render(window, frameTime);
		window.swapScreenBuffer();

		if (setupDialog.record == true) {
			UString fileName(recordPath + "frame-");
			std::ostringstream ss;
			ss << std::setw(8) << std::setfill('0') << recordFrameTimer.getRenderFrame();
			fileName.append(ss.str());
			fileName.append("." + imageSaveFormatToFileExtension(setupDialog.recordSaveFormat));
			program.saveRenderFboToFile(fileName, setupDialog.recordSaveFormat);
		}

		if (setupDialog.record == false) {
			realtimeFrameTimer.addRenderFrame();
		}
		else {
			recordFrameTimer.addRenderFrame();
		}

		RenderStats::reset();
	}

	program.destroy();
	Audio2dBass::instance().destroy();
	InputManager::instance().destroy();

	window.close();

	InitWrapperSdl2::instance().destroy();

	return true;
}


int main(int argc, char* argv[])
{
	(void)argc; (void)argv;

	redirectToExistingConsole();

	setTerminateHandlerWindowOpengl("SkiOrDeath crashed!");

	// Do some testing for new json library
	//jsontest();

//	try {
		VERSO_LOG_INFO("demo/SkiOrDeath", "Setup dialog =================================================");
		PartakoneFactory factory;
		GrinderKitSetupDialog setupDialog(&factory, "data/grinderkit.json", "SkiOrDeath");
		if (setupDialog.run(argc, argv) == false) {
			InitWrapperSdl2::instance().destroy();
			return EXIT_SUCCESS;
		}

		GrinderKitDemo program(&factory, setupDialog.sourceFileName);

		WindowOpengl window("demo/SkiOrDeath");
		if (run(setupDialog, window, program) == false) {
			return EXIT_FAILURE;
		}
//	}
//	catch (...) { // Loses original stack trace information at least on OS X
//		runTerminateHandlerWindowOpengl("SkiOrDeath crashed!");
//	}

	return EXIT_SUCCESS;
}

