#ifndef PARTAKONE_JSONTEST_HPP
#define PARTAKONE_JSONTEST_HPP

#include <nlohmann/json.hpp>


inline void jsontest()
{
	// create an empty structure (null)
	nlohmann::json j;

	// add a number that is stored as double (note the implicit conversion of j to an object)
	j["pi"] = 3.141;

	// add a Boolean that is stored as bool
	j["happy"] = true;

	// add a string that is stored as std::string
	j["name"] = "Niels";

	// add another null object by passing nullptr
	j["nothing"] = nullptr;

	// add an object inside the object
	j["answer"]["everything"] = 42;

	// add an array that is stored as std::vector (using an initializer list)
	j["list"] = { 1, 0, 2 };

	// add another object (using an initializer list of pairs)
	j["object"] = { {"currency", "USD"}, {"value", 42.99} };

	// instead, you could also write (which looks very similar to the JSON above)
	nlohmann::json j2 = {
	  {"pi", 3.141},
	  {"happy", true},
	  {"name", "Niels"},
	  {"nothing", nullptr},
	  {"answer", {
		{"everything", 42}
	  }},
	  {"list", {1, 0, 2}},
	  {"object", {
		{"currency", "USD"},
		{"value", 42.99}
	  }}
	};


	// create object from string literal
	nlohmann::json js = "{ \"happy\": true, \"pi\": 3.141 }"_json;

	// or even nicer with a raw string literal
	auto js2 = R"(
	  {
		"happy": true,
		"pi": 3.141
	  }
	)"_json;

	std::string s = js2.dump();    // {"happy":true,"pi":3.141}

	// serialization with pretty printing
	// pass in the amount of spaces to indent
	std::cout << js2.dump(4) << std::endl;


//	// read a JSON file
//	std::ifstream i("data/grinderkit.json");
//	nlohmann::json jss;
//	i >> jss;

//	// write prettified JSON to another file
//	std::ofstream o("data/grinderkit-pretty.json");
//	o << std::setw(4) << jss << std::endl;
}


#endif // End header guard

