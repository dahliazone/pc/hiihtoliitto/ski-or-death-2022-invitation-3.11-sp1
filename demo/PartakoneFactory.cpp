#include "PartakoneFactory.hpp"
#include <Verso/GrinderKit/Verso3dDemoPartFactory.hpp>
#include <Verso/GrinderKit/DemoPaths.hpp>

namespace Verso {


PartakoneFactory::PartakoneFactory()
{
}


PartakoneFactory::~PartakoneFactory()
{
}


Verso::DemoPart* PartakoneFactory::instance(const Verso::DemoPaths* demoPaths, const JSONObject& demoPartJson,
											const Verso::UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator) const
{
	using namespace Verso;
	UString type = JsonHelper::readString(demoPartJson, currentPathJson, "type", true);

	//if (type == "faerjan") {
	//	return new Verso::Faerjan(demoPaths, demoPartJson, currentPathJson, jsonOrder, startAccumulator);
	//}

	//else {
		Verso3dDemoPartFactory factory;
		return factory.instance(demoPaths, demoPartJson, currentPathJson, jsonOrder, startAccumulator);
	//}
}


} // End namespace Verso

