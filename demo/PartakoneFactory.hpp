#ifndef PARTAKONE_PARTAKONEFACTORY_HPP
#define PARTAKONE_PARTAKONEFACTORY_HPP

#include <Verso/GrinderKit/IDemoPartFactory.hpp>

namespace Verso {


class PartakoneFactory : public Verso::IDemoPartFactory
{
public:
	PartakoneFactory();
	virtual ~PartakoneFactory() override;

public:
	virtual Verso::DemoPart* instance(
			const Verso::DemoPaths* demoPaths, const JSONObject& demoPartJson,
			const Verso::UString& currentPathJson, std::int32_t jsonOrder, double startAccumulator) const override;
};


} // End namespace Verso

#endif // End header guard

