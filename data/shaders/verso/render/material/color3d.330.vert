#version 330 core
precision highp float;

in vec3 position;
in vec2 uv;
in vec3 normal;
in vec3 color;

out vec2 ex_Uv;
out vec3 ex_Normal;
out vec3 ex_Color;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;


void main()
{
	gl_Position = projection * view * model * vec4(position, 1.0);
	ex_Uv = uv;
	ex_Color = color;
}
