#version 330 core
precision mediump float;

layout(triangles) in;

// Three lines will be generated: 6 vertices
layout(line_strip, max_vertices=6) out;

in Vertex
{
  vec4 normal;
  vec4 color;
} vertex[];

out vec4 vertexColor;

uniform float normalLength;
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;


void main()
{
    int i;
    for (i=0; i<gl_in.length(); i++) {
        vec3 P = gl_in[i].gl_Position.xyz;
        vec3 N = vertex[i].normal.xyz;

        gl_Position = projection * view * model * vec4(P, 1.0);
        vertexColor = vertex[i].color;
        EmitVertex();

        gl_Position = projection * view * model * vec4(P + N * normalLength, 1.0);

        vertexColor = vertex[i].color;
        EmitVertex();

        EndPrimitive();
    }
}
