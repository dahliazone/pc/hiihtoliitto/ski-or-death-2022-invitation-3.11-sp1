#version 330 core
precision mediump float;

in vec4 vertexColor;
out vec4 fragColor;


void main()
{
  fragColor = vertexColor;
}

