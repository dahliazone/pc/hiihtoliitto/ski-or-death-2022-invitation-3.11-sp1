#version 330 core
precision highp float;

in vec2 position;
in vec2 uv;

out vec2 ex_Uv;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform vec2 uvOffset;
uniform vec2 meshOffset;
uniform vec2 uvScale;


void main()
{
	gl_Position = projection * view * model * vec4(position - meshOffset, 0.0, 1.0);
	ex_Uv = uvOffset + uv * uvScale;
}


