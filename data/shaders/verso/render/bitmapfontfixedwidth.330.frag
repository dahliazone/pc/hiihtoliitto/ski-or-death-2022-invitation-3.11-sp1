#version 330 core
precision mediump float;

in vec2 ex_Uv;
in vec3 ex_Normal;
out vec4 fragColor;

struct Material {
	sampler2D diffuseTexture;
};

uniform Material material;
uniform float alpha;

void main()
{
	vec4 diffuse = texture(material.diffuseTexture, ex_Uv);
	fragColor = vec4(diffuse.rgb, diffuse.a * alpha);
}
