#version 330 core
precision highp float;

in vec3 position;
in vec2 uv;

out vec2 ex_Uv;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform vec3 meshOffset;


void main()
{
	gl_Position = projection * view * model * vec4(position - meshOffset, 1.0);
	ex_Uv = uv;
}

