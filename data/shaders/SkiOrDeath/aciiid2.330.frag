#version 330 core
precision highp float;

uniform vec3      iResolution;           // viewport resolution (in pixels)
uniform float     iTime;                 // shader playback time (in seconds)
uniform float     iTimeDelta;            // render time (in seconds)
uniform int       iFrame;                // shader playback frame
uniform float     iChannelTime[4];       // channel playback time (in seconds)
uniform vec3      iChannelResolution[4]; // channel resolution (in pixels)
uniform vec4      iMouse;                // mouse pixel coords. xy: current (if MLB down), zw: click
uniform sampler2D iChannel0;             // input channel. XX = 2D/Cube
uniform sampler2D iChannel1;             // input channel. XX = 2D/Cube
uniform sampler2D iChannel2;             // input channel. XX = 2D/Cube
uniform sampler2D iChannel3;             // input channel. XX = 2D/Cube
uniform vec4      iDate;                 // (year, month, day, time in seconds)
uniform float     iSampleRate;           // sound sample rate (i.e., 44100)
uniform float     alpha;                 // alpha value

in vec2 fragCoord;
out vec4 fragColor;

#define RGB(r,g,b) (vec3(r,g,b) / 255.0);
vec3 palette[32];

void InitPalette()
{
	palette[ 0] = RGB(84,254,254);
	palette[ 1] = RGB(84,254,254);
	palette[ 2] = RGB(84,254,254);
	palette[ 4] = RGB(84,254,254);
	palette[15] = RGB(84,254,254);
	palette[ 6] = RGB(84,254,254);
	palette[ 3] = RGB(84,254,254);
	palette[ 7] = RGB(254,254,84);
	palette[ 8] = RGB(84,254,254);
	palette[ 9] = RGB(84,254,254);
	palette[10] = RGB(84,254,254);
	palette[11] = RGB(84,254,254);
	palette[12] = RGB(84,254,254);
	palette[13] = RGB(84,254,254);
	palette[14] = RGB(84,254,254);
	palette[ 5] = RGB(254,254,254);

	palette[ 0+16] = RGB(84,254,254);
	palette[ 1+16] = RGB(84,84,84);
	palette[ 2+16] = RGB(84,84,254);
	palette[ 4+16] = RGB(84,254,84);
	palette[15+16] = RGB(84,254,254);
	palette[ 6+16] = RGB(254,84,84);
	palette[ 3+16] = RGB(254,84,254);
	palette[ 7+16] = RGB(254,254,84);
	palette[ 8+16] = RGB(0,0,168);
	palette[ 9+16] = RGB(0,168,0);
	palette[10+16] = RGB(0,168,168);
	palette[11+16] = RGB(168,0,0);
	palette[12+16] = RGB(168,0,168);
	palette[13+16] = RGB(168,0,168);
	palette[14+16] = RGB(168,168,168);
	palette[ 5+16] = RGB(254,254,254);
}

void main()
{
	InitPalette();

	vec3 color;
	for (int colorIndex = 0; colorIndex < 3; colorIndex++){
		vec2 uv = (fragCoord*50.0-iResolution.xy)/iResolution.y;
		for (int i = 0; i < 3; i++)
		{
			uv /= 1.5;
			uv += sin(color.yx);
			uv += float(i) + cos(uv.x) * sin(uv.y) + cos(iTime) * sin(uv.y) + cos(uv.y) * sin(iTime);
		}
		color[colorIndex] = sin(iTime + uv.x + uv.y);
	}
	fragColor = vec4(palette[int((color.x + color.y + color.z) / 3.0 * 15.0)+16], alpha);
}
