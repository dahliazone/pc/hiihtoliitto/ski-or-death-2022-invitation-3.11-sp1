#version 330 core
precision highp float;

uniform vec3      iResolution;           // viewport resolution (in pixels)
uniform float     iTime;                 // shader playback time (in seconds)
uniform float     iTimeDelta;            // render time (in seconds)
uniform int       iFrame;                // shader playback frame
uniform float     iChannelTime[4];       // channel playback time (in seconds)
uniform vec3      iChannelResolution[4]; // channel resolution (in pixels)
uniform vec4      iMouse;                // mouse pixel coords. xy: current (if MLB down), zw: click
uniform sampler2D iChannel0;             // input channel. XX = 2D/Cube
uniform sampler2D iChannel1;             // input channel. XX = 2D/Cube
uniform sampler2D iChannel2;             // input channel. XX = 2D/Cube
uniform sampler2D iChannel3;             // input channel. XX = 2D/Cube
uniform vec4      iDate;                 // (year, month, day, time in seconds)
uniform float     iSampleRate;           // sound sample rate (i.e., 44100)
uniform float     alpha;                 // alpha value

in vec2 fragCoord;
out vec4 fragColor;

#define RGB(r,g,b) (vec3(r,g,b) / 255.0);
#define PI 3.1415926535897932384626433832795
vec3 palette[33];

void InitPalette()
{
	palette[ 0] = RGB(84,254,254);
	palette[ 1] = RGB(84,84,84);
	palette[ 2] = RGB(84,84,254);
	palette[ 4] = RGB(84,254,84);
	palette[15] = RGB(84,254,254);
	palette[ 6] = RGB(254,84,84);
	palette[ 3] = RGB(254,84,254);
	palette[ 7] = RGB(254,254,84);
	palette[ 8] = RGB(0,0,168);
	palette[ 9] = RGB(0,168,0);
	palette[10] = RGB(0,168,168);
	palette[11] = RGB(168,0,0);
	palette[12] = RGB(168,0,168);
	palette[13] = RGB(168,0,168);
	palette[14] = RGB(168,168,168);
	palette[ 5] = RGB(254,254,254);

	palette[16] = RGB(84,254,254);
	palette[17] = RGB(84,254,254);
	palette[18] = RGB(84,254,254);
	palette[19] = RGB(84,254,254);
	palette[20] = RGB(84,254,254);
	palette[21] = RGB(84,254,254);
	palette[22] = RGB(84,254,254);
	palette[23] = RGB(84,254,254);
	palette[24] = RGB(84,254,254);
	palette[25] = RGB(84,254,254);
	palette[26] = RGB(84,254,254);
	palette[27] = RGB(84,254,254);
	palette[28] = RGB(84,254,254);
	palette[30] = RGB(84,254,254);
	palette[31] = RGB(84,254,254);
	palette[32] = RGB(84,254,254);
}

void main()
{
	InitPalette();
	vec2 uv = -1.0 + 2.0 * fragCoord.xy / iResolution.xy;
	float t = iTime/2.5;
	float vx = 0.0;
	vec2 c = uv * 12.0 - 12.0/2.0;
	vx += sin((c.y+t*10.0)/2.0);
	vx += cos(sin((c.x+t*10.0)))+sin((c.x+c.y+t*10.0)/2.0)+sin((c.y+t)/2.0);
	c += 12.0/2.0 * vec2(sin(t/3.0), cos(t*10.0/2.0));
	vx += cos(sin(sqrt(c.x*c.x+c.y*c.y+12.0)+t*10.0));
	vx = vx/1.5;
	float sincos = sin(PI*vx)+cos(PI*vx);
	vec3 col = palette[int((1.0+sincos/2.0)*15.0)];
	fragColor = vec4(col, alpha);
}
