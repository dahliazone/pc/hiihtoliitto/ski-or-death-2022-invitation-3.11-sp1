#version 330 core
precision highp float;

uniform vec3      iResolution;           // viewport resolution (in pixels)
uniform float     iTime;                 // shader playback time (in seconds)
uniform float     iTimeDelta;            // render time (in seconds)
uniform int       iFrame;                // shader playback frame
uniform float     iChannelTime[4];       // channel playback time (in seconds)
uniform vec3      iChannelResolution[4]; // channel resolution (in pixels)
uniform vec4      iMouse;                // mouse pixel coords. xy: current (if MLB down), zw: click
uniform sampler2D iChannel0;             // input channel. XX = 2D/Cube
uniform sampler2D iChannel1;             // input channel. XX = 2D/Cube
uniform sampler2D iChannel2;             // input channel. XX = 2D/Cube
uniform sampler2D iChannel3;             // input channel. XX = 2D/Cube
uniform vec4      iDate;                 // (year, month, day, time in seconds)
uniform float     iSampleRate;           // sound sample rate (i.e., 44100)
uniform float     alpha;                 // alpha value

in vec2 fragCoord;
out vec4 fragColor;

void main()
{
	float time = iTime + 100;
	vec2 uv = fragCoord.xy / iResolution.xy;

	vec2 turn = uv;

	turn.x += cos(time*1.5+uv.y*4.0) * 0.15 + cos(time+uv.y*4.0) * 0.15;
	float turntime = cos(time*1.5+1.0) * 0.15 + cos(time+2.0) * 0.15;

	float ground=abs((turn.x-0.5)/(-turn.y*1.15+cos(time*3.0+time*0.2)*0.1+0.6));
	ground=round(pow(6.0*ground,1.5));

	vec3 groundcolor;

	if (turn.y<cos(time*3.0+time*0.2)*0.03+0.45 + cos(turn.x*10.0*time)*0.02){
		if(ground==0.0){
			groundcolor=vec3(0.9,0.9,0.9);
		}

		if(ground==1.0){
			groundcolor=vec3(1,1,1);

		}
		if(ground==2.0){
			groundcolor=vec3(0.8,0.8,0.8);

		}
		if(ground==3.0){
			groundcolor=vec3(0.6,0.6,0.6);

		}
		if(ground==4.0){
			groundcolor=vec3(0.8,0.8,0.8);

		}
		if(ground>=5.0){
			groundcolor=vec3(1.0,1.0,1.0);

		}

		groundcolor.rg *= clamp(round((sin((5.0/(0.55-uv.y*0.90))+(time*6.0+sin(time)*8.0))+1.0)*0.5)+0.1,0.3,1.0);
		groundcolor.b *= clamp(round((sin((5.0/(0.55-uv.y*0.90))+(time*6.0+sin(time)*8.0))+1.0)*0.5)+0.1,0.5,1.0);
	   //groundcolor *= clamp(round((sin((5.0/(0.55-uv.y*0.90))+(time*6.0+sin(time)*8.0))+1.0)*0.5)+0.3,0.5,1.0);
	}
	else {
		float mountain=((cos((uv.x+turntime+0.7)*7.0+cos(time)*0.1)+cos((uv.x+turntime)*23.0)+cos((uv.x+turntime)*13.0)+2.0)-(uv.y*3.0));
		groundcolor=vec3(mountain*0.8,mountain*0.8,mountain*0.8);

		if(groundcolor.y<=0.3){
		 groundcolor=vec3(0.7,0.7,0.9);
		}
	}

	fragColor = vec4(groundcolor,1.0);
}
