#version 330 core
precision highp float;

uniform vec3      iResolution;           // viewport resolution (in pixels)
uniform float     iTime;                 // shader playback time (in seconds)
uniform float     iTimeDelta;            // render time (in seconds)
uniform int       iFrame;                // shader playback frame
uniform float     iChannelTime[4];       // channel playback time (in seconds)
uniform vec3      iChannelResolution[4]; // channel resolution (in pixels)
uniform vec4      iMouse;                // mouse pixel coords. xy: current (if MLB down), zw: click
uniform sampler2D iChannel0;             // input channel. XX = 2D/Cube
uniform sampler2D iChannel1;             // input channel. XX = 2D/Cube
uniform sampler2D iChannel2;             // input channel. XX = 2D/Cube
uniform sampler2D iChannel3;             // input channel. XX = 2D/Cube
uniform vec4      iDate;                 // (year, month, day, time in seconds)
uniform float     iSampleRate;           // sound sample rate (i.e., 44100)
uniform float     alpha;                 // alpha value

in vec2 fragCoord;
out vec4 fragColor;

#define RGB(r,g,b) (vec3(r,g,b) / 255.0);
#define PI 3.1415926535897932384626433832795
vec3 palette[33];

//Initalizes the color palette.
void InitPalette()
{
	//16-Color EGA color palette.
	palette[ 0] = RGB(0,0,0);
	palette[ 1] = RGB(84,84,84);
	palette[ 2] = RGB(84,84,254);
	palette[ 4] = RGB(84,254,84);
	palette[15] = RGB(84,254,254);
	palette[ 6] = RGB(254,84,84);
	palette[ 3] = RGB(254,84,254);
	palette[ 7] = RGB(254,254,84);
	palette[ 8] = RGB(0,0,168);
	palette[ 9] = RGB(0,168,0);
	palette[10] = RGB(0,168,168);
	palette[11] = RGB(168,0,0);
	palette[12] = RGB(168,0,168);
	palette[13] = RGB(168,0,168)// RGB(168,84,0);
	palette[14] = RGB(168,168,168);
	palette[ 5] = RGB(254,254,254);

	palette[16] = RGB(84,254,254);
	palette[17] = RGB(84,254,254);
	palette[18] = RGB(84,254,254);
	palette[19] = RGB(84,254,254);
	palette[20] = RGB(84,254,254);
	palette[21] = RGB(84,254,254);
	palette[22] = RGB(84,254,254);
	palette[23] = RGB(84,254,254);
	palette[24] = RGB(84,254,254);
	palette[25] = RGB(84,254,254);
	palette[26] = RGB(84,254,254);
	palette[27] = RGB(84,254,254);
	palette[28] = RGB(84,254,254);
	palette[30] = RGB(84,254,254);
	palette[31] = RGB(84,254,254);
	palette[32] = RGB(84,254,254);
}

void main()
{
	float time = iTime+900.;
	InitPalette();
	float t = time*2.0;
	float t1 = time*10.0;
	vec2 p = (-iResolution.xy + 2.0*fragCoord)/iResolution.y;
	float a = sin(cos(sin(atan(p.y,p.x))));
	float r = length(p)*cos(mod(a+.785, 1.57)-0.785);
	vec2 uv = vec2(1.0/r + 0.1*t1, a + 2.5*t1 + .5/r*t/10.0);
	vec2 gv = sin(fract(t/5500.*uv/3.14)-.125);
	float d = abs(abs(gv.x+gv.y)-.5);
	float f = smoothstep(.01, -.01, d-.25);
	float sincos = sin(PI*t);
	vec3 col = tan(0.5*sin(f + palette[int((1.0+sincos/2.0)*15.0)]));
	col = col*r;
	fragColor = vec4(col, alpha);
}

