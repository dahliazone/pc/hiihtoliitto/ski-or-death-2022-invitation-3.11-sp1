#version 330 core
precision highp float;

uniform vec3      iResolution;           // viewport resolution (in pixels)
uniform float     iTime;                 // shader playback time (in seconds)
uniform float     iTimeDelta;            // render time (in seconds)
uniform int       iFrame;                // shader playback frame
uniform float     iChannelTime[4];       // channel playback time (in seconds)
uniform vec3      iChannelResolution[4]; // channel resolution (in pixels)
uniform vec4      iMouse;                // mouse pixel coords. xy: current (if MLB down), zw: click
uniform sampler2D iChannel0;             // input channel. XX = 2D/Cube
uniform sampler2D iChannel1;             // input channel. XX = 2D/Cube
uniform sampler2D iChannel2;             // input channel. XX = 2D/Cube
uniform sampler2D iChannel3;             // input channel. XX = 2D/Cube
uniform vec4      iDate;                 // (year, month, day, time in seconds)
uniform float     iSampleRate;           // sound sample rate (i.e., 44100)
uniform float     alpha;                 // alpha value

in vec2 fragCoord;
out vec4 fragColor;

void main()
{
	float imageRatio = 0.64;
	float scrollLength = 1. - imageRatio;
	float effectLength = 17.;

	// Normalized pixel coordinates (from 0 to 1)
	vec2 uv = fragCoord/iResolution.xy;
	uv.y *= -1.; // TODO: fix this from engine!
	uv.x *= imageRatio;
	uv.x += (iTime / effectLength) * scrollLength;

	uv.x += sin(uv.y * 55.f + 5*iTime) * 0.001f;
	uv.y -= sin(uv.y * 35.f + 3*iTime) * 0.0015f;

	// Time varying pixel color
	vec4 col = texture(iChannel0, uv).rgba;

	// Output to screen
	fragColor = vec4(col.rgb, col.a * alpha);
}
