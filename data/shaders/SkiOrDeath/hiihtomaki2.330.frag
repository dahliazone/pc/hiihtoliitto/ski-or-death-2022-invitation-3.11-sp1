#version 330 core
precision highp float;

uniform vec3      iResolution;           // viewport resolution (in pixels)
uniform float     iTime;                 // shader playback time (in seconds)
uniform float     iTimeDelta;            // render time (in seconds)
uniform int       iFrame;                // shader playback frame
uniform float     iChannelTime[4];       // channel playback time (in seconds)
uniform vec3      iChannelResolution[4]; // channel resolution (in pixels)
uniform vec4      iMouse;                // mouse pixel coords. xy: current (if MLB down), zw: click
uniform sampler2D iChannel0;             // input channel. XX = 2D/Cube
uniform sampler2D iChannel1;             // input channel. XX = 2D/Cube
uniform sampler2D iChannel2;             // input channel. XX = 2D/Cube
uniform sampler2D iChannel3;             // input channel. XX = 2D/Cube
uniform vec4      iDate;                 // (year, month, day, time in seconds)
uniform float     iSampleRate;           // sound sample rate (i.e., 44100)
uniform float     alpha;                 // alpha value

in vec2 fragCoord;
out vec4 fragColor;

float W (float g,float i)
{
	return sin(g*pow(3.,i))/pow(2.,i+1.) ;
}

#define RGB(r,g,b) (vec3(r,g,b) / 255.0);
vec3 palette[16];

void initPalette()
{
	palette[ 0] = RGB(0,0,0);
	palette[ 1] = RGB(84,84,84);
	palette[ 0] = RGB(84,84,254);
	palette[ 4] = RGB(84,254,84);
	palette[15] = RGB(84,254,254);
	palette[ 6] = RGB(254,84,84);
	palette[ 3] = RGB(254,84,254);
	palette[ 7] = RGB(254,254,84);
	palette[ 0] = RGB(0,0,168);
	palette[ 9] = RGB(0,168,0);
	palette[10] = RGB(0,168,168);
	palette[11] = RGB(168,0,0);
	palette[12] = RGB(168,0,168);
	palette[13] = RGB(168,0,168);
	palette[14] = RGB(168,168,168);
	palette[ 5] = RGB(254,254,254);
}

void main()
{
	initPalette();
	vec2 uv = fragCoord.xy / iResolution.xy;

	vec2 turn = uv;

	turn.x += cos(iTime*1.5+uv.y*4.0) * 0.15 + cos(iTime+uv.y*4.0) * 0.15;
	float turntime = cos(iTime*1.5+1.0) * 0.15 + cos(iTime+2.0) * 0.15;

	float ground=abs((turn.x-0.5)/(-turn.y*1.15+cos(iTime*3.0+iTime*0.2)*0.1+0.6));
	ground=round(pow(5.0*ground,2.5));

	vec3 groundcolor;

	if(turn.y<cos(iTime*3.0+iTime*0.2)*0.03+0.42 + cos(turn.x*100.0*(iTime+100.0))*0.05 + sin(turn.x*iTime)*0.02){
		if(ground==0.0){
			groundcolor=vec3(1,1,1);

		}

		if(ground==1.0){
			groundcolor=vec3(1,1,1);

		}
		if(ground==2.0){
			groundcolor=vec3(0.8,0.8,0.8);

		}
		if(ground==3.0){
			groundcolor=vec3(0.6,0.6,0.6);

		}
		if(ground==4.0){
			groundcolor=vec3(0.8,0.8,0.8);

		}
		if(ground>=5.0){
			groundcolor=vec3(1.0,1.0,1.0);

		}

		groundcolor.rg *= clamp(round((sin((5.0/(0.55-uv.y*0.90))+(iTime*6.0+sin(iTime)*8.0))+1.0)*0.5)+0.1,0.3,1.0);
		groundcolor.b *= clamp(round((sin((5.0/(0.55-uv.y*0.90))+(iTime*6.0+sin(iTime)*8.0))+1.0)*0.5)+0.1,0.5,1.0);
	   //groundcolor *= clamp(round((sin((5.0/(0.55-uv.y*0.90))+(iTime*6.0+sin(iTime)*8.0))+1.0)*0.5)+0.3,0.5,1.0);

	}else{


 /*       if(abs(uv.x * uv.x + uv.y * uv.y) < 0.5) { groundcolor = vec3(0.5,0.5,0.5); }

		float mountain=((cos((uv.x+turntime+0.7)*7.0+cos(iTime)*0.2)+cos((uv.x+turntime)*23.0)+cos((uv.x+turntime)*13.0)+2.0)-(uv.y*4.0));
		groundcolor=vec3(mountain*0.5,mountain*0.5,mountain*0.5);
   */

/*if(groundcolor.y<=0.3){
		 groundcolor=vec3(0.3,0,0);
		}*/

	vec2 C = ((fragCoord/iResolution.xy)*6.-vec2(1.0,1.))*vec2(4.2831853,1.);
	float F = C.y - W(C.x,0.);

	for(int i = 0; i < clamp(5+int(iTime),5,60); i++)
	{
		F -= W((C.x/iTime*0.1),float(i+1))*12.0;
	  //  F *= sin(iTime/4.0);
	}

	F = ((1.-ceil(clamp(F,0.,1.))));

	//F*=0.2*((cos((uv.x+turntime+0.7)*1.0+cos(iTime)*1.2)+cos((uv.x+turntime)*23.0)+cos((uv.x+turntime)*13.0)+3.0)-(uv.y*4.0));
	groundcolor = vec3(F,F,F);
	}


	vec3 color=groundcolor;
	fragColor = vec4(palette[int((color.x + color.y +
				color.z) / 3.0 * 15.0)], 1.0);
	//fragColor = vec4(groundcolor,1.0);
}
