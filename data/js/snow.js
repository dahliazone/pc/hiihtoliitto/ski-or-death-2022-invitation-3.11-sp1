function animate(time, fb, isFirstTimeRun) {
    if (isFirstTimeRun) {
        // Do something only once for the framebuffer
    }

    var COLOR = 7;
    var BLACK = 0;
    var COLLISION = 3;

    var horizontalOptions = [-1, 0, 1];

    var getRandomInt = function(max) {
        return Math.floor(Math.random() * max);
    }

    for (var i=0; i< 320; i++) {
        fb[i] = getRandomInt(24) < 2 ? COLOR : BLACK;
    }

    for (var y = 0; y < 200; y++) {
        for (var x = 0; x < 320; x++) {
            var currentPixel = y * 320 + x;
            var nextPixel = (y * 320 + x) + 320;
            var horizontalOffset = horizontalOptions[getRandomInt(3)];
            var currentColor = fb[currentPixel];
            var offsetColor = fb[currentPixel + 320 + horizontalOffset];

            if (currentColor == COLOR) {
                fb[currentPixel] = BLACK;

                if (offsetColor == BLACK) {
                    if (offsetColor != COLOR) {
                        fb[currentPixel + horizontalOffset + 320] = COLOR;
                    } else {
                        fb[currentPixel] = COLOR;
                    }
                } else {
                    fb[currentPixel] = COLOR;
                }
            }
        }
    }
}
