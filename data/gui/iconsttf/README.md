# TTF files for IconFontCppHeaders library

2017/08/21 snapshot with license files included.

* [Font Awesome](http://fontawesome.io/)
    * [github repository](https://github.com/FortAwesome/Font-Awesome/)
    * [fontawesome-webfont.ttf](https://github.com/FortAwesome/Font-Awesome/blob/master/fonts/fontawesome-webfont.ttf)
* [Google Material Design icons](https://design.google.com/icons/)
    * [github repository](https://github.com/google/material-design-icons/)
    * [MaterialIcons-Regular.ttf](https://github.com/google/material-design-icons/blob/master/iconfont/MaterialIcons-Regular.ttf)
* [Kenney Game icons](http://kenney.nl/assets/game-icons) and [Game icons expansion](http://kenney.nl/assets/game-icons-expansion)
    * [github repository](https://github.com/nicodinh/kenney-icon-font/)
    * [kenney-icon-font.ttf](https://github.com/nicodinh/kenney-icon-font/blob/master/fonts/kenney-icon-font.ttf)

