# Ski or Death! invitation 2021

Powered by GrinderKit+Verso demoengine by Dahlia.

## Command-line parameters

```
 -i,--input <FILE>            Input file (default: data/grinderkit.json)
 -d,--display <DISPLAY>       Display index or name, e.g. 0 or "LG" (default: 0)
 -D,--displays                List displays (and exit)
 -s,--downscale <RATIO>       Resolution downscale ratio: 1:1, 1:2, 1:4, 1:8 (default: 1:1)
 -f,--fullscreen-desktop      Fullscreen with desktop resolution (default)
 -w,--window                  Windowed mode
 -m,--displaymode <MODE>      Display mode, e.g. 0 or "1920x1080 @ 60hz" (not applied for --fullscreen-desktop)
 -M,--displaymodes            List display modes for selected display & windowed/fullscreen option (and exit)
 -a,--audio <DEVICE>          Audio device, e.g. 0 or "Built-in output" (default: system default)
 -A,--listaudio               List audio devices (and exit)
 -U,--muteaudio               Mute audio
 -V,--novsync                 Disable vsync
 -R,--unrestrictedfps         Unrestricted FPS
 -k,--skipdialog              No setup dialog (use defaults & given options)
 -e,--record <RESW> <RESH>    Record demo to images (also mute audio, disable vsync, skip dialog)
 -E,--recordformat <FORMAT>   Record image format: "Png", "Bmp", "Tga" (default: "Png")
 -h,--help                    This help (and exit)
```


## Building on Windows

1) Install required software

* Microsoft Visual Studio (Community) 2019 with `Desktop C++ development` and `CMake` support installed.
* Python for Windows: https://www.python.org/downloads/windows/
* Optional for sometimes nicer IDE: Qt Creator (comes with latest Qt Onliner installer)


2) Fetch library dependencies: (NOT NEEDED IN THIS REPOSITORY AS THEY'RE INCLUDED)

Run `fetch_externals.cmd` inside `lib` directory or `fetch_externals.sh` if you use Bash.


3) Build (Visual Studio):

* Open the project root directory with Visual Studio 2019
* Click on the toolbar dropdown item arrow in the list which lists the default configuration (probably x64-Debug)
* Click on `Manage configurations...` and make sure you have `x64-Debug` and `x64-Release` configurations added there.
* Click on `Build All` in `BUILD` menu.
* Build will appear under `out/build/<Build Configuration>/dist/<ProjectName>` directory.


4) Run (Visual Studio):

* Click on the toolbar dropdown item arrow which says `Select Startup Item...` and choose the only listed .exe file.
* Click on the run button in the same toolbar item


5) Run with command line paratemers (Visual Studio):

* Make sure you have the executable build and selected as default run configuration
* Find the `Solution Explorer` area and click on the `Switch Views` icon under title
* Change to `CMake Targets View` by double clicking it.
* Find the executable on the list and right click on it and select `Add Debug Configuration`.
* `launch.vs.json` file will open where you should add the following line (or whatever parameters you want) inside the configuration:

```
    "args": ["--window", "--skipdialog", "--muteaudio", "--input data/shadereffect.json"]
```
* Save the file and try if it work! (it should)

More info: https://docs.microsoft.com/en-us/cpp/build/configure-cmake-debugging-sessions?view=msvc-160


6) Optional: Build & Run with Qt Creator

* Check that MSVC 2019 compiler is found and add it in the Prefences if it's missing.
* Open the CMakeLists.txt as project in Qt Creator with MSVC as the compiler and with all the default build targets
* It should work as a charm!


## Building on Linux

1) Install dependencies (Ubuntu 18.04+):

```
sudo apt-get install cmake imagemagick build-essential \
    libasound2-dev libdbus-1-dev libegl1-mesa-dev libgl1-mesa-dev \
    libgles2-mesa-dev libglu1-mesa-dev libibus-1.0-dev \
    libmirclient-dev libpulse-dev libsndio-dev libudev-dev \
    libwayland-dev libx11-dev libxcursor-dev libxext-dev \
    libxi-dev libxkbcommon-dev libxrandr-dev libxss-dev \
    libxt-dev libxv-dev libxxf86vm-dev libxinerama-dev
```


2) Fetch externals: (NOT NEEDED IN THIS REPOSITORY AS THEY'RE INCLUDED)

```
cd lib
./fetch_externals.sh
cd ..
```


3.a) Build with cmake & gcc

```
cd build
cmake .. && make -j9
```


3.b) Build/develop with Qt Creator IDE.

* Open the CMakeLists.txt as project in Qt Creator with all the default build targets
* It should work as a charm!


4) Optional: Making release build for Linux:

```
cd build
cmake .. -DBUILD_PROJECT_STATIC=OFF -DBUILD_PROJECT_LINUX_APPIMAGE=ON -DCMAKE_BUILD_TYPE=Release && make -j9
```

5) Optional: Running cppcheck

Download & build latest version (2.0) from http://cppcheck.sourceforge.net/
or install older version with package manager:
```
sudo apt-get install cppcheck
```

```
cd build
cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=ON ..
cppcheck --project=compile_commands.json
cppcheck --force --enable=all --project=compile_commands.json -i lib/SDL2  2> err.txt
```

This command could be improved because there's a lot of noise from 3rd party libraries.

## Video capture

1) Capture frames at 4K vertical resolution

```
./SkiOrDeath -e 3840 2160 -E Tga
```

Result TGAs take about 54 GB. Use Png for much slower processing and less hard drive space.

2) Copy audio

```
cd record
cp ../data/music/skiordie.mp3 audio.mp3
```

3) Two pass FFMPEG encode to VP9 codec with super high quality

```
ffmpeg -threads 8 -framerate 60 -pattern_type glob -i '*.tga' -i audio.mp3 -c:v libvpx-vp9 -b:v 0 -crf 17 -b:a 160k -r 60 -pass 1 -an -f webm \
-tile-columns 6 -frame-parallel 1  -pix_fmt yuv420p /dev/null && ffmpeg -threads 8 -framerate 60 -pattern_type glob -i '*.tga' -i audio.mp3 \
-c:v libvpx-vp9 -b:v 0 -crf 17 -b:a 160k -r 60 -pass 2 -c:a libopus -tile-columns 6 -frame-parallel 1 -auto-alt-ref 1 -lag-in-frames 25 -pix_fmt yuv420p \
output-crf17.webm
```

Result takes about 1,7 GB for 3min 12s demo at 4:3 4K resolution.
Less quality probably would suffice for most material.
This was good for Youtube uploading but bad for local watching.

Note: for testing encoding use -t SECONDS or (HH:)MM:SS.ms parameter around the end
and/or maybe limit encoding to certain frames.

