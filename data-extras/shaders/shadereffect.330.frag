#version 330 core
precision highp float;

//https://www.shadertoy.com/view/Ml2GDR#
//iChannel0 = shadertoy/tex07.jpg
//iChannel1 = shadertoy/tex07.jpg

uniform vec3      iResolution;           // viewport resolution (in pixels)
uniform float     iTime;                 // shader playback time (in seconds)
uniform float     iTimeDelta;            // render time (in seconds)
uniform int       iFrame;                // shader playback frame
uniform float     iChannelTime[4];       // channel playback time (in seconds)
uniform vec3      iChannelResolution[4]; // channel resolution (in pixels)
uniform vec4      iMouse;                // mouse pixel coords. xy: current (if MLB down), zw: click
uniform sampler2D iChannel0;             // input channel. XX = 2D/Cube
uniform sampler2D iChannel1;             // input channel. XX = 2D/Cube
uniform sampler2D iChannel2;             // input channel. XX = 2D/Cube
uniform sampler2D iChannel3;             // input channel. XX = 2D/Cube
uniform vec4      iDate;                 // (year, month, day, time in seconds)
uniform float     iSampleRate;           // sound sample rate (i.e., 44100)


uniform vec2      testVec2;
uniform vec3      testVec3;
uniform vec4      testVec4;
uniform ivec2      testIVec2;
uniform ivec3      testIVec3;
uniform ivec4      testIVec4;
uniform uvec2      testUVec2;
uniform uvec3      testUVec3;
uniform uvec4      testUVec4;

uniform vec2[3]      testVec2Arr;
uniform vec3[3]      testVec3Arr;
uniform vec4[3]      testVec4Arr;
uniform ivec2[3]      testIVec2Arr;
uniform ivec3[3]      testIVec3Arr;
uniform ivec4[3]      testIVec4Arr;
uniform uvec2[3]      testUVec2Arr;
uniform uvec3[3]      testUVec3Arr;
uniform uvec4[3]      testUVec4Arr;

in vec2 fragCoord;
out vec4 fragColor;

float height(in vec2 uv) {
	return texture(iChannel0,uv).b*texture(iChannel1,uv+vec2(0.0,iTime*0.1)).b;
}

const vec2 NE = vec2(0.05,0.0);
vec3 normal(in vec2 uv) {
	return normalize(vec3(height(uv+NE.xy)-height(uv-NE.xy),
						  0.0,
						  height(uv+NE.yx)-height(uv-NE.yx)));
}


const vec4 waterColor = vec4(0.1,0.1,0.32,1.0);
vec3 lightDir = normalize(vec3(10.0,15.0,5.0));

void main() {
	vec2 uv = fragCoord.xy/iResolution.xy-vec2(.5);
	uv.y *= iResolution.y/iResolution.x;
	//uv *= 3.;

	float foo0 = iTimeDelta;
	int foo1 = iFrame;
	float foo2 = iSampleRate;
	float foo3 = iChannelTime[0];
	float foo4 = iChannelTime[2];
	vec3 foo5 = iChannelResolution[0];
	vec3 foo6 = iChannelResolution[1];
	vec2 foo7 = testVec2;
	vec3 foo8 =     testVec3;
	vec4 foo9 =     testVec4;
	ivec2 foo10 =     testIVec2;
	ivec3 foo11 =     testIVec3;
	ivec4 foo12 =     testIVec4;
	uvec2 foo13 =     testUVec2;
	uvec3 foo14 =     testUVec3;
	uvec4 foo15 =     testUVec4;

	vec2 foo16 = testVec2Arr[0];
	vec3 foo17 =     testVec3Arr[0];
	vec4 foo18 =     testVec4Arr[0];
	ivec2 foo19 =     testIVec2Arr[0];
	ivec3 foo20 =     testIVec3Arr[0];
	ivec4 foo21 =     testIVec4Arr[0];
	uvec2 foo22 =     testUVec2Arr[0];
	//uvec3 foo230 =     testUVec3Arr[0];
	//uvec3 foo231 =     testUVec3Arr[1];
	uvec3 foo232 =     testUVec3Arr[2];
	uvec4 foo24 =     testUVec4Arr[1];

	float dist = length(uv);
	float angle = atan(uv.y,uv.x);

	vec2 ruv = uv;
	uv = vec2(cos(angle+dist*3.),dist+(iTime*0.2));

	float h = height(uv);
	vec3 norm = normal(uv);
	fragColor =
		mix(vec4(0.), mix(mix(waterColor+waterColor*max(0.0,dot(lightDir,norm))*0.1,
		texture(iChannel0,uv),0.2),
					   texture(iChannel0,norm.xz*0.5+0.5),0.3),min(1.,length(ruv)*10.));
}

